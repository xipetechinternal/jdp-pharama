<?php
/**
 * Single Product Sale Flash
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

/*if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $post, $product;
?>
<?php if ( $product->is_on_sale() ) : ?>

	<?php echo apply_filters( 'woocommerce_sale_flash', '<span class="onsale">' . __( 'Sale!', 'woocommerce' ) . '</span>', $post, $product ); ?>

<?php endif; ?>*/
?>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">

<div id="contentcolumn" class="container-fluid contentcolumn col--9 mama--12"> 
  <!--  REMOVE ANTHEM div id="Anthem_ctl00_MessageBox1_pnlMain__"></div-->
  
  <div id="ctl00_MainContentHolder_ValidationSummary1" class="bcErrorBox"></div>
  
  <!--<div id="ProductPageBreadCrumb">&nbsp;</div>-->
  
  <div class="ProductPageContainer" id="ProductImageGrid1" style="display:block;">
    <div class="ProductPageContainerTitle">
      <h2><span id="ctl00_MainContentHolder_VariantsDisplay_modifierTemplate2_HeaderLabel">1. Choose Your Layout</span></h2>
    </div>
    <div class="ProductPageContainerContent">
      <div id="step-layout" class="ProductImageGrid step-2">
        <?php
	  //Meta Details for Productt layout
	    $id = get_the_ID();
	  	$Product_layout_image = get_post_meta( $id, '_Product_layout', true );
		$Product_layout_text = get_post_meta( $id, '_Layout_Option_Text', true );
		//print_r($Product_layout_text);die;
		?>
        <div class="row"> 
         
         <?php if(!empty($Product_layout_image)){
			 foreach($Product_layout_image as $key=>$products){
				 ?>
                  <div class="col-md-4 col--4 papa--4 mama--6">
              <div class=" <?php if($_POST['layout_key']==$key and isset($_POST['layout_key'])){?>colnmselected<?php }?>" style=" cursor:pointer" onclick="layout_selected($(this),<?php echo $key?>)">
              <div class="row" style="height:120px;  text-align:center">
                
                  <label class="image-label" for="option-1708"> <img style="cursor:pointer; width:150px; height:100px"  class="imageRadioButtonImage" src="<?php echo $products;?>" alt="frame-1708"> <div class="imageRadioButtonLabel"><?php echo $Product_layout_text[$key];?> </div> </label>
                
                
              </div>
              </div>
             </div>
          <?php }
		 }else{ echo 'NO Layout For This Product'; } ?>
        </div>
      </div>
      <div class="clear">&nbsp;</div>
    </div>
    <div class="ProductPageContainerBottom">&nbsp;</div>
  </div>
  <div id="ProductControls">
    <div id="ProductOptions">
      <div id="ProductVariations">
        <div id="ctl00_MainContentHolder_VariantsDisplay_VariantsPanel" class="variantsdisplay">
          <div id="ProductSizeModifier" class="ProductPageContainer">
            <div class="ProductPageContainerTitle">
              <h2><span id="ctl00_MainContentHolder_VariantsDisplay_modifierTemplate1_HeaderLabel">2. Canvas Size</span></h2>
            </div>
            <div class="ProductPageContainerContent SizeOptionsContainer">
              <div id="SizeValidatorMessage" style="display:none;" class="errormessage ProductError">Please select a canvas
                size. </div>
              <div class="row">
                <div id="ctl00_MainContentHolder_VariantsDisplay_modifierTemplate1_DescriptionDiv" class="ProductPageContainerContentDescription col--12 col-md-12 text-left">
                  <div class=""> <b>Tip</b>: Select <b>standard size</b> or <b>custom size</b> below. Don't worry about cropping or aspect
                    ratios or orientation at this stage. Our experienced designers will send you a proof and intelligently crop the image
                    for you so your image looks great.<br>
                    <?php  if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product, $post;
?>
                    <?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>
                    <form class="variations_form cart" method="post" enctype='multipart/form-data' data-product_id="<?php echo $post->ID; ?>" data-product_variations="<?php echo esc_attr( json_encode( $available_variations ) ) ?>">
                    <input type="hidden" id="layout_key" name="layout_key" value="" />
                    <input type="hidden" id="edge_key" name="edge_key" value="" />
                    
                    <input type="hidden" id="upload_img" name="upload_img" value="" />
                    <input type="hidden" id="CopyRightCheckBox_upload" name="CopyRightCheckBox_upload" value="" />
                    <input type="hidden" id="apply_picture_perfect_checkbox" name="apply_picture_perfect_checkbox" value="" />
                    <input type="hidden" id="design_instructions_textarea" name="design_instructions_textarea" value="" />
                    <input type="hidden" id="FREE_DIGITAL_PROOF" name="FREE_DIGITAL_PROOF" value="" />
                      <?php if ( ! empty( $available_variations ) ) : ?>
                      <table class="variations" cellspacing="0">
                        <tbody>
                          <?php $loop = 0; foreach ( $attributes as $name => $options ) : $loop++; ?>
                          <tr>
                            <td class="label"><label for="<?php echo sanitize_title($name); ?>"><?php echo wc_attribute_label( $name ); ?></label></td>
                            <td class="value"><select id="<?php echo esc_attr( sanitize_title( $name ) ); ?>" name="attribute_<?php echo sanitize_title( $name ); ?>">
                                <option value=""><?php echo __( 'Choose an option', 'woocommerce' ) ?>&hellip;</option>
                                <?php
								if ( is_array( $options ) ) {

									if ( isset( $_REQUEST[ 'attribute_' . sanitize_title( $name ) ] ) ) {
										$selected_value = $_REQUEST[ 'attribute_' . sanitize_title( $name ) ];
									} elseif ( isset( $selected_attributes[ sanitize_title( $name ) ] ) ) {
										$selected_value = $selected_attributes[ sanitize_title( $name ) ];
									} else {
										$selected_value = '';
									}

									// Get terms if this is a taxonomy - ordered
									if ( taxonomy_exists( sanitize_title( $name ) ) ) {

										$orderby = wc_attribute_orderby( sanitize_title( $name ) );

										switch ( $orderby ) {
											case 'name' :
												$args = array( 'orderby' => 'name', 'hide_empty' => false, 'menu_order' => false );
											break;
											case 'id' :
												$args = array( 'orderby' => 'id', 'order' => 'ASC', 'menu_order' => false, 'hide_empty' => false );
											break;
											case 'menu_order' :
												$args = array( 'menu_order' => 'ASC', 'hide_empty' => false );
											break;
										}

										$terms = get_terms( sanitize_title( $name ), $args );

										foreach ( $terms as $term ) {
											if ( ! in_array( $term->slug, $options ) )
												continue;

											echo '<option value="' . esc_attr( $term->slug ) . '" ' . selected( sanitize_title( $selected_value ), sanitize_title( $term->slug ), false ) . '>' . apply_filters( 'woocommerce_variation_option_name', $term->name ) . '</option>';
										}
									} else {

										foreach ( $options as $option ) {
											echo '<option value="' . esc_attr( sanitize_title( $option ) ) . '" ' . selected( sanitize_title( $selected_value ), sanitize_title( $option ), false ) . '>' . esc_html( apply_filters( 'woocommerce_variation_option_name', $option ) ) . '</option>';
										}

									}
								}
							?>
                              </select>
                              <?php
							if ( sizeof( $attributes ) == $loop )
								echo '<a class="reset_variations" href="#reset">' . __( 'Clear selection', 'woocommerce' ) . '</a>';
						?></td>
                          </tr>
                          <?php endforeach;?>
                        </tbody>
                      </table>
                      <?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>
                      <div class="single_variation_wrap" style="display:none;">
                        <?php do_action( 'woocommerce_before_single_variation' ); ?>
                        <div class="single_variation"></div>
                        <div class="variations_button">
                          <?php woocommerce_quantity_input(); ?>
                          <button type="submit" class="single_add_to_cart_button button alt"><?php echo $product->single_add_to_cart_text(); ?></button>
                        </div>
                        <input type="hidden" name="add-to-cart" value="<?php echo $product->id; ?>" />
                        <input type="hidden" name="product_id" value="<?php echo esc_attr( $post->ID ); ?>" />
                        <input type="hidden" name="variation_id" value="" />
                        <?php do_action( 'woocommerce_after_single_variation' ); ?>
                      </div>
                      <?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
                      <?php else : ?>
                      <p class="stock out-of-stock">
                        <?php _e( 'This product is currently out of stock and unavailable.', 'woocommerce' ); ?>
                      </p>
                      <?php endif; ?>
                    </form>
                    <?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>
                  </div>
                  <div class="clear">&nbsp;</div>
                </div>
              </div>
              <div class="clear">&nbsp;</div>
            </div>
            <div class="clear">&nbsp;</div>
            <div class="ProductPageContainerBottom">&nbsp;</div>
          </div>
        </div>
        <div class="ProductPageContainer RequiresSizeSelection" id="CanvasPopEdgeOptions" style="display: block;">
          <div class="ProductPageContainerTitle">
            <div id="EdgeValidatorMessage" style="display:none;" class="errormessage ProductError"> Please select an edge option. </div>
            <h2> <span id="ctl00_MainContentHolder_VariantsDisplay_modifierTemplate3_HeaderLabel">4. Edge Options</span> <span id="Anthem_ctl00_MainContentHolder_VariantsDisplay_modifierTemplate3_CustomValidatorEdgeOption__"> <span id="ctl00_MainContentHolder_VariantsDisplay_modifierTemplate3_CustomValidatorEdgeOption" class="errormessage" style="visibility:hidden;">*</span> </span> </h2>
          </div>
          <div class="ProductPageContainerContent" id="UploadOptionsTab">
            <div class="clear">&nbsp;</div>
            <span id="ctl00_MainContentHolder_VariantsDisplay_modifierTemplate3_rfvEdgeModifierList" class="errormessage" style="color:Red;display:none;"></span>
            <div id="ctl00_MainContentHolder_VariantsDisplay_modifierTemplate3_EdgeModifierList" class="EdgeModifierRadioButtonList row">
            
            <?php
			 //Meta Details for Productt Edge Options
	    $id = get_the_ID();
	  	  $field_value = get_post_meta( $id, '_Edge_Opton_image', true );
		  $field_value_txt = get_post_meta($id, '_Edge_Opton_text', true );
		?>
        
        
         <?php if(!empty($field_value)){
			 foreach($field_value as $key=>$products){
				 ?>
              <div class="edgeModifierOption imageRadioButtonOption col-md-4 col--3 papa--4 mama--6" price="0" displayname="Black Border">
                <div class="row">
                  <div class="col--12 col-md-12 col--lostbear centered">
                    <label class="image-label" for="ctl00_MainContentHolder_VariantsDisplay_modifierTemplate3_EdgeModifierList_<?php echo $key?>"> <img class="imageRadioButtonImage" style="height:120px; width:120px;" src="<?php echo $products;?>" alt="edge-1681"> </label>
                  </div>
                  <div class="col--12 col--lostbear" style="width:80%">
                    <input <?php if($_POST['edge_key']==$key and isset($_POST['edge_key'])){?>checked="checked" <?php } ?> style="float:left" id="ctl00_MainContentHolder_VariantsDisplay_modifierTemplate3_EdgeModifierList_<?php echo $key?>" type="radio" name="edge" data-name="Black Border" data-price="0" value="<?php echo $key?>" data-ga-track-click="true" class="edge_layout" data-ga-category="Create and Order" data-ga-action="clicked_edge_option" data-ga-label="Black Border">
                    <label class="text-label" for="ctl00_MainContentHolder_VariantsDisplay_modifierTemplate3_EdgeModifierList_1681">
                    <div class="imageRadioButtonLabel"> <?php echo $field_value_txt[$key];?> </div>
                   <!-- <div class="SitePrice"> No extra cost </div>-->
                    </label>
                  </div>
                </div>
              </div>
              <?php }
		 }else{
			 echo "No Edge Option For this Product";
		 }?>
              
              
              
            </div>
            <div class="clear">&nbsp;</div>
          </div>
          <div class="ProductPageContainerBottom">&nbsp;</div>
        </div>
        <div class="ProductPageContainer RequiresSizeSelection" id="CanvasPhotoModule" style="display: block;">
          <div class="ProductPageContainerTitle">
            <h2> <span id="ctl00_MainContentHolder_VariantsDisplay_inputTemplate4_HeaderLabel">5. Upload Your Photo</span> </h2>
          </div>
          <div class="ProductPageContainerContent">
            <div id="SWFS3Upload" class="row">
              <div class="col--12 col-md-12">
                <div id="SWFS3Upload_Pre" <?php if($_POST['upload_img']!=''){?> style="display: none;" <?php }?>>
                  <div id="SWFS3Upload_Pre_upload">
                    <div id="browsMessage" class="clr">
                      <p id="BrowsHelpText">Click browse to begin. Once you select your file, it will upload automatically.</p>
                    </div>
                    <div class="row">
                      <div id="SWFS3Upload_Pre_upload_controls" class="col--8 mama--12"> 
                        <!--<div id='upload_error' style="display:none;margin" class='alert alert-error'></div>-->
                        
                        <div id="fileupload-container"> 
                          <!-- The file upload form used as target for the file upload widget -->
                          <div id="files-container" class="row" style="position: relative;">
                            <div class="browse-button-container col--3 papa--4 col--lostbear"> <span class="btn fileinput-button btn-cp-primary" id="pickfiles" data-ga-track-click="true" data-ga-category="Create and Order" data-ga-action="clicked_select_picture" style="position: relative; z-index: 0;"> <i class="icon-search icon-white"></i> <span style="">Browse</span> </span> </div>
                            <div id="progress" class="col--9 papa--8 col--lostbear">
                              <div class="bar" style="width: 0%;"></div>
                            </div>
                            <div id="p197mnkj28mcm1aafs29g9vk9e0_html5_container" class="plupload html5" style="position: absolute; width: 0px; height: 0px; overflow: hidden; z-index: -1; opacity: 0; top: 0px; left: 0px; background: transparent;">
                              <input id="p197mnkj28mcm1aafs29g9vk9e0_html5" style="font-size: 999px; position: absolute; width: 100%; height: 100%;" type="file" accept="image/jpeg,image/tiff,image/png,image/bmp,image/gif">
                            </div>
                          </div>
                          
                          <!--                    <div id="SWFS3Upload_Pre_upload_desc" style="display:none;">--> 
                          <!--                        <span><strong>We accept TIFF, JPEG and PNG formats.</strong> Higher resolution is best but what makes us different is we can work with almost any resolution image!</span>--> 
                          <!--                    </div>--> 
                          
                        </div>
                      </div>
                      <div id="SWFS3Upload_Progress_Message" class="col--4 mama--12 centered" style="display: none;"> <img src="//d14c5m7lr5jmwa.cloudfront.net/v2/images/tmp_img_smiley_anim.gif" alt="smiley">
                        <div id="progress-container" style="display: block;"> <span id="progress-message">Upload in progress</span> &nbsp;... </div>
                      </div>
                      <div class="col--12">
                        <div class="agree-terms row">
                          <div class="col--12 col--lostbear">
                            <input id="CopyRightCheckBox" type="checkbox" name="CopyRightCheckBox" checked="checked">
                            <label for="CopyRightCheckBox"> I certify that I own the copyright to this image (<a href="javascript:void(0);" onclick="javascript:window.open('/help/termspopup/','','width=745, height=520, menubar=no, scrollbars=yes, resizable=yes, status=no, toolbar=no')">Terms of Use</a>) </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div id="SWFS3Upload_Post" <?php if($_POST['upload_img']==''){?> style="display: none;" <?php }?>>
                  <div class="row">
                    <div class="col--6 papa--12 col--lostbear">
                      <div id="SWFS3Upload_Post_thumbnail" class="loading" style="background-image: none;">
                      <?php if($_POST['upload_img']!=''){?>
                      	<a class="SWFS3Upload_Post_thumbnailLighBoxLink SWFS3FullSize" href="<?php echo $_POST['upload_img']?>" title=""><img alt="" style="max-height: 260px; max-width: 260px; left: 242px;" class="SWFS3Upload_Post_thumbnailLighBoxMagnify" src="<?php echo $_POST['upload_img']?>"></a>
                        <?php }?>
                      </div>
                    </div>
                    <div class="col--6 papa--12 col--lostbear">
                      <div id="SWFS3Upload_Post_TextNLink" class="">
                        <div class="row">
                          <div id="SWFS3Upload_Post_Message" class="col--12 col--lostbear"> Looks Great! Uploaded Successfully! </div>
                          <div id="SWFS3Upload_Post_reUpload" class="col--12 col--lostbear"> <a id="SWFS3Upload_Post_reUpload_link" href="javascript:void(0);">Change Image</a> </div>
                        </div>
                        <div class="clr" style="height:1px; overflow:hidden;">&nbsp;</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="clear">&nbsp;</div>
          </div>
          <div class="ProductPageContainerBottom">&nbsp;</div>
        </div>
        <div id="ctl00_MainContentHolder_VariantsDisplay_modifierTemplate6_CheckboxModifier" class="ProductPageContainer CheckboxModifier RequiresSizeSelection RequiresImageSelected PicturePerfect" style="display: block;">
          <div class="ProductPageContainerTitle">
            <h2> <span id="ctl00_MainContentHolder_VariantsDisplay_modifierTemplate6_HeaderLabel">Apply Picture Perfect</span> </h2>
          </div>
          
          <!-- Picture Perfect -->
          <div class="ProductPageContainerContent">
            <div class="row">
              <div class="col--2 mama--6"> <img id="ctl00_MainContentHolder_VariantsDisplay_modifierTemplate6_ThumbnailImage" class="CheckboxModifierThumbnailImage pull-right-container" src="//d14c5m7lr5jmwa.cloudfront.net/v2/images/tmp_img_picture_perfect_thumb.jpg" alt="picture perfect"> </div>
              <div id="ctl00_MainContentHolder_VariantsDisplay_modifierTemplate6_PricePanel" class="CheckboxModifierPrice col--3 mama--6"> <span id="ctl00_MainContentHolder_VariantsDisplay_modifierTemplate6_PriceLabel"> <span class="SitePrice"> No extra cost </span> </span> </div>
              <div class="col--7 mama--12" style="padding-left:30px;">
                <div id="Anthem_ctl00_MainContentHolder_VariantsDisplay_modifierTemplate6_ModifierCheckBox__" class="row">
                  <div class="col--12 col-md-12 col--lostbear">
                    <input id="apply_picture_perfect" <?php if($_POST['apply_picture_perfect_checkbox']!=''){?> checked="checked" <?php } ?> style="float:left" type="checkbox" value="1703" data-price="0.00" name="apply_picture_perfect">
                    <div style="padding-left:15px;" for="apply_picture_perfect"> <b>Apply Picture Perfect</b><br>
                      Our designers will adjust and optimize your image so it looks its best! We optimize contrast, image sharpness and even remove red eye at no extra cost :)<br>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="clear">&nbsp;</div>
          </div>
          <div class="ProductPageContainerBottom">&nbsp;</div>
        </div>
        <div id="ctl00_MainContentHolder_VariantsDisplay_inputTemplate8_ProductTextInput" class="ProductPageContainer RequiresSizeSelection RequiresImageSelected" style="display: block;">
          <div class="ProductPageContainerTitle">
            <h2><span id="ctl00_MainContentHolder_VariantsDisplay_inputTemplate8_HeaderLabel">Design Instructions</span> </h2>
          </div>
          <div class="ProductPageContainerContent">
            <div id="DesignInstructionsValidatorMessage" style="display:none;" class="errormessage ProductError design-instructions-required">Please specify Design
              Instructions. </div>
            <div class="Form">
              <div class="FormRow">
                <textarea name="design_instructions" rows="4" cols="20" id="design_instructions"><?php echo $_POST['design_instructions_textarea']?></textarea>
              </div>
            </div>
            <div class="clear">&nbsp;</div>
          </div>
          <div class="ProductPageContainerBottom">&nbsp;</div>
        </div>
        <div id="ctl00_MainContentHolder_VariantsDisplay_modifierTemplate9_CheckboxModifier" class="ProductPageContainer CheckboxModifier RequiresSizeSelection RequiresImageSelected Digital-Proof " style="display: block;">
          <div class="ProductPageContainerTitle">
            <h2><span id="ctl00_MainContentHolder_VariantsDisplay_modifierTemplate9_HeaderLabel">Free Digital Proof <span class="is-hidden--baby">by Email </span>(optional)</span> </h2>
          </div>
          <div class="ProductPageContainerContent">
            <div id="Anthem_ctl00_MainContentHolder_VariantsDisplay_modifierTemplate9_ModifierCheckBox__">
              <div class="CheckboxModifierWithoutPrice row">
                <div class="col--12 col-md-12">
                  <input data-name="Send Digital Proof"  <?php if($_POST['FREE_DIGITAL_PROOF']!=''){?>checked="checked"<?php }  ?>  value="1687" id="ctl00_MainContentHolder_VariantsDisplay_modifierTemplate9_ModifierCheckBox" type="checkbox" name="ctl00$MainContentHolder$VariantsDisplay$modifierTemplate9$ModifierCheckBox" style="float:left">
                  <div style="padding-left:30px" class="digital-proof-text" for="ctl00_MainContentHolder_VariantsDisplay_modifierTemplate9_ModifierCheckBox">
                    <div> Free digital proofs (optional) are sent by email so you can see exactly what your canvas will look like before we print. </div>
                    <div> <strong>Allow up to 2 business days for turnaround.</strong> If you need your finished order faster or if you're pretty sure on what you want, we suggest you skip this. </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="clear">&nbsp;</div>
          </div>
          <div class="ProductPageContainerBottom">&nbsp;</div>
        </div>
        <div id="ProductControlPanelWrap" class="ProductPageContainer RequiresSizeSelection RequiresImageSelected " style="display: block;">
          <div class="ProductPageContainerTitle">&nbsp;</div>
          <div class="ProductPageContainerContent">
            <div id="ctl00_MainContentHolder_ProductControlsPanel" class="row">
              <div id="product-summary" class="col--4 mama--12" >
                <div class="product-summary-header">Here is what you selected</div>
                <div class="product-summary-item" style="display: block; ">LAYOUT:<span id="product-summary-LAYOUT-value" class="product-summary-frame-value product-summary-item-value"></span></div>
                <div class="product-summary-item" style="display: block; ">Edge:<span id="product-summary-depth-value" class="product-summary-depth-value product-summary-item-value"></span></div>
                <div class="product-summary-item" style="display: block; ">Apply Picture Perfect:<span id="product-Picture-Perfect-value" class="product-summary-depth-value product-summary-item-value"></span></div>
				<div class="product-summary-item" style="display: block; ">FREE DIGITAL PROOF:<span id="product-DIGITAL-PROOF-value" class="product-summary-depth-value product-summary-item-value"></span></div>
				<div class="product-summary-item" style="display: block; ">DESIGN INSTRUCTIONS:<span id="product-DESIGN-INSTRUCTIONS-value" class="product-summary-depth-value product-summary-item-value"></span></div>

              </div>
              <div class="col--8 mama-12" style="text-align:right; float:right">
                <div class="row">
                  <div class="Prices col--6 papa--12"> <span class="SitePrice pull-right-container"> <span class="item-total">Total <span data-value="79.00" id="grand_total">00</span></span> </span> </div>
                  <div class="BuyButtons col--6 papa--12 col--lostbear" style="margin-left:10px">
                    <div class="AddToCartButton pull-right-container">
                      <input type="submit" id="add-to-cart" class="btn start-button primary" value="Add to Cart">
                      <div class="row">
                        <div class="secureBadge">
                          <div class="col--4 mama--4 col--lostbear"> <a href="http://www.instantssl.com"> <img src="//d14c5m7lr5jmwa.cloudfront.net/v2/images/comodo_secure_100x85_transp.png" alt="SSL Certificates"> </a> </div>
                          <div class="col--8 mama--8 col--lostbear"> <span class="secureMessage">Checkout is always secure. We guarantee it!</span> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="clear">&nbsp;</div>
          </div>
          <div class="ProductPageContainerBottom">&nbsp;</div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $plugins_url = plugins_url();?>
<style>
[class*=col--] {
float: left;

min-height: 1px;
margin-bottom: 20px;
margin-bottom: 1.25rem;
padding-left:1.25rem;


}
.ProductPageContainer {
max-width: 635px;
clear: both;
overflow: hidden;
}

.ProductPageContainer .ProductPageContainerTitle {
clear: both;
height: 65px;

line-height: 50px;
padding: 5px 15px 0;
background-repeat: no-repeat;
background-image: url(<?php echo $plugins_url?>/WooCommerce-addon/assets/customize_img_container_top.png);
background-size: contain;
}

.ProductPageContainerTitle h2 {
float: left;
font-size: 14px;
margin-top: 16px;
}
h2 {
font-size: 18px;
font-weight: 700;
color: #2D2D2D;
line-height: normal;
margin: 0 0 13px;
}

.ProductPageContainer .ProductPageContainerContent {

clear: both;
padding: 10px 15px 0;
background-image: url(<?php echo $plugins_url?>/WooCommerce-addon/assets/customize_img_container_middle.png);
background-repeat: repeat-y;
background-size: contain;
}
.ProductImageGrid {
width: 100%;
clear: both;
}
.row {
background-color: transparent;
}
/*.col--3 {
width: 20.9999%;
}
[class*=col--] {
float: left;

min-height: 1px;
margin-bottom: 20px;
margin-bottom: 1.25rem;

}*/
.layout_single.selected, .layout_single:hover {
background-image: url(<?php echo $plugins_url?>/WooCommerce-addon/assets/customize_img_layout_single_panel_f2.jpg);
}
.layout_single.selected, .layout_single:hover {
background-image: url(<?php echo $plugins_url?>/WooCommerce-addon/assets/customize_img_layout_single_panel_f2.jpg);
}
.layout_single {
background-image: url(<?php echo $plugins_url?>/WooCommerce-addon/assets/customize_img_layout_single_panel.jpg);
}
.layout_option {
width: 100%;
height: 152px;
display: block;
background-size: contain;
background-repeat: no-repeat;
background-position: 50%;
}
.layout_diptych {
background-image: url(<?php echo $plugins_url?>/WooCommerce-addon/assets/customize_img_layout_diptych.jpg);
}
.layout_triptych {
background-image: url(<?php echo $plugins_url?>/WooCommerce-addon/assets/customize_img_layout_triptych.jpg);
}
.layout_quad {
background-image: url(<?php echo $plugins_url?>/WooCommerce-addon/assets/customize_img_layout_quad.jpg);
}
.ProductPageContainer .clear {
height: 1px;
}
/*.clear {
clear: both;
}*/
.ProductPageContainer .ProductPageContainerBottom {

clear: both;
height: 8px;
background-image: url(<?php echo $plugins_url?>/WooCommerce-addon/assets/customize_img_container_bottom.png);
background-repeat: no-repeat;
background-size: contain;

}
.ProductPageContainerTitle h2 {
float: left;
font-size: 14px !important;
margin-top: 16px;
}

/*#footer-container h3, #footer-container h4, #footer-container h5, .CustomSizeResponse .PriceLabel, .CustomSizeResponse .SizeLabel, .alt-button, .block-button, .bootbox .btn-primary, .breadcrumb, .btn.primary, .cart-indicator .cart-button, .compare-table th, .flex, .media-items .date, .nav li a, .price-column>a, .price-grid-header, .pricing-tabs li a div, .progress-step, .promo-message, .square-radio label, .start-button, .uppercased, form label, h1, h2, h3, h4, h5, input[type=submit].start-button {
font-family: futura-pt,arial,sans-serif;
text-transform: uppercase;
font-weight: 700;
}*/
.ProductPageContainer {

clear: both;
overflow: hidden;
}
.ProductPageContainer .ProductPageContainerContent{
	text-align:left;
}
.gbtr_poduct_details_right_col{
	float:left;
	width:100%
}
.gbtr_poduct_details_left_col{
	display:none;
	width:auto;
}
#SWFS3Upload_Pre {
padding: 0 15px 0 0;
}
#SWFS3Upload_Pre_upload {
width: 100%;
clear: both;
}
#pickfiles {
text-transform: uppercase;
font-family: futura-pt,arial,sans-serif;
font-weight: 800;
padding: 5px 9px;
}
.icon-search {
background-position: -48px 0;
}
#progress {
border: 1px solid #EEE;
padding: 1px;
margin-top: -1px;
width:233px;
}
#progress .bar {
height: 27px;
right: 0;
padding: 3px 0 0;
background: #666;
background: -moz-linear-gradient(top,#666 0,#3f3f3f 100%);
background: -webkit-gradient(linear,left top,left bottom,color-stop(0%,#666),color-stop(100%,#3f3f3f));
background: -webkit-linear-gradient(top,#666 0,#3f3f3f 100%);
background: -o-linear-gradient(top,#666 0,#3f3f3f 100%);
background: -ms-linear-gradient(top,#666 0,#3f3f3f 100%);
background: linear-gradient(to bottom,#666 0,#3f3f3f 100%);
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#666666', endColorstr='#3f3f3f', GradientType=0);
}
.agree-terms {
font-size: 9pt;
}
.btn:hover, .btn:active, .btn.active, .btn.disabled, .btn[disabled] {
color: #333333;
background-color: #e6e6e6;
}
www.canvaspop.com/media="all"
.btn-cp-primary {
font-size: 8pt;
padding: 4px 14px;
background-color: #ad1a61!important;
background-repeat: repeat-x;
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#e13789", endColorstr="#ad1960");
background-image: -khtml-gradient(linear,left top,left bottom,from(#e13789),to(#ad1960));
background-image: -moz-linear-gradient(top,#e13789,#ad1960);
background-image: -ms-linear-gradient(top,#e13789,#ad1960);
background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0%,#e13789),color-stop(100%,#ad1960));
background-image: -webkit-linear-gradient(top,#e13789,#ad1960);
background-image: -o-linear-gradient(top,#e13789,#ad1960);
background-image: linear-gradient(#e13789,#ad1960);
border-color: #ad1960 #ad1960 #9b1757;
color: #fff!important;
text-shadow: 0 -1px 0 rgba(0,0,0,.26);
-webkit-font-smoothing: antialiased;
}

.btn {
display: inline-block;
padding: 4px 14px;
margin-bottom: 0;
font-size: 14px;
line-height: 20px;
text-align: center;
vertical-align: middle;
cursor: pointer;
color: #333333;
text-shadow: 0 1px 1px rgba(255, 255, 255, 0.75);
background-color: #f5f5f5;
background-image: -moz-linear-gradient(top, #ffffff, #e6e6e6);
background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#ffffff), to(#e6e6e6));
background-image: -webkit-linear-gradient(top, #ffffff, #e6e6e6);
background-image: -o-linear-gradient(top, #ffffff, #e6e6e6);
background-image: linear-gradient(to bottom, #ffffff, #e6e6e6);
background-repeat: repeat-x;
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffe6e6e6', GradientType=0);
border-color: #e6e6e6 #e6e6e6 #bfbfbf;
border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
border: 1px solid #bbbbbb;
border-bottom-color: #a2a2a2;
-webkit-border-radius: 4px;
-moz-border-radius: 4px;
border-radius: 4px;
-webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,.2), 0 1px 2px rgba(0,0,0,.05);
-moz-box-shadow: inset 0 1px 0 rgba(255,255,255,.2), 0 1px 2px rgba(0,0,0,.05);
box-shadow: inset 0 1px 0 rgba(255,255,255,.2), 0 1px 2px rgba(0,0,0,.05);
}
.btn-cp-primary {
font-size: 8pt;
padding: 4px 14px;
background-color: #ad1a61!important;
background-repeat: repeat-x;
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#e13789", endColorstr="#ad1960");
background-image: -khtml-gradient(linear,left top,left bottom,from(#e13789),to(#ad1960));
background-image: -moz-linear-gradient(top,#e13789,#ad1960);
background-image: -ms-linear-gradient(top,#e13789,#ad1960);
background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0%,#e13789),color-stop(100%,#ad1960));
background-image: -webkit-linear-gradient(top,#e13789,#ad1960);