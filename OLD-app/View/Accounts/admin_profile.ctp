<?php $this->set('title_for_layout', __('Profile',true)); ?>
<?php $this->Html->addCrumb($this->Html->tag('li',__('Profile',true),array('class'=>'active')),false,false); ?>
<div class="row-fluid">
<div class="block">
<div class="navbar navbar-inner block-header">
<div class="muted pull-left"><?php echo __('Profile'); ?></div>
</div>
<div class="block-content collapse in">
<div class="span12">
<?php echo $this->Form->create('Account',array('action' => 'admin_profile','class' => 'form-horizontal'));?>
<fieldset>
<legend><?php echo __('Edit Profile'); ?></legend>

<?php
echo $this->Form->input('User.id');
echo $this->Form->input('User.name', array('div'=>'control-group',
'before'=>' <label class="control-label">'.__('Name').' : </label><div class="controls">', 
'after'=>$this->Form->error('name', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));
echo $this->Form->input('User.email', array('div'=>'control-group', 
'before'=>' <label class="control-label">'.__('Email').' : </label><div class="controls">',
'after'=>$this->Form->error('email', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));
echo $this->Form->input('User.group_id', array('div'=>'control-group',  
'before'=>' <label class="control-label">'.__('Group').' : </label><div class="controls">', 
'after'=>'</div>','label'=>false, 'class'=>'input-xlarge focused'));
echo $this->Form->input('User.status', array('div'=>'control-group', 
'before'=>' <label class="control-label">'.__('Status').' : </label><div class="controls">',
'after'=>'</div>','label'=>false, 'class'=>'uniform_on'));
?>                                        

<div class="form-actions">
<?php echo $this->Form->submit(__('Submit'), array('class'=>'btn btn-primary', 'div'=>false));?>&nbsp;
<?php echo $this->Form->reset(__('Cancel'), array('class'=>'btn', 'div'=>false));?>
</div>

</fieldset>
<?php echo $this->Form->end();?>

</div>
</div>
</div>
</div>

