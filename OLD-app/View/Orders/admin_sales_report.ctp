<div class="col-lg-12">
<h4><a href="<?php echo $this->Html->url('/admin/users');?>">Home</a> :: Reports</h4>
      


</div>

<div class="col-lg-12 text-center">

      <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Sales Reports</h3>
      </div>
      <div class="panel-body">
       <div class="table-responsive">
             

            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                        <th><?php echo __('Client Name & Details');?></th>
                        <th><?php echo __('Order Id & Dates');?></th>
                        <th><?php echo __('Order Details');?></th> 
                        <th><?php echo __('Sales Info');?></th> 
                       
                    </tr>
                </thead>
                
                <tbody>
                
				<?php 
				$i=0;
				//print_r($users);
				foreach ($users as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				
				$product_qauantity=count($user['OrderdetailMany']);
				
				$priice=0;
				foreach($user['OrderdetailMany'] as $orderdeatils){
					$priice=$priice+($orderdeatils['price']*$orderdeatils['quantity']);
				}
				
				
				?>
                <tr <?php echo $class;?> > 
                        
               
                <td>Name:<?php echo h($user['User']['fname']); ?><br />
                	Address:<?php echo h($user['User']['address']); ?><br />
                    Phone:<?php echo h($user['User']['phone']); ?><br />
                    Email:<?php echo h($user['User']['email']); ?><br />
                    &nbsp;</td>
                <td><?php echo h($user['Order']['tmporderid']); ?><br />
                <?php echo h(date('m-d-Y H:i:s',strtotime($user['Order']['createddt']))); ?>&nbsp;</td>
               
                <td><a class=" external" data-toggle="modal" href="/admin/orders/order_ifno/<?php echo $user['Order']['id']; ?>" data-target="#myModal"><?php echo h($product_qauantity); ?> Products</a><br />Price:<?php echo h($priice); ?>&nbsp;</td>                
               <td>Name:<?php echo h($salesman[$user['User']['sid']]['fname']); ?><br />
                	Address:<?php echo h($salesman[$user['User']['sid']]['address']); ?><br />
                    Phone:<?php echo h($salesman[$user['User']['sid']]['phone']); ?><br />
                    Email:<?php echo h($salesman[$user['User']['sid']]['email']); ?><br />
                    &nbsp;</td>
                </tr>
                <?php endforeach; ?>                
                </tbody>
            </table>
            <div class="pagination pagination-right pagination-mini">
	  <ul>
		<?php echo $this->Paginator->prev(''); ?>
		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
		<?php echo $this->Paginator->next(''); ?>
	  </ul>
	</div>
       </div>
      </div>
    </div>


</div>

 
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
      </div>
      </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

