<style>
.navbar{
	z-index:0;
}
</style>
<div class="col-lg-10  main">
         <h4><a href="dashboard.php">Home</a> ::  Customers Return Credit</h4>
         </div>
<div class="clearfix"></div>
 <div class="gap20"></div>
           <?php echo $this->Form->create('ProductInfo',array('url'=>array('action'=>'return_credit','controller'=>'Orders'),'type'=>'get', 'class'=>'form-horizontal login-from')); ?>

<div class="col-lg-10  ">
  <div class="form-group">
  <div class="col-lg-2"><label for="exampleInputEmail1">Customer:</label></div>
 
  <div class="col-lg-8">
   <div class="form-group">
    
    <div class="col-lg-12 ">                  
    				<?php echo $this->Form->input('cid',array('empty'=>array(''=>'--Select Client Name--'),'options' => $clinets,
        'label' => false,
		'required'=>'required',		
        'class'=>'form-control'));?>                    
                  </div> 
  
            </div></div>

  </div>
  <div class="form-group">
  <div class="col-lg-2"><label for="exampleInputEmail1">Product NDC:</label></div>
 
  <div class="col-lg-8">
   <div class="form-group">
    
    <div class="col-lg-12 ">                  
    				<?php echo $this->Form->input('ndc',array(
        'label' => false,
			
        'class'=>'form-control'));?>                    
                  </div>

  
         <div class="clearfix"></div>
     <div class="clearfix"></div>
  <div class="col-lg-12">
  
<div class="col-lg-12 text-right"> <?php echo $this->Form->button('<i class="fa fa-search"></i> Search', array(
    'type' => 'submit',
    'class' => 'btn btn-default',
    'escape' => false
));?></div>
  </div>
  
            </div></div>

  </div>
  <?php echo $this->Form->end();?>
  <div class="clearfix"></div>
  <div class="gap20"></div>
</div>


<div class="table-responsive">
             
 <?php echo $this->Form->create('ProductInfo',array('inputDefaults' => array('label' => false),'url'=>array('action'=>'return_credit','controller'=>'Orders'),'type'=>'post', 'class'=>'form-horizontal login-from')); ?>  
 	
    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                     
                     <th><?php echo __('Product');?></th>
                        <th><?php echo __('Product NDC');?></th>
                        <th><?php echo __('InvoiceNo');?></th>
                        <th><?php echo __('Order Id');?></th>
                        <th><?php echo __('Lot Number');?></th>
                        <th><?php echo __('Quantity');?></th>
                        <th><?php echo __('Price');?></th>
                        <th><?php echo __('Action');?></th> 
                       
                       
                    </tr>
                </thead>
                
                <tbody>
                <?php if(empty($users)): ?>
		<tr>
			<td colspan="7" class="striped-info">No record found.</td>
		</tr>
		<?php endif;  ?>
        <?php echo $this->Form->input('customerid',array('placeholder' => 'From Date',
        'label' => false,
		'value'=>$cidd,
		'type'=>'hidden',
        'class'=>'form-control'));?>        
				<?php 
				$i=0;
				$total_price=0;
								
				foreach ($users as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				
				?>
                <tr <?php echo $class;?> > 
                  
               <td><?php echo h($user['ProductInfo']['Product']['productname']); ?>&nbsp;</td>
                <td><?php echo h($user['ProductInfo']['Product']['product_ndc']); ?></td>
                <td><?php echo h($user['ProductInfo']['InvoiceNo']); ?></td>
                <td>	<?php echo h($user['OrderLot']['tmporderid']); ?>&nbsp;</td>
                <td><?php echo h($user['OrderLot']['lot_no']); ?></td>
                 <td><?php echo h($user['OrderLot']['quantity']); ?></td>
                 <td><?php echo h($user['OrderLot']['price']); ?></td>
                 <td> <div class="row">
				 		<div class="col-xs-6">
						<?php echo $this->Form->input('returncredit_'.$user['OrderLot']['id'],array('type'=>'input','placeholder'=>'Quantity','label' => '','class'=>'form-control input-sm','onblur'=>"checklimit($(this),".$user['OrderLot']['quantity'].")"));?>
						</div>
						<div class="col-xs-6">
						<input type="checkbox" name="data[ProductInfo][returnCheck][]" value="<?php echo $user['OrderLot']['id']?>" placeholder="Return Credit" class="input" id="ProductInfoReturnCheck"></div></div></td>
               
             
                </tr>
                <?php endforeach; ?>                
                </tbody>
                <tfoot>
          <tr>
          <td colspan="8" align="right">
          	<?php echo $this->Form->button('Add To Return Credit', array(
    'type' => 'submit',
    'class' => 'btn btn-info',
    'escape' => false
));?>
          </td>
            
          </tr>
        </tfoot>
            </table>
           <?php echo $this->Form->end(); ?>
            <div class="pagination pagination-right pagination-mini">
	  <ul>
		<?php echo $this->Paginator->prev(''); ?>
		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
		<?php echo $this->Paginator->next(''); ?>
	  </ul>
	</div>
    <div class="gap20"></div>
    <h2>Return Credit Selected List</h2>
    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                     
                     <th><?php echo __('Product');?></th>
                        <th><?php echo __('Product NDC');?></th>
                        <th><?php echo __('InvoiceNo');?></th>
                        <th><?php echo __('Order Id');?></th>
                        <th><?php echo __('Lot Number');?></th>
                        <th><?php echo __('Quantity');?></th>
                        <th><?php echo __('Price');?></th>
                        <th><?php echo __('Action');?></th> 
                       
                       
                    </tr>
                </thead>
                
                <tbody>
                <?php if(empty($creit_data)): ?>
		<tr>
			<td colspan="7" class="striped-info">No record found.</td>
		</tr>
		<?php endif;  ?>
             
				<?php 
				$i=0;
				$total_price=0;
								
				foreach ($creit_data as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				$lot_list=array();
				foreach($user['OrderLot']['ProductInfo']['Product']['ProductInfo'] as $lots){
					$lot_list[$lots['id']]=$lots['batchno'];
				}
				?>
                <tr <?php echo $class;?> > 
                  
               <td><?php echo h($user['OrderLot']['ProductInfo']['Product']['productname']); ?>&nbsp;</td>
                <td><?php echo h($user['OrderLot']['ProductInfo']['Product']['product_ndc']); ?></td>
                <td><?php echo h($user['OrderLot']['ProductInfo']['InvoiceNo']); ?></td>
                <td>	<?php echo h($user['OrderLot']['tmporderid']); ?>&nbsp;</td>
                <td><?php echo $this->Form->input('lot'.$user['OrderLot']['id'],array('default'=>$user['OrderLot']['lot_no'],'empty'=>array(''=>'--Select Lot--'),'options' => $lot_list,'value'=>$user['OrderLot']['productinfoid'],
       'label' => false,
	   'onchange'=>"$('#lot_t".$user['OrderLot']['id']."').val('')",
	   'required'=>'required',
        'class'=>'form-control'));?>
        <?php echo $this->Form->input('lot_t'.$user['OrderLot']['id'],array(
       'label' => false,
	   'placeholder'=>'Lot',
	   'onchange'=>"$('#lot".$user['OrderLot']['id']."').val('')",
        'class'=>'form-control'));?></td>
                 <td><?php echo h($user['OrderLot']['quantity']); ?></td>
                 <td><?php echo h($user['OrderLot']['price']); ?></td>
                 <td>   <?php
                 echo $this->Form->postLink($this->Html->tag('i','Delete',array('class'=>'icon-remove icon-white')), array('action' => 'admin_return_credit_delete', $user['CreditReturn']['id'],$cidd), array("class"=>"btn btn-danger btn-mini tooltip-top","data-original-title"=>"Delete",'escape'=>false), __('Are you sure you want to Delete   ?', $user['OrderLot']['ProductInfo']['Product']['productname']));
			   ?> </td>
               
             
                </tr>
                <?php endforeach; ?>                
                </tbody>
                <tfoot>
          <tr>
          <td colspan="8" align="right">
          	<?php echo $this->Form->button('Return Credit', array(
    'type' => 'submit',
    'class' => 'btn btn-info',
    'escape' => false
));?>
          </td>
            
          </tr>
        </tfoot>
            </table>
       </div>


   <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
      </div>
      </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->   
      
<script>
function checklimit(obj,qauntity){
	console.log(parseInt(obj.val())+">="+parseInt(qauntity));
	if(parseInt(obj.val())>parseInt(qauntity)){
		alert("Return Quantity must be less than or equal to Purchase Quantity");
		obj.val('');
	}
}
$(document).ready(function(){
	$("#ProductInfoAdminSalesReportPersonForm").validate();
	//$( "#ProductInfoSelfrmdate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
   // $( "#ProductInfoSeltodate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
	
	
    $( "#ProductInfoSelfrmdate, #ProductInfoSeltodate" ).datepicker({
        //defaultDate: "+1w",
        changeMonth: true,
       dateFormat: 'yy-mm-dd' ,
        onSelect: function( selectedDate ) {
            if(this.id == 'ProductInfoSelfrmdate'){
              var dateMin = $('#ProductInfoSelfrmdate').datepicker("getDate");
              var rMin = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 1); // Min Date = Selected + 1d
              var rMax = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 365); // Max Date = Selected + 31d
              $('#ProductInfoSeltodate').datepicker("option","minDate",rMin);
              $('#ProductInfoSeltodate').datepicker("option","maxDate",rMax);                    
            }

        }
    });

});
</script>