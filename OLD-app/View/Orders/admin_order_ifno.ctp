<div class="container">
    <div class="row row-centered">
    	
        <div class="col-xs-9 col-centered">

 <button style="padding:10px" type="button" class="close btn btn-primary" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Product List</h3>
      </div>
      <div class="panel-body">
       <div class="table-responsive">
             

            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                     <th><?php echo __('Sl no.');?></th>
                        <th><?php echo __('Product Name');?></th>
                        <th><?php echo __('Quantity');?></th>
                        <th><?php echo __('Price');?></th>
                        <th><?php echo __('Lot & Exp');?></th> 
                        
                       
                       
                    </tr>
                </thead>
                
                <tbody>
                
				<?php 
				$i=0;
				$total_price=0;
				
				foreach ($Orderdetail[0]['OrderdetailMany'] as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				?>
                <tr <?php echo $class;?> > 
                   <td><?php echo $i;?>
                    &nbsp;</td>      
               
                <td><?php echo h($user['Product']['productname']); ?><br />
                	<?php echo h($user['Product']['product_ndc']); ?>
                    &nbsp;</td>
                <td><?php echo h($user['quantity']); ?>&nbsp;</td>
               
             <td><?php echo h($user['price']*$user['quantity']); ?>&nbsp;</td>
              <?php $total_price=$total_price+($user['price']*$user['quantity']);?>
              <td><?php 
					
					 
					  foreach($user["lot_details"] as $lots){
						if(!in_array(trim($lots['product_details']['ProductInfo']['id']),$lot_number)){  
						  $lot_number[]=trim($lots['product_details']['ProductInfo']['id']);
						 if($lots['product_details']['ProductInfo']['Pedigree']=="Y"){
							
							  echo $lots['product_details']['ProductInfo']['batchno']." <br> ".date("m-d-Y",strtotime($lots['product_details']['ProductInfo']['expdate']));
							  echo "&nbsp;";
						 }else{
						
							  echo $lots['product_details']['ProductInfo']['batchno']." <br> ".date("m-d-Y",strtotime($lots['product_details']['ProductInfo']['expdate'])); 
							echo "&nbsp;";  
						 }
						}
					 } ?> </td>
                </tr>
                <?php endforeach; ?>                
                </tbody>
                <tfoot>
          <tr>
          
            <th colspan="3" style="text-align:right">Total:</th>
            <th><?php echo $total_price?></th>
          </tr>
        </tfoot>
            </table>
            <div class="pagination pagination-right pagination-mini">
	  <ul>
		<?php echo $this->Paginator->prev(''); ?>
		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
		<?php echo $this->Paginator->next(''); ?>
	  </ul>
	</div>
       </div>
      </div>
    </div>


</div>
       
    </div>
</div>
 
<style>
.row-centered {
    text-align:center;
}
.col-centered {
    display:inline-block;
    float:none;
    /* reset the text-align */
    text-align:left;
    /* inline-block space fix */
    margin-right:-4px;
}
.col-fixed {
    /* custom width */
    width:320px;
}
.col-min {
    /* custom min width */
    min-width:320px;
}
.col-max {
    /* custom max width */
    max-width:320px;
}
</style>

