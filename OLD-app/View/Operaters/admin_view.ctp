
<div class="block-content collapse in">
<div class="span12">                            
<table class="table table-bordered">
    <tbody>
        <tr>
            <th><?php echo __('Id'); ?></th>
            <td><?php echo h($user['User']['id']); ?></td>
        </tr>
        <tr>
            <th><?php echo __('Email'); ?></th>
            <td><?php echo h($user['User']['email']); ?></td>
        </tr>
        
        <tr>
            <th><?php echo __('Username'); ?></th>
            <td><?php echo h($user['User']['username']); ?></td>
        </tr>
         <tr>
            <th><?php echo __('Phone'); ?></th>
            <td><?php echo h($user['User']['phone']); ?></td>
        </tr>
        <tr>
            <th><?php echo __('Address'); ?></th>
            <td><?php echo h($user['User']['address']); ?></td>
        </tr>
        <tr>
            <th><?php echo __('Group'); ?></th>
            <td><?php echo h($user['Group']['name']); ?></td>
        </tr>
        <tr>
            <th><?php echo __('Created'); ?></th>
            <td><?php echo h(date("j M Y , g:i A",strtotime($user['User']['created']))); ?></td>
        </tr>
        <tr>
            <th><?php echo __('Modified'); ?></th>
            <td><?php echo h(date("j M Y , g:i A",strtotime($user['User']['modified']))); ?></td>
        </tr>
    </tbody>
</table> 
</div>
</div>
