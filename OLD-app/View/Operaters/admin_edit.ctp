<?php $this->set('title_for_layout', __('Operaters',true)); ?>
<?php $this->Html->addCrumb(__('Operaters',true), '/admin/sales/index',array('tag'=>'li'));  ?>
<?php $this->Html->addCrumb($this->Html->tag('li',__('Edit',true),array('class'=>'active')),false,false); ?>

<div class="row-fluid">
<div class="block">
<div class="navbar navbar-inner block-header">
<div class="muted pull-left"><?php echo __('Sales User'); ?></div>
</div>
<div class="block-content collapse in">
<div class="span12">
<?php echo $this->Form->create('Operater',array('controller'=>'operaters','action' => 'admin_edit','class' => 'form-horizontal'));?>
<fieldset>
<legend><?php echo __('Add Operaters User'); ?></legend>

<?php
echo $this->Form->input('User.id');

echo $this->Form->input('User.username', array('div'=>'control-group',
'before'=>' <label class="control-label">'.__('Username').' : </label><div class="controls">', 
'after'=>$this->Form->error('User.username', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));


echo $this->Form->input('User.name', array('div'=>'control-group',
'before'=>' <label class="control-label">'.__('Name').' : </label><div class="controls">', 
'after'=>$this->Form->error('User.name', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));


echo $this->Form->input('User.email', array('div'=>'control-group', 
'before'=>' <label class="control-label">'.__('Email').' : </label><div class="controls">',
'after'=>$this->Form->error('User.email', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));

echo $this->Form->input('User.phone', array('div'=>'control-group',
'before'=>' <label class="control-label">'.__('Phone').' : </label><div class="controls">', 
'after'=>$this->Form->error('User.phone', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));

echo $this->Form->input('User.address', array('div'=>'control-group',
'before'=>' <label class="control-label">'.__('Address').' : </label><div class="controls">', 
'after'=>$this->Form->error('User.address', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));

echo $this->Form->input('User.group_id', array('div'=>'control-group',  
'before'=>' <label class="control-label">'.__('Group').' : </label><div class="controls">', 
'after'=>'</div>','label'=>false, 'class'=>'input-xlarge focused','disabled'=>true,'selected'=>array('0'=>3)));


echo $this->Form->input('User.status', array('div'=>'control-group', 
'before'=>' <label class="control-label">'.__('Status').' : </label><div class="controls">',
'after'=>'</div>','label'=>false, 'class'=>'uniform_on','type'=>'checkbox'));
?>                                        

<div class="form-actions">
<?php echo $this->Form->submit(__('Submit'), array('class'=>'btn btn-primary', 'div'=>false));?>&nbsp;
<?php echo $this->Form->reset(__('Cancel'), array('class'=>'btn', 'div'=>false));?>
</div>

</fieldset>
<?php echo $this->Form->end();?>

</div>
</div>
</div>
</div>