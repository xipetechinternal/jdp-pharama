<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
  


    	<?php echo $this->Html->charset(); ?>
        <title><?php echo $title_for_layout; ?></title>      

    <!-- Bootstrap core CSS -->
    <!-- Bootstrap -->
  
       <?php
	echo $this->Html->css('admin/bootstrap.min');
	echo $this->Html->css('admin/bootstrap-theme.min');	
	echo $this->Html->css('admin/bootstrap_calendar');
	echo $this->Html->css('admin/font-awesome');
	
	?>
   
     <?php
	echo $this->Html->css('admin/dashboard');
	echo $this->Html->css('admin/datatables');	
	echo $this->Html->css('jquery.ui.datepicker.min');	
	echo $this->Html->css('jquery.ui.core.min');
	echo $this->Html->css('jquery.ui.theme.min');
	?>
    
<?php echo $this->Html->script(array('jquery-1.8.3.min'));?>
<?php echo $this->Html->script(array('jquery.validation.functions'));?>
<?php echo $this->Html->script(array('jquery.validate.min'));?>
<?php echo $this->Html->script(array('jquery-ui-1.9.2.datepicker.custom.min'));?>
<?php echo $this->Html->script(array('bootstrap.min'));?>
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-66023494-1', 'auto');
  ga('send', 'pageview');

</script>

  </head>

  <body>

            <div class="jumbotron" >
      <div class=" row">
      <div class="col-lg-4 col-xs-6">
        
          <a class="" href="#"><img src="<?php echo $this->Html->url('/img/');?>/logo2.png"></a>
        
        </div>
<div class="col-lg-4 toppad30 col-xs-6"><h2>Admin Login Panel</h2></div>
<div class="col-lg-4 col-xs-6">
     <?php echo $this->element('admin_right');?>
     </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-2 ">
           
 <?php echo $this->element('admin_menu_left');?>
          
        </div>
        <div class="col-lg-10" id="content">
                <div class="row-fluid">    
                <?php echo $this->Session->flash(); ?> 
                <?php echo $this->element('admin_crumbs');?>	
                </div>     
                    
                <!-- Content -->
                <?php echo $this->fetch('content'); ?>		
                <!-- /Content -->
                    
                </div>
      </div>
    </div>



  </body>
</html>
