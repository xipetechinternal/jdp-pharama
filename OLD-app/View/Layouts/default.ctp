<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>CakePHP Authentication and ACL Management Plugin</title>
    <meta name="description" content="CakePHP Authentication and ACL Management Plugin">
    <meta name="author" content="vu khanh truong">
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $title_for_layout; ?>
	</title>
    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
   <!-- [if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
   <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src="https://www.google.com/jsapi?key=ABQIAAAAa44qXAhHZFTYANZzBZYvahSJNboRFY-KWCF1_jCiST2eg5RhLRSZtibOiJfIYeMGYIUbzDeGeg5hww" type="text/javascript"></script>
    <script type="text/javascript">
        google.load("jquery", "1.7.1");
    </script>
   	<?php echo $this->Html->script(array('carousel'));?>
	
    <!-- Le styles -->
    <?php echo $this->Html->css('twitter/bootstrap.min');?>
    <?php echo $this->Html->script(array(
        'twitter/bootstrap-alerts',
        'twitter/bootstrap-dropdown'
    ));?>
    <style type="text/css">
      /* Override some defaults */
      html, body {
        background-color: #eee;
      }
      body {
        padding-top: 40px; /* 40px to make the container go all the way to the bottom of the topbar */
      }
      .container > footer p {
        text-align: center; /* center align it with the container */
      }
      .container {
        width: 820px; /* downsize our container to make the content feel a bit tighter and more cohesive. NOTE: this removes two full columns from the grid, meaning you only go to 14 columns and not 16. */
      }

      /* The white background content wrapper */
      .container > .content {
        background-color: #fff;
        padding: 20px;
        margin: 0 -20px; /* negative indent the amount of the padding to maintain the grid system */
        -webkit-border-radius: 0 0 6px 6px;
           -moz-border-radius: 0 0 6px 6px;
                border-radius: 0 0 6px 6px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.15);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.15);
                box-shadow: 0 1px 2px rgba(0,0,0,.15);
      }

      /* Page header tweaks */
      .page-header {
        background-color: #f5f5f5;
        padding: 20px 20px 10px;
        margin: -20px -20px 20px;
      }

      /* Styles you shouldn't keep as they are for displaying this base example only */
      .content .span16,
      .content .span7,
      .content .span7,
      .content .span10,
      .content .span4 {
        min-height: 500px;
      }
      /* Give a quick and non-cross-browser friendly divider */
      .content .span7,
      .content .span4 {
        margin-left: 0;
        padding-left: 19px;
        border-left: 1px solid #eee;
      }

      .topbar .btn {
        border: 0;
      }
	  
	  .alert-error{
		      background-image: -webkit-linear-gradient(top,#f2dede 0,#e7c3c3 100%);
    background-image: -o-linear-gradient(top,#f2dede 0,#e7c3c3 100%);
    background-image: -webkit-gradient(linear,left top,left bottom,from(#f2dede),to(#e7c3c3));
    background-image: linear-gradient(to bottom,#f2dede 0,#e7c3c3 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fff2dede', endColorstr='#ffe7c3c3', GradientType=0);
    background-repeat: repeat-x;
    border-color: #dca7a7;
	color: #a94442;
    background-color: #f2dede;
    border-color: #ebccd1;
	  }

    </style>

  </head>

  <body>
	 <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide">
      <div class="carousel-inner">
        <div class="item active">
          <img src="../assets/img/examples/slide-01.jpg" alt="">
          <div class="container">
            <div class="carousel-caption">
              <h1>Save on branded Rx's</h1>
              <p class="lead">Join the thousands of pharmacies who are already saving more than $20,000 per year on branded pharmaceuticals</p>
              <a class="btn btn-large btn-primary" href=""style="display:none""></a>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="../assets/img/examples/slide-02.jpg" alt="">
          <div class="container">
            <div class="carousel-caption">
              <h1>Free shipping available!</h1>
              <p class="lead">Free shipping on all orders over $500 in the continental US. Remember that an average bottle of brands is $350, so you'll make the minimum, no problem. Order before 5pm EST and we'll ship the same day.</p>
              <a class="btn btn-large btn-primary" href="" style="display:none;""></a>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="../assets/img/examples/slide-03.jpg" alt="">
          <div class="container">
            <div class="carousel-caption">
              <h1>Sign Up Now</h1>
              <p class="lead">What are you waiting for?  Start saving now.</p>
              <a class="btn btn-large btn-primary" href="../cgi/cgictl.pgm?PN=BSTNU">Click here to sign up</a>
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
    </div><!-- /.carousel -->
    
    <div class="topbar">
      <div class="fill">
        <div class="container">   
                
        <ul class="nav" data-dropdown="dropdown">
        <li class="menu">
        <a class="menu" href="#" class="dropdown-toggle">Posts</a>
        <ul class="menu-dropdown">
        <li><a href="<?php echo $this->Html->url('/posts');?>">Manage Posts</a></li>
        <li class="divider"></li>
        <li><a href="<?php echo $this->Html->url('/posts/add');?>">New Post</a></li>
        </ul>
        </li><li class="menu">
        <a class="menu" href="#" class="dropdown-toggle">User</a>
        <ul class="menu-dropdown">
        <li><a href="<?php echo $this->Html->url('/users');?>">Manage Users</a></li>
        <li class="divider"></li>
        <li><a href="<?php echo $this->Html->url('/users/add');?>">New User</a></li>
        </ul>
        </li>
        <li class="menu">
        <a class="menu" href="#">Group</a>
        <ul class="menu-dropdown">
        <li><a href="<?php echo $this->Html->url('/groups');?>">Manage Group</a></li>
        <li class="divider"></li>
        <li><a href="<?php echo $this->Html->url('/groups/add');?>">New Group</a></li>
        </ul>
        </li>
        </ul>
        
        
        
		<?php
        if($this->Session->check('Auth.User.id')){
        ?>
        <ul class="nav secondary-nav">
        <li class="menu"><a>Hi, <?php echo $this->Session->read('Auth.User.name');?></a></li>
        <li class="menu"><?php echo $this->Html->link('Logout', '/users/logout');?></li>
        </ul>
        <?php
        }
        ?>       
          <!--
          <form action="" class="pull-right">
            <input class="input-small" type="text" placeholder="Username">
            <input class="input-small" type="password" placeholder="Password">
            <button class="btn" type="submit">Sign in</button>
          </form>
          -->
        </div>
      </div>
    </div>

    <div class="container">

      <div class="content">
        <div class="row">
            <div class="span14">
            <?php if(isset($title)){?>
            <div class="page-header">
              <h1><?php echo $title; ?> <small><?php if(isset($description)) echo $description;?></small></h1>
            </div>
            <?php } ?>
            <?php echo $this->Session->flash(); ?>
            <?php echo $this->Session->flash('auth'); ?>

			<?php echo $this->fetch('content'); ?>
            </div>
        </div>
      </div>

      <footer>
        <p>&copy; 2014</p>
      </footer>

    </div> <!-- /container -->
<script>
      !function ($) {
        $(function(){
          // carousel demo
          $('#myCarousel').carousel()
        })
      }(window.jQuery)
    </script>
  </body>
</html>
