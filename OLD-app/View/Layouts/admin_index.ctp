<?php $this->set('title_for_layout', __('Admin Users',true)); ?>
<?php /*$this->Html->addCrumb($this->Html->tag('li',__('Admin Users',true),array('class'=>'active')),false,false);*/ ?>

<h1 class="page-header">Dashboard</h1>

          <div class="row placeholders">
            <div class="col-xs-6 col-sm-3 placeholder">
              <i class="fa fa-users fa-4x"></i>
              <h4>Admin User </h4>
              <span class="text-muted"><?=$inttotal_admin?> Accounts</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <i class="fa fa-user fa-4x"></i>
              <h4>Sales User</h4>
              <span class="text-muted"><?=$inttotal_sales?> Accounts</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <i class="fa fa-users fa-4x"></i>
              <h4>Data Entry Users</h4>
              <span class="text-muted"><?=$inttotal_entry?> Accounts</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <i class="fa fa-user fa-4x"></i>
              <h4>Client User</h4>
              <span class="text-muted"><?=$inttotal_client?> Accounts</span>
            </div>
          </div>
<div class="row-fluid">
<!-- block -->
<h2 class="sub-header">Latest Orders</h2>
<div class="block">
    
    <div class="block-content collapse in">
        <div class="span12">
        
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                        <th><?php echo __('Email');?></th>
                        <th><?php echo __('Group');?></th> 
                        <th><?php echo __('Created');?></th>
                        <th><?php echo __('Actions');?></th>
                    </tr>
                </thead>
                
                <tbody>
                
				<?php 
				$i=0;
				foreach ($users as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				?>
                <tr <?php echo $class;?> >               
                <td><?php echo h($user['User']['email']); ?>&nbsp;</td>
                <td><?php echo h($user['Group']['name']); ?>&nbsp;</td>
                <td class="center"><?php echo h(date("j M Y , g:i A",strtotime($user['User']['created']))); ?>&nbsp; </td>
                <td class="center">
                
                
                
                <div class="btn-group">
                <button class="btn btn-mini btn-info">Info</button>
                <button data-toggle="dropdown" class="btn btn-info btn-mini dropdown-toggle"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                <li><a href="#myModal" data-toggle="modal" onclick="uprofiles.toggle('<?= $this->Html->url('/admin/profiles/uprofiles/').$user['User']['id']; ?>');"><i class="icon-user"></i> Total Profiles <span class="badge"><?= count($user['Profile']);?> </span></a></li>
                <li><a href="#myModal" data-toggle="modal" onclick="share.toggle('<?= $this->Html->url('/admin/profiles/ushare/').$user['User']['id']; ?>');" ><i class="icon-share"></i> Share Profiles <span class="badge"><?= count($user['Post']);?></span></a></li>
                <li><a href="#"><i class="icon-tags"></i> Tagged at Profiles <span class="badge"><?= count($user['UserProfile']);?></span></a></li>
                </ul>
                </div>
             
             
                
				<?php echo $this->Html->link($this->Html->tag('i',false,array('class'=>'icon-eye-open')),'#myModal',array("class"=>"btn btn-mini tooltip-top","data-original-title"=>"View","data-toggle"=>"modal",'escape'=>false,'onclick' => "view.toggle('".$this->Html->url('/admin/users/view/').$user['User']['id']."');")); ?>
                
                <?php 
				if($user['User']['status'])
				echo $this->Html->link($this->Html->tag('i',false,array('class'=>'icon-ok icon-white')),'#',array('class'=>'btn btn-inverse btn-mini tooltip-top','data-original-title'=>'Publish','escape'=>false,'onclick' => "published.toggle('status-".$user['User']['id']."','".$this->Html->url('/users/toggle/').$user['User']['id']."/".intval($user['User']['status'])."');",'id' =>'status-'.$user['User']['id']));
				else
				echo $this->Html->link($this->Html->tag('i',false,array('class'=>'icon-off icon-white')),'#',array('class'=>'btn btn-inverse btn-mini tooltip-top','data-original-title'=>'Publish','escape'=>false,'onclick' => "published.toggle('status-".$user['User']['id']."','".$this->Html->url('/users/toggle/').$user['User']['id']."/".intval($user['User']['status'])."');",'id' =>'status-'.$user['User']['id']));
				
				 ?>
                
                <?php echo $this->Html->link($this->Html->tag('i',false,array('class'=>'icon-pencil icon-white')),array('action' => 'admin_edit', $user['User']['id']),array("class"=>"btn btn-primary btn-mini tooltip-top","data-original-title"=>"Edit",'escape'=>false)); ?>
                
                <?php echo $this->Form->postLink($this->Html->tag('i',false,array('class'=>'icon-remove icon-white')), array('action' => 'delete', $user['User']['id']), array("class"=>"btn btn-danger btn-mini tooltip-top","data-original-title"=>"Delete",'escape'=>false), __('Are you sure you want to delete # %s?', $user['User']['id'])); ?>
                
                
                
                
 				</td>
                </tr>
                <?php endforeach; ?>                
                </tbody>
            </table>
            <?php echo $this->element('admin_pagination');?>
        </div>
    </div>
</div>
<!-- /block -->
</div>





<!--POPUP-->
<div id="myModal" class="modal hide">
<div class="modal-header">
<button data-dismiss="modal" class="close" type="button">&times;</button>
<h3><?php echo __("View"); ?></h3>
</div>
<div class="modal-body">
<p>Modal Example Body</p>
</div>
</div>
<!--POPUP-->

<script type="text/javascript">
    var published = {
        toggle : function(id, url){
            obj = $('#'+id).parent();
            $.ajax({
                url: url,
                type: "POST",
				beforeSend: function(){
				$('.modal-body').html('<p align="center"> <img src="<?php echo $this->Html->url('/img/generated-image.gif');?>"></p>');
				},
                success: function(response){
                    obj.html(response);
					$('.tooltip-top').tooltip({ placement: 'top' });	
					$('.popover-top').popover({placement: 'top', trigger: 'hover'});
                }
            });
        }
    };
	
	var view = {
        toggle : function(url){	
            $.ajax({
                url: url,
                type: "POST",
				beforeSend: function(){
				$('.modal-body').html('<p align="center"> <img src="<?php echo $this->Html->url('/img/generated-image.gif');?>"></p>');
				},
                success: function(response){					
                $('.modal-body').html(response);
				$('.modal-header h3').html("<?php echo __("View"); ?>");
                }
            });
        }
    };
	
	var uprofiles = {
        toggle : function(url){	
            $.ajax({
                url: url,
                type: "POST",
				beforeSend: function(){
				$('.modal-body').html('<p align="center"> <img src="<?php echo $this->Html->url('/img/generated-image.gif');?>"></p>');
				},
                success: function(response){		
                    $('.modal-body').html(response);
					$('.modal-header h3').html("<?php echo __("Total Profiles"); ?>");
                }
            });
        }
    };
	
	var share = {
        toggle : function(url){	
            $.ajax({
                url: url,
                type: "POST",
				beforeSend: function(){
				$('.modal-body').html('<p align="center"> <img src="<?php echo $this->Html->url('/img/generated-image.gif');?>"></p>');
				},
                success: function(response){		
                    $('.modal-body').html(response);
					$('.modal-header h3').html("<?php echo __("Share Profiles"); ?>");
                }
            });
        }
    };
	
</script>