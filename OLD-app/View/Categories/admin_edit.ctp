<?php $this->set('title_for_layout', __('Categories',true)); ?>
<?php $this->Html->addCrumb(__('Categories',true), '/admin/categories/index',array('tag'=>'li'));  ?>
<?php $this->Html->addCrumb($this->Html->tag('li',__('Edit',true),array('class'=>'active')),false,false); ?>
<div class="row-fluid">
<div class="block">
<div class="navbar navbar-inner block-header">
<div class="muted pull-left"><?php echo __('Category'); ?></div>
</div>
<div class="block-content collapse in">
<div class="span12">
<?php echo $this->Form->create('Category',array('class' => 'form-horizontal'));?>
	<fieldset>
		<legend><?php echo __('Edit Category'); ?></legend>
        
    <?php 
	echo $this->Form->input('id');
	echo $this->Form->input('parent_id', array('div'=>'control-group',
					'before'=>'<label class="control-label">'.__('Parent').'</label><div class="controls">',
					'after'=>$this->Form->error('panrent', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
					'error' => array('attributes' => array('style' => 'display:none')),
					'label'=>false, 'class'=>'input-xlarge focused','type' => 'select', 'options' => $categories, 'empty' => true)); 
					
		echo $this->Form->input('name', array('div'=>'control-group',
					'before'=>'<label class="control-label">'.__('Name').'</label><div class="controls">',
					'after'=>$this->Form->error('name', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
					'error' => array('attributes' => array('style' => 'display:none')),
					'label'=>false, 'class'=>'input-xlarge focused'));
		echo $this->Form->input('description', array('div'=>'control-group',
					'before'=>'<label class="control-label">'.__('Description').'</label><div class="controls">',
					'after'=>$this->Form->error('description', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
					'error' => array('attributes' => array('style' => 'display:none')),
					'label'=>false, 'class'=>'input-xxlarge focused'));
	?>
       <div class="form-actions">
            <?php echo $this->Form->submit(__('Submit'), array('class'=>'btn btn-primary', 'div'=>false));?>  
            <?php echo $this->Form->reset(__('Cancel'), array('class'=>'btn', 'div'=>false));?> 
        </div>
	</fieldset>
<?php echo $this->Form->end();?>
</div>
</div>
</div>
</div>