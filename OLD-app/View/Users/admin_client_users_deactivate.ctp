<style type="text/css">
.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td {
    padding: 3px;
}
.table thead > tr > th{text-align: center; vertical-align: middle;font-size: 13px!important;}
.table tbody > tr > td{
	font-size: 13px!important;
}
.table tbody > tr > td .btn{
	margin-top: 2px;
	margin-bottom: 2px;
}

</style>

<div class="col-lg-12">
<h4><a href="<?php echo $this->Html->url('/admin/client_users');?>">Home</a> :: Manage Deactive Client User</h4>
<a href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'client_users')); ?>" class="btn btn-primary btn-mini">&nbsp;All Clients </a>
<div>&nbsp;</div>
            
      
    


</div>

<div class="col-lg-12 text-center">

      <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Manage Client Accounts</h3>
      </div>
      <div class="panel-body">
       <div class="table-responsive">
             
			<?php $membertype=array(1=>'Gold',2=>'Silver',3=>'Bronze'); ?>
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                        <th><?php echo __('Email');?></th>
                        <th><?php echo __('Username');?></th>
                        <th><?php echo __('CUSTOMER #');?></th>
                         <th><?php echo __('Sales Manager');?></th>
                        <th><?php echo __('% Discount');?></th>
                        <th><?php echo __('$ Unpaid ');?></th>
                        <th><?php echo __('Phone');?></th> 
                        <th><?php echo __('Address');?></th> 
                        <th><?php echo __('Credit Limit');?></th>
                        <th><?php echo __('Created');?></th>
                        <th><?php echo __('Actions');?></th>
                    </tr>
                </thead>
                
                <tbody>
                
				<?php 
				$i=0;
				foreach ($users as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				?>
                <tr <?php echo $class;?> >               
                <td><?php echo h($user['User']['email']); ?></td>
                <td><?php echo h($user['User']['username']); ?></td>
                <td><?php echo h($user['User']['CUSTOMER_NUMBER']); ?></td>
                <td><?php echo h($sales[$user['User']['sid']]); ?></td>
                <td>Brand:<?php echo h($user['User']['Brand_discount']); ?><br />
                OTC:<?php echo h($user['User']['OTC_discount']); ?><br />
                Generic:<?php echo h($user['User']['Generic_discount']); ?><br />
                Refrigirated:<?php echo h($user['User']['Refrigirated_discount']); ?><br />
                </td>
                <td>$<?php echo h(number_format(($user['User']['totalUnpaid']-$user['User']['batch_amount']),2)); ?><br />
                 <?php if($user['User']['batch_amount']>0){?><a  class="btn btn-info btn-mini btn-left-margin btn-xs" href="<?php echo $this->Html->url(array_merge(array('controller'=>'users','action'=>'admin_client_order_paid_batch'),$this->params['named'],array(''))).'/'.$user['User']['id'].'/'.$user['User']['batchamount']; ?>"><i class="icon-file-text"></i>B-<?php echo $user['User']['batch_amount'];}?></a>
                 </td>
                <td><?php echo h($user['User']['phone']); ?></td>
                <td><?php echo h($user['User']['address']); ?><br /><?php echo h($user['User']['city']); ?><br /><?php echo h($user['User']['state']); ?><br /><?php echo h($user['User']['zip_code']); ?></td>
                <td><?php echo h($customer_credit); ?></td>
                <td class="center"><?php echo h(date("Y-m-d",strtotime($user['User']['createddt']))); ?> </td>
                <td class="left" style="text-align:left">              
                <?php echo $this->Html->link($this->Html->tag('i','Edit',array('class'=>'icon-pencil icon-white')),array('action' => 'admin_client_users', $user['User']['id']),array("class"=>"btn btn-primary btn-mini btn-xs tooltip-top","data-original-title"=>"Edit",'escape'=>false)); ?>
                
                  <?php echo $this->Html->link($this->Html->tag('i','Order History',array('class'=>'icon-pencil icon-white')),array('action' => 'admin_client_order_history', $user['User']['id']),array("class"=>"btn btn-primary btn-mini btn-xs tooltip-top","data-original-title"=>"Edit",'escape'=>false)); ?>
                <?php echo $this->Html->link($this->Html->tag('i','Login',array('class'=>'icon-pencil icon-white')),array('admin' => false,"controller"=>"customers",'action' => 'client_login_byadmin', $user['User']['id']),array("target"=>"_new","class"=>"btn btn-primary btn-mini btn-xs tooltip-top","data-original-title"=>"Edit",'escape'=>false)); ?>
			  <?php echo $this->Html->link($this->Html->tag('i','Return Credit',array('class'=>'icon-pencil icon-white')),array('action' => 'admin_return_credit', $user['User']['id']),array("class"=>"btn btn-primary btn-mini btn-xs tooltip-top","data-original-title"=>"Edit",'escape'=>false)); ?>
              <?php echo $this->Html->link($this->Html->tag('i','Statement',array('class'=>'icon-pencil icon-white')),array('action' => 'admin_client_order_statement', $user['User']['id']),array("class"=>"btn btn-primary btn-mini btn-xs tooltip-top","data-original-title"=>"Statement",'escape'=>false)); ?>
              
              <?php 
			  if($user['User']['status']){
			  echo $this->Html->link($this->Html->tag('i','Deactivate',array('class'=>'icon-pencil icon-white')),array('action' => 'admin_client_status', $user['User']['id'],0),array("class"=>"btn btn-primary btn-mini btn-xs tooltip-top","data-original-title"=>"Deactivate",'escape'=>false));
			  }else{
				  echo $this->Html->link($this->Html->tag('i','Activate',array('class'=>'icon-pencil icon-white')),array('action' => 'admin_client_status', $user['User']['id'],1),array("class"=>"btn btn-primary btn-mini btn-xs tooltip-top","data-original-title"=>"Activate",'escape'=>false));
			  }?>
              
                <?php echo $this->Form->postLink($this->Html->tag('i','Delete',array('class'=>'icon-remove icon-white')), array('action' => 'admin_client_users_delete', $user['User']['id']), array("class"=>"btn btn-danger btn-mini btn-xs tooltip-top","data-original-title"=>"Delete",'escape'=>false), __('Are you sure you want to delete # %s?', $user['User']['id'])); ?>
                
                
 				</td>
                </tr>
                <?php endforeach; ?>                
                </tbody>
            </table>
            <div class="pagination pagination-right pagination-mini">
	  <ul>
		<?php echo $this->Paginator->prev(''); ?>
		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
		<?php echo $this->Paginator->next(''); ?>
	  </ul>
	</div>
       </div>
      </div>
    </div>


</div>
<script>
$(document).ready(function(){
	$("#UserAdminClientUsersForm").validate({

		rules: {
			"data[User][password]":{
				required: true,
				minlength:5,

				},
			"data[User][password2]":{
				required: true,
				minlength:5,
				equalTo: "#UserPassword"

				}
		},
		messages: {
			"data[User][password]":"Please check this field",
			"data[User][password2]":{equalTo:"make sure both match"}
		},
		errorElement:"span"

		});
		
		 $( "#Datepicker1" ).datepicker({dateFormat: 'yy-mm-dd' }); 
   $( "#UserStateNoExp" ).datepicker({dateFormat: 'yy-mm-dd' }); 
    $( "#UserDenNoExp" ).datepicker({dateFormat: 'yy-mm-dd' }); 
});
</script>