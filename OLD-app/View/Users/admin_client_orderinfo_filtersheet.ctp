<div class="col-lg-12  row">
    <div class="col-lg-6">
        <h4><a href="<?php echo $this->Html->url('/admin/users'); ?>">Home</a> :: Order Information For Customer <?php echo $Orderdetail[0]['User']['fname'] ?></h4>   
    </div>
    <div class="col-lg-6"><a href="javascript:;" onclick="history.go(-1);" class="btn btn-primary">Back</a>
    </div>
</div>
<div class="col-lg-12 text-center">

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Order List</h3>
        </div>
        <div class="panel-body">
            <div class="table-responsive">

                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                    <thead>
                        <tr>
                            <th><?php echo __('Sl no.'); ?></th>
                            <th><?php echo __('Product Name'); ?></th>

                            <th><?php echo __('Quantity'); ?></th> 
                            <th><?php echo __('Original Price'); ?></th>
                            <th><?php echo __('Special Discount'); ?></th>  
                            <th><?php echo __('Price'); ?></th>
                            

                        </tr>
                    </thead>
                    <?php if (empty($Orderdetail)): ?>
                        <tr>
                            <td colspan="7" class="striped-info">No record found.</td>
                        </tr>
                    <?php endif; ?>
                    <tbody>

                        <?php
                        $i = 0;
                        $product_price = 0;
                        $lot_number = array();
                        //print_r($Orderdetail);
                        foreach ($Orderdetail[0]['OrderdetailMany'] as $user):
                            $class = ' class="odd"';
                            if ($i++ % 2 == 0) {
                                $class = ' class="even"';
                            }

                            $product_qauantity = count($user['OrderdetailMany']);

                            $priice = 0;

                            $priice = ($user['price'] * $user['quantity']);

                            $product_price = $product_price + $priice;
                            $orginal_price = ($user['original_price'] > 0) ? ($user['original_price'] * $user['quantity']) : $priice;
                            ?>
                            <tr <?php echo $class; ?> > 

                                <td><?php echo h($user['Product']['itemnumber']); ?> &nbsp;</td>
                                <td><?php echo h($user['Product']['productname']); ?><br /><?php echo h($user['Product']['product_ndc']); ?> &nbsp;</td>



                                <td> <?php echo h($user['quantity']); ?>
                                    &nbsp;</td>
                                <td> $<?php echo h($orginal_price); ?></td>
                                <td> <?php echo h($user['special_discount']); ?>%</td>
                                <td>$<?php echo $priice ?> </td> 
                                  
                            </tr>
                        <?php endforeach; ?>                
                    </tbody>
                    <tfoot>
                        <tr>

                            <th colspan="3" style="text-align:right">Total:</th>
                            <th>$<?php echo $product_price ?></th>
                        </tr>
                    </tfoot>
                </table>
                <div class="pagination pagination-right pagination-mini">
                    <ul>
                        <?php echo $this->Paginator->prev(''); ?>
                        <?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2)); ?>
                        <?php echo $this->Paginator->next(''); ?>
                    </ul>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="gap20"></div>

        </div>
    </div>


</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"> Lot  Exchange</h4>
            </div>
            <div class="modal-body">
                <div class="something"> </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



<script>
    $(function () {

        $('.push').click(function () {
            var essay_id = $(this).attr('id');


            $.ajax({
                type: 'post',
                url: "<?php echo $this->webroot . 'admin/' . $this->params["controller"]; ?>/lot_exchange",
                data: 'order_id=' + essay_id + '&userid=' +<?php echo $Orderdetailid ?>,
                // in php you should use $_POST['post_id'] to get this value 
                success: function (r)
                {
                    // now you can show output in your modal 
                    $('#mymodal').show();  // put your modal id 
                    $('.something').show().html(r);
                }
            });


            $('#myModal').on('hidden', function () {
                $(this).removeData('modal');
            });

        });

    });
</script>