<div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Secure Admin Login</h3>
      </div>
      <div class="panel-body">
             <?php echo $this->Form->create('User', array('action' => 'admin_login','class'=>'form-signin')); ?>
    
               <div class="form-group">
              <?php echo $this->Form->input('username', array('div'=>'control-group',
    'before'=>'<div class="control-label">',
    'after'=>$this->Form->error('username', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
    'error' => array('attributes' => array('style' => 'display:none')),
    'label'=>false, 'class'=>'input-block-level','placeholder'=>'Username')); ?> 
            </div>
            <div class="form-group">
            <?php echo $this->Form->input('password', array('div'=>'control-group',
    'before'=>'<div class="control-label">',
    'after'=>$this->Form->error('password', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
    'error' => array('attributes' => array('style' => 'display:none')),
    'label'=>false, 'class'=>'input-block-level','placeholder'=>'Password')); ?> <!-- <input type="password" placeholder="Password" name="password" id="password" class="form-control">-->
            </div>
         
          
             <?php echo $this->Form->submit(__('Submit'), array('class'=>'btn btn-small btn-primary', 'div'=>false));?>
    <?php echo $this->Form->end(); ?>
          <?php echo $this->Session->flash();  echo $this->Session->flash('auth'); ?>
      </div>
    </div>