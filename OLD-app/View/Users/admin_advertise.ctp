<style>
.navbar{
	z-index:0;
}
</style>
<div class="col-lg-10  main">
         <h4><a href="dashboard.php">Home</a> ::   Advertise</h4>
         </div>
<div class="clearfix"></div>
 <div class="gap20"></div>
          




<div class="table-responsive">
    <h3>New Adds --</h3>          
 
 	
    <div class="panel panel-primary">
      
      <div class="panel-heading">
        <h3 class="panel-title">Products</h3>
      </div>
      <div class="panel-body" style="margin:15px">

    
           <?php echo $this->Form->create('Advertise',array('url'=>array('action'=>'admin_advertise','controller'=>'users'),'enctype'=>"multipart/form-data", 'class'=>'form-horizontal login-from')); ?>

           <div class="form-group">
              <?php echo $this->Form->input('user_ids',array('options' =>$Users,
        'label' => 'Customers',
		'multiple' => true,
		'required'=>'required',
        'class'=>'form-control'));?>
            </div>
            
            <div class="form-group">
              <?php echo $this->Form->input('advertise',array('placeholder' => 'Advertise',
        'label' => 'Advertise',
		'type'=>'textarea',
		'required'=>'required',
        'class'=>'form-control'));?> 
            </div>
            
             <div class="form-group">
              <?php echo $this->Form->input('advertise_img',array('placeholder' => 'Advertise image',
        'label' => 'Advertise',
		'type'=>'file',		
        'class'=>'form-control'));?> 
            </div>
            
             <div class="form-group">
              <?php echo $this->Form->input('status',array('placeholder' => 'Advertise',
        'label' => 'Enable',
		'type'=>'checkbox',
		
        'class'=>''));?> 
            </div>
           
            
            
            
            <div class="gap20"></div>

<div class="col-md-12 form-group">&nbsp;</div>
 <div class="form-actions">
 
  <?php echo $this->Form->input('id',array('type'=>'hidden'));?>
 <?php echo $this->Form->input('created',array('type'=>'hidden','value'=>date("Y-m-d H:i:s")));?>
<?php echo $this->Form->submit(__('Submit'), array('class'=>'btn btn-primary', 'div'=>false));?>&nbsp;
</div>

</fieldset>
<?php echo $this->Form->end();?>
      </div>
    </div>
           
            
    <div class="gap20"></div>
     
    <h3>Advertise--</h3>
                
<table id="productTable" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                         <th><?php echo __('Advertise');?></th>
                        <th><?php echo __('Customers');?></th>
                         <th><?php echo __('Status');?></th>
                       <th><?php echo __('Action');?></th>
                            </tr>
                        </thead>
                        <tbody>                                    
                       <?php if(empty($Advertise)): ?>
		<tr>
			<td colspan="3" class="striped-info">No record found.</td>
		</tr>
		<?php endif;  ?>
        
				<?php 
				$i=0;
				
						
				foreach ($Advertise as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				
				$user_ids=explode(",",$user['Advertise']['user_ids']);
				
				?>
                <tr <?php echo $class;?> >               
               
                <td><?php if($user['Advertise']['image']!=''){
					echo $this->Html->image('advertise/'.$user['Advertise']['image'],array('height'=>100,'width'=>100));}?>
				<?php echo h($user['Advertise']['advertise']); ?></td>
                 <td><?php foreach($user_ids as $uid){
					 echo $Users[$uid].',';
				 }?></td>
                <td><?php echo $user['Advertise']['status']; ?></td>
                 <td>  <a href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'advertise', $user['Advertise']['id'])); ?>" class="btn btn-success btn-mini"><i class="glyphicon glyphicon-edit"></i>&nbsp;Edit</a>
                 <?php
                                    echo $this->Form->postLink($this->Html->tag('i', 'Delete', array('class' => 'icon-remove icon-white')), array('action' => 'admin_advertise_delete', $user['Advertise']['id']), array("class" => "btn btn-danger btn-mini tooltip-top", "data-original-title" => "Delete", 'escape' => false), __('Are you sure you want to Delete ?'));
                                    ?></td>
                </tr>
                <?php endforeach; ?>
</tbody>
<tfoot>
<tr>
          <td colspan="9" align="right">
          	
          </td>
            
          </tr>
          </tfoot>
</table>

            <div class="pagination pagination-right pagination-mini">
	  <ul>
		<?php echo $this->Paginator->prev(''); ?>
		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
		<?php echo $this->Paginator->next(''); ?>
	  </ul>
	</div>
       </div>
<div class="gap-20"></div>
		
      
