<div class="block-content collapse in">
<div class="span12">                            
<table class="table table-bordered">
	<tr>
		<th><?php echo __('Title'); ?></th>
		<td>
			<?php echo h($template['EmailTemplate']['title']); ?>
			&nbsp;
		</td>
	</tr>
	<tr>
		<th><?php echo __('Email Body'); ?></th>
		<td>
			<?php echo h($template['EmailTemplateDescription']['content']); ?>
			&nbsp;
		</td>
	</tr>
    <tr>
		<th><?php echo __('Subject'); ?></th>
		<td>
			<?php echo h($template['EmailTemplateDescription']['subject']); ?>
			&nbsp;
		</td>
	</tr>
	<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($template['EmailTemplate']['created']); ?>
			&nbsp;
		</td>
	</tr>
	<tr>
		<th><?php echo __('Action'); ?></th>
		<td>
			<?php echo $this->Html->link('Edit', array('action'=>'edit', $template['EmailTemplateDescription']['id']));?>
			&nbsp;
		</td>
	</tr>
	</table>
</div>
</div>
