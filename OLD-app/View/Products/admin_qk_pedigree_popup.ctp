<?php echo $this->Html->script(array('jQuery.print'));?>
<div class="row">
<div class="col-xs-12 text-right">
  
  <a class="btn btn-primary btn-mini tooltip-top"  href="<?php echo $this->Html->url('/admin/products/qk_pedigree');?>" >Back </a>
  </div>
 </div>
  <br />
<div class="container">
    <div class="row row-centered">
    	
        <div class="col-xs-12 col-centered">

 <button style="padding:10px" type="button" class="close btn btn-primary" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="panel panel-primary" id="print_div">
      <div class="panel-heading">
        <h3 class="panel-title">Lot Details</h3>
      </div>
      <div class="panel-body">
       <div class="table-responsive">
             

            <table  style="width:100%" border="1" cellpadding="0" cellspacing="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                     <th align="center"><?php echo __('Ndc');?></th>
                       <th align="center"><?php echo __('Invoice Number #');?></th>
                        <th align="center"><?php echo __('dis');?></th>
                        <th align="center"><?php echo __('lot');?></th>
                        <th align="center"><?php echo __('Exp');?></th>
                        <th align="center"><?php echo __('Rec date');?></th> 
                       <th align="center"><?php echo __('Quantity');?></th> 
                        <th align="center"><?php echo __('Manufacture');?></th> 
                        <th><?php echo __('Actions');?></th> 
                    </tr>
                </thead>                
                <tbody>
               
						<?php
						$i=0;
							foreach ($dhtml as $Orderdetail):				
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				?>
                <?PHP $pedigree=$Orderdetail['TempPedigree']['INVOICE_T1'];
					  $pedigree=json_decode($pedigree);
					  
					  ?>
                <tr <?php echo $class;?> > 
						 <td align="center"><?php echo $product_ndec=($Orderdetail['TempPedigree']['NDC']);?></td>
                         <td align="center"><?php echo $InvoiceNumber=$Orderdetail['TempPedigree']['Invoice'];?></td>               
						 <td align="center"><?php echo $InvoiceNumber=$Orderdetail['TempPedigree']['Description'];?></td>
						 <td align="center"><?php echo $InvoiceNumber=$Orderdetail['TempPedigree']['LOT'];?></td>              
						 <td align="center"><?php echo $pedigree[0]->Expiration;?></td>
						 <td align="center"><?php echo date('m-d-Y');?></td>
                         <td align="center"><?php echo $InvoiceNumber=$Orderdetail['TempPedigree']['Qty'];?></td>  
                         <td align="center"><?php echo $pedigree[0]->Manufacturer;?></td>
						<td> <?php  echo $this->Html->link($this->Html->tag('i','Pedigree',array('class'=>'icon-pencil icon-white')),array('action' => 'admin_pending_qk_pedigree', $Orderdetail['TempPedigree']['id']),array("class"=>"btn btn-primary btn-mini btn-left-margin","data-original-title"=>"Edit",'escape'=>false));?></td>
						</tr>
                       <?php endforeach; ?>            
                </tbody>               
            </table>
            <div class="pagination pagination-right pagination-mini">
	  <ul>
		<?php echo $this->Paginator->prev(''); ?>
		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
		<?php echo $this->Paginator->next(''); ?>
	  </ul>
	</div>
   
       </div>
      </div>
    </div>


</div>
       
    </div>
</div>
 <div class="row">
  <div class="col-xs-9">
  <button class="btn btn-primary btn-mini tooltip-top" onclick="history.go(-1);">Back </button>
  </div>
  <div class="col-xs-3">
  
          <button type="button" class="btn btn-primary print" data-dismiss="modal">Print</button> <br /><br />
       

   </div>
  </div>
  <script>
	$('.print').click(function(){
		$('.print').hide();
     $("#print_div").print();
	 $('.print').show();
});</script>
<style>
.row-centered {
    text-align:center;
}
.col-centered {
    display:inline-block;
    float:none;
    /* reset the text-align */
    text-align:left;
    /* inline-block space fix */
    margin-right:-4px;
}
.col-fixed {
    /* custom width */
    width:320px;
}
.col-min {
    /* custom min width */
    min-width:320px;
}
.col-max {
    /* custom max width */
    max-width:320px;
}
</style>

