<div class="col-lg-10  main">
         <h4><a href="dashboard.php">Home</a> :: QK EDI Pending  Pedigree </h4>
         </div>
<div class="clearfix"></div>
 <div class="gap20"></div>
         


<div class="clearfix"></div>
<div class="col-lg-12 text-right" style="padding:20px">

         &nbsp;&nbsp;<a class="btn btn-primary btn-mini btn-left-margin" href="<?php echo $this->Html->url(array_merge(array('controller'=>'products','action'=>'parseQkEdi'),array(''))); ?>"><i class="icon-file-text"></i> Refresh QK EDI Pedigree</a> &nbsp;&nbsp;</div>

			<div class="table-responsive">
              
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                    	
                     
                       
                          <th><?php echo __('Invoice Number');?></th>
                          <th><?php echo __('Recived Date');?></th>                
                        <th><?php echo __('Actions');?></th> 
                       
                       
                    </tr>
                </thead>
                 <?php if(empty($order_list)): ?>
		<tr>
			<td colspan="12" class="striped-info">No record found.</td>
		</tr>
		<?php endif;  ?>
                <tbody>
                
				<?php 
				
				$i=0;
				
		foreach ($order_list as $Orderdetail):				
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				$other_details=json_decode($Orderdetail['TempPedigreesEDI']['Other_details'],true);
				?>
                <tr <?php echo $class;?> > 
              
                 
                 <td><?php echo h($Orderdetail['TempPedigreesEDI']['Invoice']); ?></td> 
                 <td><?php echo h(date("m-d-Y",strtotime($other_details['RecivedDate']))); ?></td>                                     
               <td> <?php  echo $this->Html->link($this->Html->tag('i','View Item',array('class'=>'icon-pencil icon-white')),array('action' => 'admin_qkedi_pedigree_popup', $Orderdetail['TempPedigreesEDI']['Invoice']),array("class"=>"btn btn-primary btn-mini btn-left-margin","data-original-title"=>"Edit",'escape'=>false));?>&nbsp;&nbsp;&nbsp;&nbsp;
               
               <?php echo $this->Form->postLink($this->Html->tag('i','Upload',array('class'=>'icon-remove icon-white')), array('action' => 'admin_qkedi_lot_add', $Orderdetail['TempPedigreesEDI']['Invoice']), array("class"=>"btn btn-primary btn-mini tooltip-top","data-original-title"=>"Add Lot",'escape'=>false), __('Are you sure you want to Upload # %s?',$Orderdetail['TempPedigreesEDI']['Invoice'])); ?>
			 &nbsp;&nbsp;&nbsp;&nbsp;  <?php 
			   echo $this->Form->postLink($this->Html->tag('i','Delete',array('class'=>'icon-remove icon-white')), array('action' => 'admin_qkedi_lot_delete', $Orderdetail['TempPedigreesEDI']['Invoice']), array("class"=>"btn btn-danger btn-mini tooltip-top","data-original-title"=>"Delete",'escape'=>false), __('Are you sure you want to Delete # %s?', $Orderdetail['TempPedigreesEDI']['Invoice']));
			   ?>
                
                
               
               </td>
              </tr>
               
                <?php endforeach; ?>
                 
                </tbody>
                <tfoot>
          
        </tfoot>
            </table>
             <div class="pagination pagination-right pagination-mini">
	  <ul>
		<?php echo $this->Paginator->prev(''); ?>
		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
		<?php echo $this->Paginator->next(''); ?>
	  </ul>
	</div>
       </div>



   <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
      </div>
      </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->   
      
      <script>
$(document).ready(function(){
	$("#ProductInfoAdminReportMonthlyForm").validate();
	//$( "#ProductInfoSelfrmdate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
   // $( "#ProductInfoSeltodate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
	
	
    $( "#ProductInfoSelfrmdate, #ProductInfoSeltodate" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
       dateFormat: 'yy-mm-dd' ,
        onSelect: function( selectedDate ) {
            if(this.id == 'ProductInfoSelfrmdate'){
              var dateMin = $('#ProductInfoSelfrmdate').datepicker("getDate");
              var rMin = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 1); // Min Date = Selected + 1d
              var rMax = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 365); // Max Date = Selected + 31d
              $('#ProductInfoSeltodate').datepicker("option","minDate",rMin);
              $('#ProductInfoSeltodate').datepicker("option","maxDate",rMax);                    
            }

        }
    });

});
</script>
<style>
.navbar-inner{
	z-index:0;
}
.ui-datepicker{
	z-index:100000 !important;
}
</style>