

      
 
      <div class="clearfix"></div>
<div class="row">
  <div class="col-lg-12"> 
  <?php echo $this->Session->flash();  ?>
    <ol class="breadcrumb">
      <li><a href="<?php echo $this->Html->url('/customers/index');?>">Home</a></li>
      <li class="active">Order Statement</li>
    </ol>
    
     
      
        <div class="clearfix"></div>
      
      <div class="col-lg-12 text-right"><input action="action" class="btn btn-success" value="Back" onclick="history.go(-1);" align="right" />
</div>

      <div class="clearfix"></div>
      <div class="gap20"></div>
    
    
    
    
    
  </div>
  
  
      <div class="col-lg-12 text-center">
        <div class="panel panel-primary">
        
        
        
        
          <div class="panel-heading">
            <h2 class="panel-title">Order Statement</h2>
          </div>
                      <div class="clearfix"></div>
             <div class="clearfix"></div>
             <h3>Statement List</h3>  
             <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                     
                     
                        <th><?php echo __('Statement Number');?></th>                        
                        <th><?php echo __('Date');?></th>
                        <th><?php echo __('$');?></th>
                        <th><?php echo __('Action');?></th>                  
                       
                    </tr>
                </thead>
                
                <tbody>
                <?php if(empty($StatementList)): ?>
		<tr>
			<td colspan="5" class="striped-info">No record found.</td>
		</tr>
		<?php endif;  ?>
        
				<?php 
				$i=0;
				
						
				foreach ($StatementList as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				
				?>
                <tr <?php echo $class;?> >               
               
                <td><?php echo h($user['OrderStatement']['statement']); ?></td>
                 <td><?php echo date("m-d-Y",strtotime($user['OrderStatement']['created'])); ?></td>
                
               <td><?php echo $starray[$user['OrderStatement']['statement']]; ?></td>
                <td><?php echo $this->Html->link('Statement',array('action' => 'order_statement_invoice',$user['OrderStatement']['statement']),array("class"=>"","data-original-title"=>"Edit",'escape'=>false)); ?>  &nbsp;
                <?php echo $user['OrderStatement']['status']?>
               </td>
               
             
                </tr>
                <?php endforeach; ?>                
                </tbody>
                <tfoot>
          
        </tfoot>
            </table>
            
        </div>
          
            

            
          </div>
        </div>
     
      </div>
      </div>
    
</div>
