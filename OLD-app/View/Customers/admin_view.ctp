<?php 
$member_type=array(0=>'None','1'=>'Gold','2'=>'Silver','3'=>'Bronze');


 ?>
<div class="block-content collapse in">
  <div class="span12">
    <table class="table table-bordered">
      <tbody>
        <tr>
          <th><?php echo __('Id'); ?></th>
          <td><?php echo h($user['User']['id']); ?></td>
        </tr>
        <tr>
          <th><?php echo __('Email'); ?></th>
          <td><?php echo h($user['User']['email']); ?></td>
        </tr>
        <tr>
          <th><?php echo __('Username'); ?></th>
          <td><?php echo h($user['User']['username']); ?></td>
        </tr>
        <tr>
          <th><?php echo __('Name'); ?></th>
          <td><?php echo h($user['User']['name']); ?></td>
        </tr>
        <tr>
          <th><?php echo __('Contact Name'); ?></th>
          <td><?php echo h($user['User']['contact_name']); ?></td>
        </tr>
        <tr>
          <th><?php echo __('Phone'); ?></th>
          <td><?php echo h($user['User']['phone']); ?></td>
        </tr>
        <tr>
          <th><?php echo __('Fax'); ?></th>
          <td><?php echo h($user['User']['fax']); ?></td>
        </tr>
        <tr>
          <th><?php echo __('City'); ?></th>
          <td><?php echo h($user['User']['city']); ?></td>
        </tr>
        <tr>
          <th><?php echo __('State'); ?></th>
          <td><?php echo h($user['User']['state']); ?></td>
        </tr>
        <tr>
          <th><?php echo __('Zip Code'); ?></th>
          <td><?php echo h($user['User']['zip_code']); ?></td>
        </tr>
        <tr>
          <th><?php echo __('Address'); ?></th>
          <td><?php echo h($user['User']['address']); ?></td>
        </tr>
         <tr>
          <th><?php echo __('State No.'); ?></th>
          <td><?php echo h($user['User']['state_no']); ?></td>
        </tr>
         <tr>
          <th><?php echo __('State Exp Date'); ?></th>
          <td><?php echo h(date("j M Y",strtotime($user['User']['state_exp_date']))); ?></td>
        </tr>
        <tr>
          <th><?php echo __('DEN No.'); ?></th>
          <td><?php echo h($user['User']['den_no']); ?></td>
        </tr> 
         <tr>
          <th><?php echo __('DEN Exp Date'); ?></th>
          <td><?php echo h(date("j M Y",strtotime($user['User']['den_exp_date']))); ?></td>
        </tr>
        
        <tr>
          <th><?php echo __('Member Type'); ?></th>
          <td><?php echo h($member_type($user['User']['member_type'])); ?></td>
        </tr>
        
        
        <tr>
          <th><?php echo __('Group'); ?></th>
          <td><?php echo h($user['Group']['name']); ?></td>
        </tr>
        <tr>
          <th><?php echo __('Created'); ?></th>
          <td><?php echo h(date("j M Y , g:i A",strtotime($user['User']['created']))); ?></td>
        </tr>
        <tr>
          <th><?php echo __('Modified'); ?></th>
          <td><?php echo h(date("j M Y , g:i A",strtotime($user['User']['modified']))); ?></td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
