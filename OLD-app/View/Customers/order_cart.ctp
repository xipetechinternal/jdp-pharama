

      
 
      <div class="clearfix"><br /></div>
<div class="row">
  <div class="col-lg-12"> 
  <?php echo $this->Session->flash();  ?>
    <ol class="breadcrumb">
      <li><a href="<?php echo $this->Html->url('/customers/index');?>">Home</a></li>
      <li class="active">My Cart</li>
    </ol>
    
     
    
  </div>
  
  
</div>

 <div class="clearfix"></div>
<div class="col-lg-12 text-center">

      <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Product List</h3>
      </div>
      <div class="panel-body">
       <div class="table-responsive">
             
			
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                                          
                        <th><?php echo __('Product Name');?></th>
                        <th><?php echo __('Price Per Item');?></th> 
                        <th><?php echo __('Quantity');?></th>                         
                        <th><?php echo __('Price');?></th>                        
                        <th><?php echo __('Actions');?></th>
                    </tr>
                </thead>
                
                <tbody>
                
                <?php if(empty($products)): ?>
		<tr>
			<td colspan="7" class="striped-info">No record found.</td>
		</tr>
		<?php endif;  ?>
				<?php 
				$i=0;
				$totalprice=0;
				$totla_quantity=0;
				$arry_orderid=array();
				
				foreach ($products as $user): 
				$arry_orderid=$user['Orderdetail']['tmporderid'];
				
				$stotal=round($user['Orderdetail']['quantity']*$user['Orderdetail']['price'],2);
				$totalprice=$totalprice+$stotal;
				$totla_quantity=$totla_quantity+$user['Orderdetail']['quantity'];
				$tmporderid=$user['Order']['tmporderid'];
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				?>
                <tr <?php echo $class;?> > 
                        
                
               
                <td><?php echo h($user['Orderdetail']['Product']['productname'])?><br>
                	<?php echo h($user['Orderdetail']['Product']['product_ndc']); ?><br>
                    &nbsp;</td>
                <td><?php echo h($user['Orderdetail']['price']); ?>&nbsp;</td>
                <td><?php echo h($user['Orderdetail']['quantity']); ?>&nbsp;</td>
                <td><?php echo h($stotal); ?>&nbsp;</td>                
                <td class="center"> <?php echo $this->Form->postLink($this->Html->tag('i','Remove',array('class'=>'icon-remove icon-white')), array('action' => 'removeorder_delete', $user['Orderdetail']['id']), array("class"=>"btn btn-danger btn-mini tooltip-top","data-original-title"=>"Delete",'escape'=>false), __('Are you sure you want to delete # %s?',$user['Order']['tmporderid'])); ?>

               
               
 				</td>
                </tr>
                <?php endforeach; ?>                
                </tbody>
                <tfoot>
          <tr>
          
            <th colspan="4" style="text-align:right"><?php echo $totla_quantity?></th>
            <th>$<?php echo $totalprice?></th>
          </tr>
        </tfoot>
            </table>
            <div class="pagination pagination-right pagination-mini">
            
            
<div class="col-lg-12 text-center">
<a href="#myModal" rel="chatusr"   class="push btn push btn-success" data-toggle="modal">Confirm & Place Order</a>
	<?php echo $this->Form->end();?> 
<?php echo $this->Form->postLink($this->Html->tag('i','Empty Cart',array('class'=>'icon-remove icon-white')), array('action' => 'order_empty', $tmporderid), array("class"=>"btn btn-danger  tooltip-top","data-original-title"=>"Delete",'escape'=>false), __('Are you sure you want to empty# %s?',$tmporderid)); ?>
</div>

	</div>
       </div>
      </div>
    </div>


</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">PO Number</h4>
      </div>
      <div class="modal-body">
        <div class="something"> <?php echo $this->Form->create('cart',array('url'=>array('action'=>'confirm_cort','controller'=>'Customers'),'type'=>'file', 'class'=>'paraform')); ?>
	 <?php echo $this->Form->input('orderid',array('placeholder' => 'Product NDC',
        'label' => '',
		'value'=>$arry_orderid,
		'type'=>'hidden',		
        'class'=>'form-control'));?> 
<div class="col-lg-12 text-center">


<table width="100%" border="0" cellspacing="0" cellpadding="0" > 
       
 
  <tr>
    <td align="left" valign="top">Please Provide PO Number :   <?php echo $this->Form->input('po_number',array('placeholder' => 'PO Number',
        'label' => '',
		'required'=>'required',		
        'class'=>'form-control'));?><div class="gap20"></div></td>
  </tr>

 <tr>
    <td align="left" valign="top"><?php echo $this->Form->submit(__('Confirm & Place Order'), array('class'=>'btn btn-primary', 'div'=>false));?>&nbsp;</td>
  </tr>
  
  
</table>
<?php echo $this->Form->end();?> </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
