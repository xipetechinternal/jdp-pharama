<?php echo $this->Html->script('https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js', array('inline' => true)); ?>
<?php
echo $this->Html->css('https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css');
?>



<div class="clearfix"><br /></div>

<div class="row">
    <div class="col-lg-12"> 
        <?php echo $this->Session->flash(); ?>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->Html->url('/customers/index'); ?>">Home</a></li>
            <li class="active">Product Type Search</li>
        </ol>
        <?php echo $this->Form->create('OrderHistory', array('url' => array('action' => 'dispense', 'controller' => 'customers'), 'class' => 'paraform')); ?>

        <div class="form-group">
            <div class="col-lg-2">
                <label for="exampleInputEmail1">Select Product:</label>
            </div>
            <div class="col-lg-8">
                <div class="form-group">
                    <div class="col-lg-4">
                        <?php
                        echo $this->Form->input('product_ndc', array('placeholder' => 'Product NDC',
                            'label' => false,
                            'class' => 'form-control'));
                        ?>

                    </div>
                    <div class="col-lg-8">
                        <?php
                       /* echo $this->Form->input('prodid', array('options' => $product_list,
                            'empty' => array('' => '--Select Product Name--'),
                            'label' => false,
                            'required' => 'required',
                            'class' => 'form-control'));*/
							 echo $this->Form->input('prodid1', array('placeholder' => 'Product Name',
                            'label' => false,
							'required' => 'required',
                            'class' => 'form-control'));
							echo $this->Form->input('prodid', array('placeholder' => 'Product Name',
                            'label' => false,
							'type'=>'hidden',
							'required' => 'required',
                            'class' => 'form-control'));

                        ?>

                    </div>           
                </div>
            </div>

        </div>
        <div class="clearfix"></div>
        <div class="gap20"></div>

        <div class="form-group row">
            <div class=" col-lg-2">
                <label for="exampleInputEmail1">Product Size Dispense:</label>
            </div>
            <div class="col-lg-8">
                <div class="form-group">
                    <div class="col-lg-4">
<?php
echo $this->Form->input('product_dispense', array('placeholder' => 'Product Size Dispense',
    'label' => false,
    'required' => 'required',
    'class' => 'form-control'));
?>

                    </div>
                    <div class="col-lg-4">
<?php
echo $this->Form->input('date_time', array('placeholder' => 'Date',
    'label' => false,
    'class' => 'form-control'));
?>   
                    </div>
                    <div class="col-lg-2">
<?php echo $this->Form->submit(__('Add'), array('class' => 'btn btn-warning', 'div' => false)); ?>
                    </div>



                </div>
            </div>

        </div>

<?php echo $this->Form->end(); ?>
        <div class="clearfix"></div>
        <div class="gap20"></div>





    </div>
    
    <?php echo $this->Form->create('OrderHistory',array('type'=>'GET','url'=>array('action'=>'dispense','controller'=>'customers'),'class'=>'paraform')); ?>
   <?php echo $this->Form->input('orderid',array('placeholder' => 'From Date',
        'label' => false,
		'type'=>'hidden',
		'required'=>'required',
        'class'=>'form-control'));?> 
     
      <div class="form-group">
        <div class="col-lg-2">
          <label for="exampleInputEmail1">Select Date:</label>
        </div>
        <div class="col-lg-8">
          <div class="form-group">
            <div class="col-lg-3">
            <?php echo $this->Form->input('selfrmdate',array('placeholder' => 'From Date',
        'label' => false,
		'required'=>'required',
		'value'=>$selfrmdate,
        'class'=>'form-control'));?> 
             
            </div>
            <div class="col-lg-3">
            <?php echo $this->Form->input('seltodate',array('placeholder' => 'To Date',
        'label' => false,
		'value'=>$seltodate,
		'required'=>'required', 
        'class'=>'form-control'));?>
		<?php echo $this->Form->input('uid',array('placeholder' => 'To Date',
        'label' => false,
		'value'=>$uid,
		'type'=>'hidden',
        'class'=>'form-control'));?>
              
            </div>
            <div class="col-lg-3">
            <?php echo $pdnc?>
              <?php echo $this->Form->input('pndc',array('placeholder' => 'NDC',
        'label' => false,
		'value'=>$pndc,		 
        'class'=>'form-control small-width'));?>  
        </div><div class="col-lg-3">&nbsp;&nbsp;
            <?php echo $this->Form->button('<i class="fa fa-search"></i> ', array(
    'type' => 'submit',
    'class' => 'btn btn-default',
    'escape' => false
));?>
              
            </div>
            
            
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
      
           
        <div class="clearfix"></div>
           <?php echo $this->Form->end();?>
      <div class="clearfix"></div>
      <div class="gap20"></div>

    <?PHP  $selfrmdate=isset($selfrmdate)?$selfrmdate:'';
			$seltodate=(isset($seltodate))?$seltodate:'';
			
	?>
    
    <div class="col-lg-12">
        <div class="row">
            <div class="col-sm-5 text-left">
                <a class="btn btn-warning" href="<?php echo $this->Html->url('/customers/dispense_vendor_excel/'.$selfrmdate.'/'.$seltodate.'/'.$pndc); ?>"> Download Excel/CSV</a>&nbsp;
                <a class="btn btn-warning" href="<?php echo $this->Html->url('/customers/dispense_vendor'); ?>">Vendor Purchases</a>
            </div>
            <div class="col-sm-7 text-right">
                <span id="fromid"></span>
                <span id="toid"></span>
            </div>
        </div>
    </div>
    
    <style type="text/css">
        #fromid, #toid{
            width: 100%;
            max-width: 160px;
            display: inline-block;
            vertical-align: middle;
        }
        
        .dataTables_filter{ display: none;}
        .paginate_enabled_previous:hover, .paginate_enabled_next:hover,
        .paginate_disabled_previous:hover, .paginate_disabled_next:hover{
             text-decoration: none;
        }
        
        .paginate_enabled_previous, .paginate_enabled_next{
            background: #EB7228;
            text-decoration: none;
            min-width: 80px;
            color: #333;
            display: inline-block;
            padding: 3px 10px 3px 10px;
            cursor: pointer;
            margin-right: 5px;
            margin-left: 5px;
            margin-top: 10px;
        }
        
        .paginate_disabled_previous, .paginate_disabled_next{
            background: #ccc;
            text-decoration: none;
            min-width: 80px;
            color: #333;
            display: inline-block;
            padding: 3px 10px 3px 10px; 
            cursor: not-allowed;
            margin-left: 5px;
            margin-right: 5px;
            margin-top: 10px;
        }
    </style>


    <div class="col-lg-12 text-center">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Product Inventory</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">

                </div>

                
                <table id="example" class="datatable display table table-striped table-bordered" width="100%" >
                    <thead>
                        <tr>
							<th id="tracntidate"><strong><?php echo __('Transaction Date'); ?></strong></th>
                            <th><strong><?php echo __('Product NDC'); ?></strong></th>
                            <th><strong><?php echo __('Item Number'); ?></strong></th>
                            <th><strong><?php echo __('Product Name'); ?></strong></th>
                            <th><strong><?php echo __('JDP'); ?></strong></th>
                            <th><strong><?php echo __('Other Vendor'); ?></strong></th>
                            <th><strong><?php echo __('Total Quantity'); ?></strong></th>
                            <th><strong><?php echo __('Size'); ?></strong></th>
                            <th><strong><?php echo __('Total Size Purchase'); ?></strong></th>
                            <th><strong><?php echo __('Total Dispense'); ?></strong></th>
                            <th><strong><?php echo __('Total Left over'); ?></strong></th>
                            <th><strong><?php echo __('Date'); ?></strong></th>
                        </tr>
                    </thead>
                        <?php if (empty($users)): ?>
                        <tr>
                            <td colspan="8" class="striped-info">History not available.</td>
                        </tr>
                        <?php endif; ?>

                    <tbody>

                        <?php
                        $i = 0;
                        $product_price = 0;
						function date_compare($a, $b)
						{
							$t1 = strtotime($a['AddDate']);
							$t2 = strtotime($b['AddDate']);
							return $t1 - $t2;
						}    
						//usort($users, 'date_compare');
						
                        foreach ($users as $user):


                            $class = ' class="odd"';
                            if ($i++ % 2 == 0) {
                                $class = ' class="even"';
                            }
                            ?>
                            <tr <?php echo $class; ?> >

								<td><?php echo ($user['AddDate']!='')?date("m-d-Y",strtotime($user['AddDate'])):''; ?> &nbsp;</td>
                                <td><?php echo h($user['Product NDC']); ?> &nbsp;</td>
                                <td><?php echo h($user['Item Number']); ?> &nbsp;</td>


                                <td>  <?php echo h($user['Product Name']); ?>&nbsp;</td>
                                <td>  <?php echo h($user['jdp_quantity']); ?>&nbsp;</td>
                                <td>  <?php echo isset($user['other_vendor_quantity']) ? $this->Html->link($this->Html->tag('i', $user['other_vendor_quantity'], array('class' => 'icon-pencil icon-white ')), array('action' => 'other_vendor_dispense_list', $user['Product NDC'],$selfrmdate,$seltodate), array("class" => "btn btn-success btn-mini btn-left-margin othervendor", "data-original-title" => "Manually", 'escape' => false)) : 0; ?>&nbsp;</td>
                                <td><?php echo h($user['Quantity']); ?> </td>

                                <td><?php echo h($user['Size']); ?> &nbsp;</td>  
                                <td><?php echo h($user['Total Size Purchase']); ?> &nbsp;</td>  
                                <td><?php if ($user['Total Dispense'] != '') {
                            echo $this->Html->link($this->Html->tag('i', $user['Total Dispense'], array('class' => 'icon-pencil icon-white')), array('action' => 'dispense_list', $user['Product NDC'],$selfrmdate,$seltodate), array("class" => "btn btn-success btn-mini btn-left-margin", "data-original-title" => "Manually", 'escape' => false));
                        } ?> &nbsp;</td>
                                <td><?php echo h($user['Total Left over']); ?> &nbsp;</td>
                                <td><?php echo date("m-d-Y",strtotime(h($user['Date']))); ?> &nbsp;</td>

                            </tr>
<?php endforeach; ?>                
                    </tbody>

                </table>

            </div>
            <div class="clearfix"></div>
            <div class="gap20"></div>



        </div>
    </div>
</div>
<?php echo $this->Html->script("https://jquery-datatables-column-filter.googlecode.com/svn/trunk/media/js/jquery.dataTables.js"); ?>
<?php echo $this->Html->script("https://jquery-datatables-column-filter.googlecode.com/svn/trunk/media/js/jquery.dataTables.columnFilter.js"); ?>

<script>
$(document).ready(function(){
	$("#ProductInfoAdminSalesReportPersonForm").validate();
	//$( "#ProductInfoSelfrmdate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
   // $( "#ProductInfoSeltodate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
	
	
    $( "#OrderHistorySelfrmdate, #OrderHistorySeltodate" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
       dateFormat: 'yy-mm-dd' ,
        onSelect: function( selectedDate ) {
            if(this.id == 'OrderHistorySelfrmdate'){
              var dateMin = $('#OrderHistorySelfrmdate').datepicker("getDate");
              var rMin = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 1); // Min Date = Selected + 1d
              var rMax = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 365); // Max Date = Selected + 31d
              $('#OrderHistorySeltodate').datepicker("option","minDate",rMin);
              $('#OrderHistorySeltodate').datepicker("option","maxDate",rMax);                    
            }

        }
    });

});
</script>

<script>
 $(document).ready(function () {
     
  $.datepicker.regional[""].dateFormat = 'mm-dd-yy';
  $.datepicker.regional[""].changeMonth = true;
  $.datepicker.regional[""].changeYear = true;

  $.datepicker.setDefaults($.datepicker.regional['']);
<?php if(!empty($users)){?>
    $('#example').dataTable({            
        "bLengthChange": false,
    }).columnFilter({sPlaceHolder: "head:before",
        aoColumns: [
            { type: "null"},
            { type: "null"},
            { type: "null"},
            { type: "null"},
            { type: "null"},
            { type: "null"},
            { type: "null"},
            { type: "null"},
            { type: "null"},
            { type: "null"},

            {type: "date-range"}
        ]

    });
   
    var $customFrom = $('.filter_date_range').find("#example_range_from_10");
    $('#example_range_from_10').attr({ placeholder: "From : MM-DD-YY"});
    $('#fromid').html($customFrom);

    
    var $customTo = $('.filter_date_range').find("#example_range_to_10");
        $('#example_range_to_10').attr({ placeholder: "To : MM-DD-YY"});
        $('#toid').html($customTo);

    
    $('.filter_date_range').html('Date');
$('#tracntidate').click()
 });
  <?php } ?>
</script> 

<script>

    $(document).ready(function () {
        $("#OrderHistoryDateTime").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
        });
        $("#from").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
        });
        $("#to").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
        });
        $('#OrderHistoryProductNdc').on('blur mouseup', function () {
            var products_jshon =<?php echo json_encode($product_list); ?>;
            $.each(products_jshon, function (i, v) {
               // console.log(i, v);
                if (i == $("#OrderHistoryProductNdc").val()) {

                    $("#OrderHistoryProdid").val(i);
					$("#OrderHistoryProdid1").val(v);
                    return;
                }
            });
        });
		
		$("#OrderHistoryProdid1").autocomplete(
                {
                    source:<?php echo json_encode($product_listid_auto); ?>,
                    select: function (event, ui) {
                        $("#OrderHistoryProductNdc").val(ui.item.value);
                        $("#OrderHistoryProdid").val(ui.item.value);
						$("#OrderHistoryProdid1").val(ui.item.label);
                        return false;
                    }
                });

        $("#OrderHistoryProductNdc").autocomplete(
                {
                    source:<?php echo json_encode($product_list_auto); ?>,
                    select: function (event, ui) {
                        $("#OrderHistoryProductNdc").val(ui.item.label);
                        $("#OrderHistoryProdid").val(ui.item.label);
                        return false;
                    }
                });
		
		$("#OrderHistoryPndc").autocomplete(
                {
                    source:<?php echo json_encode($product_list_auto); ?>,
                    
                });
		

        $("#ProductInfoAdminSalesReportPersonForm").validate();
        //$( "#ProductInfoSelfrmdate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
        // $( "#ProductInfoSeltodate" ).datepicker({dateFormat: 'yy-mm-dd' }); 





    });
</script>
<script>
    $(".othervendor").click(function(){
        
        from = $("#example_range_from_10").val();
        to =   $("#example_range_to_10").val();
        if(to !='' || from !='' ){
           url = $(this).attr("href")
           url = url+"/"+from+"/"+to
           $(this).attr("href",url);
        }
      
    })
    </script>
    
    
    