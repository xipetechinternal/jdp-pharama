<?php /*echo "<pre>";
print_r($products); */?>

<div class="row">

  <div class="col-lg-12">
   <?php echo $this->Session->flash();  ?>
    <ol class="breadcrumb">
      <li><a href="<?php echo $this->Html->url('/customers/index');?>">Home</a></li>
      <li class="active">Manage Products</li>
    </ol>
    
    <ol class="breadcrumb">
      <li > <strong>Categries</strong> : </li>
      <li ><a href="<?php echo $this->Html->url('/pages/brand');?>" > Brand </a></li>
      <li ><a href="<?php echo $this->Html->url('/pages/generic');?>"  style="color:#ED9C28;"> Generic </a</li>
      <li ><a href="<?php echo $this->Html->url('/pages/otc');?>"> OTC </a></li>
    </ol>

    
    <div class="col-lg-12 text-center">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">Product List</h3>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table cellpadding="0" cellspacing="0" border="0" class="text-center datatable table table-striped table-bordered" id="example">
              <thead>
                <tr>
                  <th>Sr.No.</th>
                  <th>Item Number</th>
                  <th>Product NDC</th>
                  <th>Product Info</th>
                  <th>Price</th>
                  <th>Inventory</th>
                  <th>Action</th>
                </tr>
              </thead>
              <?php if($products){ ?>
              <tbody>
                <?php $i=0;
	  foreach($products as $product){ $i++; ?>
                <tr class="odd gradeA">
                  <td><?php echo $i; ?></td>
                  <td><?php echo $product['Product']['item_number']; ?></td>
                  <td><?php echo $product['Product']['product_ndc'];; ?></td>
                  <td class="center"><?php echo $product['Product']['product_name']."<br>"; ?> Category : <?php echo $product['Category']['name']; ?></td>
                  <td class="center"><?php echo $product['Product']['product_price']; ?></td>
                  <?php foreach($product['ProductStock'] as $stock ){ $quantity=$quantity+$stock['no_of_quantity']; } ?>
                  <td class="center"><?php  echo $quantity; ?></td>
                  <td class="center" valign="bottom"><button type="submit" class="btn btn-warning btn-block">Order</button></td>
                </tr>
                 <?php } ?>
                </tbody>
			    <?php  }else{ ?>
                <tfoot>
                <tr>
                <td colspan="7" align="center"> Data not found.</td>
                </tr>
                </tfoot>
                <?php } ?>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
