<?php $this->set('title_for_layout', __('Product Stocks',true)); ?>
<?php $this->Html->addCrumb($this->Html->tag('li',__('Product Stocks',true),array('class'=>'active')),false,false); ?>
<div class="row-fluid">
<!-- block -->
<div class="block">
    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left"><?php echo __('Product Stocks');?></div>
        <div class="pull-right"><?php echo $this->Html->link($this->Html->tag('span',$this->Html->tag('i',false,array('class'=>'icon-plus icon-white')).__(' ADD NEW'),array('class'=>'badge badge-info')),array('action' => 'admin_add'),array("class"=>false,"data-original-title"=>"ADD NEW",'escape'=>false)); ?>
       </div>
    </div>
    <div class="block-content collapse in">
        <div class="span12">
        
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                        <th><?php echo __('Name');?></th>
                        <th><?php echo __('Lot No.');?></th> 
                        <th><?php echo __('Quantity');?></th> 
                        <th><?php echo __('Exp Date');?></th>
                        <th><?php echo __('Created');?></th>
                        <th ><?php echo __('Actions');?></th>
                    </tr>
                </thead>
                
                <tbody>
                
				<?php 
				$i=0;
				foreach ($products as $product): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				?>
                <tr <?php echo $class;?> >               
                <td><?php echo h($product['Product']['product_name']); ?>&nbsp;</td>
                <td><?php echo h($product['ProductStock']['lot_no']); ?>&nbsp;</td>
                <td><?php echo h($product['ProductStock']['no_of_quantity']); ?>&nbsp;</td>
                <td><?php echo h(date("j M Y",strtotime($product['ProductStock']['exp_date']))); ?>&nbsp;</td>
                
                <td class="center"><?php echo h(date("j M Y , g:i A",strtotime($product['ProductStock']['created']))); ?>&nbsp; </td>
                <td class="center">
                
                
                
                <div class="btn-group">
                <button class="btn btn-mini btn-info">Info</button>
                <button data-toggle="dropdown" class="btn btn-info btn-mini dropdown-toggle"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                <li><a href="#myModal" data-toggle="modal" onclick="uprofiles.toggle('');"><i class="icon-user"></i> Clients <span class="badge"></span></a></li>
                
                <li><a href="#"><i class="icon-tags"></i> Orders <span class="badge"></span></a></li>
                </ul>
                </div>
             
             
                
				<?php echo $this->Html->link($this->Html->tag('i',false,array('class'=>'icon-eye-open')),'#myModal',array("class"=>"btn btn-mini tooltip-top","data-original-title"=>"View","data-toggle"=>"modal",'escape'=>false,'onclick' => "view.toggle('".$this->Html->url('/admin/productStocks/view/').$product['ProductStock']['id']."');")); ?>
                
                <?php 
				if($product['ProductStock']['status'])
				echo $this->Html->link($this->Html->tag('i',false,array('class'=>'icon-ok icon-white')),'#',array('class'=>'btn btn-inverse btn-mini tooltip-top','data-original-title'=>'Publish','escape'=>false,'onclick' => "published.toggle('status-".$product['ProductStock']['id']."','".$this->Html->url('/productStocks/toggle/').$product['ProductStock']['id']."/".intval($product['ProductStock']['status'])."');",'id' =>'status-'.$product['ProductStock']['id']));
				else
				echo $this->Html->link($this->Html->tag('i',false,array('class'=>'icon-off icon-white')),'#',array('class'=>'btn btn-inverse btn-mini tooltip-top','data-original-title'=>'Publish','escape'=>false,'onclick' => "published.toggle('status-".$product['ProductStock']['id']."','".$this->Html->url('/productStocks/toggle/').$product['ProductStock']['id']."/".intval($product['ProductStock']['status'])."');",'id' =>'status-'.$product['ProductStock']['id']));
				
				 ?>
                
                <?php echo $this->Html->link($this->Html->tag('i',false,array('class'=>'icon-pencil icon-white')),array('action' => 'admin_edit', $product['ProductStock']['id']),array("class"=>"btn btn-primary btn-mini tooltip-top","data-original-title"=>"Edit",'escape'=>false)); ?>
                
                <?php echo $this->Form->postLink($this->Html->tag('i',false,array('class'=>'icon-remove icon-white')), array('action' => 'delete', $product['ProductStock']['id']), array("class"=>"btn btn-danger btn-mini tooltip-top","data-original-title"=>"Delete",'escape'=>false), __('Are you sure you want to delete # %s?', $product['ProductStock']['id'])); ?>
                
                
                
                
 				</td>
                </tr>
                <?php endforeach; ?>                
                </tbody>
            </table>
            <?php echo $this->element('admin_pagination');?>
        </div>
    </div>
</div>
<!-- /block -->
</div>





<!--POPUP-->
<div id="myModal" class="modal hide">
<div class="modal-header">
<button data-dismiss="modal" class="close" type="button">&times;</button>
<h3><?php echo __("View"); ?></h3>
</div>
<div class="modal-body">
<p>Modal Example Body</p>
</div>
</div>
<!--POPUP-->

<script type="text/javascript">
    var published = {
        toggle : function(id, url){
            obj = $('#'+id).parent();
            $.ajax({
                url: url,
                type: "POST",
				beforeSend: function(){
				$('.modal-body').html('<p align="center"> <img src="<?php echo $this->Html->url('/img/generated-image.gif');?>"></p>');
				},
                success: function(response){
                    obj.html(response);
					$('.tooltip-top').tooltip({ placement: 'top' });	
					$('.popover-top').popover({placement: 'top', trigger: 'hover'});
                }
            });
        }
    };
	
	var view = {
        toggle : function(url){	
            $.ajax({
                url: url,
                type: "POST",
				beforeSend: function(){
				$('.modal-body').html('<p align="center"> <img src="<?php echo $this->Html->url('/img/generated-image.gif');?>"></p>');
				},
                success: function(response){					
                $('.modal-body').html(response);
				$('.modal-header h3').html("<?php echo __("View"); ?>");
                }
            });
        }
    };
	
	var uprofiles = {
        toggle : function(url){	
            $.ajax({
                url: url,
                type: "POST",
				beforeSend: function(){
				$('.modal-body').html('<p align="center"> <img src="<?php echo $this->Html->url('/img/generated-image.gif');?>"></p>');
				},
                success: function(response){		
                    $('.modal-body').html(response);
					$('.modal-header h3').html("<?php echo __("Total Profiles"); ?>");
                }
            });
        }
    };
	
	var share = {
        toggle : function(url){	
            $.ajax({
                url: url,
                type: "POST",
				beforeSend: function(){
				$('.modal-body').html('<p align="center"> <img src="<?php echo $this->Html->url('/img/generated-image.gif');?>"></p>');
				},
                success: function(response){		
                    $('.modal-body').html(response);
					$('.modal-header h3').html("<?php echo __("Share Profiles"); ?>");
                }
            });
        }
    };
	
</script>