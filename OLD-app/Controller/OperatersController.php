<?php
App::uses('AppController', 'Controller');


/**
 * Users Controller
 *
 * @property User $User
 */
class OperatersController extends AppController {
    
   public $uses = array('User','EmailTemplateDescription');
   public $paginate = array('limit' => 10);
    function beforeFilter() {
        parent::beforeFilter();
        $this->layout = "default";
        $this->User->bindModel(array('belongsTo'=>array(
            'Group' => array(
                'className' => 'Group',
                'foreignKey' => 'group_id',
                'dependent'=>true
            )
        )), false);
		
		
    }
	
	
	
	 public function admin_index() {
		$this->layout = "admin_dashboard";
        $this->set('title', __('Operaters'));
        $this->set('description', __('Manage Operaters'));        
        $this->User->recursive = 1;
        $this->set('users', $this->paginate("User",array('NOT'=>array('User.id' => $this->Auth->user('id')),'User.group_id'=>3)));
    }
    /**
     * view method
     *
     * @param string $id
     * @return void
     */
	 
 	 public function admin_view($id = null) {
		$this->layout = "ajax";		
        $this->set('title', __('View'));
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'), 'error');
        }
        $this->set('user', $this->User->read(null, $id));
     }
	 
    /**
     * add method
     *
     * @return void
     */
 
	public function admin_add() {		
		$this->layout = "admin_dashboard";		
        if ($this->request->is('post')) {
            $this->loadModel('User');
			$password=$this->request->data["User"]['password'];
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The Operaters has been saved'), 'admin_success');
				$email = new CakeEmail();
				try {
				$email->config('default');
				} catch (Exception $e) {
				Throw new ConfigureException('Config in email.php not found. ' . $e->getMessage());
				}
				$Templates=$this->EmailTemplateDescription->find('all',array('conditions'=>array('EmailTemplateDescription.email_template_id' => 1)));	
				$TempContent=stripcslashes($Templates[0]['EmailTemplateDescription']['content']);
				$TempContent=str_replace("{email}",$this->request->data["User"]['email'],$TempContent); 
				$TempContent=str_replace("{date}",date("Y-m-d"),$TempContent); 
				$TempContent=str_replace("{name}",$this->request->data["User"]['username'],$TempContent); 
				$TempContent=str_replace("{password}",$password,$TempContent);
				$TempContent=str_replace("../../..",'http://' . env('SERVER_NAME'),$TempContent);
				$data=array('Message'=>$TempContent);
				$email->template('default', 'default');
				$email->emailFormat('both');
				$email->viewVars(array('data' =>$data)) ; 
				$email->from(array('i-care@i-care.mobi' => 'JDPharmaceutical'));                                     
				$email->to(Sanitize::clean($_REQUEST["email"]));
				$email->subject(__d('default', $Templates[0]['EmailTemplateDescription']['subject']));
				$email->send();
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The Operaters could not be saved. Please, try again.'), 'admin_error');
            }
        }
        $groups = $this->User->Group->find('list');
        $this->set(compact('groups'));
    }

    /**
     * edit method
     *
     * @param string $id
     * @return void
     */
    
	  public function admin_edit($id = null) {		
		$this->layout = "admin_dashboard";		
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The Operaters has been saved'), 'admin_success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The Operaters could not be saved. Please, try again.'), 'admin_error');
            }
        } else {
            $this->request->data = $this->User->read(null, $id);
            $this->request->data['User']['password'] = null;
        }
        $groups = $this->User->Group->find('list');
        $this->set(compact('groups'));
    }
	
	

    /**
     * delete method
     *
     * @param string $id
     * @return void
     */


	public function admin_delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->User->delete()) {
            $this->Session->setFlash(__('Operaters deleted'), 'admin_success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Operaters was not deleted'), 'admin_error');
        $this->redirect(array('action' => 'index'));
    }

    /**
     *  Active/Inactive User
     *
     * @param <int> $user_id
     */
    public function toggle($user_id, $status) {
        $this->layout = "ajax";
        $status = ($status) ? 0 : 1;
        $this->set(compact('user_id', 'status'));
        if ($user_id) {
            $data['User'] = array('id'=>$user_id, 'status'=>$status);
            $allowed = $this->User->saveAll($data["User"], array('validate'=>false));           
        } 
    }
}
?>