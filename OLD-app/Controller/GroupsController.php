<?php

App::uses('AppController', 'Controller');

/**
 * Groups Controller
 *
 * @property Group $Group
 */
class GroupsController extends AppController {


	public $paginate =array('limit'=>10);

    function beforeFilter() {
        parent::beforeFilter();        
        $this->layout = "admin_dashboard";
    }    
    /**
     * index method
     *
     * @return void
     */
    public function admin_index() {
        $this->set('title', __('Groups'));
        $this->set('description', __('Manage Groups'));
        $this->Group->recursive = 0;        
        $this->set('groups', $this->paginate());
    }

    /**
     * view method
     *
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->Group->id = $id;
        if (!$this->Group->exists()) {
            throw new NotFoundException(__('Invalid group'));
        }
        $this->set('group', $this->Group->read(null, $id));
    }

    /**
     * add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->Group->create();
            if ($this->Group->save($this->request->data)) {
                $this->Session->setFlash(__('The group has been saved'), 'admin_success');
                $this->redirect(array('action' => 'admin_index'));
            } else {
                $this->Session->setFlash(__('The group could not be saved. Please, try again.'), 'admin_error');
            }
        }
    }

    /**
     * edit method
     *
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->Group->id = $id;
        if (!$this->Group->exists()) {
            throw new NotFoundException(__('Invalid group'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Group->save($this->request->data)) {
                $this->Session->setFlash(__('The group has been saved'), 'admin_success');
                $this->redirect(array('action' => 'admin_index'));
            } else {
                $this->Session->setFlash(__('The group could not be saved. Please, try again.'), 'admin_error');
            }
        } else {
            $this->request->data = $this->Group->read(null, $id);
        }
    }

    /**
     * delete method
     *
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->Group->id = $id;
        if (!$this->Group->exists()) {
            throw new NotFoundException(__('Invalid group'), 'admin_error');
        }
        if ($this->Group->delete()) {
            $this->Session->setFlash(__('Group deleted'), 'admin_success');
            $this->redirect(array('action' => 'admin_index'));
        }
        $this->Session->setFlash(__('Group was not deleted'), 'admin_error');
        $this->redirect(array('action' => 'admin_index'));
    }

}
?>