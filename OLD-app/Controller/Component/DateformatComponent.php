<?php
App::uses('HttpSocket', 'Network/Http');

class DateformatComponent extends Component {
    
     /**
     * Date Formate
     *  
     * @return object
     * @todo Determine what to do if response status is an error
     */
	 
	 public function yearago($timestamp){	
			
				if(!is_numeric($timestamp)){
				$timestamp = strtotime($timestamp);
					if( !is_numeric( $timestamp ) ){
					return "";
					}
				}
				
				//$difference = strtotime(date("Y-m-d H:i(worry)")) - $timestamp;
				$difference = time() - $timestamp;
				
				// Customize in your own language.
				$periods = array( "sec", "min", "hour", "day", "week", "month", "years", "decade" );
				$lengths = array( "60","60","24","7","4.35","12","10");
				
				if ($difference >= 0) {
					// this was in the past
					$ending = "ago";
				}else { 
					// this was in the future
					$difference = -$difference;
					$ending = "ago";
				}
				
				
				for($j=0; $difference>=$lengths[$j] and $j < 7; $j++)
				$difference /= $lengths[$j];
				
				$difference = round($difference);
				if( $difference!= 1){
					// Also change this if needed for an other language
					$periods[$j].= "s";
				}
				$text = "$difference $periods[$j] $ending";
				return $text;
			
		
		 
 }
	 
	
	 
}