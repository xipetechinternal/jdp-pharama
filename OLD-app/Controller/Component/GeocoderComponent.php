<?php
App::uses('HttpSocket', 'Network/Http');

class GeocoderComponent extends Component {
    
    /**
     * Geocodes an address.
     *
     * @param string $address
     * @param array $parameters
     * @return object
     * @todo Determine what to do if response status is an error
     */
	 
	public function destination($lat,$lon, $bearing, $distance,$units="mi") {
		$radius = strcasecmp($units, "km") ? 3963.19 : 6378.137;
		$rLat = deg2rad($lat);
		$rLon = deg2rad($lon);
		$rBearing = deg2rad($bearing);
		$rAngDist = $distance / $radius;	
		$rLatB = asin(sin($rLat) * cos($rAngDist) + 
		cos($rLat) * sin($rAngDist) * cos($rBearing));	
		$rLonB = $rLon + atan2(sin($rBearing) * sin($rAngDist) * cos($rLat), 
		cos($rAngDist) - sin($rLat) * sin($rLatB));	
		return array("lat" => rad2deg($rLatB), "lon" => rad2deg($rLonB));
	}
	
	// calculate bounding box
	public function bound($lat,$lon, $distance,$units="mi") {
		return array("N" => $this->destination($lat,$lon,0, $distance,$units),
		"E" => $this->destination($lat,$lon,  90, $distance,$units),
		"S" => $this->destination($lat,$lon, 180, $distance,$units),
		"W" => $this->destination($lat,$lon, 270, $distance,$units));
	}
		
	public function nearby($latitude,$longitude,$distance) {
			
		$b = $this->bound($latitude,$longitude, $distance,$units="mi");			
		$nearconditions['Profile.longitude BETWEEN ? AND ?']=array($b["S"]["lat"],$b["N"]["lat"]);
		$nearconditions['Profile.latitude BETWEEN ? AND ?']=array($b["W"]["lon"],$b["E"]["lon"]);	
		return $nearconditions;
	
	}
       public function NearByUser($latitude,$longitude,$distance) {
			
		$b = $this->bound($latitude,$longitude, $distance,$units="mi");			
		$nearconditions['User.longitude BETWEEN ? AND ?']=array($b["S"]["lat"],$b["N"]["lat"]);
		$nearconditions['User.latitude BETWEEN ? AND ?']=array($b["W"]["lon"],$b["E"]["lon"]);	
		return $nearconditions;
	
	} 
	 
	 
}