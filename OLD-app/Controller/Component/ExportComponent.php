<?php
App::uses('Component', 'Controller');
class ExportComponent extends Component {

/**
 * The calling Controller
 *
 * @var Controller
 */
	public $controller;

/**
 * Starts up ExportComponent for use in the controller
 *
 * @param Controller $controller A reference to the instantiating controller object
 * @return void
 */
	public function startup(Controller $controller) {
		$this->controller = $controller;
	}

	function exportCsv($data, $fileName = '',$clinete_info=array(), $maxExecutionSeconds = null, $delimiter = ',', $enclosure = '"') {

		$this->controller->autoRender = false;

		// Flatten each row of the data array
		$flatData = array();
		foreach($data as $numericKey => $row){
			$flatRow = array();
			$this->flattenArray($row, $flatRow);
			$flatData[$numericKey] = $flatRow;
		}

		$headerRow = $this->getKeysForHeaderRow($flatData);
		$flatData = $this->mapAllRowsToHeaderRow($headerRow, $flatData);

		if(!empty($maxExecutionSeconds)){
			ini_set('max_execution_time', $maxExecutionSeconds); //increase max_execution_time if data set is very large
		}

		if(empty($fileName)){
			$fileName = "export_".date("Y-m-d").".csv";
		}

		$csvFile = fopen('php://output', 'w');
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename="'.$fileName.'"');
		//cleint info not blank
		if(!empty($clinete_info)){
			fputcsv($csvFile,   array('JD PHARMACEUTICAL WHOLESALER INC ', '', '','','','',' CUSTOMER NAME :'.$clinete_info['User']['fname'].' '.$clinete_info['User']['lname']), $delimiter, $enclosure);
			fputcsv($csvFile,   array('6034 SAN FERNANDO RD. ', '', '','','','',' CUSTOMER ADDRESS :'.$clinete_info['User']['address']), $delimiter, $enclosure);
			fputcsv($csvFile,   array('GLENDALE CA 91202 ', '', '','','','',$clinete_info['User']['city'].' '.$clinete_info['User']['state'].' '.$clinete_info['User']['zip_code']), $delimiter, $enclosure);
			fputcsv($csvFile,   array('PH : (818)956-0578    FAX : (818) 956-0721', '', '','','','','PH :  '.$clinete_info['User']['phone'].' FAX : '.$clinete_info['User']['fax']), $delimiter, $enclosure);		
			fputcsv($csvFile,   array('DEA # RJ0450104', '', '','','','','DEA # '.$clinete_info['User']['den_no']), $delimiter, $enclosure);
			fputcsv($csvFile,   array(''), $delimiter, $enclosure);
			fputcsv($csvFile,   array(''), $delimiter, $enclosure);
		}
		fputcsv($csvFile,$headerRow, $delimiter, $enclosure);
		foreach ($flatData as $key => $value) {
			fputcsv($csvFile, $value, $delimiter, $enclosure);
		}
		fclose($csvFile);
	}

	public function flattenArray($array, &$flatArray, $parentKeys = ''){
		foreach($array as $key => $value){
			$chainedKey = ($parentKeys !== '')? $parentKeys.'.'.$key : $key;
			if(is_array($value)){
				$this->flattenArray($value, $flatArray, $chainedKey);
			} else {
				$flatArray[$chainedKey] = $value;
			}
		}
	}

	public function getKeysForHeaderRow($data){
		$headerRow = array();
		foreach($data as $key => $value){
			foreach($value as $fieldName => $fieldValue){
				if(array_search($fieldName, $headerRow) === false){
					$headerRow[] = $fieldName;
				}
			}
		}

		return $headerRow;
	}

	public function mapAllRowsToHeaderRow($headerRow, $data){
		$newData = array();
		foreach($data as $intKey => $rowArray){
			foreach($headerRow as $headerKey => $columnName){
				if(!isset($rowArray[$columnName])){
					//$rowArray[$columnName] = '';
					$newData[$intKey][$columnName] = '';
				} else {
					$newData[$intKey][$columnName] = $rowArray[$columnName];
				}
			}
		}

		return $newData;
	}



}