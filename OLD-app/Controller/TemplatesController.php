<?php
App::uses('AppController', 'Controller');

 /**
 * EmailTemplateDescriptions Controller
 *
 * @property EmailTemplateDescription $EmailTemplateDescription
 */
 
class TemplatesController extends AppController {

	public $uses = array('EmailTemplateDescription');
    public function beforeFilter() {
        parent::beforeFilter();       	     
        $this->layout = "admin_dashboard";   
		$this->EmailTemplateDescription->bindModel(array('belongsTo'=>array(
            'EmailTemplate' => array(
                'className' => 'EmailTemplate',
                'foreignKey' => 'email_template_id',
                'dependent'=>true
            )
        )), false); 
	}
/**
 * home method
 *
 * @return void
 */
	public function admin_index() {
        $this->set('title', __('EmailTemplateDescriptions')); 
		$this->paginate = array('order'=>array('EmailTemplateDescription.id' => 'DESC'));
		$this->EmailTemplateDescription->recursive = 0;
		$this->set('templates', $this->paginate());
	}
	


/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		 $this->layout = "ajax";
		$this->EmailTemplateDescription->id = $id;		
		if (!$this->EmailTemplateDescription->exists()) {
			throw new NotFoundException(__('Invalid template'));
		}
		
		$this->set('template', $this->EmailTemplateDescription->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {			
		if ($this->request->is('post')) {
			$this->loadModel('EmailTemplateDescription');			
			$this->EmailTemplateDescription->create();			
			if ($this->EmailTemplateDescription->save($this->request->data,array('validate' => 'only'))) {
				$this->Session->setFlash(__('The template has been saved'), 'admin_success');
				$this->redirect(array('action' => 'admin_index'));
			} else {
				$this->Session->setFlash(__('The template could not be saved. Please, try again.'), 'admin_error');
			}
		}
	
        $templates = $this->EmailTemplateDescription->EmailTemplate->find('list');
        $this->set(compact('templates'));
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->EmailTemplateDescription->id = $id;
		if (!$this->EmailTemplateDescription->exists()) {
			throw new NotFoundException(__('Invalid template'));
		}
		
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->loadModel('EmailTemplateDescription');
			if ($this->EmailTemplateDescription->save($this->request->data,array('validate' => 'only'))) {
				$this->Session->setFlash(__('The template has been saved'), 'admin_success');
				$this->redirect(array('action' => 'admin_index'));
			} else {
				$this->Session->setFlash(__('The template could not be saved. Please, try again.'), 'admin_error');
			}
		} else {
			$this->request->data = $this->EmailTemplateDescription->read(null, $id);
		}
		$templates = $this->EmailTemplateDescription->EmailTemplate->find('list');
        $this->set(compact('templates'));
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		
		$this->EmailTemplateDescription->id = $id;
		if (!$this->EmailTemplateDescription->exists()) {
			throw new NotFoundException(__('Invalid template'), 'admin_error');
		}
		if ($this->EmailTemplateDescription->delete()) {
			$this->Session->setFlash(__('EmailTemplateDescription deleted'), 'admin_error');
			$this->redirect(array('action' => 'admin_index'));
		}
		$this->Session->setFlash(__('EmailTemplateDescription was not deleted'), 'admin_success');
		$this->redirect(array('action' => 'admin_index'));
	}
	
 	/**
     *  Active/Inactive Templates
     *
     * @param <int> 
     */
    public function toggle($temp_id, $status) {
        $this->layout = "ajax";
        $status = ($status) ? 0 : 1;
        $this->set(compact('temp_id', 'status'));
        if ($temp_id) {
            $data['EmailTemplateDescription'] = array('id'=>$temp_id, 'status'=>$status);
            $allowed = $this->EmailTemplateDescription->saveAll($data["EmailTemplateDescription"], array('validate'=>false));           
        } 
    }	
	
	
}
