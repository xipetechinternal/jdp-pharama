<div class="col-lg-10  main">
         <h4><a href="dashboard.php">Home</a> :: Filler Sheet</h4>
         </div>
<div class="clearfix"></div>
 <div class="gap20"></div>
           <?php echo $this->Form->create('ProductInfo',array('url'=>array('action'=>'admin_filler_sheetpdf','controller'=>'Orders'),'type'=>'get', 'class'=>'form-horizontal login-from')); ?>

<div class="col-lg-10  ">
  <div class="form-group">
  <div class="col-lg-2"><label for="exampleInputEmail1">Select Date:</label></div>
  <div class="col-lg-8"> <div class="form-group">
  <div class="col-lg-4">   <?php echo $this->Form->input('selfrmdate',array('placeholder' => 'From Date',
        'label' => false,
		'required'=>'required',
        'class'=>'form-control'));?> 
   
  </div><div class="col-lg-4"><?php echo $this->Form->input('seltodate',array('placeholder' => 'To Date',
        'label' => false,
		'required'=>'required',
        'class'=>'form-control'));?>  </div>
  <div class="col-lg-4"> <?php echo $this->Form->button('<i class="fa fa-search"></i> Send', array(
    'type' => 'submit',
    'class' => 'btn btn-primary',
    'escape' => false
));?></div>
  <div class="clearfix"></div>
  <div class="col-lg-6">
 
  </div>
  
            </div></div>

  </div>
  <?php echo $this->Form->end();?>
  <div class="clearfix"></div>
  <div class="gap20"></div>
</div>





   <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
      </div>
      </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->   
      
      <script>
$(document).ready(function(){
	$("#ProductInfoAdminReportMonthlyForm").validate();
	//$( "#ProductInfoSelfrmdate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
   // $( "#ProductInfoSeltodate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
	
	
    $( "#ProductInfoSelfrmdate, #ProductInfoSeltodate" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
       dateFormat: 'yy-mm-dd' ,
        onSelect: function( selectedDate ) {
            if(this.id == 'ProductInfoSelfrmdate'){
              var dateMin = $('#ProductInfoSelfrmdate').datepicker("getDate");
              var rMin = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 1); // Min Date = Selected + 1d
              var rMax = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 365); // Max Date = Selected + 31d
              $('#ProductInfoSeltodate').datepicker("option","minDate",rMin);
              $('#ProductInfoSeltodate').datepicker("option","maxDate",rMax);                    
            }

        }
    });

});
</script>