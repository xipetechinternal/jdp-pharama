
<div class="row">
  <div class="col-lg-4">
    <div class="panel panel-default" style="margin-top:-100px">
      <div class="panel-body form-bg">
        <?php 
	  echo $this->Session->flash(); 
	   echo $this->Session->flash('auth'); ?>
        <?php echo $this->Form->create('User', array('action' => 'login')); ?>
        <h4 class="orgtxt"><?php echo __('Secure Customer Login'); ?></h4>
        <?php
    echo $this->Form->input('email', array('div'=>'clearfix',
        'before'=>'<label>'.__('Email').'</label><div class="input">',
        'after'=>$this->Form->error('email', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
        'error' => array('attributes' => array('style' => 'display:none')),
        'label'=>false, 'class'=>'form-control','placeholder'=>'Enter username'));
    echo $this->Form->input('password', array('div'=>'clearfix',
        'before'=>'<label>'.__('Password').'</label><div class="input">',
        'after'=>$this->Form->error('password', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
        'error' => array('attributes' => array('style' => 'display:none')),
        'label'=>false, 'class'=>'form-control','placeholder'=>'Enter password'));
    ?>
        <div class="checkbox">
          <label>Remember Me
            <input type="checkbox">
          </label>
        </div>
        <?php echo $this->Form->submit(__('Submit'), array('class'=>'btn btn-warning', 'div'=>false));?> <?php echo $this->Form->reset(__('Cancel'), array('class'=>'btn btn-warning', 'div'=>false));?> <?php echo $this->Form->end(); ?> <a href="forgot_password.php">
        <button type="submit" class="btn btn-default">Forgot your password?</button>
        </a> <a href="forgot_username.php">
        <button type="submit" class="btn btn-default">Forgot your username? </button>
        </a> </div>
    </div>
  </div>
</div>
