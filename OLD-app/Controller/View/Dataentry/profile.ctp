
      <div class="row">   

        <div class="col-md-offset-8 col-lg-4">
<div class="panel panel-default" style="margin-top:-100px">

  <div class="panel-body form-bg text-center ">
<p>Welcome <?php echo ucfirst($this->Session->read('Auth.dataentry.fname')); ?></p>

   <?php echo $this->Html->link('Sign out', array('controller'=>'Dataentry', 'action'=>'logout'),array('class' => 'btn btn-warning btn-block'));?>
  </div>
</div>
        </div>

        </div>
 
      <div class="clearfix"></div>
      <div class="row">   

      <div class="col-lg-12">
        

<ol class="breadcrumb">
  <li> <?php echo $this->Html->link('Home', array('controller'=>'Dataentry', 'action'=>'index'),array('class' => ''));?></li>
  <li class="active">Manage Profile</li>
</ol>


   <?php echo $this->Form->create('User', array(
    'url' => array('controller' => 'Dataentry', 'action' => 'profile')
));?>

   <div class="form-group">
  <div class="col-lg-4"><label for="exampleInputEmail1">First Name:</label></div>
  <div class="col-lg-4"><?php echo $this->Form->input('fname',array('placeholder' => 'First Name',
        'label' => '',
        'class'=>'form-control col-md-4'));?> </div>
  </div>
  <div class="clearfix"></div>
  <div class="gap20"></div>
<div class="form-group">
  <div class="col-lg-4"><label for="exampleInputEmail1">Username:</label></div>
  <div class="col-lg-4"><?php echo $this->Form->input('username',array('placeholder' => 'Username',
        'label' => '',
        'class'=>'form-control col-md-4'));?> </div>
  </div>
  <div class="clearfix"></div>
  <div class="gap20"></div>
  <div style="display:none"  id="passwordid">
  <div class="form-group">
   <div class="col-lg-4"><label for="exampleInputEmail1">Password:</label></div>
  <div class="col-lg-4">   <?php echo $this->Form->input('password',array('placeholder' => 'Password',
        'label' => '',
        'class'=>'form-control col-md-4'));?> </div>
            </div>
    
 <div class="clearfix"></div>
  <div class="gap20"></div>    

   <div class="form-group">
   <div class="col-lg-4"><label for="exampleInputEmail1">Confirm Password:</label></div>
  <div class="col-lg-4"> <?php echo $this->Form->input('password2',array('placeholder' => 'Confirm Password',
        'label' => '',
		'type'=>'password',
		'required'=>'required',
        'class'=>'form-control col-md-4'));?> </div>
            </div>
   </div>
 <div class="clearfix"></div>
 <div class="gap20"></div>
<div class="form-group">
  <div class="col-lg-4"><label for="exampleInputEmail1">Address:</label></div>
  <div class="col-lg-4"> <?php echo $this->Form->input('address',array('placeholder' => 'Address',
        'label' => '',
        'class'=>'form-control col-md-4'));?> </div>
  </div>
  
   <div class="clearfix"></div>
   <div class="gap20"></div>
<div class="form-group">
  <div class="col-lg-4"><label for="exampleInputEmail1">Phone:</label></div>
  <div class="col-lg-4"> <?php echo $this->Form->input('phone',array('placeholder' => 'Phone',
        'label' => '',
        'class'=>'form-control col-md-4'));?> </div>
  </div>

   
  <div class="clearfix"></div>
  <div class="gap20"></div>
<div class="form-group">
  <div class="col-lg-4"><label for="exampleInputEmail1">Email:</label></div>
  <div class="col-lg-4"> <?php echo $this->Form->input('email',array('placeholder' => 'Email',
        'label' => '',
        'class'=>'form-control col-md-4'));?> </div>
  </div>
   <div class="clearfix"></div>
   <div class="gap20"></div>
<div class="col-lg-12" style="text-align:center;">
<?php echo $this->Form->input('id');?>
 
 <?php echo $this->Form->input('status',array('type' => 'hidden','value'=>'1'));?> 
  <?php echo $this->Form->input('active',array('type' => 'hidden','value'=>'Y'));?> 
<?php echo $this->Form->submit(__('Update'), array('class'=>'btn btn-primary', 'div'=>false));?>
  &nbsp;<?php echo $this->Form->reset(__('Change Password'), array('value'=>'Change Password','class'=>'btn btn-warning','onclick'=>"$('#passwordid').show();",'type'=>'button','div'=>false));?>

  </div>
</form>
      </div>

      </div>

     
<style>
#content-area{
	background-image:none;
}
</style>
<script>
$(document).ready(function(){
	$("#UserProfileForm").validate({

		rules: {
			"data[User][password]":{
				required: true,
				minlength:5,

				},
			"data[User][password2]":{
				required: true,
				minlength:5,
				equalTo: "#UserPassword"

				}
		},
		messages: {
			"data[User][password]":"Please check this field",
			"data[User][password2]":{equalTo:"make sure both match"}
		},
		errorElement:"span"

		});
});
</script>