<?php echo $this->Html->script('https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js', array('inline' => true));?>
   <?php
      echo $this->Html->css('https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css');
    ?>
      <div class="row">   

        <div class="col-md-offset-8 col-lg-4">
<div class="panel panel-default" style="margin-top:-100px">

  <div class="panel-body form-bg text-center ">
<p>Welcome <?php echo ucfirst($this->Session->read('Auth.dataentry.fname')); ?></p>

   <?php echo $this->Html->link('Sign out', array('controller'=>'Dataentry', 'action'=>'logout'),array('class' => 'btn btn-warning btn-block'));?>
  </div>
  
</div>
        </div>

        </div>
 
      <div class="clearfix"></div>
      <div class="row">   

      <div class="col-lg-12">
        
<h4><?php echo $this->Html->link('Home', array('controller'=>'Dataentry', 'action'=>'index'),array('class' => ''));?> :: Manage Products</h4>


<div class="col-lg-12  ">
 <?php echo $this->Form->create('ProductInfo',array('url'=>array('action'=>'manage_products','controller'=>'Dataentry'),'type'=>'get', 'class'=>'form-horizontal login-from')); ?>
 <div class="clearfix"></div>
  <div class="gap20">&nbsp;<br /><br /><br /></div>
   <div class="gap20"></div>
  <div class="form-group">
  <div class="col-lg-2"><label for="exampleInputEmail1">Search By:</label></div>
  <div class="col-lg-8"> <div class="form-group">
  <div class="col-lg-4">   <?php echo $this->Form->input('ndcproduct',array('placeholder' => 'Lot no/Product NDC/Product Details',
        'label' => false,
		'value'=>$search,
		'required'=>'required',
        'class'=>'form-control'));?> 
   
  </div>
  <div class="col-lg-4"> <?php echo $this->Form->button('<i class="fa fa-search"></i> Search', array(
    'type' => 'submit',
    'class' => 'btn btn-default',
    'escape' => false
));?></div>
  <div class="clearfix"></div>
    <small>Search By <cite title="Source Title">Lot no/Product NDC/Product Details</cite></small>
  
            </div></div>

  </div>
  <?php echo $this->Form->end();?>
  <div class="clearfix"></div>
  <div class="gap20"></div>
</div>


 <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Products</h3>
      </div>
 <?php 
	   echo $this->Session->flash(); 
	  
	   //echo $this->Session->flash('auth'); ?>
<div class="panel-body">
 <?php echo $this->Form->create('ProductInfo',array('url'=>array('action'=>'manage_products','controller'=>'Dataentry'),'type'=>'file', 'class'=>'login-from')); ?>

		 <div class="form-group">
              <?php echo $this->Form->input('product_ndc',array('placeholder' => 'Product NDC',
        'label' => 'Product NDC',
		
        'class'=>'form-control'));?> 
            </div>
           <div class="form-group">
             <?php echo $this->Form->input('prodid',array('options' =>$products,
			 'empty'=>array(''=>'--Select Product Name--'),
        'label' => 'Product Name',
		'required'=>'required',
        'class'=>'form-control'));?>
            </div>

            <div class="form-group">
              <?php echo $this->Form->input('batchno',array('placeholder' => 'Lot No',
        'label' => 'Lot No',
		'required'=>'required',
        'class'=>'form-control'));?> 
            </div>
    
               <div class="form-group">
               <?php echo $this->Form->input('expdate',array('placeholder' => 'Expiry Date',
        'label' => 'Expiry Date',
		'type'=>'text',
		'required'=>'required',	
        'class'=>'form-control'));?> 
            </div>
         
            
            
             <div class="form-group">
               <?php echo $this->Form->input('availability',array('placeholder' => 'Product Availability',
        'label' => 'Product Availability',
		'required'=>'required',
        'class'=>'form-control'));?> 
            </div>
            
         <div class="form-group">
               <?php echo $this->Form->input('VendorName',array('placeholder' => 'Vendor Name',
        'label' => 'Vendor Name',
		'required'=>'required',
        'class'=>'form-control'));?> 
            </div>
            
             <div class="form-group">
               <?php echo $this->Form->input('InvoiceNo',array('placeholder' => 'Invoice No',
        'label' => 'Invoice No',
		'required'=>'required',
        'class'=>'form-control'));?> 
            </div>
            
             <div class="form-group">
               <?php echo $this->Form->input('ReceivedDate',array('placeholder' => 'Received Date',
        'label' => 'Received Date',
		'type'=>'text',
		'required'=>'required',
        'class'=>'form-control'));?> 
            </div>                        
             <div class="form-group">
               <?php echo $this->Form->input('Note',array('placeholder' => 'Note',
        'label' => '',
		'type' => 'textarea',
        'class'=>'form-control'));?> 
            </div>  
        
             
            
            <div class="gap20"></div>


 <div class="form-actions">
 <?php echo $this->Form->input('id',array('type'=>'hidden'));?>
<?php echo $this->Form->submit(__('Submit'), array('class'=>'btn btn-primary', 'div'=>false));?>&nbsp;
<?php echo $this->Form->reset(__('Cancel'), array('class'=>'btn', 'div'=>false));?>
</div>
<?php echo $this->Form->end();?>


      </div>
  </div>
  </div>

<div class="col-lg-12 text-center">

      <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Product List</h3>
      </div>
      <div class="panel-body">
       <div class="table-responsive">
             
<input type="text" name="foucs" style="width:0px" id="foucs" />
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                        
                        <th><?php echo __('Product Name');?></th>
                        <th><?php echo __('Lot no');?></th> 
                        <th><?php echo __('Expiry');?></th> 
                        <th><?php echo __('Vendor Name');?></th>
                        <th><?php echo __('Invoice No');?></th>
                        <th><?php echo __('Received Date');?></th>
                        <th><?php echo __('Quantity');?></th>
                        <th><?php echo __('Actions');?></th>
                    </tr>
                </thead>
                
                <tbody>
                
				<?php 
				$i=0;
				foreach ($users as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				?>
                <tr <?php echo $class;?> > 
                       
                <td><?php echo h($user['Product']['productname']); ?>&nbsp;</td>
                <td><?php echo h($user['ProductInfo']['batchno']); ?>&nbsp;</td>
                <td><?php echo h(date("Y-m-d",strtotime($user['ProductInfo']['expdate']))); ?>&nbsp;</td>
                 <td><?php echo h($user['ProductInfo']['VendorName']); ?>&nbsp;</td> 
                <td><?php echo h($user['ProductInfo']['InvoiceNo']); ?>&nbsp;</td> 
                <td><?php echo h(date("m-d-Y H:i:s",strtotime($user['ProductInfo']['ReceivedDate']))); ?>&nbsp;</td> 
                <td><?php echo h($user['ProductInfo']['availability']); ?>&nbsp;</td>                
                <td class="center"> <?php echo $this->Html->link($this->Html->tag('i','Edit',array('class'=>'icon-pencil icon-white')),array('action' => 'manage_products', $user['ProductInfo']['id']),array("class"=>"btn btn-primary btn-mini tooltip-top","data-original-title"=>"Edit",'escape'=>false)); ?>
                
                <?php echo $this->Form->postLink($this->Html->tag('i','Delete',array('class'=>'icon-remove icon-white')), array('action' => 'manageproducts_delete', $user['ProductInfo']['id']), array("class"=>"btn btn-danger btn-mini tooltip-top","data-original-title"=>"Delete",'escape'=>false), __('Are you sure you want to delete # %s?',$user['Product']['productname'])); ?>
               
 				</td>
                </tr>
                <?php endforeach; ?>                
                </tbody>
            </table>
            <div class="pagination pagination-right pagination-mini">
	  <ul>
		<?php echo $this->Paginator->prev(''); ?>
		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
		<?php echo $this->Paginator->next(''); ?>
	  </ul>
	</div>
       </div>
      </div>
    </div>


</div>
      </div>

     

    </div> 
<style>
#content-area{
	background-image:none;
}
</style>

<script>

$(document).ready(function(){
	
	
	
<?php if($search!=''){?>
		$("#foucs").focus();
	$("#foucs").hide();
	<?php }?>
	$("#foucs").hide();

$( "#ProductInfoExpdate" ).datepicker({dateFormat: 'yy-mm-dd' ,minDate: 0 }); 
	$( "#ProductInfoReceivedDate" ).datepicker({dateFormat: 'yy-mm-dd',onSelect: function(dateText, inst) {
       if($("#ProductInfoBatchno").val()=='JD/STOCK'){
	   $('#ProductInfoReceivedDate').val('2014-06-01');
	   $('#ProductInfoReceivedDate').attr("readonly","readonly");
   }

    }});
	
	$("#ProductInfoManageProductsForm").validate({
		rules: {
			"data[ProductInfo][availability]":{
				required: true,
				number:true,
				min: 1
				}
		},
		messages: {
			"data[ProductInfo][availability]":"Please enter a valid number",
			
		},
		errorElement:"span"});
	$( "#ProductInfoExpdate" ).datepicker({dateFormat: 'yy-mm-dd' ,minDate: 0 }); 
	
	$( "#ProductInfoProductNdc" ).autocomplete(
	{
		source:<?php echo $products_jshon;?>,
		select: function( event, ui ) {
			$( "#ProductInfoProductNdc" ).val( ui.item.label  );
			$( "#ProductInfoProdid" ).val( ui.item.value  );
			return false;
		}
	}).data( "autocomplete" )._renderItem = function( ul, item ) {
		return $( "<li></li>" )
			.data( "item.autocomplete", item )
			.append( "<a><strong>" + item.label + "</strong> </a>" )
			.appendTo( ul );
		};		
});


//Updaet Recived date
$("#ProductInfoBatchno").blur(function(){
   if($("#ProductInfoBatchno").val()=='JD/STOCK'){
	   $('#ProductInfoReceivedDate').val('2014-06-01');
	   $('#ProductInfoReceivedDate').attr("readonly","readonly");
   }else{
	   $('#ProductInfoReceivedDate').removeAttr("readonly");
   }
});
</script>