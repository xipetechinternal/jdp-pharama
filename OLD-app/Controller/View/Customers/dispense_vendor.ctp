<?php echo $this->Html->script('https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js', array('inline' => true));?>
   <?php
      echo $this->Html->css('https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css');
    ?>

      <div class="row">   

        <div class="col-md-offset-8 col-lg-4">
<div class="panel panel-default" style="margin-top:-100px">

  <div class="panel-body form-bg text-center ">
<p>Welcome <?php echo ucfirst($this->Session->read('Auth.User.fname')); ?></p>

   <?php echo $this->Html->link('Sign out', array('controller'=>'customers', 'action'=>'logout'),array('class' => 'btn btn-warning btn-block'));?>
   
   <p><?php echo $this->Html->link($cart_list.' Orders Pending', array('controller'=>'customers', 'action'=>'order_cart'),array('class' => ''));?>
</p>
  </div>
</div>
        </div>

        </div>
 
      <div class="clearfix"></div>
      
<div class="row">
  <div class="col-lg-12"> 
  <?php echo $this->Session->flash();  ?>
    <ol class="breadcrumb">
      <li><a href="<?php echo $this->Html->url('/customers/index');?>">Home</a></li>
      <li class="active">Vendor Purchases</li>
    </ol>
    <?php echo $this->Form->create('OrderHistory',array('url'=>array('action'=>'dispense_vendor','controller'=>'customers'),'class'=>'paraform')); ?>
     
      <div class="form-group">
        <div class="col-lg-2">
          <label for="exampleInputEmail1">Select Product:</label>
        </div>
        <div class="col-lg-8">
          <div class="form-group">
            <div class="col-lg-4">
            <?php echo $this->Form->input('product_ndc',array('placeholder' => 'Product NDC',
        'label' => false,		
        'class'=>'form-control'));?>
             
            </div>
            <div class="col-lg-8">
           <?php echo $this->Form->input('prodid',array('options' =>$product_list,
			 'empty'=>array(''=>'--Select Product Name--'),
        'label' => false,
		'required'=>'required',
        'class'=>'form-control'));?>
              
            </div>           
          </div>
        </div>
         
      </div>
      <div class="clearfix"></div>
      <div class="gap20"></div>
    <div class="form-group row">
        <div class=" col-lg-2">
          <label for="exampleInputEmail1">Vendor Name:</label>
        </div>
        <div class="col-lg-8">
          <div class="form-group">
            <div class="col-lg-4">
            <?php echo $this->Form->input('vendor_name',array('placeholder' => 'Vendor Name',
        'label' => false,	
		'required'=>'required',	
        'class'=>'form-control'));?>
             
            </div>
            
           
            
          </div>
        </div>
         
      </div>
       <div class="form-group row">
        <div class=" col-lg-2">
          <label for="exampleInputEmail1">Product Quantity:</label>
        </div>
        <div class="col-lg-8">
          <div class="form-group">
            <div class="col-lg-4">
            <?php echo $this->Form->input('product_dispense',array('placeholder' => 'Product Quantity',
        'label' => false,	
		'required'=>'required',	
        'class'=>'form-control'));?>
             
            </div>
           <!-- <div class="col-lg-4">
            <?php echo $this->Form->input('date_time',array('placeholder' => 'Date',
        'label' => false,		
        'class'=>'form-control'));?>   
            </div>-->
            
           
            
          </div>
        </div>
         
      </div>
      <div class="col-lg-10 text-right">
      <?php echo $this->Form->submit(__('Add'), array('class'=>'btn btn-success', 'div'=>false));?>
      </div>
      <?php echo $this->Form->end();?>
      <div class="clearfix"></div>
      <div class="gap20"></div>
    
    
    
    
    
  </div>
  <div class="col-lg-12 text-left"><a class="btn btn-info " href="<?php echo $this->Html->url('/customers/dispense');?>">Back</a> </div>
      <div class="col-lg-12 text-center">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">Vendor Product Inventory</h3>
          </div>
          <div class="panel-body">
            <div class="table-responsive">
             
             </div>
                 
                
              <table id="example" class="datatable display table table-striped table-bordered" width="100%" >
                <thead>
                  <tr>
                   
                    <th><strong><?php echo __('Product NDC');?></strong></th>
                    <th><strong><?php echo __('Item Number');?></strong></th>
                    <th><strong><?php echo __('Product Name');?></strong></th>
                    <th><strong><?php echo __('Quantity');?></strong></th>
                    <th><strong><?php echo __('Size');?></strong></th>                    
                    <th><strong><?php echo __('Total Size Purchase');?></strong></th>
                    
                  </tr>
                </thead>
                 <?php if(empty($users)): ?>
		<tr>
			<td colspan="8" class="striped-info">History not available.</td>
		</tr>
		<?php endif;  ?>

                <tbody>
                
				<?php 
				
				$i=0;
				$product_price=0;
				foreach ($users as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				
				
				?>
                <tr <?php echo $class;?> > 
                        
               
                <td><?php echo h($user['Product NDC']); ?> &nbsp;</td>
               <td><?php echo h($user['Item Number']); ?> &nbsp;</td>
               
                             
               <td><?php echo h($user['Product Name']); ?>&nbsp;</td>
                    <td><a class="btn btn-warning " href="<?php echo $this->Html->url('/customers/dispense_vendor_list/'.$user['Product NDC']);?>"><?php echo h($user['Quantity']); ?></a> </td>
                    <td><?php echo h($user['Size']); ?> &nbsp;</td>  
                    <td><?php echo h($user['Total Size Purchase']); ?> &nbsp;</td>  
                    
 
                </tr>
                <?php endforeach; ?>                
                </tbody>
                
              </table>
              
            </div>
            <div class="clearfix"></div>
            <div class="gap20"></div>
           

            
          </div>
        </div>
      </div>
     
      
<script>

$(document).ready(function(){
	$( "#OrderHistoryDateTime" ).datepicker({
       
        changeMonth: true,
		changeYear: true,
       dateFormat: 'yy-mm-dd' ,
        
    });
	$('#OrderHistoryProductNdc').on('blur mouseup',function(){
    		var products_jshon=<?php echo json_encode($product_list);?>;
        $.each(products_jshon, function(i, v) {
			console.log(i,v);
			if (i == $("#OrderHistoryProductNdc").val()) {
				
				$( "#OrderHistoryProdid" ).val(i);
				return;
			}
		});
    });
	
	$( "#OrderHistoryProductNdc" ).autocomplete(
	{
		source:<?php echo json_encode($product_list_auto);?>,
		select: function( event, ui ) {
			$( "#OrderHistoryProductNdc" ).val( ui.item.label  );
			$( "#OrderHistoryProdid" ).val( ui.item.value  );
			return false;
		}
	}).data( "autocomplete" )._renderItem = function( ul, item ) {
		return $( "<li></li>" )
			.data( "item.autocomplete", item )
			.append( "<a><strong>" + item.label + "</strong> </a>" )
			.appendTo( ul );
		};

	$("#ProductInfoAdminSalesReportPersonForm").validate();
	//$( "#ProductInfoSelfrmdate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
   // $( "#ProductInfoSeltodate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
	
	
    


});
</script>