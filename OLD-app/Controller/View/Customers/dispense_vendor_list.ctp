<?php echo $this->Html->script('https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js', array('inline' => true));?>
   <?php
      echo $this->Html->css('https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css');
    ?>

      <div class="row">   

        <div class="col-md-offset-8 col-lg-4">
<div class="panel panel-default" style="margin-top:-100px">

  <div class="panel-body form-bg text-center ">
<p>Welcome <?php echo ucfirst($this->Session->read('Auth.User.fname')); ?></p>

   <?php echo $this->Html->link('Sign out', array('controller'=>'customers', 'action'=>'logout'),array('class' => 'btn btn-warning btn-block'));?>
   
   <p><?php echo $this->Html->link($cart_list.' Orders Pending', array('controller'=>'customers', 'action'=>'order_cart'),array('class' => ''));?>
</p>
  </div>
</div>
        </div>

        </div>
 
      <div class="clearfix"></div>
      
<div class="row">
  <div class="col-lg-12"> 
  <?php echo $this->Session->flash();  ?>
    <ol class="breadcrumb">
      <li><a href="<?php echo $this->Html->url('/customers/index');?>">Home</a></li>
      <li class="active">Vendor Products Purchases List</li>
    </ol>
         <div class="clearfix"></div>
      <div class="gap20"></div>
    
    
    
    
    
  </div>
  <div class="col-lg-12 text-left"><a class="btn btn-info " href="<?php echo $this->Html->url('/customers/dispense_vendor');?>">Back</a> </div>
      <div class="col-lg-12 text-center">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">Vendor Product Inventory</h3>
          </div>
          <div class="panel-body">
            <div class="table-responsive">
             
             </div>
                 
                
              <table id="example" class="datatable display table table-striped table-bordered" width="100%" >
                <thead>
                  <tr>
                   <th><strong><?php echo __('Product Vendor');?></strong></th>
                    <th><strong><?php echo __('Product NDC');?></strong></th>
                    <th><strong><?php echo __('Item Number');?></strong></th>
                    <th><strong><?php echo __('Product Name');?></strong></th>
                    <th><strong><?php echo __('Quantity');?></strong></th>
                    <th><strong><?php echo __('Size');?></strong></th>                    
                    <th><strong><?php echo __('Total Size Purchase');?></strong></th>
                    <th><strong><?php echo __('Created');?></strong></th>
                    <th><strong><?php echo __('Action');?></strong></th>
                  </tr>
                </thead>
                 <?php if(empty($users)): ?>
		<tr>
			<td colspan="8" class="striped-info">History not available.</td>
		</tr>
		<?php endif;  ?>

                <tbody>
                
				<?php 
				
				$i=0;
				$product_price=0;
				foreach ($users as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				
				
				?>
                <tr <?php echo $class;?> > 
                        
               <td><?php echo h($user['Vendor Name']); ?> &nbsp;</td>
                <td><?php echo h($user['Product NDC']); ?> &nbsp;</td>
               <td><?php echo h($user['Item Number']); ?> &nbsp;</td>
               
                             
               <td><?php echo h($user['Product Name']); ?>&nbsp;</td>
                    <td><?php echo $user['Quantity']?> </td>
                    <td><?php echo h($user['Size']); ?> &nbsp;</td>  
                    <td><?php echo h($user['Total Size Purchase']); ?> &nbsp;</td> 
                    <td><?php echo h(date("m-d-Y",strtotime($user['created']))); ?> &nbsp;</td> 
                    <td><?php echo $this->Form->postLink($this->Html->tag('i','Delete',array('class'=>'icon-remove icon-white')), array('action' => 'dispense_list_delete',$user['id'],$user['Product NDC']), array("class"=>"btn btn-danger btn-mini tooltip-top","data-original-title"=>"Delete",'escape'=>false), __('Are you sure you want to delete # %s?',$user['Product NDC'])); ?></td>
 					
                </tr>
                <?php endforeach; ?>                
                </tbody>
                
              </table>
              
            </div>
            <div class="clearfix"></div>
            <div class="gap20"></div>
           

            
          </div>
        </div>
      </div>
     
      
<script>

$(document).ready(function(){
	$( "#OrderHistoryDateTime" ).datepicker({
       
        changeMonth: true,
		changeYear: true,
       dateFormat: 'yy-mm-dd' ,
        
    });
	$('#OrderHistoryProductNdc').on('blur mouseup',function(){
    		var products_jshon=<?php echo json_encode($product_list);?>;
        $.each(products_jshon, function(i, v) {
			console.log(i,v);
			if (i == $("#OrderHistoryProductNdc").val()) {
				
				$( "#OrderHistoryProdid" ).val(i);
				return;
			}
		});
    });
	
	$( "#OrderHistoryProductNdc" ).autocomplete(
	{
		source:<?php echo json_encode($product_list_auto);?>,
		select: function( event, ui ) {
			$( "#OrderHistoryProductNdc" ).val( ui.item.label  );
			$( "#OrderHistoryProdid" ).val( ui.item.value  );
			return false;
		}
	}).data( "autocomplete" )._renderItem = function( ul, item ) {
		return $( "<li></li>" )
			.data( "item.autocomplete", item )
			.append( "<a><strong>" + item.label + "</strong> </a>" )
			.appendTo( ul );
		};

	$("#ProductInfoAdminSalesReportPersonForm").validate();
	//$( "#ProductInfoSelfrmdate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
   // $( "#ProductInfoSeltodate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
	
	
    


});
</script>