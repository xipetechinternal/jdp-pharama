<div class="row">   
  <div class="col-md-offset-8 col-lg-4">
    <div class="panel panel-default" style="margin-top:-100px">
      <div class="panel-body form-bg text-center ">
        <p>Welcome <?php echo ucfirst($this->Session->read('Auth.User.fname')); ?></p>

        <?php echo $this->Html->link('Sign out', array('controller'=>'customers', 'action'=>'logout'),array('class' => 'btn btn-warning btn-block'));?>
   
        <p><?php echo $this->Html->link($cart_list.' Orders Pending', array('controller'=>'customers', 'action'=>'order_cart'),array('class' => ''));?>
        </p>
      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>

<div class="row">
  <div> 
  <?php echo $this->Session->flash(); ?>
    <ol class="breadcrumb">
      <li><a href="<?php echo $this->Html->url('/customers/index');?>">Home</a></li>
      <li class="active">Request stock shold</li>
    </ol>
    
    
  </div>
</div>



<div class="clearfix"></div>

<div class="col-lg-12 text-center">

      <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Request stock shold</h3>
      </div>
      <div class="panel-body">
       <div class="table-responsive">
             

            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                        
                         <th><?php echo __('Product NDC');?></th>
                          <th><?php echo __('Item Number');?></th>
                        <th><?php echo __('Product Name');?></th>
                        <th><?php echo __('Client');?></th>
                        <th><?php echo __('Quantity');?></th>
                        <th><?php echo __('Requested ON');?></th>
                        <th><?php echo __('Status');?></th>
                       <th><?php echo __('Actions');?></th>
                    </tr>
                </thead>
                
                <tbody>
                
				<?php 
				$i=0;
				
				foreach ($users as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				?>
                <tr <?php echo $class;?> > 
                        
               
               
                <td><?php echo h($user['Product']['product_ndc']);  ?></td>  
                <td><?php echo h($user['Product']['itemnumber']); ?></td>    
                <td><?php echo h($user['Product']['productname']); ?> &nbsp;</td>
                <td><?php echo h($user['User']['fname']); ?></td> 
                <td><?php echo h($user['Productrequest']['quantity']); ?></td>                
                <td><?php echo h($user['Productrequest']['created']); ?> &nbsp;</td>
                <td><?php echo h(($user['Productrequest']['status']=='ano')?"Announce":"Pending"); ?></td>    
               <td class="center"> 
				
                
                <?php echo $this->Form->postLink($this->Html->tag('i','Delete',array('class'=>'icon-remove icon-white')), array('action' => 'request_stock_delete', $user['Productrequest']['id']), array("class"=>"btn btn-danger btn-mini tooltip-top","data-original-title"=>"Delete",'escape'=>false), __('Are you sure you want to delete # %s?',$products[$user['ProductInfo']['itemnumber']])); ?><br />
				
              
 				</td>
                </tr>
                <?php endforeach; ?>                
                </tbody>
            </table>
            <div class="pagination pagination-right pagination-mini">
	  <ul>
		<?php echo $this->Paginator->prev(''); ?>
		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
		<?php echo $this->Paginator->next(''); ?>
	  </ul>
	</div>
       </div>
      </div>
    </div>


</div>





