
<div class="clearfix"><br /></div>

<div class="row">
  <div> 
  <?php echo $this->Session->flash(); ?>
    <ol class="breadcrumb">
      <li><a href="<?php echo $this->Html->url('/customers/index');?>">Home</a></li>
      <li class="active">Product Type Search</li>
    </ol>
    
    
  </div>
</div>

<?php if($product_type!=NULL):;?>
<div class="row mt30">
      <div class="col-sm-12">
        <div class="btn-group btn-group-justified btn-justified-common" aria-label="Justified button group" role="group">
          <a class="btn btn-default" role="button" href="<?php echo $this->Html->url('/customers/product_types/brand');?>">Brand</a>
          <a class="btn btn-default" role="button" href="<?php echo $this->Html->url('/customers/product_types/generic');?>">Generic</a>
          <a class="btn btn-default" role="button" href="<?php echo $this->Html->url('/customers/product_types/otc');?>">Otc</a>
          <a class="btn btn-default" role="button" href="<?php echo $this->Html->url('/customers/product_types/Refrigirated');?>">Refrigirated</a>
          <a class="btn btn-default" role="button" href="<?php echo $this->Html->url('/customers/product_types/Controlled');?>">Controlled</a>
        </div>
      </div>
    </div>
    <br />
<div class="clearfix"></div>

<div class="text-center">
  <div class="panel panel-primary">
    <div class="panel-heading">
      <h3 class="panel-title">Product List</h3>
    </div>
    <div class="panel-body11">
      <div class="table-responsive11">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
          <thead>
            <tr>
              <th><?php echo __('Item&nbsp;#');?></th>
              <th><?php echo __('Product NDC');?></th> 
              <th><?php echo __('Product Info');?></th> 
              <th><?php echo __('Price');?></th>
              <th><?php echo __('Inventory');?></th>
              <th><?php echo __('Manufacture');?></th>
              <th><?php echo __('Actions');?></th>
            </tr>
          </thead>    
          <tbody>
            <?php if(empty($products)): ?>
        		<tr>
        			<td colspan="7" class="striped-info">No record found.</td>
        		</tr>
        		<?php endif;  ?>
        				<?php 
        				$i=0;
        				foreach ($products as $user): 
        				$class =' class="odd"';
        				if ($i++ % 2 == 0) {
        					$class = ' class="even"';
        				}
				//Apply price discount accourding client base;
				/*$priceafter_discount=$user['Product']['productprice'];
				$discount=($this->Session->read("Auth.User.".$user['Product']['producttype']."_discount")/100);
				$priceafter_discount=($discount>0)?round($priceafter_discount-($discount*$priceafter_discount),2):$priceafter_discount;*/
				$product_price=$user['products']['productprice'];
				$purchasePrice=$user['products']['PurchasePrice'];
				$product_prafit=$product_price-$purchasePrice;
				$discount=($this->Session->read("Auth.User.".$user['products']['producttype']."_discount")/100);				
				$profit_percent=$product_prafit*$discount;
				
				$priceafter_discount=($profit_percent>0)?round(($product_price-$profit_percent),2):$product_price;
				//apply three percent overall cost
				if($threpercenteoff)
				$priceafter_discount=$priceafter_discount-($priceafter_discount*3/100);
				?>
                <tr <?php echo $class;?> > 
                        
                <td><?php echo h($user['products']['itemnumber']); ?>&nbsp;</td>
                <td><?php echo h($user['products']['product_ndc']); ?>&nbsp;</td>
                <td><?php echo h('products Name: '.$user['products']['productname'])?><br>
                	Category :<?php echo h($user['products']['producttype']); ?><br>
                    &nbsp;</td>
                <td><?php echo ($user[0]['availability']>0)?h(round($priceafter_discount,2)):'Call for Price'; ?>&nbsp;</td>
                <td><?php echo h($user[0]['availability']); ?>&nbsp;</td>     
                 <td><?php echo h($user['products']['manufacture']); ?>&nbsp;</td>               
                <td class="center"> 
                <?php if($user['products']['add_restriction']){?>
                <a href="<?Php echo $this->Html->url('/customers/add_restriction/'.$user['products']['id']).','.$product_type?>"  class="push btn push btn-success" data-toggle="modal">Order</a>
                <?php }else{
				 echo ($user[0]['availability']>0 and $user['products']['active']!="False")? '<a href="#myModal" data-qtyinstock="'.$user[0]['availability'].'" data-producttype="'.$user['products']['producttype'].'" rel="chatusr"  id="'.$user['products']['id'].'" class="push btn push btn-success" data-toggle="modal">Order</a>':'<a class="btn btn-primary" href="'.$this->Html->url('/customers/product_request/'.$user['products']['id']).'_'.$product_type.'"> Request </a>'; 
				 } ?>
                
               
               
 				         </td>
                </tr>
                <?php endforeach; ?>                
              </tbody>
        </table>
        <div class="pagination pagination-right pagination-mini">
      	  <ul>
      		<?php echo $this->Paginator->prev(''); ?>
      		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
      		<?php echo $this->Paginator->next(''); ?>
      	  </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Place Order</h4>
      </div>
      <div class="modal-body">
        <div class="something"><?php echo $this->Form->create('customers',array('action'=>'basketUpdate','type'=>'file', 'class'=>'login-from')); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <?php echo $this->Form->input('prodtype',array('placeholder' => 'Item Number',
        'label' => '',
		'required'=>'required',
		'type'=>'hidden',
        'class'=>'form-control'));?>
       <?php echo $this->Form->input('pro_id',array('placeholder' => 'Item Number',
        'label' => '',
		'required'=>'required',
		'type'=>'hidden',
        'class'=>'form-control'));?>
 
  <tr>
    <td align="left" valign="top">Quantity :   <?php echo $this->Form->input('quantity',array('placeholder' => 'Quantity',
        'label' => '',
		'required'=>'required',		
        'class'=>'form-control'));?><div class="gap20"></div></td>
  </tr>

 <tr>
    <td align="left" valign="top"> <?php echo $this->Form->submit(__('Order'), array('class'=>'btn btn-primary', 'div'=>false));?>&nbsp;</td>
  </tr>
  
  
</table>
<?php echo $this->Form->end();?> </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php endif;?>


<script>
$(function(){

   $('.push').click(function(){
      var essay_id = $(this).attr('id');
	    var quantity = $(this).data('qtyinstock');
		$("#customersProId").val(essay_id);
		$("#customersProdtype").val($(this).data('producttype'));
		
$('#mymodal').show();
      
	  $("#customersBasketUpdateForm").validate({

		rules: {
			"data[customers][quantity]":{
				required: true,
				number:true,
				min:1,
				max:quantity

				}
		},
		messages: {
			"data[customers][quantity]":"Please enter a valid number & Order should be less than ".quantity,
			
		},
		errorElement:"span"

		});


});

   });
</script>