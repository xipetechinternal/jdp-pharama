<!DOCTYPE html>
<html class="no-js">
<!--<![endif]-->
<head>
<meta name="description" content="">
<meta name="viewport" content="width=device-width">
<?php echo $this->Html->charset(); ?>
<title><?php echo $title_for_layout; ?></title>
<?php echo $this->Html->css(array('bootstrap.min','main'));?> 
<?php // echo $this->Html->css(array('/css/bootstrap/css/bootstrap-notify'));?> 
<?php echo $this->Html->css('jquery.ui.datepicker.min');?>
<?php echo $this->Html->css('jquery.ui.core.min');
	echo $this->Html->css('jquery.ui.theme.min');
?>
<?php echo $this->Html->script(array('/vendors/modernizr-2.6.2-respond-1.1.0.min'));?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script> 
<script>window.jQuery || document.write('<script src="<?php echo $this->Html->url('js/jquery-1.8.3.min.js');?>"><\/script>')</script> 
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-66023494-1', 'auto');
  ga('send', 'pageview');

</script>

<style>
.alert-error{
		      background-image: -webkit-linear-gradient(top,#f2dede 0,#e7c3c3 100%);
    background-image: -o-linear-gradient(top,#f2dede 0,#e7c3c3 100%);
    background-image: -webkit-gradient(linear,left top,left bottom,from(#f2dede),to(#e7c3c3));
    background-image: linear-gradient(to bottom,#f2dede 0,#e7c3c3 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fff2dede', endColorstr='#ffe7c3c3', GradientType=0);
    background-repeat: repeat-x;
    border-color: #dca7a7;
	color: #a94442;
    background-color: #f2dede;
    border-color: #ebccd1;
	  }
/* Carousel base class */
    .carousel {
      margin-bottom: 60px;
    }

    .carousel .container {
      position: absolute;
      right: 0;
      bottom: 0;
      left: 0;
	   top: 100px;
    }

    .carousel-control {
      background-color: transparent;
      border: 0;
      font-size: 120px;
      margin-top: 0;
      text-shadow: 0 1px 1px rgba(0,0,0,.4);
    }

    .carousel .item {
      height: 300px;
    }
    .carousel img {
      min-width: 100%;
      height: 300px;
    }

    .carousel-caption {
      background-color: transparent;
      position: static;
      max-width: 550px;
      padding: 0 20px;
      margin-bottom: 100px;
    }
    .carousel-caption h1,
    .carousel-caption .lead {
      margin: 0;
      line-height: 1.25;
      color: #fff;
      text-shadow: 0 1px 1px rgba(0,0,0,.4);
    }
    .carousel-caption .btn {
      margin-top: 10px;
    }

	  </style>

</head>
<body>
<div class="jumbotron">
  <div class="container">
    <div id="wrapper">
      <div class="row">
        <div class="col-lg-4"> <a href="#"><img src="<?php echo $this->Html->url('/img/logo2.png');?>" alt="" class="img-responsive"></a> </div>
      </div>
      <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide">
      <div class="carousel-inner">
        <div class="item active">
          <img src="<?php echo $this->Html->url('/img/JD-banner-img.png');?>" alt="">
          <div class="container">
            <div class="carousel-caption">
              <h1></h1>
              <p class="lead"></p>
              
            </div>
          </div>
        </div>
        
      </div>
      <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
    </div><!-- /.carousel -->

    </div>
  </div>
</div>

<div class="container">
   <div id="content-area-inner-pages">
  <?php if($this->Session->read('Auth.User.name')){ ?>
    <div class="row">
      <div class="col-md-offset-8 col-lg-4">
        <div class="panel panel-default" style="margin-top:-100px">
          <div class="panel-body form-bg text-center ">
            <p><?php echo ucfirst($this->Session->read('Auth.User.name')); ?></p>
            <button type="submit" class="btn btn-warning btn-block" onclick="window.location='<?php echo $this->Html->url('/customers/logout');?>'">Sign out</button>
          </div>
        </div>
      </div>
    </div>
    <?php } ?>
    <div class="clearfix"></div>
    
     <?php echo $this->fetch('content'); ?>		
  </div>


  <?php echo $this->element('footer');?>
  
  <footer class="text-center">
  	<div class="row">
      <div class="col-sm-12">
        <table style="width:100%;">
          <tr>
            <td style="width:48%; text-align:right; text-transform:uppercase; vertical-align: text-top;"><a target="_blank" href="/Website Privacy Statement and Terms of Use.pdf">Website Privacy Statement and Terms of Use</a></td>
            <td style="width:4%; text-align:center;vertical-align: text-top;"><span style="color:#eb7228;font-weight:bold">|</span></td>
            <td style="width:48%; text-align:left;vertical-align: text-top;"><a target="_blank" href="/ADDENDUM & AGREEMENT.pdf">ADDENDUM &amp; AGREEMENT</a></td>
          </tr>
          <tr>
            <td style="width:48%; text-align:right;vertical-align: text-top;"><a target="_blank" href="/JD PHARMACEUTICAL WHOLESALER INC. RETURN POLICY.pdf">JD PHARMACEUTICAL WHOLESALER INC. RETURN POLICY</a></td>
            <td class="text-center" style="vertical-align: text-top;"><span style="color:#eb7228;font-weight:bold">|</span></td>
            <td style="width:48%; text-align:left;vertical-align: text-top;"><a target="_blank" href="/new app for new customer.pdf">NEW CUSTOMER APPLICATION</a></td>
          </tr>
          
          <tr>
            <td style="width:48%; text-align:right;vertical-align: text-top;"><a target="_blank" href="/NEWSBLOG.pdf">TI,TH,TS ACT</a></td>
            <td class="text-center" style="vertical-align: text-top;"><span style="color:#eb7228;font-weight:bold">|</span></td>
            <td style="width:48%; text-align:left;vertical-align: text-top;"><a href="<?php echo $this->Html->url('/pages/index/about_us');?>" <?php if($this->params['pass'][0]=='about_us'){ ?>style="color:#EB7228;" <?php } ?> >About Us</a></td>
           
            <tr><td colspan="3" class="text-center"  ><a style="color:#eb7228;font-weight:bold" target="_blank" href="http://www.fda.gov/downloads/Drugs/GuidanceComplianceRegulatoryInformation/Guidances/UCM453225.pdf"> FDA Publishes New Guidance Delaying Dispenser 3T Requirements Until March 1, 2016</a></td></tr>
          </tr>
        </table>
      </div>
    </div>
    <br/>
    <small style="color:#777;">Copyright &copy;<?php echo date("Y")?> JD Pharmaceutical Inc. All rights reserverd</small>
  </footer>
</div>
<!-- /container --> 
 <!-- set up the modal to start hidden and fade in and out -->
<div id="login_alaert_modal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- dialog body -->
      <div class="modal-body" style="padding-bottom:0px">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       <strong style="color:#eb7228;font-size:18px">ATTENTION ALL CUSTOMERS</strong>
      </div>
      <!-- dialog buttons -->
      <div class="modal-footer text-center"><p class="text-left">EARN ADDITIONAL 1% ON BRANDS AND 3% ON GENERICS BY PURCHASING 10 OR MORE OF AN ITEM - THIS SPECIAL IS ONLY ON ITEMS THAT HAS 10 OR MORE AVAILABLE IN STOCK</p></div>
    </div>
  </div>
</div>

<?php echo $this->Html->script(array('main','/css/bootstrap/js/bootstrap.min','plugins'));?>
<?php echo $this->Html->script(array('main','/css/bootstrap/js/bootstrap-notify'));?>
<?php echo $this->Html->script(array('jquery.validation.functions'));?>
<?php echo $this->Html->script(array('jquery.validate.min'));?>
<?php echo $this->Html->script(array('carousel'));?>
<?php echo $this->Html->script(array('jquery-ui-1.9.2.datepicker.custom.min'));?>
<!-- sometime later, probably inside your on load event callback -->
<?php echo $this->Html->script(array('carousel'));?>
  <?php  if($this->Session->read('Auth.User.fname') and ($this->Session->check('Auth.User.client_alert'))){ ?>
 <?php if($this->Session->read('Auth.User.client_alert')=='false'){?>
<script>
    $(document).ready(function(e) {
        $("#login_alaert_modal").modal({                    // wire up the actual modal functionality and show the dialog
      "backdrop"  : "static",
      "keyboard"  : true,
      "show"      : true                     // ensure the modal is shown immediately
    });
    });
    
    
</script>
<?php }}?>
<script>
      !function ($) {
        $(function(){
          // carousel demo
          $('#myCarousel').carousel()
        })
      }(window.jQuery)
    </script>

</body>
</html>
