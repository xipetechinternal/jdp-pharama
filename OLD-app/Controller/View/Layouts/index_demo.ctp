<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width,user-scalable=no">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="favicon.ico">
  <title><?php echo $title_for_layout; ?></title>
  <!-- Bootstrap core CSS -->
  <?php echo $this->Html->css(array("bootstrap.min","style"));?>
  <!-- Custom styles for this template -->
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<?php // echo $this->Html->css(array('/css/bootstrap/css/bootstrap-notify'));?> 
<?php echo $this->Html->css('jquery.ui.datepicker.min');?>
<?php echo $this->Html->css('jquery.ui.core.min');
	echo $this->Html->css('jquery.ui.theme.min');
?>
<?php echo $this->Html->script(array('/vendors/modernizr-2.6.2-respond-1.1.0.min'));?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script> 
<script>window.jQuery || document.write('<script src="<?php echo $this->Html->url('js/jquery-1.8.3.min.js');?>"><\/script>')</script> 
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-66023494-1', 'auto');
  ga('send', 'pageview');

</script>

<style>
.alert-error{
		      background-image: -webkit-linear-gradient(top,#f2dede 0,#e7c3c3 100%);
    background-image: -o-linear-gradient(top,#f2dede 0,#e7c3c3 100%);
    background-image: -webkit-gradient(linear,left top,left bottom,from(#f2dede),to(#e7c3c3));
    background-image: linear-gradient(to bottom,#f2dede 0,#e7c3c3 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fff2dede', endColorstr='#ffe7c3c3', GradientType=0);
    background-repeat: repeat-x;
    border-color: #dca7a7;
	color: #a94442;
    background-color: #f2dede;
    border-color: #ebccd1;
	  }

/* Carousel base class */
    .carousel {
      margin-bottom: 60px;
    }

    .carousel .container {
      position: absolute;
      right: 0;
      bottom: 0;
      left: 0;
	   top: 100px;
    }

    .carousel-control {
      background-color: transparent;
      border: 0;
      font-size: 120px;
      margin-top: 0;
      text-shadow: 0 1px 1px rgba(0,0,0,.4);
    }

    .carousel .item {
      height: 300px;
    }
    .carousel img {
      min-width: 100%;
      height: 300px;
    }

    .carousel-caption {
      background-color: transparent;
      position: static;
      max-width: 550px;
      padding: 0 20px;
      margin-bottom: 100px;
    }
    .carousel-caption h1,
    .carousel-caption .lead {
      margin: 0;
      line-height: 1.25;
      color: #fff;
      text-shadow: 0 1px 1px rgba(0,0,0,.4);
    }
    .carousel-caption .btn {
      margin-top: 10px;
    }

	  </style>
</head>
<body>
<section class="top-section">
  <div class="container">
    <div class="row">
      <div class="col-sm-8 col-xs-7">
        <a href="#"><img class="logo-img" style="max-height:136px;max-width:338px;" src="<?php echo $this->Html->url('/img/logo2.png');?>"></a>
      </div>
      <div class="col-sm-4 col-xs-5 text-right">

        <div class="btn-group customer-loged" role="group">
          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Welcome <p><?php echo ucfirst($this->Session->read('Auth.User.fname')); ?></p><span class="caret"></span>
          </button>
          <ul class="dropdown-menu dropdown-menu-right">
           <!-- <li><a href="<?php echo $this->Html->url(array("controller"=>"customers","action"=>"order_cart"))?>"><?php echo $cart_list; ?> Orders Pending</a></li>-->
            <li><a href="<?php echo $this->Html->url(array("controller"=>"customers","action"=>"logout"))?>">Logout</a></li>
          </ul>
        </div>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        
        <a style="color:#FFF; padding-top:10px" href="<?php echo $this->Html->url(array("controller"=>"customers","action"=>"order_cart"))?>" id="cart"><i class="fa fa-shopping-cart fa-2x"></i> <span class="badge"><?php echo $cart_list; ?></span> Orders Pending </a>
      </div>
    </div>
  </div>
</section>
     <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide">
      <div class="carousel-inner">
        <div class="item active">
          <img src="<?php echo $this->Html->url('/img/JD-banner-img.png');?>" alt="">
          <div class="container">
            <div class="carousel-caption">
              <h1></h1>
              <p class="lead"></p>
              
            </div>
          </div>
        </div>
        
      </div>
      <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
    </div><!-- /.carousel -->

    <section>
         <?php 
	   echo $this->Session->flash(); 
	  /* echo $this->Session->flash('auth');*/ ?>
  <?php if($product_annouced>0){?>
  		<div class="col-sm-12 text-center"> 
 <div class="alert alert-success" style="color:#eb7228" role="alert"><b>YOUR REQUESTED ITEM IS NOW AVALABLE</b>&nbsp;&nbsp;<a href="<?php echo $this->Html->url('/customers/request_stock_announce');?>">Show</a></div> 	
 </div>
  <?php }?>
  
  <?php if(!empty($Advertise)){
	  foreach($Advertise as $adt){ ?>
	  
	  <div class="col-sm-12 text-center"> 
 <div class="alert alert-success" style="color:#000;background-color:#eb7228;" role="alert"><b><?php echo $adt['Advertise']['advertise'];?></b>&nbsp;&nbsp;</div> 	
 </div>
  		
	  <?php }
      }?>
      <!-- Begin page content -->
<section>
  <div class="container">

        <?php echo $this->fetch('content'); ?>	
        <div class="row mt30">
      <div class="col-sm-12">
        <div class="btn-group btn-group-justified btn-justified-common" aria-label="Justified button group" role="group">
          <a class="btn btn-default" role="button" href="<?php echo $this->Html->url('/customers/product_types/brand');?>">Brand</a>
          <a class="btn btn-default" role="button" href="<?php echo $this->Html->url('/customers/product_types/generic');?>">Generic</a>
          <a class="btn btn-default" role="button" href="<?php echo $this->Html->url('/customers/product_types/otc');?>">Otc</a>
          <a class="btn btn-default" role="button" href="<?php echo $this->Html->url('/customers/product_types/Refrigirated');?>">Refrigirated</a>
          <a class="btn btn-default" role="button" href="<?php echo $this->Html->url('/customers/product_types/Controlled');?>">Controlled</a>
        </div>
      </div>
    </div>
         <!-- // -->
    <div class="row">
      <div class="col-sm-6 mt30">
        <div class="list-group list-g-common">
            <a class="list-group-item text-uppercase"  target="_blank" href="/Website Privacy Statement and Terms of Use.pdf">Website Privacy Statement and Terms of Use</a>
          <a class="list-group-item" target="_blank" href="/JD PHARMACEUTICAL WHOLESALER INC. RETURN POLICY.pdf">JD PHARMACEUTICAL WHOLESALER INC. RETURN POLICY</a>
          <a class="list-group-item" target="_blank" href="/NEWSBLOG.pdf">TI,TH,TS ACT</a>
        </div>
      </div>
      <div class="col-sm-6 mt30">
        <div class="list-group list-g-common">
          <a class="list-group-item" target="_blank" href="/ADDENDUM & AGREEMENT.pdf">ADDENDUM & AGREEMENT</a>
          <a class="list-group-item" target="_blank" href="/new app for new customer.pdf">NEW CUSTOMER APPLICATION</a>
          <a class="list-group-item" target="_blank" href="<?php echo $this->Html->url('/pages/index/about_us');?>">About Us</a>
        </div>
      </div>
    </div>
    <!-- // -->
    <div class="row">
      <div class="col-sm-12 mt10">
        <div class="list-group list-g-common current text-center">
          <a class="list-group-item" target="_blank" href="http://www.fda.gov/downloads/Drugs/GuidanceComplianceRegulatoryInformation/Guidances/UCM453225.pdf">FDA Publishes New Guidance Delaying Dispenser 3T Requirements Until March 1, 2016</a>
        </div>
      </div>
    </div>

  </div>
</section>



<footer class="footer">
  <span>&copy;<?php echo date("Y")?> JD Pharmaceutical Inc. All rights reserverd</span>
</footer>

    </section>

<!-- /container --> 
 <!-- set up the modal to start hidden and fade in and out -->
<div id="login_alaert_modal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- dialog body -->
      <div class="modal-body" style="padding-bottom:0px">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       <strong style="color:#eb7228;font-size:18px">ATTENTION ALL CUSTOMERS</strong>
      </div>
      <!-- dialog buttons -->
      <div class="modal-footer text-center"><p class="text-left">EARN ADDITIONAL 1% ON BRANDS AND 3% ON GENERICS BY PURCHASING 10 OR MORE OF AN ITEM - THIS SPECIAL IS ONLY ON ITEMS THAT HAS 10 OR MORE AVAILABLE IN STOCK</p></div>
    </div>
  </div>
</div>

<?php echo $this->Html->script(array('main','/css/bootstrap/js/bootstrap.min','plugins'));?>
<?php echo $this->Html->script(array('main','/css/bootstrap/js/bootstrap-notify'));?>
<?php echo $this->Html->script(array('jquery.validation.functions'));?>
<?php echo $this->Html->script(array('jquery.validate.min'));?>
<?php echo $this->Html->script(array('jquery-ui-1.9.2.datepicker.custom.min'));?>
<?php echo $this->Html->script(array("bootstrap.min,bootstrap.min"))?>
<!-- sometime later, probably inside your on load event callback -->
<?php echo $this->Html->script(array('carousel'));?>
 <?php  if($this->Session->read('Auth.User.fname') and ($this->Session->check('Auth.User.client_alert'))){ ?>
 <?php if($this->Session->read('Auth.User.client_alert')=='false'){?>
<script>
    $(document).ready(function(e) {
        $("#login_alaert_modal").modal({                    // wire up the actual modal functionality and show the dialog
      "backdrop"  : "static",
      "keyboard"  : true,
      "show"      : true                     // ensure the modal is shown immediately
    });
    });
    
    
</script>
<?php }}?>
<script>
      !function ($) {
        $(function(){
          // carousel demo
          $('#myCarousel').carousel()
        })
      }(window.jQuery)
    </script>

</body>
</html>
