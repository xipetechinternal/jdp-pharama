<?php $this->set('title_for_layout', __('Users',true)); ?>
<?php $this->Html->addCrumb(__('Users',true), '/admin/users/index',array('tag'=>'li'));  ?>
<?php $this->Html->addCrumb($this->Html->tag('li',__('Add',true),array('class'=>'active')),false,false); ?>

<div class="row-fluid">
<div class="block">
<div class="navbar navbar-inner block-header">
<div class="muted pull-left"><?php echo __('User'); ?></div>
</div>
<div class="block-content collapse in">
<div class="span12">
<?php echo $this->Form->create('User',array('action' => 'admin_add','class' => 'form-horizontal'));?>
<fieldset>
<legend><?php echo __('Add User'); ?></legend>

<?php
echo $this->Form->input('name', array('div'=>'control-group',
'before'=>' <label class="control-label">'.__('Name').' : </label><div class="controls">', 
'after'=>$this->Form->error('name', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));


echo $this->Form->input('email', array('div'=>'control-group', 
'before'=>' <label class="control-label">'.__('Email').' : </label><div class="controls">',
'after'=>$this->Form->error('email', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));
echo $this->Form->input('password', array('div'=>'control-group', 
'before'=>' <label class="control-label">'.__('Password').' : </label><div class="controls">',
'after'=>$this->Form->error('password', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));
echo $this->Form->input('password2', array('div'=>'control-group', 'type'=>'password', 
'before'=>' <label class="control-label">'.__('Confirm Password').' : </label><div class="controls">',
'after'=>$this->Form->error('password2', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));
echo $this->Form->input('group_id', array('div'=>'control-group',  
'before'=>' <label class="control-label">'.__('Group').' : </label><div class="controls">', 
'after'=>'</div>','label'=>false, 'class'=>'input-xlarge focused','disabled'=>true,'selected'=>array('0'=>1)));

echo $this->Form->input('status', array('div'=>'control-group', 
'before'=>' <label class="control-label">'.__('Status').' : </label><div class="controls">',
'after'=>'</div>','label'=>false, 'class'=>'uniform_on','type'=>'checkbox'));
?>                                        

<div class="form-actions">
<?php echo $this->Form->submit(__('Submit'), array('class'=>'btn btn-primary', 'div'=>false));?>&nbsp;
<?php echo $this->Form->reset(__('Cancel'), array('class'=>'btn', 'div'=>false));?>
</div>

</fieldset>
<?php echo $this->Form->end();?>

</div>
</div>
</div>
</div>