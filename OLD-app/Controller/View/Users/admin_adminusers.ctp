<div class="col-lg-12">
<h4><a href="<?php echo $this->Html->url('/admin/users');?>">Home</a> :: Manage Admin User</h4>
      <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Admin User(s)</h3>
      </div>
      <div class="panel-body" style="margin:15px">

    
           <?php echo $this->Form->create('User',array('action'=>'admin_adminusers','type'=>'file', 'class'=>'form-horizontal login-from')); ?>

           <div class="form-group">
             <?php echo $this->Form->input('username',array('placeholder' => 'Username',
        'label' => 'Username',
        'class'=>'form-control'));?> 
            </div>
<div <?php if($id!=NULL){?> style="display:none" <?php } ?> id="passwordid">
            <div class="form-group">
              <?php echo $this->Form->input('password',array('placeholder' => 'Password',
        'label' => 'Password',
        'class'=>'form-control'));?> 
            </div>
    
               <div class="form-group">
               <?php echo $this->Form->input('password2',array('placeholder' => 'Confirm Password',
        'label' => 'Confirm Password',
		'type'=>'password',
		'required'=>'required',
        'class'=>'form-control'));?> 
            </div>
         </div>
            <div class="form-group">
              <?php echo $this->Form->input('fname',array('placeholder' => 'First Name',
        'label' => 'First Name',
        'class'=>'form-control'));?> 
            </div>
            
             <div class="form-group">
               <?php echo $this->Form->input('email',array('placeholder' => 'Email',
        'label' => 'Email',
        'class'=>'form-control'));?> 
            </div>
            
            <div class="form-group">
              <?php echo $this->Form->input('phone',array('placeholder' => 'Phone',
        'label' => 'Phone',
        'class'=>'form-control'));?> 
            </div>
            
             <div class="form-group">
             <?php echo $this->Form->input('address',array('placeholder' => 'Address',
        'label' => 'Address',
        'class'=>'form-control'));?> 
          <?php echo $this->Form->input('level',array('type' => 'hidden','value'=>'admin'));?> 
        <?php echo $this->Form->input('status',array('type' => 'hidden','value'=>'1'));?> 
            </div>
            
            
             
            
            <div class="gap20"></div>


 <div class="form-actions">
 <?php echo $this->Form->input('id');?>
  <?php echo $this->Form->input('createddt',array('type' => 'hidden','value'=>date("Y-m-d H:i:s")));?>
  <?php echo $this->Form->input('active',array('type' => 'hidden','value'=>'Y'));?> 
<?php echo $this->Form->submit(__('Submit'), array('class'=>'btn btn-primary', 'div'=>false));?>&nbsp;
<?php echo $this->Form->reset(__('Cancel'), array('class'=>'btn btn-primary', 'div'=>false));?>
<?php if($id!=NULL){?>
&nbsp;<?php echo $this->Form->reset(__('Change Password'), array('value'=>'Change Password','class'=>'btn btn-primary','onclick'=>"$('#passwordid').show();",'type'=>'button','div'=>false));?>
<?php } ?>
</div>

</fieldset>
<?php echo $this->Form->end();?>
      </div>
    </div>


</div>

<div class="col-lg-12 text-center">

      <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Sales Login</h3>
      </div>
      <div class="panel-body">
       <div class="table-responsive">
             

            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                        <th><?php echo __('Email');?></th>
                        <th><?php echo __('Username');?></th>
                        <th><?php echo __('Phone');?></th> 
                        <th><?php echo __('Address');?></th> 
                        <th><?php echo __('Created');?></th>
                        <th><?php echo __('Actions');?></th>
                    </tr>
                </thead>
                
                <tbody>
                
				<?php 
				$i=0;
				foreach ($users as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				?>
                <tr <?php echo $class;?> >               
                <td><?php echo h($user['User']['email']); ?>&nbsp;</td>
                <td><?php echo h($user['User']['username']); ?>&nbsp;</td>
                <td><?php echo h($user['User']['phone']); ?>&nbsp;</td>
                <td><?php echo h($user['User']['address']); ?>&nbsp;</td>
                <td class="center"><?php echo h(date("m-d-Y h:i A",strtotime($user['User']['createddt']))); ?>&nbsp; </td>
                <td class="center">
                
                
                
               
             
             
                
				               
               
                
                <?php echo $this->Html->link($this->Html->tag('i','Edit',array('class'=>'icon-pencil icon-white')),array('action' => 'admin_adminusers', $user['User']['id']),array("class"=>"btn btn-primary btn-mini tooltip-top","data-original-title"=>"Edit",'escape'=>false)); ?>
                
                <?php echo $this->Form->postLink($this->Html->tag('i','Delete',array('class'=>'icon-remove icon-white')), array('action' => 'admin_admin_users_delete', $user['User']['id']), array("class"=>"btn btn-primary btn-mini tooltip-top","data-original-title"=>"Delete",'escape'=>false), __('Are you sure you want to delete # %s?', $user['User']['id'])); ?>
                
                
                
                
 				</td>
                </tr>
                <?php endforeach; ?>                
                </tbody>
            </table>
            <div class="pagination pagination-right pagination-mini">
	  <ul>
		<?php echo $this->Paginator->prev(''); ?>
		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
		<?php echo $this->Paginator->next(''); ?>
	  </ul>
	</div>
       </div>
      </div>
    </div>


</div>
<script>
$(document).ready(function(){
	$("#UserAdminAdminusersForm").validate({

		rules: {
			"data[User][password]":{
				required: true,
				minlength:5,

				},
			"data[User][password2]":{
				required: true,
				minlength:5,
				equalTo: "#UserPassword"

				}
		},
		messages: {
			"data[User][password]":"Please check this field",
			"data[User][password2]":{equalTo:"make sure both match"}
		},
		errorElement:"span"

		});
});
</script>