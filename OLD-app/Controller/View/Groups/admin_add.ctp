<?php $this->set('title_for_layout', __('Groups',true)); ?>
<?php $this->Html->addCrumb(__('Groups',true), '/admin/groups/index',array('tag'=>'li'));  ?>
<?php $this->Html->addCrumb($this->Html->tag('li',__('Add',true),array('class'=>'active')),false,false); ?>
<div class="row-fluid">
<div class="block">
<div class="navbar navbar-inner block-header">
<div class="muted pull-left"><?php echo __('Groups'); ?></div>
</div>
<div class="block-content collapse in">
<div class="span12">
<?php echo $this->Form->create('Group',array('action' => 'admin_add','class' => 'form-horizontal'));?>
<fieldset>
<legend><?php echo __('Add Group'); ?></legend>
<?php
echo $this->Form->input('name', array('div'=>'control-group',
'before'=>'<label class="control-label">'.__('Name').'</label><div class="controls">',
'after'=>$this->Form->error('name', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));
?>
<div class="form-actions">
<?php echo $this->Form->submit(__('Submit'), array('class'=>'btn btn-primary', 'div'=>false));?>&nbsp;
<?php echo $this->Form->reset(__('Cancel'), array('class'=>'btn', 'div'=>false));?>
</div>
</fieldset>
<?php echo $this->Form->end();?>
</div>
</div>
</div>
</div>