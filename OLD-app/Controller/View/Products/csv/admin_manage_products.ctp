<?php echo $this->Html->script('https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js', array('inline' => true));?>
   <?php
      echo $this->Html->css('https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css');
    ?>
<div class="col-lg-12">
<h4><a href="<?php echo $this->Html->url('/admin/users');?>">Home</a> :: Manage Products Info</h4>
      <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Products Info</h3>
      </div>
      <div class="panel-body" style="margin:15px">

    
           <?php echo $this->Form->create('ProductInfo',array('url'=>array('action'=>'admin_manage_products','controller'=>'Products'),'type'=>'file', 'class'=>'form-horizontal login-from')); ?>
			 <div class="form-group">
              <?php echo $this->Form->input('product_ndc',array('placeholder' => 'Product NDC',
        'label' => '',		
        'class'=>'form-control'));?> 
            </div>
           <div class="form-group">
             <?php echo $this->Form->input('prodid',array('options' =>$products,
			 'empty'=>array(''=>'--Select Product Name--'),
        'label' => '',
		'required'=>'required',
        'class'=>'form-control'));?>
            </div>

            <div class="form-group">
              <?php echo $this->Form->input('batchno',array('placeholder' => 'Lot No',
        'label' => '',
		'required'=>'required',
        'class'=>'form-control'));?> 
            </div>
    
               <div class="form-group">
               <?php echo $this->Form->input('expdate',array('placeholder' => 'Expiry Date',
        'label' => '',
		'type'=>'text',
		'required'=>'required',	
        'class'=>'form-control'));?> 
            </div>
         
            
            
             <div class="form-group">
               <?php echo $this->Form->input('availability',array('placeholder' => 'Product Availability',
        'label' => '',
		'required'=>'required',
        'class'=>'form-control'));?> 
            </div>
            
                      
            
            
             
            
            <div class="gap20"></div>


 <div class="form-actions">
 <?php echo $this->Form->input('id',array('type'=>'hidden'));?>
<?php echo $this->Form->submit(__('Submit'), array('class'=>'btn btn-primary', 'div'=>false));?>&nbsp;
<?php echo $this->Form->reset(__('Cancel'), array('class'=>'btn', 'div'=>false));?>
</div>

</fieldset>
<?php echo $this->Form->end();?>
      </div>
    </div>


</div>

<div class="col-lg-12 text-right" style="padding:20px"><a class="btn btn-info btn-mini btn-left-margin" href="<?php echo $this->Html->url(array_merge(array('controller'=>'products','action'=>'manage_productscsv'),$this->params['named'],array(''))).'/Reporte-'.date('m-d-Y-His-A').'.csv'; ?>"><i class="icon-file-text"></i> Download Excel/CSV</a></div>
<div class="col-lg-12 text-center">

      <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Product List</h3>
      </div>
      <div class="panel-body">
       <div class="table-responsive">
             

            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                        <th><?php echo __('Sl no.');?></th>
                         <th><?php echo __('Product NDC');?></th>
                          <th><?php echo __('Item Number');?></th>
                        <th><?php echo __('Product Details');?></th>
                        <th><?php echo __('Lot no');?></th> 
                        <th><?php echo __('Expiry');?></th> 
                        <th><?php echo __('Quantity');?></th>
                        <th><?php echo __('Actions');?></th>
                    </tr>
                </thead>
                
                <tbody>
                
				<?php 
				$i=0;
				
				foreach ($users as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				?>
                <tr <?php echo $class;?> > 
                <td><?php echo $i; ?></td>
                <td><?php echo h($user['Product']['product_ndc']);  ?></td> 
                <td><?php echo h($user['Product']['itemnumber']); ?></td>                             
                <td>Product Name <?php echo h($products[$user['ProductInfo']['prodid']]); ?>&nbsp </td>
                <td><?php echo h($user['ProductInfo']['batchno']); ?>&nbsp;</td>
                <td><?php echo h(date("Y-m-d",strtotime($user['ProductInfo']['expdate']))); ?>&nbsp;</td>
                <td><?php echo h($user['ProductInfo']['availability']); ?>&nbsp;</td>                
                <td class="center"> <?php echo $this->Html->link($this->Html->tag('i','Edit',array('class'=>'icon-pencil icon-white')),array('action' => 'admin_manage_products', $user['ProductInfo']['id']),array("class"=>"btn btn-primary btn-mini tooltip-top","data-original-title"=>"Edit",'escape'=>false)); ?>
                
                <?php echo $this->Form->postLink($this->Html->tag('i','Delete',array('class'=>'icon-remove icon-white')), array('action' => 'admin_manageproducts_delete', $user['ProductInfo']['id']), array("class"=>"btn btn-danger btn-mini tooltip-top","data-original-title"=>"Delete",'escape'=>false), __('Are you sure you want to delete # %s?',$products[$user['ProductInfo']['prodid']])); ?>
              
 				</td>
                </tr>
                <?php endforeach; ?>                
                </tbody>
            </table>
            <div class="pagination pagination-right pagination-mini">
	  <ul>
		<?php echo $this->Paginator->prev(''); ?>
		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
		<?php echo $this->Paginator->next(''); ?>
	  </ul>
	</div>
       </div>
      </div>
    </div>


</div>
<script>
$(document).ready(function(){
	$("#ProductInfoAdminManageProductsForm").validate({
		rules: {
			"data[ProductInfo][availability]":{
				required: true,
				number:true,
				min: 1

				}
		},
		messages: {
			"data[ProductInfo][availability]":"Please enter a valid number",
			
		},
		errorElement:"span"});
	$( "#ProductInfoExpdate" ).datepicker({dateFormat: 'yy-mm-dd' ,minDate: 0 }); 
	
	$( "#ProductInfoProductNdc" ).autocomplete(
	{
		source:<?php echo $products_jshon;?>,
		select: function( event, ui ) {
			$( "#ProductInfoProductNdc" ).val( ui.item.label  );
			$( "#ProductInfoProdid" ).val( ui.item.value  );
			return false;
		}
	}).data( "autocomplete" )._renderItem = function( ul, item ) {
		return $( "<li></li>" )
			.data( "item.autocomplete", item )
			.append( "<a><strong>" + item.label + "</strong> </a>" )
			.appendTo( ul );
		};		
});


	

   $('.push').click(function(){
	   
	   var essay_id = $(this).attr('id');
	   $('#myModal').modal('show');
		$("#ProductInfoupdateId").val(essay_id);
		$("#product_name").html($(this).data('prodcutname'));
		$('#mymodal').show();
		$( "#ProductInfoupdateExpdate" ).datepicker({dateFormat: 'yy-mm-dd' ,minDate: 0 });
		$("#ProductInfoupdateAdminManageProductsForm").validate({
		rules: {
			"data[ProductInfoupdate][availability]":{
				required: true,
				number:true,
				min: 1

				}
		},
		messages: {
			"data[ProductInfo][availability]":"Please enter a valid number",
			
		},
		errorElement:"span"});
		
	   });

   
    



</script>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Update Product</h4>
      </div>
      <div class="modal-body">
        <div class="col-lg-12">

      <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Add Quantity</h3>
      </div>
      <div class="panel-body">

 <?php echo $this->Form->create('ProductInfoupdate',array('url'=>array('action'=>'admin_products_updated','controller'=>'Products'),'type'=>'file', 'class'=>'login-from')); ?>
      <div class="form-group">
             
              
           
           <span id="product_name">test</span>
          <?php echo $this->Form->input('id',array('type'=>'hidden'));?>
  
              </div>
            
            <div class="gap20"></div>
            <div class="form-group">
               <?php echo $this->Form->input('batchno',array('placeholder' => 'Lot No',
        'label' => '',
		'required'=>'required',
        'class'=>'form-control'));?> 
            
            </div>

                   
            <div class="gap20"></div>

            <div class="form-group">
              <?php echo $this->Form->input('expdate',array('placeholder' => 'Expiry Date',
        'label' => '',
		'type'=>'text',
		'required'=>'required',	
        'class'=>'form-control'));?> 

            </div>

                              
            <div class="gap20"></div>

            <div class="form-group">
              <?php echo $this->Form->input('availability',array('placeholder' => 'Product Availability',
        'label' => '',
		'required'=>'required',
        'class'=>'form-control'));?> 

            </div>
            
           

            
            <div class="gap20"></div>


 <?php echo $this->Form->submit(__('Update'), array('class'=>'btn btn-success', 'div'=>false));?>&nbsp;


          <?php echo $this->Form->end();?>
      </div>
    </div>


</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       
      </div>
    </div>
  </div>
</div>