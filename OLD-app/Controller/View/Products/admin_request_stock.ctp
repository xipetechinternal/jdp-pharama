<div class="col-lg-12">
<h4><a href="<?php echo $this->Html->url('/admin/users');?>">Home</a> :: Request stock shold</h4>
      


</div>
<!--<div class="col-lg-12 text-right" style="padding:10px"><a class="btn btn-info btn-mini btn-left-margin" href="<?php echo $this->Html->url(array_merge(array('controller'=>'products','action'=>'threshold_productcsv'),$this->params['named'],array(''))).'/Reporte-'.date('m-d-Y-His-A').'.csv'; ?>"><i class="icon-file-text"></i> Download Excel/CSV</a></div>-->
<div class="col-lg-12 text-right" style="padding:10px"><a class="btn btn-primary btn-mini btn-left-margin" href="<?php echo $this->Html->url(array_merge(array('controller'=>'products','action'=>'request_stockcsv'),$this->params['named'],array(''))).'/Request-'.date('m-d-Y-His-A').'.csv'; ?>"><i class="icon-file-text"></i> Download Excel/CSV</a></div>
<div class="col-lg-12 text-center">

      <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Request stock shold</h3>
      </div>
      <div class="panel-body">
       <div class="table-responsive">
             

            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                        
                         <th><?php echo __('Product NDC');?></th>
                          <th><?php echo __('Item Number');?></th>
                        <th><?php echo __('Product Name');?></th>
                        <th><?php echo __('Client');?></th>
                        <th><?php echo __('Quantity');?></th>
                        <th><?php echo __('Requested ON');?></th>
                       <th><?php echo __('Actions');?></th>
                    </tr>
                </thead>
                
                <tbody>
                
				<?php 
				$i=0;
				
				foreach ($users as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				?>
                <tr <?php echo $class;?> > 
                        
               
               
                <td><?php echo h($user['Product']['product_ndc']);  ?></td>  
                <td><?php echo h($user['Product']['itemnumber']); ?></td>    
                <td><?php echo h($user['Product']['productname']); ?> &nbsp;</td>
                <td><?php echo h($user['User']['fname']); ?></td> 
                <td><?php echo h($user['Productrequest']['quantity']); ?></td>    
                <td><?php echo h($user['Productrequest']['created']); ?> &nbsp;</td>
               <td class="center text-left"> 
				
                
                <?php echo $this->Form->postLink($this->Html->tag('i','Delete',array('class'=>'icon-remove icon-white')), array('action' => 'admin_request_stock_delete', $user['Productrequest']['id']), array("class"=>"btn btn-danger btn-mini tooltip-top","data-original-title"=>"Delete",'escape'=>false), __('Are you sure you want to delete # %s?',$products[$user['ProductInfo']['itemnumber']])); ?><br />
				<?php echo $this->Form->postLink($this->Html->tag('i','Announce',array('class'=>'icon-remove icon-white')), array('action' => 'admin_request_stock_announce', $user['Productrequest']['id']), array("class"=>"btn btn-primary btn-mini tooltip-top","data-original-title"=>"Delete",'escape'=>false), __('Are you sure you want to Announce # %s?',$products[$user['ProductInfo']['itemnumber']])); ?>
              
 				</td>
                </tr>
                <?php endforeach; ?>                
                </tbody>
            </table>
            <div class="pagination pagination-right pagination-mini">
	  <ul>
		<?php echo $this->Paginator->prev(''); ?>
		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
		<?php echo $this->Paginator->next(''); ?>
	  </ul>
	</div>
       </div>
      </div>
    </div>


</div>

 
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
      </div>
      </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

