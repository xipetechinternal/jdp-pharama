<div class="col-lg-12  row">
<div class="col-lg-6">
<h4><a href="<?php echo $this->Html->url('/admin/users');?>">Home</a> :: Order Information</h4>   
</div>
<div class="col-lg-11 text-right">
<a href="javascirpit:;" onclick="history.go(-1);" class="btn btn-primary">Back</a>
&nbsp;
<br />
</div>
</div>
<div class="col-lg-12 text-center">

      <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Order List</h3>
      </div>
      <div class="panel-body">
       <div class="table-responsive">
              
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                      <th><?php echo __('Customer');?></th>
                        <th><?php echo __('Product NDC');?></th>
                          <th><?php echo __('Item Number');?></th>
                        <th><?php echo __('Product Name');?></th>
                        
                        <th><?php echo __('Quantity');?></th> 
                        <th><?php echo __('Original Price');?></th>
                        <th><?php echo __('Special Discount');?></th>                         
                       <th><?php echo __('Price After Discount');?></th>
                       <th><?php echo __('Status');?></th>
                       <th><?php echo __('Action');?></th>  
                       
                    </tr>
                </thead>
                 <?php if(empty($Orderdetail)): ?>
		<tr>
			<td colspan="7" class="striped-info">No record found.</td>
		</tr>
		<?php endif;  ?>
                <tbody>
                
				<?php 
				
				$i=0;
				$product_price=0;
				
				foreach ($Orderdetail[0]['OrderdetailMany'] as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				
				$product_qauantity=count($user['OrderdetailMany']);
				
				$priice=0;
				
					$priice=($user['price']*$user['quantity']);
				
				$product_price=$product_price+$priice;
				$orginal_price=($user['original_price']>0)?($user['original_price']*$user['quantity']):$priice;
				?>
                <tr <?php echo $class;?> > 
                     
               <td><?php echo h($Orderdetail[0]['User']['fname']); ?> &nbsp;</td>
                 <td><?php echo h($user['Product']['product_ndc']);  ?></td>  
                <td><?php echo h($user['Product']['itemnumber']); ?></td>  
                <td><?php echo h($user['Product']['productname']); ?> &nbsp;</td>
               
               
                             
               <td> <?php echo h($user['quantity']); ?>
                    &nbsp;</td>
                    <td> <?php echo h($orginal_price); ?></td>
                     <td> <?php echo h($user['special_discount']); ?>%</td>
                    <td>$<?php echo $priice ?> </td> 
                     <td>InProgress </td>  
                     <td> &nbsp; <a href="#myModal"  rel="chatusr"  id="<?php echo $user['id']?>" class="btn push btn-primary" data-toggle="modal">View</a> 
                     <?php if(!$user['special_discount']>0){?>
                     <a href="#myModaldiscount"  rel="chatusr" data-orderid="<?php echo $user['id']?>"  id="discount<?php echo $user['id']?>" class="btn  btn-primary pushdiscount" data-toggle="modal">Special Discount</a>
                     <?php }else{
                       echo $this->Form->postLink($this->Html->tag('i','Cancel Special Discount',array('class'=>'icon-remove icon-white')), array('action' => 'orderinfo_dicount_reset', $user['id']), array("class"=>"btn btn-danger btn-mini tooltip-top","data-original-title"=>"Cancel Special Discount",'escape'=>false), __('Are you sure to Cancel?',$user['special_discount']));
					 }?>
                      </td>   
                </tr>
                <?php endforeach; ?>                
                </tbody>
                <tfoot>
          <tr>
          
            <th colspan="5" style="text-align:right">Total:</th>
            <th>$<?php echo $product_price?></th>
          </tr>
        </tfoot>
            </table>
            <div class="pagination pagination-right pagination-mini">
	  <ul>
		<?php echo $this->Paginator->prev(''); ?>
		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
		<?php echo $this->Paginator->next(''); ?>
	  </ul>
	</div>
       </div>
       <div class="clearfix"></div>
<div class="gap20"></div>
	<?php if($Orderdetail[0]['Order']['status']=='Con'){?>
     <?php echo $this->Form->postLink($this->Html->tag('i','Confirm & Assgin Product(s)',array('class'=>'icon-remove icon-white')), array('action' => 'cnfirm_order', $Orderdetailid), array("class"=>"btn btn-success btn-mini tooltip-top","data-original-title"=>"Confirm & Assgin Product(s)",'escape'=>false), __('Are you sure you confirm?',$Orderdetailid)); ?>
 &nbsp;&nbsp;
  
   <?php echo $this->Form->postLink($this->Html->tag('i','Test',array('class'=>'icon-remove icon-white')), array('action' => 'test_order', $Orderdetailid), array("class"=>"btn btn-warning btn-mini tooltip-top","data-original-title"=>"Test Product(s) Order",'escape'=>false), __('Are you sure to Test?',$Orderdetailid)); ?>
 
  
    <?php echo $this->Form->postLink($this->Html->tag('i','Cancel',array('class'=>'icon-remove icon-white')), array('action' => 'cancel_order', $Orderdetailid), array("class"=>"btn btn-danger btn-mini tooltip-top","data-original-title"=>"Cancel Product(s) Order",'escape'=>false), __('Are you sure to Cancel?',$Orderdetailid)); ?>

 <?php } ?>
       
      </div>
    </div>


</div>

 
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close btn-primary" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Order Detail</h4>
      </div>
      <div class="modal-body">
        <div class="something"> </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myModaldiscount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Special Discount</h4>
      </div>
      <div class="modal-body">
        <div class="something1"><?php echo $this->Form->create('products',array('action'=>'orderinfo_dicount','type'=>'file', 'class'=>'login-from')); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <?php echo $this->Form->input('orderdetailid',array('placeholder' => 'orderdetailid',
        'label' => '',
		'required'=>'required',
		'type'=>'hidden',
        'class'=>'form-control'));?>
    
 
  <tr>
    <td align="left" valign="top" width="50%">Discount% :   <?php echo $this->Form->input('discount',array('placeholder' => 'Discount',
        'label' => '',
		'required'=>'required',		
        'class'=>'form-control input input-small'));?><div class="gap20"></div></td>
  </tr>

 <tr>
    <td align="left" valign="top"> <?php echo $this->Form->submit(__('Apply Discount'), array('class'=>'btn btn-primary', 'div'=>false));?>&nbsp;</td>
  </tr>
  
  
</table>
<?php echo $this->Form->end();?> </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script>
$(function(){

   $('.push').click(function(){
      var essay_id = $(this).attr('id');
	   
		
$.ajax({
          type : 'post',
           url : "<?php echo $this->webroot .'admin/'. $this->params["controller"]; ?>/getlot_details",
		   data:'order_id='+ essay_id,    
                     // in php you should use $_POST['post_id'] to get this value 
       success : function(r)
           {
              // now you can show output in your modal 
              $('#mymodal').show();  // put your modal id 
             $('.something').show().html(r);
           }
    });

      
	$('#myModaldiscount').on('hidden', function() {
    $(this).removeData('modal');
});  

});

$('.pushdiscount').click(function(e) {
	 e.preventDefault();
     var orderid = $(this).data('orderid');
	 $("#productsOrderdetailid").val(orderid);
	 $('#myModaldiscount').show();
});

   });
</script>