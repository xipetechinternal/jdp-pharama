<div class="col-lg-10  main">
         <h4><a href="dashboard.php">Home</a> :: Brand Sold Reports</h4>
         </div>
<div class="clearfix"></div>
 <div class="gap20"></div>
           <?php echo $this->Form->create('ProductInfo',array('url'=>array('action'=>'admin_brand_sold','controller'=>'products'),'type'=>'get', 'class'=>'form-horizontal login-from')); ?>

<div class="col-lg-10  ">

  <div class="form-group">
  <div class="col-lg-2"><label for="exampleInputEmail1">Select Date:</label></div>
  <div class="col-lg-8"> <div class="form-group">
  <div class="col-lg-4">   <?php echo $this->Form->input('selfrmdate',array('placeholder' => 'From Date',
        'label' => false,
		'required'=>'required',
        'class'=>'form-control'));?> 
   
  </div><div class="col-lg-4"><?php echo $this->Form->input('seltodate',array('placeholder' => 'To Date',
        'label' => false,
		'required'=>'required',
        'class'=>'form-control'));?>  </div>
  <div class="col-lg-4"> <?php echo $this->Form->button('<i class="fa fa-search"></i> Send', array(
    'type' => 'submit',
    'class' => 'btn btn-primary',
    'escape' => false
));?></div>
  <div class="clearfix"></div>
  <div class="col-lg-6">
 
  </div>
  
            </div></div>

  </div>
  <?php echo $this->Form->end();?>
  
  <div class="gap20"></div>
</div>
<div class="clearfix"></div>
<div class="col-lg-12 text-right" style="padding:20px">
<?PHP $searcset=(isset($this->request->data['ProductInfo']['selfrmdate']) and isset($this->request->data['ProductInfo']['seltodate']))?"?selfrmdate=".$this->request->data['ProductInfo']['selfrmdate']."&seltodate=".$this->request->data['ProductInfo']['seltodate']:'';?>

         &nbsp;&nbsp;<a class="btn btn-primary btn-mini btn-left-margin" href="<?php echo $this->Html->url(array_merge(array('controller'=>'products','action'=>'admin_brand_sold_excel'),$this->params['named'],array(''))).'/'.$searcset; ?>"><i class="icon-file-text"></i> Download Excel/CSV</a></div>

			<div class="table-responsive">
              
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                    	<!--<th><?php echo __('Order Date');?></th>-->
                      <th><?php echo __('Customer');?></th>
                        <th><?php echo __('Product NDC');?></th>
                          <th><?php echo __('Item Number');?></th>
                        <th><?php echo __('Product Name');?></th>                        
                        <th><?php echo __('Quantity');?></th> 
                        <!--<th><?php echo __('Single Itme Sold Price $');?></th>
                        <th><?php echo __('Total Itme Sold Price $');?></th>                         
                       <th><?php echo __('Single Itme Original Price $');?></th>
                       <th><?php echo __('Total Itme Original Price $');?></th>
                       <th><?php echo __('Prof/Loss Per Item');?></th>
                       <th><?php echo __('Total Prof/Loss');?></th>  -->
                       
                    </tr>
                </thead>
                 <?php if(empty($order_list)): ?>
		<tr>
			<td colspan="12" class="striped-info">No record found.</td>
		</tr>
		<?php endif;  ?>
                <tbody>
                
				<?php 
				
				$i=0;
				$temarray=array();
				$product_price=0;
		foreach ($order_list as $Orderdetail):	
				foreach ($Orderdetail['OrderdetailMany'] as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				
			if($user['Product']['producttype']=='Brand'){
			if (!in_array($user['Product']['product_ndc'], $temarray)) {
				$temarray[]=$user['Product']['product_ndc'];
				$orginal_price=($user['original_price']>0)?($user['original_price']*$user['quantity']):$priice;
				?>
                <tr <?php echo $class;?> > 
                     
              <!-- <td><?php echo h(date("m-d-Y",strtotime($Orderdetail['OrderDist']['createddt']))); ?> &nbsp;</td>-->
                 <td><?php echo h($Orderdetail['User']['fname']);  ?></td> 
                 <td><?php echo h($user['Product']['product_ndc']); ?></td> 
                <td><?php echo h($user['Product']['itemnumber']); ?></td>  
                <td><?php echo h($user['Product']['productname']); ?> &nbsp;</td>                            
               <td> <?php echo h($TMP_ARRAY[$user['Product']['product_ndc']]); ?>&nbsp;</td>
              <!-- <td> $<?php echo h($user['price']); ?></td>
               <td> $<?php echo h(round($user['price']*$TMP_ARRAY[$user['Product']['product_ndc']],2)); ?>%</td>
               <td>$<?php echo round($user['original_p_price'],2) ?> </td> 
               <td>$<?php echo round($user['original_p_price']*$user['quantity'],2) ?> </td>
               <td>$<?php echo round($user['price']-$user['original_p_price'],2) ?> </td>
               <td>$<?php echo round(($user['price']*$user['quantity'])-($user['quantity']*$user['original_p_price']),2) ?> </td>  -->
                                    </tr>
               <?php }}?>
                <?php endforeach; ?>
              <?php endforeach; ?>                
                </tbody>
                <tfoot>
          
        </tfoot>
            </table>
            <div class="pagination pagination-right pagination-mini">
	  
	</div>
       </div>



   <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
      </div>
      </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->   
      
      <script>
$(document).ready(function(){
	$("#ProductInfoAdminReportMonthlyForm").validate();
	//$( "#ProductInfoSelfrmdate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
   // $( "#ProductInfoSeltodate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
	
	
    $( "#ProductInfoSelfrmdate, #ProductInfoSeltodate" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
       dateFormat: 'yy-mm-dd' ,
        onSelect: function( selectedDate ) {
            if(this.id == 'ProductInfoSelfrmdate'){
              var dateMin = $('#ProductInfoSelfrmdate').datepicker("getDate");
              var rMin = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 1); // Min Date = Selected + 1d
              var rMax = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 365); // Max Date = Selected + 31d
              $('#ProductInfoSeltodate').datepicker("option","minDate",rMin);
              $('#ProductInfoSeltodate').datepicker("option","maxDate",rMax);                    
            }

        }
    });

});
</script>
<style>
.navbar-inner{
	z-index:0;
}
.ui-datepicker{
	z-index:100000 !important;
}
</style>