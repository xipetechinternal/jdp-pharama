<div class="col-lg-12">
<h4><a href="<?php echo $this->Html->url('/admin/users');?>">Home</a> :: Assgin Products</h4>
      


</div>

<div class="col-lg-12 text-center">

      <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Order History</h3>
      </div>
      <div class="panel-body">
       <div class="table-responsive">
              <h2> Placed Orders</h2>

            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                      
                        <th><?php echo __('Customer');?></th>
                        <th><?php echo __('Order');?></th>
                        <th><?php echo __('PO Number');?></th>
                        <th><?php echo __('Quantity');?></th> 
                       <th><?php echo __('Price');?></th> 
                       
                    </tr>
                </thead>
                 <?php if(empty($order_conf)): ?>
		<tr>
			<td colspan="7" class="striped-info">No record found.</td>
		</tr>
		<?php endif;  ?>
                <tbody>
                
				<?php 
				
				$i=0;
				$product_price=0;
				
				foreach ($order_conf as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				
				$product_qauantity=count($user['OrderdetailMany']);
				
				$priice=0;
				foreach($user['OrderdetailMany'] as $orderdeatils){
					$priice=$priice+($orderdeatils['price']*$orderdeatils['quantity']);
				}
				$product_price=$product_price+$priice;
				?>
                <tr <?php echo $class;?> > 
                        
               
                <td><?php echo h($user['User']['fname']); ?> &nbsp;</td>
                <td><?php echo h($user['OrderDist']['tmporderid']); ?> &nbsp;</td>
               <td><?php echo h($user['OrderDist']['po_number']); ?> &nbsp;</td>
               
                             
               <td>  <?php echo $this->Html->link($product_qauantity.' Products', array('controller'=>'products', 'action'=>'orderinfo',$user['OrderDist']['id']),array('class' => ''));?>
                    &nbsp;</td>
                    <td>$<?php echo $priice ?> </td>   
                </tr>
                <?php endforeach; ?>                
                </tbody>
                <tfoot>
          <tr>
          
            <th colspan="3" style="text-align:right">Total:</th>
            <th>$<?php echo $product_price?></th>
          </tr>
        </tfoot>
            </table>
            <div class="pagination pagination-right pagination-mini">
	  <ul>
		<?php echo $this->Paginator->prev(''); ?>
		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
		<?php echo $this->Paginator->next(''); ?>
	  </ul>
	</div>
       </div>
       <div class="clearfix"></div>
<div class="gap20"></div>
<h2> Pending Orders</h2>
       <div class="table-responsive">
              <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                       
                        <th><?php echo __('Customer');?></th>
                        <th><?php echo __('Order');?></th>
                        <th><?php echo __('PO Number');?></th>
                        <th><?php echo __('Quantity');?></th> 
                       <th><?php echo __('Price');?></th> 
                       
                    </tr>
                </thead>
                 <?php if(empty($order_pending)): ?>
		<tr>
			<td colspan="7" class="striped-info">No record found.</td>
		</tr>
		<?php endif;  ?>
                <tbody>
                
				<?php 
				
				$i=0;
				$product_price=0;
				foreach ($order_pending as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				
				$product_qauantity=count($user['OrderdetailMany']);
				
				$priice=0;
				foreach($user['OrderdetailMany'] as $orderdeatils){
					$priice=$priice+($orderdeatils['price']*$orderdeatils['quantity']);
				}
				$product_price=$product_price+$priice;
				?>
                <tr <?php echo $class;?> > 
                        
               
               <td><?php echo h($user['User']['fname']); ?> &nbsp;</td>
                <td><?php echo h($user['Order']['tmporderid']); ?> &nbsp;</td>
               <td><?php echo h($user['Order']['po_number']); ?> &nbsp;</td>
               
                             
               <td>  <?php echo $this->Html->link($product_qauantity.' Products', array('controller'=>'products', 'action'=>'orderinfo',$user['Order']['id']),array('class' => ''));?>
                    &nbsp;</td>
                    <td>$<?php echo $priice ?> </td>   
                </tr>
                <?php endforeach; ?>                
                </tbody>
                <tfoot>
          <tr>
          
            <th colspan="3" style="text-align:right">Total:</th>
            <th>$<?php echo $product_price?></th>
          </tr>
        </tfoot>
            </table></div>
       
      </div>
    </div>


</div>

 
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
      </div>
      </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

