<?php $this->set('title_for_layout', __('Profile',true)); ?>
<?php $this->Html->addCrumb($this->Html->tag('li',__('Profile',true),array('class'=>'active')),false,false); ?>
<div class="row-fluid">
<div class="block">
<div class="navbar navbar-inner block-header">
<div class="muted pull-left"><?php echo __('Profile'); ?></div>
</div>
<div class="block-content collapse in">
<div class="span12">
<?php echo $this->Form->create('Account',array('action' => 'admin_password','class' => 'form-horizontal'));?>
<fieldset>
<legend><?php echo __('Edit Password'); ?></legend>

<?php


echo $this->Form->input('User.id');
echo $this->Form->input('User.name',array('type' => 'hidden'));
echo $this->Form->input('User.email',array('type' => 'hidden'));
echo $this->Form->input('User.password', array('div'=>'control-group', 
'before'=>' <label class="control-label">'.__('Password').' : </label><div class="controls">',
'after'=>$this->Form->error('User.password', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));

echo $this->Form->input('User.password2', array('div'=>'control-group', 'type'=>'password', 
'before'=>' <label class="control-label">'.__('Confirm Password').' : </label><div class="controls">',
'after'=>$this->Form->error('User.password2', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));

?>                                        

<div class="form-actions">
<?php echo $this->Form->submit(__('Submit'), array('class'=>'btn btn-primary', 'div'=>false));?>&nbsp;
<?php echo $this->Form->reset(__('Cancel'), array('class'=>'btn', 'div'=>false));?>
</div>

</fieldset>
<?php echo $this->Form->end();?>

</div>
</div>
</div>
</div>

