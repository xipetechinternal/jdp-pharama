
<div class="row">
  <div class="col-lg-4">
    <div class="panel panel-default" style="margin-top:-100px">
      <div class="panel-body form-bg">
        <?php 
	   echo $this->Session->flash(); 
	   echo $this->Session->flash('auth');
	   //echo $this->Session->flash('auth'); ?>
       <?php echo $this->Form->create(null, array(
    'url' => array('controller' => 'Sales', 'action' => 'login')
));
?>
       
        <h4 class="orgtxt"><?php echo __('Secure Sales  Login'); ?></h4>
        <?php
    echo $this->Form->input('User.username', array('div'=>'clearfix',
        'before'=>'<label>'.__('Username').'</label><div class="input">',
        'after'=>$this->Form->error('User.username', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
        'error' => array('attributes' => array('style' => 'display:none')),
        'label'=>false, 'class'=>'form-control','placeholder'=>'Enter username'));
    echo $this->Form->input('User.password', array('div'=>'clearfix',
        'before'=>'<label>'.__('Password').'</label><div class="input">',
        'after'=>$this->Form->error('User.password', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
        'error' => array('attributes' => array('style' => 'display:none')),
        'label'=>false, 'class'=>'form-control','placeholder'=>'Enter password'));
    ?>
        <div class="checkbox">
          <label>Remember Me
            <input type="checkbox">
          </label>
        </div>
        <?php echo $this->Form->submit(__('Submit'), array('class'=>'btn btn-warning', 'div'=>false));?> <?php echo $this->Form->reset(__('Cancel'), array('class'=>'btn btn-warning', 'div'=>false));?> <?php echo $this->Form->end(); ?>
        
        
        <?php
		echo $this->Html->link(
    $this->Html->tag('span', '', array('class' => 'icon new')) . "Forgot your password?",
    array('controller' => 'Sales', 'action' => 'forgot_password'),
    array('class' => 'btn btn-default', 'escape' => false)
);
?>
 <?php
		echo $this->Html->link(
    $this->Html->tag('span', '', array('class' => 'icon new')) . "Forgot your username?",
    array('controller' => 'Sales', 'action' => 'forgot_username'),
    array('class' => 'btn btn-default', 'escape' => false)
);
?>
          </div>
    </div>
  </div>
</div>
<script type="text/javascript">
          $(document).ready(function(){
	$("#UserLoginForm").validate();
		
	
});
        </script>