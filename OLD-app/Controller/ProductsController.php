<?php

/**

 * Static content controller.

 *

 * This file will render views from views/pages/

 *

 * PHP 5

 *

 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)

 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)

 *

 * Licensed under The MIT License

 * Redistributions of files must retain the above copyright notice.

 *

 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)

 * @link          http://cakephp.org CakePHP(tm) Project

 * @package       app.Controller

 * @since         CakePHP(tm) v 0.2.9

 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)

 */
App::uses('CakeTime', 'Utility');
App::uses('AppController', 'Controller');

App::uses('CakeEmail', 'Network/Email');

/**

 * Static content controller

 *

 * Override this controller by placing a copy in controllers directory of an application

 *

 * @package       app.Controller

 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html

 */
class ProductsController extends AppController {

    public $components = array('RequestHandler', 'Export');
    public $uses = array('User', 'EmailTemplateDescription', 'Order', 'Orderdetail', 'Products', 'ProductInfo', 'Product', 'OrderLot', 'Productrequest', 'Pedigree', 'ProductActivity', 'OrderDist');
    public $paginate = array('limit' => 10);

    function beforeFilter() {

        parent::beforeFilter();

        $this->layout = "default";

        $this->Auth->allow('admin_login', 'admin_andapedigree');

        $this->Auth->userScope = array('User.group_id' => 1);

        $this->Auth->loginAction = '/admin/users/login';

        $this->Auth->logoutRedirect = '/admin/users/login';

        $this->Auth->loginRedirect = array('plugin' => false, 'controller' => 'users', 'action' => 'index', 'admin' => true);

        $this->User->bindModel(array('belongsTo' => array(
                'Group' => array(
                    'className' => 'Group',
                    'foreignKey' => 'group_id',
                    'dependent' => true
                )
            )), false);
    }

    public function admin_manage_productscsv($csvFile = null) {
        $data = $this->ProductInfo->find('all', array('conditions' => array("Product.active !='False'")));
        // print_r($data);die;
        $csvdata = array();
        foreach ($data as $key => $dt) {
            if ($dt['ProductInfo']['availability'] > 0) {
                $csvdata[$key]['Product NDC'] = $dt['Product']['product_ndc'];
                $csvdata[$key]['Item Number'] = $dt['Product']['itemnumber'];
                $csvdata[$key]['Product Name'] = $dt['Product']['productname'];
                $csvdata[$key]['Lot no'] = $dt['ProductInfo']['batchno'];
                $csvdata[$key]['Expiry'] = $dt['ProductInfo']['expdate'];
                $csvdata[$key]['Quantity'] = $dt['ProductInfo']['availability'];
                $csvdata[$key]['Vendor Name'] = $dt['ProductInfo']['VendorName'];
                $csvdata[$key]['Invoice No'] = $dt['ProductInfo']['InvoiceNo'];
                $csvdata[$key]['Received Date'] = $dt['ProductInfo']['ReceivedDate'];
            }
        }
        $this->Export->exportCsv($csvdata);
    }

    public function admin_manage_products($id = null) {

        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));
        $products_product_ndcs = $this->Products->find("list", array('conditions' => array("Products.active !='False'"), 'fields' => array('product_ndc')));
        $products_jshon = array();
        $i = 0;
        foreach ($products_product_ndcs as $key => $products_product_ndc) {
            $products_jshon[$i]['label'] = $products_product_ndc;
            $products_jshon[$i]['value'] = $key;
            $i++;
        }
        $this->set('products_jshon', json_encode($products_jshon));
        $this->set('products', $this->Products->find("list", array('conditions' => array("Products.active !='False'"), 'fields' => array('productname'))));
        $this->ProductInfo->id = $id;


        if ($this->request->is('put') || $this->request->is('post')) {

            //count previous quenty
            $previous_quentity = 0;
            if ($id != null) {

                $previousquentit = $this->ProductInfo->find('first', array(
                    'conditions' => array('ProductInfo.id' => $id),
                    'fields' => array('availability')
                ));
                $previous_quentity = $previousquentit['ProductInfo']['availability'];
            }
            $this->ProductInfo->create();

            if ($this->ProductInfo->save($this->request->data)) {


                //Add product update acitvity
                if ($previous_quentity != $this->request->data['ProductInfo']['availability']) {
                    $activit_data = array(
                        'product_id' => $this->request->data['ProductInfo']['prodid'],
                        'product_info_id' => $this->ProductInfo->id,
                        'user_id' => $this->Auth->user('id'),
                        'previous_qty' => $previous_quentity,
                        'new_qty' => $this->request->data['ProductInfo']['availability'],
                        'note' => $this->request->data['ProductInfo']['Note'],
                        'created' => date("Y-m-d H:i:s")
                    );
                    $this->admin_add_product_activity($activit_data);
                }

                $this->Session->setFlash(__('The Lot has been Added'), 'admin_success');
                $this->redirect(array('action' => 'admin_manage_products'));
            } else {

                $this->Session->setFlash(__('The Product could not be saved. Please, try again.'), 'admin_error');
            }
        } else {
            $this->request->data = $this->ProductInfo->read(null, $id);
            //$this->request->data['User']['password'] = null;
        }
//Configure::write('debug',2);
        $this->ProductInfo->recursive = 1;
        $search = "";
        if (!isset($this->params['url']['ndcproduct'])) {
            $this->paginate = array(
                'conditions' => array("ProductInfo.availability >'0'", "Product.active!='False'"),
                'order' => array('ProductInfo.createddt' => 'desc'),
                'limit' => 10
            );
        } else {
            $search_text = trim($this->params['url']['ndcproduct']);
            //ndc divide multi condtion
            $search_text_c1 = substr($search_text, 1, -1);
            $search_text_c2 = substr($search_text, 1);
            $search_text_c3 = substr($search_text, 0, -1);
            $search_text_c3 = '0' . $search_text;
            $search_text_c4 = $this->insertAtPosition($search_text, 0, 5);
            $search_text_c5 = $this->insertAtPosition($search_text, 0, 9);
            $search_text_c6 = '0' . $search_text_c1;
            $search_text_c7 = $this->insertAtPosition($search_text_c2, 0, 5);
            $search_text_c8 = $this->insertAtPosition($search_text_c2, 0, 9);
            $search_text_c9 = $this->insertAtPosition($search_text_c1, 0, 9);
            $search_text_c10 = $this->insertAtPosition($search_text_c1, 0, 5);

            $this->paginate = array(
                'conditions' => array("ProductInfo.availability >'0'", "Product.active!='False'", "OR" => array("itemnumber like '%" . $this->params['url']['ndcproduct'] . "%'", "product_ndc" => array($search_text, $search_text_c1, $search_text_c2, $search_text_c3, $search_text_c4, $search_text_c5, $search_text_c6, $search_text_c7, $search_text_c8, $search_text_c9, $search_text_c10), "productname like '%" . $this->params['url']['ndcproduct'] . "%'", "batchno like '%" . $this->params['url']['ndcproduct'] . "%'", "Product.producttype like '%" . $this->params['url']['ndcproduct'] . "%'")),
                'order' => array('ProductInfo.createddt' => 'desc'),
                'limit' => 10
            );
            $search = $this->params['url']['ndcproduct'];
        }
        //print_r($this->paginate("ProductInfo"));
        $this->set('search', $search);
        $this->set('users', $this->paginate("ProductInfo"));
    }

    public function admin_recived_reports_excel($id = null) {


        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));


        $search = "";
        if (!isset($this->params['url']['ndcproduct'])) {
            $this->paginate = array(
                // 'conditions' => array("ProductInfo.availability >'0'" ),
                'order' => array('Product.name' => 'desc'),
                'limit' => 10
            );
            $this->set('users', array());
        } else {
            $search_text = trim($this->params['url']['ndcproduct']);
            //ndc divide multi condtion
            $search_text_c1 = substr($search_text, 1, -1);
            $search_text_c2 = substr($search_text, 1);
            $search_text_c3 = substr($search_text, 0, -1);
            $search_text_c3 = '0' . $search_text;
            $search_text_c4 = $this->insertAtPosition($search_text, 0, 5);
            $search_text_c5 = $this->insertAtPosition($search_text, 0, 9);
            $search_text_c6 = '0' . $search_text_c1;
            $search_text_c7 = $this->insertAtPosition($search_text_c2, 0, 5);
            $search_text_c8 = $this->insertAtPosition($search_text_c2, 0, 9);
            $search_text_c9 = $this->insertAtPosition($search_text_c1, 0, 9);
            $search_text_c10 = $this->insertAtPosition($search_text_c1, 0, 5);

            $this->paginate = array(
                'conditions' => array("OR" => array("product_ndc" => array($search_text, $search_text_c1, $search_text_c2, $search_text_c3, $search_text_c4, $search_text_c5, $search_text_c6, $search_text_c7, $search_text_c8, $search_text_c9, $search_text_c10), "productname like '%" . $this->params['url']['ndcproduct'] . "%'", "batchno like '%" . $this->params['url']['ndcproduct'] . "%'", "itemnumber like '%" . $this->params['url']['ndcproduct'] . "%'")),
                'order' => array('Product.name' => 'desc'),
                'limit' => 10
            );
            $search = $this->params['url']['ndcproduct'];

            $Orderdetail = $this->OrderLot->find('all', array('recursive' => 2, 'order' => array('OrderLot.create_date' => 'desc'), 'fields' => array('id', 'product_id', 'quantity', 'lot_no', 'price', 'productinfoid', 'create_date', 'user_id'), 'conditions' => array("OR" => array("Product.product_ndc" => array($search_text, $search_text_c1, $search_text_c2, $search_text_c3, $search_text_c4, $search_text_c5, $search_text_c6, $search_text_c7, $search_text_c8, $search_text_c9, $search_text_c10), "Product.productname like '%" . $this->params['url']['ndcproduct'] . "%'", "ProductInfo.batchno like '%" . $this->params['url']['ndcproduct'] . "%'", "Product.itemnumber like '%" . $this->params['url']['ndcproduct'] . "%'"))));
            $csv_data = array();
            foreach ($Orderdetail as $key => $ord) {
                $users = $this->User->find('first', array('recursive' => 1, 'fields' => array('id', 'fname', 'lname'), 'conditions' => array("User.id" => $ord['OrderLot']['user_id'])));
                $csv_data[$key]['Product NDC'] = $ord['Product']['product_ndc'];
                $csv_data[$key]['Item Number'] = $ord['Product']['itemnumber'];
                $csv_data[$key]['Product Name'] = $ord['Product']['productname'];
                $csv_data[$key]['Vendor Name'] = $ord['ProductInfo']['VendorName'];
                $csv_data[$key]['Lot#'] = $ord['ProductInfo']['batchno'];
                $csv_data[$key]['Original Quantity'] = $ord['ProductInfo']['balance'];
                $csv_data[$key]['Purchase Date'] = date("m-d-Y", strtotime($ord['ProductInfo']['ReceivedDate']));
            }
            $this->Export->exportCsv($csv_data);
            $this->set('users', $Orderdetail);
        }

        //print_r($Orderdetail);die;
        $this->set('search', $search);
        //$this->set('users', $this->paginate("ProductInfo"));
    }

    public function admin_recived_reports($id = null) {

        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));


        $search = "";
        if (!isset($this->params['url']['ndcproduct'])) {
            $this->paginate = array(
                // 'conditions' => array("ProductInfo.availability >'0'" ),
                'order' => array('Product.name' => 'desc'),
                'limit' => 10
            );
            $this->set('users', array());
        } else {
            $search_text = trim($this->params['url']['ndcproduct']);
            //ndc divide multi condtion
            $search_text_c1 = substr($search_text, 1, -1);
            $search_text_c2 = substr($search_text, 1);
            $search_text_c3 = substr($search_text, 0, -1);
            $search_text_c3 = '0' . $search_text;
            $search_text_c4 = $this->insertAtPosition($search_text, 0, 5);
            $search_text_c5 = $this->insertAtPosition($search_text, 0, 9);
            $search_text_c6 = '0' . $search_text_c1;
            $search_text_c7 = $this->insertAtPosition($search_text_c2, 0, 5);
            $search_text_c8 = $this->insertAtPosition($search_text_c2, 0, 9);
            $search_text_c9 = $this->insertAtPosition($search_text_c1, 0, 9);
            $search_text_c10 = $this->insertAtPosition($search_text_c1, 0, 5);
            $this->paginate = array(
                'conditions' => array("OR" => array("product_ndc" => array($search_text, $search_text_c1, $search_text_c2, $search_text_c3, $search_text_c4, $search_text_c5, $search_text_c6, $search_text_c7, $search_text_c8, $search_text_c9, $search_text_c10), "productname like '%" . $this->params['url']['ndcproduct'] . "%'", "batchno like '%" . $this->params['url']['ndcproduct'] . "%'", "itemnumber like '%" . $this->params['url']['ndcproduct'] . "%'")),
                'order' => array('Product.name' => 'desc'),
                'limit' => 10
            );
            $search = $this->params['url']['ndcproduct'];
            $this->set('users', $this->paginate("ProductInfo"));
        }
        $this->set('search', $search);
        //$this->set('users', $this->paginate("ProductInfo"));
    }

    public function admin_sold_reports($id = null) {

        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));


        $search = "";
        if (!isset($this->params['url']['ndcproduct'])) {
            $this->paginate = array(
                // 'conditions' => array("ProductInfo.availability >'0'" ),
                'order' => array('Product.name' => 'desc'),
                'limit' => 10
            );
            $this->set('users', array());
        } else {
            $search_text = trim($this->params['url']['ndcproduct']);
            //ndc divide multi condtion
            $search_text_c1 = substr($search_text, 1, -1);
            $search_text_c2 = substr($search_text, 1);
            $search_text_c3 = substr($search_text, 0, -1);
            $search_text_c3 = '0' . $search_text;
            $search_text_c4 = $this->insertAtPosition($search_text, 0, 5);
            $search_text_c5 = $this->insertAtPosition($search_text, 0, 9);
            $search_text_c6 = '0' . $search_text_c1;
            $search_text_c7 = $this->insertAtPosition($search_text_c2, 0, 5);
            $search_text_c8 = $this->insertAtPosition($search_text_c2, 0, 9);
            $search_text_c9 = $this->insertAtPosition($search_text_c1, 0, 9);
            $search_text_c10 = $this->insertAtPosition($search_text_c1, 0, 5);

            $this->paginate = array(
                'conditions' => array("OR" => array("product_ndc" => array($search_text, $search_text_c1, $search_text_c2, $search_text_c3, $search_text_c4, $search_text_c5, $search_text_c6, $search_text_c7, $search_text_c8, $search_text_c9, $search_text_c10), "productname like '%" . $this->params['url']['ndcproduct'] . "%'", "batchno like '%" . $this->params['url']['ndcproduct'] . "%'", "itemnumber like '%" . $this->params['url']['ndcproduct'] . "%'")),
                'order' => array('Product.name' => 'desc'),
                'limit' => 10
            );
            $search = $this->params['url']['ndcproduct'];

            $Orderdetail = $this->OrderLot->find('all', array('recursive' => 2, 'order' => array('OrderLot.create_date' => 'desc'), 'fields' => array('id', 'product_id', 'quantity', 'lot_no', 'price', 'productinfoid', 'create_date', 'user_id'), 'conditions' => array("OR" => array("Product.product_ndc" => array($search_text, $search_text_c1, $search_text_c2, $search_text_c3, $search_text_c4, $search_text_c5, $search_text_c6, $search_text_c7, $search_text_c8, $search_text_c9, $search_text_c10), "Product.productname like '%" . $this->params['url']['ndcproduct'] . "%'", "ProductInfo.batchno like '%" . $this->params['url']['ndcproduct'] . "%'", "Product.itemnumber like '%" . $this->params['url']['ndcproduct'] . "%'"))));
            foreach ($Orderdetail as $key => $ord) {
                $users = $this->User->find('first', array('recursive' => 1, 'fields' => array('id', 'fname', 'lname'), 'conditions' => array("User.id" => $ord['OrderLot']['user_id'])));
                $Orderdetail[$key]['User'] = $users['User'];
            }
            $this->set('users', $Orderdetail);
        }

        // print_r($Orderdetail);
        $this->set('search', $search);
        //$this->set('users', $this->paginate("ProductInfo"));
    }

    public function admin_sold_reports_excel($id = null) {

        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));


        $search = "";
        if (!isset($this->params['url']['ndcproduct'])) {
            $this->paginate = array(
                // 'conditions' => array("ProductInfo.availability >'0'" ),
                'order' => array('Product.name' => 'desc'),
                'limit' => 10
            );
            $this->set('users', array());
        } else {
            $search_text = trim($this->params['url']['ndcproduct']);
            //ndc divide multi condtion
            $search_text_c1 = substr($search_text, 1, -1);
            $search_text_c2 = substr($search_text, 1);
            $search_text_c3 = substr($search_text, 0, -1);
            $search_text_c3 = '0' . $search_text;
            $search_text_c4 = $this->insertAtPosition($search_text, 0, 5);
            $search_text_c5 = $this->insertAtPosition($search_text, 0, 9);
            $search_text_c6 = '0' . $search_text_c1;
            $search_text_c7 = $this->insertAtPosition($search_text_c2, 0, 5);
            $search_text_c8 = $this->insertAtPosition($search_text_c2, 0, 9);
            $search_text_c9 = $this->insertAtPosition($search_text_c1, 0, 9);
            $search_text_c10 = $this->insertAtPosition($search_text_c1, 0, 5);

            $this->paginate = array(
                'conditions' => array("OR" => array("product_ndc" => array($search_text, $search_text_c1, $search_text_c2, $search_text_c3, $search_text_c4, $search_text_c5, $search_text_c6, $search_text_c7, $search_text_c8, $search_text_c9, $search_text_c10), "productname like '%" . $this->params['url']['ndcproduct'] . "%'", "batchno like '%" . $this->params['url']['ndcproduct'] . "%'", "itemnumber like '%" . $this->params['url']['ndcproduct'] . "%'")),
                'order' => array('Product.name' => 'desc'),
                'limit' => 10
            );
            $search = $this->params['url']['ndcproduct'];

            $Orderdetail = $this->OrderLot->find('all', array('recursive' => 2, 'order' => array('OrderLot.create_date' => 'desc'), 'fields' => array('id', 'product_id', 'quantity', 'lot_no', 'price', 'productinfoid', 'create_date', 'user_id'), 'conditions' => array("OR" => array("Product.product_ndc" => array($search_text, $search_text_c1, $search_text_c2, $search_text_c3, $search_text_c4, $search_text_c5, $search_text_c6, $search_text_c7, $search_text_c8, $search_text_c9, $search_text_c10), "Product.productname like '%" . $this->params['url']['ndcproduct'] . "%'", "ProductInfo.batchno like '%" . $this->params['url']['ndcproduct'] . "%'", "Product.itemnumber like '%" . $this->params['url']['ndcproduct'] . "%'"))));
            $csv_data = array();
            foreach ($Orderdetail as $key => $ord) {
                $users = $this->User->find('first', array('recursive' => 1, 'fields' => array('id', 'fname', 'lname'), 'conditions' => array("User.id" => $ord['OrderLot']['user_id'])));
                $csv_data[$key]['Product NDC'] = $ord['Product']['product_ndc'];
                $csv_data[$key]['Item Number'] = $ord['Product']['itemnumber'];
                $csv_data[$key]['Product Name'] = $ord['Product']['productname'];
                $csv_data[$key]['Vendor Name'] = $ord['ProductInfo']['VendorName'];
                $csv_data[$key]['Lot#'] = $ord['ProductInfo']['batchno'];
                $csv_data[$key]['Original Quantity'] = $ord['ProductInfo']['balance'];
                $csv_data[$key]['Purchase Quantity'] = $ord['ProductInfo']['availability'];
                $csv_data[$key]['Customer'] = $users['User']['fname'] . '' . $users['User']['fname'];
                $csv_data[$key]['Purchase Date'] = date("m-d-Y", strtotime($ord['OrderLot']['create_date']));
            }
            $this->Export->exportCsv($csv_data);
            $this->set('users', $Orderdetail);
        }

        print_r($Orderdetail);
        die;
        $this->set('search', $search);
        //$this->set('users', $this->paginate("ProductInfo"));
    }

    public function admin_lot_history($id = null) {

        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));


        $search = "";
        if (!isset($this->params['url']['ndcproduct'])) {
            $this->paginate = array(
                // 'conditions' => array("ProductInfo.availability >'0'" ),
                'order' => array('Product.name' => 'desc'),
                'limit' => 10
            );
            $this->set('users', array());
        } else {
            $search_text = trim($this->params['url']['ndcproduct']);
            //ndc divide multi condtion
            $search_text_c1 = substr($search_text, 1, -1);
            $search_text_c2 = substr($search_text, 1);
            $search_text_c3 = substr($search_text, 0, -1);
            $search_text_c3 = '0' . $search_text;
            $search_text_c4 = $this->insertAtPosition($search_text, 0, 5);
            $search_text_c5 = $this->insertAtPosition($search_text, 0, 9);
            $search_text_c6 = '0' . $search_text_c1;
            $search_text_c7 = $this->insertAtPosition($search_text_c2, 0, 5);
            $search_text_c8 = $this->insertAtPosition($search_text_c2, 0, 9);
            $search_text_c9 = $this->insertAtPosition($search_text_c1, 0, 9);
            $search_text_c10 = $this->insertAtPosition($search_text_c1, 0, 5);
            $this->paginate = array(
                'conditions' => array("OR" => array("product_ndc" => array($search_text, $search_text_c1, $search_text_c2, $search_text_c3, $search_text_c4, $search_text_c5, $search_text_c6, $search_text_c7, $search_text_c8, $search_text_c9, $search_text_c10), "productname like '%" . $this->params['url']['ndcproduct'] . "%'", "batchno like '%" . $this->params['url']['ndcproduct'] . "%'", "itemnumber like '%" . $this->params['url']['ndcproduct'] . "%'")),
                'order' => array('Product.name' => 'desc'),
                'limit' => 10
            );
            $search = $this->params['url']['ndcproduct'];
            $this->set('users', $this->paginate("ProductInfo"));
        }
        $this->set('search', $search);
        $this->set('users', $this->paginate("ProductInfo"));
    }

    public function admin_manageproducts_delete($id = null) {

        if (!$this->request->is('post')) {

            throw new MethodNotAllowedException();
        }

        $this->ProductInfo->id = $id;

        if (!$this->ProductInfo->exists()) {

            throw new NotFoundException(__('Invalid user'));
        }

        $Orderdetail = $this->OrderLot->find('count', array('conditions' => array('OrderLot.productinfoid' => $id)));
        if ($Orderdetail > 0) {

            $this->Session->setFlash(__('Product ifno was not deleted because this lot already alloted'), 'admin_error');

            $this->redirect(array('action' => 'admin_manage_products'));
        }

        //count previous quenty
        $previous_quentity = 0;
        if ($id != null) {

            $previousquentit = $this->ProductInfo->find('first', array(
                'conditions' => array('ProductInfo.id' => $id),
                'fields' => array('availability')
            ));
            $previous_quentity = $previousquentit['ProductInfo']['availability'];
        }

        if ($this->ProductInfo->delete()) {

            //Add product update acitvity
            if ($previous_quentity != 0) {
                $activit_data = array(
                    'product_info_id' => $id,
                    'user_id' => $this->Auth->user('id'),
                    'previous_qty' => $previous_quentity,
                    'new_qty' => 0,
                    'created' => date("Y-m-d H:i:s")
                );
                $this->admin_add_product_activity($activit_data);
            }

            $this->Session->setFlash(__('Product Information deleted'), 'admin_success');

            $this->redirect(array('action' => 'admin_manage_products'));
        }

        $this->Session->setFlash(__('Product ifno was not deleted'), 'admin_error');

        $this->redirect(array('action' => 'admin_manage_products'));
    }

    public function insertAtPosition($string, $insert, $position) {
        return implode($insert, str_split($string, $position));
    }

    public function admin_add_products($id = NULL) {
        //Configure::write('debug', 2);
        $this->layout = "admin_dashboard";
        $product_action = ($id == NULL) ? 'Created' : 'Updated';
        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));

        $this->Products->id = $id;


        if ($this->request->is('put') || $this->request->is('post')) {

            //$user_active = $this->Products->find('first',array('conditions'=>array('Products.product_ndc'=>$this->request->data['Products']['product_ndc'])));
            if ($id == NULL) {
                $item_first_charter = substr($this->request->data['Products']['producttype'], 0, 1);
                $itemnumber = $this->Products->find('first', array('order' => array(
                        'Products.itemnumber' => 'desc'), 'conditions' => array("Products.itemnumber like '" . $item_first_charter . "%'")));
                if (!empty($itemnumber)) {
                    $remove_first = substr($itemnumber['Products']['itemnumber'], 1);
                    $next_itmenumber = (int) $remove_first + 1;
                } else {
                    $next_itmenumber = 222;
                }
                $this->request->data['Products']['itemnumber'] = $item_first_charter . $next_itmenumber;
            }else{
				
				if($this->request->data['Products']['update_price']!=''){
				$this->loadModel('ProductPriceChange');
					$pricedata['ProductPriceChange']=array(
							'product_id'=>$this->request->data['Products']['id'],
							'WAC'=>$this->request->data['Products']['WAC'],
							'Old_Extended_Price'=>$this->request->data['Products']['original_price'],
							'Extended_Price'=>$this->request->data['Products']['productprice'],
							'Purchase_Price'=>$this->request->data['Products']['update_price'],
							'Invoice'=>$this->request->data['Products']['invoice_update'],
							'Vendor'=>$this->request->data['Products']['vendor_update'],
							'MODIFIED_BY'=>$this->Auth->user('id'),
							'MODIFIED_DATE'=>date("Y-m-d H:i:s")							
					);
					//print_r($pricedata);die;
					$this->ProductPriceChange->save($pricedata);
					
					$this->request->data['Products']['original_price']=$this->request->data['Products']['update_price'];
					
				}
				
			}
            //echo $user_active.'_'.$this->request->data['Products']['id'].'_'.$iddie;die;
            /* if(count($user_active)>0 and ($id=='' or $user_active['Products']['id']!=$id)){
              $this->Session->setFlash(__('The Prouduct NDC must be unique'), 'admin_error');
              }elseif(count($user_active1)>0 and ($id=='' or $user_active['Products']['id']!=$id)){
              $this->Session->setFlash(__('The Prouduct Item Number must be unique'), 'admin_error');
              }else{ */
			//print_r($this->request->data);die;
            $this->Products->create();

            if ($this->Products->save($this->request->data)) {

                //Add in product activityes
				
                /* $activit_data=array(
                  'product_id'=>$this->Products->id,
                  'user_id'=> $this->Auth->user('id'),
                  'action'=>$product_action,
                  'action_on'=>'Product',
                  'created'=>date("Y-m-d H:i:s")
                  );
                  $this->admin_add_product_activity($activit_data);
                 */
                $this->Session->setFlash(__('The Prouduct has been Added'), 'admin_success');
                $this->redirect(array('action' => 'admin_add_products'));
            } else {
                //print_r($this->Products->validationErrors);
                //print_r($this->Products->getDataSource()->getLog(false, false)); 
                $this->Session->setFlash(__('The Product could not be saved. Please, try again.'), 'admin_error');
            }
            //}
        } else {
            $this->request->data = $this->Products->read(null, $id);
            //$this->request->data['User']['password'] = null;
        }

        $this->Products->recursive = 1;

        $search = "";
        if (!isset($this->params['url']['ndcproduct'])) {
            $this->paginate = array('conditions' => array("Products.active!='False'"), 'order' => array(
                    'Products.createddt' => 'desc'));
        } else {
            /* "Products.active!='False'", */
            $search_text = trim($this->params['url']['ndcproduct']);
            //ndc divide multi condtion
            $search_text_c1 = substr($search_text, 1, -1);
            $search_text_c2 = substr($search_text, 1);
            $search_text_c3 = substr($search_text, 0, -1);
            $search_text_c3 = '0' . $search_text;
            $search_text_c4 = $this->insertAtPosition($search_text, 0, 5);
            $search_text_c5 = $this->insertAtPosition($search_text, 0, 9);
            $search_text_c6 = '0' . $search_text_c1;
            $search_text_c7 = $this->insertAtPosition($search_text_c2, 0, 5);
            $search_text_c8 = $this->insertAtPosition($search_text_c2, 0, 9);
            $search_text_c9 = $this->insertAtPosition($search_text_c1, 0, 9);
            $search_text_c10 = $this->insertAtPosition($search_text_c1, 0, 5);
            $this->paginate = array(
                'conditions' => array("OR" => array("product_ndc" => array($search_text, $search_text_c1, $search_text_c2, $search_text_c3, $search_text_c4, $search_text_c5, $search_text_c6, $search_text_c7, $search_text_c8, $search_text_c9, $search_text_c10), "itemnumber like '%" . trim($this->params['url']['ndcproduct']) . "%'", "productname like '%" . trim($this->params['url']['ndcproduct']) . "%'", "producttype like '%" . trim($this->params['url']['ndcproduct']) . "%'")),
                'order' => array('Products.createddt' => 'desc'),
                'limit' => 10
            );
            $search = $this->params['url']['ndcproduct'];
        }
		if($id!=NULL){
			$this->loadModel('ProductPriceChange');
			$ProductPriceChange = $this->ProductPriceChange->find("all", array('fields'=>array('Old_Extended_Price'),'conditions'=>array('ProductPriceChange.product_id'=>$id),'order' => array('ProductPriceChange.MODIFIED_DATE' => 'desc'),
                'limit' => 2));
				$this->set('ProductPriceChange', $ProductPriceChange);
				//print_r($ProductPriceChange);
		}
        $this->set('id', $id);
        $this->set('users', $this->paginate("Products"));
    }

    public function admin_add_productscsv($csvFile = null) {
        $data = $this->Products->find('all', array('conditions' => array("Products.active !='False'")));

        $csvdata = array();
        foreach ($data as $key => $dt) {
            $product_instock = 0;
            foreach ($dt['ProductInfo'] as $productsinfo) {
                $product_instock = $product_instock + $productsinfo['availability'];
            }

            $csvdata[$key]['Item #'] = $dt['Products']['itemnumber'];
            $csvdata[$key]['Product NDC'] = $dt['Products']['product_ndc'];
            $csvdata[$key]['Product Name'] = $dt['Products']['productname'];

            $csvdata[$key]['Product Type'] = $dt['Products']['producttype'];
            $csvdata[$key]['Product Price'] = $dt['Products']['productprice'];
            $csvdata[$key]['Purchase Price'] = $dt['Products']['PurchasePrice'];
            $csvdata[$key]['Manufacture'] = $dt['Products']['manufacture'];
            $csvdata[$key]['Threshold'] = $dt['Products']['qtyinstock'];
            $csvdata[$key]['IN STOCK'] = $product_instock;
            $csvdata[$key]['WAC'] = $dt['Products']['WAC'];
            $csvdata[$key]['AWP'] = $dt['Products']['awp'];
            $csvdata[$key]['Size'] = $dt['Products']['product_size'];
            $csvdata[$key]['Unit'] = $dt['Products']['product_unit'];
        }
        $this->Export->exportCsv($csvdata);
    }

    public function admin_deactive_products($id = NULL) {

        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));

        $this->Products->id = $id;

        $this->Products->recursive = 1;

        $search = "";
        if (!isset($this->params['url']['ndcproduct'])) {
            $this->paginate = array('conditions' => array('Products.active' => 'False'), 'order' => array(
                    'Products.createddt' => 'desc'));
        } else {
            $search_text = trim($this->params['url']['ndcproduct']);
            //ndc divide multi condtion
            $search_text_c1 = substr($search_text, 1, -1);
            $search_text_c2 = substr($search_text, 1);
            $search_text_c3 = substr($search_text, 0, -1);
            $search_text_c3 = '0' . $search_text;
            $search_text_c4 = $this->insertAtPosition($search_text, 0, 5);
            $search_text_c5 = $this->insertAtPosition($search_text, 0, 9);
            $search_text_c6 = '0' . $search_text_c1;
            $search_text_c7 = $this->insertAtPosition($search_text_c2, 0, 5);
            $search_text_c8 = $this->insertAtPosition($search_text_c2, 0, 9);
            $search_text_c9 = $this->insertAtPosition($search_text_c1, 0, 9);
            $search_text_c10 = $this->insertAtPosition($search_text_c1, 0, 5);
            $this->paginate = array(
                'conditions' => array("OR" => array("product_ndc" => array($search_text, $search_text_c1, $search_text_c2, $search_text_c3, $search_text_c4, $search_text_c5, $search_text_c6, $search_text_c7, $search_text_c8, $search_text_c9, $search_text_c10), "itemnumber like '%" . $this->params['url']['ndcproduct'] . "%'", "productname like '%" . $this->params['url']['ndcproduct'] . "%'"), 'and' => array('Products.active' => 'False')),
                'order' => array('Products.createddt' => 'desc'),
                'limit' => 10
            );
            $search = $this->params['url']['ndcproduct'];
        }
        $this->set('search', $search);
        $this->set('users', $this->paginate("Products"));
    }

    public function admin_products_updated() {
        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));

        //print_r($this->request->data);
        $status = $this->ProductInfo->updateAll(
                array('batchno' => $this->request->data['ProductInfoupdate']['batchno'],
            'expdate' => "'" . $this->request->data['ProductInfoupdate']['expdate'] . "'",
            'availability' => $this->request->data['ProductInfoupdate']['availability']), array('ProductInfo.id' => $this->request->data['ProductInfoupdate']['id'])
        );

        if ($status) {



            $this->Session->setFlash(__('The Prouduct has been Updated'), 'admin_success');
            $this->redirect(array('action' => 'admin_manage_products'));
        } else {

            $this->Session->setFlash(__('The Product could not be saved. Please, try again.'), 'admin_error');
        }
    }

    public function admin_productsdetails_updated() {
        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));

        //print_r($this->request->data);
        /* $status=$this->ProductInfo->updateAll(
          array('batchno'=>$this->request->data['ProductInfoupdate']['batchno'],
          'expdate'=>"'".$this->request->data['ProductInfoupdate']['expdate']."'",
          'availability'=>$this->request->data['ProductInfoupdate']['availability']),
          array('ProductInfo.id' => $this->request->data['ProductInfoupdate']['id'])
          ); */
        $this->ProductInfo->create();

        if ($this->ProductInfo->save($this->request->data)) {




            $this->Session->setFlash(__('The Prouduct Lot has been Updated'), 'admin_success');
            $this->redirect(array('action' => 'add_products'));
        } else {

            $this->Session->setFlash(__('The Product could not be saved. Please, try again.'), 'admin_error');
            $this->redirect(array('action' => 'add_products'));
        }
    }

    public function admin_products_delete($id = null) {

        if (!$this->request->is('post')) {

            throw new MethodNotAllowedException();
        }

        $this->Products->id = $id;


        if (!$this->Products->exists()) {

            throw new NotFoundException(__('Invalid user'));
        }

        $status = $this->Products->updateAll(
                array('active' => "'False'"), array('Products.id' => $id)
        );
        if ($status) {

            $this->Session->setFlash(__('Product deactivate'), 'admin_success');

            $this->redirect(array('action' => 'admin_add_products'));
        }
        /* if ($this->Products->delete()) {

          $this->Session->setFlash(__('Product deleted'), 'admin_success');

          $this->redirect(array('action' => 'admin_add_products'));

          }
         */
        $this->Session->setFlash(__('Product was not deleted'), 'admin_error');

        $this->redirect(array('action' => 'admin_add_products'));
    }

    public function admin_products_activate($id = null) {

        if (!$this->request->is('post')) {

            throw new MethodNotAllowedException();
        }

        $this->Products->id = $id;


        if (!$this->Products->exists()) {

            throw new NotFoundException(__('Invalid user'));
        }

        $status = $this->Products->updateAll(
                array('active' => "'True'"), array('Products.id' => $id)
        );
        if ($status) {

            $this->Session->setFlash(__('Product Activate'), 'admin_success');

            $this->redirect(array('action' => 'admin_deactive_products'));
        }

        $this->Session->setFlash(__('Product was not deleted'), 'admin_error');

        $this->redirect(array('action' => 'admin_deactive_products'));
    }

    public function admin_threshold_productcsv($csvFile = null) {
        /* $this->paginate = array(

          'joins' => array(array('table'=> 'product_infos',
          'type' => 'INNER',
          'alias' => 'ProductInfos',
          'conditions' => array('ProductInfos.prodid = Products.id','Products.qtyinstock >=ProductInfos.availability' ))),
          'limit' => 1000000);
          $this->paginate['fields'] = array('Products.id','Products.qtyinstock','Products.product_ndc','Products.productname','Products.producttype','Products.productprice','ProductInfos.batchno','ProductInfos.expdate','ProductInfos.availability','Products.itemnumber');
          $data= $this->paginate("Products"); */

        $allprodcuts = $this->Products->query("select products .*,(select sum(`availability`) from product_infos where `prodid`=products.id) as qnty,(select `VendorName` from product_infos where `prodid`=products.id order by id desc limit 0,1) as VendorName from products where active<>'False' and add_restriction<>1 and qtyinstock>(select COALESCE(sum(`availability`),0) from product_infos where `prodid`=products.id)");
        //print_r($data);die;
        $csvdata = array();
        foreach ($allprodcuts as $key => $dt) {

            $csvdata[$key]['Product NDC'] = $dt['products']['product_ndc'];
            $csvdata[$key]['Item Number'] = $dt['products']['itemnumber'];
            $csvdata[$key]['Product Name'] = $dt['products']['productname'];
            $csvdata[$key]['Vendor Name'] = $dt[0]['VendorName'];
            $csvdata[$key]['Quantity'] = ($dt[0]['qnty'] != '') ? $dt[0]['qnty'] : 0;
            $csvdata[$key]['Thershole'] = $dt['products']['qtyinstock'];
        }
        $this->Export->exportCsv($csvdata);
    }

    public function admin_threshold_product() {
        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));

        //Configure::write('debug',2);
        //$this->paginate = array(
        // 'joins' => array(array('table'=> 'product_infos',
        //  'type' => 'INNER',
        //  'alias' => 'ProductInfos',
        // 'conditions' => array('ProductInfos.prodid = Products.id','Products.qtyinstock >=ProductInfos.availability' ))),
        //  'limit' => 10);
        //$this->paginate['recursive'] = 2;
        //  $this->paginate['Limit'] = 1000;
        //$this->paginate['conditions'] = array("ProductInfo.availability>0");
        //$this->paginate['fields'] = array('Products.id','Products.qtyinstock','Products.product_ndc','Products.productname','Products.producttype','Products.productprice','ProductInfos.batchno','ProductInfos.expdate','ProductInfos.availability','Products.itemnumber');
        $search = "";
        if (!isset($this->params['url']['ndcproduct'])) {
            $allprodcuts = $this->Products->query("select products .*,(select sum(`availability`) from product_infos where `prodid`=products.id) as qnty,(select `VendorName` from product_infos where `prodid`=products.id order by id desc limit 0,1) as VendorName from products where active<>'False' and add_restriction<>1 and qtyinstock>(select COALESCE(sum(`availability`),0) from product_infos where `prodid`=products.id)");
        } else {
            $ndcproduct = $this->params['url']['ndcproduct'];

            $allprodcuts = $this->Products->query("select products .*,(select sum(`availability`) from product_infos where `prodid`=products.id) as qnty,(select `VendorName` from product_infos where `prodid`=products.id order by id desc limit 0,1) as VendorName from products where active<>'False' and add_restriction<>1 and  ( qtyinstock>(select COALESCE(sum(`availability`),0) from product_infos where `prodid`=products.id) and (product_ndc like '%" . $ndcproduct . "%' or productname like '%" . $ndcproduct . "%' or itemnumber like '%" . $ndcproduct . "%'))");

            $search = $this->params['url']['ndcproduct'];
        }
        $this->set('search', $search);
        //$allprodcuts = $this->Products->find('all',array('recursive' => 1,'conditions'=>array("ProductInfo.availability"=>10)));
        //print_r($allprodcuts);
        /*
          $theresholproductlist=array();
          foreach($allprodcuts as $key=>$allprodcut){
          $qyantity=0;
          foreach($allprodcut['ProductInfo'] as $productinof){
          $qyantity=$productinof['availability']+$qyantity;
          if($qyantity<=$allprodcut['qtyinstock']){
          $theresholproductlist[]=$allprodcuts[$key];
          break;
          }
          }
          } */


        $this->set('users', $allprodcuts);
    }

    public function admin_request_stock() {

        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));


        /* $this->paginate = array(
          'conditions' => array('Order.status' => 'Con'),
          'fields' => array('DISTINCT Order.id','Order.tmporderid','Order.userid','Order.createddt','Order.status')

          ); */

        $this->paginate = array('conditions' => array('NOT' => array('Productrequest.status ' => 'ano')), 'order' => array(
                'Productrequest.created' => 'desc'));
        $users = $this->paginate("Productrequest");

        // print_r($users);

        $this->set('users', $users);
    }

    public function admin_request_stock_announce($id) {

        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));



        $this->Productrequest->updateAll(
                array('Productrequest.status' => "'ano'"), array('Productrequest.id' => $id)
        );

        $this->Session->setFlash(__('Product successfully Announced'), 'admin_success');

        $this->redirect(array('action' => 'admin_request_stock'));
    }

    public function admin_request_stock_delete($id = NULL) {

        if (!$this->request->is('post')) {

            throw new MethodNotAllowedException();
        }

        $this->Productrequest->id = $id;

        if (!$this->Productrequest->exists()) {

            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->Productrequest->delete()) {



            $this->Session->setFlash(__('Product Request deleted'), 'admin_success');

            $this->redirect(array('action' => 'admin_request_stock'));
        }

        $this->Session->setFlash(__('Product Request was not deleted'), 'admin_error');

        $this->redirect(array('action' => 'admin_request_stock'));
    }

    public function admin_request_stockcsv() {

        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));


        /* $this->paginate = array(
          'conditions' => array('Order.status' => 'Con'),
          'fields' => array('DISTINCT Order.id','Order.tmporderid','Order.userid','Order.createddt','Order.status')

          ); */

        //$this->paginate = array('order'=>array('Productrequest.created' => 'desc'));
        $users = $this->Productrequest->find("all", array('order' => array('Productrequest.created' => 'desc')));
        $request_product = array();
        foreach ($users as $key => $user) {
            $request_product[$key]["Product NDC"] = $user["Product"]["product_ndc"];
            $request_product[$key]["Item Number"] = $user["Product"]["itemnumber"];
            $request_product[$key]["Product Name"] = $user["Product"]["productname"];
            $request_product[$key]["Client"] = $user["User"]["fname"];
            $request_product[$key]["Quantity"] = $user["Productrequest"]["quantity"];
            $request_product[$key]["Requested ON"] = $user["Productrequest"]["created"];
        }
        $this->Export->exportCsv($request_product);
    }

    public function admin_stocklevel_report() {

        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));



        //$this->paginate['fields'] = array('Products.id','Products.qtyinstock','Products.product_ndc','Products.productname','Products.producttype','Products.productprice','ProductInfos.batchno','ProductInfos.expdate','Sum(ProductInfos.availability)');
        //print_r($this->paginate("Product"));
        $instock = 0;
        $products = $this->paginate("Product");
        foreach ($products as $key => $product) {
            foreach ($product['ProductInfo'] as $ProductInfo) {
                $instock = $instock + $ProductInfo['availability'];
            }
            $products[$key]['Product']['instock'] = $instock;
        }
        //print_r($products);
        $this->set('users', $products);
    }

    public function admin_asgin_products() {
        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));

        $this->paginate = array(
            'conditions' => array('Order.confirmat' => 'Fill')
        );

        $Orderdetail = $this->Order->find('all', array('fields' => array('DISTINCT Order.id', 'Order.tmporderid', 'Order.userid', 'Order.createddt', 'Order.status', 'User.fname', 'Order.po_number'), 'conditions' => array('Order.status' => 'Fill')));
        $this->set('order_pending', $Orderdetail);


        $this->paginate = array(
            'conditions' => array('OrderDist.status' => 'Con'),
            'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'User.fname', 'OrderDist.po_number')
        );
        $order_conf = $this->paginate("OrderDist");

        $this->set('order_conf', $order_conf);
    }

    public function admin_orderinfo($oid) {
		ini_set('memory_limit', '-1');
        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));

        $Orderdetail = $this->Order->find('all', array('recursive' => 2, 'conditions' => array('Order.id' => $oid)));
        $this->set('Orderdetail', $Orderdetail);
        $this->set('Orderdetailid', $oid);
    }

    public function admin_getlot_details() {
        if ($this->request->isAjax()) {
            $this->layout = 'ajax';

            $Orderdetail = $this->OrderLot->find('all', array('fields' => array('id', 'product_id', 'quantity', 'lot_no', 'price', 'productinfoid'), 'conditions' => array('OrderLot.Orderdetailid' => $this->request->data['order_id'])));

            foreach ($Orderdetail as $key => $detail) {
                $prodetajl = $this->ProductInfo->find('all', array('conditions' => array('ProductInfo.id' => $detail['OrderLot']['productinfoid'])));
                $Orderdetail[$key]['product_details'] = $prodetajl;
            }
            $this->set('Orderdetail', $Orderdetail);
        }
    }

    public function admin_cnfirm_order($oid) {
        $this->layout = "admin_dashboard";

        $this->Order->updateAll(
                array('Order.status' => "'Accept'", 'confirmat' => date("Y-m-d")), array('Order.id' => $oid)
        );

        $order_lot = $this->OrderLot->find('all', array('fields' => array('productinfoid', 'orderid', 'Orderdetailid', 'quantity', 'id', 'product_id', 'user_id'), 'conditions' => array('OrderLot.orderid	' => $oid)));
        foreach ($order_lot as $lot) {
            $findoldavai = $this->ProductInfo->find('first', array('conditions' => array('ProductInfo.id' => $lot['OrderLot']['productinfoid'])));
            /* $this->ProductInfo->updateAll(
              array('ProductInfo.availability' => $findoldavai['ProductInfo']['availability']+$lot['OrderLot']['quantity']),
              array('ProductInfo.id' => $lot['OrderLot']['productinfoid'])
              ); */

            $previous_quentity = $findoldavai['ProductInfo']['availability'] + $lot['OrderLot']['quantity'];

            $activit_data = array(
                'product_id' => $lot['OrderLot']['product_id'],
                'product_info_id' => $findoldavai['ProductInfo']['id'],
                'user_id' => $lot['OrderLot']['user_id'],
                'previous_qty' => $previous_quentity,
                'event' => 'Orderdetailid',
                'event_id' => $lot['OrderLot']['Orderdetailid'],
                'new_qty' => $findoldavai['ProductInfo']['availability'],
                'created' => date("Y-m-d H:i:s")
            );
            $this->admin_add_product_activity($activit_data);
        }


        $this->Session->setFlash(__('The Order Successfuly Accepted.'), 'admin_success');

        $this->redirect(array('action' => 'asgin_products'));
    }

    public function admin_test_order($oid) {
        $this->layout = "admin_dashboard";

        $this->Order->updateAll(
                array('Order.status' => "'Test'", 'confirmat' => date("Y-m-d")), array('Order.id' => $oid)
        );


        $order_lot = $this->OrderLot->find('all', array('fields' => array('productinfoid', 'orderid', 'Orderdetailid', 'quantity', 'id', 'product_id'), 'conditions' => array('OrderLot.orderid	' => $oid)));

        foreach ($order_lot as $lot) {
            $findoldavai = $this->ProductInfo->find('first', array('conditions' => array('ProductInfo.id' => $lot['OrderLot']['productinfoid'])));
            $this->ProductInfo->updateAll(
                    array('ProductInfo.availability' => $findoldavai['ProductInfo']['availability'] + $lot['OrderLot']['quantity']), array('ProductInfo.id' => $lot['OrderLot']['productinfoid'])
            );

            //$this->OrderLot->deleteAll(array('OrderLot.tmporderid' => $tmpid,'OrderLot.user_id'=>$this->Auth->user('id')), false);

            /* $activit_data=array(
              'product_info_id'=>$lot['OrderLot']['productinfoid'],
              'user_id'=> $this->Auth->user('id'),
              'action'=>"Lot reAdded after Tested",
              'action_on'=>'lotid',
              'event_id'=>$lot['OrderLot']['id'],
              'created'=>date("Y-m-d H:i:s")
              );
              $this->admin_add_product_activity($activit_data);

              if($previous_product!=$lot['OrderLot']['product_id']){
              $activit_data=array(
              'product_id'=>$lot['OrderLot']['product_id'],
              'user_id'=> $this->Auth->user('id'),
              'action'=>"Order Tested",
              'action_on'=>'orderid',
              'event_id'=>$oid,
              'created'=>date("Y-m-d H:i:s")
              );
              $this->admin_add_product_activity($activit_data);
              } */
        }


        $this->Session->setFlash(__('The Order Successfuly Tested.'), 'admin_success');

        $this->redirect(array('action' => 'asgin_products'));
    }

    public function admin_cancel_order($oid) {
        $this->layout = "admin_dashboard";

        $this->Order->updateAll(
                array('Order.status' => "'Test'", 'confirmat' => date("Y-m-d")), array('Order.id' => $oid)
        );


        $order_lot = $this->OrderLot->find('all', array('fields' => array('productinfoid', 'orderid', 'Orderdetailid', 'quantity', 'id', 'product_id'), 'conditions' => array('OrderLot.orderid	' => $oid)));


        $previous_product = '';
        foreach ($order_lot as $lot) {
            $findoldavai = $this->ProductInfo->find('first', array('conditions' => array('ProductInfo.id' => $lot['OrderLot']['productinfoid'])));
            $this->ProductInfo->updateAll(
                    array('ProductInfo.availability' => $findoldavai['ProductInfo']['availability'] + $lot['OrderLot']['quantity']), array('ProductInfo.id' => $lot['OrderLot']['productinfoid'])
            );

            $this->OrderLot->deleteAll(array('OrderLot.tmporderid' => $tmpid, 'OrderLot.user_id' => $this->Auth->user('id')), false);
            /* if($previous_product!=$lot['OrderLot']['product_id']){
              $activit_data=array(
              'product_id'=>$lot['OrderLot']['product_id'],
              'user_id'=> $this->Auth->user('id'),
              'action'=>"Order Cancel",
              'action_on'=>'orderid',
              'event_id'=>$oid,
              'created'=>date("Y-m-d H:i:s")
              );
              $this->admin_add_product_activity($activit_data);
              }

              $activit_data=array(
              'product_info_id'=>$lot['OrderLot']['productinfoid'],
              'user_id'=> $this->Auth->user('id'),
              'action'=>"Lot reAdded after Order Cancel",
              'action_on'=>'qnty',
              'event_id'=>$lot['OrderLot']['quantity'],
              'created'=>date("Y-m-d H:i:s")
              );
              $this->admin_add_product_activity($activit_data);
             */
        }


        $this->Order->deleteAll(array('Order.id' => $oid), false);

        $this->Session->setFlash(__('The Order Successfuly Cancel.'), 'admin_success');

        $this->redirect(array('action' => 'asgin_products'));
    }

    public function admin_Pedigree_invoice() {
        $this->layout = "admin_dashboard";
        Configure::write('debug', 2);
        $prodetajl = $this->ProductInfo->find('all', array('conditions' => array('ProductInfo.Pedigree' => 'N')));
        //print_r($prodetajl);die;
        $error_array = array();
        $success_array = array();
        foreach ($prodetajl as $key => $details) {
            if ($details['ProductInfo']['InvoiceNo'] != '') {
                $response = $this->admin_Pedigree($details['ProductInfo']['id'], $details['ProductInfo']['InvoiceNo']);
                //print_r($response);
                if ($response['status'] == 'true') {
                    $success_array[] = " INVOICE " . $response['INVOICE'] . " and Lot " . $response['successmsg'];
                } else {
                    $error_array[] = " INVOICE " . $response['INVOICE'];
                }
                sleep(rand(2, 10));
            }
        }



        if (!empty($success_array)) {
            $this->Session->setFlash(__('Added Pedigrees for these  ' . implode(",", $success_array)), 'admin_success');
            $this->redirect(array('action' => 'manage_products'));
        } else {
            $this->Session->setFlash(__("sorry,  could not be Found any New Pedigree on qkrx.com"), 'admin_success');
        }

        if (!empty($error_array)) {
            $this->Session->setFlash(__('sorry,  could not be Found any Pedigree with INVOICE ' . implode(",", $error_array) . '. Please, try again.'), 'admin_error');
        }
        $this->redirect(array('action' => 'manage_products'));
    }

    public function admin_Pedigree($pid, $INVOICE) {
        $this->layout = "admin_dashboard";
        Configure::write('debug', 3);

        App::import('Vendor', 'CURL/Curl');
        $curl = new Curl();
        $curl->setCookie('CSID', '499150');
        $curl->setCookie('pos_pw', 'derou123');
        $curl->get('https://www.qkrx.com/cgi/cgictl.pgm?PGMNAME=BSTSO&MSGOK=MSGOK&OLDNEW=RB2&EMAIL=derou@me.com&PASSWORD=derou123', array());

        if ($curl->error) {
            //echo $curl->error;
            //return array("status"=>"false","INVOICE"=>$INVOICE);
        }

        $curl->get('http://www.qkrx.com/CGI/CGICTL.PGM?PN=BSTDET&IV=' . $INVOICE, array());

        if ($curl->error) {

            //$this->Session->setFlash(__('sorry,  could not be Found any Pedigree with INVOICE '.$INVOICE.'. Please, try again.'), 'admin_error');
            //return array("status"=>"false","INVOICE"=>$INVOICE);
        } else {

            if (preg_match('/Requested Was Not Found/', $curl->response)) {
                //$this->Session->setFlash(__('sorry,  could not be Found any Pedigree with Invoice ID '.$INVOICE.'. Please, try again.'), 'admin_error');
                $this->ProductInfo->id = $pid;
                $this->ProductInfo->saveField('Pedigree', "CH");
                return array("status" => "false", "INVOICE" => $INVOICE);
            } else {
                $data = $curl->response;
                $dom = new DOMDocument();
                @$dom->loadHTML($data); //Lots of invalid html going on so I am suppressing the warnings
                $xpathDom = new DOMXPath($dom);
                $scarping_results = array();
                //$tabledata=$this->dom_find_content($xpath,'//div[@class="span8 offset2"]//table[1]/tbody');							
                //$studentNodes = $xpathDom->evaluate('/html/body/div[1]/div[3]/div/div/div/table[1]/tbody/tr[1]/th[2]');
                $Vendor = "";
                $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[2]/td[1]');
                foreach ($row as $value) {
                    $Vendor = trim($value->textContent);
                }
                $scarping_results['Vendor_name'] = $Vendor;

                $vendor_address = "";
                $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[3]/td[1]');
                foreach ($row as $value) {
                    $vendor_address = trim($value->textContent);
                }
                $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[4]/td[1]');
                foreach ($row as $value) {
                    $vendor_address .=" " . trim($value->textContent);
                }
                $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[5]/td[1]');
                foreach ($row as $value) {
                    $vendor_address .=" " . trim($value->textContent);
                }
                $scarping_results['vendor_address'] = $vendor_address;


                $customer = "";
                $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[2]/td[2]');
                foreach ($row as $value) {
                    $customer = trim($value->textContent);
                }
                $scarping_results['Customer_Name'] = $customer;

                $customer_address = "";
                $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[3]/td[2]');
                foreach ($row as $value) {
                    $customer_address = trim($value->textContent);
                }
                $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[4]/td[2]');
                foreach ($row as $value) {
                    $customer_address .=" " . trim($value->textContent);
                }
                $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[5]/td[2]');
                foreach ($row as $value) {
                    $customer_address .=" " . trim($value->textContent);
                }
                $scarping_results['Customer_address'] = $customer_address;


                //shipt
                $Shipto = "";
                $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[2]/td[3]');
                foreach ($row as $value) {
                    $Shipto = trim($value->textContent);
                }
                $scarping_results['Shipto_Name'] = $customer;

                $Shipto_address = "";
                $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[3]/td[3]');
                foreach ($row as $value) {
                    $Shipto_address = trim($value->textContent);
                }
                $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[4]/td[3]');
                foreach ($row as $value) {
                    $Shipto_address .=" " . trim($value->textContent);
                }
                $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[5]/td[3]');
                foreach ($row as $value) {
                    $Shipto_address .=" " . trim($value->textContent);
                }
                $scarping_results['Shipto_address'] = $customer_address;

                //invocie details
                $Invoice = "";
                $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[2]/td[4]');
                foreach ($row as $value) {
                    $Invoice = trim($value->textContent);
                }
                $scarping_results['Invoice'] = str_replace("Invoice: ", "", $Invoice);

                $PO = "";
                $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[3]/td[4]');
                foreach ($row as $value) {
                    $PO = trim($value->textContent);
                }
                $scarping_results['PO'] = $PO;

                $Rep = "";
                $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[4]/td[4]');
                foreach ($row as $value) {
                    $Rep = trim($value->textContent);
                }
                $scarping_results['Rep'] = $Rep;
                $Date = "";
                $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[5]/td[4]');
                foreach ($row as $value) {
                    $Date = trim($value->textContent);
                }
                $scarping_results['Date'] = $Date;

                $Amount = "";
                $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[6]/td[4]');
                foreach ($row as $value) {
                    $Amount = trim($value->textContent);
                }
                $scarping_results['Amount'] = $Amount;

                $ND_LOT = array();
                $i = 2;
                do {
                    $lotndc = array();
                    $NDC = "";
                    $row = $xpathDom->query('//table[@class="table2"]//tr[' . $i . ']/td[3]');
                    foreach ($row as $value) {
                        $NDC = trim($value->textContent);
                    }
                    $lotndc['NDC'] = $NDC;
                    $row = $xpathDom->query('//table[@class="table2"]//tr[' . $i . ']/td[5]');
                    foreach ($row as $value) {
                        $LOT = trim($value->textContent);
                    }
                    $lotndc['LOT'] = $LOT;

                    //GeT URL OF INVOIC
                    $url = "";
                    $ahrefs = $xpathDom->query('//table[@class="table2"]//tr[' . $i . ']/td[1]/a');
                    foreach ($ahrefs as $ahref) {
                        $url = $ahref->attributes->getNamedItem("href")->nodeValue;
                    }
                    $lotndc['IN_URL'] = $url;
                    if ($NDC != '')
                        $ND_LOT[] = $lotndc;
                    $i++;
                }while ($NDC != '');
                $scarping_results['ND_LOT'] = $ND_LOT;

                //print_r($scarping_results);
                //Find Lot Number and INvoice Number in table
                if ($scarping_results['ND_LOT']) {
                    foreach ($scarping_results['ND_LOT'] as $key => $lotdt) {

                        $prodetajl = $this->ProductInfo->find('first', array('conditions' => array("AND" => array('ProductInfo.batchno' => $lotdt['LOT'], 'ProductInfo.InvoiceNo' => $scarping_results['Invoice'], 'ProductInfo.id' => $pid))));

                        if (!empty($prodetajl)) {
                            $curl->get($lotdt['IN_URL'], array());

                            if ($curl->error) {
                                echo 'Error: ' . $curl->error_code . ': ' . $curl->error_message;
                            } else {
                                $data = $curl->response;
                                $dom = new DOMDocument();
                                @$dom->loadHTML($data); //Lots of invalid html going on so I am suppressing the warnings
                                $xpathDom = new DOMXPath($dom);

                                $INVOICE_T1 = array();
                                $i = 2;
                                do {
                                    $Table_1 = array();
                                    $Item = "";
                                    $row = $xpathDom->query('//table[@class="table2"]//tr[' . $i . ']/td[1]');
                                    foreach ($row as $value) {
                                        $Item = trim($value->textContent);
                                    }
                                    $Table_1['Item'] = $Item;
                                    $Package_Size = "";
                                    $row = $xpathDom->query('//table[@class="table2"]//tr[' . $i . ']/td[3]');
                                    foreach ($row as $value) {
                                        $Package_Size = trim($value->textContent);
                                    }
                                    $Table_1['Package_Size'] = $Package_Size;

                                    $DosageForm = "";
                                    $row = $xpathDom->query('//table[@class="table2"]//tr[' . $i . ']/td[4]');
                                    foreach ($row as $value) {
                                        $DosageForm = trim($value->textContent);
                                    }
                                    $Table_1['DosageForm'] = $DosageForm;

                                    $Strength = "";
                                    $row = $xpathDom->query('//table[@class="table2"]//tr[' . $i . ']/td[5]');
                                    foreach ($row as $value) {
                                        $Strength = trim($value->textContent);
                                    }
                                    $Table_1['Strength'] = $Strength;

                                    $Expiration = "";
                                    $row = $xpathDom->query('//table[@class="table2"]//tr[' . $i . ']/td[8]');
                                    foreach ($row as $value) {
                                        $Expiration = trim($value->textContent);
                                    }
                                    $Table_1['Expiration'] = $Expiration;

                                    $Manufacturer = "";
                                    $row = $xpathDom->query('//table[@class="table2"]//tr[' . $i . ']/td[10]');
                                    foreach ($row as $value) {
                                        $Manufacturer = trim($value->textContent);
                                    }
                                    $Table_1['Manufacturer'] = $Manufacturer;
                                    if ($Item != '')
                                        $INVOICE_T1[] = $Table_1;
                                    $i++;
                                }while ($Item != '');
                                $scarping_results['ND_LOT'][$key]['INVOICE_T1'] = $INVOICE_T1;
                                $scarping_results['ND_LOT'][$key]['ProductInfo_id'] = $prodetajl['ProductInfo']['id'];
                                //Fetch Table Two data

                                $row = $xpathDom->query('/html/body/div[1]/div[3]/div/div/table[2]');
                                $Invoice_T2 = "";
                                foreach ($row as $tag) {
                                    $innerHTML = '';

                                    // see http://fr.php.net/manual/en/class.domelement.php#86803
                                    $children = $tag->childNodes;
                                    foreach ($children as $child) {
                                        $tmp_doc = new DOMDocument();
                                        $tmp_doc->appendChild($tmp_doc->importNode($child, true));
                                        $innerHTML .= $tmp_doc->saveHTML();
                                    }
                                    if (trim($innerHTML) != '')
                                        $Invoice_T2 = "<table>" . (trim($innerHTML)) . "</table>";
                                }
                                $scarping_results['ND_LOT'][$key]['Invoice_T2'] = $Invoice_T2;


                                $Invoice_P1 = "";
                                $row = $xpathDom->query('/html/body/div[1]/div[3]/div/div/p[1]');
                                foreach ($row as $value) {
                                    $Invoice_P1 = trim($value->textContent);
                                }
                                $scarping_results['ND_LOT'][$key]['Invoice_P1'] = $Invoice_P1;

                                $Invoice_P2 = "";
                                $row = $xpathDom->query('/html/body/div[1]/div[3]/div/div/p[2]/img');
                                foreach ($row as $value) {
                                    $Invoice_P2 = $value->attributes->getNamedItem("src")->nodeValue;
                                }
                                $scarping_results['ND_LOT'][$key]['Invoice_P2'] = $Invoice_P2;

                                $Invoice_P3 = "";
                                $row = $xpathDom->query('/html/body/div[1]/div[3]/div/div/p[3]');
                                foreach ($row as $value) {
                                    $Invoice_P3 = trim($value->textContent);
                                }
                                $scarping_results['ND_LOT'][$key]['Invoice_P3'] = $Invoice_P3;
                            }
                        } else {
                            $this->ProductInfo->id = $pid;
                            $this->ProductInfo->saveField('Pedigree', "CH");
                        }
                    }

                    //print_r($scarping_results);die;
                }

                //save data 

                if (!empty($scarping_results['ND_LOT'])) {
                    $successmsg = "";
                    foreach ($scarping_results['ND_LOT'] as $key => $sp_result) {
                        if (@$sp_result['ProductInfo_id'] != '') {

                            $data_array = array(
                                'product_infos_id' => $sp_result['ProductInfo_id'],
                                'Invoice' => $scarping_results['Invoice'],
                                'NDC' => $sp_result['NDC'],
                                'LOT' => $sp_result['LOT'],
                                'INVOICE_T1' => json_encode($sp_result['INVOICE_T1']),
                                'Invoice_T2' => json_encode($sp_result['Invoice_T2']),
                                'Invoice_P1' => json_encode($sp_result['Invoice_P1']),
                                'Invoice_P2' => json_encode($sp_result['Invoice_P2']),
                                'Invoice_P3' => json_encode($sp_result['Invoice_P3']),
                                'Other_details' => json_encode(array('Vendor_name' => $scarping_results['Vendor_name'], 'vendor_address' => $scarping_results['vendor_address'], 'Customer_Name' => $scarping_results['Customer_Name'], 'Customer_address' => $scarping_results['Customer_address'], 'Date' => $scarping_results['Date'], 'Amount' => $scarping_results['Amount'])),
                                'created' => date("Y-m-d H:i:s")
                            );

                            //print_r($data_array);

                            $this->Pedigree->create();
                            if ($this->Pedigree->save($data_array)) {
                                $this->ProductInfo->id = $pid;
                                $this->ProductInfo->saveField('Pedigree', "Y");
                                $successmsg = $successmsg . $sp_result['LOT'] . ",";
                            }
                        } else {
                            $this->ProductInfo->id = $pid;
                            //$this->ProductInfo->saveField('Pedigree', "CH");
                        }
                    }
                    //$this->Session->setFlash(__($successmsg), 'admin_success');
                    //return true;
                    //$this->redirect(array('action' => 'admin_manage_products'));
                    return array("status" => "true", "INVOICE" => $INVOICE, "successmsg" => $successmsg);
                } else {
                    return array("status" => "false", "INVOICE" => $INVOICE);
                    //$this->redirect(array('action' => 'admin_manage_products'));
                }
            }
        }
    }

    public function admin_view_pedigree($pedgreeid) {
        $this->layout = "admin_dashboard";
        $prodetajl = $this->Pedigree->find('first', array('recursive' => 2, 'conditions' => array('Pedigree.product_infos_id' => $pedgreeid)));
        $this->set("dhtml", $prodetajl);
        if ($prodetajl['Pedigree']['anda'])
            $this->view = 'admin_view_pedigree_anda';
    }

    public function admin_pending_anda_pedigree($pedgreeid) {
        $this->layout = "admin_dashboard";
        Configure::write('debug', 2);
        if ($this->request->is('put') || $this->request->is('post')) {


            $prodetajl = $this->Pedigree->find('first', array('recursive' => 2, 'conditions' => array('Pedigree.id' => $pedgreeid)));
            $pedigree = $prodetajl['Pedigree']['INVOICE_T1'];
            $pedigree = json_decode($pedigree);

            $pedigree->shippedPedigree->itemInfo->quantity = $this->request->data['pedigree']['Quantity'];
            $status = $this->Pedigree->updateAll(
                    array('Pedigree.INVOICE_T1' => "'" . json_encode($pedigree) . "'"), array('Pedigree.id' => $pedgreeid)
            );
        }
        $prodetajl = $this->Pedigree->find('first', array('recursive' => 2, 'conditions' => array('Pedigree.id' => $pedgreeid)));
        $this->set("dhtml", $prodetajl);
        if ($prodetajl['Pedigree']['anda'])
            $this->view = 'admin_pending_anda_pedigree';
    }

    public function admin_pending_qk_pedigree($pedgreeid) {
        $this->layout = "admin_dashboard";
        Configure::write('debug', 2);
        $this->loadModel('TempPedigree');
        if ($this->request->is('put') || $this->request->is('post')) {


            $status = $this->TempPedigree->updateAll(
                    array('TempPedigree.Qty' => "'" . $this->request->data['pedigree']['Quantity'] . "'"), array('TempPedigree.id' => $pedgreeid)
            );
        }

        $prodetajl = $this->TempPedigree->find('first', array('recursive' => 2, 'conditions' => array('TempPedigree.id' => $pedgreeid)));
        $this->set("dhtml", $prodetajl);
    }

    public function admin_qk_lot_add($id) {

       // Configure::write('debug', 3);
        $this->loadModel('TempPedigree');
        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));


        $conditions = array('recursive' => 3,
            'conditions' => array(
                'and' => array(
                    array(
                        'TempPedigree.Invoice' => $id,
                        'TempPedigree.product_infos_id' => 0,
                        'TempPedigree.Status' => 'Active'
                    )
        )));
        $pedgreelist = $this->TempPedigree->find("all", $conditions);
        
        
        $error = array();
        if (!empty($pedgreelist)) {
            
            foreach ($pedgreelist as $pddetls) {
                
                $pedgree_json = json_decode($pddetls['TempPedigree']['INVOICE_T1']);
                
                $products_product_ndcs = $this->Products->find("first", array('conditions' => array("Products.product_ndc like '%" . $pddetls['TempPedigree']['NDC'] . "'"), 'fields' => array('product_ndc', 'id')));
                //print_r($products_product_ndcs);die;
                if (!empty($products_product_ndcs)) {
                    //count previous quenty
                    
                    $previous_quentity = 0;
                     $expiry_date = '20' . $pedgree_json[0]->Expiration . '/28';
					//echo date('Y-m-d', strtotime($expiry_date));die;
                    $data['ProductInfo']['prodid'] = $products_product_ndcs['Products']['id'];
                    $data['ProductInfo']['batchno'] = $pddetls['TempPedigree']['LOT'];
                    $data['ProductInfo']['mfgdate'] = $pedgree_json[0]->Manufacturer;
                    $data['ProductInfo']['expdate'] = date('Y-m-d', strtotime($expiry_date));
                    $data['ProductInfo']['availability'] = $pddetls['TempPedigree']['Qty'];
                    $data['ProductInfo']['balance'] = $pddetls['TempPedigree']['Qty'];
                    $data['ProductInfo']['VendorName'] = "QK";
                    $data['ProductInfo']['InvoiceNo'] = $pddetls['TempPedigree']['Invoice'];
                    $data['ProductInfo']['ReceivedDate'] = date('Y-m-d');
                    $data['ProductInfo']['Pedigree'] = "Y";
                    $data['ProductInfo']['createddt'] = date("Y-m-d");

                    $this->ProductInfo->create();

                    if ($this->ProductInfo->save($data)) {


                        //Add product update acitvity

                        $activit_data = array(
                            'product_id' => $products_product_ndcs['Products']['id'],
                            'product_info_id' => $this->ProductInfo->id,
                            'user_id' => $this->Auth->user('id'),
                            'previous_qty' => $previous_quentity,
                            'new_qty' => $pddetls['TempPedigree']['Qty'],
                            'note' => "Add by QK Scrap",
                            'created' => date("Y-m-d H:i:s")
                        );
                        $this->admin_add_product_activity($activit_data);

                        $data_array = array('product_infos_id' => $this->ProductInfo->id,
                            'Invoice' => $pddetls['TempPedigree']['Invoice'],
                            'NDC' => $pddetls['TempPedigree']['NDC'],
                            'LOT' => $pddetls['TempPedigree']['LOT'],
                            'INVOICE_T1' => $pddetls['TempPedigree']['INVOICE_T1'],
                            'Invoice_T2' => $pddetls['TempPedigree']['Invoice_T2'],
                            'Invoice_P1' => $pddetls['TempPedigree']['Invoice_P1'],
                            'Invoice_P2' => $pddetls['TempPedigree']['Invoice_P2'],
                            'Invoice_P3' => $pddetls['TempPedigree']['Invoice_P3'],
                            'Other_details' => $pddetls['TempPedigree']['Other_details'],
                            'anda' => 0,
                            'created' => date("Y-m-d H:i:s")
                        );

                        $this->Pedigree->create();
                        $this->Pedigree->save($data_array);
                        $status = "'Uploaded'";
                        $this->TempPedigree->updateAll(
                                array('TempPedigree.Status' => $status), array('TempPedigree.id' => $pddetls['TempPedigree']['id'])
                        );

                        $this->Session->setFlash(__('The Lot has been Added'), 'admin_success');
                        $error[] = 'The Lot has been Added NDC- ' . $pddetls['TempPedigree']['NDC'] . '.';
                        //$this->redirect(array('action' => 'admin_anda_pedigree'));
                    } else {

                        $this->Session->setFlash(__('The Product could not be saved. Please, try again.'), 'admin_error');
                        $error[] = 'The Product(' . $pddetls['Pedigree']['NDC'] . ') could not be saved. Please, try again.';
                        //$this->redirect(array('action' => 'admin_anda_pedigree'));
                    }
                } else {
                    $this->Session->setFlash(__('NO Products found with this NDC.'), 'admin_error');
                    $error[] = 'NO Products found with this NDC ' . $pddetls['TempPedigree']['NDC'] . '.';
                    //$this->redirect(array('action' => 'admin_anda_pedigree'));
                }
            }
            if (!empty($error)) {
                $this->Session->setFlash(__(implode(',', $error)), 'admin_error');
                $this->redirect(array('action' => 'admin_qk_pedigree'));
            } else {
                $this->Session->setFlash(__('The Lot has been Uploaded'), 'admin_success');
                $this->redirect(array('action' => 'admin_qk_pedigree'));
            }
        } else {
            $this->Session->setFlash(__('NO Lot found with this Invoice Number.'), 'admin_error');
            $this->redirect(array('action' => 'admin_qk_pedigree'));
        }
    }

    public function admin_anda_pedigree_popup($pedgreeid) {
        $this->layout = "admin_dashboard";
        $prodetajl = $this->Pedigree->find('all', array('recursive' => 2, 'conditions' => array('Pedigree.Invoice' => $pedgreeid, 'Pedigree.product_infos_id' => 0, 'Pedigree.anda' => 1)));
        $this->set("dhtml", $prodetajl);

        //$this->view='admin_pending_anda_pedigree';
    }

    public function admin_manual_pedigree($pedgreeid) {
        $this->layout = "admin_dashboard";
        $prodetajl = $this->ProductInfo->find('first', array('conditions' => array('ProductInfo.id' => $pedgreeid)));
        //print_r($prodetajl);
        $this->set("dhtml", $prodetajl);
    }

    public function dom_find_content($xpath, $pattern) {

        $src = $xpath->evaluate("$pattern");
        $result = array();
        for ($j = 0; $j < $src->length; $j++) {
            $li = $src->item($j);
            $heading = @$li->textContent;
            if ($heading != '')
                array_push($result, $heading);
        }
        return $result;
    }

    public function admin_product_activites($id = null) {

        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));

        $this->Products->id = $id;

        $search_text = trim($this->params['url']['ndcproduct']);
        //ndc divide multi condtion
        $search_text_c1 = substr($search_text, 1, -1);
        $search_text_c2 = substr($search_text, 1);
        $search_text_c3 = substr($search_text, 0, -1);
        $search_text_c3 = '0' . $search_text;
        $search_text_c4 = $this->insertAtPosition($search_text, 0, 5);
        $search_text_c5 = $this->insertAtPosition($search_text, 0, 9);
        $search_text_c6 = '0' . $search_text_c1;
        $search_text_c7 = $this->insertAtPosition($search_text_c2, 0, 5);
        $search_text_c8 = $this->insertAtPosition($search_text_c2, 0, 9);
        $search_text_c9 = $this->insertAtPosition($search_text_c1, 0, 9);
        $search_text_c10 = $this->insertAtPosition($search_text_c1, 0, 5);


        $this->Products->recursive = 1;

        $search = "";
        if (!isset($this->params['url']['ndcproduct'])) {
            $this->paginate = array('order' => array(
                    'ProductActivity.created' => 'desc')
                , 'limit' => 10);
        } else {
            $searchparameter = $this->params['url']['ndcproduct'];
            $this->paginate = array(
                'conditions' => array("OR" => array("Product.product_ndc" => array($search_text, $search_text_c1, $search_text_c2, $search_text_c3, $search_text_c4, $search_text_c5, $search_text_c6, $search_text_c7, $search_text_c8, $search_text_c9, $search_text_c10), "Product.itemnumber like '%" . trim($this->params['url']['ndcproduct']) . "%'", "Product.productname like '%" . trim($this->params['url']['ndcproduct']) . "%'", "ProductInfo.batchno like '%" . trim($searchparameter) . "%'", "ProductInfo.InvoiceNo like '%" . trim($searchparameter) . "%'", "User.fname like '%" . trim($searchparameter) . "%'")),
                'order' => array('ProductActivity.created' => 'desc'),
                'limit' => 10
            );
            $search = $this->params['url']['ndcproduct'];
        }
        // print_r($this->paginate("ProductActivity"));
        $this->set('search', $search);
        $this->set('users', $this->paginate("ProductActivity"));
    }
	
	public function admin_product_change_history($id = null) {

        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));

        $this->Products->id = $id;

        $search_text = trim($this->params['url']['ndcproduct']);
        //ndc divide multi condtion
        $search_text_c1 = substr($search_text, 1, -1);
        $search_text_c2 = substr($search_text, 1);
        $search_text_c3 = substr($search_text, 0, -1);
        $search_text_c3 = '0' . $search_text;
        $search_text_c4 = $this->insertAtPosition($search_text, 0, 5);
        $search_text_c5 = $this->insertAtPosition($search_text, 0, 9);
        $search_text_c6 = '0' . $search_text_c1;
        $search_text_c7 = $this->insertAtPosition($search_text_c2, 0, 5);
        $search_text_c8 = $this->insertAtPosition($search_text_c2, 0, 9);
        $search_text_c9 = $this->insertAtPosition($search_text_c1, 0, 9);
        $search_text_c10 = $this->insertAtPosition($search_text_c1, 0, 5);

		//Configure::write('debug', 2);
        $this->Products->recursive = 1;
		$this->loadModel('ProductPriceChange');
        $search = "";
        if (!isset($this->params['url']['ndcproduct'])) {
            $this->paginate = array('conditions'=>array('ProductPriceChange.product_id'=>$id),'order' => array(
                    'ProductPriceChange.MODIFIED_DATE' => 'desc')
                , 'limit' => 10);
        } else {
            $searchparameter = $this->params['url']['ndcproduct'];
            $this->paginate = array(
                'conditions' => array("AND"=>array('ProductPriceChange.product_id'=>$id),"OR" => array("Product.product_ndc" => array($search_text, $search_text_c1, $search_text_c2, $search_text_c3, $search_text_c4, $search_text_c5, $search_text_c6, $search_text_c7, $search_text_c8, $search_text_c9, $search_text_c10), "Product.itemnumber like '%" . trim($this->params['url']['ndcproduct']) . "%'", "Product.productname like '%" . trim($this->params['url']['ndcproduct']) . "%'",  "User.fname like '%" . trim($searchparameter) . "%'")),
                'order' => array('ProductPriceChange.MODIFIED_DATE' => 'desc'),
                'limit' => 10
            );
            $search = $this->params['url']['ndcproduct'];
        }
        // print_r($this->paginate("ProductActivity"));
        $this->set('search', $search);
        $this->set('users', $this->paginate("ProductPriceChange"));
    }
	
    public function admin_product_activites_csv($csvFile = null) {
        $data = $this->ProductActivity->find('all');
        // print_r($data);die;
        $csvdata = array();
        foreach ($data as $key => $dt) {

            $csvdata[$key]['Product NDC'] = $dt['Product']['product_ndc'];
            $csvdata[$key]['Item Number'] = $dt['Product']['itemnumber'];
            $csvdata[$key]['Product Name'] = $dt['Product']['productname'];
            $csvdata[$key]['Lot no'] = $dt['ProductInfo']['batchno'];
            $csvdata[$key]['Expiry'] = $dt['ProductInfo']['expdate'];
            $csvdata[$key]['Received Date'] = $dt['ProductInfo']['ReceivedDate'];
            $csvdata[$key]['Previous Qty'] = $dt['ProductActivity']['previous_qty'];
            $csvdata[$key]['New Qty'] = $dt['ProductActivity']['new_qty'];

            $csvdata[$key]['Vendor Name'] = $dt['ProductInfo']['VendorName'];
            $csvdata[$key]['Vendor Invoice'] = $dt['ProductInfo']['InvoiceNo'];
            $csvdata[$key]['Notes'] = $dt['ProductActivity']['note'];

            $csvdata[$key]['Modified Date'] = $dt['ProductActivity']['created'];
            $csvdata[$key]['Modified By'] = $dt['User']['fname'];
        }
        $this->Export->exportCsv($csvdata);
    }

    function admin_manage_products_custom() {
        Configure::write('debug', 2);
        $count = 0;
        $products = $this->Products->query("SELECT * FROM product_infos");
        foreach ($products as $product) {

            $ndc = $this->Products->query("SELECT id FROM products where product_ndc='" . $product['product_infos']['prodid'] . "'");

            //echo "UPDATE product_infos SET `prodid` = ".$ndc[0]['products']['id']." where id=".$product['product_infos']['id'];
            if (!empty($ndc)) {

                $this->Products->query("UPDATE product_infos SET `prodid`='" . $ndc[0]['products']['id'] . "' where id=" . $product['product_infos']['id']);
                $count++;
            }
        }
        echo "Total update" . $count;
        die;
    }

    public function admin_full_history($id = null) {
        $this->layout = "admin_dashboard";
        $this->set('title', __('Welcome Admin Panel'));
        $this->set('description', __('Manage Users'));

        $product = $this->Products->find('first', array('conditions' => array('Products.id' => $id)));
        $this->set("product", $product);
        //get Activityes from order
        $product_activies = $this->ProductActivity->find('all', array('order' => array('ProductActivity.created' => 'desc'), 'conditions' => array('product_id' => $id)));
        $this->set("product_activies", $product_activies);
    }

    public function admin_add_product_activity($data) {
        $this->ProductActivity->create();

        if ($this->ProductActivity->save($data)) {
            return true;
        }
    }

    public function admin_deactive_products_zero() {

        $products = $this->Products->query("select products.*,sum(`availability`) as qntity from products left join product_infos on products.id=product_infos.prodid group by product_ndc");

        foreach ($products as $produc) {
            if ($produc[0]['qntity'] == '' or $produc[0]['qntity'] == 0) {
                $status = $this->Products->updateAll(
                        array('active' => "'False'"), array('Products.id' => $produc['products']['id'])
                );
                print_r($produc);
            }
        }

        die;
    }

    public function admin_update_Purchase_price() {
        // $data = $this->Products->find('all');
        Configure::write('debug', 2);
        $data = $this->Products->query('select * from temp_products');
        $update_counter = 0;
        foreach ($data as $key => $dt) {

            $dt_product = $this->Products->find('first', array('conditions' => array("Products.itemnumber" => $dt['temp_products']['Item'], "Products.product_ndc like '%" . $dt['temp_products']['ProductNDC'] . "'")));
            //print_r($dt_product);die;
            $dt['Products']['producttype'] = $dt_product['Products']['producttype'];
            $dt['Products']['PurchasePrice'] = $dt['temp_products']['purchaseprice'];
            $dt['Products']['id'] = $dt_product['Products']['id'];
            $product_price = 0;
            //Foe Brand
            if ($dt['Products']['producttype'] == 'Brand') {
                if ($dt['Products']['PurchasePrice'] > 0 && $dt['Products']['PurchasePrice'] < 1)
                    $product_price = .5;
                if ($dt['Products']['PurchasePrice'] >= 1 && $dt['Products']['PurchasePrice'] <= 5)
                    $product_price = 1;
                if ($dt['Products']['PurchasePrice'] >= 6 && $dt['Products']['PurchasePrice'] <= 20)
                    $product_price = 1.45;
                if ($dt['Products']['PurchasePrice'] >= 21 && $dt['Products']['PurchasePrice'] <= 50)
                    $product_price = 2;
                if ($dt['Products']['PurchasePrice'] >= 51 && $dt['Products']['PurchasePrice'] <= 100)
                    $product_price = 3;
                if ($dt['Products']['PurchasePrice'] >= 101 && $dt['Products']['PurchasePrice'] <= 200)
                    $product_price = 4;
                if ($dt['Products']['PurchasePrice'] >= 201 && $dt['Products']['PurchasePrice'] <= 400)
                    $product_price = 5;
                if ($dt['Products']['PurchasePrice'] >= 401 && $dt['Products']['PurchasePrice'] <= 600)
                    $product_price = 6;
                if ($dt['Products']['PurchasePrice'] >= 601)
                    $product_price = 7;
            }
            //Foe Generic
            if ($dt['Products']['producttype'] == 'Generic') {
                if ($dt['Products']['PurchasePrice'] > 0 && $dt['Products']['PurchasePrice'] < 1)
                    $product_price = .5;
                if ($dt['Products']['PurchasePrice'] >= 1 && $dt['Products']['PurchasePrice'] <= 5)
                    $product_price = 1;
                if ($dt['Products']['PurchasePrice'] >= 6 && $dt['Products']['PurchasePrice'] <= 20)
                    $product_price = 1.45;
                if ($dt['Products']['PurchasePrice'] >= 21 && $dt['Products']['PurchasePrice'] <= 50)
                    $product_price = 2;
                if ($dt['Products']['PurchasePrice'] >= 51 && $dt['Products']['PurchasePrice'] <= 100)
                    $product_price = 3;
                if ($dt['Products']['PurchasePrice'] >= 101 && $dt['Products']['PurchasePrice'] <= 200)
                    $product_price = 5;
                if ($dt['Products']['PurchasePrice'] >= 201 && $dt['Products']['PurchasePrice'] <= 400)
                    $product_price = 7;
                if ($dt['Products']['PurchasePrice'] >= 401 && $dt['Products']['PurchasePrice'] <= 600)
                    $product_price = 9;
                if ($dt['Products']['PurchasePrice'] >= 601)
                    $product_price = 11;
            }

            //Foe Refrigirated
            if ($dt['Products']['producttype'] == 'Refrigirated') {
                if ($dt['Products']['PurchasePrice'] > 0 && $dt['Products']['PurchasePrice'] < 1)
                    $product_price = .5;
                if ($dt['Products']['PurchasePrice'] >= 1 && $dt['Products']['PurchasePrice'] <= 5)
                    $product_price = 1;
                if ($dt['Products']['PurchasePrice'] >= 6 && $dt['Products']['PurchasePrice'] <= 20)
                    $product_price = 1.45;
                if ($dt['Products']['PurchasePrice'] >= 21 && $dt['Products']['PurchasePrice'] <= 50)
                    $product_price = 2;
                if ($dt['Products']['PurchasePrice'] >= 51 && $dt['Products']['PurchasePrice'] <= 100)
                    $product_price = 3;
                if ($dt['Products']['PurchasePrice'] >= 101 && $dt['Products']['PurchasePrice'] <= 200)
                    $product_price = 4;
                if ($dt['Products']['PurchasePrice'] >= 201 && $dt['Products']['PurchasePrice'] <= 400)
                    $product_price = 5;
                if ($dt['Products']['PurchasePrice'] >= 401 && $dt['Products']['PurchasePrice'] <= 600)
                    $product_price = 6;
                if ($dt['Products']['PurchasePrice'] >= 601)
                    $product_price = 7;
            }

            //Foe OTC
            if ($dt['Products']['producttype'] == 'OTC') {
                if ($dt['Products']['PurchasePrice'] > 0 && $dt['Products']['PurchasePrice'] < 1)
                    $product_price = .5;
                if ($dt['Products']['PurchasePrice'] >= 1 && $dt['Products']['PurchasePrice'] <= 5)
                    $product_price = 1;
                if ($dt['Products']['PurchasePrice'] >= 6 && $dt['Products']['PurchasePrice'] <= 20)
                    $product_price = 1.45;
                if ($dt['Products']['PurchasePrice'] >= 21 && $dt['Products']['PurchasePrice'] <= 50)
                    $product_price = 2;
                if ($dt['Products']['PurchasePrice'] >= 51 && $dt['Products']['PurchasePrice'] <= 100)
                    $product_price = 3;
                if ($dt['Products']['PurchasePrice'] >= 101 && $dt['Products']['PurchasePrice'] <= 200)
                    $product_price = 5;
                if ($dt['Products']['PurchasePrice'] >= 201 && $dt['Products']['PurchasePrice'] <= 400)
                    $product_price = 7;
                if ($dt['Products']['PurchasePrice'] >= 401 && $dt['Products']['PurchasePrice'] <= 600)
                    $product_price = 9;
                if ($dt['Products']['PurchasePrice'] >= 601)
                    $product_price = 11;
            }

            $purchase_price = $dt['Products']['PurchasePrice'] + $product_price;
            $status = $this->Products->updateAll(
                    array('productprice' => $purchase_price, 'PurchasePrice' => $dt['Products']['PurchasePrice']), array('Products.id' => $dt['Products']['id'])
            );
            if ($status)
                $update_counter++;
        }
        echo "Update Product Price $update_counter";
        die;
    }

    public function admin_orderinfo_dicount() {

        if ($this->request->is('put') || $this->request->is('post')) {

            if ($this->request->data['products']['orderdetailid'] != '' and $this->request->data['products']['discount'] != '' and $this->request->data['products']['discount'] > 0) {
                $Orderdetail = $this->Orderdetail->find('first', array('conditions' => array('Orderdetail.id' => $this->request->data['products']['orderdetailid'])));
                // print_r($Orderdetail);die;
                if (!empty($Orderdetail)) {
                    $total_amount = ($Orderdetail['Orderdetail']['price'] * $Orderdetail['Orderdetail']['quantity']);
                    $discount_apply = ($total_amount * $this->request->data['products']['discount']) / 100;
                    $grand_total = ($total_amount - $discount_apply);
                    $discount_peritem = $grand_total / $Orderdetail['Orderdetail']['quantity'];
                    $discount_peritem = round($discount_peritem, 2);
                    $status = $this->Orderdetail->updateAll(
                            array('price' => $discount_peritem,
                        'original_price' => ($Orderdetail['Orderdetail']['quantity'] * $Orderdetail['Orderdetail']['price']),
                        'special_discount' => ($Orderdetail['Orderdetail']['special_discount'] + $this->request->data['products']['discount'])
                            ), array('Orderdetail.id' => $this->request->data['products']['orderdetailid'])
                    );

                    if ($status) {



                        $this->Session->setFlash(__($this->request->data['products']['discount'] . '% Discount Applied'), 'admin_success');
                        $this->redirect(array('action' => 'admin_orderinfo/' . $Orderdetail['Orderdetail']['orderid']));
                    } else {

                        $this->Session->setFlash(__('Discount could not be Applied. Please, try again.'), 'admin_error');
                        $this->redirect(array('action' => 'admin_orderinfo/' . $Orderdetail['Orderdetail']['orderid']));
                    }
                }
            }
        }

        $this->redirect(array('action' => 'admin_asgin_products'));
    }

    public function admin_orderinfo_dicount_reset($id = NULL) {

        if ($id != '') {

            $Orderdetail = $this->Orderdetail->find('first', array('conditions' => array('Orderdetail.id' => $id)));

            if (!empty($Orderdetail)) {
                $total_amount = ($Orderdetail['Orderdetail']['price'] * $Orderdetail['Orderdetail']['quantity']);
                $discount_apply = ($total_amount * $this->request->data['products']['discount']) / 100;
                $grand_total = ($total_amount - $discount_apply);
                $discount_peritem = $grand_total / $Orderdetail['Orderdetail']['quantity'];
                $discount_peritem = round($discount_peritem, 2);
                $status = $this->Orderdetail->updateAll(
                        array('price' => $Orderdetail['Orderdetail']['original_price'],
                    'special_discount' => 0
                        ), array('Orderdetail.id' => $Orderdetail['Orderdetail']['id'])
                );

                if ($status) {



                    $this->Session->setFlash(__($this->request->data['products']['discount'] . '% Discount Canceled'), 'admin_success');
                    $this->redirect(array('action' => 'admin_orderinfo/' . $Orderdetail['Orderdetail']['orderid']));
                } else {

                    $this->Session->setFlash(__('Discount could not be Canceled. Please, try again.'), 'admin_error');
                    $this->redirect(array('action' => 'admin_orderinfo/' . $Orderdetail['Orderdetail']['orderid']));
                }
            }
        }

        $this->redirect(array('action' => 'admin_asgin_products'));
    }

    public function admin_generic_sold_excel() {

        set_time_limit(0);
        ini_set('memory_limit', '256M');
        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));

        if (isset($this->params['url']['selfrmdate'])) {
            //Configure::write('debug',3);
            $conditions = array('recursive' => 3,
                'conditions' => array(
                    'and' => array(
                        array('OrderDist.createddt <= ' => $this->params['url']['seltodate'] . ' 23:59',
                            'OrderDist.createddt >= ' => $this->params['url']['selfrmdate'] . ' 00:00',
                            'OrderDist.status' => array('Accep')
                        )
            )));
            $order_list = $this->OrderDist->find("all", $conditions);
            //print_r($order_list);die;
            //$this->paginate=$conditions;
            //$order_list=$this->paginate("Order");

            if (empty($order_list)) {
                $this->Session->setFlash(__('NO Order found between these dates.'), 'admin_error');
                $this->redirect(array('action' => 'product_profit'));
            }

            $orderlist_tmp = array();
            $TMP_ARRAY = array();
            foreach ($order_list as $key => $ords) {
                foreach ($ords['OrderdetailMany'] as $key2 => $ords2) {
                    if (array_key_exists($ords2['Product']['product_ndc'], $TMP_ARRAY)) {
                        $val = $TMP_ARRAY[$ords2['Product']['product_ndc']];
                        $TMP_ARRAY[$ords2['Product']['product_ndc']] = $val + $ords2['quantity'];
                    } else {
                        $TMP_ARRAY[$ords2['Product']['product_ndc']] = $ords2['quantity'];
                    }
                }
            }

            $csvdata = array();
            $key = 0;
            $temarray = array();
            foreach ($order_list as $key1 => $order_list2) {
                foreach ($order_list2['OrderdetailMany'] as $key2 => $dt) {
                    if ($dt['Product']['producttype'] == 'Generic') {
                        if (!in_array($dt['Product']['product_ndc'], $temarray)) {
                            $temarray[] = $dt['Product']['product_ndc'];
                            /* $csvdata[$key]['Order Date']=date("m-d-Y",strtotime($order_list2['OrderDist']['createddt'])); */
                            $csvdata[$key]['Customer'] = $order_list2['User']['fname'];
                            $csvdata[$key]['Product NDC'] = $dt['Product']['product_ndc'];
                            $csvdata[$key]['Item Number'] = $dt['Product']['itemnumber'];
                            $csvdata[$key]['Product Name'] = $dt['Product']['productname'];
                            $csvdata[$key]['Quantity'] = $TMP_ARRAY[$dt['Product']['product_ndc']];
                            /* $csvdata[$key]['Single Itme Sold Price']=$dt['price'];
                              $csvdata[$key]['Total Itme Sold Price']=round($dt['price']*$dt['quantity'],2);
                              $csvdata[$key]['Single Itme Original Price']=round($dt['Product']['original_price'],2);
                              $csvdata[$key]['Total Itme Original Price']=round($dt['Product']['original_price']*$dt['quantity'],2);
                              $csvdata[$key]['Prof/Loss Per Item']=round($dt['price']-$user['Product']['original_price'],2);
                              $csvdata[$key]['Total Prof/Loss']=round(($dt['price']*$dt['quantity'])-($dt['quantity']*$dt['Product']['original_price']),2); */

                            $key++;
                        }
                    }
                }
            }
            $this->Export->exportCsv($csvdata);

            //print_r($order_list);
            $this->set('order_list', $order_list);
        } else {

            $this->Session->setFlash(__('NO Order found between these dates.'), 'admin_error');
            $this->redirect(array('action' => 'product_profit'));
        }
    }
	
	public function admin_product_change_history_invoices($pid) {

        set_time_limit(0);
        ini_set('memory_limit', '256M');
        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));
		Configure::write('debug',3);
        $this->set('description', __('Manage Users'));
		$this->loadModel('ProductPriceChange');
		$MODIFIED_DATE = $this->ProductPriceChange->find("first", array('fields'=>array('MODIFIED_DATE'),'conditions'=>array('ProductPriceChange.id'=>$pid)));
		$modified_from_data=$MODIFIED_DATE['ProductPriceChange']['MODIFIED_DATE'];
		//check to date 
		$MODIFIED_DATE = $this->ProductPriceChange->find("first", array('fields'=>array('MODIFIED_DATE'),'conditions'=>array('ProductPriceChange.id'=>$pid,'MODIFIED_DATE >'=>$modified_from_data)));
		if(empty($MODIFIED_DATE)){
			$todate=date("Y-m-d H:i:s");
		}else{
			$todate=$MODIFIED_DATE['ProductPriceChange']['MODIFIED_DATE'];
		}
		           
            $conditions = array('recursive' => 3,
                'order' => array('OrderDist.createddt' => 'desc'),
                'conditions' => array(
                    'and' => array(
                        array('OrderDist.createddt <= ' => $todate,
                            'OrderDist.createddt >= ' => $modified_from_data,
                            'OrderDist.status' => array('Accep')
                        )
            )));
            $order_list = $this->OrderDist->find("all", $conditions);
            $orderlist_tmp = array();
            $TMP_ARRAY = array();
            foreach ($order_list as $key => $ords) {
                foreach ($ords['OrderdetailMany'] as $key2 => $ords2) {
                    if (array_key_exists($ords2['Product']['product_ndc'], $TMP_ARRAY)) {
                        $val = $TMP_ARRAY[$ords2['Product']['product_ndc']];
                        $TMP_ARRAY[$ords2['Product']['product_ndc']] = $val + $ords2['quantity'];
                    } else {
                        $TMP_ARRAY[$ords2['Product']['product_ndc']] = $ords2['quantity'];
                    }
                }
            }
            //print_r($TMP_ARRAY);die;
            $this->set('TMP_ARRAY', $TMP_ARRAY);
            //$this->paginate=$conditions;
            //$order_list=$this->paginate("Order");

            




            //print_r($order_list);
            $this->set('order_list', $order_list);

            //$this->request->data['ProductInfo']['seltodate'] = $this->params['url']['seltodate'];
           // $this->request->data['ProductInfo']['selfrmdate'] = $this->params['url']['selfrmdate'];

            //$this->set('id', $id);
        
    }

    public function admin_generic_sold() {

        set_time_limit(0);
        ini_set('memory_limit', '256M');
        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));

        if (isset($this->params['url']['selfrmdate'])) {
            //Configure::write('debug',3);
            $conditions = array('recursive' => 3,
                'order' => array('OrderDist.createddt' => 'desc'),
                'conditions' => array(
                    'and' => array(
                        array('OrderDist.createddt <= ' => $this->params['url']['seltodate'] . ' 23:59',
                            'OrderDist.createddt >= ' => $this->params['url']['selfrmdate'] . ' 00:00',
                            'OrderDist.status' => array('Accep')
                        )
            )));
            $order_list = $this->OrderDist->find("all", $conditions);
            $orderlist_tmp = array();
            $TMP_ARRAY = array();
            foreach ($order_list as $key => $ords) {
                foreach ($ords['OrderdetailMany'] as $key2 => $ords2) {
                    if (array_key_exists($ords2['Product']['product_ndc'], $TMP_ARRAY)) {
                        $val = $TMP_ARRAY[$ords2['Product']['product_ndc']];
                        $TMP_ARRAY[$ords2['Product']['product_ndc']] = $val + $ords2['quantity'];
                    } else {
                        $TMP_ARRAY[$ords2['Product']['product_ndc']] = $ords2['quantity'];
                    }
                }
            }
            //print_r($TMP_ARRAY);die;
            $this->set('TMP_ARRAY', $TMP_ARRAY);
            //$this->paginate=$conditions;
            //$order_list=$this->paginate("Order");

            if (empty($order_list)) {
                $this->Session->setFlash(__('NO Order found between these dates.'), 'admin_error');
                $this->redirect(array('action' => 'admin_label_sheet'));
            }




            //print_r($order_list);
            $this->set('order_list', $order_list);

            $this->request->data['ProductInfo']['seltodate'] = $this->params['url']['seltodate'];
            $this->request->data['ProductInfo']['selfrmdate'] = $this->params['url']['selfrmdate'];

            $this->set('id', $id);
        }
    }

    public function admin_brand_sold_excel() {

        set_time_limit(0);
        ini_set('memory_limit', '256M');
        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));

        if (isset($this->params['url']['selfrmdate'])) {
            //Configure::write('debug',3);
            $conditions = array('recursive' => 3,
                'conditions' => array(
                    'and' => array(
                        array('OrderDist.createddt <= ' => $this->params['url']['seltodate'] . ' 23:59',
                            'OrderDist.createddt >= ' => $this->params['url']['selfrmdate'] . ' 00:00',
                            'OrderDist.status' => array('Accep')
                        )
            )));
            $order_list = $this->OrderDist->find("all", $conditions);
            //print_r($order_list);die;
            //$this->paginate=$conditions;
            //$order_list=$this->paginate("Order");

            if (empty($order_list)) {
                $this->Session->setFlash(__('NO Order found between these dates.'), 'admin_error');
                $this->redirect(array('action' => 'product_profit'));
            }

            $orderlist_tmp = array();
            $TMP_ARRAY = array();
            foreach ($order_list as $key => $ords) {
                foreach ($ords['OrderdetailMany'] as $key2 => $ords2) {
                    if (array_key_exists($ords2['Product']['product_ndc'], $TMP_ARRAY)) {
                        $val = $TMP_ARRAY[$ords2['Product']['product_ndc']];
                        $TMP_ARRAY[$ords2['Product']['product_ndc']] = $val + $ords2['quantity'];
                    } else {
                        $TMP_ARRAY[$ords2['Product']['product_ndc']] = $ords2['quantity'];
                    }
                }
            }

            $csvdata = array();
            $key = 0;
            $temarray = array();
            foreach ($order_list as $key1 => $order_list2) {
                foreach ($order_list2['OrderdetailMany'] as $key2 => $dt) {
                    if ($dt['Product']['producttype'] == 'Brand') {
                        if (!in_array($dt['Product']['product_ndc'], $temarray)) {
                            $temarray[] = $dt['Product']['product_ndc'];
                            /* $csvdata[$key]['Order Date']=date("m-d-Y",strtotime($order_list2['OrderDist']['createddt'])); */
                            $csvdata[$key]['Customer'] = $order_list2['User']['fname'];
                            $csvdata[$key]['Product NDC'] = $dt['Product']['product_ndc'];
                            $csvdata[$key]['Item Number'] = $dt['Product']['itemnumber'];
                            $csvdata[$key]['Product Name'] = $dt['Product']['productname'];
                            $csvdata[$key]['Quantity'] = $TMP_ARRAY[$dt['Product']['product_ndc']];
                            /* $csvdata[$key]['Single Itme Sold Price']=$dt['price'];
                              $csvdata[$key]['Total Itme Sold Price']=round($dt['price']*$dt['quantity'],2);
                              $csvdata[$key]['Single Itme Original Price']=round($dt['Product']['original_price'],2);
                              $csvdata[$key]['Total Itme Original Price']=round($dt['Product']['original_price']*$dt['quantity'],2);
                              $csvdata[$key]['Prof/Loss Per Item']=round($dt['price']-$user['Product']['original_price'],2);
                              $csvdata[$key]['Total Prof/Loss']=round(($dt['price']*$dt['quantity'])-($dt['quantity']*$dt['Product']['original_price']),2); */

                            $key++;
                        }
                    }
                }
            }
            $this->Export->exportCsv($csvdata);

            //print_r($order_list);
            $this->set('order_list', $order_list);
        } else {

            $this->Session->setFlash(__('NO Order found between these dates.'), 'admin_error');
            $this->redirect(array('action' => 'product_profit'));
        }
    }

    public function admin_brand_sold() {

        set_time_limit(0);
        ini_set('memory_limit', '256M');
        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));

        if (isset($this->params['url']['selfrmdate'])) {
            //Configure::write('debug',3);
            $conditions = array('recursive' => 3,
                'order' => array('OrderDist.createddt' => 'desc'),
                'conditions' => array(
                    'and' => array(
                        array('OrderDist.createddt <= ' => $this->params['url']['seltodate'] . ' 23:59',
                            'OrderDist.createddt >= ' => $this->params['url']['selfrmdate'] . ' 00:00',
                            'OrderDist.status' => array('Accep')
                        )
            )));
            $order_list = $this->OrderDist->find("all", $conditions);
            $orderlist_tmp = array();
            $TMP_ARRAY = array();
            foreach ($order_list as $key => $ords) {
                foreach ($ords['OrderdetailMany'] as $key2 => $ords2) {
                    if (array_key_exists($ords2['Product']['product_ndc'], $TMP_ARRAY)) {
                        $val = $TMP_ARRAY[$ords2['Product']['product_ndc']];
                        $TMP_ARRAY[$ords2['Product']['product_ndc']] = $val + $ords2['quantity'];
                    } else {
                        $TMP_ARRAY[$ords2['Product']['product_ndc']] = $ords2['quantity'];
                    }
                }
            }
            //print_r($TMP_ARRAY);die;
            $this->set('TMP_ARRAY', $TMP_ARRAY);
            //$this->paginate=$conditions;
            //$order_list=$this->paginate("Order");

            if (empty($order_list)) {
                $this->Session->setFlash(__('NO Order found between these dates.'), 'admin_error');
                $this->redirect(array('action' => 'admin_label_sheet'));
            }




            //print_r($order_list);
            $this->set('order_list', $order_list);

            $this->request->data['ProductInfo']['seltodate'] = $this->params['url']['seltodate'];
            $this->request->data['ProductInfo']['selfrmdate'] = $this->params['url']['selfrmdate'];

            $this->set('id', $id);
        }
    }

    public function admin_qk_pedigree() {

        $this->loadModel('TempPedigree');
        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));

        $order_list = array();
        $this->set('order_list', $order_list);

        $this->request->data['ProductInfo']['invoice'] = $this->params['url']['invoice'];

        //fetch pedgree
        if ($this->params['url']['invoice'] != '') {
            $INVOICE = $this->params['url']['invoice'];
            Configure::write('debug', 3);

            App::import('Vendor', 'CURL/Curl');
            $curl = new Curl();
            $curl->setCookie('CSID', '499150');
            $curl->setCookie('pos_pw', 'derou123');
            $curl->get('https://www.qkrx.com/cgi/cgictl.pgm?PGMNAME=BSTSO&MSGOK=MSGOK&OLDNEW=RB2&EMAIL=derou@me.com&PASSWORD=derou123', array());

            if ($curl->error) {
                //echo $curl->error;
                //return array("status"=>"false","INVOICE"=>$INVOICE);
            }

            $curl->get('http://www.qkrx.com/CGI/CGICTL.PGM?PN=BSTDET&IV=' . $INVOICE, array());

            if ($curl->error) {

                //$this->Session->setFlash(__('sorry,  could not be Found any Pedigree with INVOICE '.$INVOICE.'. Please, try again.'), 'admin_error');
                //return array("status"=>"false","INVOICE"=>$INVOICE);
            } else {

                if (preg_match('/Requested Was Not Found/', $curl->response)) {
                    //$this->Session->setFlash(__('sorry,  could not be Found any Pedigree with Invoice ID '.$INVOICE.'. Please, try again.'), 'admin_error');
                    //$this->ProductInfo->id = $pid;
                    //$this->ProductInfo->saveField('Pedigree', "CH");
                    $result = array("status" => "false", "INVOICE" => $INVOICE);
                } else {
                    $data = $curl->response;
                    $dom = new DOMDocument();
                    @$dom->loadHTML($data); //Lots of invalid html going on so I am suppressing the warnings
                    $xpathDom = new DOMXPath($dom);
                    $scarping_results = array();
                    //$tabledata=$this->dom_find_content($xpath,'//div[@class="span8 offset2"]//table[1]/tbody');							
                    //$studentNodes = $xpathDom->evaluate('/html/body/div[1]/div[3]/div/div/div/table[1]/tbody/tr[1]/th[2]');
                    $Vendor = "";
                    $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[2]/td[1]');
                    foreach ($row as $value) {
                        $Vendor = trim($value->textContent);
                    }
                    $scarping_results['Vendor_name'] = $Vendor;

                    $vendor_address = "";
                    $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[3]/td[1]');
                    foreach ($row as $value) {
                        $vendor_address = trim($value->textContent);
                    }
                    $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[4]/td[1]');
                    foreach ($row as $value) {
                        $vendor_address .=" " . trim($value->textContent);
                    }
                    $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[5]/td[1]');
                    foreach ($row as $value) {
                        $vendor_address .=" " . trim($value->textContent);
                    }
                    $scarping_results['vendor_address'] = $vendor_address;


                    $customer = "";
                    $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[2]/td[2]');
                    foreach ($row as $value) {
                        $customer = trim($value->textContent);
                    }
                    $scarping_results['Customer_Name'] = $customer;

                    $customer_address = "";
                    $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[3]/td[2]');
                    foreach ($row as $value) {
                        $customer_address = trim($value->textContent);
                    }
                    $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[4]/td[2]');
                    foreach ($row as $value) {
                        $customer_address .=" " . trim($value->textContent);
                    }
                    $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[5]/td[2]');
                    foreach ($row as $value) {
                        $customer_address .=" " . trim($value->textContent);
                    }
                    $scarping_results['Customer_address'] = $customer_address;


                    //shipt
                    $Shipto = "";
                    $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[2]/td[3]');
                    foreach ($row as $value) {
                        $Shipto = trim($value->textContent);
                    }
                    $scarping_results['Shipto_Name'] = $customer;

                    $Shipto_address = "";
                    $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[3]/td[3]');
                    foreach ($row as $value) {
                        $Shipto_address = trim($value->textContent);
                    }
                    $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[4]/td[3]');
                    foreach ($row as $value) {
                        $Shipto_address .=" " . trim($value->textContent);
                    }
                    $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[5]/td[3]');
                    foreach ($row as $value) {
                        $Shipto_address .=" " . trim($value->textContent);
                    }
                    $scarping_results['Shipto_address'] = $customer_address;

                    //invocie details
                    $Invoice = "";
                    $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[2]/td[4]');
                    foreach ($row as $value) {
                        $Invoice = trim($value->textContent);
                    }
                    $scarping_results['Invoice'] = str_replace("Invoice: ", "", $Invoice);

                    $PO = "";
                    $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[3]/td[4]');
                    foreach ($row as $value) {
                        $PO = trim($value->textContent);
                    }
                    $scarping_results['PO'] = $PO;

                    $Rep = "";
                    $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[4]/td[4]');
                    foreach ($row as $value) {
                        $Rep = trim($value->textContent);
                    }
                    $scarping_results['Rep'] = $Rep;
                    $Date = "";
                    $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[5]/td[4]');
                    foreach ($row as $value) {
                        $Date = trim($value->textContent);
                    }
                    $scarping_results['Date'] = $Date;

                    $Amount = "";
                    $row = $xpathDom->query('//div[@class="span8 offset2"]//table[1]/tr[6]/td[4]');
                    foreach ($row as $value) {
                        $Amount = trim($value->textContent);
                    }
                    $scarping_results['Amount'] = $Amount;

                    $ND_LOT = array();
                    $i = 2;
                    do {
                        $lotndc = array();
                        $NDC = "";
                        $row = $xpathDom->query('//table[@class="table2"]//tr[' . $i . ']/td[3]');
                        foreach ($row as $value) {
                            $NDC = trim($value->textContent);
                        }
                        $lotndc['NDC'] = $NDC;
                        $Description = "";
                        $row = $xpathDom->query('//table[@class="table2"]//tr[' . $i . ']/td[4]');
                        foreach ($row as $value) {
                            $Description = trim($value->textContent);
                        }
                        $lotndc['Description'] = $Description;

                        $Qty = "";
                        $row = $xpathDom->query('//table[@class="table2"]//tr[' . $i . ']/td[6]');
                        foreach ($row as $value) {
                            $Qty = trim($value->textContent);
                        }
                        $lotndc['Qty'] = $Qty;

                        $row = $xpathDom->query('//table[@class="table2"]//tr[' . $i . ']/td[5]');
                        foreach ($row as $value) {
                            $LOT = trim($value->textContent);
                        }
                        $lotndc['LOT'] = $LOT;

                        //GeT URL OF INVOIC
                        $url = "";
                        $ahrefs = $xpathDom->query('//table[@class="table2"]//tr[' . $i . ']/td[1]/a');
                        foreach ($ahrefs as $ahref) {
                            $url = $ahref->attributes->getNamedItem("href")->nodeValue;
                        }
                        $lotndc['IN_URL'] = $url;
                        if ($NDC != '')
                            $ND_LOT[] = $lotndc;
                        $i++;
                    }while ($NDC != '');
                    $scarping_results['ND_LOT'] = $ND_LOT;

                    //print_r($scarping_results);
                    //Find Lot Number and INvoice Number in table
                    if ($scarping_results['ND_LOT']) {
                        foreach ($scarping_results['ND_LOT'] as $key => $lotdt) {

                            $prodetajl = $this->ProductInfo->find('first', array('conditions' => array("AND" => array('ProductInfo.batchno' => $lotdt['LOT'], 'ProductInfo.InvoiceNo' => $scarping_results['Invoice'], 'Product.product_ndc' => $lotndc['NDC']))));

                            if (empty($prodetajl)) {
                                $curl->get($lotdt['IN_URL'], array());

                                if ($curl->error) {
                                    echo 'Error: ' . $curl->error_code . ': ' . $curl->error_message;
                                } else {
                                    $data = $curl->response;
                                    $dom = new DOMDocument();
                                    @$dom->loadHTML($data); //Lots of invalid html going on so I am suppressing the warnings
                                    $xpathDom = new DOMXPath($dom);

                                    $INVOICE_T1 = array();
                                    $i = 2;
                                    do {
                                        $Table_1 = array();
                                        $Item = "";
                                        $row = $xpathDom->query('//table[@class="table2"]//tr[' . $i . ']/td[1]');
                                        foreach ($row as $value) {
                                            $Item = trim($value->textContent);
                                        }
                                        $Table_1['Item'] = $Item;
                                        $Package_Size = "";
                                        $row = $xpathDom->query('//table[@class="table2"]//tr[' . $i . ']/td[3]');
                                        foreach ($row as $value) {
                                            $Package_Size = trim($value->textContent);
                                        }
                                        $Table_1['Package_Size'] = $Package_Size;

                                        $DosageForm = "";
                                        $row = $xpathDom->query('//table[@class="table2"]//tr[' . $i . ']/td[4]');
                                        foreach ($row as $value) {
                                            $DosageForm = trim($value->textContent);
                                        }
                                        $Table_1['DosageForm'] = $DosageForm;

                                        $Strength = "";
                                        $row = $xpathDom->query('//table[@class="table2"]//tr[' . $i . ']/td[5]');
                                        foreach ($row as $value) {
                                            $Strength = trim($value->textContent);
                                        }
                                        $Table_1['Strength'] = $Strength;

                                        $Expiration = "";
                                        $row = $xpathDom->query('//table[@class="table2"]//tr[' . $i . ']/td[8]');
                                        foreach ($row as $value) {
                                            $Expiration = trim($value->textContent);
                                        }
                                        $Table_1['Expiration'] = $Expiration;

                                        $Manufacturer = "";
                                        $row = $xpathDom->query('//table[@class="table2"]//tr[' . $i . ']/td[10]');
                                        foreach ($row as $value) {
                                            $Manufacturer = trim($value->textContent);
                                        }
                                        $Table_1['Manufacturer'] = $Manufacturer;
                                        if ($Item != '')
                                            $INVOICE_T1[] = $Table_1;
                                        $i++;
                                    }while ($Item != '');
                                    $scarping_results['ND_LOT'][$key]['INVOICE_T1'] = $INVOICE_T1;
                                    $scarping_results['ND_LOT'][$key]['ProductInfo_id'] = '';
                                    //Fetch Table Two data

                                    $row = $xpathDom->query('/html/body/div[1]/div[3]/div/div/table[2]');
                                    $Invoice_T2 = "";
                                    foreach ($row as $tag) {
                                        $innerHTML = '';

                                        // see http://fr.php.net/manual/en/class.domelement.php#86803
                                        $children = $tag->childNodes;
                                        foreach ($children as $child) {
                                            $tmp_doc = new DOMDocument();
                                            $tmp_doc->appendChild($tmp_doc->importNode($child, true));
                                            $innerHTML .= $tmp_doc->saveHTML();
                                        }
                                        if (trim($innerHTML) != '')
                                            $Invoice_T2 = "<table>" . (trim($innerHTML)) . "</table>";
                                    }
                                    $scarping_results['ND_LOT'][$key]['Invoice_T2'] = $Invoice_T2;


                                    $Invoice_P1 = "";
                                    $row = $xpathDom->query('/html/body/div[1]/div[3]/div/div/p[1]');
                                    foreach ($row as $value) {
                                        $Invoice_P1 = trim($value->textContent);
                                    }
                                    $scarping_results['ND_LOT'][$key]['Invoice_P1'] = $Invoice_P1;

                                    $Invoice_P2 = "";
                                    $row = $xpathDom->query('/html/body/div[1]/div[3]/div/div/p[2]/img');
                                    foreach ($row as $value) {
                                        $Invoice_P2 = $value->attributes->getNamedItem("src")->nodeValue;
                                    }
                                    $scarping_results['ND_LOT'][$key]['Invoice_P2'] = $Invoice_P2;

                                    $Invoice_P3 = "";
                                    $row = $xpathDom->query('/html/body/div[1]/div[3]/div/div/p[3]');
                                    foreach ($row as $value) {
                                        $Invoice_P3 = trim($value->textContent);
                                    }
                                    $scarping_results['ND_LOT'][$key]['Invoice_P3'] = $Invoice_P3;
                                }
                            }
                            /* }else{
                              $this->ProductInfo->id = $pid;
                              $this->ProductInfo->saveField('Pedigree', "CH");
                              } */
                        }

                        //print_r($scarping_results);die;
                    }

                    //save data 

                    if (!empty($scarping_results['ND_LOT'])) {
                        $successmsg = "";
                        foreach ($scarping_results['ND_LOT'] as $key => $sp_result) {
                            //check duplicate
                            $conditions = array(
                                'conditions' => array(
                                    'and' => array(
                                        array(
                                            'TempPedigree.Invoice' => $scarping_results['Invoice'],
                                            'TempPedigree.NDC' => $sp_result['NDC'],
                                            'TempPedigree.LOT' => $sp_result['LOT'],
                                        )
                            )));
                            $temp_pedgree = $this->TempPedigree->find("all", $conditions);
                            if (empty($temp_pedgree)) {
                                $data_array = array(
                                    //'product_infos_id'=>$sp_result['ProductInfo_id'],
                                    'Invoice' => $scarping_results['Invoice'],
                                    'NDC' => $sp_result['NDC'],
                                    'LOT' => $sp_result['LOT'],
                                    'Description' => $sp_result['Description'],
                                    'Qty' => $sp_result['Qty'],
                                    'INVOICE_T1' => json_encode($sp_result['INVOICE_T1']),
                                    'Invoice_T2' => json_encode($sp_result['Invoice_T2']),
                                    'Invoice_P1' => json_encode($sp_result['Invoice_P1']),
                                    'Invoice_P2' => json_encode($sp_result['Invoice_P2']),
                                    'Invoice_P3' => json_encode($sp_result['Invoice_P3']),
                                    'Other_details' => json_encode(array('Vendor_name' => $scarping_results['Vendor_name'], 'vendor_address' => $scarping_results['vendor_address'], 'Customer_Name' => $scarping_results['Customer_Name'], 'Customer_address' => $scarping_results['Customer_address'], 'Date' => $scarping_results['Date'], 'Amount' => $scarping_results['Amount'])),
                                    'Status' => 'Active',
                                    'created' => date("Y-m-d H:i:s")
                                );

                                //print_r($data_array);

                                $this->TempPedigree->create();
                                if ($this->TempPedigree->save($data_array)) {
                                    //$this->ProductInfo->id =$pid;
                                    //$this->ProductInfo->saveField('Pedigree', "Y");
                                    //$successmsg=$successmsg.$sp_result['LOT'].",";	 
                                }
                            }
                        }
                        //$this->Session->setFlash(__($successmsg), 'admin_success');
                        //return true;
                        //$this->redirect(array('action' => 'admin_manage_products'));
                        $result = array("status" => "true", "INVOICE" => $INVOICE, "successmsg" => $successmsg);
                    } else {
                        $result = array("status" => "false", "INVOICE" => $INVOICE);
                        //$this->redirect(array('action' => 'admin_manage_products'));
                    }
                }
            }
        }

        $conditions = array('fields' => array("DISTINCT  TempPedigree.Invoice"),
            'order' => array('TempPedigree.created' => 'desc'),
            'conditions' => array(
                'and' => array(
                    array(
                        'TempPedigree.Status' => 'Active',
						'TempPedigree.LOT !=' => '',
                    )
        )));
        $order_list = $this->TempPedigree->find("all", $conditions);
        $this->set('order_list', $order_list);
    }
	
	public function admin_qk_pedigree_delete($Invoice = null) {

        if (!$this->request->is('post')) {

            throw new MethodNotAllowedException();
        }

       $this->loadModel('TempPedigree');

        $status = $this->TempPedigree->updateAll(
                array('TempPedigree.Status' => "'Uploaded'"), array('TempPedigree.Invoice' => $Invoice)
        );
        if ($status) {

            $this->Session->setFlash(__('Invoice Deleted'), 'admin_success');

            $this->redirect(array('action' => 'admin_qk_pedigree'));
        }
        
        $this->Session->setFlash(__('Invoice was not deleted'), 'admin_error');

        $this->redirect(array('action' => 'admin_qk_pedigree'));
    }

    public function admin_qk_pedigree_popup($pedgreeid) {
        ///Configure::write('debug',3);
        $this->layout = "admin_dashboard";
        $this->loadModel('TempPedigree');
        $prodetajl = $this->TempPedigree->find('all', array('recursive' => 2, 'conditions' => array('TempPedigree.Invoice' => $pedgreeid, 'TempPedigree.product_infos_id' => 0, 'TempPedigree.Status' => 'Active','TempPedigree.LOT !=' => '')));

        $this->set("dhtml", $prodetajl);

        //$this->view='admin_pending_anda_pedigree';
    }

    public function admin_anda_pedigree() {

        Configure::write('debug', 3);
        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));

        // if (isset($this->params['url']['selfrmdate'])) {
        //Configure::write('debug',3);
        $conditions = array('fields' => array("DISTINCT  Pedigree.Invoice"), 'recursive' => 3,
            'order' => array('Pedigree.created' => 'desc'),
            'conditions' => array(
                'and' => array(
                    array(
                        'Pedigree.product_infos_id' => 0,
                        'Pedigree.anda' => 1
                    )
        )));
        $order_list = $this->Pedigree->find("all", $conditions);
        //print_r($order_list);die;
        //$this->paginate=$conditions;
        //$order_list=$this->paginate("Order");
        //print_r($order_list);
        $this->set('order_list', $order_list);

        /* $this->request->data['ProductInfo']['seltodate']=$this->params['url']['seltodate'];
          $this->request->data['ProductInfo']['selfrmdate']=$this->params['url']['selfrmdate'];

          $this->set('id', $id); */

        //}
    }
	
	
	public function admin_anda_lot_delete($Invoice = null) {

        if (!$this->request->is('post')) {

            throw new MethodNotAllowedException();
        }

       

        $status = $this->Pedigree->updateAll(
                array('Pedigree.product_infos_id' => 1), array('Pedigree.Invoice' => $Invoice,'Pedigree.anda' => 1)
        );
        if ($status) {

            $this->Session->setFlash(__('Invoice Deleted'), 'admin_success');

            $this->redirect(array('action' => 'admin_anda_pedigree'));
        }
        
        $this->Session->setFlash(__('Invoice was not deleted'), 'admin_error');

        $this->redirect(array('action' => 'admin_anda_pedigree'));
    }

    public function admin_anda_lot_add($id) {

        Configure::write('debug', 3);
        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));


        $conditions = array('recursive' => 3,
            'conditions' => array(
                'and' => array(
                    array(
                        'Pedigree.Invoice' => $id,
                        'Pedigree.product_infos_id' => 0,
                        'Pedigree.anda' => 1
                    )
        )));
        $pedgreelist = $this->Pedigree->find("all", $conditions);

        $error = array();
        if (!empty($pedgreelist)) {
            foreach ($pedgreelist as $pddetls) {
                $pedgree_json = json_decode($pddetls['Pedigree']['INVOICE_T1']);
                $products_product_ndcs = $this->Products->find("first", array('conditions' => array("Products.product_ndc" => $pddetls['Pedigree']['NDC']), 'fields' => array('product_ndc', 'id')));
                //print_r($products_product_ndcs);die;
                if (!empty($products_product_ndcs)) {
                    //count previous quenty
                    $previous_quentity = 0;

                    $data['ProductInfo']['prodid'] = $products_product_ndcs['Products']['id'];
                    $data['ProductInfo']['batchno'] = $pddetls['Pedigree']['LOT'];
                    $data['ProductInfo']['mfgdate'] = $pedgree_json->shippedPedigree->pedigree->receivedPedigree->initialPedigree->productInfo->manufacturer;
                    $data['ProductInfo']['expdate'] = date('Y-m-d', strtotime($pedgree_json->shippedPedigree->pedigree->receivedPedigree->receivingInfo->itemInfo->expirationDate));
                    $data['ProductInfo']['availability'] = $pedgree_json->shippedPedigree->itemInfo->quantity;
                    $data['ProductInfo']['balance'] = $pedgree_json->shippedPedigree->itemInfo->quantity;
                    $data['ProductInfo']['VendorName'] = $pedgree_json->shippedPedigree->transactionInfo->senderInfo->businessAddress->businessName;
                    $data['ProductInfo']['InvoiceNo'] = $pddetls['Pedigree']['Invoice'];
                    $data['ProductInfo']['ReceivedDate'] = date('Y-m-d', strtotime($pedgree_json->shippedPedigree->pedigree->receivedPedigree->initialPedigree->receivingInfo->dateReceived));
                    $data['ProductInfo']['Pedigree'] = "Y";

                    $data['ProductInfo']['createddt'] = date("Y-m-d");

                    $this->ProductInfo->create();

                    if ($this->ProductInfo->save($data)) {


                        //Add product update acitvity

                        $activit_data = array(
                            'product_id' => $products_product_ndcs['Products']['id'],
                            'product_info_id' => $this->ProductInfo->id,
                            'user_id' => $this->Auth->user('id'),
                            'previous_qty' => $previous_quentity,
                            'new_qty' => $pedgree_json->shippedPedigree->itemInfo->quantity,
                            'note' => "Add by Anda AS2",
                            'created' => date("Y-m-d H:i:s")
                        );
                        $this->admin_add_product_activity($activit_data);

                        $this->Pedigree->updateAll(
                                array('Pedigree.product_infos_id' => $this->ProductInfo->id), array('Pedigree.id' => $pddetls['Pedigree']['id'])
                        );
                        $this->Session->setFlash(__('The Lot has been Added'), 'admin_success');
                        $error[] = 'The Lot has been Added NDC- ' . $pddetls['Pedigree']['NDC'] . '.';
                        //$this->redirect(array('action' => 'admin_anda_pedigree'));
                    } else {

                        $this->Session->setFlash(__('The Product could not be saved. Please, try again.'), 'admin_error');
                        $error[] = 'The Product(' . $pddetls['Pedigree']['NDC'] . ') could not be saved. Please, try again.';
                        //$this->redirect(array('action' => 'admin_anda_pedigree'));
                    }
                } else {
                    $this->Session->setFlash(__('NO Products found with this NDC.'), 'admin_error');
                    $error[] = 'NO Products found with this NDC ' . $pddetls['Pedigree']['NDC'] . '.';
                    //$this->redirect(array('action' => 'admin_anda_pedigree'));
                }
            }
            if (!empty($error)) {
                $this->Session->setFlash(__(implode(',', $error)), 'admin_error');
                $this->redirect(array('action' => 'admin_anda_pedigree'));
            } else {
                $this->Session->setFlash(__('The Lot has been Uploaded'), 'admin_success');
                $this->redirect(array('action' => 'admin_anda_pedigree'));
            }
        } else {
            $this->Session->setFlash(__('NO Lot found with this Invoice Number.'), 'admin_error');
            $this->redirect(array('action' => 'admin_anda_pedigree'));
        }
    }

    public function admin_product_profit() {

        set_time_limit(0);
        ini_set('memory_limit', '256M');
        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));

        if (isset($this->params['url']['selfrmdate'])) {
            //Configure::write('debug',3);
            $conditions = array('recursive' => 3,
                'order' => array('OrderDist.createddt' => 'desc'),
                'conditions' => array(
                    'and' => array(
                        array('OrderDist.createddt <= ' => $this->params['url']['seltodate'] . ' 23:59',
                            'OrderDist.createddt >= ' => $this->params['url']['selfrmdate'] . ' 00:00',
                            'OrderDist.status' => array('Accep')
                        )
            )));
            $order_list = $this->OrderDist->find("all", $conditions);
            //print_r($order_list);die;
            //$this->paginate=$conditions;
            //$order_list=$this->paginate("Order");

            if (empty($order_list)) {
                $this->Session->setFlash(__('NO Order found between these dates.'), 'admin_error');
                $this->redirect(array('action' => 'admin_label_sheet'));
            }




            //print_r($order_list);
            $this->set('order_list', $order_list);

            $this->request->data['ProductInfo']['seltodate'] = $this->params['url']['seltodate'];
            $this->request->data['ProductInfo']['selfrmdate'] = $this->params['url']['selfrmdate'];

            $this->set('id', $id);
        }
    }

    public function admin_product_profit_excel() {

        set_time_limit(0);
        ini_set('memory_limit', '256M');
        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));

        if (isset($this->params['url']['selfrmdate'])) {
            //Configure::write('debug',3);
            $conditions = array('recursive' => 3,
                'conditions' => array(
                    'and' => array(
                        array('OrderDist.createddt <= ' => $this->params['url']['seltodate'] . ' 23:59',
                            'OrderDist.createddt >= ' => $this->params['url']['selfrmdate'] . ' 00:00',
                            'OrderDist.status' => array('Accep')
                        )
            )));
            $order_list = $this->OrderDist->find("all", $conditions);
            //print_r($order_list);die;
            //$this->paginate=$conditions;
            //$order_list=$this->paginate("Order");

            if (empty($order_list)) {
                $this->Session->setFlash(__('NO Order found between these dates.'), 'admin_error');
                $this->redirect(array('action' => 'product_profit'));
            }

            $csvdata = array();
            $key = 0;
            foreach ($order_list as $key1 => $order_list2) {
                foreach ($order_list2['OrderdetailMany'] as $key2 => $dt) {

                    $csvdata[$key]['Order Date'] = date("m-d-Y", strtotime($order_list2['OrderDist']['createddt']));
                    $csvdata[$key]['Customer'] = $order_list2['User']['fname'];
                    $csvdata[$key]['Product NDC'] = $dt['Product']['product_ndc'];
                    $csvdata[$key]['Item Number'] = $dt['Product']['itemnumber'];
                    $csvdata[$key]['Product Name'] = $dt['Product']['productname'];
                    $csvdata[$key]['Quantity'] = $dt['quantity'];
                    $csvdata[$key]['Single Itme Sold Price'] = $dt['price'];
                    $csvdata[$key]['Total Itme Sold Price'] = round($dt['price'] * $dt['quantity'], 2);
                    $csvdata[$key]['Single Itme Original Price'] = round($dt['Product']['original_price'], 2);
                    $csvdata[$key]['Total Itme Original Price'] = round($dt['Product']['original_price'] * $dt['quantity'], 2);
                    $csvdata[$key]['Prof/Loss Per Item'] = round($dt['price'] - $user['Product']['original_price'], 2);
                    $csvdata[$key]['Total Prof/Loss'] = round(($dt['price'] * $dt['quantity']) - ($dt['quantity'] * $dt['Product']['original_price']), 2);

                    $key++;
                }
            }
            $this->Export->exportCsv($csvdata);

            //print_r($order_list);
            $this->set('order_list', $order_list);
        } else {

            $this->Session->setFlash(__('NO Order found between these dates.'), 'admin_error');
            $this->redirect(array('action' => 'product_profit'));
        }
    }

    public function admin_andapedigree() {
        Configure::write('debug', 3);
        $files1 = scandir("/var/lib/tomcat7/webapps/rssbus/WEB-INF/data/as2connector/profiles/ZZEPEDIGREE/Incoming");
        $files = array_diff($files1, array('.', '..'));
        $successmsg = '';
        foreach ($files as $name => $file) {
            $xml = simplexml_load_file("/var/lib/tomcat7/webapps/rssbus/WEB-INF/data/as2connector/profiles/ZZEPEDIGREE/Incoming/$file") or die("Error: Cannot create object");
            $product_ndec = ($xml->shippedPedigree->pedigree->receivedPedigree->initialPedigree->productInfo->productCode);
            $lot_no = $xml->shippedPedigree->pedigree->receivedPedigree->initialPedigree->itemInfo->lot;
            $InvoiceNumber = $xml->shippedPedigree->transactionInfo->transactionIdentifier->identifier;
            if ($product_ndec != '' and $lot_no != '') {

                $productinfo = $this->ProductInfo->find('all', array(
                    'conditions' => array('ProductInfo.batchno' => $lot_no, 'ProductInfo.VendorName' => 'ANDA', 'Product.product_ndc' => $product_ndec)));

                if (!empty($productinfo)) {
                    foreach ($productinfo as $prdifno) {
                        $prodetajl = $this->Pedigree->find('count', array('recursive' => 2, 'conditions' => array('Pedigree.product_infos_id' => $prdifno['ProductInfo']['id'])));

                        if ($prodetajl <= 0) {
                            $data_array = array(
                                'product_infos_id' => $prdifno['ProductInfo']['id'],
                                'Invoice' => $InvoiceNumber,
                                'NDC' => $product_ndec,
                                'LOT' => $lot_no,
                                'INVOICE_T1' => json_encode($xml),
                                'Invoice_T2' => '',
                                'Invoice_P1' => '',
                                'Invoice_P2' => '',
                                'Invoice_P3' => '',
                                'Other_details' => '',
                                'anda' => 1,
                                'created' => date("Y-m-d H:i:s")
                            );

                            $this->Pedigree->create();
                            if ($this->Pedigree->save($data_array)) {
                                $this->ProductInfo->id = $prdifno['ProductInfo']['id'];
                                $this->ProductInfo->saveField('Pedigree', "Y");
                                $successmsg = $successmsg . $lot_no . ",";
                            }
                        }
                    }
                }
            }
        }
        $this->Session->setFlash(__($successmsg), 'admin_success');
        $this->redirect(array('action' => 'manage_products'));

        echo $successmsg;
        die;
    }

    function admin_anda_pending_peidgree() {

        Configure::write('debug', 3);
        $files1 = scandir("/var/lib/tomcat7/webapps/rssbus/WEB-INF/data/as2connector/profiles/ZZEPEDIGREE/Incoming");
        $files = array_diff($files1, array('.', '..'));
        $successmsg = '';
        foreach ($files as $name => $file) {
            $xml = simplexml_load_file("/var/lib/tomcat7/webapps/rssbus/WEB-INF/data/as2connector/profiles/ZZEPEDIGREE/Incoming/$file") or die("Error: Cannot create object");
            $product_ndec = ($xml->shippedPedigree->pedigree->receivedPedigree->initialPedigree->productInfo->productCode);
            $lot_no = $xml->shippedPedigree->pedigree->receivedPedigree->initialPedigree->itemInfo->lot;
            $InvoiceNumber = $xml->shippedPedigree->transactionInfo->transactionIdentifier->identifier;
            if ($product_ndec != '' and $lot_no != '') {

                $productinfo = $this->ProductInfo->find('all', array(
                    'conditions' => array('ProductInfo.batchno' => $lot_no, 'ProductInfo.VendorName' => 'ANDA', 'Product.product_ndc' => $product_ndec, 'ProductInfo.InvoiceNo' => $InvoiceNumber)));

                if (!empty($productinfo)) {
                    foreach ($productinfo as $prdifno) {
                        $prodetajl = $this->Pedigree->find('count', array('recursive' => 2, 'conditions' => array('Pedigree.product_infos_id' => $prdifno['ProductInfo']['id'])));

                        if ($prodetajl <= 0) {
                            $data_array = array(
                                'product_infos_id' => $prdifno['ProductInfo']['id'],
                                'Invoice' => $InvoiceNumber,
                                'NDC' => $product_ndec,
                                'LOT' => $lot_no,
                                'INVOICE_T1' => json_encode($xml),
                                'Invoice_T2' => '',
                                'Invoice_P1' => '',
                                'Invoice_P2' => '',
                                'Invoice_P3' => '',
                                'Other_details' => '',
                                'anda' => 1,
                                'created' => date("Y-m-d H:i:s")
                            );

                            $this->Pedigree->create();
                            if ($this->Pedigree->save($data_array)) {
                                $this->ProductInfo->id = $prdifno['ProductInfo']['id'];
                                $this->ProductInfo->saveField('Pedigree', "Y");
                                $successmsg = $successmsg . $lot_no . ",";
                            }
                        }
                    }
                } else {

                    //print_r($conditions);
                    $prodetajl = $this->Pedigree->find('all', array('conditions' => array('AND' => array('Pedigree.Invoice' => $InvoiceNumber, 'Pedigree.LOT' => $lot_no))));
                    //echo  $prodetajl;

                    if (empty($prodetajl)) {
                        $data_array = array(
                            'product_infos_id' => 0,
                            'Invoice' => $InvoiceNumber,
                            'NDC' => $product_ndec,
                            'LOT' => $lot_no,
                            'INVOICE_T1' => json_encode($xml),
                            'Invoice_T2' => '',
                            'Invoice_P1' => '',
                            'Invoice_P2' => '',
                            'Invoice_P3' => '',
                            'Other_details' => '',
                            'anda' => 1,
                            'created' => date("Y-m-d H:i:s")
                        );

                        $this->Pedigree->create();
                        if ($this->Pedigree->save($data_array)) {
                            //$this->ProductInfo->id =$prdifno['ProductInfo']['id'];
                            //$this->ProductInfo->saveField('Pedigree', "Y");
                            $successmsg = $successmsg . $lot_no . ",";
                        }
                    }
                }
            }
        }
        $this->Session->setFlash(__($successmsg), 'admin_success');
        $this->redirect(array('action' => 'anda_pedigree'));

        echo $successmsg;
        die;
    }
	
	
	
	    public function admin_qkedi_pedigree() {

        Configure::write('debug', 3);
		$this->loadModel('TempPedigreesEDI');
        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));

        // if (isset($this->params['url']['selfrmdate'])) {
        //Configure::write('debug',3);
        $this->paginate=array('fields' => array("TempPedigreesEDI.Invoice","Invoice","TempPedigreesEDI.id","TempPedigreesEDI.Other_details"), 'recursive' => 3,
            'order' => array('TempPedigreesEDI.created' => 'desc'),
			 'group' => '`TempPedigreesEDI`.`Invoice`',
            'conditions' => array(
                'and' => array(
                    array(                        
                        'TempPedigreesEDI.Status' =>'Active'
                    )
        )));
        $order_list = $this->paginate("TempPedigreesEDI");
        
        $this->set('order_list', $order_list);

        
    }
	
	public function admin_qkedi_pedigree_popup($pedgreeid) {
        Configure::write('debug',3);
        $this->layout = "admin_dashboard";
        $this->loadModel('TempPedigreesEDI');
        $prodetajl = $this->TempPedigreesEDI->find('all', array('recursive' => 2, 'conditions' => array('TempPedigreesEDI.Invoice' => $pedgreeid,  'TempPedigreesEDI.Status' => 'Active','TempPedigreesEDI.LOT !=' => '')));
		//print_r($prodetajl);
        $this->set("dhtml", $prodetajl);

        //$this->view='admin_pending_anda_pedigree';
    }
	
	public function admin_qkedi_lot_delete($Invoice = null) {
		Configure::write('debug', 2);
		$this->loadModel('TempPedigreesEDI');
        if (!$this->request->is('post')) {

            throw new MethodNotAllowedException();
        }

       

        $status = $this->TempPedigreesEDI->updateAll(
                array('TempPedigreesEDI.Status' => "'Deactive'"), array('TempPedigreesEDI.Invoice' =>trim($Invoice))
        );
        if ($status) {

            $this->Session->setFlash(__('Invoice Deleted'), 'admin_success');

            $this->redirect(array('action' => 'admin_qkedi_pedigree'));
        }
        
        $this->Session->setFlash(__('Invoice was not deleted'), 'admin_error');

        $this->redirect(array('action' => 'admin_qkedi_pedigree'));
    }

	
	function admin_parseQkEdi(){
		//Configure::write('debug',3);
		$this->loadModel('TempPedigree');
		 $this->loadModel('TempPedigreesEDI');	
								// set up basic connection
						$conn_id = ftp_connect("63.87.220.88");
						
						// login with username and password
						$login_result = ftp_login($conn_id,"Node2103", "Ma7896");
						
						// get contents of the current directory
						$contents = ftp_nlist($conn_id, "Inbox");
						
						// output $contents
						$successmsg='';
						$content=array();
					
						foreach ($contents as $key=>$file)
						{
							
							// Full path to a remote file
							$remote_path = "Inbox/$file";
							// Path to a temporary local copy of the remote file
							$temp_path = tempnam(sys_get_temp_dir(), "ftp");
							// Temporarily download the file
							ftp_get($conn_id, $temp_path, $remote_path, FTP_BINARY);
							// Read the contents of temporary copy
							$contents = file_get_contents($temp_path);
							$file = fopen("/var/www/app/webroot/qk_pedgree.txt","w+");
							
							
							fwrite($file,$contents);
							fclose($file);
							
							$result=$this->QkParse();
							//check inovice and lot in database
							
							
							foreach($result['productdetail'] as $key=>$rt){
							
							
							  $prodetajl = $this->ProductInfo->find('first', array('conditions' => array("AND" => array('ProductInfo.batchno' =>trim($rt[0]['LOT']), 'ProductInfo.InvoiceNo' =>trim($result['REF'][0]['Invoice/OrderNumber']),'ProductInfo.VendorName' =>'QK', 'Product.product_ndc' =>trim($rt[0]['Product/ServiceID'])))));
							
							
							
							  if(empty($prodetajl)){
								   $TempPedigreesEDI = $this->TempPedigreesEDI->find('first', array('conditions' => array("AND" => array('TempPedigreesEDI.LOT' =>trim($rt[0]['LOT']), 'TempPedigreesEDI.Invoice'=>trim($result['REF'][0]['Invoice/OrderNumber']),'TempPedigreesEDI.NDC' =>trim($rt[0]['Product/ServiceID'])))));
								   if(empty($TempPedigreesEDI)){	
									$data_array = array(
                                    //'product_infos_id'=>$sp_result['ProductInfo_id'],
                                    'Invoice' =>trim($result['REF'][0]['Invoice/OrderNumber']),
                                    'NDC' =>trim($rt[0]['Product/ServiceID']),
                                    'LOT' =>trim($rt[0]['LOT']),
                                    'Description' => trim($rt[2]['Description']),
                                    'Qty' =>trim($rt[1]['QuantityOrdered']),
                                    'INVOICE_T1' => json_encode($result['N1']),
                                    'Invoice_T2' => json_encode($rt),                                    
                                    'Other_details' => json_encode(array('RecivedDate' =>trim($result['DTM'][0]['Date']), 'exp_date' =>trim($rt['3']['Date']))),
                                    'Status' => 'Active',
                                    'created' => date("Y-m-d H:i:s")
                                );
							$tarray=array();
                              $tarray['TempPedigreesEDI']=$data_array;
								
                                $this->TempPedigreesEDI->create();
                                if ($this->TempPedigreesEDI->save($data_array)) {
									
                                    //$this->ProductInfo->id =$pid;
                                   // $this->ProductInfo->saveField('Pedigree', "Y");
                                   $successmsg=$successmsg.trim($rt[0]['LOT']).",";	 
                                }	
								   }
							  }
							  
							}
							
							$content[$file] = $contents;
							// Discard the temporary copy
							unlink($temp_path);
							
						}
						$this->Session->setFlash(__($successmsg), 'admin_success');
        				$this->redirect(array('action' => 'qkedi_pedigree'));
						
	}
public function QkParse(){
	//Configure::write('debug',3);
		$globalAeray=array();
					$product_detail=array();
					$inLIN=false;
					$myfile = fopen("/var/www/app/webroot/qk_pedgree.txt","r");
					if ($myfile) {
						//echo $myfile;die;
				while (($line = fgets($myfile)) !== false) {
					
					// process the line read.
					  if($this->startsWith($line,'ISA*')){
						  $globalAeray["ISA"]=$this->parseISA($line);
					  }
					  
					  if($this->startsWith($line,'GS*')){
						  $globalAeray["GS"]=$this->parseGS($line);
					  }
					  
					  if($this->startsWith($line,'ST*')){
						  $globalAeray["ST"]=$this->parseST($line);
					  }
					  if($this->startsWith($line,'SE*')){
						  $globalAeray["SE"]=$this->parseSE($line);
					  }
					  if($this->startsWith($line,'GE*')){
						  $globalAeray["GE"]=$this->parseGE($line);
					  }
					  if($this->startsWith($line,'IEA*')){
						  $globalAeray["IEA"]=$this->parseIEA($line);
					  }
					  if($this->startsWith($line,'BSN*')){
						  $globalAeray["BSN"]=$this->parseBSN($line);
					  }
					/*  if($this->startsWith($line,'HL*')){
						  $globalAeray["HL"]=$this->parseHL($line);
					  }*/
					  if($this->startsWith($line,'HL*')){
						  $globalAeray["HL"][]=$this->parseHL($line);
					  }
					  if($this->startsWith($line,'LIN*')){
						 // $globalAeray["LIN"][]=$this->parseLIN($line);
						 $inLIN=true;
						 $product_detail[]=$this->parseLIN($line);
					  }
					  if($this->startsWith($line,'DTM*')){
						  if($inLIN)
						  $product_detail[]=$this->parseDTM($line);
						  else
						  $globalAeray["DTM"][]=$this->parseDTM($line);
					  }
					  if($this->startsWith($line,'N1*')){
						  if($inLIN)
						  $product_detail[]=$this->parseN1($line);
						  else
						  $globalAeray["N1"][]=$this->parseN1($line);
					  }
					   if($this->startsWith($line,'N3*')){
						   if($inLIN)
						  $product_detail[]=$this->parseN3($line);
						  else
						  $globalAeray["N3"][]=$this->parseN3($line);
					  }
					  if($this->startsWith($line,'N4*')){
						  if($inLIN)
						  $product_detail[]=$this->parseN4($line);
						  else
						  $globalAeray["N4"][]=$this->parseN4($line);
					  }
					  if($this->startsWith($line,'YNQ*')){
						  if($inLIN){
							$product_detail[]=$this->parseYNQ($line);
							$globalAeray["productdetail"][]=$product_detail;
							$product_detail=array();
							$inLIN=false;
						  }else{
						  $globalAeray["YNQ"][]=$this->parseYNQ($line);
						  }
					  }
					  if($this->startsWith($line,'REF*')){
						  if($inLIN)
						  $product_detail[]=$this->parseREF($line);
						  else
						  $globalAeray["REF"][]=$this->parseREF($line);
					  }
					  
					  if($this->startsWith($line,'SN1*')){
						  if($inLIN)
						  $product_detail[]=$this->parseSN1($line);
						  else
						  $globalAeray["SN1"][]=$this->parseSN1($line);
					  }
					  if($this->startsWith($line,'PID*')){
						  if($inLIN)
						  $product_detail[]=$this->parsePID($line);
						  else
						  $globalAeray["PID"][]=$this->parsePID($line);
					  }
					//echo $line;die;
							
						}
					
						fclose($myfile);
						
						$result=array();
						foreach($globalAeray['productdetail'] as $key=>$pdtetial){	
						$lotunc=$pdtetial[0]['Product/ServiceID'].$pdtetial[0]['LOT'].$pdtetial[0]['SKU'];						
							if(array_key_exists($lotunc,$result)){
								$result[trim($lotunc)][1]['QuantityOrdered']=($result[$lotunc][1]['QuantityOrdered']+$pdtetial[1]['QuantityOrdered']);
							
							 
							}else{	
								$lotunc=$pdtetial[0]['Product/ServiceID'].$pdtetial[0]['LOT'].$pdtetial[0]['SKU'];							
								$result[trim($lotunc)]=$pdtetial; 
								
							}
							
						}
						
						$globalAeray['productdetail']=$result;
						return $globalAeray;
					} else {
						// error opening the file.
					}
	}
	function parseISA($string){
		$data=explode("*",$string);
		//print_r($data);
		$tdata=array();
		$tdata['AuthorizationInformationQualifier']=$data[1];
		$tdata['AuthorizationInformation']=$data[2];
		$tdata['SecurityInformationQualifier']=$data[3];
		$tdata['SecurityInformation']=$data[4];
		$tdata['InterchangeIDQualifier']=$data[5];
		$tdata['InterchangeSenderID']=$data[6];
		$tdata['InterchangeIDQualifier']=$data[7];
		$tdata['InterchangeReceiverID']=$data[8];
		$tdata['InterchangeDate']=$data[9];
		$tdata['InterchangeTime']=$data[10];
		$tdata['InterchangeControlStandardsID']=$data[11];
		$tdata['InterchangeControlVersionNumber']=$data[12];
		$tdata['InterchangeControlNumber']=$data[13];
		$tdata['AcknowledgementRequested']=$data[14];
		$tdata['TestIndicator']=$data[15];
		$tdata['SubelementSeparator']=$data[16];
		return $tdata;
	}
	
	function parseGS($string){
		$data=explode("*",$string);
		//print_r($data);die;
		$tdata=array();
		$tdata['FunctionalIDcode']=$data[1];
		$tdata['ApplicationSendersCode']=$data[2];
		$tdata['ApplicationReceiversCode']=$data[3];
		$tdata['Date']=$data[4];
		$tdata['Time']=$data[5];
		$tdata['GroupControlNumber']=$data[6];
		$tdata['ResponsibleAgencyCode']=$data[7];
		$tdata['VersionRelIndIDCode']=$data[8];
		
		return $tdata;
	}
	
	function parseST($string){
		$data=explode("*",$string);
		//print_r($data);die;
		$tdata=array();
		$tdata['TransactionsetIDcode']=$data[1];
		$tdata['Transactionsetcontrolnumber']=$data[2];
		return $tdata;
	}
	
	function parseSE($string){
		$data=explode("*",$string);
		//print_r($data);die;
		$tdata=array();
		$tdata['Numberofincludedsegments']=$data[1];
		$tdata['Transactionsetcontrolnumber']=$data[2];
		return $tdata;
	}
	
	function parseGE($string){
		$data=explode("*",$string);
		//print_r($data);die;
		$tdata=array();
		$tdata['NumberofTransactionSetFunctionGroup']=$data[1];
		$tdata['GroupControlNumber']=$data[2];
		return $tdata;
	}
	
	function parseIEA($string){
		$data=explode("*",$string);
		//print_r($data);die;
		$tdata=array();
		$tdata['NumberofIncludedFunctionalGroups']=$data[1];
		$tdata['InterchangeControlNumber']=$data[2];
		return $tdata;
	}
	
	function parseBSN($string){
		$data=explode("*",$string);
		//print_r($data);die;
		$tdata=array();
		$tdata['TransactionSetPurposeCode']=$data[1];
		$tdata['ShipmentIdentification']=$data[2];
		$tdata['Date']=$data[3];
		$tdata['Time']=$data[4];
		return $tdata;
	}
	
	function parseHL($string){
		$data=explode("*",$string);
		//print_r($data);die;
		$tdata=array();
		$tdata['HierarchicalIDNumber']=$data[1];
		$tdata['HierarchicalLevelCode']=$data[2];
		$tdata['HierarchicalChildCode']=$data[3];
		return $tdata;
	}
	function parseDTM($string){
		$data=explode("*",$string);
		//print_r($data);die;
		$tdata=array();
		$tdata['Date/Time']=$data[1];
		$tdata['Date']=$data[2];
		
		return $tdata;
	}
	function parseN1($string){
		$data=explode("*",$string);
		//print_r($data);die;
		$tdata=array();
		$tdata['EntityIdentifie']=$data[1];
		$tdata['Name']=$data[2];
		
		return $tdata;
	}
	function parseN3($string){
		$data=explode("*",$string);
		//print_r($data);die;
		$tdata=array();
		$tdata['Address']=$data[1];		
		
		return $tdata;
	}
	function parseN4($string){
		$data=explode("*",$string);
		//print_r($data);die;
		$tdata=array();
		$tdata['CityName']=$data[1];		
		$tdata['StateorProvinceCode']=$data[2];
		$tdata['PostalCode']=$data[3];
		/*$tdata['CountryCode']=$data[4];
		$tdata['LocationQualifier']=$data[5];
		$tdata['LocationIdentifier']=$data[6];*/
		return $tdata;
	}
	
	function parseYNQ($string){
		$data=explode("*",$string);
		//print_r($data);die;
		$tdata=array();
		$tdata['ResponseCode']=$data[2];		
		$tdata['Free-FormMessageText']=$data[5];
		$tdata['CodeListQualifierCode']=$data[8];
		$tdata['IndustryCode']=$data[8];
		
		return $tdata;
	}
	function parseREF($string){
		$data=explode("*",$string);
		//print_r($data);die;
		$tdata=array();
		if(count($data)==4){
		$tdata['ReferenceIdentification']=$data[1];		
		$tdata['DeliveryReferenceDate']=$data[2];
		$tdata['AdvanceShipNoticeNumber']=$data[3];	
		}else{
		$tdata['ReferenceIdentification']=$data[1];		
		$tdata['Invoice/OrderNumber']=$data[2];	
		}
		return $tdata;
	}
	
	function parseLIN($string){
		$data=explode("*",$string);
		//print_r($data);die;
		$tdata=array();
		$tdata['AssignedIdentification']=$data[1];		
		$tdata['NDC']=$data[2];
		$tdata['Product/ServiceID']=$data[3];
		$tdata['LotNumber']=$data[4];
		$tdata['LOT']=$data[5];
		$tdata['ServiceID']=$data[6];
		$tdata['SKU']=$data[7];
		
		return $tdata;
	}
	
	function parseSN1($string){
		$data=explode("*",$string);
		//print_r($data);die;
		$tdata=array();
		$tdata['AssignedIdentification']=$data[1];		
		$tdata['NumberofUnitsShipped ']=$data[2];
		$tdata['UnitorBasisforMeasurementCode']=$data[3];
		$tdata['QuantityShippedtoDate']=$data[4];
		$tdata['QuantityOrdered']=$data[5];
		$tdata['UnitorBasisforMeasurementCode']=$data[6];
		
		return $tdata;
	}
	
	function parsePID($string){
		$data=explode("*",$string);
		//print_r($data);die;
		$tdata=array();
		$tdata['ItemDescriptionType']=$data[1];		
		$tdata['Product/ProcessCharacteristicCode']=$data[2];
		$tdata['AgencyQualifierCode']=$data[3];
		$tdata['ProductDescriptionCode']=$data[4];
		$tdata['Description']=$data[5];
			
		return $tdata;
	}
	
	function startsWith($haystack, $needle) {
		// search backwards starting from haystack length characters from the end
		return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
	}
	function endsWith($haystack, $needle) {
		// search forward starting from end minus needle length characters
		return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
	}

}
