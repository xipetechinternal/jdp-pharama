<?php
App::uses('CakeTime', 'Utility');
App::uses('AppController', 'Controller');

App::uses('CakeEmail', 'Network/Email');



/**

 * Users Controller

 *

 * @property User $User

 */

class DataentryController extends AppController {

      

       public $components = array('RequestHandler','Cookie');

   public $uses = array('User','EmailTemplateDescription','Order','Orderdetail','Products','ProductInfo','Product','OrderLot','ProductActivity');

   public $paginate = array('limit' => 10);

   function beforeFilter() {

        parent::beforeFilter();

        $this->layout = "default";

		$this->Auth->allow('forgot_password','forgot_username');

		$this->Auth->loginAction = '/Dataentry/login';

        $this->Auth->logoutRedirect = '/Dataentry/login';

        $this->Auth->loginRedirect = '/Dataentry/index';

        

		
		

    }

public function isAuthorized($user) {
    // All registered users can add posts
	
     if ($user['level'] == 'data') {
        return true;
    }
	$this->Session->setFlash('Sorry, you don\'t have permission to access that page.');
                 $this->redirect('login');
                 return false;
    return parent::isAuthorized($user);
}

	

	


	public function index(){
		
		 $this->layout = "dataentry_index";	
		 
		}
	public function forgot_password(){
		 $this->layout = "dataentry_index";
		 
		 if ($this->request->is('post')) {
			
			$user_active = $this->User->find('first',array('fields'=>array('active','fname','email','id','username'),'conditions'=>array('User.username'=>$this->request->data['Dataentry']['username'],'level'=>'data')));
 		      
				$username=$user_active['User']['username'];
				$email=$user_active['User']['email'];	
			if(!empty($user_active)){
			
			/*** Encript password before insert to DB  ***/
			$password = ($tmppass);
			
			
				
				$new_password = substr( str_shuffle( 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$' ) , 0 , 8 );
				
				
				$this->User->id = $user_active['User']['id'];				
				$this->User->saveField('password',$new_password);
				
				$message=<<<HTM
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
</HEAD>

<BODY>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>Hi User,<br>
      <br>
Password for $username has been reset.<br>
<br>
Heres all the information you need to get you started -<br>
<br>
------------------<br>
Login Information:<br>
------------------<br>
User ID: $username<br>
Password: $new_password</td>
  </tr>
</table>

</BODY>
HTM;

// subject
$subject = 'Forgot Password';

// To send HTML mail, the Content-type header must be set
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= 'From: jdpwholesaler.com <info@jdpwholesaler.com>' . "\r\n";
// Mail it
mail($email, $subject, $message, $headers);
			
			
			$this->Session->setFlash(__('We have send your password to your email address.'), 'admin_success');
			
			}else {
					//$this->Session->setFlash(__('User not active'),'alert',array('class'=>'alert-error'));
					 $this->Session->setFlash(__('User not found'), 'error',array('class'=>'alert-error'));
				}
		 }
	}
	public function forgot_username(){
		 $this->layout = "dataentry_index";
		 
		 if ($this->request->is('post')) {
			
			$user_active = $this->User->find('first',array('fields'=>array('active','fname','email','id','username'),'conditions'=>array('User.email'=>$this->request->data['Dataentry']['username'],'level'=>'data')));
 		      
				
				
			if(!empty($user_active)){
				
			
			$username=$user_active['User']['username'];
			$email=$user_active['User']['email'];	
				$message=<<<HTM
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
</HEAD>

<BODY>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>Hi User,<br>
      <br>
Username for $email <br>
<br>
Heres all the information you need to get you started -<br>
<br>
------------------<br>
Login Information:<br>
------------------<br>
User ID: $username<br>
</td>
  </tr>
</table>

</BODY>
HTM;

// subject
$subject = 'Forgot Username';

// To send HTML mail, the Content-type header must be set
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= 'From: jdpwholesaler.com <info@jdpwholesaler.com>' . "\r\n";
// Mail it
mail($email, $subject, $message, $headers);
			
			
			$this->Session->setFlash(__('We have send your password to your email address.'), 'admin_success');
			
			}else {
					//$this->Session->setFlash(__('User not active'),'alert',array('class'=>'alert-error'));
					 $this->Session->setFlash(__('User not found'), 'error',array('class'=>'alert-error'));
				}
		 }
	}
	
	
	
	
	public function login(){
		 $this->layout = "dataentry_index";	
		if ($this->request->is('post')) {
			
			$user_active = $this->User->find('first',array('fields'=>'active','conditions'=>array('User.username'=>$this->request->data['User']['username'],'level'=>'data')));
 		        error_log("TEST: " . print_r($user_active, true));
				
			if(!empty($user_active)){
				if ($user_active['User']['active']){
					if ($this->Auth->login()) {
						$this->Cookie->write('username',$this->Auth->user('username'),true,'+4 weeks');
						
						if ($this->request->data['User']['remember'] == 1) {
							unset($this->request->data['User']['remember']);
							
							$this->request->data['User']['password'] = $this->Auth->password($this->request->data['User']['password']);
						
							$this->Cookie->write('remember_me_cookie', $this->request->data['User'], true, '2 weeks');
						}
						$this->redirect($this->Auth->redirect());
					} else {
						$this->Session->setFlash(__('Invalid username or password, try again'),'error',array('class'=>'alert-error'));
					}
				}
				else {
					//$this->Session->setFlash(__('User not active'),'alert',array('class'=>'alert-error'));
					 $this->Session->setFlash(__('User not active'), 'error',array('class'=>'alert-error'));
				}
			}
			else {
			//	$this->Session->setFlash(__('User not found'),'alert',array('class'=>'alert-error'));
				 $this->Session->setFlash(__('User not found'), 'error',array('class'=>'alert-error'));
				//$this->redirect('index');
			}
		}
		else {
			//die($this->Session->read('Auth.User.level'));
		if ($this->Auth->login()) {
			if ($this->Session->read('Auth.dataentry.level')=='data') {
				
				$this->redirect(array('action' => 'index'));
			}else{
			}
		}
		}
	}
	
	 /**

     * logout method

     *

     * @return void

     */

 public function logout() {

        $this->Session->setFlash('Good-Bye', 'success');
		$this->Auth->logout();
        $this->redirect($this->Auth->logout());

    } 
public function profile() {		

	$this->layout = "dataentry_index";		

	 $this->User->id =  $this->Auth->user('id');

			if (!$this->User->exists()) {

				throw new NotFoundException(__('Invalid user'));

			}

			if ($this->request->is('post') || $this->request->is('put')) {

				if ($this->User->save($this->request->data)) {
					//print_r($this->Session->read('Auth'));die; 
					 $userid=$this->User->read(null, $this->Auth->user('id'));
					$this->Session->write('Auth.dataentry',$userid['User']);
					 
					$this->Session->setFlash(__('The user account has been updated'), 'admin_success');

					$this->redirect(array('controller'=>'Dataentry','action' => 'index'));

				} else {

					$this->Session->setFlash(__('The user account could not be updated. Please, try again.'), 'admin_error');

				}

			} else {
				
			
            
		
				$this->request->data = $this->User->read(null, $this->Auth->user('id'));
				$this->request->data['User']['password'] = null;

			}

			

	}

		public function products($id=NULL) {

	 $this->layout = "dataentry_index";	
		$product_action=($id==NULL)?'Created':'Updated';
	   $this->Products->id = $id;

        
        if ($this->request->is('put') || $this->request->is('post')) {
			
				$user_active = $this->Products->find('count',array('conditions'=>array('Products.product_ndc'=>$this->request->data['Products']['product_ndc'])));	
				
				if($user_active and $this->request->data['Products']['id']!=$id){
					$this->Session->setFlash(__('The Prouduct NDC must be unique'), 'admin_error');
				}else{
					
					if($id==NULL){
			$item_first_charter=substr($this->request->data['Products']['producttype'],0,1);
			$itemnumber = $this->Products->find('first',array('order'=>array(
        'Products.itemnumber' => 'desc'),'conditions'=>array("Products.itemnumber like '".$item_first_charter."%'")));	
				$remove_first=substr($itemnumber['Products']['itemnumber'],1);
				$next_itmenumber=(int)$remove_first+1;
				$this->request->data['Products']['itemnumber']=$item_first_charter.$next_itmenumber;
			
				
				$this->Products->create();
				
				if ($this->Products->save($this->request->data['Products'])) {		
               
						/*$activit_data=array(						
						'product_info_id'=>$this->Products->id,
						'user_id'=> $this->Auth->user('id'),
						'action'=>$product_action,
						'action_on'=>'Product',
						'created'=>date("Y-m-d H:i:s")
					);					
					$this->admin_add_product_activity($activit_data);*/
					
					
					$this->Session->setFlash(__('The Prouduct has been Added'), 'admin_success');
					$this->redirect(array('action' => 'products'));
				} else {
					
					$this->Session->setFlash(__('The Product could not be saved. Please, try again.'), 'admin_error');
				
				}
		}else{
			$this->request->data = $this->Products->read(null, $id);
            //$this->request->data['User']['password'] = null;
		}
				}
				
			$this->set('user_id',$this->Auth->user('id'));
         $this->Products->recursive = 1;

          $search="";
		 if(!isset($this->params['url']['ndcproduct'])){
		$this->paginate = array('order'=>array(
        'Products.createddt' => 'desc'));
		 }else{
			$this->paginate = array(
            'conditions' => array("OR"=>array("product_ndc like '%".$this->params['url']['ndcproduct']."%'","itemnumber like '%".$this->params['url']['ndcproduct']."%'","productname like '%".$this->params['url']['ndcproduct']."%'") ), 
			'order'=>array('Products.createddt' => 'desc'),
			'limit'=>10
           );
		   $search=$this->params['url']['ndcproduct'];
		 }
		$this->set('search',$search);		 
		$this->set('users', $this->paginate("Products"));

	}
		}
		
	public function products_delete($id = null) {

        if (!$this->request->is('post')) {

            throw new MethodNotAllowedException();

        }

        $this->Products->id = $id;

        if (!$this->Products->exists()) {

            throw new NotFoundException(__('Invalid user'));

        }
		
		$status=$this->Products->updateAll(
					array('active'=>"'False'"),
					array('Products.id' => $id)
				);
 if ($status) {

            $this->Session->setFlash(__('Product deactivate'), 'admin_success');

            $this->redirect(array('action' => 'products'));

        }

       /* if ($this->Products->delete()) {

            $this->Session->setFlash(__('Product deleted'), 'admin_success');

            $this->redirect(array('action' => 'products'));

        }*/

        $this->Session->setFlash(__('Product was not deleted'), 'admin_error');

        $this->redirect(array('action' => 'products'));

    }
	
	public function products_activate($id = null) {

        if (!$this->request->is('post')) {

            throw new MethodNotAllowedException();

        }

        $this->Products->id = $id;
		

        if (!$this->Products->exists()) {

            throw new NotFoundException(__('Invalid user'));

        }
		
		$status=$this->Products->updateAll(
					array('active'=>"'True'"),
					array('Products.id' => $id)
				);
 if ($status) {

            $this->Session->setFlash(__('Product Activate'), 'admin_success');

            $this->redirect(array('action' => 'deactive_products'));

        }
       
        $this->Session->setFlash(__('Product was not deleted'), 'admin_error');

        $this->redirect(array('action' => 'deactive_products'));

    }
	
	public function manage_products($id = null) {

		$this->layout = "dataentry_index";

        $this->set('title', __('Welcome Admin Panel'));
		$product_action=($id==NULL)?'Created':'Updated';
        $this->set('description', __('Manage Users'));
		$products_product_ndcs=$this->Products->find("list",array('fields'=>array('product_ndc')));
		$products_jshon=array();
		$i=0;
		foreach($products_product_ndcs as $key=>$products_product_ndc){
			$products_jshon[$i]['label']=$products_product_ndc;
			$products_jshon[$i]['value']=$key;
			$i++;
		}
		$this->set('products_jshon',json_encode($products_jshon));
		$this->set('products', $this->Products->find("list",array('fields'=>array('productname'))));
		
		 $this->ProductInfo->id = $id;

        
        if ($this->request->is('put') || $this->request->is('post')) {
			
			
			//count previous quenty
				$previous_quentity=0;
				if($id!=null){
				
				$previousquentit=$this->ProductInfo->find('first', array(
								  'conditions'=>array('ProductInfo.id'=>$id),
								  'fields'=>array('availability')
								));
				$previous_quentity=$previousquentit['ProductInfo']['availability'];
				
				}
			
				$this->ProductInfo->create();
				
				if ($this->ProductInfo->save($this->request->data)) {
				
						
              //Add product update acitvity
					if($previous_quentity!=$this->request->data['ProductInfo']['availability']){
						$activit_data=array(
							'product_id'=>$this->request->data['ProductInfo']['prodid'],
							'product_info_id'=>$this->ProductInfo->id,
							'user_id'=> $this->Auth->user('id'),
							'previous_qty'=>$previous_quentity,
							'new_qty'=>$this->request->data['ProductInfo']['availability'],
							'note'=>$this->request->data['ProductInfo']['Note'],
							'created'=>date("Y-m-d H:i:s")
						);					
						$this->add_product_activity($activit_data);
					}	
					
					$this->Session->setFlash(__('The Prouduct has been Added'), 'admin_success');
					$this->redirect(array('action' => 'manage_products'));
				} else {
					
					$this->Session->setFlash(__('The Product could not be saved. Please, try again.'), 'admin_error');
				}
			
		}else{
			$this->request->data = $this->ProductInfo->read(null, $id);
            //$this->request->data['User']['password'] = null;
		}

         $this->ProductInfo->recursive = 1;
	
         $search="";
		 if(!isset($this->params['url']['ndcproduct'])){
		$this->paginate = array(
            'conditions' => array("ProductInfo.availability >'0'","Product.active!='False'"),
			'order'=>array('ProductInfo.createddt' => 'desc'),
			'limit'=>10
           );
		 }else{
			 $this->paginate = array(
            'conditions' => array("ProductInfo.availability >'0'","Product.active!='False'", "OR"=>array("product_ndc like '%".$this->params['url']['ndcproduct']."%'","productname like '%".$this->params['url']['ndcproduct']."%'","batchno like '%".$this->params['url']['ndcproduct']."%'") ), 
			'order'=>array('Product.name' => 'desc'),
			'limit'=>10
           );
		   $search=$this->params['url']['ndcproduct'];
		 }
		$this->set('search',$search); 
        $this->set('users', $this->paginate("ProductInfo"));
    }
	
	public function manageproducts_delete($id = null) {

        if (!$this->request->is('post')) {

            throw new MethodNotAllowedException();

        }

        $this->ProductInfo->id = $id;

        if (!$this->ProductInfo->exists()) {

            throw new NotFoundException(__('Invalid user'));

        }
		
		$Orderdetail = $this->OrderLot->find('count',array('conditions'=>array('OrderLot.productinfoid'=>$id)));
		 if ($Orderdetail>0) {

           $this->Session->setFlash(__('Product ifno was not deleted because this lot already alloted'), 'admin_error');

            $this->redirect(array('action' => 'manage_products'));

        }
		
		//count previous quenty
				$previous_quentity=0;
				if($id!=null){
				
				$previousquentit=$this->ProductInfo->find('first', array(
								  'conditions'=>array('ProductInfo.id'=>$id),
								  'fields'=>array('availability')
								));
				$previous_quentity=$previousquentit['ProductInfo']['availability'];
				
				}

        if ($this->ProductInfo->delete()) {
			
			$activit_data=array(
							
							'product_info_id'=>$id,
							'user_id'=> $this->Auth->user('id'),
							'previous_qty'=>$previous_quentity,
							'new_qty'=>0,
							'created'=>date("Y-m-d H:i:s")
						);					
						$this->add_product_activity($activit_data);

            $this->Session->setFlash(__('Product Information deleted'), 'admin_success');

            $this->redirect(array('action' => 'manage_products'));

        }

        $this->Session->setFlash(__('Product ifno was not deleted'), 'admin_error');

        $this->redirect(array('action' => 'manage_products'));

    }
	
	public function deactive_products($id=NULL) {

		$this->layout = "dataentry_index";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));
		
		 $this->Products->id = $id;        
       
         $this->Products->recursive = 1;
		 
		  $search="";
		 if(!isset($this->params['url']['ndcproduct'])){
		$this->paginate = array('conditions' => array('Products.active'=>'False'),'order'=>array(
        'Products.createddt' => 'desc'));
		 }else{
			$this->paginate = array(
            'conditions' => array("OR"=>array("product_ndc like '%".$this->params['url']['ndcproduct']."%'","itemnumber like '%".$this->params['url']['ndcproduct']."%'","productname like '%".$this->params['url']['ndcproduct']."%'") ), 
			'order'=>array('Products.createddt' => 'desc'),
			'limit'=>10
           );
		   $search=$this->params['url']['ndcproduct'];
		 }
		$this->set('search',$search);		 
		$this->set('users', $this->paginate("Products"));
    }
	
	public function add_product_activity($data){
			$this->ProductActivity->create();
				
				if ($this->ProductActivity->save($data)) {    
					return true; 
				}
		}
	
}

?>