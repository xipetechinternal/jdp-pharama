<?php
App::uses('CakeTime', 'Utility');
App::uses('AppController', 'Controller');

App::uses('CakeEmail', 'Network/Email');



/**

 * Users Controller

 *

 * @property User $User

 */

class DataentriesController extends AppController {

      

        public $components = array('RequestHandler','Cookie');

   public $uses = array('EmailTemplateDescription','Order','Orderdetail');

   public $paginate = array('limit' => 10);

   function beforeFilter() {

        parent::beforeFilter();

        $this->layout = "default";

		$this->Auth->allow('forgot_password','forgot_username');

		$this->Auth->loginAction = '/Dataentries/login';

        $this->Auth->logoutRedirect = '/Dataentries/login';

        $this->Auth->loginRedirect = '/Dataentries/index';

        

		
		

    }

	

	


	public function index(){
		
		 $this->layout = "index";	
		 
		}
	public function forgot_password(){
		 $this->layout = "index";	
	}
	public function forgot_username(){
		 $this->layout = "index";	
	}
	public function login(){
		 $this->layout = "index";	
		if ($this->request->is('post')) {
			
			$user_active = $this->User->find('first',array('fields'=>'active','conditions'=>array('User.username'=>$this->request->data['User']['username'],'level'=>'data')));
 		        error_log("TEST: " . print_r($user_active, true));
				
			if(!empty($user_active)){
				if ($user_active['User']['active']){
					if ($this->Auth->login()) {
						$this->Cookie->write('username',$this->Auth->user('username'),true,'+4 weeks');
						
						if ($this->request->data['User']['remember'] == 1) {
							unset($this->request->data['User']['remember']);
							
							$this->request->data['User']['password'] = $this->Auth->password($this->request->data['User']['password']);
						
							$this->Cookie->write('remember_me_cookie', $this->request->data['User'], true, '2 weeks');
						}
						$this->redirect($this->Auth->redirect());
					} else {
						$this->Session->setFlash(__('Invalid username or password, try again'),'error',array('class'=>'alert-error'));
					}
				}
				else {
					//$this->Session->setFlash(__('User not active'),'alert',array('class'=>'alert-error'));
					 $this->Session->setFlash(__('User not active'), 'error',array('class'=>'alert-error'));
				}
			}
			else {
			//	$this->Session->setFlash(__('User not found'),'alert',array('class'=>'alert-error'));
				 $this->Session->setFlash(__('User not found'), 'error',array('class'=>'alert-error'));
				$this->redirect('index');
			}
		}
		else {
			if($this->Cookie->check('username')) {
				$lastUsername = $this->Cookie->read('username');
				if(!empty($lastUsername)){
					$this->request->data['User']['username'] = $lastUsername;
				}
			}
		}
	}
	
	

}

?>