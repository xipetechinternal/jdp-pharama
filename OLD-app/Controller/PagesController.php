<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
* Controller name
*
* @var string
*/

public $name = 'Pages';

/**
* This controller does not use a model
*
* @var array
*/

public $uses = array('Page','User','Product');

 function beforeFilter() {
        parent::beforeFilter();
        $this->layout = "default";
		$this->Auth->allow('otc','index','brand','generic');   
		$this->Product->bindModel(array('hasMany'=>array(
            'ProductStock' => array(
                'className' => 'ProductStock',
                'foreignKey' => 'product_id',
                'dependent'=>true
            )
       	 ),'belongsTo'=>array(
            'Category' => array(
                'className' => 'Category',
                'foreignKey' => 'category_id',
                'dependent'=>true
            )
        )), false);
		
 }
/**
* Index Dashboard
*
* @return void
*/
	 
function admin_index(){	
			 
	$this->layout = "admin_dashboard";   
	$this->set('title', __('Users'));
	
 
}
	 
/**
* edit method
*
* @param string $id
* @return void
*/	 
function index(){
	$this->layout = "index";
	$pages_arr=array('about_us'=>3,'terms_conditions'=>1,'documents'=>5,'online_store'=>4);  
	$this->set('pages', $this->Page->find("all",array('conditions'=>array('Page.id'=>$pages_arr[$this->params['pass'][0]]))));
}

/**
* edit method
*
* @param string $id
* @return void
*/
function brand(){
	  $this->layout = "index";	
	  $this->Product->recursive = 1;
      $this->set('products', $this->Product->find("all",array('conditions'=>array('Product.status'=>1,'Product.category_id'=>1))));	
}

/**
* edit method
*
* @param string $id
* @return void
*/
function generic(){
	  $this->layout = "index";	
	  $this->Product->recursive = 1;
      $this->set('products', $this->Product->find("all",array('conditions'=>array('Product.status'=>1,'Product.category_id'=>2))));	
}

/**
* edit method
*
* @param string $id
* @return void
*/
function otc(){
	  $this->layout = "index";	
	  $this->Product->recursive = 1;
      $this->set('products', $this->Product->find("all",array('conditions'=>array('Product.status'=>1,'Product.category_id'=>3))));	
}

/**
* edit method
*
* @param string $id
* @return void
*/

public function admin_terms_conditions($id = null) {
	$this->layout = "admin_dashboard"; 
	$this->Page->id = $id;
	if (!$this->Page->exists()) {
		throw new NotFoundException(__('Invalid post'));
	}
	
	if ($this->request->is('post') || $this->request->is('put')) {
		
		if ($this->Page->save($this->request->data)) {
			$this->Session->setFlash(__('Terms & Conditions has been saved'), 'admin_success');
			$this->redirect(array('action' => 'admin_terms_conditions',$id));
		} else {
			$this->Session->setFlash(__('Terms & Conditions could not be saved. Please, try again.'), 'admin_error');
		}
	} else {
		$this->request->data = $this->Page->read(null, $id);
	}
	
}

public function admin_privacy_policy($id = null) {
	$this->layout = "admin_dashboard"; 
	$this->Page->id = $id;
	if (!$this->Page->exists()) {
		throw new NotFoundException(__('Invalid post'));
	}
	
	if ($this->request->is('post') || $this->request->is('put')) {
		
		if ($this->Page->save($this->request->data)) {
			$this->Session->setFlash(__('Privacy Policy has been saved'), 'admin_success');
			$this->redirect(array('action' => 'admin_privacy_policy',$id));
		} else {
			$this->Session->setFlash(__('Privacy Policy could not be saved. Please, try again.'), 'admin_error');
		}
	} else {
		$this->request->data = $this->Page->read(null, $id);
	}
	
}

/**
* edit method
*
* @param string $id
* @return void
*/

public function admin_about_us($id = null) {
	$this->layout = "admin_dashboard"; 
	$this->Page->id = $id;
	if (!$this->Page->exists()) {
		throw new NotFoundException(__('Invalid post'));
	}
	
	if ($this->request->is('post') || $this->request->is('put')) {
		
		if ($this->Page->save($this->request->data)) {
			$this->Session->setFlash(__('About Us has been saved'), 'admin_success');
			$this->redirect(array('action' => 'admin_about_us',$id));
		} else {
			$this->Session->setFlash(__('About Us could not be saved. Please, try again.'), 'admin_error');
		}
	} else {
		$this->request->data = $this->Page->read(null, $id);
	}
	
}

public function admin_online_store($id = null) {
	$this->layout = "admin_dashboard"; 
	$this->Page->id = $id;
	if (!$this->Page->exists()) {
		throw new NotFoundException(__('Invalid post'));
	}
	
	if ($this->request->is('post') || $this->request->is('put')) {
		
		if ($this->Page->save($this->request->data)) {
			$this->Session->setFlash(__('Online store has been saved'), 'admin_success');
			$this->redirect(array('action' => 'admin_online_store',$id));
		} else {
			$this->Session->setFlash(__('Online storecould not be saved. Please, try again.'), 'admin_error');
		}
	} else {
		$this->request->data = $this->Page->read(null, $id);
	}
	
}
/**
* edit method
*
* @param string $id
* @return void
*/

public function admin_documents($id = null) {
	$this->layout = "admin_dashboard"; 
	$this->Page->id = $id;
	if (!$this->Page->exists()) {
		throw new NotFoundException(__('Invalid post'));
	}
	
	if ($this->request->is('post') || $this->request->is('put')) {
		
		if ($this->Page->save($this->request->data)) {
			$this->Session->setFlash(__('Documents has been saved'), 'admin_success');
			$this->redirect(array('action' => 'admin_documents',$id));
		} else {
			$this->Session->setFlash(__('Documents could not be saved. Please, try again.'), 'admin_error');
		}
	} else {
		$this->request->data = $this->Page->read(null, $id);
	}
	
}


	 
	 

}
