<?php
App::uses('CakeTime', 'Utility');
App::uses('AppController', 'Controller');

App::uses('CakeEmail', 'Network/Email');



/**

 * Users Controller

 *

 * @property User $User

 */

class SalesController extends AppController {

      

       public $components = array('RequestHandler','Cookie');

   public $uses = array('User','EmailTemplateDescription','Order','Orderdetail','Products','ProductInfo','Product');

   public $paginate = array('limit' => 10);

   function beforeFilter() {

        parent::beforeFilter();

        $this->layout = "default";

		$this->Auth->allow('forgot_password','forgot_username');

		$this->Auth->loginAction = '/Sales/login';

        $this->Auth->logoutRedirect = '/Sales/login';

        $this->Auth->loginRedirect = '/Sales/index';

        

		
		

    }

public function isAuthorized($user) {
    // All registered users can add posts
	
     if ($user['level'] == 'sales') {
        return true;
    }
	$this->Session->setFlash('Sorry, you don\'t have permission to access that page.');
                 $this->redirect('login');
                 return false;
    return parent::isAuthorized($user);
}

	

	


	public function index(){
		
		 $this->layout = "sales_index";	
		 
		}
	public function forgot_password(){
		 $this->layout = "sales_index";
		 
		 if ($this->request->is('post')) {
			
			$user_active = $this->User->find('first',array('fields'=>array('active','fname','email','id','username'),'conditions'=>array('User.username'=>$this->request->data['Sales']['username'],'level'=>'sales')));
 		      
				$username=$user_active['User']['username'];
				$email=$user_active['User']['email'];	
			if(!empty($user_active)){
				
			/*** Encript password before insert to DB  ***/
			$password = ($tmppass);
			
			
				
				$new_password = substr( str_shuffle( 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$' ) , 0 , 8 );
				
				
				$this->User->id = $user_active['User']['id'];				
				$this->User->saveField('password',$new_password);
				
				$message=<<<HTM
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
</HEAD>

<BODY>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>Hi User,<br>
      <br>
Password for $username has been reset.<br>
<br>
Heres all the information you need to get you started -<br>
<br>
------------------<br>
Login Information:<br>
------------------<br>
User ID: $username<br>
Password: $new_password</td>
  </tr>
</table>

</BODY>
HTM;

// subject
$subject = 'Forgot Password';

// To send HTML mail, the Content-type header must be set
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= 'From: jdpwholesaler.com <info@jdpwholesaler.com>' . "\r\n";
// Mail it
mail($email, $subject, $message, $headers);
			
			
			$this->Session->setFlash(__('We have send your password to your email address.'), 'admin_success');
			
			}else {
					//$this->Session->setFlash(__('User not active'),'alert',array('class'=>'alert-error'));
					 $this->Session->setFlash(__('User not found'), 'error',array('class'=>'alert-error'));
				}
		 }
	}
	public function forgot_username(){
		 $this->layout = "sales_index";
		 
		 if ($this->request->is('post')) {
			
			$user_active = $this->User->find('first',array('fields'=>array('active','fname','email','id','username'),'conditions'=>array('User.email'=>$this->request->data['Sales']['username'],'level'=>'sales')));
 		      
				
				
			if(!empty($user_active)){
				
			
			$username=$user_active['User']['username'];
			$email=$user_active['User']['email'];	
				$message=<<<HTM
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
</HEAD>

<BODY>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>Hi User,<br>
      <br>
Username for $email <br>
<br>
Heres all the information you need to get you started -<br>
<br>
------------------<br>
Login Information:<br>
------------------<br>
User ID: $username<br>
</td>
  </tr>
</table>

</BODY>
HTM;

// subject
$subject = 'Forgot Username';

// To send HTML mail, the Content-type header must be set
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= 'From: jdpwholesaler.com <info@jdpwholesaler.com>' . "\r\n";
// Mail it
mail($email, $subject, $message, $headers);
			
			
			$this->Session->setFlash(__('We have send your password to your email address.'), 'admin_success');
			
			}else {
					//$this->Session->setFlash(__('User not active'),'alert',array('class'=>'alert-error'));
					 $this->Session->setFlash(__('User not found'), 'error',array('class'=>'alert-error'));
				}
		 }
	}
	
	
	
	
	public function login(){
		 $this->layout = "sales_index";	
		if ($this->request->is('post')) {
			
			$user_active = $this->User->find('first',array('fields'=>'active','conditions'=>array('User.username'=>$this->request->data['User']['username'],'level'=>'sales')));
 		        error_log("TEST: " . print_r($user_active, true));
				
			if(!empty($user_active)){
				if ($user_active['User']['active']){
					if ($this->Auth->login()) {
						$this->Cookie->write('username',$this->Auth->user('username'),true,'+4 weeks');
						
						if ($this->request->data['User']['remember'] == 1) {
							unset($this->request->data['User']['remember']);
							
							$this->request->data['User']['password'] = $this->Auth->password($this->request->data['User']['password']);
						
							$this->Cookie->write('remember_me_cookie', $this->request->data['User'], true, '2 weeks');
						}
						$this->redirect($this->Auth->redirect());
					} else {
						$this->Session->setFlash(__('Invalid username or password, try again'),'error',array('class'=>'alert-error'));
					}
				}
				else {
					//$this->Session->setFlash(__('User not active'),'alert',array('class'=>'alert-error'));
					 $this->Session->setFlash(__('User not active'), 'error',array('class'=>'alert-error'));
				}
			}
			else {
			//	$this->Session->setFlash(__('User not found'),'alert',array('class'=>'alert-error'));
				 $this->Session->setFlash(__('User not found'), 'error',array('class'=>'alert-error'));
				//$this->redirect('index');
			}
		}
		else {
			//die($this->Session->read('Auth.User.level'));
		if ($this->Auth->login()) {
			if ($this->Session->read('Auth.Sales.level')=='sales') {
				
				$this->redirect(array('action' => 'index'));
			}else{
			}
		}
		}
	}
	
	 /**

     * logout method

     *

     * @return void

     */

 public function logout() {

        $this->Session->setFlash('Good-Bye', 'success');
		$this->Auth->logout();
        $this->redirect($this->Auth->logout());

    } 
public function profile() {		

	$this->layout = "sales_index";		

	 $this->User->id =  $this->Auth->user('id');

			if (!$this->User->exists()) {

				throw new NotFoundException(__('Invalid user'));

			}

			if ($this->request->is('post') || $this->request->is('put')) {

				if ($this->User->save($this->request->data)) {
					//print_r($this->Session->read('Auth'));die; 
					 $userid=$this->User->read(null, $this->Auth->user('id'));
					$this->Session->write('Auth.Sales',$userid['User']);
					 
					$this->Session->setFlash(__('The Sales account has been updated'), 'admin_success');

					$this->redirect(array('controller'=>'Sales','action' => 'index'));

				} else {

					$this->Session->setFlash(__('The Sales account could not be updated. Please, try again.'), 'admin_error');

				}

			} else {
				
			
            
		
				$this->request->data = $this->User->read(null, $this->Auth->user('id'));
				$this->request->data['User']['password'] = null;

			}

			

	}

		public function products($id=NULL) {

	 $this->layout = "sales_index";	

	   $this->Products->id = $id;

        
        if ($this->request->is('put') || $this->request->is('post')) {
			
				$user_active = $this->Products->find('count',array('conditions'=>array('Products.product_ndc'=>$this->request->data['Products']['product_ndc'])));	
				
				if($user_active and $this->request->data['Products']['id']!=$id){
					$this->Session->setFlash(__('The Prouduct NDC must be unique'), 'admin_error');
				}else{
				
				$this->Products->create();
				
				if ($this->Products->save($this->request->data['Products'])) {		
               
						
					
					$this->Session->setFlash(__('The Prouduct has been Added'), 'admin_success');
					$this->redirect(array('action' => 'products'));
				} else {
					
					$this->Session->setFlash(__('The Product could not be saved. Please, try again.'), 'admin_error');
				}
				}
		}else{
			$this->request->data = $this->Products->read(null, $id);
            //$this->request->data['User']['password'] = null;
		}
			$this->set('user_id',$this->Auth->user('id'));
         $this->Products->recursive = 1;

        //$this->set('users', $this->paginate("Products",array('Products.user_id'=>$this->Auth->user('id'))));
		$this->set('users', $this->paginate("Products"));	

	}
	
	public function products_delete($id = null) {

        if (!$this->request->is('post')) {

            throw new MethodNotAllowedException();

        }

        $this->Products->id = $id;

        if (!$this->Products->exists()) {

            throw new NotFoundException(__('Invalid user'));

        }

        if ($this->Products->delete()) {

            $this->Session->setFlash(__('Product deleted'), 'admin_success');

            $this->redirect(array('action' => 'products'));

        }

        $this->Session->setFlash(__('Product was not deleted'), 'admin_error');

        $this->redirect(array('action' => 'products'));

    }
	
	public function manage_products($id = null) {

		$this->layout = "sales_index";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));
		$products_product_ndcs=$this->Products->find("list",array('fields'=>array('product_ndc')));
		$products_jshon=array();
		$i=0;
		foreach($products_product_ndcs as $key=>$products_product_ndc){
			$products_jshon[$i]['label']=$products_product_ndc;
			$products_jshon[$i]['value']=$key;
			$i++;
		}
		$this->set('products_jshon',json_encode($products_jshon));
		$this->set('products', $this->Products->find("list",array('fields'=>array('productname'))));
		
		 $this->ProductInfo->id = $id;

        
        if ($this->request->is('put') || $this->request->is('post')) {
			
				$this->ProductInfo->create();
				
				if ($this->ProductInfo->save($this->request->data)) {
				
						
               
						
					
					$this->Session->setFlash(__('The Prouduct has been Added'), 'admin_success');
					$this->redirect(array('action' => 'manage_products'));
				} else {
					
					$this->Session->setFlash(__('The Product could not be saved. Please, try again.'), 'admin_error');
				}
			
		}else{
			$this->request->data = $this->ProductInfo->read(null, $id);
            //$this->request->data['User']['password'] = null;
		}

        // $this->ProductInfo->recursive = 1;
	
        $this->set('users', $this->paginate("ProductInfo"));
    }
	
	public function manageproducts_delete($id = null) {

        if (!$this->request->is('post')) {

            throw new MethodNotAllowedException();

        }

        $this->ProductInfo->id = $id;

        if (!$this->ProductInfo->exists()) {

            throw new NotFoundException(__('Invalid user'));

        }

        if ($this->ProductInfo->delete()) {

            $this->Session->setFlash(__('Product Information deleted'), 'admin_success');

            $this->redirect(array('action' => 'manage_products'));

        }

        $this->Session->setFlash(__('Product ifno was not deleted'), 'admin_error');

        $this->redirect(array('action' => 'manage_products'));

    }
	
	public function create_clients($id = NULL) {		

	$this->layout = "sales_index";		

	 $this->User->id = $id;
	$this->set('uid',$this->Auth->user('id'));
			
        if ($this->request->is('put') || $this->request->is('post')) {
			
			 $user_with_this_username = $this->User->findByUsername($this->request->data['User']['username']);
			 $user_with_this_email = $this->User->findByEmail($this->request->data['User']['email']);
			
			if(!empty($user_with_this_username) and ($this->request->data['User']['id'] != $id)) {
				
				$this->Session->setFlash(__('Username already exists. Please chose another one.'), 'admin_error');
			}
			elseif (!empty($user_with_this_email) and ($this->request->data['User']['id'] != $id)){
				
				$this->Session->setFlash(__('Email is already registered by another user. Please chose another one.'), 'admin_error');
			}
			else {
			
				$this->User->create();
				
				if ($this->User->save($this->request->data)) {
				
					
						
						$message=<<<HTM
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
</HEAD>

<BODY>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>Dear $fname</td>
  </tr>
  <tr>
    <td>Thanks for choosing us. Your login details given below:<br>
Username : $name<br>
Password : $cpassword<br>
      Regards <br>

      </td>
  </tr>
</table>
</BODY>
HTM;

/*** Set to address here  ***/
$to = $email;
/*** Set email subject here  ***/
if($id==NULL){
	$subject = 'Signup Confirmation ';
				}else{
	$subject = 'Update Confirmation ';
}
// To send HTML mail, the Content-type header must be set
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
//$headers .= 'From: Testing <info@promptdesigners.com>' . "\r\n";
$headers .= 'From: '.$fname.' <'.$email.'>' . "\r\n";

// Mail it
mail($to, $subject, $message, $headers);
               
				
					
					$this->Session->setFlash(__('The user has been saved'), 'admin_success');
					$this->redirect(array('action' => 'create_clients'));
				} else {
					
					$this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'admin_error');
				}
			}
		}else{
			$this->request->data = $this->User->read(null, $id);
            $this->request->data['User']['password'] = null;
		}
		

         $this->User->recursive = 1;
		$this->set('id',$id);
        $this->set('users', $this->paginate("User",array('User.level'=>'client','User.sid'=>$this->Auth->user('id'))));

			

	}
	
		public function sales_users_delete($id = null) {

        if (!$this->request->is('post')) {

            throw new MethodNotAllowedException();

        }

        $this->User->id = $id;

        if (!$this->User->exists()) {

            throw new NotFoundException(__('Invalid user'));

        }

        if ($this->User->delete()) {

            $this->Session->setFlash(__('User deleted'), 'admin_success');

            $this->redirect(array('action' => 'create_clients'));

        }

        $this->Session->setFlash(__('User was not deleted'), 'admin_error');

        $this->redirect(array('action' => 'create_clients'));

    }
	
		public function product_search(){
		$this->layout = "sales_index";
		$cart_list = $this->Order->find('count',array('conditions'=>array('Order.userid'=>$this->Auth->user('id'),'Order.status'=>'Fill')));
		$this->set('cart_list',$cart_list);

		if($this->request->is('get')){
			
		$this->set('product_type',$this->request->query['searchtxt']);
		
		$this->request->data['customers']['searchtxt']=$this->request->query['searchtxt'];
		
		 $this->paginate = array(
        'conditions' => array('OR' => array("Product.productname like '%".$this->request->query['searchtxt']."%'","Product.product_ndc like '%".$this->request->query['searchtxt']."%'"))
		
    );
		
	$products=$this->paginate("Product");
		foreach($products as $key=>$product){
			$instock=0;
			foreach($product['ProductInfo'] as $ProductInfo){
				$instock=$instock+$ProductInfo['availability'];
			}
			$products[$key]['Product']['instock']=$instock;
		}
	
		//print_r($products);
        $this->set('products', $products);

		
		}
		
		}
		
	public function order_history(){
		$this->layout = "sales_index";
		$sales=$this->User->find('list',array('fields'=>array('username'),'conditions'=>array('User.sid'=>$this->Auth->user('id'),'User.level'=>'client')));
		$this->set('sales',$sales);

		
		
		if ($this->request->is('post')) {
				 $this->paginate = array(
        'conditions' => array('Order.status'=>array('Test','Accep'),'Order.userid'=>$this->request->data['Sales']['sid']),'fields' => array('DISTINCT Order.id','Order.tmporderid','Order.userid','Order.createddt','Order.status')		
    	);	
			$products=$this->paginate("Order");
			$this->set('users',$products);
			
			}
		}
		
	public function orderinfo($oid){
			$this->layout = "ajax";

        //$this->set('title', __('Welcome Admin Panel'));

        //$this->set('description', __('Manage Users'));
		if ($this->request->is('ajax')) {
		$Orderdetail = $this->Order->find('all',array('recursive'=>2,'conditions'=>array('Order.id'=>$oid)));		
		 $this->set('Orderdetail', $Orderdetail);
		  $this->set('Orderdetailid',$oid);
		}
	}
	
		public function report_monthly() {

		$this->layout = "sales_index";

      $sales=$this->User->find('list',array('fields'=>array('username'),'conditions'=>array('User.sid'=>$this->Auth->user('id'),'User.level'=>'client')));
		$this->set('sales',$sales);
			
		if ($this->request->is('post')) {
				 $this->paginate = array(
        'conditions' => array('Order.createddt >= ' => $this->request->data['Sales']['selfrmdate'],'Order.createddt <= ' => $this->request->data['Sales']['seltodate'],'Order.status'=>array('Test','Accep'),'Order.userid'=>$this->request->data['Sales']['sid']),'fields' => array('DISTINCT Order.id','Order.tmporderid','Order.userid','Order.createddt','Order.status')		
    	);	
			$products=$this->paginate("Order");
			$this->set('users',$products);
			
			}
		
        
    }
	
	
		public function client_users_delete($id = null) {

        if (!$this->request->is('post')) {

            throw new MethodNotAllowedException();

        }

        $this->User->id = $id;

        if (!$this->User->exists()) {

            throw new NotFoundException(__('Invalid user'));

        }

        if ($this->User->delete()) {

            $this->Session->setFlash(__('User deleted'), 'admin_success');

            $this->redirect(array('action' => 'create_clients'));

        }

        $this->Session->setFlash(__('User was not deleted'), 'admin_error');

        $this->redirect(array('action' => 'create_clients'));

    }
	
}

?>