<?php
// Designate string to be encrypted
$string ="Applied Cryptography, by Bruce Schneier, is a wonderful
cryptography reference.";

// Encryption/decryption key
$key ="C770634F437346D7FC8FA22F9287849E741A33438EEDEAAD";

// Encryption Algorithm
$cipher_alg =MCRYPT_3DES;

// Create the initialization vector for added security.
$iv =mcrypt_create_iv(mcrypt_get_iv_size($cipher_alg,
MCRYPT_MODE_ECB), MCRYPT_RAND);

// Output original string
print "Original string: $string ";

// Encrypt $string
$encrypted_string =mcrypt_encrypt($cipher_alg, $key, $string,
MCRYPT_MODE_CBC, $iv);

// Convert to hexadecimal and output to browser
print "Encrypted string: ".bin2hex($encrypted_string)." ";

$crypttext=file_get_contents('content.txt');
$key="MIICXwIBAAKBgQDMhwjRPO7o1c2g1a0G8A9WUzZuikdwws9XzSJi7tSRuSqLpwQ5
AHs/0cL//ysOBoe9fBvNKhUik+cjRhiZV6LCilTOixA1tvl+sTfaSLxkL1DiZNzF
6+k6wsdytzASCIAhZbYlUzxvcPZ3NiuTic3lbHtbSynoP6PmHaSPvlD8sQIDAQAB
AoGBAJjtoxco046/RwiqnLziROWMRNf0hyR2VajWFo8Y+1KWvIw9brfcd0up4X+e
4SeUlGyHXxZSEAmRw7MpV31Y/LXYD5wME+RRkVwg/9NJ5AJGPNB2cP9lhNPRAdaq
8XJ4x0eh328VuInbQkAPIqWJxmnJGtewn2peV5HKBI9R9Df9AkEA+7hTTi+MooPO
HbQfAtWCvDIXrt+bwrK442dXXrfzymMA1NbtBZeM5BD+CrinykLdA0de5aTyeVkc
v4n1kZh/SwJBANABSqjt7YOfDRQnn2ioHzFfpscYfkBy6ExqhTGRxu9kXfhSFClj
611XF5XfPD1yIwJ8xSdclR5Fz//M9CHYqnMCQQD08Nw/TI2w5kj/JnK2s0TQFH5v
MC/FOVwoDDs8dAvKeKQqeCmUM3BpzQ8nJ6A+dOnTcvOoyuUXp9sNlxVaY+YZAkEA
i0TSIFlt9frAXwZIjWJDwGMy34tevPgy4TdRBud8Mz0YTfNm3G+yX0S43fgh9WI+
XqP7vWcX0LUdl0A7FY5kUQJBAOYxU/r7jCCelCGRvs31jWM9LCJiMyAD992VonBH
2WoqGeplRYDQjWRwCqjgNgEbesyFDiblWXjmM2BgBAu4kuc=";
//$iv="khalidhashmi";
$decrypted_string =mcrypt_decrypt($cipher_alg, $key,
$crypttext, MCRYPT_MODE_CBC, $iv);

print "Decrypted string: $decrypted_string";
?>