<?php

App::uses('AppController', 'Controller');

/**

 * Users Controller

 *

 * @property User $User

 */
class CustomersController extends AppController {

    public $helpers = array('Html', 'Form', 'Js' => array("Jquery"), "Session");
    public $components = array('RequestHandler', 'Cookie', 'Export');
    public $uses = array('User', 'EmailTemplateDescription', 'Order', 'ProductsC', 'Orderdetail', 'Products', 'ProductInfo', 'Product', 'OrderLot', 'Pedigree',
        'Productrequest', 'ProductActivity', 'OrderDist', 'ProductUsedSize','CustomerField');
    public $paginate = array('limit' => 10);

    function beforeFilter() {

        parent::beforeFilter();

        $this->layout = "default";

        $this->Auth->allow('login', 'invoice_view', 'forgot_password', 'forgot_username', 'client_login_byadmin','client_order_download_pdf_url','invoice_view_split');

        $this->Auth->loginAction = '/customers/login';

        $this->Auth->logoutRedirect = '/customers/login';

        $this->Auth->loginRedirect = '/customers/index';

        $this->set('loggedIn', $this->Auth->loggedIn());


        if ($this->Session->read('Auth.User.fname') != '' and ( $this->Session->read('Auth.User.client_alert') == 'false')) {
            $this->Session->write('Auth.User.client_alert', 'true');
        }
        if ($this->Session->read('Auth.User.fname') and ( $this->Session->read('Auth.User.client_alert') == '')) {
            $this->Session->write('Auth.User.client_alert', 'false');
        }
    }

    public function isAuthorized($user) {
        // All registered users can add posts

        if ($user['level'] == 'client') {
            return true;
        }
        $this->Session->setFlash('Sorry, you don\'t have permission to access that page.');
        $this->redirect('login');
        return false;
        return parent::isAuthorized($user);
    }

    /**

     * login method

     *

     * @return void

     */
    function login() {
        $this->layout = "index";
        if ($this->request->is('post')) {

            $user_active = $this->User->find('first', array('fields' => 'active', 'conditions' => array('User.username' => $this->request->data['User']['username'], 'level' => 'client')));
            error_log("TEST: " . print_r($user_active, true));

            if (!empty($user_active)) {
                if ($user_active['User']['active']) {
                    if ($this->Auth->login()) {
                        $this->Cookie->write('username', $this->Auth->user('username'), true, '+4 weeks');
                        if ($this->request->data['User']['remember'] == 1) {
                            unset($this->request->data['User']['remember']);

                            $this->request->data['User']['password'] = $this->Auth->password($this->request->data['User']['password']);

                            $this->Cookie->write('remember_me_cookie', $this->request->data['User'], true, '2 weeks');
                        }
                        $this->redirect($this->Auth->redirect());
                    } else {
                        $this->Session->setFlash(__('Invalid username or password, try again'), 'error', array('class' => 'alert-error'));
                    }
                } else {
                    //$this->Session->setFlash(__('User not active'),'alert',array('class'=>'alert-error'));
                    $this->Session->setFlash(__('User not active'), 'error', array('class' => 'alert-error'));
                }
            } else {
                //	$this->Session->setFlash(__('User not found'),'alert',array('class'=>'alert-error'));
                $this->Session->setFlash(__('User not found'), 'error', array('class' => 'alert-error'));
                //$this->redirect('index');
            }
        } else {
            //die($this->Session->read('Auth.User.level'));
            if ($this->Auth->login()) {
                if ($this->Session->read('Auth.User.level') == 'client') {

                    $this->redirect(array('action' => 'index'));
                } else {
                    
                }
            }
        }
    }

    /**

     * index method

     *

     * @return void

     */
    function index() {
//Configure::write('debug',3);
         $this->layout = "index_demo";
        $cart_list = $this->Order->find('count', array('conditions' => array('Order.userid' => $this->Auth->user('id'), 'Order.status' => 'Fill')));
        $this->set('cart_list', $cart_list);
        $product_annouced = $this->Productrequest->find('count', array('conditions' => array('Productrequest.user_id' => $this->Auth->user('id'), 'Productrequest.showmsg' => 'false', 'Productrequest.status' => 'ano')));
        $this->set('product_annouced', $product_annouced);
		
		$this->loadModel('Advertise');
		$Advertise = $this->Advertise->find('all', array('conditions' => array("OR"=>array('Advertise.user_ids like' => $this->Auth->user('id'),"Advertise.user_ids like '%,".$this->Auth->user('id')."%'","Advertise.user_ids like '%,".$this->Auth->user('id').",%'","Advertise.user_ids like '%".$this->Auth->user('id').",%'"), 'Advertise.status' => 1)));
		$this->set('Advertise', $Advertise);
    }

    /**

     * index method

     *

     * @return void

     */
    function index_demo() {
//Configure::write('debug',3);
        $this->layout = "index_demo";
        $cart_list = $this->Order->find('count', array('conditions' => array('Order.userid' => $this->Auth->user('id'), 'Order.status' => 'Fill')));
        $this->set('cart_list', $cart_list);
		$this->loadModel('Advertise');
		$Advertise = $this->Advertise->find('all', array('conditions' => array("OR"=>array('Advertise.user_ids like' => $this->Auth->user('id'),"Advertise.user_ids like '%,".$this->Auth->user('id')."%'","Advertise.user_ids like '%,".$this->Auth->user('id').",%'","Advertise.user_ids like '%".$this->Auth->user('id').",%'"), 'Advertise.status' => 1)));
		$this->set('Advertise', $Advertise);
        $product_annouced = $this->Productrequest->find('count', array('conditions' => array('Productrequest.user_id' => $this->Auth->user('id'), 'Productrequest.showmsg' => 'false', 'Productrequest.status' => 'ano')));
        $this->set('product_annouced', $product_annouced);
		
		$this->loadModel('Advertise');
		$Advertise = $this->Advertise->find('all', array('conditions' => array("OR"=>array('Advertise.user_ids like' => $this->Auth->user('id'),"Advertise.user_ids like '%,".$this->Auth->user('id')."%'","Advertise.user_ids like '%,".$this->Auth->user('id').",%'","Advertise.user_ids like '%".$this->Auth->user('id').",%'"), 'Advertise.status' => 1)));
		$this->set('Advertise', $Advertise);
    }
    
    
    public function products() {

        $this->layout = "index";

        $this->Product->recursive = 1;

        $this->set('products', $this->Product->find("all", array('conditions' => array('Product.status' => 1))));
    }

    /**

     * logout method

     *

     * @return void

     */
    function logout() {

        $this->Session->setFlash('Good-Bye', 'success');

        $this->redirect($this->Auth->logout());
    }

    /**

     * Profile method

     *

     * @return void

     */
    function profile() {

        //$this->layout = "index";
		 $this->layout = "index_demo";
        $cart_list = $this->Order->find('count', array('conditions' => array('Order.userid' => $this->Auth->user('id'), 'Order.status' => 'Fill')));
        $this->set('cart_list', $cart_list);
		$this->loadModel('Advertise');
		$Advertise = $this->Advertise->find('all', array('conditions' => array("OR"=>array('Advertise.user_ids like' => $this->Auth->user('id'),"Advertise.user_ids like '%,".$this->Auth->user('id')."%'","Advertise.user_ids like '%,".$this->Auth->user('id').",%'","Advertise.user_ids like '%".$this->Auth->user('id').",%'"), 'Advertise.status' => 1)));
		$this->set('Advertise', $Advertise);
        $this->User->id = $this->Auth->user('id');

        if (!$this->User->exists()) {

            throw new NotFoundException(__('Invalid user'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {

            $this->User->id = $this->request->data['User']['id'];
            //$this->User->saveField('password',$this->request->data['User']['password']);

            if ($this->User->saveField('password', $this->request->data['User']['password'])) {

                $this->Session->setFlash(__(' Password has been updated'), 'admin_success');

                $this->redirect(array('controller' => 'customers', 'action' => 'index'));
            } else {

                $this->Session->setFlash(__('The user account could not be updated. Please, try again.'), 'admin_error');
            }
        } else {

            $this->request->data = $this->User->read(null, $this->Auth->user('id'));
        }

        $this->request->data['User']['password'] = '';
    }

    /**

     * admin index  method

     *

     * @return void

     */
    public function admin_index() {

        $this->layout = "admin_dashboard";

        $this->set('title', __('Customers'));

        $this->set('description', __('Manage Customers'));

        $this->User->recursive = 1;

        $this->set('users', $this->paginate("User", array('NOT' => array('User.id' => $this->Auth->user('id')), 'User.group_id' => 2)));
    }

    /**

     * view method

     *

     * @param string $id

     * @return void

     */
    public function admin_view($id = null) {

        $this->layout = "ajax";

        $this->set('title', __('View'));

        $this->User->id = $id;

        if (!$this->User->exists()) {

            throw new NotFoundException(__('Invalid user'), 'error');
        }

        $this->set('user', $this->User->read(null, $id));
    }

    /**

     * add method

     *

     * @return void

     */
    public function admin_add() {

        $this->layout = "admin_dashboard";

        if ($this->request->is('post')) {

            $this->loadModel('User');

            $password = $this->request->data["User"]['password'];

            $this->User->create();

            if ($this->User->save($this->request->data)) {

                $this->Session->setFlash(__('The Customers has been saved'), 'admin_success');

                $email = new CakeEmail();

                try {

                    $email->config('default');
                } catch (Exception $e) {

                    Throw new ConfigureException('Config in email.php not found. ' . $e->getMessage());
                }

                $Templates = $this->EmailTemplateDescription->find('all', array('conditions' => array('EmailTemplateDescription.email_template_id' => 1)));

                $TempContent = stripcslashes($Templates[0]['EmailTemplateDescription']['content']);

                $TempContent = str_replace("{email}", $this->request->data["User"]['email'], $TempContent);

                $TempContent = str_replace("{date}", date("Y-m-d"), $TempContent);

                $TempContent = str_replace("{name}", $this->request->data["User"]['username'], $TempContent);

                $TempContent = str_replace("{password}", $password, $TempContent);

                $TempContent = str_replace("../../..", 'http://' . env('SERVER_NAME'), $TempContent);

                $data = array('Message' => $TempContent);

                $email->template('default', 'default');

                $email->emailFormat('both');

                $email->viewVars(array('data' => $data));

                $email->from(array('i-care@i-care.mobi' => 'JDPharmaceutical'));

                $email->to(Sanitize::clean($_REQUEST["email"]));

                $email->subject(__d('default', $Templates[0]['EmailTemplateDescription']['subject']));

                $email->send();

                $this->redirect(array('controller' => 'sales', 'action' => 'admin_index'));
            } else {

                $this->Session->setFlash(__('The Customers could not be saved. Please, try again.'), 'admin_error');
            }
        }





        $groups = $this->User->Group->find('list');

        $sales = $this->User->find('list', array('fields' => array('id', 'username'), 'conditions' => array('User.status' => 1, 'User.group_id' => 4)));





        $this->set(compact('groups', 'sales'));
    }

    /**

     * edit method

     *

     * @param string $id

     * @return void

     */
    public function admin_edit($id = null) {

        $this->layout = "admin_dashboard";

        $this->User->id = $id;

        if (!$this->User->exists()) {

            throw new NotFoundException(__('Invalid user'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {

            if ($this->User->save($this->request->data)) {

                $this->Session->setFlash(__('The Customers has been saved'), 'admin_success');

                $this->redirect(array('action' => 'index'));
            } else {

                $this->Session->setFlash(__('The Customers could not be saved. Please, try again.'), 'admin_error');
            }
        } else {

            $this->request->data = $this->User->read(null, $id);

            $this->request->data['User']['password'] = null;
        }

        $groups = $this->User->Group->find('list');

        $sales = $this->User->find('list', array('fields' => array('id', 'username'), array('conditions' => array('User.group_id' => 4))));



        $this->set(compact('groups'));
    }

    /**

     * delete method

     *

     * @param string $id

     * @return void

     */
    public function admin_delete($id = null) {

        if (!$this->request->is('post')) {

            throw new MethodNotAllowedException();
        }

        $this->User->id = $id;

        if (!$this->User->exists()) {

            throw new NotFoundException(__('Invalid user'));
        }

        if ($this->User->delete()) {

            $this->Session->setFlash(__('Customers deleted'), 'admin_success');

            $this->redirect(array('action' => 'index'));
        }

        $this->Session->setFlash(__('Customers was not deleted'), 'admin_error');

        $this->redirect(array('action' => 'index'));
    }

    /**

     *  Active/Inactive User

     *

     * @param <int> $user_id

     */
    public function toggle($user_id, $status) {

        $this->layout = "ajax";

        $status = ($status) ? 0 : 1;

        $this->set(compact('user_id', 'status'));

        if ($user_id) {

            $data['User'] = array('id' => $user_id, 'status' => $status);

            $allowed = $this->User->saveAll($data["User"], array('validate' => false));
        }
    }

    public function product_types($product_type = NULL) {
     //  Configure::write('debug',2);
        $this->layout = "index_demo";
        $cart_list = $this->Order->find('count', array('conditions' => array('Order.userid' => $this->Auth->user('id'), 'Order.status' => 'Fill')));
        
        $this->set('cart_list', $cart_list);
		$this->loadModel('Advertise');
		$Advertise = $this->Advertise->find('all', array('conditions' => array("OR"=>array('Advertise.user_ids like' => $this->Auth->user('id'),"Advertise.user_ids like '%,".$this->Auth->user('id')."%'","Advertise.user_ids like '%,".$this->Auth->user('id').",%'","Advertise.user_ids like '%".$this->Auth->user('id').",%'"), 'Advertise.status' => 1)));
		$this->set('Advertise', $Advertise);
        $this->Product->recursive = 1;
        $this->set('product_type', $product_type);
        if ($product_type != NULL) {

            $stock_status_type = $this->User->find('first', array('fields' => 'client_stock_type,threpercenteoff', 'conditions' => array('User.id' => $this->Auth->user('id'))));
         
            $this->set('threpercenteoff', $stock_status_type['User']['threpercenteoff']);
            $this->Paginator = $this->Components->load('Paginator');
            
            
            $conditions = array('products.producttype' => $product_type);
            if (!$stock_status_type['User']['client_stock_type'])
                $conditions['product_infos.batchno'] = "JD/STOCK";

            $products = $this->Paginator->paginate("ProductsC", array('limit' => 10,
                'conditions' => $conditions));
           // debug($products);exit;
            $this->set('products', $products);
        }

        //print_r($products);
    }

    public function add_restriction($product_type = NULL, $pdocuts_id) {

        $this->layout = "index";
        $product_type = explode(',', $product_type);
        $this->Session->setFlash(__('Please Contact Customer Service.'), 'admin_error');
        $this->redirect('product_types/' . $product_type[1]);
    }

    public function add_restriction_serach($searchtxt = NULL) {

        $this->layout = "index";
        $product_type = explode(',', $product_type);
        $this->Session->setFlash(__('Please Contact Customer Service.'), 'admin_error');
        $this->redirect('product_search?searchtxt=' . $searchtxt);
    }

    public function basketUpdate() {
        $this->layout = "index";
        $basket_id = '';

        // Configure::write('debug',3);

        if ($this->request->is('post')) {

            $this->paginate = array(
                'conditions' => array('Product.id' => $this->request->data['customers']['pro_id'])
            );

            $products = $this->paginate("Product");
            //print_r($products);die;
            $pro_id = $this->request->data['customers']['pro_id'];

            foreach ($products as $key => $product) {
                $product_instokc = 0;
                foreach ($product['ProductInfo'] as $ProductInfo) {
                    $instock = $instock + $ProductInfo['availability'];
                }
                $product_instokc = $instock;
            }

            //calculate client credit limit
            $order_details = $this->OrderDist->find('all', array('conditions' => array('OrderDist.userid' => $this->Auth->user('id'), 'OrderDist.Paid_Status' => 'unpaid', 'OrderDist.status' => array('Accep', 'Con', 'Fill', 'Test'))));
            $UnPaid_price = 0;

            foreach ($order_details as $ordtl) {
                foreach ($ordtl['OrderdetailMany'] as $ordpr) {
                    $UnPaid_price = $UnPaid_price + ($ordpr['price'] * $ordpr['quantity']);
                }
            }
            
            //get user current credit limit
            $user_credit = $this->User->find('first', array('fields' => array("Customer_Credit_Limit", "threpercenteoff", "JDLotPriority"), 'conditions' => array('User.id' => $this->Auth->user('id'))));
            
            $threpercenteoff = $user_credit['User']['threpercenteoff'];
            $customer_credit = $user_credit["User"]['Customer_Credit_Limit'];
            $JDLotPriority = $user_credit["User"]['JDLotPriority'];
            $purchasePrice = $products[0]['Product']['PurchasePrice'];
            
            $discount = ($this->Session->read("Auth.User.Refrigirated_discount")/100);
            
            $profit_percent = $purchasePrice * $discount;
            $priceafter_discount = ($profit_percent > 0) ? round($purchasePrice - ($profit_percent), 2) : $purchasePrice;
            $UnPaid_price = $UnPaid_price + ($priceafter_discount * $this->request->data['customers']['quantity']);

            if ($product_instokc < $this->request->data['customers']['quantity']) {
                $this->Session->setFlash(__('The Product could not be available.'), 'admin_error');
            } elseif ($customer_credit < $UnPaid_price) {
                $this->Session->setFlash(__('Sorry, Your Credit Limit has been Exceed . Please pay old invoices.'), 'admin_error');
            } else {
                foreach ($products as $key => $product) {
                    foreach ($product['ProductInfo'] as $ProductInfo) {
                        if ($ProductInfo['availability'] > 0) {
                            
                        }
                    }
                    $product_instokc = $instock;
                }
                $basket_id = $_COOKIE["basket_id"];


                if ($basket_id == '') {

                    // Generate a new basket_id and create entry in database
                    $basket_id = rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9);

                    $data = array(
                        'tmporderid' => $basket_id,
                        'createddt' => date("Y-m-d H:i:s"),
                        'status' => 'Fill',
                        'Paid_Status' => 'unpaid',
                        'userid' => $this->Auth->user('id')
                    );
                    $this->Order->create();
                    $this->Order->save($data);
                    $order_id = $this->Order->id;
                    setcookie("basket_id", $basket_id);
                } else {

                    $order_details = $this->Order->find('first', array('fields' => 'id', 'conditions' => array('Order.userid' => $this->Auth->user('id'), 'Order.tmporderid' => $basket_id, 'Order.status' => 'Fill')));
                    if (empty($order_details['Order'])) {
                        $basket_id = rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9);
                        $data = array(
                            'tmporderid' => $basket_id,
                            'createddt' => date("Y-m-d H:i:s"),
                            'status' => 'Fill',
                            'Paid_Status' => 'unpaid',
                            'userid' => $this->Auth->user('id')
                        );
                        $this->Order->create();
                        $this->Order->save($data);
                        $order_id = $this->Order->id;
                        setcookie("basket_id", $basket_id);
                    } else {
                        $order_id = $order_details['Order']['id'];
                    }
                }

                //find productt already in product info
                $Orderdetail = $this->Orderdetail->find('first', array('fields' => array('id', 'quantity'), 'conditions' => array('Orderdetail.tmporderid' => $basket_id, 'Orderdetail.productid' => $pro_id)));
                if (empty($Orderdetail)) {
                    //Apply price discount accourding client base;
                   // $productprice = $products[0]['Product']['productprice'];
                    $purchasePrice = $products[0]['Product']['PurchasePrice'];
                    //$product_prafit = $productprice - $purchasePrice;
                    $discount = ($this->Session->read("Auth.User.Refrigirated_discount")/100);
                    $profit_percent = $purchasePrice * $discount;
                    $original_price = $products[0]['Product']['original_price'];

                    //$discount = ($this->Session->read("Auth.User." . $products[0]['Product']['producttype'] . "_discount") / 100);
                    $priceafter_discount = ($profit_percent > 0) ? round($purchasePrice - ($profit_percent), 2) : $purchasePrice;
                   // if ($threpercenteoff)
                      //  $priceafter_discount = $priceafter_discount - ($priceafter_discount * $threpercenteoff / 100);
                    $data = array(
                        'orderid' => $order_id,
                        'tmporderid' => $basket_id,
                        'productid' => $pro_id,
                        'quantity' => $this->request->data['customers']['quantity'],
                        'original_p_price' => $original_price,
                        'price' => round($priceafter_discount, 2),
                        'createddt' => date("Y-m-d H:i:s"),
                        'orderstatus' => 'Fill',
                        'status' => 'Y'
                    );
                    $this->Orderdetail->create();
                    $this->Orderdetail->save($data);
                    $orderdetailsid = $this->Orderdetail->id;
                    
                    $this->Session->setFlash(__('The Product Added To Cart.'), 'admin_success');
                }else {
                    //print_r($Orderdetail);die;
                    $this->Orderdetail->id = $Orderdetail['Orderdetail']['id'];
                    $orderdetailsid = $this->Orderdetail->id;
                    $data = array(
                        'quantity' => $Orderdetail['Orderdetail']['quantity'] + $this->request->data['customers']['quantity'],
                        'createddt' => date("Y-m-d H:i:s"),
                        'status' => 'Y'
                    );
               
                    $this->Orderdetail->save($data);
                    $this->Session->setFlash(__('The Product Updated To Cart.'), 'admin_success');
                }

                $product_needed = $this->request->data['customers']['quantity']; //15
                $product_needed_remaning = $product_needed; //15
                foreach ($products as $key => $product) {
                    //firs jdp strat lot
                    if ($JDLotPriority)
                        $order_array = array('ProductInfo.Pedigree' => 'ASC');
                    else
                        $order_array = array('ProductInfo.expdate' => 'ASC');
                  //  print_r($order_array);exit;
                    
                    $prodcutinfo_all = $this->ProductInfo->find('all', array('order' => $order_array, 'conditions' => array('ProductInfo.prodid' => $product['Product']['id'])));
                    //print_r($prodcutinfo_all);die;
                    foreach ($product['ProductInfo'] as $ProductInfo) {
                        $totalprdoucavalibalilty = ($ProductInfo['balance'] > 0) ? $ProductInfo['balance'] : $ProductInfo['availability'];
                        if ($ProductInfo['availability'] > 0) {
                            
                            $available_product_quaanty = $ProductInfo['availability']; //10//30
                            $remaintin_product = $available_product_quaanty - $product_needed_remaning; //10-15=-5//30-5=25
                            $podcut_alloted = ($remaintin_product >= 0) ? $product_needed_remaning : $available_product_quaanty; //15-5=10//15-0
                            $availabproduct_update = ($remaintin_product < 0) ? 0 : $remaintin_product; //0//25
                            $product_needed_remaning = ($remaintin_product >= 0) ? 0 : $remaintin_product; //-5//0
                            $product_needed_remaning = -1 * $product_needed_remaning; //5//0
                            //$podcut_alloted=($product_needed-$product_needed_remaning);//15-5=10//15-0
                            //Alot lot
                            //update productinfo table
                            $this->ProductInfo->id = $ProductInfo['id'];

                            $this->ProductInfo->saveField('balance', $totalprdoucavalibalilty);
                            $this->ProductInfo->saveField('availability', $availabproduct_update);


                            //Apply price discount accourding client base;
                          //  $productprice = $product['Product']['productprice'];
                            $purchasePrice = $product['Product']['PurchasePrice'];
                            //$product_prafit = $productprice - $purchasePrice;
                            $discount = ($this->Session->read("Auth.User.Refrigirated_discount")/100);
                            $profit_percent = $purchasePrice * $discount;

                            $priceafter_discount = ($profit_percent > 0) ? round(($purchasePrice - $profit_percent), 2) : $purchasePrice;
                            /*if ($threpercenteoff)
                                $priceafter_discount = $priceafter_discount - ($priceafter_discount * $$threpercenteoff / 100);*/
                            //update lot nubmer
                            $data = array(
                                'product_id' => $product['Product']['id'],
                                'user_id' => $this->Auth->user('id'),
                                'quantity' => $podcut_alloted,
                                'price' => $priceafter_discount,
                                'orderid' => $order_id,
                                'tmporderid' => $basket_id,
                                'lot_no' => $ProductInfo['batchno'],
                                'create_date' => date("Y-m-d H:i:s"),
                                'status' => 'N'
                            );
                            //print_r($data);die;
                            $this->OrderLot->create();
                            $this->OrderLot->save($data);
                            $lotid = $this->OrderLot->id;

                            //add activityes
                            /* $activit_data=array(						
                              'product_info_id'=>$ProductInfo['id'],
                              'user_id'=> $this->Auth->user('id'),
                              'action'=>"Lot Alloted",
                              'action_on'=>'Lot',
                              'event_id'=>$lotid,
                              'created'=>date("Y-m-d H:i:s")
                              );
                              $this->admin_add_product_activity($activit_data);
                             */

                            $this->OrderLot->updateAll(
                                    array('OrderLot.tmporderid' => $basket_id, 'OrderLot.productinfoid' => $ProductInfo['id'], 'OrderLot.orderid' => $order_id, 'Orderdetailid' => $orderdetailsid), array('OrderLot.id' => $lotid)
                            );
                            if ($product_needed_remaning == 0)
                                break;
                        }
                    }
                    $product_instokc = $instock;
                }
            }
        }

        if (isset($this->request->data['customers']['product_search']))
            $this->redirect('product_search?searchtxt=' . $this->request->data['customers']['product_search']);
        else
            $this->redirect('product_types/' . $this->request->data['customers']['prodtype']);
    }

    public function order_cart() {
		//Configure::write('debug',2);
		ini_set('memory_limit', '-1');
        $this->layout = "index_demo";
        $cart_list = $this->Order->find('count', array('conditions' => array('Order.userid' => $this->Auth->user('id'), 'Order.status' => 'Fill')));
        $this->set('cart_list', $cart_list);

        $cart_list = $this->Order->find('all', array('recursive' => 2, 'conditions' => array('Order.userid' => $this->Auth->user('id'), 'Order.status' => 'Fill')));
        //print_r($cart_list);
        $this->set('products', $cart_list);
		$this->loadModel('Advertise');
		$Advertise = $this->Advertise->find('all', array('conditions' => array("OR"=>array('Advertise.user_ids like' => $this->Auth->user('id'),"Advertise.user_ids like '%,".$this->Auth->user('id')."%'","Advertise.user_ids like '%,".$this->Auth->user('id').",%'","Advertise.user_ids like '%".$this->Auth->user('id').",%'"), 'Advertise.status' => 1)));
		$this->set('Advertise', $Advertise);
    }

    public function insertAtPosition($string, $insert, $position) {
        return implode($insert, str_split($string, $position));
    }

    public function product_search() {
        $this->layout = "index_demo";
        $cart_list = $this->Order->find('count', array('conditions' => array('Order.userid' => $this->Auth->user('id'), 'Order.status' => 'Fill')));
        $this->set('cart_list', $cart_list);
		$this->loadModel('Advertise');
		$Advertise = $this->Advertise->find('all', array('conditions' => array("OR"=>array('Advertise.user_ids like' => $this->Auth->user('id'),"Advertise.user_ids like '%,".$this->Auth->user('id')."%'","Advertise.user_ids like '%,".$this->Auth->user('id').",%'","Advertise.user_ids like '%".$this->Auth->user('id').",%'"), 'Advertise.status' => 1)));
		$this->set('Advertise', $Advertise);
        if ($this->request->is('get')) {
            $search_text = trim($this->request->query['searchtxt']);
            $this->set('product_type', $this->request->query['searchtxt']);

            //ndc divide multi condtion
            $search_text_c1 = substr($search_text, 1, -1);
            $search_text_c2 = substr($search_text, 1);
            $search_text_c3 = substr($search_text, 0, -1);
            $search_text_c3 = '0' . $search_text;
            $search_text_c4 = $this->insertAtPosition($search_text, 0, 5);
            $search_text_c5 = $this->insertAtPosition($search_text, 0, 9);
            $search_text_c6 = '0' . $search_text_c1;
            $search_text_c7 = $this->insertAtPosition($search_text_c2, 0, 5);
            $search_text_c8 = $this->insertAtPosition($search_text_c2, 0, 9);
            $search_text_c9 = $this->insertAtPosition($search_text_c1, 0, 9);
            $search_text_c10 = $this->insertAtPosition($search_text_c1, 0, 5);
            //$this->request->data['customers']['searchtxt']=$search_text;

            $stock_status_type = $this->User->find('first', array('fields' => 'client_stock_type,threpercenteoff', 'conditions' => array('User.id' => $this->Auth->user('id'))));
            $this->set('threpercenteoff', $stock_status_type['User']['threpercenteoff']);
            $show_jdk_lot = false;
            if ($stock_status_type['User']['client_stock_type'])
                $show_jdk_lot = true;
            //$this->set('show_jdk_lot', $products);

            $this->paginate = array(
                'conditions' => array('OR' => array("Product.productname like '%" . $search_text . "%'", "Product.product_ndc" => array($search_text, $search_text_c1, $search_text_c2, $search_text_c3, $search_text_c4, $search_text_c5, $search_text_c6, $search_text_c7, $search_text_c8, $search_text_c9, $search_text_c10), "Product.itemnumber like '%" . $search_text . "%'"))
            );

            $products = $this->paginate("Product");
            foreach ($products as $key => $product) {
                $instock = 0;
                foreach ($product['ProductInfo'] as $ProductInfo) {
                    if ($show_jdk_lot == true)
                        $instock = $instock + $ProductInfo['availability'];
                    else {
                        if ($ProductInfo['batchno'] != 'JD/STOCK')
                            $instock = $instock + $ProductInfo['availability'];
                    }
                }
                $products[$key]['Product']['instock'] = $instock;
            }

            //print_r($products);
            $this->set('products', $products);
        }
    }

    public function removeorder_delete($ordetdetailsid = null) {
		ini_set('memory_limit', '-1');
        $order_lot = $this->OrderLot->find('all', array('fields' => array('productinfoid', 'orderid', 'Orderdetailid', 'quantity', 'id', 'product_id'), 'conditions' => array('OrderLot.Orderdetailid	' => $ordetdetailsid, 'OrderLot.user_id' => $this->Auth->user('id'))));



        foreach ($order_lot as $lot) {
            $findoldavai = $this->ProductInfo->find('first', array('conditions' => array('ProductInfo.id' => $lot['OrderLot']['productinfoid'])));

            $this->ProductInfo->updateAll(
                    array('ProductInfo.availability' => $findoldavai['ProductInfo']['availability'] + $lot['OrderLot']['quantity']), array('ProductInfo.id' => $lot['OrderLot']['productinfoid'])
            );

            $this->OrderLot->deleteAll(array('OrderLot.id' => $lot['OrderLot']['id'], 'OrderLot.user_id' => $this->Auth->user('id')), false);
            /* $activit_data=array(
              'product_info_id'=>$lot['OrderLot']['productinfoid'],
              'user_id'=> $this->Auth->user('id'),
              'action'=>"Lot ReAdded Order Remove",
              'action_on'=>'qnty',
              'event_id'=>$lot['OrderLot']['quantity'],
              'created'=>date("Y-m-d H:i:s")
              );
              $this->admin_add_product_activity($activit_data);
             */
        }

        $this->Session->setFlash(__('Order deleted'), 'admin_success');

        //$this->Order->deleteAll(array('Order.tmporderid' => $tmpid), false);
        $this->Orderdetail->deleteAll(array('Orderdetail.id' => $ordetdetailsid), false);
        //Order delete
        /* $activit_data=array(
          'product_id'=>$order_lot[0]['OrderLot']['productid'],
          'user_id'=> $this->Auth->user('id'),
          'action'=>"Prodcut Order Remove",
          'action_on'=>'Orderdetail',
          'event_id'=>$ordetdetailsid,
          'created'=>date("Y-m-d H:i:s")
          );
          $this->admin_add_product_activity($activit_data);
         */

        $Orderdetail = $this->Orderdetail->find('count', array('conditions' => array('Orderdetail.orderid' => $order_lot[0]['OrderLot']['orderid'], 'Orderdetail.orderstatus' => 'Fill')));
        if ($Orderdetail < 1) {
            $this->Order->deleteAll(array('Order.id' => $order_lot[0]['OrderLot']['orderid']), false);
            //Order delete
            /* $activit_data=array(
              'product_id'=>$order_lot[0]['OrderLot']['productid'],
              'user_id'=> $this->Auth->user('id'),
              'action'=>"Empty Cart",
              'action_on'=>'Order',
              'event_id'=>$order_lot[0]['OrderLot']['orderid'],
              'created'=>date("Y-m-d H:i:s")
              );
              $this->admin_add_product_activity($activit_data);
             */
        }
        $this->redirect(array('action' => 'order_cart'));
    }

    public function confirm_cort() {
        $this->layout = "index";
			ini_set('memory_limit', '-1');
        if ($this->request->is('post') || $this->request->is('put')) {
            $orderlist = $this->request->data['cart']['orderid'];
            $this->Order->updateAll(
                    array('Order.status' => "'Con'", 'Order.confirmat' => date("Y-m-d"), 'Order.po_number' => "'" . $this->request->data['cart']['po_number'] . "'"), array('Order.status' => "Fill", 'Order.userid' => $this->Auth->user('id'), "Order.tmporderid" => $orderlist)
            );

            $this->Orderdetail->updateAll(
                    array('Orderdetail.orderstatus' => "'Con'"), array('Orderdetail.orderstatus' => "Fill", "Orderdetail.tmporderid" => $orderlist)
            );

            $order_details = $this->Order->find('all', array('recursive' => 2, 'conditions' => array('Order.tmporderid' => $orderlist)));
            $order_user = $this->User->find('all', array('conditions' => array('User.id' => $order_details[0]['Orderdetail']['Product']['user_id'])));
            //Configure::write('debug',2);
            /* $activit_data=array(
              'product_id'=>$order_details[0]['Orderdetail']['productid'],
              'user_id'=> $this->Auth->user('id'),
              'action'=>"Order Confirm",
              'action_on'=>'Product',
              'event_id'=>$orderlist,
              'created'=>date("Y-m-d H:i:s")
              );
              $this->admin_add_product_activity($activit_data);
             */
            $Order_user_name = $order_details[0]['User']['username'];
            $tmporderid = $order_details[0]['Order']['tmporderid'];
            $fname = $order_user[0]['User']['fname'];

            $message = <<<HTM
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
</HEAD>

<BODY>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><p>Dear $Order_user_name,</p></td>
  </tr>
  <tr>
    <td><p>Your order with us was successful and order  number $tmporderid is being processed.<br>
    Kindly use your order  number to track products.</p></td>
  </tr>
  <tr>
    <td><p>Sincerely,</p></td>
  </tr>
  <tr>
    <td><p>JDPWholesaler.com</p></td>
  </tr>
  <tr>
    <td>$fname</td>
  </tr>
</table>
</BODY>
HTM;
            $user_name = $order_details[0]['User']['fname'];
            $user_address = $order_details[0]['User']['address'];
            $user_phone = $order_details[0]['User']['phone'];
            $user_emal = $order_details[0]['User']['email'];

//echo $message;die;
            /*             * * Set to address here  ** */
            $to = $order_details[0]['User']['email'];
//$to = "derou@me.com,khalid_hashmi88@yahoo.com,fattahaabdul@gmail.com";
            $adminemail = "info@jdpwholesaler.com";
            $adminname = "JDPWholesaler";
            /*             * * Set email subject here  ** */
            $subject = 'Order Confirmation ' . $order_details[0]['Order']['tmporderid'];
// To send HTML mail, the Content-type header must be set
            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
//$headers .= 'From: Testing <info@promptdesigners.com>' . "\r\n";
            $headers .= 'From: ' . $adminname . ' <' . $adminemail . '>' . "\r\n";

// Mail it
            mail($to, $subject, $message, $headers);
//mail('kali.dots@gmail.com', $subject, $message, $headers);
// mail send to admmail('kali.dots@gmail.com', $subject, $message, $headers);

            $itemlisting = '<link rel="stylesheet" type="text/css" href="http://jdpwholesaler.com/css/bootstrap.min.css" />';
            $sno = 1;
            $grandprice = 0;
            foreach ($order_details[0]['OrderdetailMany'] as $orded) {
                $itemlisting.='<tr>
    <td>' . $sno . '</td>   
    <td class="td-font">' . $orded['Product']['productname'] . '<br>' . $orded['Product']['product_ndc'] . '</td>
    <td class="td-font" align="left">' . $orded['quantity'] . '</td>
    <td class="td-font">' . $orded['price'] . '</td>
    <td class="td-font">' . round($orded['price'] * $orded['quantity'], 2) . '</td>    
  </tr>';
                $grandprice = $grandprice + round($orded['price'] * $orded['quantity'], 2);
                $sno++;
            }


            $message = <<<HTM
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
</HEAD>

<BODY>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><p>Hello,</p></td>
  </tr>
  <tr>
    <td><p>A product order has been placed. See Sales Panel to confirm and deliver the product.<br>
    Order Number : $tmporderid <br>
	<table class="table table-bordered" border="1">
        <thead>
          <tr>
            <th>
              <h4>Item(s)</h4>
            </th>
            <th>
              <h4>Description</h4>
            </th>
            <th>
              <h4>Qty</h4>
            </th>
            <th>
              <h4>Price</h4>
            </th>
            <th>
              <h4>Sub Total</h4>
            </th>
          </tr>
        </thead>
       

 				                
  <tbody>
      $itemlisting
       <tr><td colspan="4" align="right">Grand Total </td>
	   		<td>$grandprice</td>
		</tr>
        </tbody>
      </table>
	</p>
</td>
  </tr>
   <tr>
    <td>
<b>Client Information</b>
<p>
$user_name<br>
$user_address<br>
$user_phone<br>
$user_emal<br>

</p>
</td>
  </tr>
  <tr>
    <td><p>Sincerely,</p></td>
  </tr>
  <tr>
    <td><p>JDPWholesaler.com</p></td>
  </tr>
  
</table>
</BODY>
HTM;

            /*             * * Set to address here  ** */
//$to = $order_details[0]['User']['email'];
            $to = "derou@me.com,khalid_hashmi88@yahoo.com,fattahaabdul@gmail.com";
            $adminemail = "info@jdpwholesaler.com";
            $adminname = "JDPWholesaler";
            /*             * * Set email subject here  ** */
            $subject = 'Order Confirmation ' . $tmporderid;
// To send HTML mail, the Content-type header must be set
            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
//$headers .= 'From: Testing <info@promptdesigners.com>' . "\r\n";
            $headers .= 'From: ' . $adminname . ' <' . $adminemail . '>' . "\r\n";

// Mail it
            mail($to, $subject, $message, $headers);
//echo $message;die;
        }

        $this->Session->setFlash(__('Congratulations The Order Successfuly Submited.'), 'admin_success');
        $this->redirect(array('action' => 'index'));
    }

    public function order_empty($tmpid) {
        $order_lot = $this->OrderLot->find('all', array('fields' => array('productinfoid', 'orderid', 'Orderdetailid', 'quantity', 'id', 'product_id'), 'conditions' => array('OrderLot.tmporderid	' => $tmpid, 'OrderLot.user_id' => $this->Auth->user('id'))));

        foreach ($order_lot as $lot) {
            $findoldavai = $this->ProductInfo->find('first', array('conditions' => array('ProductInfo.id' => $lot['OrderLot']['productinfoid'])));
            $this->ProductInfo->updateAll(
                    array('ProductInfo.availability' => $findoldavai['ProductInfo']['availability'] + $lot['OrderLot']['quantity']), array('ProductInfo.id' => $lot['OrderLot']['productinfoid'])
            );

            /* $activit_data=array(
              'product_info_id'=>$lot['OrderLot']['productinfoid'],
              'user_id'=> $this->Auth->user('id'),
              'action'=>"Lot ReAdded Empty Card",
              'action_on'=>'qnty',
              'event_id'=>$lot['OrderLot']['quantity'],
              'created'=>date("Y-m-d H:i:s")
              );
              $this->admin_add_product_activity($activit_data);
             */

            $this->OrderLot->deleteAll(array('OrderLot.tmporderid' => $tmpid, 'OrderLot.user_id' => $this->Auth->user('id')), false);
        }
        /* $activit_data=array(
          'user_id'=> $this->Auth->user('id'),
          'action'=>"Empty Cart",
          'action_on'=>'tmporderid',
          'event_id'=>$tmpid,
          'created'=>date("Y-m-d H:i:s")
          );
          $this->admin_add_product_activity($activit_data);
         */
        $this->Session->setFlash(__('Order deleted'), 'admin_success');
        $this->Order->deleteAll(array('Order.tmporderid' => $tmpid), false);
        $this->Orderdetail->deleteAll(array('Orderdetail.tmporderid' => $tmpid), false);
        $this->redirect(array('action' => 'order_cart'));
    }

    public function producta_inventory() {
        $this->layout = "index_demo";
		$this->loadModel('OtherVendor');
        //Configure::write('debug',2);
        $cart_list = $this->Order->find('count', array('conditions' => array('Order.userid' => $this->Auth->user('id'), 'Order.status' => 'Fill')));
        $this->set('cart_list', $cart_list);
		$this->loadModel('Advertise');
		$Advertise = $this->Advertise->find('all', array('conditions' => array("OR"=>array('Advertise.user_ids like' => $this->Auth->user('id'),"Advertise.user_ids like '%,".$this->Auth->user('id')."%'","Advertise.user_ids like '%,".$this->Auth->user('id').",%'","Advertise.user_ids like '%".$this->Auth->user('id').",%'"), 'Advertise.status' => 1)));
		$this->set('Advertise', $Advertise);


        if ($this->request->is('post')) {

            if ($this->request->data['OrderHistory']['selfrmdate'] != '' and $this->request->data['OrderHistory']['seltodate'] != '') {
                $products = $this->OrderDist->find("all", array('recursive' => 2,
                    'conditions' => array('OrderDist.createddt >= ' => '2015-08-05', 'OrderDist.status' => array('Test', 'Accep'), "OrderDist.createddt>='" . $this->request->data['OrderHistory']['selfrmdate'] . "'", "OrderDist.createddt<='" . $this->request->data['OrderHistory']['seltodate'] . "'", 'OrderDist.userid' => $this->Auth->user('id')), 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.po_number'), 'order' => array('OrderDist.createddt' => 'desc')
                ));
            } else {
                $products = $this->OrderDist->find("all", array('recursive' => 2,
                    'conditions' => array('OrderDist.createddt >= ' => '2015-08-05', 'OrderDist.status' => array('Test', 'Accep'), 'OrderDist.userid' => $this->Auth->user('id')), 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.po_number'), 'order' => array('OrderDist.createddt' => 'desc')
                ));
            }
        } else {


			if ($this->request->data['OrderHistory']['selfrmdate']=='' and $this->request->data['OrderHistory']['seltodate']=='') {
			 $this->request->data['OrderHistory']['selfrmdate']=date('Y-m-d', strtotime("-10 days"));
			$this->request->data['OrderHistory']['seltodate']=date('Y-m-d');
		}

			$products = $this->OrderDist->find("all", array('recursive' => 2,
                    'conditions' => array('OrderDist.createddt >= ' => '2015-08-05', 'OrderDist.status' => array('Test', 'Accep'), "OrderDist.createddt>='" . $this->request->data['OrderHistory']['selfrmdate'] . "'", "OrderDist.createddt<='" . $this->request->data['OrderHistory']['seltodate'] . "'", 'OrderDist.userid' => $this->Auth->user('id')), 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.po_number'), 'order' => array('OrderDist.createddt' => 'desc')
                ));
        }


        $csvdata = array();
        $temp_array = array();
        $gkey = 0;
        foreach ($products as $key => $dt) {
            $product_qauantity = count($dt['OrderdetailMany']);
            $priice = 0;
            $product_name = "";
            $product_ndc = '';
            $item_number = '';
            $productprice = '';
            $producttype = '';
            $lot_ids = '';
            foreach ($dt['OrderdetailMany'] as $key2 => $orderdeatils) {


                if (($this->request->data['OrderHistory']['searchtxt'] != '' and trim($this->request->data['OrderHistory']['searchtxt']) == $orderdeatils['Product']['product_ndc']) or $this->request->data['OrderHistory']['searchtxt'] == '') {

                    $product_name = $orderdeatils['Product']['productname'];
                    $product_ndc = $orderdeatils['Product']['product_ndc'];

                    $itemnumber = $orderdeatils['Product']['itemnumber'];
                    $quantity = $orderdeatils['quantity'];
                    $product_size = $orderdeatils['Product']['product_size'];

                    $disposes = $this->ProductUsedSize->find('first', array('conditions' => array('ProductUsedSize.user_id' => $this->Auth->user('id'), 'ProductUsedSize.product_ndc' => $product_ndc), 'fields' => array('SUM(ProductUsedSize.size) as size')));
					
					$other_vendor = $this->OtherVendor->find('first', array('conditions' => array('OtherVendor.user_id' => $this->Auth->user('id'), 'OtherVendor.product_ndc' => $product_ndc), 'fields' => array('SUM(OtherVendor.quantity) as quantity')));

                    if (in_array($product_ndc, $temp_array)) {
                        $key_tmp = array_search($product_ndc, $temp_array);
                        $csvdata[$key_tmp]['Quantity'] = ($csvdata[$key_tmp]['Quantity']) + $quantity;
                        $csvdata[$key_tmp]['Total Size Purchase'] = ($csvdata[$key_tmp]['Total Size Purchase']) + $product_size * $quantity;
                        $csvdata[$key_tmp]['Total Left over'] = ($csvdata[$key_tmp]['Total Left over']) + $product_size * $quantity;
                    } else {
                        $temp_array[$gkey] = $product_ndc;
                        $quantity = (integer)$quantity + (integer)$other_vendor[0]['quantity'];
                        $csvdata[$gkey]['Product NDC'] = $product_ndc;
                        $csvdata[$gkey]['Item Number'] = $itemnumber;
                        $csvdata[$gkey]['Product Name'] = $product_name;
                        $csvdata[$gkey]['Quantity'] = $quantity;
                        $csvdata[$gkey]['Size'] = $product_size;

                        $csvdata[$gkey]['Total Size Purchase'] = $product_size * $quantity;
                        $csvdata[$gkey]['Total Dispense'] = $disposes[0]['size'];
                        $csvdata[$gkey]['Total Left over'] = ($product_size * $quantity) - $disposes[0]['size'];

                        $gkey++;
                    }
                }
            }
        }
        //print_r($csvdata);
        $this->set('users', $csvdata);
    }

    public function producta_inventory_excel($fromdate = NULL, $todate = NULL) {
        $this->layout = "index";
        $cart_list = $this->Order->find('count', array('conditions' => array('Order.userid' => $this->Auth->user('id'), 'Order.status' => 'Fill')));
        $this->set('cart_list', $cart_list);



        if ($fromdate != NULL and $todate != NULL) {


            $products = $this->OrderDist->find("all", array('recursive' => 2,
                'conditions' => array('OrderDist.createddt >= ' => '2015-08-05', 'OrderDist.status' => array('Test', 'Accep'), "OrderDist.createddt>='" . $fromdate . "'", "OrderDist.createddt<='" . $todate . "'", 'OrderDist.userid' => $this->Auth->user('id')), 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.po_number'), 'order' => array('OrderDist.createddt' => 'desc')
            ));
        } else {




            $products = $this->OrderDist->find("all", array('recursive' => 2,
                'conditions' => array('OrderDist.createddt >= ' => '2015-08-05', 'OrderDist.status' => array('Test', 'Accep'), 'OrderDist.userid' => $this->Auth->user('id')), 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.po_number'), 'order' => array('OrderDist.createddt' => 'desc')
            ));
        }


        $csvdata = array();
        $temp_array = array();
        $gkey = 0;
        foreach ($products as $key => $dt) {
            $product_qauantity = count($dt['OrderdetailMany']);
            $priice = 0;
            $product_name = "";
            $product_ndc = '';
            $item_number = '';
            $productprice = '';
            $producttype = '';
            $lot_ids = '';
            foreach ($dt['OrderdetailMany'] as $key2 => $orderdeatils) {


                if (($this->request->data['OrderHistory']['searchtxt'] != '' and trim($this->request->data['OrderHistory']['searchtxt']) == $orderdeatils['Product']['product_ndc']) or $this->request->data['OrderHistory']['searchtxt'] == '') {


                    $product_name = $orderdeatils['Product']['productname'];
                    $product_ndc = $orderdeatils['Product']['product_ndc'];

                    $itemnumber = $orderdeatils['Product']['itemnumber'];
                    $quantity = $orderdeatils['quantity'];
                    $product_size = $orderdeatils['Product']['product_size'];

                    $disposes = $this->ProductUsedSize->find('first', array('conditions' => array('ProductUsedSize.user_id' => $this->Auth->user('id'), 'ProductUsedSize.product_ndc' => $product_ndc), 'fields' => array('SUM(ProductUsedSize.size) as size')));
                    if (in_array($product_ndc, $temp_array)) {
                        $key_tmp = array_search($product_ndc, $temp_array);
                        $csvdata[$key_tmp]['Quantity'] = ($csvdata[$key_tmp]['Quantity']) + $quantity;
                        $csvdata[$key_tmp]['Total Size Purchase'] = ($csvdata[$key_tmp]['Total Size Purchase']) + $product_size * $quantity;
                        $csvdata[$key_tmp]['Total Left over'] = ($csvdata[$key_tmp]['Total Left over']) + $product_size * $quantity;
                    } else {
                        $temp_array[$gkey] = $product_ndc;
                        $csvdata[$gkey]['Product NDC'] = $product_ndc;
                        $csvdata[$gkey]['Item Number'] = $itemnumber;
                        $csvdata[$gkey]['Product Name'] = $product_name;
                        $csvdata[$gkey]['Quantity'] = $quantity;
                        $csvdata[$gkey]['Size'] = $product_size;

                        $csvdata[$gkey]['Total Size Purchase'] = $product_size * $quantity;
                        $csvdata[$gkey]['Total Dispense'] = $disposes[0]['size'];
                        $csvdata[$gkey]['Total Left over'] = ($product_size * $quantity) - $disposes[0]['size'];

                        $gkey++;
                    }
                }
            }
        }
        //print_r($csvdata);
        $userinfo = $this->User->find('first', array('conditions' => array('User.id' => $this->Auth->user('id'))));

        $this->Export->exportCsv($csvdata, '', $userinfo);
    }

   /* public function dispense_vendor_excel($selfrmdate=NULL,$seltodate=NULL,$pndc=NULL) {
		
        $this->layout = "index";
       //Configure::write('debug',2);
        $this->loadModel('OtherVendor');
        $cart_list = $this->Order->find('count', array('conditions' => array('Order.userid' => $this->Auth->user('id'), 'Order.status' => 'Fill')));
        $this->set('cart_list', $cart_list);

		
		if ($selfrmdate == NULL and $seltodate == NULL) {
			
			 $selfrmdate=date('Y-m-d', strtotime("-5 days"));
			$seltodate=date('Y-m-d');
		}
		if(date("Y",strtotime($selfrmdate))<2016){
			 $selfrmdate="2016-01-01";
		}


       

	if ($selfrmdate != NULL and $seltodate != NULL) {
		$products = $this->OrderDist->find("all", array('recursive' => 2,
            'conditions' => array('OrderDist.status' => array('Test', 'Accep'), 'OrderDist.userid' => $this->Auth->user('id'),"OrderDist.createddt>='" . $selfrmdate . "'", "OrderDist.createddt<='" . $seltodate . "'"), 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.po_number',"createddt"), 'order' => array('OrderDist.createddt' => 'desc')
        ));
		$this->set('seltodate', $seltodate);
            $this->set('selfrmdate', $selfrmdate);
	}else{
		$products = $this->OrderDist->find("all", array('recursive' => 2,
            'conditions' => array('OrderDist.status' => array('Test', 'Accep'), 'OrderDist.userid' => $this->Auth->user('id')), 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.po_number',"createddt"), 'order' => array('OrderDist.createddt' => 'desc')
        ));
		}
        
        
        $csvdata = array();
        $temp_array = array();
        $product_list = array();
        $product_list_auto = array();
		$global_ndc=array();
        $gkey = 0;
        foreach ($products as $key => $dt) {
            $product_qauantity = count($dt['OrderdetailMany']);
            $priice = 0;
            $product_name = "";
            $product_ndc = '';
            $item_number = '';
            $productprice = '';
            $producttype = '';
            $lot_ids = '';
            foreach ($dt['OrderdetailMany'] as $key2 => $orderdeatils) {

                
                
                if (($this->request->data['OrderHistory']['searchtxt'] != '' and trim($this->request->data['OrderHistory']['searchtxt']) == $orderdeatils['Product']['product_ndc']) or $this->request->data['OrderHistory']['searchtxt'] == '') {

                    $product_name = $orderdeatils['Product']['productname'];
                    $product_ndc = $orderdeatils['Product']['product_ndc'];
                    $productdate = $orderdeatils['Orderdetail']['createddt'];
                   
                    $itemnumber = $orderdeatils['Product']['itemnumber'];
                    $quantity = $orderdeatils['quantity'];
                    $product_size = $orderdeatils['Product']['product_size'];
                  if ($selfrmdate != NULL and $seltodate != NULL) {  
					$other_vendor = $this->OtherVendor->find('first', array('conditions' => array('OtherVendor.user_id' => $this->Auth->user('id'), 'OtherVendor.product_ndc' => $product_ndc,"OtherVendor.created>='" . $selfrmdate . "'", "OtherVendor.created<='" . $seltodate . "'"), 'fields' => array('SUM(OtherVendor.quantity) as quantity')));
					
					$disposes = $this->ProductUsedSize->find('first', array('conditions' => array('ProductUsedSize.user_id' => $this->Auth->user('id'), 'ProductUsedSize.product_ndc' => $product_ndc,"ProductUsedSize.date>='" . $selfrmdate . "'", "ProductUsedSize.date<='" . $seltodate . "'"), 'fields' => array('SUM(ProductUsedSize.size) as size')));
				  }else{
                    $other_vendor = $this->OtherVendor->find('first', array('conditions' => array('OtherVendor.user_id' => $this->Auth->user('id'), 'OtherVendor.product_ndc' => $product_ndc), 'fields' => array('SUM(OtherVendor.quantity) as quantity')));
					
					$disposes = $this->ProductUsedSize->find('first', array('conditions' => array('ProductUsedSize.user_id' => $this->Auth->user('id'), 'ProductUsedSize.product_ndc' => $product_ndc), 'fields' => array('SUM(ProductUsedSize.size) as size')));
				  }
                   //   $log = $this->OtherVendor->getDataSource()->getLog(false, false);
                 //   debug($log);
                   // exit;
                    
                    
                    if (in_array($product_ndc, $temp_array)) {

                        $key_tmp = array_search($product_ndc, $temp_array);
                        
                        $csvdata[$key_tmp]['Quantity'] = ($csvdata[$key_tmp]['Quantity']) + $quantity;
                        $csvdata[$gkey]['Date']=$orderdeatils["createddt"];
                        $csvdata[$key_tmp]['Total Size Purchase'] = ($csvdata[$key_tmp]['Total Size Purchase']) + $product_size * $quantity;
                        $csvdata[$key_tmp]['Total Left over'] = ($csvdata[$key_tmp]['Total Left over']) + $product_size * $quantity;
                        
                        
                    } else {
						$global_ndc[]=$product_ndc;
                        $quantity = (integer)$quantity + (integer)$other_vendor[0]['quantity'];
                        $product_list[$product_ndc] = $product_name;
                        $temp_array[$gkey] = $product_ndc;
                        $temauto["label"] = $product_ndc;
                        $temauto["value"] = $product_ndc;
                        $product_list_auto[] = $temauto;
                        
                        $csvdata[$gkey]['Product NDC'] = $product_ndc;
                        $csvdata[$gkey]['Item Number'] = $itemnumber;
                        $csvdata[$gkey]['Product Name'] = $product_name;
						$csvdata[$gkey]["Other Vendor"] = $other_vendor[0]['quantity']; // Atul-Dev-2 Code
                        $csvdata[$gkey]['Quantity'] = $quantity;
                        $csvdata[$gkey]['Size'] = $product_size;
                        $csvdata[$gkey]['Date']=$orderdeatils["createddt"];
                        $csvdata[$gkey]['Total Size Purchase'] = $product_size * $quantity;
                        $csvdata[$gkey]['Total Dispense'] = $disposes[0]['size'];
                        $csvdata[$gkey]['Total Left over'] = ($product_size * $quantity) - $disposes[0]['size'];
                        $tp = $gkey;
                        $gkey++;
                    }
                    
                   
                }
                
                
            }
            
            
        }
        *
         * Atul-Dev-2 Code
         
        foreach($csvdata as $data){
            $data["JDP Quantity"] = $data["Quantity"] - $data["Other Vendor"];
            $newcsvdata[] = $data;
            
        }
        
       
        $this->loadModel();
		 if ($selfrmdate != NULL and $seltodate != NULL) {  
					
					$ovproducts = $this->OtherVendor->find("all", array('conditions' => array("OtherVendor.created>='" . $this->request->query['selfrmdate'] . "'", "OtherVendor.created<='" . $seltodate . "'",'OtherVendor.user_id' => $this->Auth->user('id')), 'order' => array('OtherVendor.created' => 'desc'), 'fields' => array('SUM(OtherVendor.quantity) as quantity', 'name', 'product_ndc', 'created'), 'group' => '`OtherVendor`.`product_ndc`'
        ));
		 }else{
        $ovproducts = $this->OtherVendor->find("all", array('conditions' => array('OtherVendor.user_id' => $this->Auth->user('id')), 'order' => array('OtherVendor.created' => 'desc'), 'fields' => array('SUM(OtherVendor.quantity) as quantity', 'name', 'product_ndc', 'created'), 'group' => '`OtherVendor`.`product_ndc`'
        ));
		 }
         foreach($ovproducts as $ovproduct){
             
            $ndc =  $ovproduct["OtherVendor"]["product_ndc"]; 
			if(!in_array($ndc,$global_ndc)){
			$global_ndc[]=$ndc;
            $this->loadModel("Product");
            $productDetails = $this->Product->find("first",array("conditions" =>array("Product.product_ndc" => $ndc)));
			
            if ($selfrmdate != NULL and $seltodate != NULL) {
				$disposes = $this->ProductUsedSize->find('first', array('conditions' => array('ProductUsedSize.user_id' => $this->Auth->user('id'), 'ProductUsedSize.product_ndc' => $ndc,"ProductUsedSize.date>='" . $selfrmdate . "'", "ProductUsedSize.date<='" . $seltodate . "'"), 'fields' => array('SUM(ProductUsedSize.size) as size')));
			}else{		
            $disposes = $this->ProductUsedSize->find('first', array('conditions' => array('ProductUsedSize.user_id' => $this->Auth->user('id'), 'ProductUsedSize.product_ndc' => $ndc), 'fields' => array('SUM(ProductUsedSize.size) as size')));
			}
            if(!empty($disposes[0]['size'])){
                $disposess = $disposes[0]['size'];
            }else{
                
                $disposess = 0;
            }
           
            $ovdata["Date"] = $ovproduct["OtherVendor"]["created"];
            
            $ovdata["JDP Quantity"] = 0;
            $ovdata["Product NDC"] = $ndc;
            $ovdata["Item Number"] = $productDetails["Product"]["itemnumber"];
            $ovdata["Product Name"] = $productDetails["Product"]["productname"];
			$ovdata["Other Vendor"] = $ovproduct[0]["quantity"];
            $ovdata["Quantity"] = $ovproduct[0]["quantity"];
            $ovdata["Size"] = $productDetails["Product"]["product_size"];
            $ovdata['Total Size Purchase'] = $ovdata["Size"] * $ovproduct[0]["quantity"];
            $ovdata['Total Dispense']= $disposess;					
            $ovdata['Total Left over']= ($ovdata["Size"] * $ovdata["Quantity"]) - $disposes[0]['size'];
            array_push($newcsvdata,$ovdata);
			}
            
            
                       
        }
         
        
        
        
        *
         * Atul-Dev-2 Code
         * end
         
		if ($selfrmdate != NULL and $seltodate != NULL) { 
			 $ovproducts = $this->ProductUsedSize->find('all', array('conditions' => array("ProductUsedSize.date>='" . $selfrmdate . "'", "ProductUsedSize.date<='" . $seltodate . "'",'ProductUsedSize.user_id' => $this->Auth->user('id')), 'fields' => array('SUM(ProductUsedSize.size) as size', 'product_ndc'),'group' => '`ProductUsedSize`.`product_ndc`'));
		}else{
		 $ovproducts = $this->ProductUsedSize->find('all', array('conditions' => array('ProductUsedSize.user_id' => $this->Auth->user('id')), 'fields' => array('SUM(ProductUsedSize.size) as size', 'product_ndc'),'group' => '`ProductUsedSize`.`product_ndc`'));
		}
		 //print_r($ovproducts);
		 
         foreach($ovproducts as $ovproduct){
            
            $ndc =  $ovproduct["ProductUsedSize"]["product_ndc"]; 
            if(!in_array($ndc,$global_ndc)){
            $productDetails = $this->Product->find("first",array("conditions" =>array("Product.product_ndc" => $ndc)));
            
            $disposes = $ovproduct[0]["size"];
           
            $ovdata["JDP Quantity"] = 0;
            $ovdata["Product NDC"] = $ndc;
            $ovdata["Item Number"] = $productDetails["Product"]["itemnumber"];
            $ovdata["Product Name"] = $productDetails["Product"]["productname"];
			 $ovdata["Other Vendor"] = 0;
            $ovdata["Quantity"] = 0;
            $ovdata["Size"] = $productDetails["Product"]["product_size"];
            $ovdata['Total Size Purchase'] = 0;
            $ovdata['Total Dispense']=$disposes;					
            $ovdata['Total Left over']=-$disposes;
            array_push($newcsvdata,$ovdata);
			}
            
            
            
        }
        
        $totla_products = $this->Products->find("all", array('recursive' => 0,
            'conditions' => array("Products.active" => array('True', ''))));
        foreach ($totla_products as $tproduct) {
            $temauto["label"] = $tproduct['Products']['product_ndc'];
            $temauto["value"] = $tproduct['Products']['product_ndc'];
            $product_list_auto[] = $temauto;
            $product_list[$tproduct['Products']['product_ndc']] = $tproduct['Products']['productname'];
        }
   
        
        $userinfo = $this->User->find('first', array('conditions' => array('User.id' => $this->Auth->user('id'))));

        $this->Export->exportCsv($newcsvdata, '', $userinfo);
    
		}*/
	public function dispense_vendor_excel($selfrmdate=NULL,$seltodate=NULL,$pndc=NULL) {
		
        $this->layout = "index_demo";
       //Configure::write('debug',2);
        $this->loadModel('OtherVendor');
        $cart_list = $this->Order->find('count', array('conditions' => array('Order.userid' => $this->Auth->user('id'), 'Order.status' => 'Fill')));
        $this->set('cart_list', $cart_list);
		$this->loadModel('Advertise');
		$Advertise = $this->Advertise->find('all', array('conditions' => array("OR"=>array('Advertise.user_ids like' => $this->Auth->user('id'),"Advertise.user_ids like '%,".$this->Auth->user('id')."%'","Advertise.user_ids like '%,".$this->Auth->user('id').",%'","Advertise.user_ids like '%".$this->Auth->user('id').",%'"), 'Advertise.status' => 1)));
		$this->set('Advertise', $Advertise);


        
		//echo $this->request->query['selfrmdate'];
		if ($selfrmdate==NULL and $seltodate==NULL) {
			
			 $selfrmdate=date('Y-m-d', strtotime("-5 days"));
			$seltodate=date('Y-m-d');
		}
		if(date("Y",strtotime($selfrmdate))<2016){
			 $selfrmdate="2016-01-01";
		}
	
		
		
	if ($selfrmdate != NULL and $seltodate != NULL) {
		$products = $this->OrderDist->find("all", array('recursive' => 2,
            'conditions' => array('OrderDist.status' => array('Test', 'Accep'), 'OrderDist.userid' => $this->Auth->user('id'),"OrderDist.createddt>='" . $selfrmdate . "'", "OrderDist.createddt<='" . $seltodate . "'"), 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.po_number',"createddt"), 'order' => array('OrderDist.createddt' => 'desc')
        ));
		$this->set('seltodate', $seltodate);
            $this->set('selfrmdate', $selfrmdate);
	}else{
		$products = $this->OrderDist->find("all", array('recursive' => 2,
            'conditions' => array('OrderDist.status' => array('Test', 'Accep'), 'OrderDist.userid' => $this->Auth->user('id')), 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.po_number',"createddt"), 'order' => array('OrderDist.createddt' => 'desc')
        ));
		}
        
        
        $csvdata = array();
        $temp_array = array();
        $product_list = array();
        $product_list_auto = array();
		$product_listid_auto = array();
		$global_ndc=array();
        $gkey = 0;
        foreach ($products as $key => $dt) {
            $product_qauantity = count($dt['OrderdetailMany']);
            $priice = 0;
            $product_name = "";
            $product_ndc = '';
            $item_number = '';
            $productprice = '';
            $producttype = '';
            $lot_ids = '';
            foreach ($dt['OrderdetailMany'] as $key2 => $orderdeatils) {

                
                
                if ((trim($pndc) == $orderdeatils['Product']['product_ndc']) or $pndc== NULL) {

                    $product_name = $orderdeatils['Product']['productname'];
                    $product_ndc = $orderdeatils['Product']['product_ndc'];
                    $productdate = $orderdeatils['Orderdetail']['createddt'];
                   
                    $itemnumber = $orderdeatils['Product']['itemnumber'];
                    $quantity = $orderdeatils['quantity'];
                    $product_size = $orderdeatils['Product']['product_size'];
                  if ($selfrmdate != NULL and $seltodate != NULL) {  
					$other_vendor = $this->OtherVendor->find('first', array('conditions' => array('OtherVendor.user_id' => $this->Auth->user('id'), 'OtherVendor.product_ndc' => $product_ndc,"OtherVendor.created>='" . $selfrmdate . "'", "OtherVendor.created<='" . $seltodate . "'"), 'fields' => array('SUM(OtherVendor.quantity) as quantity')));
					
					$disposes = $this->ProductUsedSize->find('first', array('conditions' => array('ProductUsedSize.user_id' => $this->Auth->user('id'), 'ProductUsedSize.product_ndc' => $product_ndc,"ProductUsedSize.date>='" . $selfrmdate . "'", "ProductUsedSize.date<='" . $seltodate . "'"), 'fields' => array('SUM(ProductUsedSize.size) as size'),'order' => array('ProductUsedSize.id' => 'desc')));
					//disposedate
					$disposes_date = $this->ProductUsedSize->find('first', array('conditions' => array('ProductUsedSize.user_id' => $this->Auth->user('id'), 'ProductUsedSize.product_ndc' => $product_ndc,"ProductUsedSize.date>='" . $selfrmdate . "'", "ProductUsedSize.date<='" . $seltodate . "'"), 'fields' => array('ProductUsedSize.created'),'order' => array('ProductUsedSize.id' => 'desc')));
					
				  }else{
                    $other_vendor = $this->OtherVendor->find('first', array('conditions' => array('OtherVendor.user_id' => $this->Auth->user('id'), 'OtherVendor.product_ndc' => $product_ndc), 'fields' => array('SUM(OtherVendor.quantity) as quantity')));
					
					$disposes = $this->ProductUsedSize->find('first', array('conditions' => array('ProductUsedSize.user_id' => $this->Auth->user('id'), 'ProductUsedSize.product_ndc' => $product_ndc), 'fields' => array('SUM(ProductUsedSize.size) as size')));
				  }
                   //   $log = $this->OtherVendor->getDataSource()->getLog(false, false);
                 //   debug($log);
                   // exit;
                    
                    
                    if (in_array($product_ndc, $temp_array)) {

                        $key_tmp = array_search($product_ndc, $temp_array);
                        
                        $csvdata[$key_tmp]['Quantity'] = ($csvdata[$key_tmp]['Quantity']) + $quantity;
                        $csvdata[$gkey]['Date']=$orderdeatils["createddt"];
						$csvdata[$gkey]['Transaction Date']=$disposes_date['ProductUsedSize']['created'];
                        $csvdata[$key_tmp]['Total Size Purchase'] = ($csvdata[$key_tmp]['Total Size Purchase']) + $product_size * $quantity;
                        $csvdata[$key_tmp]['Total Left over'] = ($csvdata[$key_tmp]['Total Left over']) + $product_size * $quantity;
                        
                        
                    } else {
						$global_ndc[]=$product_ndc;
                        $quantity = (integer)$quantity + (integer)$other_vendor[0]['quantity'];
                        $product_list[$product_ndc] = $product_name;
                        $temp_array[$gkey] = $product_ndc;
                        $temauto["label"] = $product_ndc;
                        $temauto["value"] = $product_ndc;
                        $product_list_auto[] = $temauto;
						
						$temauto["label"] = $product_name;
                        $temauto["value"] = $product_ndc;
                        $product_listid_auto[] = $temauto;
                        
                        $csvdata[$gkey]['Product NDC'] = $product_ndc;
                        $csvdata[$gkey]['Item Number'] = $itemnumber;
                        $csvdata[$gkey]['Product Name'] = $product_name;
                        $csvdata[$gkey]['Quantity'] = $quantity;
                        $csvdata[$gkey]['Size'] = $product_size;
                        $csvdata[$gkey]['Date']=$orderdeatils["createddt"];
						$csvdata[$gkey]['Transaction Date']=$disposes_date['ProductUsedSize']['created'];
                        $csvdata[$gkey]['Total Size Purchase'] = $product_size * $quantity;
                        $csvdata[$gkey]['Total Dispense'] = $disposes[0]['size'];
						$csvdata[$gkey]["Other Vendor"] = $other_vendor[0]['quantity']; // Atul-Dev-2 Code
                        $csvdata[$gkey]['Total Left over'] = ($product_size * $quantity) - $disposes[0]['size'];
                        $tp = $gkey;
                        $gkey++;
                    }
                    
                   
                }
                
                
            }
            
            
        }
        /**
         * Atul-Dev-2 Code
         */
        foreach($csvdata as $data){
            $data["JDP Quantity"] = $data["Quantity"] - $data["other_vendor_quantity"];
            $newcsvdata[] = $data;
            
        }
        
        
        $this->loadModel();
		 if ($selfrmdate != NULL and $seltodate != NULL) {  
					
					$ovproducts = $this->OtherVendor->find("all", array('conditions' => array("OtherVendor.created>='" . $selfrmdate . "'", "OtherVendor.created<='" . $seltodate . "'",'OtherVendor.user_id' => $this->Auth->user('id')), 'order' => array('OtherVendor.created' => 'desc'), 'fields' => array('SUM(OtherVendor.quantity) as quantity', 'name', 'product_ndc', 'created'), 'group' => '`OtherVendor`.`product_ndc`'
        ));
		 }else{
        $ovproducts = $this->OtherVendor->find("all", array('conditions' => array('OtherVendor.user_id' => $this->Auth->user('id')), 'order' => array('OtherVendor.created' => 'desc'), 'fields' => array('SUM(OtherVendor.quantity) as quantity', 'name', 'product_ndc', 'created'), 'group' => '`OtherVendor`.`product_ndc`'
        ));
		 }
         foreach($ovproducts as $ovproduct){
             
            $ndc =  $ovproduct["OtherVendor"]["product_ndc"]; 
			if ((trim($pndc) == $ndc) or $pndc== NULL) {

			if(!in_array($ndc,$global_ndc)){
			$global_ndc[]=$ndc;
            $this->loadModel("Product");
            $productDetails = $this->Product->find("first",array("conditions" =>array("Product.product_ndc" => $ndc)));
			
            if ($selfrmdate != NULL and $seltodate != NULL) {
				$disposes = $this->ProductUsedSize->find('first', array('conditions' => array('ProductUsedSize.user_id' => $this->Auth->user('id'), 'ProductUsedSize.product_ndc' => $ndc,"ProductUsedSize.date>='" . $selfrmdate . "'", "ProductUsedSize.date<='" . $seltodate . "'"), 'fields' => array('SUM(ProductUsedSize.size) as size')));
				
				$disposes_date = $this->ProductUsedSize->find('first', array('conditions' => array('ProductUsedSize.user_id' => $this->Auth->user('id'), 'ProductUsedSize.product_ndc' => $ndc,"ProductUsedSize.date>='" . $selfrmdate . "'", "ProductUsedSize.date<='" . $seltodate . "'"), 'fields' => array('ProductUsedSize.created'),'order' => array('ProductUsedSize.created' => 'desc')));
			}else{		
            $disposes = $this->ProductUsedSize->find('first', array('conditions' => array('ProductUsedSize.user_id' => $this->Auth->user('id'), 'ProductUsedSize.product_ndc' => $ndc), 'fields' => array('SUM(ProductUsedSize.size) as size')));
			}
            if(!empty($disposes[0]['size'])){
                $disposess = $disposes[0]['size'];
            }else{
                
                $disposess = 0;
            }
           
            $ovdata["Date"] = $ovproduct["OtherVendor"]["created"];
			$ovdata['Transaction Date']=$disposes_date['ProductUsedSize']['created'];
            $ovdata["Other Vendor"] = $ovproduct[0]["quantity"];
            $ovdata["JDP Quantity"] = 0;
            $ovdata["Product NDC"] = $ndc;
            $ovdata["Item Number"] = $productDetails["Product"]["itemnumber"];
            $ovdata["Product Name"] = $productDetails["Product"]["productname"];
            $ovdata["Quantity"] = $ovproduct[0]["quantity"];
            $ovdata["Size"] = $productDetails["Product"]["product_size"];
            $ovdata['Total Size Purchase'] = $ovdata["Size"] * $ovproduct[0]["quantity"];
            $ovdata['Total Dispense']= $disposess;					
            $ovdata['Total Left over']= ($ovdata["Size"] * $ovdata["Quantity"]) - $disposes[0]['size'];
            array_push($newcsvdata,$ovdata);
			}
            
		 }
                       
        }
         
        
        
        
        /**
         * Atul-Dev-2 Code
         * end
         */
		if ($selfrmdate != NULL and $seltodate != NULL) { 
			 $ovproducts = $this->ProductUsedSize->find('all', array('conditions' => array("ProductUsedSize.date>='" . $selfrmdate . "'", "ProductUsedSize.date<='" . $seltodate . "'",'ProductUsedSize.user_id' => $this->Auth->user('id')), 'fields' => array('SUM(ProductUsedSize.size) as size', 'product_ndc'),'group' => '`ProductUsedSize`.`product_ndc`'));
			 $disposes_date = $this->ProductUsedSize->find('all', array('conditions' => array("ProductUsedSize.date>='" . $selfrmdate . "'", "ProductUsedSize.date<='" . $seltodate . "'",'ProductUsedSize.user_id' => $this->Auth->user('id')), 'fields' => array('ProductUsedSize.created','product_ndc'),'order' => array('ProductUsedSize.created' => 'desc'),'group' => '`ProductUsedSize`.`product_ndc`'));
		}else{
		 $ovproducts = $this->ProductUsedSize->find('all', array('conditions' => array('ProductUsedSize.user_id' => $this->Auth->user('id')), 'fields' => array('SUM(ProductUsedSize.size) as size', 'product_ndc'),'group' => '`ProductUsedSize`.`product_ndc`'));
		}
		 //print_r($ovproducts);
		 
         foreach($ovproducts as $ovproduct){
            
            $ndc =  $ovproduct["ProductUsedSize"]["product_ndc"]; 
			if ((trim($pndc)== $ndc) or $pndc== NULL) {
            if(!in_array($ndc,$global_ndc)){
            $productDetails = $this->Product->find("first",array("conditions" =>array("Product.product_ndc" => $ndc)));
            
            $disposes = $ovproduct[0]["size"];
            
			
            
            $ovdata["Product NDC"] = $ndc;
            $ovdata["Item Number"] = $productDetails["Product"]["itemnumber"];
            $ovdata["Product Name"] = $productDetails["Product"]["productname"];
			$ovdata["Other Vendor"] = 0;
            $ovdata["Quantity"] = 0;
            $ovdata["Size"] = $productDetails["Product"]["product_size"];
			$ovdata['Transaction Date']=$disposes_date['ProductUsedSize']['created'];
            $ovdata['Total Size Purchase'] = 0;
            $ovdata['Total Dispense']=$disposes;					
            $ovdata['Total Left over']=-$disposes;
			$ovdata["JDP Quantity"] = 0;
            array_push($newcsvdata,$ovdata);
			}
            
			}
            
        }
      //print_r($newcsvdata);die;
		 $userinfo = $this->User->find('first', array('conditions' => array('User.id' => $this->Auth->user('id'))));
        $this->Export->exportCsv($newcsvdata, '', $userinfo);
    
	}
	public function new_dispense(){
		 $this->layout = "index_demo";
		 ini_set('memory_limit', '-1');
            //Configure::write('debug',2);        
             $cart_list = $this->Order->find('count', array('conditions' => array('Order.userid' => $this->Auth->user('id'), 'Order.status' => 'Fill')));
             $this->set('cart_list', $cart_list);
		$this->loadModel('Advertise');
		$Advertise = $this->Advertise->find('all', array('conditions' => array("OR"=>array('Advertise.user_ids like' => $this->Auth->user('id'),"Advertise.user_ids like '%,".$this->Auth->user('id')."%'","Advertise.user_ids like '%,".$this->Auth->user('id').",%'","Advertise.user_ids like '%".$this->Auth->user('id').",%'"), 'Advertise.status' => 1)));
		$this->set('Advertise', $Advertise);
		$this->loadModel('Dispense');
		$this->loadModel('OtherVendor');
		
		if ($this->request->is('post')) {

           
				if($this->request->data['OrderHistory']['prodid']!=''){
				$product_id=$this->Products->find('first',array('recursive' => -1,'fields'=>array('Products.id'),'conditions'=>array('product_ndc'=>$this->request->data['OrderHistory']['prodid'])));
				
                $data['ProductUsedSize']['product_ndc'] = $this->request->data['OrderHistory']['prodid'];
                $data['ProductUsedSize']['size'] = $this->request->data['OrderHistory']['product_dispense'];
                $data['ProductUsedSize']['date'] = $this->request->data['OrderHistory']['date_time'];
                $data['ProductUsedSize']['user_id'] = $this->Auth->user('id');
				$data['ProductUsedSize']['productid'] = $product_id['Products']['id'];
				$data['ProductUsedSize']['created'] =date("Y-m-d H:i:s");
                $this->ProductUsedSize->create();
                $this->ProductUsedSize->save($data);

                $this->request->data['OrderHistory']['product_ndc'] = '';
				$this->request->data['OrderHistory']['prodid1'] = '';
				$this->request->data['OrderHistory']['prodid'] = '';
                $this->request->data['OrderHistory']['product_dispense'] = '';
                $this->request->data['OrderHistory']['date_time'] = '';
                $this->Session->setFlash(__('Dispense added Successfuly .'), 'admin_success');
				}else{
					 $this->Session->setFlash(__('Please select NDC .'), 'admin_error');
				}
		}
		
		$this->Dispense->recursive = 0;
		$condtitions=array();
		$condtitionsOtherVendor=array();
		$condtitionsProductUsedSize=array();
		$condtitions['user_id']=$this->Auth->user('id');
		$condtitionsOtherVendor['user_id']=$this->Auth->user('id');
		$condtitionsProductUsedSize['user_id']=$this->Auth->user('id');
		$limit=10;
		if($this->request->query['excel']){
			$limit=10000000000000;
		}
		if($this->request->query['selfrmdate']!=''){
			$this->set('selfrmdate', $this->request->query['selfrmdate']);
			$condtitions['created >']="'".$this->request->query['selfrmdate']."'";
			$condtitionsOtherVendor['OtherVendor.created >=']=$this->request->query['selfrmdate'];
			$condtitionsProductUsedSize['ProductUsedSize.date >=']=$this->request->query['selfrmdate'];
		}
		
		if($this->request->query['seltodate']!=''){
			$this->set('seltodate', $this->request->query['seltodate']);
			$condtitions['created <']="'".$this->request->query['seltodate']."'";
			$condtitionsOtherVendor['date(created) <=']=$this->request->query['seltodate'];
			$condtitionsProductUsedSize['date(date) <=']=$this->request->query['seltodate'];
		}
		if($this->request->query['pndc']!=''){
			$productid=$this->Product->find('first',array('recursive' => -1,'conditions'=>array('Product.product_ndc like '=>"%".$this->request->query['pndc']),'fields'>=array('Product.id')));
			if(!empty($productid['Product'])){
			$condtitions['productid']=$productid['Product']['id'];			
			}else{
			$condtitions['productid']=0;			
			}
		}
		//print_r($condtitions);
		 $this->paginate = array('conditions' => $condtitions,'limit'=>$limit);
		$Dispense = $this->paginate("Dispense");
		//print_r($Dispense);
		$dataGloobal=array();
		foreach($Dispense as $key=>$Dispen){
			$productDetail=$this->Product->find('first',array('recursive' => -1,'conditions'=>array('Product.id'=>$Dispen['t']['productid'])));
			$condtitionsOtherVendor['productid']=$Dispen['t']['productid'];
			$other_vendor = $this->OtherVendor->find('first', array('conditions' =>$condtitionsOtherVendor, 'fields' => array('SUM(OtherVendor.quantity) as quantity','created')));
			//print_r($other_vendor);
			$condtitionsProductUsedSize['ProductUsedSize.productid']=$Dispen['t']['productid'];
			$disposes = $this->ProductUsedSize->find('first', array('conditions' => $condtitionsProductUsedSize, 'fields' => array('SUM(ProductUsedSize.size) as size'),'order' => array('ProductUsedSize.id' => 'desc')));
			//print_r($condtitionsProductUsedSize);
			//$disposes_date = $this->ProductUsedSize->find('first', array('conditions' => $condtitionsProductUsedSize, 'fields' => array('ProductUsedSize.created'),'order' => array('ProductUsedSize.created' => 'DESC')));
			//print_r($Dispen);die;
			$ovdata='';
			$ovdata["Transaction Date"] = $Dispen[0]["created"];
			//$ovdata['AddDate']=$disposes_date['ProductUsedSize']['created'];
            $ovdata["other_vendor_quantity"] = (integer)$other_vendor[0]["quantity"];
            $ovdata["jdp_quantity"] = $Dispen[0]["total"]-((integer)$other_vendor[0]["quantity"]);
            $ovdata["Product NDC"] = $productDetail['Product']['product_ndc'];
            $ovdata["Item Number"] = $productDetail["Product"]["itemnumber"];
            $ovdata["Product Name"] = $productDetail["Product"]["productname"];
            $ovdata["Quantity"] = $Dispen[0]["total"];
            $ovdata["Size"] = $productDetail["Product"]["product_size"];
            $ovdata['Total Size Purchase'] = $productDetail["Product"]["product_size"] * $Dispen[0]["total"];
            $ovdata['Total Dispense']= $disposes[0]['size'];					
            $ovdata['Total Left over']= ($productDetail["Product"]["product_size"] * $Dispen[0]["total"]) - $disposes[0]['size'];
            array_push($dataGloobal,$ovdata);

			
		}
		//print_r($dataGloobal);die;
		
		if($this->request->query['excel']){
			$userinfo = $this->User->find('first', array('conditions' => array('User.id' => $this->Auth->user('id'))));
        	$this->Export->exportCsv($dataGloobal, '', $userinfo);
		}else{
			$totla_products = $this->Products->find("all", array('recursive' => 0));
			foreach ($totla_products as $tproduct) {
				$temauto["label"] = $tproduct['Products']['product_ndc'];
				$temauto["value"] = $tproduct['Products']['product_ndc'];
				$product_list_auto[] = $temauto;
				
				$temauto["label"] = $tproduct['Products']['productname'];
				$temauto["value"] = $tproduct['Products']['product_ndc'];
				$product_listid_auto[] = $temauto;
				
				$product_list[$tproduct['Products']['product_ndc']] = $tproduct['Products']['productname'];
			}
			$pndc=$this->request->query['pndc'];
			$this->set('pndc',$pndc);
        	$this->set('users', $dataGloobal);
       		$this->set('product_list_auto', $product_list_auto);
			$this->set('product_listid_auto', $product_listid_auto);
        	$this->set('product_list', $product_list);
		}
	}
    public function dispense() {
        $this->layout = "index_demo";
       //Configure::write('debug',2);
        $this->loadModel('OtherVendor');
        $cart_list = $this->Order->find('count', array('conditions' => array('Order.userid' => $this->Auth->user('id'), 'Order.status' => 'Fill')));
        $this->set('cart_list', $cart_list);
		$this->loadModel('Advertise');
		$Advertise = $this->Advertise->find('all', array('conditions' => array("OR"=>array('Advertise.user_ids like' => $this->Auth->user('id'),"Advertise.user_ids like '%,".$this->Auth->user('id')."%'","Advertise.user_ids like '%,".$this->Auth->user('id').",%'","Advertise.user_ids like '%".$this->Auth->user('id').",%'"), 'Advertise.status' => 1)));
		$this->set('Advertise', $Advertise);


        if ($this->request->is('post')) {

            /* $disposes=$this->ProductUsedSize->find('first',array('conditions'=>array('ProductUsedSize.user_id'=>$this->Auth->user('id'),'ProductUsedSize.product_ndc'=>$this->request->data['OrderHistory']['prodid']))); */
            //print_r($this->request->data);die;
            if (1) {
				if($this->request->data['OrderHistory']['prodid']!=''){
				$product_id=$this->Products->find('first',array('recursive' => -1,'fields'=>array('Products.id'),'conditions'=>array('product_ndc'=>$this->request->data['OrderHistory']['prodid'])));
				
                $data['ProductUsedSize']['product_ndc'] = $this->request->data['OrderHistory']['prodid'];
                $data['ProductUsedSize']['size'] = $this->request->data['OrderHistory']['product_dispense'];
                $data['ProductUsedSize']['date'] = $this->request->data['OrderHistory']['date_time'];
                $data['ProductUsedSize']['user_id'] = $this->Auth->user('id');
				$data['ProductUsedSize']['productid'] = $product_id['Products']['id'];
                $this->ProductUsedSize->create();
                $this->ProductUsedSize->save($data);

                $this->request->data['OrderHistory']['product_ndc'] = '';
				$this->request->data['OrderHistory']['prodid1'] = '';
				$this->request->data['OrderHistory']['prodid'] = '';
                $this->request->data['OrderHistory']['product_dispense'] = '';
                $this->request->data['OrderHistory']['date_time'] = '';
                $this->Session->setFlash(__('Dispense added Successfuly .'), 'admin_success');
				}else{
					 $this->Session->setFlash(__('Please select NDC .'), 'admin_error');
				}
            } else {

                $this->ProductUsedSize->updateAll(
                        array('ProductUsedSize.size' => $this->request->data['OrderHistory']['product_dispense'],
                    'ProductUsedSize.date' => "'" . $this->request->data['OrderHistory']['date_time'] . "'"
                        ), array('ProductUsedSize.id' => $disposes['ProductUsedSize']['id'])
                );

                $this->request->data['OrderHistory']['prodid'] = '';
                $this->request->data['OrderHistory']['product_dispense'] = '';
                $this->request->data['OrderHistory']['date_time'] = '';
                $this->Session->setFlash(__('Dispense updated Successfuly .'), 'admin_success');
            }
        } else {
            
        }
		//echo $this->request->query['selfrmdate'];
		if ($this->request->query['selfrmdate']=='' and $this->request->query['seltodate']=='') {
			
			 $this->request->query['selfrmdate']=date('Y-m-d', strtotime("-5 days"));
			$this->request->query['seltodate']=date('Y-m-d');
		}
		if(date("Y",strtotime($this->request->query['selfrmdate']))<2016){
			 $this->request->query['selfrmdate']="2016-01-01";
		}
	
		
		
	if ($this->request->query['selfrmdate'] != '' and $this->request->query['seltodate'] != '') {
		$products = $this->OrderDist->find("all", array('recursive' => 2,
            'conditions' => array('OrderDist.status' => array('Test', 'Accep'), 'OrderDist.userid' => $this->Auth->user('id'),"OrderDist.createddt>='" . $this->request->query['selfrmdate'] . "'", "OrderDist.createddt<='" . $this->request->query['seltodate'] . "'"), 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.po_number',"createddt"), 'order' => array('OrderDist.createddt' => 'desc')
        ));
		$this->set('seltodate', $this->request->query['seltodate']);
            $this->set('selfrmdate', $this->request->query['selfrmdate']);
	}else{
		$products = $this->OrderDist->find("all", array('recursive' => 2,
            'conditions' => array('OrderDist.status' => array('Test', 'Accep'), 'OrderDist.userid' => $this->Auth->user('id')), 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.po_number',"createddt"), 'order' => array('OrderDist.createddt' => 'desc')
        ));
		}
        
        
        $csvdata = array();
        $temp_array = array();
        $product_list = array();
        $product_list_auto = array();
		$product_listid_auto = array();
		$global_ndc=array();
        $gkey = 0;
        foreach ($products as $key => $dt) {
            $product_qauantity = count($dt['OrderdetailMany']);
            $priice = 0;
            $product_name = "";
            $product_ndc = '';
            $item_number = '';
            $productprice = '';
            $producttype = '';
            $lot_ids = '';
            foreach ($dt['OrderdetailMany'] as $key2 => $orderdeatils) {

                
                
                if ((trim($this->request->query['pndc']) == $orderdeatils['Product']['product_ndc']) or $this->request->query['pndc']== '') {

                    $product_name = $orderdeatils['Product']['productname'];
                    $product_ndc = $orderdeatils['Product']['product_ndc'];
                    $productdate = $orderdeatils['Orderdetail']['createddt'];
                   
                    $itemnumber = $orderdeatils['Product']['itemnumber'];
                    $quantity = $orderdeatils['quantity'];
                    $product_size = $orderdeatils['Product']['product_size'];
                  if ($this->request->query['selfrmdate'] != '' and $this->request->query['seltodate'] != '') {  
					$other_vendor = $this->OtherVendor->find('first', array('conditions' => array('OtherVendor.user_id' => $this->Auth->user('id'), 'OtherVendor.product_ndc' => $product_ndc,"OtherVendor.created>='" . $this->request->query['selfrmdate'] . "'", "OtherVendor.created<='" . $this->request->query['seltodate'] . "'"), 'fields' => array('SUM(OtherVendor.quantity) as quantity')));
					
					$disposes = $this->ProductUsedSize->find('first', array('conditions' => array('ProductUsedSize.user_id' => $this->Auth->user('id'), 'ProductUsedSize.product_ndc' => $product_ndc,"ProductUsedSize.date>='" . $this->request->query['selfrmdate'] . "'", "ProductUsedSize.date<='" . $this->request->query['seltodate'] . "'"), 'fields' => array('SUM(ProductUsedSize.size) as size'),'order' => array('ProductUsedSize.id' => 'desc')));
					//disposedate
					$disposes_date = $this->ProductUsedSize->find('first', array('conditions' => array('ProductUsedSize.user_id' => $this->Auth->user('id'), 'ProductUsedSize.product_ndc' => $product_ndc,"ProductUsedSize.date>='" . $this->request->query['selfrmdate'] . "'", "ProductUsedSize.date<='" . $this->request->query['seltodate'] . "'"), 'fields' => array('ProductUsedSize.created'),'order' => array('ProductUsedSize.id' => 'desc')));
					
				  }else{
                    $other_vendor = $this->OtherVendor->find('first', array('conditions' => array('OtherVendor.user_id' => $this->Auth->user('id'), 'OtherVendor.product_ndc' => $product_ndc), 'fields' => array('SUM(OtherVendor.quantity) as quantity')));
					
					$disposes = $this->ProductUsedSize->find('first', array('conditions' => array('ProductUsedSize.user_id' => $this->Auth->user('id'), 'ProductUsedSize.product_ndc' => $product_ndc), 'fields' => array('SUM(ProductUsedSize.size) as size')));
				  }
                   //   $log = $this->OtherVendor->getDataSource()->getLog(false, false);
                 //   debug($log);
                   // exit;
                    
                    
                    if (in_array($product_ndc, $temp_array)) {

                        $key_tmp = array_search($product_ndc, $temp_array);
                        
                        $csvdata[$key_tmp]['Quantity'] = ($csvdata[$key_tmp]['Quantity']) + $quantity;
                        $csvdata[$gkey]['Date']=$orderdeatils["createddt"];
						$csvdata[$gkey]['AddDate']=$disposes_date['ProductUsedSize']['created'];
                        $csvdata[$key_tmp]['Total Size Purchase'] = ($csvdata[$key_tmp]['Total Size Purchase']) + $product_size * $quantity;
                        $csvdata[$key_tmp]['Total Left over'] = ($csvdata[$key_tmp]['Total Left over']) + $product_size * $quantity;
                        
                        
                    } else {
                        
                            $global_ndc[]=$product_ndc;
                        $quantity = (integer)$quantity + (integer)$other_vendor[0]['quantity'];
                        $product_list[$product_ndc] = $product_name;
                        $temp_array[$gkey] = $product_ndc;
                        $temauto["label"] = $product_ndc;
                        $temauto["value"] = $product_ndc;
                        $product_list_auto[] = $temauto;
						
						$temauto["label"] = $product_name;
                        $temauto["value"] = $product_ndc;
                        $product_listid_auto[] = $temauto;
                        $csvdata[$gkey]["other_vendor_quantity"] = $other_vendor[0]['quantity']; // Atul-Dev-2 Code
                        $csvdata[$gkey]['Product NDC'] = $product_ndc;
                        $csvdata[$gkey]['Item Number'] = $itemnumber;
                        $csvdata[$gkey]['Product Name'] = $product_name;
                        $csvdata[$gkey]['Quantity'] = $quantity;
                        $csvdata[$gkey]['Size'] = $product_size;
                        $csvdata[$gkey]['Date']=$orderdeatils["createddt"];
						$csvdata[$gkey]['AddDate']=$disposes_date['ProductUsedSize']['created'];
                        $csvdata[$gkey]['Total Size Purchase'] = $product_size * $quantity;
                        $csvdata[$gkey]['Total Dispense'] = $disposes[0]['size'];
                        $csvdata[$gkey]['Total Left over'] = ($product_size * $quantity) - $disposes[0]['size'];
                        $tp = $gkey;
                        $gkey++;
                    }
                    
                   
                }
                
                
            }
            
            
        }
        /**
         * Atul-Dev-2 Code
         */
        foreach($csvdata as $data){
            $data["jdp_quantity"] = $data["Quantity"] - $data["other_vendor_quantity"];
            $newcsvdata[] = $data;
            
        }
        
       $this->loadModel();
		 if ($this->request->query['selfrmdate'] != '' and $this->request->query['seltodate'] != '') {  
					
					$ovproducts = $this->OtherVendor->find("all", array('conditions' => array("OtherVendor.created>='" . $this->request->query['selfrmdate'] . "'", "OtherVendor.created<='" . $this->request->query['seltodate'] . "'",'OtherVendor.user_id' => $this->Auth->user('id')), 'order' => array('OtherVendor.created' => 'desc'), 'fields' => array('SUM(OtherVendor.quantity) as quantity', 'name', 'product_ndc', 'created'), 'group' => '`OtherVendor`.`product_ndc`'
        ));
		 }else{
        $ovproducts = $this->OtherVendor->find("all", array('conditions' => array('OtherVendor.user_id' => $this->Auth->user('id')), 'order' => array('OtherVendor.created' => 'desc'), 'fields' => array('SUM(OtherVendor.quantity) as quantity', 'name', 'product_ndc', 'created'), 'group' => '`OtherVendor`.`product_ndc`'
        ));
		 }
         foreach($ovproducts as $ovproduct){
             
            $ndc =  $ovproduct["OtherVendor"]["product_ndc"]; 
			if ((trim($this->request->query['pndc']) == $ndc) or $this->request->query['pndc']== '') {

			if(!in_array($ndc,$global_ndc)){
			$global_ndc[]=$ndc;
            $this->loadModel("Product");
            $productDetails = $this->Product->find("first",array("conditions" =>array("Product.product_ndc" => $ndc)));
			
            if ($this->request->query['selfrmdate'] != '' and $this->request->query['seltodate'] != '') {
				$disposes = $this->ProductUsedSize->find('first', array('conditions' => array('ProductUsedSize.user_id' => $this->Auth->user('id'), 'ProductUsedSize.product_ndc' => $ndc,"ProductUsedSize.date>='" . $this->request->query['selfrmdate'] . "'", "ProductUsedSize.date<='" . $this->request->query['seltodate'] . "'"), 'fields' => array('SUM(ProductUsedSize.size) as size')));
				
				$disposes_date = $this->ProductUsedSize->find('first', array('conditions' => array('ProductUsedSize.user_id' => $this->Auth->user('id'), 'ProductUsedSize.product_ndc' => $ndc,"ProductUsedSize.date>='" . $this->request->query['selfrmdate'] . "'", "ProductUsedSize.date<='" . $this->request->query['seltodate'] . "'"), 'fields' => array('ProductUsedSize.created'),'order' => array('ProductUsedSize.created' => 'desc')));
			}else{		
            $disposes = $this->ProductUsedSize->find('first', array('conditions' => array('ProductUsedSize.user_id' => $this->Auth->user('id'), 'ProductUsedSize.product_ndc' => $ndc), 'fields' => array('SUM(ProductUsedSize.size) as size')));
			}
            if(!empty($disposes[0]['size'])){
                $disposess = $disposes[0]['size'];
            }else{
                
                $disposess = 0;
            }
           
            $ovdata["Date"] = $ovproduct["OtherVendor"]["created"];
			$ovdata['AddDate']=$disposes_date['ProductUsedSize']['created'];
            $ovdata["other_vendor_quantity"] = $ovproduct[0]["quantity"];
            $ovdata["jdp_quantity"] = 0;
            $ovdata["Product NDC"] = $ndc;
            $ovdata["Item Number"] = $productDetails["Product"]["itemnumber"];
            $ovdata["Product Name"] = $productDetails["Product"]["productname"];
            $ovdata["Quantity"] = $ovproduct[0]["quantity"];
            $ovdata["Size"] = $productDetails["Product"]["product_size"];
            $ovdata['Total Size Purchase'] = $ovdata["Size"] * $ovproduct[0]["quantity"];
            $ovdata['Total Dispense']= $disposess;					
            $ovdata['Total Left over']= ($ovdata["Size"] * $ovdata["Quantity"]) - $disposes[0]['size'];
            array_push($newcsvdata,$ovdata);
			}
            
		 }
                       
        }
         
        
        
        
        /**
         * Atul-Dev-2 Code
         * end
         */
		if ($this->request->query['selfrmdate'] != '' and $this->request->query['seltodate'] != '') { 
			 $ovproducts = $this->ProductUsedSize->find('all', array('conditions' => array("ProductUsedSize.date>='" . $this->request->query['selfrmdate'] . "'", "ProductUsedSize.date<='" . $this->request->query['seltodate'] . "'",'ProductUsedSize.user_id' => $this->Auth->user('id')), 'fields' => array('SUM(ProductUsedSize.size) as size', 'product_ndc'),'group' => '`ProductUsedSize`.`product_ndc`'));
			 $disposes_date = $this->ProductUsedSize->find('all', array('conditions' => array("ProductUsedSize.date>='" . $this->request->query['selfrmdate'] . "'", "ProductUsedSize.date<='" . $this->request->query['seltodate'] . "'",'ProductUsedSize.user_id' => $this->Auth->user('id')), 'fields' => array('ProductUsedSize.created','product_ndc'),'order' => array('ProductUsedSize.created' => 'desc'),'group' => '`ProductUsedSize`.`product_ndc`'));
		}else{
		 $ovproducts = $this->ProductUsedSize->find('all', array('conditions' => array('ProductUsedSize.user_id' => $this->Auth->user('id')), 'fields' => array('SUM(ProductUsedSize.size) as size', 'product_ndc'),'group' => '`ProductUsedSize`.`product_ndc`'));
		}
		 //print_r($ovproducts);
		 
         foreach($ovproducts as $ovproduct){
            
            $ndc =  $ovproduct["ProductUsedSize"]["product_ndc"]; 
			if ((trim($this->request->query['pndc']) == $ndc) or $this->request->query['pndc']== '') {
            if(!in_array($ndc,$global_ndc)){
            $productDetails = $this->Product->find("first",array("conditions" =>array("Product.product_ndc" => $ndc)));
            
            $disposes = $ovproduct[0]["size"];
            $ovdata["other_vendor_quantity"] = 0;
			$ovdata['AddDate']=$disposes_date['ProductUsedSize']['created'];
            $ovdata["jdp_quantity"] = 0;
            $ovdata["Product NDC"] = $ndc;
            $ovdata["Item Number"] = $productDetails["Product"]["itemnumber"];
            $ovdata["Product Name"] = $productDetails["Product"]["productname"];
            $ovdata["Quantity"] = 0;
            $ovdata["Size"] = $productDetails["Product"]["product_size"];
            $ovdata['Total Size Purchase'] = 0;
            $ovdata['Total Dispense']=$disposes;					
            $ovdata['Total Left over']=-$disposes;
            array_push($newcsvdata,$ovdata);
			}
            
			}
            
        }
        
      /*  $totla_products = $this->Products->find("all", array('recursive' => 0,
            'conditions' => array("Products.active" => array('True', ''))));*/
			$totla_products = $this->Products->find("all", array('recursive' => 0));
        foreach ($totla_products as $tproduct) {
            $temauto["label"] = $tproduct['Products']['product_ndc'];
            $temauto["value"] = $tproduct['Products']['product_ndc'];
            $product_list_auto[] = $temauto;
			
			$temauto["label"] = $tproduct['Products']['productname'];
            $temauto["value"] = $tproduct['Products']['product_ndc'];
            $product_listid_auto[] = $temauto;
			
            $product_list[$tproduct['Products']['product_ndc']] = $tproduct['Products']['productname'];
        }
   
     	//   print_r($newcsvdata);
        $pndc=$this->request->query['pndc'];
        $this->set('pndc',$pndc);
        $this->set('users', $newcsvdata);
        $this->set('product_list_auto', $product_list_auto);
        $this->set('product_listid_auto', $product_listid_auto);
        $this->set('product_list', $product_list);
    }

    public function dispense_list($product_ndc, $disposes_id = NULL,$from_date=NULL,$to_date=NULL) {
        $this->layout = "index_demo";
        //Configure::write('debug',2);
        $cart_list = $this->Order->find('count', array('conditions' => array('Order.userid' => $this->Auth->user('id'), 'Order.status' => 'Fill')));
        $this->set('cart_list', $cart_list);
		$this->loadModel('Advertise');
		$Advertise = $this->Advertise->find('all', array('conditions' => array("OR"=>array('Advertise.user_ids like' => $this->Auth->user('id'),"Advertise.user_ids like '%,".$this->Auth->user('id')."%'","Advertise.user_ids like '%,".$this->Auth->user('id').",%'","Advertise.user_ids like '%".$this->Auth->user('id').",%'"), 'Advertise.status' => 1)));
		$this->set('Advertise', $Advertise);
        if ($product_ndc != '' and $disposes_id != NULL) {

            $this->ProductUsedSize->deleteAll(array('ProductUsedSize.id' => $disposes_id, 'ProductUsedSize.user_id' => $this->Auth->user('id')), false);
        }
		if($to_date==NULL and $from_date!=NULL){
			$to_date=$from_date;
			$from_date=$disposes_id;
			
		}
		if($from_date!=NULL and $to_date!=NULL){
			$disposes = $this->ProductUsedSize->find('all', array('conditions' => array("ProductUsedSize.date>='" . $from_date . "'", "ProductUsedSize.date<='" . $to_date . "'",'ProductUsedSize.user_id' => $this->Auth->user('id'), 'ProductUsedSize.product_ndc' => $product_ndc)));
			
		}else{
        $disposes = $this->ProductUsedSize->find('all', array('conditions' => array('ProductUsedSize.user_id' => $this->Auth->user('id'), 'ProductUsedSize.product_ndc' => $product_ndc)));
		}
        $Products = $this->Products->find('first', array('conditions' => array('Products.product_ndc' => $product_ndc)));
        //print_r($csvdata);
        $this->set('Products', $Products);
        
        $this->set('product_list', $disposes);
    }

    public function order_history() {
        //Configure::write('debug',2);
        $this->layout = "index_demo";
       // debug($this->Auth->user('id'));exit;
        $cart_list = $this->Order->find('count', array('conditions' => array('Order.userid' => $this->Auth->user('id'), 'Order.status' => 'Fill')));
        $this->set('cart_list', $cart_list);
		$this->loadModel('Advertise');
		$Advertise = $this->Advertise->find('all', array('conditions' => array("OR"=>array('Advertise.user_ids like' => $this->Auth->user('id'),"Advertise.user_ids like '%,".$this->Auth->user('id')."%'","Advertise.user_ids like '%,".$this->Auth->user('id').",%'","Advertise.user_ids like '%".$this->Auth->user('id').",%'"), 'Advertise.status' => 1)));
		$this->set('Advertise', $Advertise);


        if ($this->request->is('post')) {

            if ($this->request->data['OrderHistory']['selfrmdate'] != '' and $this->request->data['OrderHistory']['seltodate'] != '') {
                $this->paginate = array(
                    'conditions' => array('OrderDist.status' => array('Test', 'Accep'), "OrderDist.createddt>='" . $this->request->data['OrderHistory']['selfrmdate'] . "'", "OrderDist.createddt<='" . $this->request->data['OrderHistory']['seltodate'] . "'", 'OrderDist.userid' => $this->Auth->user('id')), 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.po_number'), 'order' => array('OrderDist.createddt' => 'desc')
                );
            } elseif ($this->request->data['OrderHistory']['selfrmdate'] != '' and $this->request->data['OrderHistory']['seltodate'] != '' and $this->request->data['OrderHistory']['searchtxt'] != '') {
                $this->paginate = array(
                    'conditions' => array('OrderDist.status' => array('Test', 'Accep'), "OrderDist.createddt>='" . $this->request->data['OrderHistory']['selfrmdate'] . "'", "OrderDist.tmporderid" => trim($this->request->data['OrderHistory']['searchtxt']) . "'", "OrderDist.createddt<='" . $this->request->data['OrderHistory']['seltodate'] . "'", 'OrderDist.userid' => $this->Auth->user('id')), 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.po_number'), 'order' => array('OrderDist.createddt' => 'desc')
                );
            } elseif ($this->request->data['OrderHistory']['searchtxt'] != '') {
                $this->paginate = array(
                    'conditions' => array('OrderDist.status' => array('Test', 'Accep'), "OrderDist.tmporderid" => trim($this->request->data['OrderHistory']['searchtxt']) . "'", 'OrderDist.userid' => $this->Auth->user('id')), 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.po_number'), 'order' => array('OrderDist.createddt' => 'desc')
                );
            }
            $products = $this->paginate("OrderDist");

            $cart_list = $this->Order->find('all', array('order' => array(
                    'Order.createddt' => 'desc'
                ), 'fields' => array('DISTINCT Order.id', 'Order.tmporderid', 'Order.userid', 'Order.createddt', 'Order.status', 'Order.po_number'), 'conditions' => array('Order.userid' => $this->Auth->user('id'), 'Order.status' => 'Con', "Order.createddt>='" . $this->request->data['OrderHistory']['selfrmdate'] . "'", "Order.createddt<='" . $this->request->data['OrderHistory']['seltodate'] . "'")));
            //print_r($cart_list);
            $this->set('pendingorder', $cart_list);
        } else {

            if ($this->params->query['unpaid'] != '') {
                $this->paginate = array(
                    'conditions' => array('OrderDist.status' => array('Test', 'Accep'), "OrderDist.Paid_Status" => "unpaid", 'OrderDist.userid' => $this->Auth->user('id')), 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.po_number'), 'order' => array('OrderDist.createddt' => 'desc')
                );
            } else {
                $this->paginate = array(
                    'conditions' => array('OrderDist.status' => array('Test', 'Accep'), 'OrderDist.userid' => $this->Auth->user('id')), 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.po_number'), 'order' => array('OrderDist.createddt' => 'desc')
                );
            }
            $products = $this->paginate("OrderDist");

            $cart_list = $this->Order->find('all', array('fields' => array('DISTINCT Order.id', 'Order.tmporderid', 'Order.userid', 'Order.createddt', 'Order.status', 'Order.po_number'), 'conditions' => array('Order.userid' => $this->Auth->user('id'), 'Order.status' => 'Con')));
            //print_r($cart_list);
            $this->set('pendingorder', $cart_list);
        }
        $this->set('users', $products);
    }

    public function dispense_list_delete($id, $ndc) {
        $this->loadModel('OtherVendor');
        $this->OtherVendor->deleteAll(array('OtherVendor.id' => $id, 'OtherVendor.user_id' => $this->Auth->user('id')), false);
        $this->Session->setFlash(__('Deleted Successfully.'), 'admin_success');
        $this->redirect($this->referer());
    }

    public function dispense_vendor_list($ndc) {
        $this->layout = "index";
        //Configure::write('debug',2);
        $cart_list = $this->Order->find('count', array('conditions' => array('Order.userid' => $this->Auth->user('id'), 'Order.status' => 'Fill')));
        $this->set('cart_list', $cart_list);

        $this->loadModel('OtherVendor');




        $products = $this->OtherVendor->find("all", array('conditions' => array('OtherVendor.user_id' => $this->Auth->user('id'), 'OtherVendor.product_ndc' => $ndc), 'order' => array('OtherVendor.created' => 'desc')));

        $csvdata = array();
        $temp_array = array();
        $product_list = array();
        $product_list_auto = array();
        $gkey = 0;
        foreach ($products as $key => $dt) {

            $priice = 0;
            $product_name = "";
            $product_ndc = '';
            $item_number = '';
            $productprice = '';
            $producttype = '';
            $lot_ids = '';


            $products_imt = $this->Products->find("first", array('recursive' => 0,
                'conditions' => array("Products.product_ndc" => trim($dt['OtherVendor']['product_ndc']))));


            $product_list[$product_ndc] = $product_name;
            $temp_array[$gkey] = $product_ndc;
            $temauto["label"] = $product_ndc;
            $temauto["value"] = $product_ndc;
            $product_list_auto[] = $temauto;
            $csvdata[$gkey]['Vendor Name'] = $dt['OtherVendor']['name'];
            $csvdata[$gkey]['Product NDC'] = $dt['OtherVendor']['product_ndc'];
            $csvdata[$gkey]['Item Number'] = $products_imt['Products']['itemnumber'];
            $csvdata[$gkey]['Product Name'] = $products_imt['Products']['productname'];
            $csvdata[$gkey]['Quantity'] = $dt['OtherVendor']['quantity'];
            $csvdata[$gkey]['Size'] = $products_imt['Products']['product_size'];
            $csvdata[$gkey]['created'] = $dt['OtherVendor']['created'];
            $csvdata[$gkey]['id'] = $dt['OtherVendor']['id'];
            $csvdata[$gkey]['Total Size Purchase'] = $products_imt['Products']['product_size'] * $dt['OtherVendor']['quantity'];
            //$csvdata[$gkey]['Total Dispense']=$disposes[0]['size'];					
            //$csvdata[$gkey]['Total Left over']=($product_size*$quantity)-$disposes[0]['size'];

            $gkey++;
        }
        //print_r($csvdata);
        $totla_products = $this->Products->find("all", array('recursive' => 0,
            'conditions' => array("Products.active" => array('True', ''))));
			$totla_products = $this->Products->find("all", array('recursive' => 0));
        foreach ($totla_products as $tproduct) {
            $temauto["label"] = $tproduct['Products']['product_ndc'];
            $temauto["value"] = $tproduct['Products']['product_ndc'];
            $product_list_auto[] = $temauto;
            $product_list[$tproduct['Products']['product_ndc']] = $tproduct['Products']['productname'];
        }
        $this->set('users', $csvdata);
        $this->set('product_list_auto', $product_list_auto);
        $this->set('product_list', $product_list);
    }
    /**
     * @category
     * @param 
     * @author Atul-Dev-2 Code
     */
    public function other_vendor_dispense_list($ndc=null){
        $this->layout = "index_demo";
		$this->loadModel('Advertise');
		$Advertise = $this->Advertise->find('all', array('conditions' => array("OR"=>array('Advertise.user_ids like' => $this->Auth->user('id'),"Advertise.user_ids like '%,".$this->Auth->user('id')."%'","Advertise.user_ids like '%,".$this->Auth->user('id').",%'","Advertise.user_ids like '%".$this->Auth->user('id').",%'"), 'Advertise.status' => 1)));
		$this->set('Advertise', $Advertise);
        //Configure::write("debug",2);
        if($ndc != null){
            $cart_list = $this->Order->find('count', array('conditions' => array('Order.userid' => $this->Auth->user('id'), 'Order.status' => 'Fill')));
        $this->set('cart_list', $cart_list);
            $this->loadModel("OtherVendor");
            $products = $this->OtherVendor->find("all", array('conditions' => array('OtherVendor.user_id' => $this->Auth->user('id'), 'OtherVendor.product_ndc' => $ndc), 'order' => array('OtherVendor.created' => 'desc')));

        $csvdata = array();
        $temp_array = array();
        $product_list = array();
        $product_list_auto = array();
        $gkey = 0;
        foreach ($products as $key => $dt) {

            $priice = 0;
            $product_name = "";
            $product_ndc = '';
            $item_number = '';
            $productprice = '';
            $producttype = '';
            $lot_ids = '';


            $products_imt = $this->Products->find("first", array('recursive' => 0,
                'conditions' => array("Products.product_ndc" => trim($dt['OtherVendor']['product_ndc']))));


            $product_list[$product_ndc] = $product_name;
            $temp_array[$gkey] = $product_ndc;
            $temauto["label"] = $product_ndc;
            $temauto["value"] = $product_ndc;
            $product_list_auto[] = $temauto;
            $csvdata[$gkey]['Vendor Name'] = $dt['OtherVendor']['name'];
            $csvdata[$gkey]['Product NDC'] = $dt['OtherVendor']['product_ndc'];
            $csvdata[$gkey]['Item Number'] = $products_imt['Products']['itemnumber'];
            $csvdata[$gkey]['Product Name'] = $products_imt['Products']['productname'];
            $csvdata[$gkey]['Quantity'] = $dt['OtherVendor']['quantity'];
            $csvdata[$gkey]['Size'] = $products_imt['Products']['product_size'];
            $csvdata[$gkey]['created'] = $dt['OtherVendor']['created'];
            $csvdata[$gkey]['id'] = $dt['OtherVendor']['id'];
            $csvdata[$gkey]['Total Size Purchase'] = $products_imt['Products']['product_size'] * $dt['OtherVendor']['quantity'];
            //$csvdata[$gkey]['Total Dispense']=$disposes[0]['size'];					
            //$csvdata[$gkey]['Total Left over']=($product_size*$quantity)-$disposes[0]['size'];

            $gkey++;
        }
        
        $this->set('users', $csvdata);
        }
    }
    /**
     * @category
     * @param 
     * Atul-Dev-2 Code 
     * end
     */
	 
	 public function update_other_verder(){
		// Configure::write('debug',2);
		 $this->loadModel('ProductUsedSize');
		 $othervnedor=$this->ProductUsedSize->find('all');
		 foreach($othervnedor as $vendor){
			 $product_id=$this->Products->find('first',array('recursive' => -1,'fields'=>array('Products.id'),'conditions'=>array('product_ndc'=>$vendor['ProductUsedSize']['product_ndc'])));
			 $this->ProductUsedSize->updateAll(
                            array('ProductUsedSize.productid' => $product_id['Products']['id']
                            ), array('ProductUsedSize.id' => $vendor['ProductUsedSize']['id'])
                    );
		 }
		 print_r($othervnedor);die;
	 }

    public function dispense_vendor() {
        $this->layout = "index";
        //Configure::write('debug',2);
        $cart_list = $this->Order->find('count', array('conditions' => array('Order.userid' => $this->Auth->user('id'), 'Order.status' => 'Fill')));
        $this->set('cart_list', $cart_list);

        $this->loadModel('OtherVendor');

        if ($this->request->is('post')) {
			$product_id=$this->Products->find('first',array('recursive' => -1,'fields'=>array('Products.id'),'conditions'=>array('product_ndc'=>$this->request->data['OrderHistory']['prodid'])));
            $data['OtherVendor']['product_ndc'] = $this->request->data['OrderHistory']['prodid'];
            $data['OtherVendor']['quantity'] = $this->request->data['OrderHistory']['product_dispense'];
            $data['OtherVendor']['name'] = $this->request->data['OrderHistory']['vendor_name'];
			$data['OtherVendor']['productid'] = $product_id['Products']['id'];
            $data['OtherVendor']['user_id'] = $this->Auth->user('id');
            $data['OtherVendor']['created'] = date("Y-m-d H:i:s");
            $this->OtherVendor->create();
            $this->OtherVendor->save($data);

            $this->request->data['OrderHistory']['prodid'] = '';
            $this->request->data['OrderHistory']['product_dispense'] = '';
            $this->request->data['OrderHistory']['vendor_name'] = '';
            $this->Session->setFlash(__('added Successfuly .'), 'admin_success');
        }


        /*$products = $this->OtherVendor->find("all", array('conditions' => array('OtherVendor.user_id' => $this->Auth->user('id')), 'order' => array('OtherVendor.created' => 'desc'), 'fields' => array('SUM(OtherVendor.quantity) as quantity', 'name', 'product_ndc', ''), 'group' => '`OtherVendor`.`product_ndc`'
        ));*/
		 $this->paginate = array('conditions' => array('OtherVendor.created >' => "2015-12-30",'OtherVendor.user_id' => $this->Auth->user('id')), 'order' => array(
                'OtherVendor.created' => 'desc'), 'fields' => array('SUM(OtherVendor.quantity) as quantity', 'name', 'product_ndc', ''),'group' => '`OtherVendor`.`product_ndc`');
        $products = $this->paginate("OtherVendor");

        $csvdata = array();
        $temp_array = array();
        $product_list = array();
        $product_list_auto = array();
        $gkey = 0;
        foreach ($products as $key => $dt) {

            $priice = 0;
            $product_name = "";
            $product_ndc = '';
            $item_number = '';
            $productprice = '';
            $producttype = '';
            $lot_ids = '';


            $products_imt = $this->Products->find("first", array('recursive' => 0,
                'conditions' => array("Products.product_ndc" => trim($dt['OtherVendor']['product_ndc']))));


            $product_list[$product_ndc] = $product_name;
            $temp_array[$gkey] = $product_ndc;
            $temauto["label"] = $product_ndc;
            $temauto["value"] = $product_ndc;
            $product_list_auto[] = $temauto;
            $csvdata[$gkey]['Vendor Name'] = $dt['OtherVendor']['name'];
            $csvdata[$gkey]['Product NDC'] = $dt['OtherVendor']['product_ndc'];
            $csvdata[$gkey]['Item Number'] = $products_imt['Products']['itemnumber'];
            $csvdata[$gkey]['Product Name'] = $products_imt['Products']['productname'];
            $csvdata[$gkey]['Quantity'] = $dt[0]['quantity'];
            $csvdata[$gkey]['Size'] = $products_imt['Products']['product_size'];

            $csvdata[$gkey]['Total Size Purchase'] = $products_imt['Products']['product_size'] * $dt[0]['quantity'];
            //$csvdata[$gkey]['Total Dispense']=$disposes[0]['size'];					
            //$csvdata[$gkey]['Total Left over']=($product_size*$quantity)-$disposes[0]['size'];

            $gkey++;
        }
        //print_r($csvdata);
       /* $totla_products = $this->Products->find("all", array('recursive' => 0,
            'conditions' => array("Products.active" => array('True', ''))));*/
			 $totla_products = $this->Products->find("all", array('recursive' => 0));
        foreach ($totla_products as $tproduct) {
            $temauto["label"] = $tproduct['Products']['product_ndc'];
            $temauto["value"] = $tproduct['Products']['product_ndc'];
            $product_list_auto[] = $temauto;
            $product_list[$tproduct['Products']['product_ndc']] = $tproduct['Products']['productname'];
        }
        $this->set('users', $csvdata);
        $this->set('product_list_auto', $product_list_auto);
        $this->set('product_list', $product_list);
    }

    public function client_order_history_excel($fromdate = NULL, $todate = NULL) {
        $this->layout = "admin_dashboard";


        if ($fromdate != NULL and $todate != NULL) {
            //$cart_list = $this->Order->find('all',array('fields' => array('DISTINCT Order.id','Order.tmporderid','Order.userid','Order.createddt','Order.status'),'conditions'=>array('Order.userid'=>$this->Auth->user('id'),"Order.createddt>='".$fromdate."'","Order.createddt<='".$todate."'")));				
            $cart_list = $this->OrderDist->find('all', array('recursive' => 2, 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.createddt'), 'conditions' => array('OrderDist.userid' => $this->Auth->user('id'), "OrderDist.createddt>='" . $fromdate . "'", "OrderDist.createddt<='" . $todate . "'")));
        } else {
            //$cart_list = $this->Order->find('all',array('fields' => array('DISTINCT Order.id','Order.tmporderid','Order.userid','Order.createddt','Order.status'),'conditions'=>array('Order.userid'=>$this->Auth->user('id'))));
            $cart_list = $this->OrderDist->find('all', array('recursive' => 2, 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.createddt'), 'conditions' => array('OrderDist.userid' => $this->Auth->user('id'))));
        }
        $csvdata = array();
        $gkey = 0;
        foreach ($cart_list as $key => $dt) {
            $product_qauantity = count($dt['OrderdetailMany']);
            $priice = 0;
            $product_name = "";
            $product_ndc = '';
            $item_number = '';
            $productprice = '';
            $producttype = '';
            $lot_ids = '';
            foreach ($dt['OrderdetailMany'] as $key2 => $orderdeatils) {
                $Orderdetail_lot = $this->OrderLot->find('all', array('recursive' => -1, 'fields' => array('id', 'product_id', 'quantity', 'lot_no', 'price', 'productinfoid', 'Orderdetailid', 'tmporderid', 'exchange_lot'), 'conditions' => array('OrderLot.Orderdetailid' => $orderdeatils['id'])));


                $priice = ($orderdeatils['price'] * $orderdeatils['quantity']);
                $product_name = $orderdeatils['Product']['productname'];
                $product_ndc = $orderdeatils['Product']['product_ndc'];
                $productprice = $orderdeatils['Product']['productprice'];
                $itemnumber = $orderdeatils['Product']['itemnumber'];
                $producttype = $orderdeatils['Product']['producttype'];

                foreach ($Orderdetail_lot as $lots) {
                    $lot_ids = ($lots['OrderLot']['exchange_lot'] != '') ? $lots['OrderLot']['exchange_lot'] : $lots['OrderLot']['lot_no'];
                    $csvdata[$gkey]['Order'] = $dt['OrderDist']['tmporderid'];
                    $csvdata[$gkey]['Quantity'] = $orderdeatils['quantity'];
                    $csvdata[$gkey]['Price'] = '$' . $priice;
                    $csvdata[$gkey]['Status'] = ($dt['OrderDist']['status'] == 'Accep') ? 'Accept' : $dt['OrderDist']['status'];
                    $csvdata[$gkey]['ORDER DATE'] = date("m-d-Y", strtotime($dt['OrderDist']['createddt']));
                    $csvdata[$gkey]['Product NDC'] = $product_ndc;
                    $csvdata[$gkey]['Item Number'] = $itemnumber;
                    $csvdata[$gkey]['Per Item Price'] = $productprice;
                    $csvdata[$gkey]['Product Type'] = $producttype;
                    $csvdata[$gkey]['Lot #'] = $lot_ids;
                    $csvdata[$gkey]['Product Name'] = $product_name;

                    $gkey++;
                }
            }
        }
        //get user details
        $userinfo = $this->User->find('first', array('conditions' => array('User.id' => $this->Auth->user('id'))));

        $this->Export->exportCsv($csvdata, '', $userinfo);
    }

    public function orderinfo($oid) {
        $this->layout = "index_demo";
		$this->loadModel('Advertise');
		$Advertise = $this->Advertise->find('all', array('conditions' => array("OR"=>array('Advertise.user_ids like' => $this->Auth->user('id'),"Advertise.user_ids like '%,".$this->Auth->user('id')."%'","Advertise.user_ids like '%,".$this->Auth->user('id').",%'","Advertise.user_ids like '%".$this->Auth->user('id').",%'"), 'Advertise.status' => 1)));
		$this->set('Advertise', $Advertise);
ini_set('memory_limit', '-1');
        //$this->set('title', __('Welcome Admin Panel'));
        //$this->set('description', __('Manage Users'));
        $cart_list = $this->Order->find('count', array('conditions' => array('Order.userid' => $this->Auth->user('id'), 'Order.status' => 'Fill')));
        $this->set('cart_list', $cart_list);
        $Orderdetail = $this->OrderDist->find('all', array('recursive' => 2, 'conditions' => array('OrderDist.id' => $oid)));

        foreach ($Orderdetail[0]['OrderdetailMany'] as $mkey => $ordersd) {
            $Orderdetail_lot = $this->OrderLot->find('all', array('recursive' => -1, 'fields' => array('id', 'product_id', 'quantity', 'lot_no', 'price', 'productinfoid', 'Orderdetailid', 'tmporderid', 'exchange_lot', 'exchange_lot_id'), 'conditions' => array('OrderLot.Orderdetailid' => $ordersd['id'])));

            foreach ($Orderdetail_lot as $key => $detail) {

                $productifo_id = ($detail['OrderLot']['exchange_lot_id'] != '') ? $detail['OrderLot']['exchange_lot_id'] : $detail['OrderLot']['productinfoid'];
                $prodetajl = $this->ProductInfo->find('first', array('recursive' => -1, 'conditions' => array('ProductInfo.id' => $productifo_id)));
                $Orderdetail_lot[$key]['product_details'] = $prodetajl;
            }
            $Orderdetail[0]['OrderdetailMany'][$mkey]['lot_details'] = $Orderdetail_lot;
        }
        //print_r($Orderdetail);die;	
        $this->set('Orderdetail', $Orderdetail);
        $this->set('Orderdetailid', $oid);

        //Update STOCK
        if ($this->request->is('put') || $this->request->is('post')) {
            //print_r($this->request->data);die;
            foreach ($this->request->data['User'] as $key => $lost) {
                if ($key != 'exchange') {
                    $lotid = trim(str_replace('lot', '', $key));
                    $productinfodetilas = $this->ProductInfo->find('first', array('fields' => array('batchno'), 'conditions' => array('ProductInfo.id' => $lost)));
                    //Configure::write('debug',2);
                    $this->OrderLot->updateAll(
                            array('OrderLot.exchange_lot' => "'" . $productinfodetilas['ProductInfo']['batchno'] . "'",
                        'OrderLot.exchange_lot_id' => $lost
                            ), array('OrderLot.id' => $lotid)
                    );
                }
            }

            $this->Session->setFlash(__('Lot Exchanged Successfuly .'), 'admin_success');
            $this->redirect(array('action' => 'admin_client_orderinfo/' . $oid));
        }
    }

    public function invoice($oid) {
        $this->layout = "ajax";

        $Orderdetail = $this->Order->find('first', array('recursive' => 2, 'conditions' => array('Order.id' => $oid)));
        //print_r($Orderdetail);		
        $this->set('Orderdetail', $Orderdetail);
    }

    public function invoice_view($oid) {
        $this->layout = "ajax";

        $Orderdetail = $this->Order->find('first', array('recursive' => 2, 'conditions' => array('Order.id' => $oid)));
        //print_r($Orderdetail);		
        $this->set('Orderdetail', $Orderdetail);
    }
	
	public function invoice_view_split($oid,$sno) {
        $this->layout = "ajax";

        $Orderdetail = $this->Order->find('first', array('recursive' => 2, 'conditions' => array('Order.id' => $oid)));
        //print_r($Orderdetail);		
        $this->set('Orderdetail', $Orderdetail);
		 $this->set('sno', $sno);
    }

    public function invoicepdf($id) {
        if (!$id) {
            $this->Session->setFlash('Sorry, there was no property ID submitted.');
            $this->redirect(array('action' => 'index'), null, true);
        }
        Configure::write('debug', 0); // Otherwise we cannot use this method while developing 

        $id = intval($id);

        $property = 'test'; // here the data is pulled from the database and set for the view 

        if (empty($property)) {
            $this->Session->setFlash('Sorry, there is no property with the submitted ID.');
            $this->redirect(array('action' => 'index'), null, true);
        }
        $this->set('id', $id);
        $this->layout = 'pdf'; //this will use the pdf.ctp layout 
        $this->render();
    }

    public function product_request() {
        $pid = explode('_', $pid);
        //Configure::write('debug',2);
        if ($this->request->is('post')) {

            $data = array('prodect_id' => $this->request->data['customers']['repro_id'],
                'user_id' => $this->Auth->user('id'),
                'quantity' => $this->request->data['customers']['requantity'],
                'created' => date("Y-m-d H:i:s")
            );
            $this->Productrequest->create();
            $this->Productrequest->save($data);
            //$this->Productrequest->query("insert into productrequests set prodect_id=".$pid[0]." ,user_id=".$this->Auth->user('id').",created='".date("Y-m-d H:i:s")."'");

            $this->Session->setFlash('Your Request has been submited', 'success');
            $this->redirect(array('action' => 'product_search?searchtxt=' . $this->request->data['customers']['reproduct_search']), null, true);
        } else {
            $this->Session->setFlash(__('No Product Found'), 'error', array('class' => 'alert-error'));
        }
        if ($pid[1] == 'Brand' or $pid[1] == 'Generic' or $pid[1] == 'OTC')
            $this->redirect(array('action' => 'product_types/' . $pid[1]), null, true);
        else
            $this->redirect(array('action' => 'product_search?searchtxt=' . $pid[1]), null, true);
    }

    public function forgot_password() {
        $this->layout = "index";

        if ($this->request->is('post')) {

            $user_active = $this->User->find('first', array('fields' => array('active', 'fname', 'email', 'id', 'username'), 'conditions' => array('User.username' => $this->request->data['customers']['username'], 'level' => 'client')));

            $username = $user_active['User']['username'];
            $email = $user_active['User']['email'];
            if (!empty($user_active)) {
                $tmppass = $this->randomPassword();
                /*                 * * Encript password before insert to DB  ** */
                $password = ($tmppass);



                $new_password = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$'), 0, 8);


                $this->User->id = $user_active['User']['id'];
                $this->User->saveField('password', $new_password);

                $message = <<<HTM
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
</HEAD>

<BODY>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>Hi User,<br>
      <br>
Password for $username has been reset.<br>
<br>
Heres all the information you need to get you started -<br>
<br>
------------------<br>
Login Information:<br>
------------------<br>
User ID: $username<br>
Password: $new_password</td>
  </tr>
</table>

</BODY>
HTM;

// subject
                $subject = 'Forgot Password';

// To send HTML mail, the Content-type header must be set
                $headers = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers .= 'From: jdpwholesaler.com <info@jdpwholesaler.com>' . "\r\n";
// Mail it
                mail($email, $subject, $message, $headers);


                $this->Session->setFlash(__('We have send your password to your email address.'), 'admin_success');
            } else {
                //$this->Session->setFlash(__('User not active'),'alert',array('class'=>'alert-error'));
                $this->Session->setFlash(__('User not found'), 'error', array('class' => 'alert-error'));
            }
        }
    }

    public function forgot_username() {
        $this->layout = "index";

        if ($this->request->is('post')) {

            $user_active = $this->User->find('first', array('fields' => array('active', 'fname', 'email', 'id', 'username'), 'conditions' => array('User.email' => $this->request->data['customers']['username'], 'level' => 'client')));



            if (!empty($user_active)) {


                $username = $user_active['User']['username'];
                $email = $user_active['User']['email'];
                $message = <<<HTM
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
</HEAD>

<BODY>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>Hi User,<br>
      <br>
Username for $email <br>
<br>
Heres all the information you need to get you started -<br>
<br>
------------------<br>
Login Information:<br>
------------------<br>
User ID: $username<br>
</td>
  </tr>
</table>

</BODY>
HTM;

// subject
                $subject = 'Forgot Username';

// To send HTML mail, the Content-type header must be set
                $headers = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers .= 'From: jdpwholesaler.com <info@jdpwholesaler.com>' . "\r\n";
// Mail it
                mail($email, $subject, $message, $headers);


                $this->Session->setFlash(__('We have send your password to your email address.'), 'admin_success');
            } else {
                //$this->Session->setFlash(__('User not active'),'alert',array('class'=>'alert-error'));
                $this->Session->setFlash(__('User not found'), 'error', array('class' => 'alert-error'));
            }
        }
    }

    public function randomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    function sendemial() {
        // The message
        $message = "Line 1\r\nLine 2\r\nLine 3";

// In case any of our lines are larger than 70 characters, we should use wordwrap()
        $message = wordwrap($message, 70, "\r\n");

// Send
        echo mail('hashmi8835@gmail.com', 'My Subject', $message);
        die;
    }

    public function admin_add_product_activity($data) {
        $this->ProductActivity->create();

        if ($this->ProductActivity->save($data)) {
            return true;
        }
    }

    function client_login_byadmin($CID) {
        $this->layout = "index";

        if ($this->request->is('GET') and $this->Session->read('Auth.Admin.level') == 'admin') {

            $user_active = $this->User->find('first', array('fields' => 'active', 'conditions' => array('User.id' => $CID, 'level' => 'client')));
            //print_r($user_active);die;
            // error_log("TEST: " . print_r($user_active, true));

            if (!empty($user_active)) {
                if ($user_active['User']['active']) {
                    $user = $this->User->findById($CID);
                    $user = $user['User'];
                    if ($this->Auth->login($user)) {
                        $this->Cookie->write('username', $this->Auth->user('username'), true, '+4 weeks');
                        if (@$this->request->data['User']['remember'] == 1) {
                            unset($this->request->data['User']['remember']);

                            $this->request->data['User']['password'] = $this->Auth->password($this->request->data['User']['password']);

                            $this->Cookie->write('remember_me_cookie', $this->request->data['User'], true, '2 weeks');
                        }
                        $this->redirect($this->Auth->redirect());
                    } else {
                        $this->Session->setFlash(__('Invalid username or password, try again'), 'error', array('class' => 'alert-error'));
                    }
                } else {
                    //$this->Session->setFlash(__('User not active'),'alert',array('class'=>'alert-error'));
                    $this->Session->setFlash(__('User not active'), 'error', array('class' => 'alert-error'));
                }
            } else {
                //	$this->Session->setFlash(__('User not found'),'alert',array('class'=>'alert-error'));
                $this->Session->setFlash(__('User not found'), 'error', array('class' => 'alert-error'));
                //$this->redirect('index');
            }
        } else {
            //die($this->Session->read('Auth.User.level'));
            if ($this->Auth->login()) {
                if ($this->Session->read('Auth.User.level') == 'client') {

                    $this->redirect(array('action' => 'index'));
                } else {
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                $this->redirect(array('action' => 'index'));
            }
        }
    }

    public function request_stock_announce() {

        $this->layout = "index";


        /* $this->paginate = array(
          'conditions' => array('Order.status' => 'Con'),
          'fields' => array('DISTINCT Order.id','Order.tmporderid','Order.userid','Order.createddt','Order.status')

          ); */

        $this->Productrequest->updateAll(
                array('Productrequest.status' => "'true'"), array('Productrequest.user_id' => $this->Auth->user('id'))
        );

        $this->paginate = array('conditions' => array('Productrequest.user_id' => $this->Auth->user('id')), 'order' => array(
                'Productrequest.created' => 'desc'));
        $users = $this->paginate("Productrequest");

        // print_r($users);

        $this->set('users', $users);
        $cart_list = $this->Order->find('count', array('conditions' => array('Order.userid' => $this->Auth->user('id'), 'Order.status' => 'Fill')));
        $this->set('cart_list', $cart_list);
    }

    public function request_stock_delete($id = NULL) {

        if (!$this->request->is('post')) {

            throw new MethodNotAllowedException();
        }

        $this->Productrequest->id = $id;

        if (!$this->Productrequest->exists()) {

            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->Productrequest->delete()) {



            $this->Session->setFlash(__('Product Request deleted'), 'admin_success');

            $this->redirect(array('action' => 'request_stock_announce'));
        }

        $this->Session->setFlash(__('Product Request was not deleted'), 'admin_error');

        $this->redirect(array('action' => 'request_stock_announce'));
    }

    public function pedigree() {
        $this->layout = "index_demo";
		$this->loadModel('Advertise');
		$Advertise = $this->Advertise->find('all', array('conditions' => array("OR"=>array('Advertise.user_ids like' => $this->Auth->user('id'),"Advertise.user_ids like '%,".$this->Auth->user('id')."%'","Advertise.user_ids like '%,".$this->Auth->user('id').",%'","Advertise.user_ids like '%".$this->Auth->user('id').",%'"), 'Advertise.status' => 1)));
		$this->set('Advertise', $Advertise);
        //Configure::write('debug',2);
        $cart_list = $this->Order->find('count', array('conditions' => array('Order.userid' => $this->Auth->user('id'), 'Order.status' => 'Fill')));
        $this->set('cart_list', $cart_list);
        $cart_list = $this->Order->find('count', array('conditions' => array('Order.userid' => $this->Auth->user('id'), 'Order.status' => 'Fill')));
        $this->set('cart_list', $cart_list);
        $uid = $this->Auth->user('id');

        if ($this->request->query['invoice'] != '') {
            $this->paginate = array(
                'conditions' => array('OrderDist.status' => array('Test', 'Accep'), "OrderDist.tmporderid" => $this->request->query['invoice'], 'OrderDist.userid' => $uid), 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.Paid_Status', 'OrderDist.po_number', 'OrderDist.batch_no')
            );
            $this->set('seltodate', $this->request->query['seltodate']);
            $this->set('selfrmdate', $this->request->query['selfrmdate']);
            $this->set('invoice', $this->request->query['invoice']);
        } elseif ($this->request->query['selfrmdate'] != '' and $this->request->query['seltodate'] != '') {
            $this->paginate = array(
                'conditions' => array('OrderDist.status' => array('Test', 'Accep'), "OrderDist.createddt>='" . $this->request->query['selfrmdate'] . "'", "OrderDist.createddt<='" . $this->request->query['seltodate'] . "'", 'OrderDist.userid' => $uid), 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.Paid_Status', 'OrderDist.po_number', 'OrderDist.batch_no')
            );
            $this->set('seltodate', $this->request->query['seltodate']);
            $this->set('selfrmdate', $this->request->query['selfrmdate']);
        } else {
            $this->paginate = array(
                'conditions' => array('OrderDist.status' => array('Test', 'Accep'), 'OrderDist.userid' => $uid), 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.Paid_Status', 'OrderDist.po_number', 'OrderDist.batch_no'),
                'order' => array(
                    'OrderDist.createddt' => 'desc'
                )
            );
        }
        $products = $this->paginate("OrderDist");


        //print_r($products);

        $cart_list = $this->Order->find('all', array('fields' => array('DISTINCT Order.id', 'Order.tmporderid', 'Order.userid', 'Order.createddt', 'Order.status', 'Order.Paid_Status', 'Order.po_number'), 'order' => array(
                'Order.createddt' => 'desc'
            ), 'conditions' => array('Order.userid' => $uid, 'Order.status' => 'Con')));
        //print_r($cart_list);
        $this->set('pendingorder', $cart_list);
        $this->set('users', $products);

        $user_info = $this->User->find('first', array('fields' => array('User.fname', 'User.id'), 'conditions' => array('User.id' => $uid)));
        $this->set('user_info', $user_info);





        /* $totalPaid = $this->Order->find('first',array('fields' => array('sum(Orderdetail.quantity*Orderdetail.price) as totalPaid','Order.userid', 'Order.id'),'order' => array(
          'Order.id' => 'desc'),'group' => array('Order.userid'),'conditions'=>array('Order.userid'=>$uid,'Order.Paid_Status'=>'paid','Order.status'=>array('Test','Accep'))));
          $totalUnpaid = $this->Order->find('first',array('fields' => array('sum(Orderdetail.quantity*Orderdetail.price) as totalUnpaid'),'order' => array(
          'Order.id' => 'desc'),'group' => array('Order.userid'),'conditions'=>array('Order.userid'=>$uid,'Order.Paid_Status'=>array('unpaid',''),'Order.status'=>array('Test','Accep')))); */


        $totalPaidv = 0;
        $totalPaid = $this->OrderDist->find('all', array('fields' => array('OrderDist.tmporderid'), 'order' => array(
                'OrderDist.id' => 'desc'), 'conditions' => array('OrderDist.userid' => $uid, 'OrderDist.status' => array('Test', 'Accep'), 'OrderDist.Paid_Status' => 'paid')));

        //calculate total unpad
        foreach ($totalPaid as $padamounts) {

            foreach ($padamounts['OrderdetailMany'] as $pdamount) {

                $totalPaidv = ($pdamount['quantity'] * $pdamount['price']) + $totalPaidv;
            }
        }

        $totalUnpaidv = 0;
        $totalUnpaid = $this->OrderDist->find('all', array('fields' => array('OrderDist.tmporderid'), 'order' => array(
                'OrderDist.id' => 'desc'), 'conditions' => array('OrderDist.userid' => $uid, 'OrderDist.status' => array('Test', 'Accep'), 'OrderDist.Paid_Status' => 'unpaid')));
        //calculate total unpad

        foreach ($totalUnpaid as $padamounts) {
            foreach ($padamounts['OrderdetailMany'] as $pdamount) {
                $totalUnpaidv = ($pdamount['quantity'] * $pdamount['price']) + $totalUnpaidv;
            }
        }
        $this->set('totalUnpaid', $totalUnpaidv);
        $this->set('totalPaid', $totalPaidv);
        $this->set('uid', $uid);
    }

    public function client_view_pedigree($pedgreeid, $tmporderid) {
        //Configure::write('debug',2);
        $this->layout = "index";
        $prodetajl = $this->Pedigree->find('first', array('recursive' => 2, 'conditions' => array('Pedigree.product_infos_id' => $pedgreeid)));
        $this->set("dhtml", $prodetajl);
        $Orderdetail = $this->Order->find('first', array('recursive' => 2, 'conditions' => array('Order.tmporderid' => $tmporderid)));
        $this->set("Orderdetail", $Orderdetail);
        $this->set("pedgreeid", $pedgreeid);
        $this->set("tmporderid", $tmporderid);
        //Lot informations
        $lotdetails = $this->OrderLot->find('all', array('fields' => array('id', 'product_id', 'quantity', 'lot_no', 'price', 'productinfoid', 'exchange_lot', 'exchange_lot_id', 'tmporderid'), 'conditions' => array('OR' => array('OrderLot.productinfoid' => $pedgreeid, 'OrderLot.exchange_lot_id' => $pedgreeid), 'OrderLot.tmporderid' => $tmporderid)));

        $lot_qunetiony = 0;
        foreach ($lotdetails as $lots) {
            $lot_qunetiony = $lot_qunetiony + $lots['OrderLot']['quantity'];
        }

        $this->set('lot_qunetiony', $lot_qunetiony);
        if ($prodetajl['Pedigree']['anda'])
            $this->view = 'client_view_pedigree_anda';
    }

    public function client_manual_pedigree($pedgreeid, $tmporderid) {

        $this->layout = "index_demo";
		$this->loadModel('Advertise');
		$Advertise = $this->Advertise->find('all', array('conditions' => array("OR"=>array('Advertise.user_ids like' => $this->Auth->user('id'),"Advertise.user_ids like '%,".$this->Auth->user('id')."%'","Advertise.user_ids like '%,".$this->Auth->user('id').",%'","Advertise.user_ids like '%".$this->Auth->user('id').",%'"), 'Advertise.status' => 1)));
		$this->set('Advertise', $Advertise);
        $cart_list = $this->Order->find('count', array('conditions' => array('Order.userid' => $this->Auth->user('id'), 'Order.status' => 'Fill')));
        $this->set('cart_list', $cart_list);
        $Orderdetail = $this->Order->find('first', array('recursive' => 2, 'conditions' => array('Order.tmporderid' => $tmporderid)));
        $this->set("Orderdetail", $Orderdetail);

        $prodetajl = $this->ProductInfo->find('first', array('conditions' => array('ProductInfo.id' => $pedgreeid)));

        $this->set("dhtml", $prodetajl);

        //Lot informations
        $lotdetails = $this->OrderLot->find('all', array('fields' => array('id', 'product_id', 'quantity', 'lot_no', 'price', 'productinfoid', 'exchange_lot', 'exchange_lot_id', 'tmporderid'), 'conditions' => array('OrderLot.productinfoid' => $pedgreeid, 'OrderLot.tmporderid' => $tmporderid)));
        //print_r($lotdetails);
        //count purchase quenstity for this log
        $lot_qunetiony = 0;
        foreach ($lotdetails as $lots) {
            $lot_qunetiony = $lot_qunetiony + $lots['OrderLot']['quantity'];
        }

        $this->set('lot_qunetiony', $lot_qunetiony);
        $this->set("lotdetails", $lotdetails);
    }

    public function order_statement() {

        $this->layout = "index_demo";
        $cart_list = $this->Order->find('count', array('conditions' => array('Order.userid' => $this->Auth->user('id'), 'Order.status' => 'Fill')));
        $this->set('cart_list', $cart_list);
		$this->loadModel('Advertise');
		$Advertise = $this->Advertise->find('all', array('conditions' => array("OR"=>array('Advertise.user_ids like' => $this->Auth->user('id'),"Advertise.user_ids like '%,".$this->Auth->user('id')."%'","Advertise.user_ids like '%,".$this->Auth->user('id').",%'","Advertise.user_ids like '%".$this->Auth->user('id').",%'"), 'Advertise.status' => 1)));
		$this->set('Advertise', $Advertise);
        $uid = $this->Auth->user('id');
       // Configure::write('debug', 2);

        $this->loadModel('OrderStatement');
        $staementlist = $this->OrderStatement->find("all", array('order' => array(
                'OrderStatement.created' => 'desc'),
            'conditions' => array('OrderStatement.user_id' => $uid)));
        $StatementList_t = array();
        $starray = array();
        foreach ($staementlist as $stmt) {
            $price = 0;
            if ($stmt['OrderStatement']['order_type'] == "order") {
                $Orderdetail = $this->Order->find('first', array('recursive' => 2, 'conditions' => array('Order.id' => $stmt['OrderStatement']['order_id'])));
                foreach ($Orderdetail['OrderdetailMany'] as $orderdeatils) {
                    $price = $price + ($orderdeatils['price'] * $orderdeatils['quantity']);
                }
            } else {
                $return_statmement = $this->CreditReturn->find("first", array('recursive' => 2, 'order' => array(
                        'CreditReturn.created' => 'desc'),
                    'conditions' => array("CreditReturn.id" => $stmt['OrderStatement']['order_id'], 'CreditReturn.user_id' => $uid)));
                $price = ($return_statmement['CreditReturn']['quantity'] * $return_statmement['CreditReturn']['product_price']) + $return_statmement['CreditReturn']['restocking_fees'];
                $price = -$price;
            }

            if (array_key_exists($stmt['OrderStatement']['statement'], $starray)) {
                $starray[$stmt['OrderStatement']['statement']] = $starray[$stmt['OrderStatement']['statement']] + $price;
            } else {
                $StatementList_t[] = $stmt;
                $starray[$stmt['OrderStatement']['statement']] = $price;
            }
        }


        $this->set('starray', $starray);

        $this->set('StatementList', $StatementList_t);
    }

    public function order_statement_invoice($oid) {
        //Configure::write('debug',2);
        $uid = $this->Auth->user('id');
        $this->loadModel('OrderStatement');
        $this->loadModel('CreditReturn');
        $this->layout = "ajax";
        $orderstatment = $this->OrderStatement->find("list", array('fields' => 'order_id', 'order' => array(
                'OrderStatement.created' => 'desc'),
            'conditions' => array('OrderStatement.user_id' => $uid, 'OrderStatement.order_type' => 'order', 'OrderStatement.statement' => $oid)));
        $Orderdetail = $this->Order->find('all', array('recursive' => 2, 'conditions' => array('Order.id' => $orderstatment)));
        $this->set('Orderdetail', $Orderdetail);

        $orderstatment_return = $this->OrderStatement->find("list", array('fields' => 'order_id', 'order' => array(
                'OrderStatement.created' => 'desc'),
            'conditions' => array('OrderStatement.user_id' => $uid, 'OrderStatement.order_type' => 'returnorder', 'OrderStatement.statement' => $oid)));
        $return_statmement = $this->CreditReturn->find("all", array('recursive' => 2, 'order' => array(
                'CreditReturn.created' => 'desc'),
            'conditions' => array("CreditReturn.id" => $orderstatment_return, 'CreditReturn.user_id' => $uid)));
        foreach ($return_statmement as $key => $rtcr) {

            $return_statmement[$key]['Prodcuts'] = $this->Products->find("first", array('recursive' => 0,
                'conditions' => array("Products.product_ndc like '%" . trim($rtcr['CreditReturn']['product_ndc'] . "'"))));
        }

        $this->set('return_statmement', $return_statmement);

        $orderstatment = $this->OrderStatement->find("first", array('conditions' => array('OrderStatement.user_id' => $uid, 'OrderStatement.statement' => $oid)));
        // print_r($return_statmement);
        $this->set('orderstatment', $orderstatment);
    }
	
	public function client_order_download_pdf_url($uid,$selfrmdate,$seltodate) {
        $this->layout = "";
        //Configure::write('debug',2);
		ini_set('MAX_EXECUTION_TIME', -1);
		ini_set('memory_limit', '-1');        

		
		if ($selfrmdate!= '' and $seltodate!= '') {
            
            
			$cart_list = $this->OrderDist->find('all', array('recursive' => 2, 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.createddt'), 'conditions' => array('OrderDist.userid' => $uid, "OrderDist.createddt>='" . $selfrmdate . "'", "OrderDist.createddt<='" . $seltodate . "'")));
			
			$csvdata = array();
        $gkey = 0;
        foreach ($cart_list as $key => $dt) {
            $product_qauantity = count($dt['OrderdetailMany']);
            $priice = 0;
            $product_name = "";
            $product_ndc = '';
            $item_number = '';
            $productprice = '';
            $producttype = '';
            $lot_ids = '';
            foreach ($dt['OrderdetailMany'] as $key2 => $orderdeatils) {
                $Orderdetail_lot = $this->OrderLot->find('all', array('recursive' => -1, 'fields' => array('id', 'product_id', 'quantity', 'lot_no', 'price', 'productinfoid', 'Orderdetailid', 'tmporderid', 'exchange_lot'), 'conditions' => array('OrderLot.Orderdetailid' => $orderdeatils['id'])));


                $priice = ($orderdeatils['price'] * $orderdeatils['quantity']);
                $product_name = $orderdeatils['Product']['productname'];
                $product_ndc = $orderdeatils['Product']['product_ndc'];
                $productprice = $orderdeatils['Product']['productprice'];
                $itemnumber = $orderdeatils['Product']['itemnumber'];
                $producttype = $orderdeatils['Product']['producttype'];

                foreach ($Orderdetail_lot as $lots) {
                    $lot_ids = ($lots['OrderLot']['exchange_lot'] != '') ? $lots['OrderLot']['exchange_lot'] : $lots['OrderLot']['lot_no'];
                    $csvdata[$gkey]['Order'] = $dt['OrderDist']['tmporderid'];
                    $csvdata[$gkey]['Quantity'] = $orderdeatils['quantity'];
					$csvdata[$gkey]['special_discount'] = $orderdeatils['special_discount'];
					$csvdata[$gkey]['original_price'] = $orderdeatils['original_price'];
                    $csvdata[$gkey]['Price'] = $orderdeatils['price'];
                   
                    $csvdata[$gkey]['ORDER DATE'] = date("m-d-Y", strtotime($dt['OrderDist']['createddt']));
                    $csvdata[$gkey]['Product NDC'] = $product_ndc;
                    $csvdata[$gkey]['Item Number'] = $itemnumber;
                    $csvdata[$gkey]['Per Item Price'] = $productprice;
                    $csvdata[$gkey]['Product Type'] = $producttype;
                    $csvdata[$gkey]['Lot #'] = $lot_ids;
                    $csvdata[$gkey]['Product Name'] = $product_name;

                    $gkey++;
                }
            }
        }
		
				
			
        }
        $user_info = $this->User->find('first', array('conditions' => array('User.id' => $uid)));
        $this->set('user_info', $user_info);

        $customer_fields = $this->CustomerField->find('first', array('fields' => array('CustomerField.batch_invoice', 'CustomerField.batch_invoice_status'), 'conditions' => array('CustomerField.user_id' => $uid, 'CustomerField.batch_invoice_status' => 'unpaid')));

        $this->set('customer_fields', $customer_fields);
		$this->set('uid', $uid);
		$this->set('csvdata',$csvdata);
		
		
    }

}

?>
