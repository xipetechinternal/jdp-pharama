<?php

App::uses('AppController', 'Controller');

/**
 * Category Controller
 *
 * @property Category $Category
 */
class CategoriesController extends AppController {

	/**
	 * Controller name
	 *
	 * @var string
	 * @access public
	 */
	 
		public $name = 'Categories';
		public $user = array('Category');



	/**
	 * beforeFilter
	 *
	 * @return void
	 * @access public
	 */
	 
		 public function beforeFilter() {
			parent::beforeFilter(); 		     
			$this->layout = "admin_dashboard";    
		}

	/**
	 * Admin index
	 *
	 * @return void
	 * @access public
	 */
	 
		 public function admin_index() {
				$this->set('title', __('Categories')); 
				$this->paginate = array('order'=>array('Category.lft' => 'ASC'));
				$this->Category->recursive = 0;
				$this->set('category_tree', $this->Category->generateTreeList());
				$this->set('categories', $this->paginate());
		 }
 
	 /**
	 * view method
	 *
	 * @param string $id
	 * @return void
	 */
	 
		public function admin_view($id = null) {		
			
			$this->layout = "ajax";		
			$this->Category->id = $id;		
			if (!$this->Category->exists()) {
				throw new NotFoundException(__('Invalid post'));
			}
			
			$this->set('categories', $this->Category->read(null, $id));
		}

 
 
	 /**
	 * add method
	 *
	 * @return void
	 */
		public function admin_add() {		
			$this->set('categories', $this->Category->generateTreeList());
			if ($this->request->is('post')) {
				$this->Category->Behaviors->attach('Tree');	
				$this->Category->create();			
				$this->request->data["Category"]["status"] = 1;
				if ($this->Category->save($this->request->data)) {
					$this->Session->setFlash(__('The Category has been saved'), 'success');
					$this->redirect(array('action' => 'admin_index'));
				} else {
					$this->Session->setFlash(__('The Category could not be saved. Please, try again.'), 'admin_error');
				}
			}
		}
	



	/**
	 * edit method
	 *
	 * @param string $id
	 * @return void
	 */
		public function admin_edit($id = null) {
			$this->Category->id = $id;
			if (!$this->Category->exists()) {
				throw new NotFoundException(__('Invalid post'));
			}
			$this->set('categories', $this->Category->generateTreeList());
			if ($this->request->is('post') || $this->request->is('put')) {
				$this->Category->Behaviors->attach('Tree');
				if ($this->Category->save($this->request->data)) {
					$this->Session->setFlash(__('The post has been saved'), 'admin_success');
					$this->redirect(array('action' => 'admin_index'));
				} else {
					$this->Session->setFlash(__('The post could not be saved. Please, try again.'), 'admin_error');
				}
			} else {
				$this->request->data = $this->Category->read(null, $id);
			}
		}



	 /**
	 * edit method
	 *
	 * @param string $id
	 * @return void
	 */
		public function admin_moveup($id = null, $delta = null) {
			
			$this->Category->id = $id;
			if (!$this->Category->exists()) {
				throw new NotFoundException(__('Invalid category'), 'admin_error');
			}
			
			if ($delta > 0) {
				$this->Category->moveUp($this->Category->id, abs($delta));
				$this->Session->setFlash(__('Category should'.' be moved up.'), 'admin_success');
			}else {
			$this->Session->setFlash(__('Please provide a number of positions the category should '.'be moved up.'), 'admin_error');
			}
			
			return $this->redirect(array('action' => 'admin_index'));
		}
	 
	 /**
	 * edit method
	 *
	 * @param string $id
	 * @return void
	 */
		public function admin_movedown($id = null, $delta = null) {
			
			$this->Category->id = $id;
						
			if (!$this->Category->exists()) {
				throw new NotFoundException(__('Invalid category'), 'admin_error');
			}
			
			if ($delta > 0) {
				$this->Category->moveDown($this->Category->id, abs($delta));
				$this->Session->setFlash(__('Category should'.' be moved down.'), 'admin_success');
			} else {
				$this->Session->setFlash(__('Please provide the number of positions the field should be '.'moved down.'), 'admin_error');
			}
			
			return $this->redirect(array('action' => 'admin_index'));
		}
	 
	 
	
	 /**
	 * edit method
	 *
	 * @param string $id
	 * @return void
	 */	
		public function admin_delete($id = null) {
			if (!$this->request->is('post')) {
				throw new MethodNotAllowedException();
			}
			$this->Category->id = $id;
			if (!$this->Category->exists()) {
				throw new NotFoundException(__('Invalid user'));
			}
			if ($this->Category->delete()) {
				$this->Session->setFlash(__('Category deleted'), 'admin_success');
				$this->redirect(array('action' => 'index'));
			}
			$this->Session->setFlash(__('Category was not deleted'), 'admin_error');
			$this->redirect(array('action' => 'index'));
		}

    /**
     *  Active/Inactive Category
     *
     * @param <int> $user_id
     */
		public function toggle($cate_id, $status) {
			$this->layout = "ajax";
			$status = ($status) ? 0 : 1;
			$this->set(compact('cate_id', 'status'));
			if ($cate_id) {
				$data['Category'] = array('id'=>$cate_id, 'status'=>$status);
				$allowed = $this->Category->saveAll($data["Category"], array('validate'=>false));           
			} 
		}	
	
}
