 

<div class="col-lg-12  row">
<div class="col-lg-6">
<h4><a href="<?php echo $this->Html->url('/admin/users');?>">Home</a> :: Download PDF Order  For Customer <?php echo $user_info['User']['fname']?></h4>   
</div>
<div class="col-lg-6"><a href="javascript:;" onclick="history.go(-1);" class="btn btn-primary btn-mini btn-left-margin">Back</a>
</div>
</div>


<div class="col-lg-12 text-center">
        <div class="panel panel-primary">
        
        
        <div class="col-lg-12"> 
  <?php echo $this->Session->flash();  ?>
    
    <?php echo $this->Form->create('OrderHistory',array('type'=>'GET','url'=>array('action'=>'admin_client_order_download_pdf/'.$uid,'controller'=>'users'),'class'=>'paraform')); ?>
   <?php echo $this->Form->input('orderid',array('placeholder' => 'From Date',
        'label' => false,
		'type'=>'hidden',
		'required'=>'required',
        'class'=>'form-control'));?> 
     
      <div class="form-group">
        <div class="col-lg-2">
          <label for="exampleInputEmail1">Select Date:</label>
        </div>
        <div class="col-lg-8">
          <div class="form-group">
            <div class="col-lg-4">
            <?php echo $this->Form->input('selfrmdate',array('placeholder' => 'From Date',
        'label' => false,
		'required'=>'required',
		'value'=>$selfrmdate,
        'class'=>'form-control'));?> 
             
            </div>
            <div class="col-lg-4">
            <?php echo $this->Form->input('seltodate',array('placeholder' => 'To Date',
        'label' => false,
		'value'=>$seltodate,
		'required'=>'required', 
        'class'=>'form-control'));?>
		<?php echo $this->Form->input('uid',array('placeholder' => 'To Date',
        'label' => false,
		'value'=>$uid,
		'type'=>'hidden',
        'class'=>'form-control'));?>
              
            </div>
            <div class="col-lg-4">
            <?php echo $this->Form->button('<i class="fa fa-search"></i> ', array(
    'type' => 'submit',
    'class' => 'btn btn-default',
    'escape' => false
));?>
              
            </div>
            
            
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
      
           
        <div class="clearfix"></div>
           <?php echo $this->Form->end();?>
      <div class="clearfix"></div>
      <div class="gap20"></div>
    
    
    
    
    
  </div>
        
          
          
           
            
          </div>
        </div>
      </div>

 
 

<!-- /.modal -->

<script>
$(document).ready(function(){
	$("#ProductInfoAdminSalesReportPersonForm").validate();
	//$( "#ProductInfoSelfrmdate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
   // $( "#ProductInfoSeltodate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
	
	
    $( "#OrderHistorySelfrmdate, #OrderHistorySeltodate" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
       dateFormat: 'yy-mm-dd' ,
        onSelect: function( selectedDate ) {
            if(this.id == 'OrderHistorySelfrmdate'){
              var dateMin = $('#OrderHistorySelfrmdate').datepicker("getDate");
              var rMin = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 1); // Min Date = Selected + 1d
              var rMax = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 365); // Max Date = Selected + 31d
              $('#OrderHistorySeltodate').datepicker("option","minDate",rMin);
              $('#OrderHistorySeltodate').datepicker("option","maxDate",rMax);                    
            }

        }
    });

});
</script>

<script>
$(function(){

   $('.push').click(function(){
      var essay_id = $(this).attr('id');  
	  $("#OrderHistoryPoNumber").val($(this).data('ponumber')); 
	  $("#OrderHistoryOrderId").val(essay_id);  
	   $('.bs-example-modal-sm').modal('show');

});

   });



</script>