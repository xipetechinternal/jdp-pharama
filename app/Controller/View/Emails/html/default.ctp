<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Emails.html
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>

<?php
/*echo sprintf(
"%s: %s<br/>
%s: %s<br/>

%s:%s<br/>
----------------------------
%s %s",
    __d('default', 'name'), Sanitize::clean($data['Name']),
    __d('default', 'email'), Sanitize::clean($data['Mail']),

    __d('default', 'message'),
    Sanitize::stripAll($data['Message']),

    __d('default', 'sent from'), Router::url('/', true)
);*/

echo $data['Message'];

?>