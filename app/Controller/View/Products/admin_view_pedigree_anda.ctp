<?php echo $this->Html->script(array('jQuery.print'));?>
<div class="col-lg-12" >
<h4><a href="<?php echo $this->Html->url('/admin/users');?>">Home</a> :: TI,TH,TS</h4>
      <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">TI,TH,TS</h3>
      </div>
      
      
      <?php //print_r($dhtml);?>
      <?PHP $pedigree=$dhtml['Pedigree']['INVOICE_T1'];
	  $pedigree=json_decode($pedigree);
	  //print_r($pedigree);
	  ?>
      
    </div>
<div class="col-lg-12" id="print_div">
	 <h3 class="text-left">TI</h3>
    <table  style="width:100%" border="1" cellpadding="0" cellspacing="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                     <th align="center"><?php echo __('Ndc');?></th>
                       <th align="center"><?php echo __('Item #');?></th>
                        <th align="center"><?php echo __('dis');?></th>
                        <th align="center"><?php echo __('lot');?></th>
                        <th align="center"><?php echo __('Exp');?></th>
                        <th align="center"><?php echo __('Rec date');?></th> 
                       <!--<th align="center"><?php echo __('Quantity');?></th>--> 
                        <th align="center"><?php echo __('Manufacture');?></th> 
                    </tr>
                </thead>                
                <tbody>
               
						<tr> 
						 <td align="center"><?php echo $dhtml['ProductInfo']['Product']['product_ndc'];?></td>
                         <td align="center"><?php echo $dhtml['ProductInfo']['Product']['itemnumber'];?></td>               
						 <td align="center"><?php echo $dhtml['ProductInfo']['Product']['productname'];?></td>
						 <td align="center"><?php echo $dhtml['ProductInfo']['batchno'];?></td>              
						 <td align="center"><?php echo date('m-d-Y',strtotime($dhtml['ProductInfo']['expdate']));?></td>
						 <td align="center"><?php echo date('m-d-Y',strtotime($dhtml['ProductInfo']['ReceivedDate']));?></td>
                         <!--<td align="center"><?php echo $dhtml['ProductInfo']['availability'];?></td> --> 
                         <td align="center"><?php echo $dhtml['ProductInfo']['Product']['manufacture'];?></td>
						
						</tr>
                                  
                </tbody>               
            </table>
  
  <div class="gap20"></div>
  
    <h3 class="text-left">TH</h3>
  				<!--<table  style="width:100%" border="1" cellpadding="0" cellspacing="0" class="table table-striped table-bordered" id="example">
                             
                <tbody>
               
						<tr> 					           
						 <td align="center"> <div class="form-group">
             <?php echo $this->Form->input('itemnumber',array('placeholder' => 'VendorName',
        'label' => 'Vendor Name',
		'readonly'=>'readonly',
		'value'=>$dhtml['ProductInfo']['VendorName'],
        'class'=>'form-control'));?> 
            </div></td>
						 <td align="center"> <?php echo $this->Form->input('itemnumber',array('placeholder' => 'Purchase date',
        'label' => 'Purchase Date',
		'readonly'=>'readonly',
		'value'=>date('m-d-Y',strtotime($dhtml['ProductInfo']['createddt'])),
        'class'=>'form-control'));?> </td>
						
						</tr>
                <tr> 					           
						 <td align="center"> <div class="form-group">
             <?php echo $this->Form->input('itemnumber',array('placeholder' => 'company name ',
        'label' => 'Company Name ',
		'readonly'=>'readonly',
		'value'=>'Pharmaceutical Inc.',
        'class'=>'form-control'));echo " Invoice# ".$dhtml["ProductInfo"]["InvoiceNo"]?> 
            </div></td>
						 <td align="center"> <?php echo $this->Form->input('itemnumber',array('placeholder' => 'Receive date ',
        'label' => 'Receive date',
		'readonly'=>'readonly',
		'value'=>date('m-d-Y',strtotime($dhtml['ProductInfo']['ReceivedDate'])),
        'class'=>'form-control'));?> </td>
						
						</tr>                  
                </tbody>               
            </table>-->
            <table class="table table-striped table-bordered">
            
            <tr valign="middle"><td style="vertical-align:middle" valign="middle" align="center" rowspan="4"><strong>SOLD TO</strong></td><td><strong>Customer Name - </strong> <?php echo $pedigree->shippedPedigree->transactionInfo->recipientInfo->businessAddress->businessName;?></td></tr>
                <tr><td><strong>Received ON - </strong> <?php echo date('m-d-Y',strtotime($pedigree->shippedPedigree->transactionInfo->transactionDate))?></td></tr>  
                <tr><td><strong>LicenseNumber - </strong> <?php echo $pedigree->shippedPedigree->transactionInfo->recipientInfo->licenseNumber;?></td></tr>
                 <tr><td><strong>InvoiceNumber - </strong> <?php echo $pedigree->shippedPedigree->transactionInfo->transactionIdentifier->identifier;?></td></tr>  
<tr><td colspan="2"></td></tr>
            
                <tr valign="middle"><td style="vertical-align:middle" valign="middle" align="center" rowspan="4"><strong>SOLD TO</strong></td><td><strong>Customer Name - </strong> <?php echo $pedigree->shippedPedigree->pedigree->receivedPedigree->initialPedigree->transactionInfo->recipientInfo->businessAddress->businessName;?></td></tr>
                <tr><td><strong>Received ON - </strong> <?php echo date('m-d-Y',strtotime($pedigree->shippedPedigree->pedigree->receivedPedigree->initialPedigree->receivingInfo->dateReceived))?></td></tr>  
                <tr><td><strong>QTY Sold - </strong> <?php echo $pedigree->shippedPedigree->pedigree->receivedPedigree->receivingInfo->itemInfo->quantity;?></td></tr>
                 <tr><td><strong>Transaction Identifier - </strong> <?php echo $pedigree->shippedPedigree->pedigree->receivedPedigree->initialPedigree->transactionInfo->transactionIdentifier->identifier;?></td></tr>  
<tr><td colspan="2"></td></tr>
			<tr valign="middle"><td style="vertical-align:middle" valign="middle" align="center" rowspan="4"><strong>START HERE </strong></td><td><strong>Manufacturer - </strong> <?php echo $pedigree->shippedPedigree->pedigree->receivedPedigree->initialPedigree->productInfo->manufacturer;?></td></tr>
                <tr><td><strong>Lot No - </strong> <?php echo $pedigree->shippedPedigree->pedigree->receivedPedigree->initialPedigree->itemInfo->lot;?></td></tr>  
                <tr><td><strong>Container Size - </strong> <?php echo $pedigree->shippedPedigree->pedigree->receivedPedigree->initialPedigree->productInfo->containerSize;?><!--START JD--><?php /*echo $dhtml['ProductInfo']['id']*/?></td></tr>
                 <tr><td><strong>DosageForm - </strong> <?php echo $pedigree->shippedPedigree->pedigree->receivedPedigree->initialPedigree->productInfo->dosageForm;?> <?php /* echo date('m-d-Y',strtotime($dhtml['ProductInfo']['createddt']))*/?></td></tr>  
  
             </table>
  
 <br>
    <h3 class="text-left">TS</h3>
    
    <strong>Jd pharmaceutical wholesaler inc. certifies that it</strong>
    <br>
    <br>
    <ol>
    	<li>is authorized under the Drug Quality and Security Act (the "Act") to sell the products identified in this Transaction History to you</li>
    <li>received these products from a person or entity authorized under the Act</li>
    <li>received a Transaction Statement from the prior owner of these products</li>    
    <li> did not knowingly ship a suspect or illegitimate produc</li>    
    <li> had systems in place to comply with the verification requirements of the Act</li>    
    <li>did not knowingly provide false Transaction Information </li>
    <li>did not knowingly alter the Transaction History</li>
   </ol>
     <p></p>
</div>
    <div class="row">
  <div class="col-xs-9">
  <button class="btn btn-primary btn-mini tooltip-top" onclick="history.go(-1);">Back </button>
  </div>
  <div class="col-xs-3">
  
          <button type="button" class="btn btn-success print" data-dismiss="modal">Print</button> <br /><br />
       
   </div>
  </div>
  
</div>
 <script>
	$('.print').click(function(){
		$('.print').hide();
     $("#print_div").print();
	 $('.print').show();
});</script>  
<style>
/* centered columns styles */
.row-centered {
    text-align:center;
}
.col-centered {
    display:inline-block;
    float:none;
    /* reset the text-align */
    text-align:left;
    /* inline-block space fix */
    margin-right:-4px;
}
table {
max-width: 100%;
background-color: transparent;
border-collapse: collapse;
border-spacing: 0;
border:1px;
}
th {
text-align: center;
font-weight:bold;
}
h5, .h5 {
font-weight:bold;
}
</style>