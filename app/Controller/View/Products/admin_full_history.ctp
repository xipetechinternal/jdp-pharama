<?php echo $this->Html->script(array('jQuery.print'));?>
<div class="col-lg-12">
<h4><a href="<?php echo $this->Html->url('/admin/users');?>">Home</a> :: Product Full History</h4>
 <div class="row">
  <div class="col-xs-9">
  
  </div>
  <div class="col-xs-3">
  
        <button class="btn btn-primary btn-mini tooltip-top" onclick="history.go(-1);">Back </button> <br /><br />
       
   </div>
  </div>     

<button class="btn btn-info" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
  <?php echo $product['Products']['productname'];?>
</button>
<div class="gap20"></div>

<div class="collapse" id="collapseExample">
  <div class="well">
  <?php //loop for product info
  $create=false;
  foreach($product_activies as $product_activie):
  	if($product_activie['ProductActivity']['action']=='Created')
	$create=true;
  	switch($product_activie['ProductActivity']['action']){
    	case "Product Added To Cart":?>
        	<div class="alert alert-info">
            <strong><?php echo $product_activie['ProductActivity']['action']?></strong> BY <?php echo $product_activie['User']['fname']?> on Date <?php echo date("m-d-Y h:i A",strtotime($product_activie['ProductActivity']['created']))?>     <?php echo $this->Html->link($this->Html->tag('i','View  Order',array('class'=>'icon-pencil icon-white')),array('controller'=>'products','action' => 'orderinfo', $product_activie['ProductActivity']['event_id']),array("target"=>"_blank","class"=>"btn btn-success btn-mini tooltip-top","data-original-title"=>"Full History",'escape'=>false)); ?>
        </div>
		<?php
		 break;
        default:
        ?>
        <div class="alert alert-info">
            <strong>Product <?php echo $product_activie['ProductActivity']['action']?></strong> BY <?php echo $product_activie['User']['fname']?> on Date <?php echo date("m-d-Y h:i A",strtotime($product_activie['ProductActivity']['created']))?> 
        </div>
  <?php } endforeach;?>
  <?php //add if product exported 
  	if(!$create):?>
    <div class="alert alert-success">
            <strong>Product Imported from excel </strong> BY Admin on Date <?php echo date("m-d-Y h:i A",strtotime($product_activie['Product']['createddt']))?> 
        </div>
   <?php endif;?>
  </div>
</div>
<br />
 <div class="gap20"></div>
 
 <!--Lot Looping-->
 <h3><b>Lot History</b></h3>
 <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
 <?php if(!empty($product['ProductInfo'])){
	 foreach($product['ProductInfo'] as $key=>$lot){
	 ?>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne<?php echo $key?>">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?php echo $key?>" aria-expanded="true" aria-controls="collapseOne<?php echo $key?>">
          <?php echo $lot['batchno']; ?>
        </a>
      </h4>
    </div>
    <div id="collapseOne<?php echo $key?>" class="panel-collapse collapse <?php if($key==0){?>in<?php }?>" role="tabpanel" aria-labelledby="headingOne<?php echo $key?>">
      <div class="panel-body">
        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
      </div>
    </div>
  </div>
 <?php } }else{
	 echo "NO Lot";
 }?>
  
</div>


</div>
  
