<div class="col-lg-12">
<h4><a href="<?php echo $this->Html->url('/admin/users');?>">Home</a> :: Threshold Product Report</h4>
      


</div>
<div class="col-lg-12 text-right" style="padding:10px"><a class="btn btn-primary btn-mini btn-left-margin" href="<?php echo $this->Html->url(array_merge(array('controller'=>'products','action'=>'threshold_productcsv'),$this->params['named'],array(''))).'/Reporte-'.date('m-d-Y-His-A').'.csv'; ?>"><i class="icon-file-text"></i> Download Excel/CSV</a></div>
<div class="col-lg-12  ">
 <?php echo $this->Form->create('ProductInfo',array('url'=>array('action'=>'admin_threshold_product','controller'=>'products'),'type'=>'get', 'class'=>'form-horizontal login-from')); ?>
  <div class="form-group">
  <div class="col-lg-2"><label for="exampleInputEmail1">Search By:</label></div>
  <div class="col-lg-8"> <div class="form-group">
  <div class="col-lg-4">   <?php echo $this->Form->input('ndcproduct',array('placeholder' => 'Itemnumber #/Product NDC/Product Details',
        'label' => false,
		'title'=>'Itemnumber no/Product NDC/Product Details',
		'value'=>$search,
		'required'=>'required',
        'class'=>'form-control'));?> 
   
  </div>
  <div class="col-lg-4"> <?php echo $this->Form->button('<i class="fa fa-search"></i> Search', array(
    'type' => 'submit',
    'class' => 'btn btn-primary',
    'escape' => false
));?></div>
  <div class="clearfix"></div>
  
  
            </div></div>

  </div>
  <?php echo $this->Form->end();?>
  <div class="clearfix"></div>
  <div class="gap20"></div>
</div>
<div class="col-lg-12 text-center">

      <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Threshold Product List</h3>
      </div>
      <div class="panel-body">
       <div class="table-responsive">
             

            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                        
                         <th><?php echo __('Product NDC');?></th>
                          <th><?php echo __('Item Number');?></th>
                        <th><?php echo __('Product Name');?></th>
                        <th><?php echo __('Vendor Name');?></th> 
                        <th><?php echo __('Quantity');?></th> 
                       <th><?php echo __('Thershole');?></th> 
                    </tr>
                </thead>
                
                <tbody>
                
				<?php 
				$i=0;
				
				foreach ($users as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				?>
                <tr <?php echo $class;?> > 
                        
               
               
                <td><?php echo h($user['products']['product_ndc']);  ?></td>  
                <td><?php echo h($user['products']['itemnumber']); ?></td>    
                <td><?php echo h($user['products']['productname']); ?> &nbsp;</td>
                <td><?php echo h($user[0]['VendorName']); ?>&nbsp;</td>
               
                <td><?php echo h($user[0]['qnty']); ?><?php if ($user[0]['qnty'] <= 0) { ?>
					<span class="redtxt">Out of Stock</span>
					<?php
					}
					else
					{
					?>
					<span class="greentxt">In Stock</span>
					<?php
					}
					?> </td>                
               <td><?php echo $user['products']['qtyinstock'] ?>
                    &nbsp;</td>
                </tr>
                <?php endforeach; ?>                
                </tbody>
            </table>
            <div class="pagination pagination-right pagination-mini">
	  <ul>
		<?php echo $this->Paginator->prev(''); ?>
		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
		<?php echo $this->Paginator->next(''); ?>
	  </ul>
	</div>
       </div>
      </div>
    </div>


</div>

 
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
      </div>
      </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

