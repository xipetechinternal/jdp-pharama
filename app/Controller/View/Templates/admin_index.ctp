<?php $this->set('title_for_layout', __('Email Templates',true)); ?>
<?php $this->Html->addCrumb($this->Html->tag('li',__('Email Templates',true),array('class'=>'active')),false,false); ?>

<div class="row-fluid">
<!-- block -->
<div class="block">
    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left"><?php echo __('Email Templates');?></div>
        <div class="pull-right"><?php echo $this->Html->link($this->Html->tag('span',$this->Html->tag('i',false,array('class'=>'icon-plus icon-white')).__(' ADD NEW'),array('class'=>'badge badge-info')),array('action' => 'admin_add'),array("class"=>false,"data-original-title"=>"ADD NEW",'escape'=>false)); ?>
       </div>
    </div>
    <div class="block-content collapse in">
        <div class="span12">
        
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                        <th><?php echo __('Title');?></th>
                        <th><?php echo __('Subject');?></th> 
                        <th><?php echo __('Created');?></th>
                        <th><?php echo __('Actions');?></th>
                    </tr>
                </thead>
                
                <tbody>
                
				<?php 
				$i=0;
				foreach ($templates as $template): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				?>
                <tr <?php echo $class;?> >               
                <td><?php echo h($template['EmailTemplate']['title']); ?>&nbsp;</td>
                <td><?php echo h($template['EmailTemplateDescription']['subject']); ?>&nbsp;</td>
                <td class="center"><?php echo h(date("j M Y , g:i A",strtotime($template['EmailTemplate']['created']))); ?>&nbsp; </td>
                <td class="center">
             
                
				<?php echo $this->Html->link($this->Html->tag('i',false,array('class'=>'icon-eye-open')),'#myModal',array("class"=>"btn btn-mini tooltip-top","data-original-title"=>"View","data-toggle"=>"modal",'escape'=>false,'onclick' => "view.toggle('".$this->Html->url('/admin/templates/view/').$template['EmailTemplateDescription']['id']."');")); ?>
                
                <?php 
				if($template['EmailTemplateDescription']['status'])
				echo $this->Html->link($this->Html->tag('i',false,array('class'=>'icon-ok icon-white')),'#',array('class'=>'btn btn-inverse btn-mini tooltip-top','data-original-title'=>'Publish','escape'=>false,'onclick' => "published.toggle('status-".$template['EmailTemplateDescription']['id']."','".$this->Html->url('/templates/toggle/').$template['EmailTemplateDescription']['id']."/".intval($template['EmailTemplateDescription']['status'])."');",'id' =>'status-'.$template['EmailTemplateDescription']['id']));
				else
				echo $this->Html->link($this->Html->tag('i',false,array('class'=>'icon-off icon-white')),'#',array('class'=>'btn btn-inverse btn-mini tooltip-top','data-original-title'=>'Publish','escape'=>false,'onclick' => "published.toggle('status-".$template['EmailTemplateDescription']['id']."','".$this->Html->url('/templates/toggle/').$template['EmailTemplateDescription']['id']."/".intval($template['EmailTemplateDescription']['status'])."');",'id' =>'status-'.$template['EmailTemplateDescription']['id']));
				
				 ?>
                
                <?php echo $this->Html->link($this->Html->tag('i',false,array('class'=>'icon-pencil icon-white')),array('action' => 'admin_edit', $template['EmailTemplateDescription']['id']),array("class"=>"btn btn-primary btn-mini tooltip-top","data-original-title"=>"Edit",'escape'=>false)); ?>
                
                <?php echo $this->Form->postLink($this->Html->tag('i',false,array('class'=>'icon-remove icon-white')), array('action' => 'delete', $template['EmailTemplateDescription']['id']), array("class"=>"btn btn-danger btn-mini tooltip-top","data-original-title"=>"Delete",'escape'=>false), __('Are you sure you want to delete # %s?', $template['EmailTemplateDescription']['id'])); ?>
                
                
 				</td>
                </tr>
                <?php endforeach; ?>                
                </tbody>
            </table>
            <?php echo $this->element('admin_pagination');?>
        </div>
    </div>
</div>
<!-- /block -->
</div>




<!--POPUP-->
<div id="myModal" class="modal hide">
<div class="modal-header">
<button data-dismiss="modal" class="close" type="button">&times;</button>
<h3><?php echo __("View"); ?></h3>
</div>
<div class="modal-body">
<p>Modal Example Body</p>
</div>
</div>
<!--POPUP-->

<script type="text/javascript">
    var published = {
        toggle : function(id, url){
            obj = $('#'+id).parent();
            $.ajax({
                url: url,
                type: "POST",
                success: function(response){
                    obj.html(response);
					$('.tooltip-top').tooltip({ placement: 'top' });	
					$('.popover-top').popover({placement: 'top', trigger: 'hover'});
                }
            });
        }
    };
	
	var view = {
        toggle : function(url){	
            $.ajax({
                url: url,
                type: "POST",
                success: function(response){					
                    $('.modal-body').html(response);
                }
            });
        }
    };
	
</script>



