<?php $this->set('title_for_layout', __('Groups',true)); ?>
<?php $this->Html->addCrumb($this->Html->tag('li',__('Groups',true),array('class'=>'active')),false,false); ?>
<div class="row-fluid">
<!-- block -->
<div class="block">
    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left"><?php echo __('Groups');?></div>
        <div class="pull-right"><?php echo $this->Html->link($this->Html->tag('span',$this->Html->tag('i',false,array('class'=>'icon-plus icon-white')).__(' ADD NEW'),array('class'=>'badge badge-info')),array('action' => 'admin_add'),array("class"=>false,"data-original-title"=>"ADD NEW",'escape'=>false)); ?>
       </div>
    </div>
    <div class="block-content collapse in">
        <div class="span12">
        
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                        <th><?php echo __('ID');?></th>
                        <th><?php echo __('Name');?></th> 
                        <th><?php echo __('Created');?></th>
                        <th><?php echo __('Actions');?></th>
                    </tr>
                </thead>
                
                <tbody>
                
				<?php 
				$i=0;
				foreach ($groups as $group):
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				?>
                <tr <?php echo $class;?> >               
                <td><?php echo h($group['Group']['id']); ?>&nbsp;</td>
                <td><?php echo h($group['Group']['name']); ?>&nbsp;</td>
                <td class="center"><?php echo h(date("j M Y , g:i A",strtotime($group['Group']['created']))); ?>&nbsp; </td>
                <td class="center">
                
                <?php echo $this->Html->link($this->Html->tag('i',false,array('class'=>'icon-pencil icon-white')),array('action' => 'admin_edit', $group['Group']['id']),array("class"=>"btn btn-primary btn-mini tooltip-top","data-original-title"=>"Edit",'escape'=>false)); ?>
                
                <?php echo $this->Form->postLink($this->Html->tag('i',false,array('class'=>'icon-remove icon-white')), array('action' => 'delete', $group['Group']['id']), array("class"=>"btn btn-danger btn-mini tooltip-top","data-original-title"=>"Delete",'escape'=>false), __('Are you sure you want to delete # %s?', $group['Group']['id'])); ?>
                
                
 				</td>
                </tr>
                <?php endforeach; ?>                
                </tbody>
            </table>
            <?php echo $this->element('admin_pagination');?>
        </div>
    </div>
</div>
<!-- /block -->
</div>

