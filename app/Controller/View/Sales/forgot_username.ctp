
<div class="row">
  <div class="col-lg-4">
    <div class="panel panel-default" style="margin-top:-100px">

  <div class="panel-body form-bg">
 <?php 
	   echo $this->Session->flash(); 
	   echo $this->Session->flash('auth'); ?>
        <?php echo $this->Form->create('Sales', array('action' => 'forgot_username')); ?>
  <div class="form-group">
    <label for="exampleInputEmail1">Enter Your Email</label>
    <?php echo $this->Form->input('username',array('placeholder' => 'Enter username',
        'label' => false,
		'required'=>'required',
        'class'=>'form-control'));?> 
   
  </div>
  
<?php echo $this->Form->submit(__('Login'), array('class'=>'btn btn-warning', 'div'=>false));?>
  
  <?php echo $this->Form->end(); ?>

 <?php
		echo $this->Html->link(
    $this->Html->tag('span', '', array('class' => 'icon new')) . "Forgot your password?",
    array('controller' => 'Sales', 'action' => 'forgot_password'),
    array('class' => 'btn btn-default', 'escape' => false)
);
?>

  </div>
</div>
  </div>
</div>
<script type="text/javascript">
          $(document).ready(function(){
	$("#SalesForgotUsernameForm").validate();
		
	
});
        </script>