

      <div class="row">   

        <div class="col-md-offset-8 col-lg-4">
<div class="panel panel-default" style="margin-top:-100px">

  <div class="panel-body form-bg text-center ">
<p>Welcome <?php echo ucfirst($this->Session->read('Auth.Sales.fname')); ?></p>

   <?php echo $this->Html->link('Sign out', array('controller'=>'Sales', 'action'=>'logout'),array('class' => 'btn btn-warning btn-block'));?>
   
  </div>
</div>
        </div>

        </div>
 
      <div class="clearfix"></div>
<div class="row">
  <div class="col-lg-12"> 
  <?php echo $this->Session->flash();  ?>
    <ol class="breadcrumb">
      <li><a href="<?php echo $this->Html->url('/Sales/index');?>">Home</a></li>
      <li class="active">Order History</li>
    </ol>
    
   <div class="form-group">
            <div class="col-lg-2">
              <label for="exampleInputEmail1">Enter Search : </label>
            </div>
            <div class="col-lg-6"> <div class="form-group">
            <?php echo $this->Form->create('Sales',array('id'=>'product_serach','url'=>array('controller'=>'Sales','action'=>'product_search'),'type'=>'GET', 'class'=>'paraform')); ?>
             <div class="col-lg-8"><?php echo $this->Form->input('searchtxt',array('placeholder' => 'Enter Keyword',
        'label' => false,
		'required'=>'required',
		
        'class'=>'form-control'));?>
 </div>
 <div class="col-lg-4">  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i> </button></div>
            </div>
            <?php echo $this->Form->end();?>
            </div>
          </div>
    
    
    
    
  </div>
  
  
</div>
<?php if($product_type!=NULL):;?>
 <div class="clearfix"></div>
<div class="col-lg-12 text-center">

      <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Product List</h3>
      </div>
      <div class="panel-body">
       <div class="table-responsive">
             
			
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                        <th><?php echo __('Sl no.');?></th>
                        <th><?php echo __('Item #');?></th>
                        <th><?php echo __('Product NDC');?></th> 
                        <th><?php echo __('Product Info');?></th> 
                        <th><?php echo __('Price');?></th>
                        <th><?php echo __('Inventory');?></th>
                       
                    </tr>
                </thead>
                
                <tbody>
                <?php if(empty($products)): ?>
		<tr>
			<td colspan="7" class="striped-info">No record found.</td>
		</tr>
		<?php endif;  ?>
				<?php 
				$i=0;
				foreach ($products as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				?>
                <tr <?php echo $class;?> > 
                <td><?php echo $i; ?></td>              
                <td><?php echo h($user['Product']['itemnumber']); ?>&nbsp;</td>
                <td><?php echo h($user['Product']['product_ndc']); ?>&nbsp;</td>
                <td><?php echo h('Product Name: '.$user['Product']['productname'])?><br>
                	Category :<?php echo h($user['Product']['producttype']); ?><br>
                    &nbsp;</td>
                <td><?php echo ($user['Product']['instock']>0)?h($user['Product']['productprice']):'Call for Price'; ?>&nbsp;</td>
                <td><?php echo h($user['Product']['instock']); ?>&nbsp;</td>                
               
                </tr>
                <?php endforeach; ?>                
                </tbody>
            </table>
            <div class="pagination pagination-right pagination-mini">
	  <ul>
		<?php echo $this->Paginator->prev(''); ?>
		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
		<?php echo $this->Paginator->next(''); ?>
	  </ul>
	</div>
       </div>
      </div>
    </div>


</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Place Order</h4>
      </div>
      <div class="modal-body">
        <div class="something"><?php echo $this->Form->create('customers',array('action'=>'basketUpdate','type'=>'file', 'class'=>'login-from')); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <?php echo $this->Form->input('prodtype',array('placeholder' => 'Item Number',
        'label' => '',
		'required'=>'required',
		'type'=>'hidden',
        'class'=>'form-control'));?>
        <?php echo $this->Form->input('product_search',array('placeholder' => 'Item Number',
        'label' => '',
		'value'=>$product_type,
		'required'=>'required',
		'type'=>'hidden',
        'class'=>'form-control'));?>

       <?php echo $this->Form->input('pro_id',array('placeholder' => 'Item Number',
        'label' => '',
		'required'=>'required',
		'type'=>'hidden',
        'class'=>'form-control'));?>
 
  <tr>
    <td align="left" valign="top">Quantity :   <?php echo $this->Form->input('quantity',array('placeholder' => 'Quantity',
        'label' => '',
		'required'=>'required',		
        'class'=>'form-control'));?><div class="gap20"></div></td>
  </tr>

 <tr>
    <td align="left" valign="top"> <?php echo $this->Form->submit(__('Order'), array('class'=>'btn btn-primary', 'div'=>false));?>&nbsp;</td>
  </tr>
  
  
</table>
<?php echo $this->Form->end();?> </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php endif;?>
<script>
$(function(){

   $("#product_serach").validate();

   });
   $(function(){

   $('.push').click(function(){
      var essay_id = $(this).attr('id');
	    var quantity = $(this).data('qtyinstock');
		$("#customersProId").val(essay_id);
		$("#customersProdtype").val($(this).data('producttype'));
		
$('#mymodal').show();
      
	  $("#customersBasketUpdateForm").validate({

		rules: {
			"data[customers][quantity]":{
				required: true,
				number:true,
				min:1,
				max:quantity

				}
		},
		messages: {
			"data[customers][quantity]":"Please enter a valid number & Order should be less than ".quantity,
			
		},
		errorElement:"span"

		});


});

   });

</script>