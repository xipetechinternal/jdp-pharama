 <div class="row">   

        <div class="col-md-offset-8 col-lg-4">
<div class="panel panel-default" style="margin-top:-100px">

  <div class="panel-body form-bg text-center ">
<p>Welcome <?php echo ucfirst($this->Session->read('Auth.Sales.fname')); ?></p>

   <?php echo $this->Html->link('Sign out', array('controller'=>'Sales', 'action'=>'logout'),array('class' => 'btn btn-warning btn-block'));?>
  </div>
</div>
        </div>

        </div>
 
      <div class="clearfix"></div>
      <div class="row">   

      <div class="col-lg-12">
        
<?php 
	   echo $this->Session->flash(); 
	   echo $this->Session->flash('auth'); ?>
<ol class="breadcrumb">
  <li> <?php echo $this->Html->link('Home', array('controller'=>'Sales', 'action'=>'index'),array('class' => ''));?></li>
  <li class="active">Manage Clients</li>
</ol>


  <div class="panel-body" >

    
          <?php echo $this->Form->create('User',array('url'=>array('controller'=>'Sales','action'=>'create_clients'),'type'=>'file', 'class'=>'login-from')); ?>
           <div class="row">
                  <div class="col-md-6 form-group">
                   <?php echo $this->Form->input('username',array('placeholder' => 'Username',
		'required'=>'required',
        'label' => 'Username',
        'class'=>'form-control'));?> 
                  </div>
                  <div class="col-md-6 form-group">
                    <?php echo $this->Form->input('state_no',array('placeholder' => 'State No.',
        'label' => 'State No.',
		'required'=>'required',
        'class'=>'form-control'));?>
                  </div>
          </div>
          <div class="row">
                  <div class="col-md-6 form-group changepassword" <?php if($id!=NULL){?> style="display:none" <?php } ?>>
                   <?php echo $this->Form->input('password',array('placeholder' => 'Password',
        'label' => 'Password',
		'required'=>'required',
        'class'=>'form-control'));?> 
                  </div>
                  <div class="col-md-6 form-group ">
                   <?php echo $this->Form->input('state_no_exp',array('placeholder' => 'State Exp',
        'label' => 'State Exp','type' => 'text',
		'required'=>'required',
        'class'=>'form-control'));?> 
                  </div>
         </div>
         <div class="row">
                  <div class="col-md-6 form-group changepassword" <?php if($id!=NULL){?> style="display:none" <?php } ?>>
                      <?php echo $this->Form->input('password2',array('placeholder' => 'Confirm Password',
        'label' => 'Confirm Password',
		'type'=>'password',
		'required'=>'required',
        'class'=>'form-control'));?> 
                  </div>
                  <div class="col-md-6 form-group">
                    <?php echo $this->Form->input('den_no',array('placeholder' => 'DEN No.',
        'label' => 'DEN No.',
		'required'=>'required',
        'class'=>'form-control'));?> 
                   
                  </div>
         </div>
         <div class="row">
                  <div class="col-md-6 form-group">
                     <?php echo $this->Form->input('fname',array('placeholder' => 'First Name',
        'label' => 'First Name',
		'required'=>'required',
        'class'=>'form-control'));?> 
                  </div>
                  <div class="col-md-6 form-group">
                   <?php echo $this->Form->input('den_no_exp',array('placeholder' => 'DEN Exp',
        'label' => 'DEN Exp',
		'type'=>'text',
		'required'=>'required',
        'class'=>'form-control'));?> 
                  
                  </div>
         </div>
         <div class="row">
                  <div class="col-md-6 form-group">
                   <?php echo $this->Form->input('lname',array('placeholder' => 'Last Name',
        'label' => 'Last Name',
		'required'=>'required',
        'class'=>'form-control'));?> 
                   
                  </div>
                  <div class="col-md-6 form-group">
                     <?php echo $this->Form->input('email',array('placeholder' => 'Email',
        'label' => 'Email',
        'class'=>'form-control'));?> 
                  </div>
          </div>
          <div class="row">
                  <div class="col-md-6 form-group">
                   <?php echo $this->Form->input('phone',array('placeholder' => 'Phone',
        'label' => 'Phone',
		'required'=>'required',
        'class'=>'form-control'));?> 
                  </div>
          
                  <div class="col-md-6 form-group">
                   
                    <?php echo $this->Form->input('fax',array('placeholder' => 'Fax',
        'label' => 'Fax',
        'class'=>'form-control'));?> 
                  </div>
          </div>
          <div class="row">
                  <div class="col-md-6 form-group">
                   <?php echo $this->Form->input('address',array('placeholder' => 'Address',
        'label' => 'Address',
		'required'=>'required',
        'class'=>'form-control'));?> 
                  </div>
                  <div class="col-md-2 form-group">                  
                    <?php echo $this->Form->input('city',array('placeholder' => 'City',
        'label' => 'City',
		'required'=>'required',
        'class'=>'form-control'));?> 
                  </div>
                  <div class="col-md-2 form-group">
                   <?php echo $this->Form->input('state',array('placeholder' => 'State',
        'label' => 'State',
		'required'=>'required',
        'class'=>'form-control'));?> 
                    
                  </div>
                  <div class="col-md-2 form-group">
                  <?php echo $this->Form->input('zip_code',array('placeholder' => 'Zip Code',
        'label' => 'Zip Code',
		'required'=>'required',
        'class'=>'form-control'));?> 
                    
                  </div>
             </div>
          <div class="row">    
                  <div class="col-md-6 form-group">
                   <?php echo $this->Form->input('member_status',array('options' => array(''=>'--Select Member Type--',1=>'Gold',2=>'Silver',3=>'Bronze'),
        'label' => 'Member Type',
		'required'=>'required',
        'class'=>'form-control'));?>
                  </div>
          </div>
                  <div class="gap20"></div>
                  <div class="clearfix"></div>
                  <div class="col-md-12 text-right form-group">
                   <?php echo $this->Form->input('active',array('type' => 'hidden','value'=>'Y'));?> 
                  <?php echo $this->Form->input('status',array('type' => 'hidden','value'=>'1'));?> 
                   <?php echo $this->Form->input('level',array('type' => 'hidden','value'=>'client'));?>
                    <?php echo $this->Form->input('id');?>
                    <?php echo $this->Form->input('sid',array(
        'label' => '',
		'value'=>$uid,
		'type'=>'hidden',
        'class'=>'form-control'));?>
<?php echo $this->Form->submit(__('Submit'), array('class'=>'btn btn-primary', 'div'=>false));?>&nbsp;
<?php echo $this->Form->reset(__('Cancel'), array('class'=>'btn', 'div'=>false));?>
<?php if($id!=NULL){?>
&nbsp;<?php echo $this->Form->reset(__('Change Password'), array('value'=>'Change Password','class'=>'btn btn-warning','onclick'=>"$('.changepassword').show();",'type'=>'button','div'=>false));?>
<?php } ?>
                  </div>
               <?php echo $this->Form->end();?>
      </div>
  
  </div>

      </div>

<div class="col-lg-12 text-center">

      <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Manage Client Accounts</h3>
      </div>
      <div class="panel-body">
       <div class="table-responsive">
             
			<?php $membertype=array(1=>'Gold',2=>'Silver',3=>'Bronze'); ?>
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                        <th><?php echo __('Email');?></th>
                        <th><?php echo __('Username');?></th>
                         
                        <th><?php echo __('Member Type');?></th>
                        <th><?php echo __('Phone');?></th> 
                        <th><?php echo __('Address');?></th> 
                      
                        <th><?php echo __('Actions');?></th>
                    </tr>
                </thead>
                
                <tbody>
                
				<?php 
				$i=0;
				foreach ($users as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				?>
                <tr <?php echo $class;?> >               
                <td><?php echo h($user['User']['email']); ?></td>
                <td><?php echo h($user['User']['username']); ?></td>
               
                <td><?php echo h($membertype[$user['User']['member_status']]); ?></td>
                <td><?php echo h($user['User']['phone']); ?></td>
                <td><?php echo h($user['User']['address']); ?><br /><?php echo h($user['User']['city']); ?><br /><?php echo h($user['User']['state']); ?><br /><?php echo h($user['User']['zip_code']); ?></td>
                
                <td class="center">              
                <?php echo $this->Html->link($this->Html->tag('i','Edit',array('class'=>'icon-pencil icon-white')),array('action' => 'create_clients', $user['User']['id']),array("class"=>"btn btn-primary btn-mini tooltip-top","data-original-title"=>"Edit",'escape'=>false)); ?>
                
                <?php echo $this->Form->postLink($this->Html->tag('i','Delete',array('class'=>'icon-remove icon-white')), array('action' => 'client_users_delete', $user['User']['id']), array("class"=>"btn btn-danger btn-mini tooltip-top","data-original-title"=>"Delete",'escape'=>false), __('Are you sure you want to delete # %s?', $user['User']['id'])); ?>
 				</td>
                </tr>
                <?php endforeach; ?>                
                </tbody>
            </table>
            <div class="pagination pagination-right pagination-mini">
	  <ul>
		<?php echo $this->Paginator->prev(''); ?>
		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
		<?php echo $this->Paginator->next(''); ?>
	  </ul>
	</div>
       </div>
      </div>
    </div>


</div>


      </div>

     
<style>
#content-area{
	background-image:none;
}
</style>
<script>
$(document).ready(function(){
	$("#UserCreateClientsForm").validate({

		rules: {
			"data[User][password]":{
				required: true,
				minlength:5,

				},
			"data[User][password2]":{
				required: true,
				minlength:5,
				equalTo: "#UserPassword"

				}
		},
		messages: {
			"data[User][password]":"Please check this field",
			"data[User][password2]":{equalTo:"make sure both match"}
		},
		errorElement:"span"

		});
		
		 $( "#UserStateNoExp" ).datepicker({dateFormat: 'yy-mm-dd' }); 
    $( "#UserDenNoExp" ).datepicker({dateFormat: 'yy-mm-dd' }); 
});
</script>