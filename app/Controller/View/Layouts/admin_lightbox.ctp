<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
  


    	<?php echo $this->Html->charset(); ?>
        <title><?php echo $title_for_layout; ?></title>      

    <!-- Bootstrap core CSS -->
    <!-- Bootstrap -->
  
       <?php
	echo $this->Html->css('admin/bootstrap.min');
	echo $this->Html->css('admin/bootstrap-theme.min');	
	echo $this->Html->css('admin/bootstrap_calendar');
	echo $this->Html->css('admin/font-awesome');
	
	?>
   
     <?php
	echo $this->Html->css('admin/dashboard');
	echo $this->Html->css('admin/datatables');	
	echo $this->Html->css('jquery.ui.datepicker.min');	
	echo $this->Html->css('jquery.ui.core.min');
	echo $this->Html->css('jquery.ui.theme.min');
	?>
    
<?php echo $this->Html->script(array('jquery-1.8.3.min'));?>
<?php echo $this->Html->script(array('jquery.validation.functions'));?>
<?php echo $this->Html->script(array('jquery.validate.min'));?>
<?php echo $this->Html->script(array('jquery-ui-1.9.2.datepicker.custom.min'));?>
<?php echo $this->Html->script(array('bootstrap.min'));?>
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

            

    <div class="container-fluid">
      <div class="row">
        
        <div class="col-lg-10" id="content">
                     
                    
                <!-- Content -->
                <?php echo $this->fetch('content'); ?>		
                <!-- /Content -->
                    
                </div>
      </div>
    </div>



  </body>
</html>
