<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Pages
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/* echo "<pre>";
 print_r($users);*/
 
?>
<style>
 a {
	text-decoration:none !important;
}
</style>

<div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Dashboard</div>
                                
                            </div>
                            
                            <div class="block-content collapse in">
                                <div class="row-fluid padd-bottom">
                                  <div class="span3">
                                      <a href="#" class="thumbnail">
                                       <i class="icon-user"></i> Admin Users
                                      </a>
                                  </div>
                                  <div class="span3">
                                      <a href="#" class="thumbnail">
                                       <i class="icon-user"></i> Sales Users
                                      </a>
                                  </div>
                                  <div class="span3">
                                       <a href="#" class="thumbnail">
                                       <i class="icon-user"></i> Operators Users
                                      </a>
                                  </div>
                                  <div class="span3">
                                       <a href="#" class="thumbnail">
                                       <i class="icon-user"></i> Customers Users
                                      </a>
                                  </div>
                                </div>

                                <div class="row-fluid padd-bottom">
                                  <div class="span3">
                                       <a href="#" class="thumbnail">
                                       <i class="icon-th-list"></i> Categories
                                      </a>
                                  </div>
                                  <div class="span3">
                                       <a href="#" class="thumbnail">
                                       <i class="icon-briefcase"></i> Products
                                      </a>
                                  </div>
                                  <div class="span3">
                                       <a href="#" class="thumbnail">
                                       <i class="icon-briefcase"></i> Product Stocks
                                      </a>
                                  </div>
                                  <div class="span3">
                                       <a href="#" class="thumbnail">
                                       <i class="icon-book"></i> Sales Reports
                                      </a>
                                  </div>
                                </div>

                                <div class="row-fluid padd-bottom">
                                  <div class="span3">
                                       <a href="#" class="thumbnail">
                                       <i class="icon-book"></i> Threshold Stock Reports
                                      </a>
                                  </div>
                                  <div class="span3">
                                       <a href="#" class="thumbnail">
                                       <i class="icon-book"></i> Templates
                                      </a>
                                  </div>
                                  <div class="span3">
                                       <a href="#" class="thumbnail">
                                       <i class="icon-book"></i> Terms & Conditions
                                      </a>
                                  </div>
                                  <div class="span3">
                                       <a href="#" class="thumbnail">
                                       <i class="icon-book"></i> Privacy Policy
                                      </a>
                                  </div>
                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>