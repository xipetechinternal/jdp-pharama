

      
 
      <div class="clearfix"></div>
<div class="row ">
  <div class="col-lg-12"> 
  <?php echo $this->Session->flash();  ?>
    <ol class="breadcrumb">
      <li><a href="<?php echo $this->Html->url('/customers/index');?>">Home</a></li>
      <li class="active">Product Type Search</li>
    </ol>
    <div class="wall">
    <?php echo $this->Form->create('OrderHistory',array('url'=>array('action'=>'order_history','controller'=>'customers'),'class'=>'paraform')); ?>
   <?php echo $this->Form->input('orderid',array('placeholder' => 'From Date',
        'label' => false,
		'type'=>'hidden',
		'required'=>'required',
        'class'=>'form-control'));?> 
     
      <div class="form-group">
        <div class="col-lg-2">
          <label for="exampleInputEmail1">Select Date:</label>
        </div>
        <div class="col-lg-8">
          <div class="form-group">
            <div class="col-lg-4">
            <?php echo $this->Form->input('selfrmdate',array('placeholder' => 'From Date',
        'label' => false,
		
        'class'=>'form-control'));?> 
             
            </div>
            <div class="col-lg-4">
            <?php echo $this->Form->input('seltodate',array('placeholder' => 'To Date',
        'label' => false,
		
        'class'=>'form-control'));?>
              
            </div>
            <div class="col-lg-4">
            <?php echo $this->Form->button('<i class="fa fa-search"></i> ', array(
    'type' => 'submit',
    'class' => 'btn btn-default',
    'escape' => false
));?>
              
            </div>
           
            
          </div>
        </div>
      </div>
        <div class="clearfix"></div>
      <div class="form-group">
      <div class="col-md-2">
        <label for="exampleInputEmail1" style="margin-top:5px;">Enter Search :</label>
      </div>
      <div class="col-md-6">
        <div class="form-group">
        
          <div class="input-group">

            <?php echo $this->Form->input('searchtxt',array('placeholder' => 'Order ID',
  'label' => false,
  'class'=>'form-control'));?>
            
        </div>
      
        </div>
      </div>
    </div>
      <?php echo $this->Form->end();?>
      </div>
      <div class="clearfix"></div>
      <div class="gap20"></div>
    
    
    
    
    
  </div>
  
  
      <div class="col-lg-12 text-center">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">Order History</h3>
          </div>
          <div class="panel-body">
            <div class="table-responsive">
              <h2> Placed Orders</h2>
              <?PHP $searcset=(isset($this->request->data['OrderHistory']['selfrmdate']) and isset($this->request->data['OrderHistory']['seltodate']))?$this->request->data['OrderHistory']['selfrmdate'].'/'.$this->request->data['OrderHistory']['seltodate']:'';?>
                 <div class="col-lg-12 text-right" style="padding:20px"> &nbsp;&nbsp;<a class="btn btn-warning btn-mini btn-left-margin" href="<?php echo $this->Html->url(array_merge(array('controller'=>'customers','action'=>'client_order_history_excel/'.$searcset),$this->params['named'],array(''))); ?>"><i class="icon-file-text"></i> Download Excel/CSV</a></div>
                 
                 <?php echo $this->Form->create('OrderHistory',array('url'=>array('action'=>'order_history','controller'=>'customers'),'class'=>'paraform','type'=>'get')); ?>     
      <div class="form-group text-right">
      <div class="col-md-2">
        <?php echo $this->Form->input('unpaid',array('type'=>'hidden','label' => false,'value'=>true,'class'=>'form-control'));?> 
      </div>
      <div class="col-lg-12 text-right">     
        
          

            <?php  echo $this->Html->link('Statement',array('action' => 'order_statement'),array("class"=>"btn btn-success btn-mini tooltip-top","data-original-title"=>"Edit",'escape'=>false));?>
        
      
        
      </div>
      <div class="col-lg-12 text-right">     
        
          

            <?php echo $this->Form->button('Unpaid Invoice ', array(
    'type' => 'submit',
    'class' => 'btn btn-info',
    'escape' => false
));?>
        
      
        
      </div>
    </div>
      <?php echo $this->Form->end();?>
              <table id="example" class="datatable display table table-striped table-bordered" width="100%" >
                <thead>
                  <tr>
                    
                    <th><strong><?php echo __('Order');?></strong></th>
                    <th><strong><?php echo __('PO Number');?></strong></th>
                    <th><strong><?php echo __('Quantity');?></strong></th>
                    <th><strong><?php echo __('Price');?></strong></th>
                    <th><strong><?php echo __('Status');?></strong></th>
                    
                    <th><strong><?php echo __('ORDER DATE');?></strong></th>
                    <th><strong><?php echo __('Invoice');?></strong></th>
                  </tr>
                </thead>
                 <?php if(empty($users)): ?>
		<tr>
			<td colspan="7" class="striped-info">No record found.</td>
		</tr>
		<?php endif;  ?>

                <tbody>
                
				<?php 
				
				$i=0;
				$product_price=0;
				foreach ($users as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				
				$product_qauantity=count($user['OrderdetailMany']);
				
				$priice=0;
				foreach($user['OrderdetailMany'] as $orderdeatils){
					$priice=$priice+($orderdeatils['price']*$orderdeatils['quantity']);
				}
				$product_price=$product_price+$priice;
				?>
                <tr <?php echo $class;?> > 
                        
               
                <td><?php echo h($user['OrderDist']['tmporderid']); ?> &nbsp;</td>
               <td><?php echo h($user['OrderDist']['po_number']); ?> &nbsp;</td>
               
                             
               <td>  <?php echo $this->Html->link($product_qauantity.' Products', array('controller'=>'customers', 'action'=>'orderinfo',$user['OrderDist']['id']),array('class' => ''));?>
                    &nbsp;</td>
                    <td>$<?php echo $priice ?> </td>
                    <td><?php echo h(($user['OrderDist']['status']=='Accep')?'Accept':$user['OrderDist']['status']); ?> &nbsp;</td>  
                    <td><?php echo date("m-d-Y",strtotime($user['OrderDist']['createddt'])); ?> &nbsp;</td>  
                    <td class="td-font">
                    <?php if($user['OrderDist']['status']=='Accep' or $user['OrderDist']['status']=='Test'){?>
                    <a href="invoice/<?php echo $user['OrderDist']['id']?>">invoice</a> | <a href="invoicepdf/<?php echo $user['OrderDist']['id']?>">PDF Invoice</a>
                    <?php }?>
                    </td>
 
                </tr>
                <?php endforeach; ?>                
                </tbody>
                <tfoot>
          <tr>
          
            <th colspan="3" style="text-align:right">Total:</th>
            <th>$<?php echo $product_price?></th>
          </tr>
        </tfoot>
              </table>
              <div class="pagination pagination-right pagination-mini">
	  <ul>
		<?php echo $this->Paginator->prev(''); ?>
		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
		<?php echo $this->Paginator->next(''); ?>
	  </ul>
	</div>
            </div>
            <div class="clearfix"></div>
            <div class="gap20"></div>
            <h2> Pending Orders</h2>

            <div class="table-responsive">
              <table id="example" class="datatable display table table-striped table-bordered" width="100%" >
                <thead>
                  <tr>
                   
                    <th><strong><?php echo __('Order');?></strong></th>
                    <th><strong><?php echo __('PO Number');?></strong></th>
                    <th><strong><?php echo __('Quantity');?></strong></th>
                    <th><strong><?php echo __('Price');?></strong></th>
                    
                  </tr>
                </thead>
                 <?php if(empty($pendingorder)): ?>
		<tr>
			<td colspan="7" class="striped-info">No record found.</td>
		</tr>
		<?php endif;  ?>

                <tbody>
                
				<?php 
				
				$i=0;
				$product_price=0;
				foreach ($pendingorder as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				
				$product_qauantity=count($user['OrderdetailMany']);
				
				$priice=0;
				foreach($user['OrderdetailMany'] as $orderdeatils){
					$priice=$priice+($orderdeatils['price']*$orderdeatils['quantity']);
				}
				$product_price=$product_price+$priice;
				?>
                <tr <?php echo $class;?> > 
                        
               
                <td><?php echo h($user['Order']['tmporderid']); ?> &nbsp;</td>
               <td><?php echo h($user['Order']['po_number']); ?> &nbsp;</td>
               
                             
               <td>  <?php echo $this->Html->link($product_qauantity.' Products', array('controller'=>'customers', 'action'=>'orderinfo',$user['Order']['id']),array('class' => ''));?>
                    &nbsp;</td>
                    <td>$<?php echo $priice ?> </td>  
                     
                </tr>
                <?php endforeach; ?>                
                </tbody>
                <tfoot>
          <tr>
          
            <th colspan="3" style="text-align:right">Total:</th>
            <th>$<?php echo $product_price?></th>
          </tr>
        </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
     
      </div>
      
<script>
$(document).ready(function(){
	$("#ProductInfoAdminSalesReportPersonForm").validate();
	//$( "#ProductInfoSelfrmdate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
   // $( "#ProductInfoSeltodate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
	
	
    $( "#OrderHistorySelfrmdate, #OrderHistorySeltodate" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
       dateFormat: 'yy-mm-dd' ,
        onSelect: function( selectedDate ) {
            if(this.id == 'OrderHistorySelfrmdate'){
              var dateMin = $('#OrderHistorySelfrmdate').datepicker("getDate");
              var rMin = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 1); // Min Date = Selected + 1d
              var rMax = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 365); // Max Date = Selected + 31d
              $('#OrderHistorySeltodate').datepicker("option","minDate",rMin);
              $('#OrderHistorySeltodate').datepicker("option","maxDate",rMax);                    
            }

        }
    });

});
</script>