<?php echo $this->Html->script(array('jQuery.print'));?>
      <div class="row">   

        <div class="col-md-offset-8 col-lg-4">
<div class="panel panel-default" style="margin-top:-100px">

  <div class="panel-body form-bg text-center ">
<p>Welcome <?php echo ucfirst($this->Session->read('Auth.User.fname')); ?></p>

   <?php echo $this->Html->link('Sign out', array('controller'=>'customers', 'action'=>'logout'),array('class' => 'btn btn-warning btn-block'));?>
   
   <p><?php echo $this->Html->link($cart_list.' Orders Pending', array('controller'=>'customers', 'action'=>'order_cart'),array('class' => ''));?>
</p>
  </div>
</div>
        </div>

        </div>
 
      <div class="clearfix"></div>
<div class="col-lg-12">
<h4><a href="<?php echo $this->Html->url('/admin/users');?>">Home</a> :: TI,TH,TS</h4>
 <div class="row">
  <div class="col-xs-9">
  <button class="btn btn-primary btn-mini tooltip-top" onclick="history.go(-1);">Back </button>
  </div>
  <div class="col-xs-3">
  			
          <button type="button" class="btn btn-success print" data-dismiss="modal">Print</button>
          &nbsp;&nbsp;
         
           <br /><br />
       	<?php $custom_change_sz=json_decode($dhtml['Pedigree']['custom_change_sz']);
			  $batchno=($custom_change_sz->Users->batchno!='')?$custom_change_sz->Users->batchno:$dhtml['ProductInfo']['batchno'];
			  $received=($custom_change_sz->Users->received!='')?$custom_change_sz->Users->received:$dhtml['ProductInfo']['ReceivedDate'];
			  $received2=($custom_change_sz->Users->received2!='')?$custom_change_sz->Users->received2:date('m-d-Y',strtotime($dhtml['ProductInfo']['ReceivedDate']));
			  $expdate=($custom_change_sz->Users->expdate!='')?$custom_change_sz->Users->expdate:date('m-d-Y',strtotime($dhtml['ProductInfo']['expdate']));
			 
			  $quantity=($custom_change_sz->Users->quantity!='')?$custom_change_sz->Users->quantity:$lot_qunetiony;
			  $qutitblance=($dhtml['ProductInfo']['balance']>0)?$dhtml['ProductInfo']['balance']:$dhtml['ProductInfo']['availability'];
			  $purchasequantity=($custom_change_sz->Users->purchasequantity!='')?$custom_change_sz->Users->purchasequantity:$qutitblance;
		?>

   </div>
  </div>     
<div class="col-lg-12" id="print_div">
<div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">TI,TH,TS</h3>
      </div>
      
          
      
      
      
    </div>
    <h3 class="text-left">TI</h3>
<table  style="width:100%" border="1" cellpadding="0" cellspacing="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                     <th align="center"><?php echo __('Ndc');?></th>
                       <th align="center"><?php echo __('Item #');?></th>
                        <th align="center"><?php echo __('dis');?></th>
                        <th align="center"><?php echo __('lot');?></th>
                        <th align="center"><?php echo __('Exp');?></th>
                        <th align="center"><?php echo __('Rec date');?></th> 
                     <!-- <th align="center"><?php echo __('Purchase Quantity');?></th>
                       <th align="center"><?php echo __('SOLD TO');?></th>-->  

                       <th align="center"><?php echo __('Manufacture');?></th> 
                    </tr>
                </thead>                
                <tbody>
               
						<tr> 
						 <td align="center"><?php echo $dhtml['ProductInfo']['Product']['product_ndc'];?></td>
                         <td align="center"><?php echo $dhtml['ProductInfo']['Product']['itemnumber'];?></td>               
						 <td align="center"><?php echo $dhtml['ProductInfo']['Product']['productname'];?></td>
						 <td align="center"><?php echo $batchno;?></td>              
						 <td align="center"><?php echo date('m-d-Y',strtotime( $expdate));?></td>
						 <td align="center"><?php echo $received;?></td>
                         <!--<td align="center"><?php echo $purchasequantity;?></td>
                         <td align="center"><?php echo $quantity;?></td> --> 
						<td align="center"><?php echo $dhtml['ProductInfo']['Product']['manufacture'];?></td>
						</tr>
                                  
                </tbody>               
            </table>
  
  <div class="gap20"></div>
  <h3 class="text-left">TH</h3>
	<?php	$tabledata=json_decode($dhtml['Pedigree']['Invoice_T2']);
	$tabledata=str_replace('You','JD Pharmaceutical',$tabledata);
	$tabledata=str_replace("nbsp",'',$tabledata);
	$tabledata=str_replace("&",'',$tabledata);
	$tabledata=str_replace("amp;",'',$tabledata);
			echo $tabledata=str_replace("<table>",'<table class="table table-hover table-striped table-bordered" id="pedgreetable">',$tabledata);
		$Invoice_P1=json_decode($dhtml['Pedigree']['Invoice_P1']);
	//echo '<br><p align="center">'.$Invoice_P1.'</p>';
	
	//$Invoice_P2=json_decode($dhtml['Pedigree']['Invoice_P2']);
	//echo '<p align="center"><img src="'.$Invoice_P2.'" width="20%"></p>';
	
	//$Invoice_P3=json_decode($dhtml['Pedigree']['Invoice_P3']);
	//echo '<br><p align="center">'.$Invoice_P3.'</p>';
	$createddt=($custom_change_sz->Users->createddt!='')?$custom_change_sz->Users->createddt:$Orderdetail['Order']['createddt'];
	$received=($custom_change_sz->Users->received!='')?$custom_change_sz->Users->received:'';
	?>
    <br>
    <h3 class="text-left">TS</h3>
    
    <strong>Jd pharmaceutical wholesaler inc. certifies that it</strong>
    <br>
    <br>
    <ol>
    	<li>is authorized under the Drug Quality and Security Act (the "Act") to sell the products identified in this Transaction History to you</li>
    <li>received these products from a person or entity authorized under the Act</li>
    <li>received a Transaction Statement from the prior owner of these products</li>    
    <li> did not knowingly ship a suspect or illegitimate produc</li>    
    <li> had systems in place to comply with the verification requirements of the Act</li>    
    <li>did not knowingly provide false Transaction Information </li>
    <li>did not knowingly alter the Transaction History</li>
   </ol>
     <p></p>
   <!-- <p align="center">
jd pharmaceutical wholesaler inc. certifies that it:(a) is authorized under the Drug Quality and Security Act (the "Act") to sell the products identified in this Transaction History to you(b) received these products from a person or entity authorized under the Act(c) received a Transaction Statement from the prior owner of these products(d) did not knowingly ship a suspect or illegitimate product(e) had systems in place to comply with the verification requirements of the Act(f) did not knowingly provide false Transaction Information (g) did not knowingly alter the Transaction History.</p>-->
   <!-- <br><p align="center">DEROU BIGLARI</p>
    <p align="center">OPERATIONS MANAGER
JD PHARMACEUTICAL WHOLESALER INC

         			</p>-->
    
</div>

</div>
<script>
	$('.print').click(function(){
		$('.print').hide();
     $("#print_div").print();
	 $('.print').show();
});</script>  
<style>
table {
max-width: 100%;
background-color: transparent;
border-collapse: collapse;
border-spacing: 0;
border:1px;
}
th {
text-align: center;
font-weight:bold;
}
h5, .h5 {
font-weight:bold;
}
</style>
<script>
$("#pedgreetable tr:nth-child(2) td:last").html("Invoice No <?php echo $dhtml["Pedigree"]["Invoice"]?>");
row="<tr><td><strong><?php echo $Orderdetail["User"]["fname"].' '.$Orderdetail["User"]["lname"]?></strong>  Invoice# <?php echo $Orderdetail["Order"]["tmporderid"]?> Ordered on </td><td><?php echo $createddt;?></td><td>Purchased from JD Pharmaceutical</td></tr>"
$("#pedgreetable tr:first").before(row);

$(document).ready(function(e) {
	received='<?php echo $received2?>';
	if(received=='')
	received=$("#pedgreetable tr:nth-child(2) td:nth-child(2)").html();
    $("#pedgreetable tr:nth-child(2) td:nth-child(2)").html(received);
});
</script>