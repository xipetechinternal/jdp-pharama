<?php echo $this->Html->script('https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js', array('inline' => true));?>
   <?php
      echo $this->Html->css('https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css');
    ?>

      
 
      <div class="clearfix"><br /></div>
      
<div class="row">
  <div class="col-lg-12"> 
  <?php echo $this->Session->flash();  ?>
    <ol class="breadcrumb">
      <li><a href="<?php echo $this->Html->url('/customers/index');?>">Home</a></li>
      <li class="active">Product Dispense List</li>
    </ol>
    
      <div class="clearfix"></div>
      <div class="gap20"></div>
    
    <div class="col-lg-12 text-right"><a class="btn btn-info " href="<?php echo $this->Html->url('/customers/dispense');?>">Back</a> </div>
   <div class="clearfix"></div>
      <div class="gap20"></div>
    
  
      <div class="col-lg-12 text-center">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">Dispense List</h3>
          </div>
          <div class="panel-body">
            <div class="table-responsive">
             
             </div>
                 
                
              <table id="example" class="datatable display table table-striped table-bordered" width="100%" >
                <thead>
                  <tr>
                    
                    <th><strong><?php echo __('Product NDC');?></strong></th>
                   
                    <th><strong><?php echo __('Product Name');?></strong></th>
                   
                    <th><strong><?php echo __('Dispense');?></strong></th>
                    
                    
                    <th><strong><?php echo __('Date');?></strong></th>
                    <th><strong><?php echo __('Action');?></strong></th>
                  </tr>
                </thead>
                 <?php if(empty($Products)): ?>
		<tr>
			<td colspan="8" class="striped-info">History not available.</td>
		</tr>
		<?php endif;  ?>

                <tbody>
                
				<?php 
				
				$i=0;
				$product_price=0;
				foreach ($product_list as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				
				
				?>
                <tr <?php echo $class;?> > 
                        
               
                <td><?php echo h($user['ProductUsedSize']['product_ndc']); ?> &nbsp;</td>
               <td><?php echo h($Products['Products']['productname']); ?> &nbsp;</td>
               
                             
               <td>  <?php echo h($user['ProductUsedSize']['size']); ?>&nbsp;</td>
                    <td><?php echo h(date("m-d-Y",strtotime($user['ProductUsedSize']['date']))); ?> </td>
                    <td><?php echo $this->Form->postLink($this->Html->tag('i','Delete',array('class'=>'icon-remove icon-white')), array('action' => 'dispense_list',$user['ProductUsedSize']['product_ndc'],$user['ProductUsedSize']['id']), array("class"=>"btn btn-danger btn-mini tooltip-top","data-original-title"=>"Delete",'escape'=>false), __('Are you sure you want to delete # %s?',$user['ProductUsedSize']['product_ndc'])); ?></td>
                </tr>
                <?php endforeach; ?>                
                </tbody>
                
              </table>
              
            </div>
            <div class="clearfix"></div>
            <div class="gap20"></div>
           

            
          </div>
        </div>
      </div>
     
      </div>
      </div>
    
</div>
<script>

$(document).ready(function(){
	$( "#OrderHistoryDateTime" ).datepicker({
       
        changeMonth: true,
		changeYear: true,
       dateFormat: 'yy-mm-dd' ,
        
    });
	$('#OrderHistoryProductNdc').on('blur mouseup',function(){
    		var products_jshon=<?php echo json_encode($product_list);?>;
        $.each(products_jshon, function(i, v) {
			console.log(i,v);
			if (i == $("#OrderHistoryProductNdc").val()) {
				
				$( "#OrderHistoryProdid" ).val(i);
				return;
			}
		});
    });
	
	$( "#OrderHistoryProductNdc" ).autocomplete(
	{
		source:<?php echo json_encode($product_list_auto);?>,
		select: function( event, ui ) {
			$( "#OrderHistoryProductNdc" ).val( ui.item.label  );
			$( "#OrderHistoryProdid" ).val( ui.item.value  );
			return false;
		}
	}).data( "autocomplete" )._renderItem = function( ul, item ) {
		return $( "<li></li>" )
			.data( "item.autocomplete", item )
			.append( "<a><strong>" + item.label + "</strong> </a>" )
			.appendTo( ul );
		};

	$("#ProductInfoAdminSalesReportPersonForm").validate();
	//$( "#ProductInfoSelfrmdate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
   // $( "#ProductInfoSeltodate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
	
	
    


});
</script>