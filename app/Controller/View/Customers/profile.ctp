

      
 
    <br>
    <ol class="breadcrumb">
      <li><a href="<?php echo $this->Html->url('/customers/index');?>">Home</a></li>
      <li class="active">Manage Profile</li>
    </ol>
  
<div class="row ">
 <div class="col-lg-2"></div>
  <div class="col-lg-8 "> 
  <?php echo $this->Session->flash();  ?>
    
  <div class="clearfix"></div>
  <div class="well">
      <form class="form-horizontal" onsubmit="return false">
        <div class="form-group">
            <label for="UserUsername" class="control-label col-xs-2">Username</label>
            <div class="col-xs-10">
                <?php
     echo $this->Form->input('User.username', array('div'=>'clearfix',
        
        'after'=>$this->Form->error('User.username', array(), array('wrap' => 'span', 'class' => 'help-inline')),
        'error' => array('attributes' => array('style' => 'display:none')),
        'label'=>false, 'class'=>'form-control','placeholder'=>'Enter username','readonly'=>true));
	?>
            </div>
        </div>
        <div class="form-group">
            <label for="UserCUSTOMERNUMBER" class="control-label col-xs-2">CUSTOMER#</label>
            <div class="col-xs-10">
                <?php
     echo $this->Form->input('User.CUSTOMER_NUMBER', array(
        'after'=>$this->Form->error('User.CUSTOMER_NUMBER', array(), array('wrap' => 'span', 'class' => 'help-inline')),
        'error' => array('attributes' => array('style' => 'display:none')),
        'label'=>false, 'class'=>'form-control','placeholder'=>'CUSTOMER NUMBER','readonly'=>true));
	?>
            </div>
        </div>        
        <div class="form-group">
            <label for="UserFname" class="control-label col-xs-2">First Name</label>
            <div class="col-xs-10">
                <?php	
    echo $this->Form->input('User.fname', array(
        'after'=>$this->Form->error('User.fname', array(), array('wrap' => 'span', 'class' => 'help-inline')),
        'error' => array('attributes' => array('style' => 'display:none')),
        'label'=>false, 'class'=>'form-control','placeholder'=>'Enter name','readonly'=>true));
	?>
            </div>
        </div>        
        <div class="form-group">
            <label for="UserFname" class="control-label col-xs-2">Last Name</label>
            <div class="col-xs-10">
               <?php		
	echo $this->Form->input('User.lname', array(
        'after'=>$this->Form->error('User.contact_name', array(), array('wrap' => 'span', 'class' => 'help-inline')),
        'error' => array('attributes' => array('style' => 'display:none')),
        'label'=>false, 'class'=>'form-control','placeholder'=>'Enter Last name','readonly'=>true));	
	?>
            </div>
        </div>
        
        <div class="form-group">
            <label for="UserFname" class="control-label col-xs-2">Email</label>
            <div class="col-xs-10">
                <?php		
	echo $this->Form->input('User.email', array(
        'after'=>$this->Form->error('User.email', array(), array('wrap' => 'span', 'class' => 'help-inline')),
        'error' => array('attributes' => array('style' => 'display:none')),
        'label'=>false, 'class'=>'form-control','placeholder'=>'Enter email','readonly'=>true));	
	?>
            </div>
        </div>
        <div class="form-group">
            <label for="UserFname" class="control-label col-xs-2">Phone</label>
            <div class="col-xs-10">
              <?php		
	echo $this->Form->input('User.phone', array(
        'after'=>$this->Form->error('User.phone', array(), array('wrap' => 'span', 'class' => 'help-inline')),
        'error' => array('attributes' => array('style' => 'display:none')),
        'label'=>false, 'class'=>'form-control','placeholder'=>'Enter phone','readonly'=>true));	
	?>
            </div>
        </div>        
        <div class="form-group">
            <label for="UserFname" class="control-label col-xs-2">Address</label>
            <div class="col-xs-10">
             <?php		
	echo $this->Form->input('User.address', array(
        'after'=>$this->Form->error('User.address', array(), array('wrap' => 'span', 'class' => 'help-inline')),
        'error' => array('attributes' => array('style' => 'display:none')),
        'label'=>false, 'class'=>'form-control','placeholder'=>'Enter address','readonly'=>true));	
	?>
            </div>
        </div>
        
        <div class="form-group">
            <label for="UserFname" class="control-label col-xs-2">State No</label>
            <div class="col-xs-10">
             <?php			
	echo $this->Form->input('User.state_no', array(
        'after'=>$this->Form->error('User.State No.', array(), array('wrap' => 'span', 'class' => 'help-inline')),
        'error' => array('attributes' => array('style' => 'display:none')),
        'label'=>false, 'class'=>'form-control','placeholder'=>'Enter state no','readonly'=>true));	
	?>
            </div>
        </div>
        <div class="form-group">
            <label for="UserFname" class="control-label col-xs-2">State Exp Date</label>
            <div class="col-xs-10">
              <?php			
	echo $this->Form->input('User.state_exp_date', array(
        'after'=>$this->Form->error('User.state_exp_date', array(), array('wrap' => 'span', 'class' => 'help-inline')),
        'error' => array('attributes' => array('style' => 'display:none')),
        'label'=>false, 'class'=>'form-control','placeholder'=>'Enter state exp date','type'=>'text','readonly'=>true));
	?>
            </div>
        </div>
        <div class="form-group">
            <label for="UserFname" class="control-label col-xs-2">DEA No</label>
            <div class="col-xs-10">
              <?php			
    echo $this->Form->input('User.den_no', array(
        'after'=>$this->Form->error('User.den_no', array(), array('wrap' => 'span', 'class' => 'help-inline')),
        'error' => array('attributes' => array('style' => 'display:none')),
        'label'=>false, 'class'=>'form-control','placeholder'=>'Enter den no','readonly'=>true));	
		
		
	?>
            </div>
        </div>
        <div class="form-group">
            <label for="UserFname" class="control-label col-xs-2">DEA Exp Date</label>
            <div class="col-xs-10">
              <?php			
    echo $this->Form->input('User.den_exp_date', array(
        'after'=>$this->Form->error('User.den_exp_date', array(), array('wrap' => 'span', 'class' => 'help-inline')),
        'error' => array('attributes' => array('style' => 'display:none')),
        'label'=>false, 'class'=>'form-control','placeholder'=>'Enter den exp date','type'=>'text','readonly'=>true));	
		
		
	?>
            </div>
        </div>
        <div class="form-group">
            <label for="UserFname" class="control-label col-xs-2">Fax</label>
            <div class="col-xs-10">
             <?php			
    echo $this->Form->input('User.fax', array(
        'after'=>$this->Form->error('User.fax', array(), array('wrap' => 'span', 'class' => 'help-inline')),
        'error' => array('attributes' => array('style' => 'display:none')),
        'label'=>false, 'class'=>'form-control','placeholder'=>'Enter fax','readonly'=>true));	
		
		
	?>
            </div>
        </div>
        <div class="form-group">
            <label for="UserFname" class="control-label col-xs-2">City</label>
            <div class="col-xs-10">
             <?php			
    echo $this->Form->input('User.city', array(
        'after'=>$this->Form->error('User.city', array(), array('wrap' => 'span', 'class' => 'help-inline')),
        'error' => array('attributes' => array('style' => 'display:none')),
        'label'=>false, 'class'=>'form-control','placeholder'=>'Enter city','readonly'=>true));	
		
		
	?>
            </div>
        </div>
        <div class="form-group">
            <label for="UserFname" class="control-label col-xs-2">State</label>
            <div class="col-xs-10">
             <?php			
    echo $this->Form->input('User.state', array(
        'after'=>$this->Form->error('User.state', array(), array('wrap' => 'span', 'class' => 'help-inline')),
        'error' => array('attributes' => array('style' => 'display:none')),
        'label'=>false, 'class'=>'form-control','placeholder'=>'Enter state','readonly'=>true));	
		
		
	?>
            </div>
        </div>
        <div class="form-group">
            <label for="UserFname" class="control-label col-xs-2">Zip Code</label>
            <div class="col-xs-10">
              <?php			
    echo $this->Form->input('User.zip_code', array(
        'after'=>$this->Form->error('User.zip_code', array(), array('wrap' => 'span', 'class' => 'help-inline')),
        'error' => array('attributes' => array('style' => 'display:none')),
        'label'=>false, 'class'=>'form-control','placeholder'=>'Enter zip code','readonly'=>true));	
		
		
	?>
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-xs-offset-2 col-xs-10">
                <div class='notifications alert-message warning top-right'></div>
    <button class="btn btn-warning show-notification">Change Password</button>
            </div>
        </div>
    </form>
    </div>
        
   
    <!--<?php echo $this->Form->create('Customer', array('controller'=>'Customers','action' => 'profile','class'=>'paraform')); ?>  
    <div class="clearfix gap20"></div>   
    <div class="clearfix gap20"></div>   
    <div class="clearfix gap20"></div>         
	<?php			
    echo $this->Form->input('User.password', array('div'=>'clearfix',
        'before'=>'<div class="col-lg-4"><label>'.__('Password').' : </label></div> <div class="col-lg-4">',
        'after'=>$this->Form->error('User.password', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
        'error' => array('attributes' => array('style' => 'display:none')),
        'label'=>false, 'class'=>'form-control','placeholder'=>'Enter Password'));	
		
		
	?><div class="clearfix gap20"></div>         
	<?php			
    echo $this->Form->input('User.password2', array('div'=>'clearfix',
        'before'=>'<div class="col-lg-4"><label>'.__('Confirm Password').' : </label></div> <div class="col-lg-4">',
        'after'=>$this->Form->error('User.password2', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
        'error' => array('attributes' => array('style' => 'display:none')),
        'label'=>false, 'class'=>'form-control','placeholder'=>'Confirm Password'));	
		
		
	?> 
    <div class="clearfix text-center">
    <div class="clearfix gap20"></div>  
    <?php echo $this->Form->input('User.id');?>
	<?php echo $this->Form->submit(__('Submit'), array('class'=>'btn btn-warning', 'div'=>false));?> 
    <?php echo $this->Form->reset(__('Cancel'), array('value'=>'Reset Password','class'=>'btn btn-warning', 'div'=>false));?> 
    </div>
	<?php echo $this->Form->end(); ?>-->
     
</div>
 
<script>

$('.show-notification').click(function(e) {
    $('.top-right').notify({
    message: { text: 'Please Contact Customer Care! ' }
  }).show();
});

$(document).ready(function(){
	
	$("#CustomerLoginForm").validate({

		rules: {
			"data[User][password]":{
				required: true,
				minlength:5,

				},
			"data[User][password2]":{
				required: true,
				minlength:5,
				equalTo: "#UserPassword"

				}
		},
		messages: {
			"data[User][password]":"Please check this field",
			"data[User][password2]":{equalTo:"make sure both match"}
		},
		errorElement:"span"

		});
		
	
});
</script>
