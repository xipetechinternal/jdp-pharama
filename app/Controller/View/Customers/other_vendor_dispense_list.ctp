<?php echo $this->Html->script('https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js', array('inline' => true)); ?>
<?php
echo $this->Html->css('https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css');
?>



<div class="clearfix"></div>

<div class="row">
    <div class="col-lg-12"> 
        <?php echo $this->Session->flash(); ?>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->Html->url('/customers/index'); ?>">Home</a></li>
            <li class="active">Vendor Dispense List</li>
        </ol>
        <div class="clearfix"></div>
        <div class="gap20"></div>





    </div>
    <div class="col-lg-12">
        <div class="row">
            <div class="col-sm-5 text-left">
                <a class="btn btn-info " href="<?php echo $this->Html->url('/customers/dispense'); ?>">Back</a> 
            </div>
            <div class="col-sm-7 text-right">
                <span id="fromid"></span>
                <span id="toid"></span>
            </div>
        </div>
    </div>
    
    <style type="text/css">
        #fromid, #toid{
            width: 100%;
            max-width: 160px;
            display: inline-block;
            vertical-align: middle;
        }
        
        .dataTables_filter{ display: none;}
        .paginate_enabled_previous:hover, .paginate_enabled_next:hover,
        .paginate_disabled_previous:hover, .paginate_disabled_next:hover{
             text-decoration: none;
        }
        
        .paginate_enabled_previous, .paginate_enabled_next{
            background: #EB7228;
            text-decoration: none;
            min-width: 80px;
            color: #333;
            display: inline-block;
            padding: 3px 10px 3px 10px;
            cursor: pointer;
            margin-right: 5px;
            margin-left: 5px;
            margin-top: 10px;
        }
        
        .paginate_disabled_previous, .paginate_disabled_next{
            background: #ccc;
            text-decoration: none;
            min-width: 80px;
            color: #333;
            display: inline-block;
            padding: 3px 10px 3px 10px; 
            cursor: not-allowed;
            margin-left: 5px;
            margin-right: 5px;
            margin-top: 10px;
        }
    </style>
    <div class="col-lg-12 text-center">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Vendor Dispense List</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">

                </div>


                <table id="example" class="datatable display table table-striped table-bordered" width="100%" >
                    <thead>
                        <tr>
                            <th><strong><?php echo __('Product NDC'); ?></strong></th>
                            <th><strong><?php echo __('Product Name'); ?></strong></th>
                            <th><strong><?php echo __('Product Vendor'); ?></strong></th>
                            <th><strong><?php echo __('Dispense'); ?></strong></th>
                            <th><strong><?php echo __('Created'); ?></strong></th>
                            <th><strong><?php echo __('Action'); ?></strong></th>
                        </tr>
                    </thead>
                    <?php if (empty($users)): ?>
                        <tr>
                            <td colspan="8" class="striped-info">History not available.</td>
                        </tr>
                    <?php endif; ?>

                    <tbody>

                        <?php
                        $i = 0;
                        $product_price = 0;
                        foreach ($users as $user):
                            $class = ' class="odd"';
                            if ($i++ % 2 == 0) {
                                $class = ' class="even"';
                            }
                            ?>
                            <tr <?php echo $class; ?> > 
                                <td><?php echo h($user['Product NDC']); ?> &nbsp;</td>
                                <td><?php echo h($user['Product Name']); ?>&nbsp;</td>
                                <td><?php echo h($user['Vendor Name']); ?> &nbsp;</td>
                                <td><?php echo $user['Quantity'] ?> </td>
                                <td><?php echo h(date("m-d-Y", strtotime($user['created']))); ?> &nbsp;</td> 
                                <td><?php echo $this->Form->postLink($this->Html->tag('i', 'Delete', array('class' => 'icon-remove icon-white')), array('action' => 'dispense_list_delete', $user['id'], $user['Product NDC']), array("class" => "btn btn-danger btn-mini tooltip-top", "data-original-title" => "Delete", 'escape' => false), __('Are you sure you want to delete # %s?', $user['Product NDC'])); ?></td>

                            </tr>
<?php endforeach; ?>                
                    </tbody>

                </table>

            </div>
            <div class="clearfix"></div>
            <div class="gap20"></div>



        </div>
    </div>
</div>

<?php echo $this->Html->script("https://jquery-datatables-column-filter.googlecode.com/svn/trunk/media/js/jquery.dataTables.js"); ?>
<?php echo $this->Html->script("https://jquery-datatables-column-filter.googlecode.com/svn/trunk/media/js/jquery.dataTables.columnFilter.js"); ?>
<script>
 $(document).ready(function () {
     
  $.datepicker.regional[""].dateFormat = 'mm-dd-yy';
  $.datepicker.regional[""].changeMonth = true;
  $.datepicker.regional[""].changeYear = true;

  $.datepicker.setDefaults($.datepicker.regional['']);

    $('#example').dataTable({            
        "bLengthChange": false,
    }).columnFilter({sPlaceHolder: "head:before",
        aoColumns: [
            { type: "null"},
            { type: "null"},
            { type: "null"},
            { type: "null"},
            {type: "date-range"}
        ]

    });
    
    var $customFrom = $('.filter_date_range').find("#example_range_from_4");
    $('#example_range_from_4').attr({ placeholder: "From : MM-DD-YY"});
    $('#fromid').html($customFrom);

    
    var $customTo = $('.filter_date_range').find("#example_range_to_4");
        $('#example_range_to_4').attr({ placeholder: "To : MM-DD-YY"});
        $('#toid').html($customTo);

    
    $('.filter_date_range').html('Date');

 });
</script> 
<script>

    $(document).ready(function () {
        $("#OrderHistoryDateTime").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
        });
        $('#OrderHistoryProductNdc').on('blur mouseup', function () {
            var products_jshon =<?php echo json_encode($product_list); ?>;
            $.each(products_jshon, function (i, v) {
                console.log(i, v);
                if (i == $("#OrderHistoryProductNdc").val()) {

                    $("#OrderHistoryProdid").val(i);
                    return;
                }
            });
        });

        $("#OrderHistoryProductNdc").autocomplete(
                {
                    source:<?php echo json_encode($product_list_auto); ?>,
                    select: function (event, ui) {
                        $("#OrderHistoryProductNdc").val(ui.item.label);
                        $("#OrderHistoryProdid").val(ui.item.value);
                        return false;
                    }
                }).data("autocomplete")._renderItem = function (ul, item) {
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a><strong>" + item.label + "</strong> </a>")
                    .appendTo(ul);
        };

        $("#ProductInfoAdminSalesReportPersonForm").validate();
        //$( "#ProductInfoSelfrmdate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
        // $( "#ProductInfoSeltodate" ).datepicker({dateFormat: 'yy-mm-dd' }); 





    });
</script>


<?php if(!empty($this->params["pass"]["1"])){?>
<script>
    
    setTimeout(function(){
        var from = "<?php echo date("m-d-Y",strtotime($this->params["pass"]["1"]))?>";
        var b =  $("#example_range_from_4").val(from);
        $("#example_range_from_4").trigger('change');
    
    }, 1000);
    
    
</script>
<?php }?>

<?php if(!empty($this->params["pass"]["2"])){?>
<script>
    setTimeout(function(){
        var to = "<?php echo date('m-d-Y',strtotime($this->params["pass"]["2"]))?>";
        $("#example_range_to_4").attr('value',to);
         $("#example_range_to_4").trigger('change');
        
    
    }, 1000);
   
</script>
<?php }?>