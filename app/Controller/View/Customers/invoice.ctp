<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Invoice</title>
    <?php echo $this->Html->css(array('bootstrap.min'));?>
    <?php echo $this->Html->script(array('jquery-1.8.3.min'));?>
   
    <style>
      @import url(http://fonts.googleapis.com/css?family=Bree+Serif);
      body, h1, h2, h3, h4, h5, h6{
      font-family: 'Bree Serif', serif;
      }
   .inv-txt{
    font-family:sans-serif; padding: 5px 2px 2px 12px;font-size: 9px !important; font-weight: normal;font-family: sans-serif !important;
}
.panel-body{
    padding-top: 6px;
    
}
.table thead>tr>th, .table tbody>tr>th, .table tfoot>tr>th, .table thead>tr>td, .table tbody>tr>td, .table tfoot>tr>td{
    padding: 5px !important;
}
h2{
    margin-bottom: 5px;
}
h4{
	font-size:14px;
}
.container{
	font-size:11px;
}

    
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto;
	 -webkit-region-break-inside: avoid;}
    thead { display:table-header-group }
    tfoot { display:table-footer-group }

    </style>
    <script type="text/javascript" src="/js/wkhtmltopdf_tableSplitHack.js"></script>
  </head>
  
  <body>
    <div class="container">
      <div class="row">
        <!--<div class="col-xs-5">
          <h1 class="col-xs-8 text-center">
            <img src="<?php echo $this->Html->url('/img/logo.jpg');?>" alt="" class="img-responsive">
          </h1>
        </div>-->
        <div class="col-xs-12 text-center">
         <div style="max-width:300px; margin:0 auto;">
         <br />
        	 <img style="max-width:300px" src="<?php echo $this->Html->url('/img/logo.jpg');?>" alt="" class="img-responsive">
          </div>
          <h3 class="text-center">INVOICE</h3>
          <div class="text-right"><small>Date :  <?php echo date('m/d/Y',strtotime($Orderdetail['Order']['createddt']));?> </small> <br /><small>INVOICE/ORDER NO :  <?php echo $Orderdetail['Order']['tmporderid'];?></small></div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-5">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h5>From: <a href="#">JDPharmaceutical Inc.</a></h5>
            </div>
            <div class="panel-body">
             
                6034 San Fernando Road
                Glendale, CA 91202<br/>
                State No: WLS 6124<br/>
                Tel.818.956.0578<br/>
                Fax.818.956.0721<br/>
                Email : info@jdpwholesaler.com<br/>
                Website : www.jdpwholesaler.com
              
            </div>
          </div>
        </div>
        <div class="col-xs-5 col-xs-offset-2 text-right pull-right">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h5>To : <a href="#"><?php echo $Orderdetail['User']['fname'];?></a></h5>
            </div>
            <div class="panel-body">
           
               <?php echo $Orderdetail['User']['address'];?>,<?php echo $Orderdetail['User']['city'];?>, 
               <?php echo $Orderdetail['User']['state'];?>,<?php echo $Orderdetail['User']['zip_code'];?><br />
                 
                State No : <?php echo $Orderdetail['User']['state_no'];?>
                 <br />
                State Exp : <?php echo $Orderdetail['User']['state_no_exp'];?>
                <br />
                D.E.A No : <?php echo $Orderdetail['User']['den_no'];?>
                 <br />
                D.E.A. EXPIRATION DATE : <?php echo $Orderdetail['User']['den_no_exp'];?>
                 <br />
              <!-- Business name : <?php echo $Orderdetail['User']['contact_name'];?>
               <br />-->
                Tel : <?php echo $Orderdetail['User']['phone'];?>, Fax : <?php echo $Orderdetail['User']['fax'];?><br />
            
             
            </div>
          </div>
        </div>
      </div>
      <!-- / end client details section -->
      <table width="100%" border="10" cellspacing="10" cellpadding="0" class="table table-bordered print-friendly" >
        <thead>
          <tr>
            <th>
              <h4>Item(s)</h4>
            </th>
            <th>
              <h4>Description</h4>
            </th>
            <th>
              <h4>Qty</h4>
            </th>
            <th>
              <h4>Original Price</h4>
            </th>
             <th>
              <h4>Special Discount</h4>
            </th>
            <th>
              <h4>Price</h4>
            </th>
            <th>
              <h4>Sub Total</h4>
            </th>
          </tr>
        </thead>
       <tbody>

<?php if(empty($Orderdetail)): ?>
		<tr>
			<td colspan="7" class="striped-info">No record found.</td>
		</tr>
		<?php endif;  ?>
 				<?php $i=0;
				$slno=1;
				$product_price=0;
				foreach($Orderdetail['OrderdetailMany'] as $orderdeatils){
					$priice=$priice+($orderdeatils['price']*$orderdeatils['quantity']);
					$orginal_price=($orderdeatils['original_price']>0)?($orderdeatils['original_price']):$orderdeatils['price'];
				 ?>
                
  <tr nobr="true">
    <td nobr="true"><?php echo $orderdeatils['Product']['itemnumber']?></td>
   
    <td nobr="true" class="td-font"><?php echo $orderdeatils['Product']['productname']?></br><?php echo $orderdeatils['Product']['product_ndc']?></td>
    <td nobr="true" class="td-font" align="left"><?php echo $orderdeatils['quantity']?></td>
     <td nobr="true" class="td-font"><?php echo $orginal_price?></td>
    <td nobr="true" class="td-font"><?php echo $orderdeatils['special_discount']?>%</td>
    <td nobr="true" class="td-font"><?php echo $orderdeatils['price']?></td>
    <td nobr="true" class="td-font"><?php echo $orderdeatils['quantity']*$orderdeatils['price']?></td>

    
  </tr>
  <?php }?>
    
          
        </tbody>
        <tfoot>
        </tfoot>
      </table>
      
      <div class="clearfix"></div>
      <div class="row text-right ">
      <div class="col-xs-4 col-xs-offset-10"></div>
        <div class="col-xs-2 col-xs-offset-6">
          <p align="right">
            
          </p>
        </div>
        <div class="col-xs-6 text-right">
          <strong>
          Grand Total : $<?php echo $priice?> <br>
          </strong>
        </div>
       
      </div>
       <div class="clearfix"></div>
       <br />
        <div class="col-xs-12 col-xs-offset-12 text-right">
        <button type="button" class="btn btn-success print" data-dismiss="modal">Print</button> <br /><br />
        </div>
      
    </div>
    
    <script>
	$('.print').click(function(){
		$('.print').hide();
     window.print();
	 $('.print').show();
});</script>
  </body>
</html>