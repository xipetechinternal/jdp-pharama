
<div class="block-content collapse in">
<div class="span12">                            
<table class="table table-bordered">
    <tbody>
        <tr>
            <th><?php echo __('Product Stock ID'); ?></th>
            <td><?php echo h($product['ProductStock']['id']); ?></td>
        </tr>
       
        <tr>
            <th><?php echo __('Product Name'); ?></th>
            <td><?php echo h($product['Product']['product_name']); ?></td>
        </tr>
        
        <tr>
            <th><?php echo __('Product Price'); ?></th>
            <td><?php echo h($product['Product']['product_price']); ?></td>
        </tr>
        <tr>
            <th><?php echo __('Item No.'); ?></th>
            <td><?php echo h($product['Product']['item_number']); ?></td>
        </tr>         
        <tr>
            <th><?php echo __('Threshold'); ?></th>
            <td><?php echo h($product['Product']['threshold']); ?></td>
        </tr>
        
        <tr>
            <th><?php echo __('Lot No'); ?></th>
            <td><?php echo h($product['ProductStock']['lot_no']); ?></td>
        </tr>
        
        <tr>
            <th><?php echo __('No Of Quantity'); ?></th>
            <td><?php echo h($product['ProductStock']['no_of_quantity']); ?></td>
        </tr>
        
        <tr>
            <th><?php echo __('Mfg Date'); ?></th>
            <td><?php echo h(date("j M Y ",strtotime($product['ProductStock']['mfg_date']))); ?></td>
        </tr>
        
       <tr>
            <th><?php echo __('Exp Date'); ?></th>
            <td><?php echo h(date("j M Y ",strtotime($product['ProductStock']['exp_date']))); ?></td>
        </tr>
        <tr>
            <th><?php echo __('Created'); ?></th>
            <td><?php echo h(date("j M Y , g:i A",strtotime($product['ProductStock']['created']))); ?></td>
        </tr>
        
    </tbody>
</table> 
</div>
</div>
