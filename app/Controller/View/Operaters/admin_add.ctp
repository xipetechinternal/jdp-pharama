<?php $this->set('title_for_layout', __('Operaters',true)); ?>
<?php $this->Html->addCrumb(__('Operaters',true), '/admin/sales/index',array('tag'=>'li'));  ?>
<?php $this->Html->addCrumb($this->Html->tag('li',__('Add',true),array('class'=>'active')),false,false); ?>

<div class="row-fluid">
<div class="block">
<div class="navbar navbar-inner block-header">
<div class="muted pull-left"><?php echo __('Operaters User'); ?></div>
</div>
<div class="block-content collapse in">
<div class="span12">
<?php echo $this->Form->create('Operater',array('controller'=>'operaters','action' => 'admin_add','class' => 'form-horizontal'));?>

           <div class="row">
                  <div class="col-md-6 ">
                   <?php echo $this->Form->input('username',array('placeholder' => 'Username',
        'label' => 'Username',
        'class'=>'form-control'));?> 
                  </div>
                  <div class="col-md-6 ">
                    <?php echo $this->Form->input('state_no',array('placeholder' => 'State No.',
        'label' => 'State No.',
        'class'=>'form-control'));?>
                  </div>
          </div>
          <div class="row">
                  <div class="col-md-6 form-group changepassword" <?php if($id!=NULL){?> style="display:none" <?php } ?>>
                   <?php echo $this->Form->input('password',array('placeholder' => 'Password',
       'label' => 'Password',
        'class'=>'form-control'));?> 
                  </div>
                  <div class="col-md-6 form-group">
                   <?php echo $this->Form->input('state_no_exp',array('placeholder' => 'State Exp',
       'label' => 'State Exp','type' => 'text',
        'class'=>'form-control'));?> 
                  </div>
          </div>
          <div class="row">
                  <div class="col-md-6 form-group changepassword" <?php if($id!=NULL){?> style="display:none" <?php } ?>>
                      <?php echo $this->Form->input('password2',array('placeholder' => 'Confirm Password',
       'label' => 'Confirm Password',
		'type'=>'password',
		'required'=>'required',
        'class'=>'form-control'));?> 
                  </div>
                  <div class="col-md-6 form-group">
                    <?php echo $this->Form->input('den_no',array('placeholder' => ' D.E.A No.',
       'label' => ' D.E.A No.',
        'class'=>'form-control'));?> 
                   
                  </div>
          </div>
          <div class="row">
                  <div class="col-md-6 form-group">
                     <?php echo $this->Form->input('fname',array('placeholder' => 'First Name',
       'label' => 'First Name',
	   'required'=>'required',
        'class'=>'form-control'));?> 
                  </div>
                  <div class="col-md-6 form-group">
                   <?php echo $this->Form->input('den_no_exp',array('placeholder' => ' D.E.A Exp',
       'label' => ' D.E.A Exp',
		'type'=>'text',
        'class'=>'form-control'));?> 
                  
                  </div>
         </div>
         <div class="row">
                  <div class="col-md-6 form-group">
                   <?php echo $this->Form->input('lname',array('placeholder' => 'Last Name',
       'label' => 'Last Name',
	   'required'=>'required',
        'class'=>'form-control'));?> 
                   
                  </div>
                  <div class="col-md-6 form-group">
                     <?php echo $this->Form->input('email',array('placeholder' => 'Email',
       'label' => 'Email',
        'class'=>'form-control'));?> 
                  </div>
        </div>
        <div class="row">
                  <div class="col-md-6 form-group">
                   <?php echo $this->Form->input('phone',array('placeholder' => 'Phone',
       'label' => 'Phone',
        'class'=>'form-control'));?> 
                  </div>
                  <div class="col-md-6 form-group">
                   
                    <?php echo $this->Form->input('fax',array('placeholder' => 'Fax',
       'label' => 'Fax',
        'class'=>'form-control'));?> 
                  </div>
        </div>
        <div class="row">
                  <div class="col-md-6 form-group">
                   <?php echo $this->Form->input('address',array('placeholder' => 'Address',
       'label' => 'Address',
        'class'=>'form-control'));?> 
                  </div>
                  <div class="col-md-2 form-group">                  
                    <?php echo $this->Form->input('city',array('placeholder' => 'City',
       'label' => 'City',
        'class'=>'form-control'));?> 
                  </div>
        
                  <div class="col-md-2 form-group">
                   <?php echo $this->Form->input('state',array('placeholder' => 'State',
       'label' => 'State',
        'class'=>'form-control'));?> 
                    
                  </div>
                  <div class="col-md-2 form-group">
                  <?php echo $this->Form->input('zip_code',array('placeholder' => 'Zip Code',
       'label' => 'Zip Code',
        'class'=>'form-control'));?> 
                    
                  </div>
         </div>
         <div class="row">
                  <div class="col-md-6 form-group">
                  
    				<?php echo $this->Form->input('sid',array('empty'=>array(''=>'--Select Name--'),'options' => $sales,
       'label' => 'Sales Name',
	   'required'=>'required',
        'class'=>'form-control'));?>
    
                    
                  </div>
                   <div class="col-md-2 form-group">
                   <?php echo $this->Form->input('Brand_discount',array('title' => 'Brand Discount In %','placeholder' => 'Brand Discount In %',
       'label' => 'Brand Discount In %',
        'class'=>'form-control'));?>
                    
                  </div>
				 <div class="col-md-2 form-group">
                   <?php echo $this->Form->input('Generic_discount',array('title' => 'Generic Discount %','placeholder' => 'Generic Discount In %',
       'label' => 'Generic Discount %',
        'class'=>'form-control'));?> 
                    
                  </div>
				 <div class="col-md-2 form-group">
                   <?php echo $this->Form->input('OTC_discount',array('title' => 'OTC Discount In %','placeholder' => 'OTC Discount In %',
       'label' => 'OTC Discount In %',
        'class'=>'form-control'));?> 
                    
                  </div>
			<div class="col-md-6 form-group">
                   <?php echo $this->Form->input('Refrigirated_discount',array('title' => 'Refrigirated Discount In %','placeholder' => 'Refrigirated Discount In %',
       'label' => 'Refrigirated Discount In %',
        'class'=>'form-control'));?> 
                    
                  </div>
          <div class="col-md-6 form-group">
                   <?php echo $this->Form->input('CUSTOMER_NUMBER',array('title' => 'CUSTOMER NUMBER','placeholder' => 'CUSTOMER NUMBER',
	   'required'=>'required',
       'label' => 'CUSTOMER #',
        'class'=>'form-control'));?> 
                    
                  </div>
                  <!--<div class="col-md-6 form-group">
                   <?php echo $this->Form->input('member_status',array('options' => array(''=>'--Select Member Type--',1=>'Gold',2=>'Silver',3=>'Bronze'),
       'label' => false,
	   'required'=>'required',
        'class'=>'form-control'));?>
                  </div>-->
                </div>
                  <div class="gap20"></div>
                  <div class="row">
                  <div class="col-md-6 form-group">
                   <?php echo $this->Form->input('active',array('type' => 'hidden','value'=>'Y'));?> 
                  <?php echo $this->Form->input('level',array('type' => 'hidden','value'=>'client'));?>
                  <?php echo $this->Form->input('status',array('type' => 'hidden','value'=>'1'));?> 
                   <?php echo $this->Form->input('createddt',array('type' => 'hidden','value'=>date("Y-m-d H:i:s")));?>
                    <?php echo $this->Form->input('id');?>
<?php echo $this->Form->submit(__('Submit'), array('class'=>'btn btn-primary', 'div'=>false));?>&nbsp;
<?php echo $this->Form->reset(__('Cancel'), array('class'=>'btn', 'div'=>false));?>
<?php if($id!=NULL){?>
&nbsp;<?php echo $this->Form->reset(__('Change Password'), array('value'=>'Change Password','class'=>'btn btn-warning','onclick'=>"$('.changepassword').show();",'type'=>'button','div'=>false));?>
<?php } ?>
                  </div>
                  </div>
               <?php echo $this->Form->end();?>

</div>
</div>
</div>
</div>
<script>
$(document).ready(function(){
	$("#UserAdminClientUsersForm").validate({

		rules: {
			"data[User][password]":{
				required: true,
				minlength:5,

				},
			"data[User][password2]":{
				required: true,
				minlength:5,
				equalTo: "#UserPassword"

				}
		},
		messages: {
			"data[User][password]":"Please check this field",
			"data[User][password2]":{equalTo:"make sure both match"}
		},
		errorElement:"span"

		});
		
		 $( "#Datepicker1" ).datepicker({dateFormat: 'yy-mm-dd' }); 
   $( "#UserStateNoExp" ).datepicker({dateFormat: 'yy-mm-dd' }); 
    $( "#UserDenNoExp" ).datepicker({dateFormat: 'yy-mm-dd' }); 
});
</script>