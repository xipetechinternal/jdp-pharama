<!--  start message-red -->
<div class="alert alert-error">
<button class="close" data-dismiss="alert">&times;</button>
<strong>Error!</strong> <?php echo $message; ?>
</div>
<!--  end message-red -->