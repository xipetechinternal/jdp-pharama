<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ProductStocksController extends AppController {

/**
* Controller name
*
* @var string
*/

public $name = 'ProductStocks';

/**
* This controller does not use a model
*
* @var array
*/

public $uses = array('ProductStock','Product');

   function beforeFilter() {
        parent::beforeFilter();
			$this->ProductStock->bindModel(array('belongsTo'=>array(
            'Product' => array(
                'className' => 'Product',
                'foreignKey' => 'product_id',
                'dependent'=>true
            )
       	 )), false);
		
   }


	
	/**
	* Index Dashboard
	*
	* @return void
	*/
	 
	function admin_index(){	
				 
		$this->layout = "admin_dashboard";   
		$this->set('title', __('ProductStocks'));
		$this->ProductStock->recursive = 1;
        $this->set('products', $this->paginate("ProductStock"));
	 
	}
	 
	/**
	 * add method
	 *
	 * @return void
	 */
	function admin_add(){	
				 
		$this->layout = "admin_dashboard";   
		$this->set('title', __('ProductStocks'));
		if ($this->request->is('post')) {
			$this->loadModel('ProductStock');
			$this->ProductStock->create();
			if ($this->ProductStock->save($this->request->data)) {
				$this->Session->setFlash(__('The Product Stock has been saved'), 'admin_success');
				$this->redirect(array('controller'=>'products','action' => 'admin_index'));
			} else {
				$this->Session->setFlash(__('The Product Stock could not be saved. Please, try again.'), 'admin_error');
			}
		}	
		$this->set('products', $this->Product->find('list',array('fields'=>array('id','product_name'),'condition'=>array('Product.status'=>1))));
	 
	}


	 /**
     * edit method
     *
     * @param string $id
     * @return void
     */
    
	  public function admin_edit($id = null) {		
		$this->layout = "admin_dashboard";		
        $this->ProductStock->id = $id;
        if (!$this->ProductStock->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->ProductStock->save($this->request->data)) {
                $this->Session->setFlash(__('The Product Stock has been saved'), 'admin_success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The Product Stock could not be saved. Please, try again.'), 'admin_error');
            }
        } else {
            $this->request->data = $this->ProductStock->read(null, $id);
        }
      $this->set('products', $this->Product->find('list',array('fields'=>array('id','product_name'),'condition'=>array('Product.status'=>1))));
    }
	





	/**
     * delete method
     *
     * @param string $id
     * @return void
     */


	public function admin_delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->ProductStock->id = $id;
        if (!$this->ProductStock->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->ProductStock->delete()) {
            $this->Session->setFlash(__('Product Stock deleted'), 'admin_success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Product Stock was not deleted'), 'admin_error');
        $this->redirect(array('action' => 'index'));
    }

    /**
     *  Active/Inactive ProductStock
     *
     * @param <int> $user_id
     */
    public function toggle($user_id, $status) {
        $this->layout = "ajax";
        $status = ($status) ? 0 : 1;
        $this->set(compact('user_id', 'status'));
        if ($user_id) {
            $data['ProductStock'] = array('id'=>$user_id, 'status'=>$status);
            $allowed = $this->ProductStock->saveAll($data["ProductStock"], array('validate'=>false));           
        } 
    }
	 
	 public function admin_view($id = null) {
		$this->layout = "ajax";		
        $this->set('title', __('View'));
        $this->ProductStock->id = $id;
        if (!$this->ProductStock->exists()) {
            throw new NotFoundException(__('Invalid user'), 'error');
        }
        $this->set('product', $this->ProductStock->read(null, $id));
     }
	  
	 
	 

}
