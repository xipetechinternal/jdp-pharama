<?php

App::uses('CakeTime', 'Utility');
App::uses('AppController', 'Controller');

App::uses('CakeEmail', 'Network/Email');

/**

 * Users Controller

 *

 * @property User $User

 */
class UsersController extends AppController {

    public $components = array('RequestHandler', 'Cookie', 'Session', 'Export');
    public $uses = array('User', 'EmailTemplateDescription', 'Order', 'Orderdetail', 'OrderLot', 'ProductInfo', 'Pedigree', 'ProductActivity', 'OrderDist', 'CustomerField', 'CreditReturn', 'Products');
    public $paginate = array('limit' => 10);

    function beforeFilter() {

        parent::beforeFilter();

        $this->layout = "default";

        $this->Auth->allow('admin_login');

        //	$this->Auth->userScope = array('User.group_id' => 1);

        $this->Auth->loginAction = '/admin/users/login';

        $this->Auth->logoutRedirect = '/admin/users/login';

        $this->Auth->loginRedirect = array('plugin' => false, 'controller' => 'users', 'action' => 'index', 'admin' => true);

        /*   $this->User->bindModel(array('belongsTo'=>array(

          'Group' => array(

          'className' => 'Group',

          'foreignKey' => 'group_id',

          'dependent'=>true

          )

          )), false); */
    }

    public function isAuthorized($user) {
        $this->Auth->autoRedirect = false;
        // All registered users can add posts
        if (isset($user['level']) && $user['level'] === 'admin') {
            return true; //Admin can access every action
        }
        $this->Session->setFlash('Sorry, you don\'t have permission to access that page.');
        $this->redirect('login');
        return false;
        return parent::isAuthorized($user);
    }

    /**

     * login method

     *

     * @return void

     */
    function admin_login() {

        $this->layout = "admin_login";

        $this->set('title_for_layout', 'Admin Panel');

        if ($this->Auth->user() and $this->Session->read('Auth.Admin.level') == 'admin') {



            $this->redirect(array('action' => 'admin_index'));
        } else if ($this->request->is('post')) {



            if ($this->Auth->login()) {
                if ($this->Session->read('Auth.Admin.level') == 'admin')
                    $this->redirect($this->Auth->redirect());
            } else {

                $this->Session->setFlash('<div class="alert alert-error"><strong>Error ! </strong> Your username or password was incorrect.</div>', 'default');
            }
        }
    }

    /**

     * logout method

     *

     * @return void

     */
    function admin_logout() {

        $this->Session->setFlash('Good-Bye', 'success');

        $this->redirect($this->Auth->logout());
    }

    /**

     * index method

     *

     * @return void

     */
    public function admin_index() {

        $this->layout = "admin_dashboard";

        $this->set('title', __('Users'));

        $this->set('description', __('Manage Users'));



        $this->User->recursive = 1;
		ini_set('max_execution_time', 300);
		ini_set('memory_limit', '-1');
		Configure::write('debug', 2);



        // $this->set('users', $this->paginate("Order",array('NOT'=>array('User.id' => $this->Auth->user('id')),'User.group_id'=>1)));
        $slaesmalist = $this->User->find('all', array('conditions' => array('level' => 'sales')));
        $salesman_array = array();
        foreach ($slaesmalist as $salesmant) {
            $salesman_array[$salesmant['User']['id']] = $salesmant['User'];
        }

        $this->set('salesman', $salesman_array);
		

        //calculate in stock 
        $productinfo = $this->ProductInfo->find('all', array('fields' => 'SUM(Product.PurchasePrice*ProductInfo.availability) as instock,SUM(ProductInfo.availability) as quantity,Product.producttype',
            'conditions' => array("ProductInfo.availability >'0'", "Product.active!='False'"),
            'group' => array('Product.producttype'),
            'order' => array('ProductInfo.createddt' => 'desc')));
        //print_r($productinfo);
        //calculae total sell
        $totalsell = 0;
        $sellquentity = 0;
        /*$totalUnpaid = $this->OrderDist->find('all', array('fields' => array('OrderDist.tmporderid'), 'order' => array(
                'OrderDist.id' => 'desc'), 'conditions' => array('OrderDist.status' => array('Accep'))));

        foreach ($totalUnpaid as $padamounts) {
            foreach ($padamounts['OrderdetailMany'] as $pdamount) {
                $totalsell = ($pdamount['quantity'] * $pdamount['price']) + $totalsell;
                $sellquentity = $pdamount['quantity'] + $sellquentity;
            }
        }*/
        $this->set('instock', $productinfo);
       // $this->set('instockquantity', $productinfo[0][0]['quantity']);
        //$this->set('totalsell', $totalsell);
        //$this->set('sellquentity', $sellquentity);
		$NewDate=Date('Y-m-d', strtotime("-3 days"));
        $this->paginate = array('order' => array(
                'Order.createddt' => 'desc'),
				'limit'=>1000,
            'conditions' => array('Order.status' => 'con','Order.createddt >' => $NewDate), 'fields' => array('DISTINCT Order.id', 'Order.tmporderid', 'Order.userid', 'Order.createddt', 'Order.status', 'User.username', 'User.id', 'User.sid', 'User.fname', 'User.email', 'User.phone', 'User.address', 'User.city', 'User.state', 'User.zip_code')
        );
        $this->set('users', $this->paginate("Order"));
        $this->set('admincount', $this->User->find('count', array('conditions' => array('level' => 'admin'))));
        $this->set('salescount', $this->User->find('count', array('conditions' => array('level' => 'sales'))));
        $this->set('dataentrycount', $this->User->find('count', array('conditions' => array('level' => 'data'))));
        $this->set('clientcount', $this->User->find('count', array('conditions' => array('level' => 'client'))));
    }

    public function admin_sales_users($id = NULL) {

        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));

        $this->User->id = $id;


        if ($this->request->is('put') || $this->request->is('post')) {

            $this->User->create();

            if ($this->User->save($this->request->data)) {


                $fname = $this->request->data['User']['fname'];
                $name = $this->request->data['User']['username'];
                $cpassword = $this->request->data['User']['password'];
                $cpassword = ($cpassword == '') ? "Old Password" : $cpassword;
                $email = $this->request->data['User']['email'];
                $message = <<<HTM
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
</HEAD>

<BODY>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>Dear $fname</td>
  </tr>
  <tr>
    <td>Thanks for choosing us. Your login details given below:<br>
Username : $name<br>
Password : $cpassword<br>
      Regards <br>

      </td>
  </tr>
</table>
</BODY>
HTM;

                /*                 * * Set to address here  ** */
                $to = $email;
                /*                 * * Set email subject here  ** */
                if ($id == NULL) {
                    $subject = 'Signup Confirmation - Admin';
                } else {
                    $subject = 'Update Confirmation - Admin';
                }

// To send HTML mail, the Content-type header must be set
                $headers = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
//$headers .= 'From: Testing <info@promptdesigners.com>' . "\r\n";
                $headers .= 'From: ' . $fname . ' <' . $email . '>' . "\r\n";

// Mail it
                mail($to, $subject, $message, $headers);


                /* $email = new CakeEmail();
                  try {
                  $email->config('default');
                  } catch (Exception $e) {
                  Throw new ConfigureException('Config in email.php not found. ' . $e->getMessage());
                  }

                  $Templates=$this->EmailTemplateDescription->find('all',array('conditions'=>array('EmailTemplateDescription.email_template_id' => 1)));
                  $TempContent=stripcslashes($Templates[0]['EmailTemplateDescription']['content']);
                  $TempContent=str_replace("{email}",$this->request->data["User"]["email"],$TempContent);
                  $TempContent=str_replace("{date}",date("Y-m-d"),$TempContent);
                  $TempContent=str_replace("{name}",$this->request->data["User"]['username'],$TempContent);
                  $TempContent=str_replace("{password}",$this->request->data["User"]['password'],$TempContent);
                  $TempContent=str_replace("../../..",'http://' . env('SERVER_NAME'),$TempContent);
                  $data=array('Message'=>$TempContent);
                  $email->template('default', 'default');
                  $email->emailFormat('both');
                  $email->viewVars(array('data' =>$data)) ;
                  $email->from(array('i-care@i-care.mobi' => 'JDPharmaceutical'));
                  $email->to($this->request->data["User"]["email"]);
                  $email->subject(__d('default', $Templates[0]['EmailTemplateDescription']['subject']));
                  $email->send(); */


                if ($id == NULL) {
                    $this->Session->setFlash(__('The user has been saved'), 'admin_success');
                } else {
                    $this->Session->setFlash(__('The user has been Update'), 'admin_success');
                }

                $this->redirect(array('action' => 'admin_sales_users'));
            } else {

                $this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'admin_error');
            }
        } else {
            $this->request->data = $this->User->read(null, $id);
            $this->request->data['User']['password'] = null;
        }


        $search = "";
        if (!isset($this->params['url']['ndcproduct'])) {
            $this->paginate = array(
                'limit' => 10,
                'conditions' => array('User.level' => 'sales')
            );
        } else {
            Configure::write('debug', 2);
            $search = $this->params['url']['ndcproduct'];
            $clients = $this->User->find("list", array("fields" => array("sid"), "conditions" => array("OR" => array("User.username like '%" . $search . "%'", "User.fname like '%" . $search . "%'"), "and" => array("level" => "client"))));
            $this->paginate = array(
                'limit' => 10,
                'conditions' => array('User.level' => 'sales', 'User.id' => $clients)
            );
        }
        $this->set('search', $search);






        $this->set('users', $this->paginate("User", array('User.level' => 'sales')));
        $this->set('id', $id);
    }

    public function admin_sales_users_delete($id = null) {

        if (!$this->request->is('post')) {

            throw new MethodNotAllowedException();
        }

        $this->User->id = $id;

        if (!$this->User->exists()) {

            throw new NotFoundException(__('Invalid user'));
        }

        if ($this->User->delete()) {

            $this->Session->setFlash(__('User deleted'), 'admin_success');

            $this->redirect(array('action' => 'admin_sales_users'));
        }

        $this->Session->setFlash(__('User was not deleted'), 'admin_error');

        $this->redirect(array('action' => 'admin_sales_users'));
    }

    public function admin_entry_users($id = NULL) {

        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));

        $this->User->id = $id;


        if ($this->request->is('put') || $this->request->is('post')) {

            $this->User->create();

            if ($this->User->save($this->request->data)) {

                $email = new CakeEmail();
                try {
                    $email->config('default');
                } catch (Exception $e) {
                    Throw new ConfigureException('Config in email.php not found. ' . $e->getMessage());
                }

                $Templates = $this->EmailTemplateDescription->find('all', array('conditions' => array('EmailTemplateDescription.email_template_id' => 1)));
                $TempContent = stripcslashes($Templates[0]['EmailTemplateDescription']['content']);
                $TempContent = str_replace("{email}", $this->request->data["User"]["email"], $TempContent);
                $TempContent = str_replace("{date}", date("Y-m-d"), $TempContent);
                $TempContent = str_replace("{name}", $this->request->data["User"]['username'], $TempContent);
                $TempContent = str_replace("{password}", $this->request->data["User"]['password'], $TempContent);
                $TempContent = str_replace("../../..", 'http://' . env('SERVER_NAME'), $TempContent);
                $data = array('Message' => $TempContent);
                $email->template('default', 'default');
                $email->emailFormat('both');
                $email->viewVars(array('data' => $data));
                $email->from(array('i-care@i-care.mobi' => 'JDPharmaceutical'));
                $email->to($this->request->data["User"]["email"]);
                $email->subject(__d('default', $Templates[0]['EmailTemplateDescription']['subject']));
                $email->send();



                $this->Session->setFlash(__('The user has been saved'), 'admin_success');
                $this->redirect(array('action' => 'admin_entry_users'));
            } else {

                $this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'admin_error');
            }
        } else {
            $this->request->data = $this->User->read(null, $id);
            $this->request->data['User']['password'] = null;
        }

        $this->User->recursive = 1;
        $this->set('id', $id);
        $this->set('users', $this->paginate("User", array('User.level' => 'data')));
    }

    public function admin_entry_users_delete($id = null) {

        if (!$this->request->is('post')) {

            throw new MethodNotAllowedException();
        }

        $this->User->id = $id;

        if (!$this->User->exists()) {

            throw new NotFoundException(__('Invalid user'));
        }

        if ($this->User->delete()) {

            $this->Session->setFlash(__('User deleted'), 'admin_success');

            $this->redirect(array('action' => 'admin_entry_users'));
        }

        $this->Session->setFlash(__('User was not deleted'), 'admin_error');

        $this->redirect(array('action' => 'admin_entry_users'));
    }

    public function admin_client_users($id = NULL) {
//Configure::write('debug',2);
        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));

        $sales = $this->User->find('list', array('fields' => array('username'), 'conditions' => array('User.level' => 'sales')));
        $this->set('sales', $sales);
        $this->User->id = $id;


        if ($this->request->is('put') || $this->request->is('post')) {

            $this->User->create();

            if ($this->request->data['User']['threpercenteoff'] == 0 or $this->request->data['User']['threpercenteoff'] == '') {
				if ($this->request->data['User']['threpercenteoff6'] == 0 or $this->request->data['User']['threpercenteoff6'] == '')
                $this->request->data['User']['threpercenteoff'] = $this->request->data['User']['threpercenteoff10'];
				else
				$this->request->data['User']['threpercenteoff'] = $this->request->data['User']['threpercenteoff6'];
            }
            if ($this->User->save($this->request->data)) {

                $email = new CakeEmail();
                try {
                    $email->config('default');
                } catch (Exception $e) {
                    Throw new ConfigureException('Config in email.php not found. ' . $e->getMessage());
                }

                $Templates = $this->EmailTemplateDescription->find('all', array('conditions' => array('EmailTemplateDescription.email_template_id' => 1)));
                $TempContent = stripcslashes($Templates[0]['EmailTemplateDescription']['content']);
                $TempContent = str_replace("{email}", $this->request->data["User"]["email"], $TempContent);
                $TempContent = str_replace("{date}", date("Y-m-d"), $TempContent);
                $TempContent = str_replace("{name}", $this->request->data["User"]['username'], $TempContent);
                $TempContent = str_replace("{password}", $this->request->data["User"]['password'], $TempContent);
                $TempContent = str_replace("../../..", 'http://' . env('SERVER_NAME'), $TempContent);
                $data = array('Message' => $TempContent);
                $email->template('default', 'default');
                $email->emailFormat('both');
                $email->viewVars(array('data' => $data));
                $email->from(array('i-care@i-care.mobi' => 'JDPharmaceutical'));
                $email->to($this->request->data["User"]["email"]);
                $email->subject(__d('default', $Templates[0]['EmailTemplateDescription']['subject']));
                $email->send();



                $this->Session->setFlash(__('The user has been saved'), 'admin_success');
                $this->redirect(array('action' => 'admin_client_users'));
            } else {

                $this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'admin_error');
            }
        } else {
            $this->request->data = $this->User->read(null, $id);
            $this->request->data['User']['password'] = null;
            $this->request->data['User']['threpercenteoff10'] = $this->request->data['User']['threpercenteoff'];
			$this->request->data['User']['threpercenteoff6'] = $this->request->data['User']['threpercenteoff'];
        }

        $this->User->recursive = 1;
        $this->set('id', $id);

        $users = $this->paginate("User", array('User.level' => 'client','User.Status' => 1));
        foreach ($users as $key => $user) {
            
            
            
            $price_return = 0;
            $return_products = $this->CreditReturn->find('all', array('fields' => 'SUM((CreditReturn.product_price*CreditReturn.quantity)+CreditReturn.restocking_fees) as price_return',
                'conditions' => array("CreditReturn.status" => "Y", "CreditReturn.user_id" => $user['User']['id']),
                'group' => array('CreditReturn.user_id'),
            ));
            if (!empty($return_products))
                $price_return = $return_products[0][0]['price_return'];

               $customer_fields = $this->CustomerField->find('first', array('fields' => array('CustomerField.batch_invoice', 'CustomerField.batch_invoice_status'), 'conditions' => array('CustomerField.user_id' => $user['User']['id'], 'CustomerField.batch_invoice_status' => 'unpaid')));

            $totalUnpaidv = 0;
            $totalUnpaid = $this->OrderDist->find('all', array('fields' => array('OrderDist.tmporderid,OrderDist.batch_no'), 'order' => array(
                    'OrderDist.id' => 'desc'), 'conditions' => array('OrderDist.userid' => $user['User']['id'], 'OrderDist.status' => array('Test', 'Accep'), 'OrderDist.Paid_Status' => 'unpaid')));
            //calculate total unpad
            //print_r($totalUnpaid);
            $batch_amount = 0;
            foreach ($totalUnpaid as $Key2 => $padamounts) {
                $batchamount = 0;
                foreach ($padamounts['OrderdetailMany'] as $pdamount) {
                    $totalUnpaidv = ($pdamount['quantity'] * $pdamount['price']) + $totalUnpaidv;
                    $batchamount = $batchamount + ($pdamount['quantity'] * $pdamount['price']);
                }
                
                if (!empty($customer_fields) and $customer_fields['CustomerField']['batch_invoice'] == $padamounts['OrderDist']['batch_no']) {
                    $batch_amount = $batch_amount + $batchamount;
                }
                
            }
            
            $totalUnpaidv = $totalUnpaidv - $price_return;
            $users[$key]['User']['totalUnpaid'] = $totalUnpaidv;
            $users[$key]['User']['batch_amount'] = $batch_amount;
            $users[$key]['User']['batchamount'] = $customer_fields['CustomerField']['batch_invoice'];
        }
        //print_r($users);
        $this->set('users', $users);
    }
	
	
	    public function admin_client_users_deactivate($id = NULL) {
//Configure::write('debug',2);
        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));

        $sales = $this->User->find('list', array('fields' => array('username'), 'conditions' => array('User.level' => 'sales')));
        $this->set('sales', $sales);
        $this->User->id = $id;


        if ($this->request->is('put') || $this->request->is('post')) {

            $this->User->create();

            if ($this->request->data['User']['threpercenteoff'] == 0 or $this->request->data['User']['threpercenteoff'] == '') {
				if ($this->request->data['User']['threpercenteoff6'] == 0 or $this->request->data['User']['threpercenteoff6'] == '')
                $this->request->data['User']['threpercenteoff'] = $this->request->data['User']['threpercenteoff10'];
				else
				$this->request->data['User']['threpercenteoff'] = $this->request->data['User']['threpercenteoff6'];
            }
			
            if ($this->User->save($this->request->data)) {

                $email = new CakeEmail();
                try {
                    $email->config('default');
                } catch (Exception $e) {
                    Throw new ConfigureException('Config in email.php not found. ' . $e->getMessage());
                }

                $Templates = $this->EmailTemplateDescription->find('all', array('conditions' => array('EmailTemplateDescription.email_template_id' => 1)));
                $TempContent = stripcslashes($Templates[0]['EmailTemplateDescription']['content']);
                $TempContent = str_replace("{email}", $this->request->data["User"]["email"], $TempContent);
                $TempContent = str_replace("{date}", date("Y-m-d"), $TempContent);
                $TempContent = str_replace("{name}", $this->request->data["User"]['username'], $TempContent);
                $TempContent = str_replace("{password}", $this->request->data["User"]['password'], $TempContent);
                $TempContent = str_replace("../../..", 'http://' . env('SERVER_NAME'), $TempContent);
                $data = array('Message' => $TempContent);
                $email->template('default', 'default');
                $email->emailFormat('both');
                $email->viewVars(array('data' => $data));
                $email->from(array('i-care@i-care.mobi' => 'JDPharmaceutical'));
                $email->to($this->request->data["User"]["email"]);
                $email->subject(__d('default', $Templates[0]['EmailTemplateDescription']['subject']));
                $email->send();



                $this->Session->setFlash(__('The user has been saved'), 'admin_success');
                $this->redirect(array('action' => 'admin_client_users'));
            } else {

                $this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'admin_error');
            }
        } else {
            $this->request->data = $this->User->read(null, $id);
            $this->request->data['User']['password'] = null;
            $this->request->data['User']['threpercenteoff10'] = $this->request->data['User']['threpercenteoff'];
			$this->request->data['User']['threpercenteoff6'] = $this->request->data['User']['threpercenteoff'];
        }

        $this->User->recursive = 1;
        $this->set('id', $id);

        $users = $this->paginate("User", array('User.level' => 'client','User.Status' => 0));
        foreach ($users as $key => $user) {

            /* $totalUnpaid = $this->Order->find('first',array('fields' => array('sum(Orderdetail.quantity*Orderdetail.price) as totalUnpaid'),'order' => array(
              'Order.id' => 'desc'),'group' => array('Order.userid'),'conditions'=>array('Order.userid'=>$user['User']['id'],'Order.Paid_Status'=>array('unpaid',''),'Order.status'=>array('Test','Accep')))); */
            //find batch
            //sum of return products
            $price_return = 0;
            $return_products = $this->CreditReturn->find('all', array('fields' => 'SUM((CreditReturn.product_price*CreditReturn.quantity)+CreditReturn.restocking_fees) as price_return',
                'conditions' => array("CreditReturn.status" => "Y", "CreditReturn.user_id" => $user['User']['id']),
                'group' => array('CreditReturn.user_id'),
            ));
            if (!empty($return_products))
                $price_return = $return_products[0][0]['price_return'];

            $customer_fields = $this->CustomerField->find('first', array('fields' => array('CustomerField.batch_invoice', 'CustomerField.batch_invoice_status'), 'conditions' => array('CustomerField.user_id' => $user['User']['id'], 'CustomerField.batch_invoice_status' => 'unpaid')));

            $totalUnpaidv = 0;
            $totalUnpaid = $this->OrderDist->find('all', array('fields' => array('OrderDist.tmporderid,OrderDist.batch_no'), 'order' => array(
                    'OrderDist.id' => 'desc'), 'conditions' => array('OrderDist.userid' => $user['User']['id'], 'OrderDist.status' => array('Test', 'Accep'), 'OrderDist.Paid_Status' => 'unpaid')));
            //calculate total unpad
            //print_r($totalUnpaid);
            $batch_amount = 0;
            foreach ($totalUnpaid as $Key2 => $padamounts) {
                $batchamount = 0;
                foreach ($padamounts['OrderdetailMany'] as $pdamount) {
                    $totalUnpaidv = ($pdamount['quantity'] * $pdamount['price']) + $totalUnpaidv;
                    $batchamount = $batchamount + ($pdamount['quantity'] * $pdamount['price']);
                }
                if (!empty($customer_fields) and $customer_fields['CustomerField']['batch_invoice'] == $padamounts['OrderDist']['batch_no']) {
                    $batch_amount = $batch_amount + $batchamount;
                }
            }
            $totalUnpaidv = $totalUnpaidv - $price_return;
            $users[$key]['User']['totalUnpaid'] = $totalUnpaidv;
            $users[$key]['User']['batch_amount'] = $batch_amount;
            $users[$key]['User']['batchamount'] = $customer_fields['CustomerField']['batch_invoice'];
        }
        //print_r($users);
        $this->set('users', $users);
    }

    public function admin_client_users_delete($id = null) {

        if (!$this->request->is('post')) {

            throw new MethodNotAllowedException();
        }

        $this->User->id = $id;

        if (!$this->User->exists()) {

            throw new NotFoundException(__('Invalid user'));
        }

        if ($this->User->delete()) {

            $this->Session->setFlash(__('User deleted'), 'admin_success');

            $this->redirect(array('action' => 'admin_client_users'));
        }

        $this->Session->setFlash(__('User was not deleted'), 'admin_error');

        $this->redirect(array('action' => 'admin_client_users'));
    }
	
	public function admin_client_status($id = null,$status) {
		//Configure::write('debug',2);
          $this->User->id = $id;

        if (!$this->User->exists()) {

            throw new NotFoundException(__('Invalid user'));
        }

        if ($this->User->updateAll(
                            array('User.status' => $status,
                            ), array('User.id' => $id)
                    )) {

            $this->Session->setFlash(__('User Status Changed'), 'admin_success');

            $this->redirect(array('action' => 'admin_client_users'));
        }

        $this->Session->setFlash(__('User was not Change Status'), 'admin_error');

        $this->redirect(array('action' => 'admin_client_users'));
    }

    public function admin_adminusers($id = NULL) {

        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));

        $this->User->id = $id;


        if ($this->request->is('put') || $this->request->is('post')) {

            $this->User->create();

            if ($this->User->save($this->request->data)) {

                $email = new CakeEmail();
                try {
                    $email->config('default');
                } catch (Exception $e) {
                    Throw new ConfigureException('Config in email.php not found. ' . $e->getMessage());
                }

                $Templates = $this->EmailTemplateDescription->find('all', array('conditions' => array('EmailTemplateDescription.email_template_id' => 1)));
                $TempContent = stripcslashes($Templates[0]['EmailTemplateDescription']['content']);
                $TempContent = str_replace("{email}", $this->request->data["User"]["email"], $TempContent);
                $TempContent = str_replace("{date}", date("Y-m-d"), $TempContent);
                $TempContent = str_replace("{name}", $this->request->data["User"]['username'], $TempContent);
                $TempContent = str_replace("{password}", $this->request->data["User"]['password'], $TempContent);
                $TempContent = str_replace("../../..", 'http://' . env('SERVER_NAME'), $TempContent);
                $data = array('Message' => $TempContent);
                $email->template('default', 'default');
                $email->emailFormat('both');
                $email->viewVars(array('data' => $data));
                $email->from(array('i-care@i-care.mobi' => 'JDPharmaceutical'));
                $email->to($this->request->data["User"]["email"]);
                $email->subject(__d('default', $Templates[0]['EmailTemplateDescription']['subject']));
                $email->send();



                $this->Session->setFlash(__('The user has been saved'), 'admin_success');
                $this->redirect(array('action' => 'admin_adminusers'));
            } else {

                $this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'admin_error');
            }
        } else {
            $this->request->data = $this->User->read(null, $id);
            $this->request->data['User']['password'] = null;
        }

        $this->User->recursive = 1;
        $this->set('id', $id);
        $this->set('users', $this->paginate("User", array('User.level' => 'admin')));
    }

    public function admin_admin_users_delete($id = null) {

        if (!$this->request->is('post')) {

            throw new MethodNotAllowedException();
        }

        $this->User->id = $id;

        if (!$this->User->exists()) {

            throw new NotFoundException(__('Invalid user'));
        }

        if ($this->User->delete()) {

            $this->Session->setFlash(__('User deleted'), 'admin_success');

            $this->redirect(array('action' => 'admin_admin_users_delete'));
        }

        $this->Session->setFlash(__('User was not deleted'), 'admin_error');

        $this->redirect(array('action' => 'admin_admin_users_delete'));
    }

    /**

     * view method

     *

     * @param string $id

     * @return void

     */
    public function admin_view($id = null) {

        $this->layout = "ajax";

        $this->set('title', __('View'));

        $this->User->id = $id;

        if (!$this->User->exists()) {

            throw new NotFoundException(__('Invalid user'), 'error');
        }

        $this->set('user', $this->User->read(null, $id));
    }

    /**

     * add method

     *

     * @return void

     */
    public function admin_add() {

        $this->layout = "admin_dashboard";

        if ($this->request->is('post')) {

            $this->loadModel('User');

            $password = $this->request->data["User"]['password'];

            $this->User->create();

            if ($this->User->save($this->request->data)) {

                $this->Session->setFlash(__('The user has been saved'), 'admin_success');

                $email = new CakeEmail();

                try {

                    $email->config('default');
                } catch (Exception $e) {

                    Throw new ConfigureException('Config in email.php not found. ' . $e->getMessage());
                }

                $Templates = $this->EmailTemplateDescription->find('all', array('conditions' => array('EmailTemplateDescription.email_template_id' => 1)));

                $TempContent = stripcslashes($Templates[0]['EmailTemplateDescription']['content']);

                $TempContent = str_replace("{email}", $this->request->data["User"]['email'], $TempContent);

                $TempContent = str_replace("{date}", date("Y-m-d"), $TempContent);

                $TempContent = str_replace("{name}", $this->request->data["User"]['username'], $TempContent);

                $TempContent = str_replace("{password}", $password, $TempContent);

                $TempContent = str_replace("../../..", 'http://' . env('SERVER_NAME'), $TempContent);

                $data = array('Message' => $TempContent);

                $email->template('default', 'default');

                $email->emailFormat('both');

                $email->viewVars(array('data' => $data));

                $email->from(array('i-care@i-care.mobi' => 'JDPharmaceutical'));

                $email->to(Sanitize::clean($_REQUEST["email"]));

                $email->subject(__d('default', $Templates[0]['EmailTemplateDescription']['subject']));

                $email->send();

                $this->redirect(array('action' => 'index'));
            } else {

                $this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'admin_error');
            }
        }

        $groups = $this->User->Group->find('list');

        $this->set(compact('groups'));
    }

    /**

     * edit method

     *

     * @param string $id

     * @return void

     */
    public function admin_edit($id = null) {

        $this->layout = "admin_dashboard";

        $this->User->id = $id;

        if (!$this->User->exists()) {

            throw new NotFoundException(__('Invalid user'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {

            if ($this->User->save($this->request->data)) {

                $this->Session->setFlash(__('The user has been saved'), 'admin_success');

                $this->redirect(array('action' => 'index'));
            } else {

                $this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'admin_error');
            }
        } else {

            $this->request->data = $this->User->read(null, $id);

            $this->request->data['User']['password'] = null;
        }

        $groups = $this->User->Group->find('list');

        $this->set(compact('groups'));
    }

    /**

     * delete method

     *

     * @param string $id

     * @return void

     */
    public function admin_delete($id = null) {

        if (!$this->request->is('post')) {

            throw new MethodNotAllowedException();
        }

        $this->User->id = $id;

        if (!$this->User->exists()) {

            throw new NotFoundException(__('Invalid user'));
        }

        if ($this->User->delete()) {

            $this->Session->setFlash(__('User deleted'), 'admin_success');

            $this->redirect(array('action' => 'index'));
        }

        $this->Session->setFlash(__('User was not deleted'), 'admin_error');

        $this->redirect(array('action' => 'index'));
    }

    /**

     *  Active/Inactive User

     *

     * @param <int> $user_id

     */
    public function toggle($user_id, $status) {

        $this->layout = "ajax";

        $status = ($status) ? 0 : 1;

        $this->set(compact('user_id', 'status'));

        if ($user_id) {

            $data['User'] = array('id' => $user_id, 'status' => $status);

            $allowed = $this->User->saveAll($data["User"], array('validate' => false));
        }
    }

    public function index() {
        $this->layout = "index";
    }

    public function login() {
        if ($this->request->is('post')) {

            $user_active = $this->User->find('first', array('fields' => 'active', 'conditions' => array('User.username' => $this->request->data['User']['username'], 'level' => 'client')));
            error_log("TEST: " . print_r($user_active, true));

            if (!empty($user_active)) {
                if ($user_active['User']['active']) {
                    if ($this->Auth->login()) {
                        $this->Cookie->write('username', $this->Auth->user('username'), true, '+4 weeks');

                        if ($this->request->data['User']['remember'] == 1) {
                            unset($this->request->data['User']['remember']);

                            $this->request->data['User']['password'] = $this->Auth->password($this->request->data['User']['password']);

                            $this->Cookie->write('remember_me_cookie', $this->request->data['User'], true, '2 weeks');
                        }
                        $this->redirect($this->Auth->redirect());
                    } else {
                        $this->Session->setFlash(__('Invalid username or password, try again'), 'error', array('class' => 'alert-error'));
                    }
                } else {
                    //$this->Session->setFlash(__('User not active'),'alert',array('class'=>'alert-error'));
                    $this->Session->setFlash(__('User not active'), 'error', array('class' => 'alert-error'));
                }
            } else {
                //	$this->Session->setFlash(__('User not found'),'alert',array('class'=>'alert-error'));
                $this->Session->setFlash(__('User not found'), 'error', array('class' => 'alert-error'));
                $this->redirect('index');
            }
        } else {
            if ($this->Cookie->check('username')) {
                $lastUsername = $this->Cookie->read('username');
                if (!empty($lastUsername)) {
                    $this->request->data['User']['username'] = $lastUsername;
                }
            }
        }
    }

    public function admin_client_order_history($uid) {
        $this->layout = "admin_dashboard";
        //Configure::write('debug',2);

        if ($this->request->query['selfrmdate'] != '' and $this->request->query['seltodate'] != '') {
            if ($this->request->query['ndcproduct'] != '') {
                $this->set('search', $this->request->query['ndcproduct']);
                $product_id = $this->Products->find('first', array('conditions' => array("Products.product_ndc" => $this->request->query['ndcproduct']), "fields" => array("id")));
                $Orderdetail = $this->Orderdetail->find('list', array('recursive' => 2, 'fields' => array("orderid"), 'conditions' => array('Orderdetail.productid' => $product_id['Products']['id'])));

                $this->paginate = array(
                    'conditions' => array('OrderDist.status' => array('Test', 'Accep'), 'OrderDist.id' => $Orderdetail, "OrderDist.createddt>='" . $this->request->query['selfrmdate'] . "'", "OrderDist.createddt<='" . $this->request->query['seltodate'] . "'", 'OrderDist.userid' => $uid), 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.Paid_Status', 'OrderDist.po_number', 'OrderDist.batch_no'));
            } else {
                $this->paginate = array(
                    'conditions' => array('OrderDist.status' => array('Test', 'Accep'), "OrderDist.createddt>='" . $this->request->query['selfrmdate'] . "'", "OrderDist.createddt<='" . $this->request->query['seltodate'] . "'", 'OrderDist.userid' => $uid), 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.Paid_Status', 'OrderDist.po_number', 'OrderDist.batch_no')
                );
            }
            $this->set('seltodate', $this->request->query['seltodate']);
            $this->set('selfrmdate', $this->request->query['selfrmdate']);
        } elseif ($this->request->query['ndcproduct'] != '') {
            $product_id = $this->Products->find('first', array('conditions' => array("Products.product_ndc" => $this->request->query['ndcproduct']), "fields" => array("id")));
            $Orderdetail = $this->Orderdetail->find('list', array('recursive' => 2, 'fields' => array("orderid"), 'conditions' => array('Orderdetail.productid' => $product_id['Products']['id'])));

            $this->paginate = array(
                'conditions' => array('OrderDist.status' => array('Test', 'Accep'), 'OrderDist.userid' => $uid, 'OrderDist.id' => $Orderdetail), 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.Paid_Status', 'OrderDist.po_number', 'OrderDist.batch_no'));
            $this->set('search', $this->request->query['ndcproduct']);
        } else {
            $this->paginate = array(
                'conditions' => array('OrderDist.status' => array('Test', 'Accep'), 'OrderDist.userid' => $uid), 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.Paid_Status', 'OrderDist.po_number', 'OrderDist.batch_no',"OrderDist.checked"),
                'order' => array(
                    'OrderDist.createddt' => 'desc'
                )
            );
        }
        $products = $this->paginate("OrderDist");




        $cart_list = $this->Order->find('all', array('fields' => array('DISTINCT Order.id', 'Order.tmporderid', 'Order.userid', 'Order.createddt', 'Order.status', 'Order.Paid_Status', 'Order.po_number'), 'order' => array(
                'Order.createddt' => 'desc'
            ), 'conditions' => array('Order.userid' => $uid, 'Order.status' => 'Con')));
        //print_r($cart_list);
        $this->set('pendingorder', $cart_list);
        $this->set('users', $products);

        $user_info = $this->User->find('first', array('fields' => array('User.fname', 'User.id', 'User.client_due_datea'), 'conditions' => array('User.id' => $uid)));
        $this->set('user_info', $user_info);

        $customer_fields = $this->CustomerField->find('first', array('fields' => array('CustomerField.batch_invoice', 'CustomerField.batch_invoice_status'), 'conditions' => array('CustomerField.user_id' => $uid, 'CustomerField.batch_invoice_status' => 'unpaid')));

        $this->set('customer_fields', $customer_fields);

        /* $totalPaid = $this->Order->find('first',array('fields' => array('sum(Orderdetail.quantity*Orderdetail.price) as totalPaid','Order.userid', 'Order.id'),'order' => array(
          'Order.id' => 'desc'),'group' => array('Order.userid'),'conditions'=>array('Order.userid'=>$uid,'Order.Paid_Status'=>'paid','Order.status'=>array('Test','Accep'))));
          $totalUnpaid = $this->Order->find('first',array('fields' => array('sum(Orderdetail.quantity*Orderdetail.price) as totalUnpaid'),'order' => array(
          'Order.id' => 'desc'),'group' => array('Order.userid'),'conditions'=>array('Order.userid'=>$uid,'Order.Paid_Status'=>array('unpaid',''),'Order.status'=>array('Test','Accep')))); */


        $totalPaidv = 0;
        $totalPaid = $this->OrderDist->find('all', array('fields' => array('OrderDist.tmporderid'), 'order' => array(
                'OrderDist.id' => 'desc'), 'conditions' => array('OrderDist.userid' => $uid, 'OrderDist.status' => array('Test', 'Accep'), 'OrderDist.Paid_Status' => 'paid')));

        //calculate total unpad
        foreach ($totalPaid as $padamounts) {

            foreach ($padamounts['OrderdetailMany'] as $pdamount) {

                $totalPaidv = ($pdamount['quantity'] * $pdamount['price']) + $totalPaidv;
            }
        }

        $totalUnpaidv = 0;
        $totalUnpaid = $this->OrderDist->find('all', array('fields' => array('OrderDist.tmporderid'), 'order' => array(
                'OrderDist.id' => 'desc'), 'conditions' => array('OrderDist.userid' => $uid, 'OrderDist.status' => array('Test', 'Accep'), 'OrderDist.Paid_Status' => 'unpaid')));
        //calculate total unpad

        foreach ($totalUnpaid as $padamounts) {
            foreach ($padamounts['OrderdetailMany'] as $pdamount) {
                $totalUnpaidv = ($pdamount['quantity'] * $pdamount['price']) + $totalUnpaidv;
            }
        }
        $this->set('totalUnpaid', $totalUnpaidv);
        //sum of return products
        $price_return = 0;
            $return_products = $this->CreditReturn->find('all', array('fields' => 'SUM((CreditReturn.product_price*CreditReturn.quantity)+CreditReturn.restocking_fees) as price_return',
            'conditions' => array("CreditReturn.status" => "Y", "CreditReturn.user_id" => $uid),
            'group' => array('CreditReturn.user_id'),
        ));
        if (!empty($return_products))
            $price_return = $return_products[0][0]['price_return'];
			
		$productDetails = $this->Products->find("list",array('fields' => array('Products.id', 'Products.productname'),"conditions" =>array("Products.active" =>'True')));
		
		$this->set('product_list', $productDetails);
        $this->set('totalReturn', $price_return);
        $this->set('totalPaid', $totalPaidv);
        $this->set('uid', $uid);
    }

    public function admin_client_order_download_pdf($uid) {
        $this->layout = "admin_dashboard";
       // Configure::write('debug',2);
		ini_set('MAX_EXECUTION_TIME', -1);
		ini_set('memory_limit', '-1');

        if ($this->request->query['selfrmdate'] != '' and $this->request->query['seltodate'] != '') {
            
            $this->set('seltodate', $this->request->query['seltodate']);
            $this->set('selfrmdate', $this->request->query['selfrmdate']);
			
			$cart_list = $this->OrderDist->find('all', array('recursive' => 2, 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.createddt'), 'conditions' => array('OrderDist.userid' => $this->request->query['uid'], "OrderDist.createddt>='" . $this->request->query['selfrmdate'] . "'", "OrderDist.createddt<='" . $this->request->query['seltodate'] . "'")));
			
			$csvdata = array();
        $gkey = 0;
        foreach ($cart_list as $key => $dt) {
            $product_qauantity = count($dt['OrderdetailMany']);
            $priice = 0;
            $product_name = "";
            $product_ndc = '';
            $item_number = '';
            $productprice = '';
            $producttype = '';
                $lot_ids = '';
            foreach ($dt['OrderdetailMany'] as $key2 => $orderdeatils) {
                $Orderdetail_lot = $this->OrderLot->find('all', array('recursive' => -1, 'fields' => array('id', 'product_id', 'quantity', 'lot_no', 'price', 'productinfoid', 'Orderdetailid', 'tmporderid', 'exchange_lot'), 'conditions' => array('OrderLot.Orderdetailid' => $orderdeatils['id'])));

                $priice = ($orderdeatils['price'] * $orderdeatils['quantity']);
                $product_name = $orderdeatils['Product']['productname'];
                $product_ndc = $orderdeatils['Product']['product_ndc'];
                $productprice = $orderdeatils['Product']['productprice'];
                $itemnumber = $orderdeatils['Product']['itemnumber'];
                $producttype = $orderdeatils['Product']['producttype'];

                foreach ($Orderdetail_lot as $lots) {
                    $lot_ids = ($lots['OrderLot']['exchange_lot'] != '') ? $lots['OrderLot']['exchange_lot'] : $lots['OrderLot']['lot_no'];
                    $csvdata[$gkey]['Order'] = $dt['OrderDist']['tmporderid'];
                    $csvdata[$gkey]['Quantity'] = $orderdeatils['quantity'];
					$csvdata[$gkey]['special_discount'] = $orderdeatils['special_discount'];
					$csvdata[$gkey]['original_price'] = $orderdeatils['original_price'];
                    $csvdata[$gkey]['Price'] = $orderdeatils['price'];
                   
                    $csvdata[$gkey]['ORDER DATE'] = date("m-d-Y", strtotime($dt['OrderDist']['createddt']));
                    $csvdata[$gkey]['Product NDC'] = $product_ndc;
                    $csvdata[$gkey]['Item Number'] = $itemnumber;
                    $csvdata[$gkey]['Per Item Price'] = $productprice;
                    $csvdata[$gkey]['Product Type'] = $producttype;
                    $csvdata[$gkey]['Lot #'] = $lot_ids;
                    $csvdata[$gkey]['Product Name'] = $product_name;

                    $gkey++;
                }
            }
        }
		
				
			
        } 
		
       
       // print_r($csvdata);

        $user_info = $this->User->find('first', array('conditions' => array('User.id' => $uid)));
        $this->set('user_info', $user_info);

        $customer_fields = $this->CustomerField->find('first', array('fields' => array('CustomerField.batch_invoice', 'CustomerField.batch_invoice_status'), 'conditions' => array('CustomerField.user_id' => $uid, 'CustomerField.batch_invoice_status' => 'unpaid')));

        $this->set('customer_fields', $customer_fields);
		$this->set('uid', $uid);
		$this->set('csvdata',$csvdata);
		
		
if(!empty($csvdata)){
		 
App::import('Vendor','xtcpdf');  
$tcpdf = new XTCPDF(); 
$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans' 

//$tcpdf->SetAuthor("KBS Homes & Properties at http://kbs-properties.com"); 
$tcpdf->SetAutoPageBreak( false ); 
//$tcpdf->setHeaderFont(array($textfont,'',40)); 
//$tcpdf->xheadercolor = array(150,0,0); 
//$tcpdf->xheadertext = 'KBS Homes & Properties'; 
$tcpdf->xfootertext = 'Copyright © 2014 JDPharmaceutical Inc. All rights reserved.'; 
$tcpdf->SetPrintHeader(false);
// add a page (required with recent versions of tcpdf) 
$tcpdf->AddPage(); 

// Now you position and print your page content 
// example:  
//$tcpdf->SetTextColor(0, 0, 0); 
$tcpdf->SetFont($textfont); 
$tcpdf->SetAutoPageBreak( true, 30 ); 
//$tcpdf->Cell(0,14, "Hello World", 0,1,'L'); 
// ... 
// etc. 
// see the TCPDF examples  


$no_js = true;

//ob_start();
//include $filename;
//$html = ob_get_clean();
//$html=utf8_encode($html);
// Print text using writeHTMLCell()
 
//$tcpdf->writeHTMLCell($w=0, $h=0, $x=3, $y='', $html, $border=0, $ln=1, $fill=0, $reseth=false, $align='', $autopadding=true);
$filename = "http://jdpwholesaler.com/customers/client_order_download_pdf_url/".$uid."/".$this->request->query['selfrmdate']."/".$this->request->query['seltodate'];
 $html =file_get_contents($filename);
$tcpdf->writeHTML($html, true, 0, true, true);
echo $tcpdf->Output('invoice.pdf', 'D'); 

		}

		
		
    }
	
	
	

    public function admin_client_order_history_purchase_size_excel($uid) {
        $this->layout = "admin_dashboard";

        if ($this->request->query['selfrmdate'] != '' and $this->request->query['seltodate'] != '') {
            if ($this->request->query['ndcproduct'] != '') {
                $product_id = $this->Products->find('first', array('conditions' => array("Products.product_ndc" => $this->request->query['ndcproduct']), "fields" => array("id")));
                $Orderdetail = $this->Orderdetail->find('list', array('recursive' => 2, 'fields' => array("orderid"), 'conditions' => array('Orderdetail.productid' => $product_id['Products']['id'])));

                $cart_list = $this->OrderDist->find('all', array('recursive' => 2, 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.createddt'), 'conditions' => array('OrderDist.userid' => $uid, "OrderDist.createddt>='" . $this->request->query['selfrmdate'] . "'", "OrderDist.createddt<='" . $this->request->query['seltodate'] . "'", 'OrderDist.id' => $Orderdetail)));
            } else {
                $cart_list = $this->OrderDist->find('all', array('recursive' => 2, 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.createddt'), 'conditions' => array('OrderDist.userid' => $uid, "OrderDist.createddt>='" . $this->request->query['selfrmdate'] . "'", "OrderDist.createddt<='" . $this->request->query['seltodate'] . "'")));
            }
        } elseif ($this->request->query['ndcproduct'] != '') {
            $product_id = $this->Products->find('first', array('conditions' => array("Products.product_ndc" => $this->request->query['ndcproduct']), "fields" => array("id")));
            $Orderdetail = $this->Orderdetail->find('list', array('recursive' => 2, 'fields' => array("orderid"), 'conditions' => array('Orderdetail.productid' => $product_id['Products']['id'])));

            $cart_list = $this->OrderDist->find('all', array('recursive' => 2, 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.createddt'), 'conditions' => array('OrderDist.id' => $Orderdetail, 'OrderDist.userid' => $uid)));
        } else {
            $cart_list = $this->OrderDist->find('all', array('recursive' => 2, 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.createddt'), 'conditions' => array('OrderDist.userid' => $uid)));
        }
        //print_r($cart_list);die;
        $csvdata = array();
        $temp_array = array();
        $gkey = 0;
        foreach ($cart_list as $key => $dt) {
            $product_qauantity = count($dt['OrderdetailMany']);
            $priice = 0;
            $product_name = "";
            $product_ndc = '';
            $item_number = '';
            $productprice = '';
            $producttype = '';
            $lot_ids = '';
            foreach ($dt['OrderdetailMany'] as $key2 => $orderdeatils) {




                $product_name = $orderdeatils['Product']['productname'];
                $product_ndc = $orderdeatils['Product']['product_ndc'];

                $itemnumber = $orderdeatils['Product']['itemnumber'];
                $quantity = $orderdeatils['quantity'];
                $product_size = $orderdeatils['Product']['product_size'];

                if (in_array($product_ndc, $temp_array)) {
                    $key_tmp = array_search($product_ndc, $temp_array);
                    $csvdata[$key_tmp]['Quantity'] = ($csvdata[$key_tmp]['Quantity']) + $quantity;
                    $csvdata[$key_tmp]['Total Size Purchase'] = ($csvdata[$key_tmp]['Total Size Purchase']) + $product_size * $quantity;
                    $csvdata[$key_tmp]['Total Left over'] = ($csvdata[$key_tmp]['Total Left over']) + $product_size * $quantity;
                } else {
                    $temp_array[$gkey] = $product_ndc;
                    $csvdata[$gkey]['Product NDC'] = $product_ndc;
                    $csvdata[$gkey]['Item Number'] = $itemnumber;
                    $csvdata[$gkey]['Product Name'] = $product_name;
                    $csvdata[$gkey]['Quantity'] = $quantity;
                    $csvdata[$gkey]['Size'] = $product_size;

                    $csvdata[$gkey]['Total Size Purchase'] = $product_size * $quantity;
                    $csvdata[$gkey]['Total Dispense'] = 0;
                    $csvdata[$gkey]['Total Left over'] = $product_size * $quantity;

                    $gkey++;
                }
            }
        }
        //get user details
        $userinfo = $this->User->find('first', array('conditions' => array('User.id' => $uid)));

        $this->Export->exportCsv($csvdata, '', $userinfo);
    }

    public function admin_client_order_history_excel($uid) {
        $this->layout = "admin_dashboard";

        if ($this->request->query['selfrmdate'] != '' and $this->request->query['seltodate'] != '') {
            if ($this->request->query['ndcproduct'] != '') {
                $product_id = $this->Products->find('first', array('conditions' => array("Products.product_ndc" => $this->request->query['ndcproduct']), "fields" => array("id")));
                $Orderdetail = $this->Orderdetail->find('list', array('recursive' => 2, 'fields' => array("orderid"), 'conditions' => array('Orderdetail.productid' => $product_id['Products']['id'])));

                $cart_list = $this->OrderDist->find('all', array('recursive' => 2, 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.createddt'), 'conditions' => array('OrderDist.userid' => $uid, "OrderDist.createddt>='" . $this->request->query['selfrmdate'] . "'", "OrderDist.createddt<='" . $this->request->query['seltodate'] . "'", 'OrderDist.id' => $Orderdetail)));
            } else {
                $cart_list = $this->OrderDist->find('all', array('recursive' => 2, 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.createddt'), 'conditions' => array('OrderDist.userid' => $uid, "OrderDist.createddt>='" . $this->request->query['selfrmdate'] . "'", "OrderDist.createddt<='" . $this->request->query['seltodate'] . "'")));
            }
        } elseif ($this->request->query['ndcproduct'] != '') {
            $product_id = $this->Products->find('first', array('conditions' => array("Products.product_ndc" => $this->request->query['ndcproduct']), "fields" => array("id")));
            $Orderdetail = $this->Orderdetail->find('list', array('recursive' => 2, 'fields' => array("orderid"), 'conditions' => array('Orderdetail.productid' => $product_id['Products']['id'])));
            $cart_list = $this->OrderDist->find('all', array('recursive' => 2, 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.createddt'), 'conditions' => array('OrderDist.id' => $Orderdetail, 'OrderDist.userid' => $uid)));

            $this->set('search', $this->request->query['ndcproduct']);
        } else {
            $cart_list = $this->OrderDist->find('all', array('recursive' => 2, 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.createddt'), 'conditions' => array('OrderDist.userid' => $uid)));
        }
        //print_r($cart_list);die;
        $csvdata = array();
        $gkey = 0;
        foreach ($cart_list as $key => $dt) {
            $product_qauantity = count($dt['OrderdetailMany']);
            $priice = 0;
            $product_name = "";
            $product_ndc = '';
            $item_number = '';
            $productprice = '';
            $producttype = '';
            $lot_ids = '';
            foreach ($dt['OrderdetailMany'] as $key2 => $orderdeatils) {
                $Orderdetail_lot = $this->OrderLot->find('all', array('recursive' => -1, 'fields' => array('id', 'product_id', 'OrderLot.quantity', 'lot_no', 'price', 'productinfoid', 'Orderdetailid', 'tmporderid', 'exchange_lot'), 'conditions' => array('OrderLot.Orderdetailid' => $orderdeatils['id'])));


                $priice = ($orderdeatils['price'] * $orderdeatils['quantity']);
                $product_name = $orderdeatils['Product']['productname'];
                $product_ndc = $orderdeatils['Product']['product_ndc'];
                $productprice = $orderdeatils['Product']['productprice'];
                $itemnumber = $orderdeatils['Product']['itemnumber'];
                $producttype = $orderdeatils['Product']['producttype'];

                foreach ($Orderdetail_lot as $lots) {
                    $lot_ids = ($lots['OrderLot']['exchange_lot'] != '') ? $lots['OrderLot']['exchange_lot'] : $lots['OrderLot']['lot_no'];
                    //$lot_ids=$lots['OrderLot']['id'];
                    $lot_ids = $lots['OrderLot']['lot_no'];
                    $csvdata[$gkey]['Order'] = $dt['OrderDist']['tmporderid'];
                    $csvdata[$gkey]['Quantity'] = $lots['OrderLot']['quantity'];
                    $csvdata[$gkey]['Price'] = '$' . $priice;
                    $csvdata[$gkey]['Status'] = ($dt['OrderDist']['status'] == 'Accep') ? 'Accept' : $dt['OrderDist']['status'];
                    $csvdata[$gkey]['ORDER DATE'] = date("m-d-Y", strtotime($dt['OrderDist']['createddt']));
                    $csvdata[$gkey]['Product NDC'] = $product_ndc;
                    $csvdata[$gkey]['Item Number'] = $itemnumber;
                    $csvdata[$gkey]['Per Item Price'] = $productprice;
                    $csvdata[$gkey]['Product Type'] = $producttype;
                    $csvdata[$gkey]['Lot #'] = $lot_ids;
                    $csvdata[$gkey]['Product Name'] = $product_name;

                    $gkey++;
                }
            }
        }
        //get user details
        $userinfo = $this->User->find('first', array('conditions' => array('User.id' => $uid)));

        $this->Export->exportCsv($csvdata, '', $userinfo);
    }
	
	public function admin_client_order_history_excel_undc($uid) {
        $this->layout = "admin_dashboard";

        if ($this->request->query['selfrmdate'] != '' and $this->request->query['seltodate'] != '') {
            if ($this->request->query['ndcproduct'] != '') {
                $product_id = $this->Products->find('first', array('conditions' => array("Products.product_ndc" => $this->request->query['ndcproduct']), "fields" => array("id")));
                $Orderdetail = $this->Orderdetail->find('list', array('recursive' => 2, 'fields' => array("orderid"), 'conditions' => array('Orderdetail.productid' => $product_id['Products']['id'])));

                $cart_list = $this->OrderDist->find('all', array('recursive' => 2, 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.createddt'), 'conditions' => array('OrderDist.userid' => $uid, "OrderDist.createddt>='" . $this->request->query['selfrmdate'] . "'", "OrderDist.createddt<='" . $this->request->query['seltodate'] . "'", 'OrderDist.id' => $Orderdetail)));
            } else {
                $cart_list = $this->OrderDist->find('all', array('recursive' => 2, 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.createddt'), 'conditions' => array('OrderDist.userid' => $uid, "OrderDist.createddt>='" . $this->request->query['selfrmdate'] . "'", "OrderDist.createddt<='" . $this->request->query['seltodate'] . "'")));
            }
        } elseif ($this->request->query['ndcproduct'] != '') {
            $product_id = $this->Products->find('first', array('conditions' => array("Products.product_ndc" => $this->request->query['ndcproduct']), "fields" => array("id")));
            $Orderdetail = $this->Orderdetail->find('list', array('recursive' => 2, 'fields' => array("orderid"), 'conditions' => array('Orderdetail.productid' => $product_id['Products']['id'])));
            $cart_list = $this->OrderDist->find('all', array('recursive' => 2, 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.createddt'), 'conditions' => array('OrderDist.id' => $Orderdetail, 'OrderDist.userid' => $uid)));

            $this->set('search', $this->request->query['ndcproduct']);
        } else {
            $cart_list = $this->OrderDist->find('all', array('recursive' => 2, 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.createddt'), 'conditions' => array('OrderDist.userid' => $uid)));
        }
        //print_r($cart_list);die;
        $csvdata = array();
		$lotunique=array();
        $gkey = 0;
        foreach ($cart_list as $key => $dt) {
            $product_qauantity = count($dt['OrderdetailMany']);
            $priice = 0;
            $product_name = "";
            $product_ndc = '';
            $item_number = '';
            $productprice = '';
            $producttype = '';
            $lot_ids = '';
            foreach ($dt['OrderdetailMany'] as $key2 => $orderdeatils) {
                $Orderdetail_lot = $this->OrderLot->find('all', array('recursive' => -1, 'fields' => array('id', 'product_id', 'OrderLot.quantity', 'lot_no', 'price', 'productinfoid', 'Orderdetailid', 'tmporderid', 'exchange_lot'), 'conditions' => array('OrderLot.Orderdetailid' => $orderdeatils['id'])));


                $priice = ($orderdeatils['price'] * $orderdeatils['quantity']);
                $product_name = $orderdeatils['Product']['productname'];
                $product_ndc = $orderdeatils['Product']['product_ndc'];
                $productprice = $orderdeatils['Product']['productprice'];
                $itemnumber = $orderdeatils['Product']['itemnumber'];
                $producttype = $orderdeatils['Product']['producttype'];
				//print_r($orderdeatils);die;
               /* foreach ($Orderdetail_lot as $lots) {
					
					if(array_key_exists($product_ndc,$lotunique)){
						
						$lot_ids = ($lots['OrderLot']['exchange_lot'] != '') ? $lots['OrderLot']['exchange_lot'] : $lots['OrderLot']['lot_no'];
                    //$lot_ids=$lots['OrderLot']['id'];
                    $lot_ids = $lots['OrderLot']['lot_no'];
                    $csvdata[$lotunique[$product_ndc]]['Order'] = $csvdata[$lotunique[$product_ndc]]['Order'].','.$dt['OrderDist']['tmporderid'];
                    $csvdata[$lotunique[$product_ndc]]['Quantity'] =$csvdata[$lotunique[$product_ndc]]['Quantity']+$lots['OrderLot']['quantity'];
                    $csvdata[$lotunique[$product_ndc]]['Price'] = $csvdata[$lotunique[$product_ndc]]['Price']+$priice;
                    $csvdata[$lotunique[$product_ndc]]['Status'] = $csvdata[$lotunique[$product_ndc]]['Status'].','.($dt['OrderDist']['status'] == 'Accep') ? 'Accept' : $dt['OrderDist']['status'];
                    $csvdata[$lotunique[$product_ndc]]['ORDER DATE'] = $csvdata[$lotunique[$product_ndc]]['ORDER DATE'].','.date("m-d-Y", strtotime($dt['OrderDist']['createddt']));
                    $csvdata[$lotunique[$product_ndc]]['Product NDC'] = $product_ndc;
                    $csvdata[$lotunique[$product_ndc]]['Item Number'] = $itemnumber;
                    $csvdata[$lotunique[$product_ndc]]['Per Item Price'] = $productprice;
                    $csvdata[$lotunique[$product_ndc]]['Product Type'] = $producttype;
                    $csvdata[$lotunique[$product_ndc]]['Lot #'] = $csvdata[$lotunique[$product_ndc]]['Lot #'].','.$lot_ids;
                    $csvdata[$lotunique[$product_ndc]]['Product Name'] = $product_name;
					}else{
                    $lot_ids = ($lots['OrderLot']['exchange_lot'] != '') ? $lots['OrderLot']['exchange_lot'] : $lots['OrderLot']['lot_no'];
                    //$lot_ids=$lots['OrderLot']['id'];
                    $lot_ids = $lots['OrderLot']['lot_no'];
                    $csvdata[$gkey]['Order'] = $dt['OrderDist']['tmporderid'];
                    $csvdata[$gkey]['Quantity'] = $lots['OrderLot']['quantity'];
                    $csvdata[$gkey]['Price'] = $priice;
                    $csvdata[$gkey]['Status'] = ($dt['OrderDist']['status'] == 'Accep') ? 'Accept' : $dt['OrderDist']['status'];
                    $csvdata[$gkey]['ORDER DATE'] = date("m-d-Y", strtotime($dt['OrderDist']['createddt']));
                    $csvdata[$gkey]['Product NDC'] = $product_ndc;
                    $csvdata[$gkey]['Item Number'] = $itemnumber;
                    $csvdata[$gkey]['Per Item Price'] = $productprice;
                    $csvdata[$gkey]['Product Type'] = $producttype;
                    $csvdata[$gkey]['Lot #'] = $lot_ids;
                    $csvdata[$gkey]['Product Name'] = $product_name;
					$lotunique[$product_ndc]=$gkey;
                    $gkey++;
					}*/
                $lot_ids = $lots['OrderLot']['lot_no'];
                    $csvdata[$gkey]['Order'] = $dt['OrderDist']['tmporderid'];
                    $csvdata[$gkey]['Quantity'] = $orderdeatils['quantity'];
                    $csvdata[$gkey]['Price'] = $priice;
                    $csvdata[$gkey]['Status'] = ($dt['OrderDist']['status'] == 'Accep') ? 'Accept' : $dt['OrderDist']['status'];
                    $csvdata[$gkey]['ORDER DATE'] = date("m-d-Y", strtotime($dt['OrderDist']['createddt']));
                    $csvdata[$gkey]['Product NDC'] = $product_ndc;
                    $csvdata[$gkey]['Item Number'] = $itemnumber;
                    $csvdata[$gkey]['Per Item Price'] = $productprice;
                    $csvdata[$gkey]['Product Type'] = $producttype;
                    //$csvdata[$gkey]['Lot #'] = $lot_ids;
                    $csvdata[$gkey]['Product Name'] = $product_name;
					//$lotunique[$product_ndc]=$gkey;
                    $gkey++;
            }
        }
        //get user details
        $userinfo = $this->User->find('first', array('conditions' => array('User.id' => $uid)));

        $this->Export->exportCsv($csvdata, '', $userinfo);
    }
	
    public function admin_client_order_unpaid_batch($uid) {
        $this->layout = "admin_dashboard";

        if ($this->request->is('put') || $this->request->is('post')) {
            $data = array();
            $batch_inoice = time();
            if (!empty($this->request->data['OrderHistory']['batch'])) {
                $data['CustomerField']['user_id'] = $uid;
                $data['CustomerField']['batch_invoice'] = $batch_inoice;
                $data['CustomerField']['batch_invoice_status'] = 'unpaid';
                $this->CustomerField->create();
                $this->CustomerField->save($data);
                foreach ($this->request->data['OrderHistory']['batch'] as $invoices) {
                    $this->OrderDist->updateAll(
                            array('OrderDist.batch_no' => $batch_inoice,
                            ), array('OrderDist.id' => $invoices)
                    );
                }
                $this->Session->setFlash('Batch Completed Successfully', 'default');
                $this->redirect(array('action' => 'client_order_history/' . $uid));
            } else {
                $this->Session->setFlash('<div class="alert alert-error"><strong>Error ! </strong>Please select invoices.</div>', 'default');
            }
        }
        $this->paginate = array(
            'conditions' => array('OrderDist.status' => array('Test', 'Accep'), 'OrderDist.userid' => $uid, 'OrderDist.Paid_Status' => 'unpaid'), 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.Paid_Status', 'OrderDist.po_number'),
            'limit' => 100000,
            'order' => array(
                'OrderDist.createddt' => 'desc'
            )
        );

        $products = $this->paginate("OrderDist");





        $this->set('users', $products);

        $user_info = $this->User->find('first', array('fields' => array('User.fname', 'User.id'), 'conditions' => array('User.id' => $uid)));
        $this->set('user_info', $user_info);




        $totalPaidv = 0;
        $totalPaid = $this->OrderDist->find('all', array('fields' => array('OrderDist.tmporderid'), 'order' => array(
                'OrderDist.id' => 'desc'), 'conditions' => array('OrderDist.userid' => $uid, 'OrderDist.status' => array('Test', 'Accep'), 'OrderDist.Paid_Status' => 'paid')));

        //calculate total unpad
        foreach ($totalPaid as $padamounts) {

            foreach ($padamounts['OrderdetailMany'] as $pdamount) {

                $totalPaidv = ($pdamount['quantity'] * $pdamount['price']) + $totalPaidv;
            }
        }

        $totalUnpaidv = 0;
        $totalUnpaid = $this->OrderDist->find('all', array('fields' => array('OrderDist.tmporderid'), 'order' => array(
                'OrderDist.id' => 'desc'), 'conditions' => array('OrderDist.userid' => $uid, 'OrderDist.status' => array('Test', 'Accep'), 'OrderDist.Paid_Status' => 'unpaid')));
        //calculate total unpad

        foreach ($totalUnpaid as $padamounts) {
            foreach ($padamounts['OrderdetailMany'] as $pdamount) {
                $totalUnpaidv = ($pdamount['quantity'] * $pdamount['price']) + $totalUnpaidv;
            }
        }
        $this->set('totalUnpaid', $totalUnpaidv);
        $this->set('totalPaid', $totalPaidv);
        $this->set('uid', $uid);
    }

    public function admin_statement_delete($statement_id, $uid) {
        $this->loadModel('OrderStatement');
        $this->OrderStatement->deleteAll(array('OrderStatement.statement' => $statement_id), false);
        $this->Session->setFlash(__('Statement deleted'), 'admin_error');

        $this->redirect(array('action' => 'admin_client_order_statement/' . $uid));
    }

    public function admin_client_order_statement_status($uid, $id, $status) {
      //  Configure::write('debug', 2);
	  ini_set('memory_limit', '-1');
        $this->loadModel('OrderStatement');
        $this->layout = "admin_dashboard";
        $this->OrderStatement->updateAll(
                array('OrderStatement.status' => "'" . $status . "'",
                ), array('OrderStatement.user_id' => $uid, 'OrderStatement.id' => $id)
        );
        $this->redirect(array('action' => 'admin_client_order_statement/' . $uid));
    }

    public function admin_client_order_statement($uid) {
        //Configure::write('debug',2);		
        ini_set('memory_limit', '-1');		
        $this->layout = "admin_dashboard";
        $this->loadModel('OrderStatement');
        if ($this->request->is('put') || $this->request->is('post')) {
            $data = array();
            $batch_inoice = time();
            if (!empty($this->request->data['OrderHistory']['batch']) or ! empty($this->request->data['OrderHistory']['returncredit'])) {
                //print_r($this->request->data);die;
                $statment = $basket_id = 'SR-' . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9);
                foreach ($this->request->data['OrderHistory']['batch'] as $invoices) {
                    $data['OrderStatement']['user_id'] = $uid;
                    $data['OrderStatement']['statement'] = $statment;
                    $data['OrderStatement']['order_id'] = $invoices;
                    $data['OrderStatement']['order_type'] = 'order';
                    $data['OrderStatement']['created'] = date("Y-m-d");
                    $this->OrderStatement->create();
                    $this->OrderStatement->save($data);
                }

                foreach ($this->request->data['OrderHistory']['returncredit'] as $invoices) {
                    $data['OrderStatement']['user_id'] = $uid;
                    $data['OrderStatement']['statement'] = $statment;
                    $data['OrderStatement']['order_id'] = $invoices;
                    $data['OrderStatement']['order_type'] = 'returnorder';
                    $data['OrderStatement']['created'] = date("Y-m-d");
                    $this->OrderStatement->create();
                    $this->OrderStatement->save($data);
                }
                $this->Session->setFlash('Statement Added Successfully', 'default');
                $this->redirect(array('action' => 'client_order_statement/' . $uid));
            } else {
                $this->Session->setFlash('<div class="alert alert-error"><strong>Error ! </strong>Please select order.</div>', 'default');
            }
        }

        $oderstatement = $this->OrderStatement->find('list', array('fields' => array('order_id'), 'conditions' => array('OrderStatement.user_id' => $uid, 'OrderStatement.order_type' => 'order')));

        $this->paginate = array(
            'conditions' => array('OrderDist.status' => array('Test', 'Accep'), 'OrderDist.userid' => $uid, 'OrderDist.Paid_Status' => 'unpaid', "NOT" => array("OrderDist.id" => $oderstatement)), 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.Paid_Status', 'OrderDist.po_number', 'OrderDist.status'),
            'limit' => 100000,
            'order' => array(
                'OrderDist.createddt' => 'desc'
            )
        );

        $products = $this->paginate("OrderDist");





        $this->set('users', $products);

        $user_info = $this->User->find('first', array('fields' => array('User.fname', 'User.id', 'User.client_due_datea'), 'conditions' => array('User.id' => $uid)));

        $this->set('user_info', $user_info);




        $totalPaidv = 0;
        $totalPaid = $this->OrderDist->find('all', array('fields' => array('OrderDist.tmporderid'), 'order' => array(
                'OrderDist.id' => 'desc'), 'conditions' => array('OrderDist.userid' => $uid, 'OrderDist.status' => array('Test', 'Accep'), 'OrderDist.Paid_Status' => 'paid')));

        //calculate total unpad
        foreach ($totalPaid as $padamounts) {

            foreach ($padamounts['OrderdetailMany'] as $pdamount) {

                $totalPaidv = ($pdamount['quantity'] * $pdamount['price']) + $totalPaidv;
            }
        }

        $totalUnpaidv = 0;
        $totalUnpaid = $this->OrderDist->find('all', array('fields' => array('OrderDist.tmporderid'), 'order' => array(
                'OrderDist.id' => 'desc'), 'conditions' => array('OrderDist.userid' => $uid, 'OrderDist.status' => array('Test', 'Accep'), 'OrderDist.Paid_Status' => 'unpaid')));
        //calculate total unpad

        foreach ($totalUnpaid as $padamounts) {
            foreach ($padamounts['OrderdetailMany'] as $pdamount) {
                $totalUnpaidv = ($pdamount['quantity'] * $pdamount['price']) + $totalUnpaidv;
            }
        }
        $this->set('totalUnpaid', $totalUnpaidv);
        $this->set('totalPaid', $totalPaidv);
        $this->set('uid', $uid);


        //return credit code
        $this->set('products', $this->Products->find("list", array('conditions' => array("Products.active !='False'"), 'fields' => array('product_ndc', 'productname'))));
        $user_info = $this->User->find('first', array('fields' => array('User.fname', 'User.id', 'User.client_due_datea'), 'conditions' => array('User.id' => $uid)));
        $this->set('user_info', $user_info);



        $oderstatement = $this->OrderStatement->find('list', array('fields' => array('order_id'), 'conditions' => array('OrderStatement.user_id' => $uid, 'OrderStatement.order_type' => 'returnorder')));

        $this->set('CreditReturnlist', $this->CreditReturn->find("all", array('order' => array(
                        'CreditReturn.created' => 'desc'),
                    'conditions' => array("NOT" => array("CreditReturn.id" => $oderstatement), 'CreditReturn.user_id' => $uid, "CreditReturn.status<>'Y'"))));


        $staementlist = $this->OrderStatement->find("all", array('order' => array(
                'OrderStatement.created' => 'desc'),
            'conditions' => array('OrderStatement.user_id' => $uid)));

        $StatementList_t = array();
        $starray = array();
        foreach ($staementlist as $stmt) {
            $price = 0;
            if ($stmt['OrderStatement']['order_type'] == "order") {
                $Orderdetail = $this->Order->find('first', array('recursive' => 2, 'conditions' => array('Order.id' => $stmt['OrderStatement']['order_id'])));
                foreach ($Orderdetail['OrderdetailMany'] as $orderdeatils) {
                    $price = $price + ($orderdeatils['price'] * $orderdeatils['quantity']);
                }
            } else {
                $return_statmement = $this->CreditReturn->find("first", array('recursive' => 2, 'order' => array(
                        'CreditReturn.created' => 'desc'),
                    'conditions' => array("CreditReturn.id" => $stmt['OrderStatement']['order_id'], 'CreditReturn.user_id' => $uid)));
                $price = ($return_statmement['CreditReturn']['quantity'] * $return_statmement['CreditReturn']['product_price']) + $return_statmement['CreditReturn']['restocking_fees'];
                $price = -$price;
            }

            if (array_key_exists($stmt['OrderStatement']['statement'], $starray)) {
                $starray[$stmt['OrderStatement']['statement']] = $starray[$stmt['OrderStatement']['statement']] + $price;
                
            } else {
                $StatementList_t[] = $stmt;
                $starray[$stmt['OrderStatement']['statement']] = $price;
            }
            
        }
      //  exit;
        
       //


        $this->set('starray', $starray);
        $this->set('StatementList', $StatementList_t);
    }

    public function admin_client_order_statement_invoice($uid, $oid) {
        //Configure::write('debug',2);
		ini_set('memory_limit', '-1');
        $this->loadModel('OrderStatement');
        $this->layout = "ajax";
        $orderstatment = $this->OrderStatement->find("list", array('fields' => 'order_id', 'order' => array(
                'OrderStatement.created' => 'desc'),
            'conditions' => array('OrderStatement.user_id' => $uid, 'OrderStatement.order_type' => 'order', 'OrderStatement.statement' => $oid)));
        $Orderdetail = $this->Order->find('all', array('recursive' => 2, 'conditions' => array('Order.id' => $orderstatment)));
        $this->set('Orderdetail', $Orderdetail);

        
        
        
        
        
        
        $orderstatment_return = $this->OrderStatement->find("list", array('fields' => 'order_id', 'order' => array(
                'OrderStatement.created' => 'desc'),
            'conditions' => array('OrderStatement.user_id' => $uid, 'OrderStatement.order_type' => 'returnorder', 'OrderStatement.statement' => $oid)));
        $return_statmement = $this->CreditReturn->find("all", array('recursive' => 2, 'order' => array(
                'CreditReturn.created' => 'desc'),
            'conditions' => array("CreditReturn.id" => $orderstatment_return, 'CreditReturn.user_id' => $uid)));
        foreach ($return_statmement as $key => $rtcr) {

            $return_statmement[$key]['Prodcuts'] = $this->Products->find("first", array('recursive' => 0,
                'conditions' => array("Products.product_ndc like '%" . trim($rtcr['CreditReturn']['product_ndc'] . "'"))));
        }

        $this->set('return_statmement', $return_statmement);

        $orderstatment = $this->OrderStatement->find("first", array('conditions' => array('OrderStatement.user_id' => $uid, 'OrderStatement.statement' => $oid)));
        // print_r($return_statmement);
        $this->set('orderstatment', $orderstatment);
    }

    public function admin_client_order_po_edit($uid) {

        $this->layout = "admin_dashboard";
        if ($this->request->query['order_id'] != '' and $uid != '') {
            $this->OrderDist->updateAll(
                    array('OrderDist.po_number' => "'" . $this->request->query['po_number'] . "'",
                    ), array('OrderDist.id' => $this->request->query['order_id'], 'OrderDist.userid' => $uid)
            );
        }

        $this->redirect(array('action' => 'client_order_history/' . $uid), null, true);
    }
	public function admin_client_order_temporder_edit($uid) {

        $this->layout = "admin_dashboard";
		$count=$this->OrderDist->find('count',array('conditions'=>array('OrderDist.tmporderid'=>$this->request->query['temp_number'])));	
		if($count>0){
			  $this->Session->setFlash(__(' Order already available.'), 'admin_error');
				 $this->redirect(array('action' => 'client_order_history/' . $uid), null, true);
			 }
        if ($this->request->query['tmp_order_id'] != '' and $uid != '') {
            $this->OrderDist->updateAll(
                    array('OrderDist.tmporderid' => "'" . $this->request->query['temp_number'] . "'",
                    ), array('OrderDist.id' => $this->request->query['tmp_order_id'], 'OrderDist.userid' => $uid)
            );
        }
		$this->Session->setFlash(__('Order Id updated '), 'admin_success');
        $this->redirect(array('action' => 'client_order_history/' . $uid), null, true);
    }
	public function admin_client_order_date_edit($uid) {

        $this->layout = "admin_dashboard";
		$created_change=explode("-",$this->request->query['created_change']);
        if ($this->request->query['order_id_crt'] != '' and $uid != '') {
            $this->OrderDist->updateAll(
                    array('OrderDist.createddt' => "'" . $created_change[2].'-'.$created_change[0].'-'.$created_change[1] . "'",
                    ), array('OrderDist.id' => $this->request->query['order_id_crt'], 'OrderDist.userid' => $uid)
            );
        }

        $this->redirect(array('action' => 'client_order_history/' . $uid), null, true);
    }
	
	public function admin_client_order_add_itme($uid) {
            Configure::write('debug',2);
        $this->layout = "admin_dashboard";		
        if ($this->request->query['order_id'] != '' and $uid != '') {
			$ProductInfo = $this->ProductInfo->find('first', array('conditions' => array('ProductInfo.prodid' =>$this->request->query['prodid']),'order' => array(
                'ProductInfo.createddt' => 'desc'
            )));
			$original_price = $ProductInfo['Product']['original_price'];
			$data = array(
                        'orderid' =>$this->request->query['order_id'],
                        'tmporderid' =>$this->request->query['tmporderid'],
                        'productid' =>$this->request->query['prodid'],
                        'quantity' =>$this->request->query['quantity'],
                        'original_p_price' => $original_price,
                        'price' => round($original_price, 2),
                        'createddt' => date("Y-m-d H:i:s"),
                        'orderstatus' => 'Fill',
                        'status' => 'Y'
                    );
                    $this->Orderdetail->create();
                    $this->Orderdetail->save($data);
                    $orderdetailsid = $this->Orderdetail->id;
					
					$data = array(
                                'product_id' =>$this->request->query['prodid'],
								'productinfoid'=>$ProductInfo['ProductInfo']['id'],
								'Orderdetailid'=>$orderdetailsid,
                                'user_id' => $uid,
                                'quantity' =>$this->request->query['quantity'],
                                'price' => round($original_price, 2),
                                'orderid' =>$this->request->query['order_id'],
                                'tmporderid' =>$this->request->query['tmbatch_amountporderid'],
                                'lot_no' => $ProductInfo['ProductInfo']['batchno'],
                                'create_date' => date("Y-m-d H:i:s"),
                                'status' => 'N'
                            );
                            //print_r($data);die;
                            $this->OrderLot->create();
                            $this->OrderLot->save($data);
                            $lotid = $this->OrderLot->id;
                            $this->Session->setFlash('Add Item Successfully', 'default');
        }

        $this->redirect(array('action' => 'client_order_history/' . $uid), null, true);
    }

    public function admin_client_order_paid_batch($uid, $batch_no) {
        $this->layout = "admin_dashboard";

        if ($this->request->is('put') || $this->request->is('post')) {
            // Configure::write('debug',2);
            $this->OrderDist->updateAll(
                    array('OrderDist.Paid_Status' => "'paid'",
                    ), array('OrderDist.batch_no' => $batch_no, 'OrderDist.userid' => $uid)
            );
            $this->CustomerField->updateAll(
                    array('CustomerField.batch_invoice_status' => "'paid'",
                    ), array('CustomerField.batch_invoice' => $batch_no, 'CustomerField.user_id' => $uid)
            );
            $this->Session->setFlash('Batch Pay Successfully', 'default');
            $this->redirect(array('action' => 'client_users'));
        }
        $this->paginate = array(
            'conditions' => array('OrderDist.status' => array('Test', 'Accep'), 'OrderDist.userid' => $uid, 'OrderDist.Paid_Status' => 'unpaid', 'OrderDist.batch_no' => $batch_no), 'fields' => array('OrderDist.id', 'OrderDist.tmporderid', 'OrderDist.userid', 'OrderDist.createddt', 'OrderDist.status', 'OrderDist.Paid_Status', 'OrderDist.po_number'),
            'order' => array(
                'OrderDist.createddt' => 'desc'
            )
        );

        $products = $this->paginate("OrderDist");
        $this->set('users', $products);

        $user_info = $this->User->find('first', array('fields' => array('User.fname', 'User.id'), 'conditions' => array('User.id' => $uid)));
        $this->set('user_info', $user_info);




        $totalPaidv = 0;
        $totalPaid = $this->OrderDist->find('all', array('fields' => array('OrderDist.tmporderid'), 'order' => array(
                'OrderDist.id' => 'desc'), 'conditions' => array('OrderDist.userid' => $uid, 'OrderDist.status' => array('Test', 'Accep'), 'OrderDist.Paid_Status' => 'paid')));

        //calculate total unpad
        foreach ($totalPaid as $padamounts) {

            foreach ($padamounts['OrderdetailMany'] as $pdamount) {

                $totalPaidv = ($pdamount['quantity'] * $pdamount['price']) + $totalPaidv;
            }
        }

        $totalUnpaidv = 0;
        $totalUnpaid = $this->OrderDist->find('all', array('fields' => array('OrderDist.tmporderid'), 'order' => array(
                'OrderDist.id' => 'desc'), 'conditions' => array('OrderDist.userid' => $uid, 'OrderDist.status' => array('Test', 'Accep'), 'OrderDist.Paid_Status' => 'unpaid')));
        //calculate total unpad

        foreach ($totalUnpaid as $padamounts) {
            foreach ($padamounts['OrderdetailMany'] as $pdamount) {
                $totalUnpaidv = ($pdamount['quantity'] * $pdamount['price']) + $totalUnpaidv;
            }
        }
        $this->set('totalUnpaid', $totalUnpaidv);
        $this->set('totalPaid', $totalPaidv);
        $this->set('uid', $uid);
        $this->set('batch_no', $batch_no);
    }

    public function admin_invoice($oid) {
        $this->layout = "ajax";

        $Orderdetail = $this->Order->find('first', array('recursive' => 2, 'conditions' => array('Order.id' => $oid)));
        print_r($Orderdetail);		
        $this->set('Orderdetail', $Orderdetail);
    }
	public function admin_split_invoice($oid,$sno) {
        $this->layout = "ajax";

        $Orderdetail = $this->Order->find('first', array('recursive' => 2, 'conditions' => array('Order.id' => $oid)));
        //print_r($Orderdetail);	
		//Configure::write('debug',2);
		$this->set('sno', $sno);	
        $this->set('Orderdetail', $Orderdetail);
    }
	public function admin_client_invoice_split($oid,$cid) {
         $this->layout = "admin_dashboard";

        $Orderdetail = $this->Order->find('first', array('recursive' => 2, 'conditions' => array('Order.id' => $oid)));
        //print_r($Orderdetail);		
        $this->set('Orderdetail', $Orderdetail);
    }

    public function admin_invoice_CreditMemo($oid) {
        $this->layout = "ajax";
        //Configure::write('debug',2); 
        $Orderdetail = $this->CreditReturn->find('first', array('recursive' => 2, 'conditions' => array('CreditReturn.id' => $oid)));
        //print_r($Orderdetail);		
        $this->set('Orderdetail', $Orderdetail);
        $this->set('products', $this->Products->find("first", array('conditions' => array("Products.product_ndc  like '%" . $Orderdetail['CreditReturn']['product_ndc'] . "'"))));
        $this->set('User', $this->User->find("first", array('conditions' => array("User.id" => $Orderdetail['CreditReturn']['user_id']))));
    }

    public function admin_invoicepdf($id) {
        if (!$id) {
            $this->Session->setFlash('Sorry, there was no property ID submitted.');
            $this->redirect(array('action' => 'index'), null, true);
        }
        Configure::write('debug', 0); // Otherwise we cannot use this method while developing 

        $id = intval($id);

        $property = 'test'; // here the data is pulled from the database and set for the view 

        if (empty($property)) {
            $this->Session->setFlash('Sorry, there is no property with the submitted ID.');
            $this->redirect(array('action' => 'index'), null, true);
        }
        $this->set('id', $id);
        $this->layout = 'pdf'; //this will use the pdf.ctp layout 
        $this->render();
    }
	
	public function admin_invoicepdf_split($id,$sno) {
        if (!$id) {
            $this->Session->setFlash('Sorry, there was no property ID submitted.');
            $this->redirect(array('action' => 'index'), null, true);
        }
        Configure::write('debug', 0); // Otherwise we cannot use this method while developing 

        $id = intval($id);

        $property = 'test'; // here the data is pulled from the database and set for the view 

        if (empty($property)) {
            $this->Session->setFlash('Sorry, there is no property with the submitted ID.');
            $this->redirect(array('action' => 'index'), null, true);
        }
        $this->set('id', $id);
		$this->set('sno', $sno);
        $this->layout = 'pdf'; //this will use the pdf.ctp layout 
        $this->render();
    }

    public function admin_order_item_delete($order_detail_ID, $order_id) {
        $this->layout = "admin_dashboard";
        $this->Orderdetail->deleteAll(array('Orderdetail.id' => $order_detail_ID), false);
        $this->OrderLot->deleteAll(array('OrderLot.Orderdetailid' => $order_detail_ID), false);
        $this->Session->setFlash(__('Lot Delted Successfuly .'), 'admin_success');
        $this->redirect(array('action' => 'client_orderinfo/' . $order_id));
    }

    public function admin_client_orderinfo($oid) {
        $this->layout = "admin_dashboard";
        //Configure::write('debug',2);
        //$this->set('title', __('Welcome Admin Panel'));
        //$this->set('description', __('Manage Users'));

        $Orderdetail = $this->OrderDist->find('all', array('recursive' => 2, 'conditions' => array('OrderDist.id' => $oid)));

        foreach ($Orderdetail[0]['OrderdetailMany'] as $mkey => $ordersd) {
            $Orderdetail_lot = $this->OrderLot->find('all', array('recursive' => -1, 'fields' => array('id', 'product_id', 'quantity', 'lot_no', 'price', 'productinfoid', 'Orderdetailid', 'tmporderid', 'exchange_lot', 'exchange_lot_id'), 'conditions' => array('OrderLot.Orderdetailid' => $ordersd['id'])));

            foreach ($Orderdetail_lot as $key => $detail) {

                $productifo_id = ($detail['OrderLot']['exchange_lot_id'] != '') ? $detail['OrderLot']['exchange_lot_id'] : $detail['OrderLot']['productinfoid'];
                $prodetajl = $this->ProductInfo->find('first', array('recursive' => -1, 'conditions' => array('ProductInfo.id' => $productifo_id)));
                $Orderdetail_lot[$key]['product_details'] = $prodetajl;
            }
            $Orderdetail[0]['OrderdetailMany'][$mkey]['lot_details'] = $Orderdetail_lot;
        }
        //print_r($Orderdetail);die;	
        $this->set('Orderdetail', $Orderdetail);
        $this->set('Orderdetailid', $oid);

        //Update STOCK
        if ($this->request->is('put') || $this->request->is('post')) {
            //print_r($this->request->data);die;
            foreach ($this->request->data['User'] as $key => $lost) {
                if ($key != 'exchange') {
                    $lotid = trim(str_replace('lot', '', $key));
                    $productinfodetilas = $this->ProductInfo->find('first', array('fields' => array('batchno'), 'conditions' => array('ProductInfo.id' => $lost)));
                    Configure::write('debug', 2);
                    $this->OrderLot->updateAll(
                            array('OrderLot.exchange_lot' => "'" . $productinfodetilas['ProductInfo']['batchno'] . "'",
                        'OrderLot.exchange_lot_id' => $lost
                            ), array('OrderLot.id' => $lotid)
                    );
                }
            }

            $this->Session->setFlash(__('Lot Exchanged Successfuly .'), 'admin_success');
            $this->redirect(array('action' => 'admin_client_orderinfo/' . $oid));
        }
    }

	    public function admin_client_orderinfo_filtersheet($oid) {
        $this->layout = "admin_dashboard";
        //Configure::write('debug',2);
        //$this->set('title', __('Welcome Admin Panel'));
        //$this->set('description', __('Manage Users'));

        $Orderdetail = $this->OrderDist->find('all', array('recursive' => 2, 'conditions' => array('OrderDist.id' => $oid)));

        foreach ($Orderdetail[0]['OrderdetailMany'] as $mkey => $ordersd) {
            $Orderdetail_lot = $this->OrderLot->find('all', array('recursive' => -1, 'fields' => array('id', 'product_id', 'quantity', 'lot_no', 'price', 'productinfoid', 'Orderdetailid', 'tmporderid', 'exchange_lot', 'exchange_lot_id'), 'conditions' => array('OrderLot.Orderdetailid' => $ordersd['id'])));

            foreach ($Orderdetail_lot as $key => $detail) {

                $productifo_id = ($detail['OrderLot']['exchange_lot_id'] != '') ? $detail['OrderLot']['exchange_lot_id'] : $detail['OrderLot']['productinfoid'];
                $prodetajl = $this->ProductInfo->find('first', array('recursive' => -1, 'conditions' => array('ProductInfo.id' => $productifo_id)));
                $Orderdetail_lot[$key]['product_details'] = $prodetajl;
            }
            $Orderdetail[0]['OrderdetailMany'][$mkey]['lot_details'] = $Orderdetail_lot;
        }
        //print_r($Orderdetail);die;	
        $this->set('Orderdetail', $Orderdetail);
        $this->set('Orderdetailid', $oid);

        //Update STOCK
        if ($this->request->is('put') || $this->request->is('post')) {
            //print_r($this->request->data);die;
            foreach ($this->request->data['User'] as $key => $lost) {
                if ($key != 'exchange') {
                    $lotid = trim(str_replace('lot', '', $key));
                    $productinfodetilas = $this->ProductInfo->find('first', array('fields' => array('batchno'), 'conditions' => array('ProductInfo.id' => $lost)));
                    Configure::write('debug', 2);
                    $this->OrderLot->updateAll(
                            array('OrderLot.exchange_lot' => "'" . $productinfodetilas['ProductInfo']['batchno'] . "'",
                        'OrderLot.exchange_lot_id' => $lost
                            ), array('OrderLot.id' => $lotid)
                    );
                }
            }

            $this->Session->setFlash(__('Lot Exchanged Successfuly .'), 'admin_success');
            $this->redirect(array('action' => 'admin_client_orderinfo/' . $oid));
        }
    }
    public function admin_client_view_pedigree($pedgreeid, $tmporderid) {
        $this->layout = "admin_dashboard";
        $prodetajl = $this->Pedigree->find('first', array('recursive' => 2, 'conditions' => array('Pedigree.product_infos_id' => $pedgreeid)));
        $this->set("dhtml", $prodetajl);
        $Orderdetail = $this->Order->find('first', array('recursive' => 2, 'conditions' => array('Order.tmporderid' => $tmporderid)));
        $this->set("Orderdetail", $Orderdetail);
        $this->set("pedgreeid", $pedgreeid);
        $this->set("tmporderid", $tmporderid);
        //Lot informations
        $lotdetails = $this->OrderLot->find('all', array('fields' => array('id', 'product_id', 'quantity', 'lot_no', 'price', 'productinfoid', 'exchange_lot', 'exchange_lot_id', 'tmporderid'), 'conditions' => array('OR' => array('OrderLot.productinfoid' => $pedgreeid, 'OrderLot.exchange_lot_id' => $pedgreeid), 'OrderLot.tmporderid' => $tmporderid)));

        $lot_qunetiony = 0;
        foreach ($lotdetails as $lots) {
            $lot_qunetiony = $lot_qunetiony + $lots['OrderLot']['quantity'];
        }

        $this->set('lot_qunetiony', $lot_qunetiony);

        if ($prodetajl['Pedigree']['anda'])
            $this->view = 'admin_client_view_pedigree_anda';
    }

    public function admin_edit_pedigree($pedgreeid = NULL, $tmporderid = NULL) {
        $this->layout = "admin_dashboard";

        if ($this->request->is('put') || $this->request->is('post')) {
            //Configure::write('debug',2);
            $this->Pedigree->updateAll(
                    array('Pedigree.custom_change_sz' => "'" . json_encode($this->request->data) . "'"), array('Pedigree.product_infos_id' => $this->request->data['Users']['pedgreeid'])
            );
            $pedgreeid = $this->request->data['Users']['pedgreeid'];
            $tmporderid = $this->request->data['Users']['tmporderid'];
            $this->Session->setFlash(__('The Pedigree Update Successfully.'), 'admin_success');

            $this->redirect(array('action' => 'admin_client_view_pedigree/' . $pedgreeid . '/' . $tmporderid));
        }
        $prodetajl = $this->Pedigree->find('first', array('recursive' => 2, 'conditions' => array('Pedigree.product_infos_id' => $pedgreeid)));
        //print_r($prodetajl);
        $this->set("dhtml", $prodetajl);
        $Orderdetail = $this->Order->find('first', array('recursive' => 2, 'conditions' => array('Order.tmporderid' => $tmporderid)));

        //Lot informations
        $lotdetails = $this->OrderLot->find('all', array('fields' => array('id', 'product_id', 'quantity', 'lot_no', 'price', 'productinfoid', 'exchange_lot', 'exchange_lot_id', 'tmporderid'), 'conditions' => array('OR' => array('OrderLot.productinfoid' => $pedgreeid, 'OrderLot.exchange_lot_id' => $pedgreeid), 'OrderLot.tmporderid' => $tmporderid)));

        $lot_qunetiony = 0;
        foreach ($lotdetails as $lots) {
            $lot_qunetiony = $lot_qunetiony + $lots['OrderLot']['quantity'];
        }

        $this->set('lot_qunetiony', $lot_qunetiony);
        $this->set("Orderdetail", $Orderdetail);
        $this->set("pedgreeid", $pedgreeid);
        $this->set("tmporderid", $tmporderid);
    }

    public function admin_client_manual_pedigree($pedgreeid, $tmporderid) {

        $this->layout = "admin_dashboard";

        $Orderdetail = $this->Order->find('first', array('recursive' => 2, 'conditions' => array('Order.tmporderid' => $tmporderid)));
        $this->set("Orderdetail", $Orderdetail);

        $prodetajl = $this->ProductInfo->find('first', array('conditions' => array('ProductInfo.id' => $pedgreeid)));

        $this->set("dhtml", $prodetajl);

        //Lot informations

        $lotdetails = $this->OrderLot->find('all', array('fields' => array('id', 'product_id', 'quantity', 'lot_no', 'price', 'productinfoid', 'exchange_lot', 'exchange_lot_id', 'tmporderid'), 'conditions' => array('OrderLot.productinfoid' => $pedgreeid, 'OrderLot.tmporderid' => $tmporderid)));
        if (empty($lotdetails)) {
            $lotdetails = $this->OrderLot->find('all', array('fields' => array('id', 'product_id', 'quantity', 'lot_no', 'price', 'productinfoid', 'exchange_lot', 'exchange_lot_id', 'tmporderid'), 'conditions' => array('OrderLot.exchange_lot_id' => $pedgreeid, 'OrderLot.tmporderid' => $tmporderid)));
        }
        //count purchase quenstity for this log
        $lot_qunetiony = 0;
        foreach ($lotdetails as $lots) {
            $lot_qunetiony = $lot_qunetiony + $lots['OrderLot']['quantity'];
        }

        $this->set('lot_qunetiony', $lot_qunetiony);
        $this->set("lotdetails", $lotdetails);
    }

    public function admin_order_delete($oid) {
        $this->layout = "admin_dashboard";
        //Configure::write('debug',2);
        /* $this->Order->updateAll(
          array('Order.status' => "'Test'",'confirmat'=>date("Y-m-d")),
          array('Order.id' => $oid)
          );
         */

        $order_lot = $this->OrderLot->find('all', array('fields' => array('productinfoid', 'orderid', 'Orderdetailid', 'quantity', 'id', 'product_id', 'user_id', 'product_id'), 'conditions' => array('OrderLot.orderid	' => $oid)));
        //print_r($order_lot);die;

        $previous_product = '';
        $clinet_id = '';
        foreach ($order_lot as $lot) {
            $findoldavai = $this->ProductInfo->find('first', array('conditions' => array('ProductInfo.id' => $lot['OrderLot']['productinfoid'])));
            $clinet_id = $lot['OrderLot']['user_id'];
            //print_r($findoldavai);die;
            $this->ProductInfo->updateAll(
                    array('ProductInfo.availability' => $findoldavai['ProductInfo']['availability'] + $lot['OrderLot']['quantity']), array('ProductInfo.id' => $lot['OrderLot']['productinfoid'])
            );

            $this->OrderLot->deleteAll(array('OrderLot.id' => $lot['OrderLot']['id']), false);

            $activit_data = array(
                'product_id' => $lot['OrderLot']['product_id'],
                'product_info_id' => $lot['OrderLot']['productinfoid'],
                'user_id' => $this->Auth->user('id'),
                'previous_qty' => $findoldavai['ProductInfo']['availability'],
                'new_qty' => $findoldavai['ProductInfo']['availability'] + $lot['OrderLot']['quantity'],
                'note' => 'Delete Order  and items from that invoice has to go back to inventory',
                'created' => date("Y-m-d H:i:s")
            );
            //print_r($activit_data);die;		
            $this->admin_add_product_activity($activit_data);
        }

        $this->Order->id = $oid;

        $this->Order->delete();

        //$this->Order->deleteAll(array('Order.id' => $oid), false);
        //echo $clinet_id;die;
        $this->Session->setFlash(__('The Order Successfuly Cancel.'), 'admin_success');

        $this->redirect(array('action' => 'admin_client_order_history/' . $clinet_id));
    }

    public function admin_add_product_activity($data) {
        $this->ProductActivity->create();

        if ($this->ProductActivity->save($data)) {
            return true;
        }
    }

    public function admin_client_paid($oid, $uid) {
        $this->layout = "admin_dashboard";

        if ($oid != '' and $uid != '') {
            $this->Order->updateAll(
                    array('Order.Paid_Status' => "'paid'", 'confirmat' => date("Y-m-d")), array('Order.id' => $oid)
            );
        }
        $this->Session->setFlash(__('The Order Successfuly Paid.'), 'admin_success');

        $this->redirect(array('action' => 'admin_client_order_history/' . $uid));
    }

    public function admin_client_unpaid($oid, $uid) {
        $this->layout = "admin_dashboard";

        if ($oid != '' and $uid != '') {
            $this->Order->updateAll(
                    array('Order.Paid_Status' => "'unpaid'", 'confirmat' => date("Y-m-d")), array('Order.id' => $oid)
            );
        }
        $this->Session->setFlash(__('The Order Successfuly pending .'), 'admin_success');

        $this->redirect(array('action' => 'admin_client_order_history/' . $uid));
    }

    public function admin_lot_exchange() {
        if ($this->request->isAjax()) {
            $this->layout = 'ajax';

            $Orderdetail = $this->OrderLot->find('all', array('fields' => array('id', 'product_id', 'quantity', 'lot_no', 'price', 'productinfoid', 'exchange_lot', 'exchange_lot_id'), 'conditions' => array('OrderLot.Orderdetailid' => $this->request->data['order_id'])));
            //print_r($Orderdetail);
            foreach ($Orderdetail as $key => $detail) {
                $prodetajl = $this->ProductInfo->find('all', array('conditions' => array('ProductInfo.id' => $detail['OrderLot']['productinfoid'])));
                $Orderdetail[$key]['product_details'] = $prodetajl;
            }

            $product_id = $Orderdetail[0]['OrderLot']['product_id'];
            $producterlistps = $this->ProductInfo->find('all', array('fields' => array('id', 'batchno', 'VendorName',"InvoiceNo"), 'conditions' => array('ProductInfo.prodid' => $product_id)));
            $producterlist = array();

            foreach ($producterlistps as $producterlistp) {
                $producterlist[$producterlistp['ProductInfo']['id']] = $producterlistp['ProductInfo']['batchno'] . '(' . $producterlistp['ProductInfo']['VendorName'] . ')' . "(" . $producterlistp['ProductInfo']['InvoiceNo'] . ")";
            }

            $this->set('productlist', $producterlist);
            $this->set('Orderdetail', $Orderdetail);
            $this->set('oid', $this->request->data['userid']);
        }
    }

    public function admin_return_credit($user_id = NULL) {
        //Configure::write('debug',3);
        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));
        
        
        $this->set('products', $this->Products->find("list", array('conditions' => array("Products.active !='False'"), 'fields' => array('product_ndc', 'productname'))));
        $user_info = $this->User->find('first', array('fields' => array('User.fname', 'User.id'), 'conditions' => array('User.id' => $user_id)));
        $this->set('user_info', $user_info);


        if ($this->request->is('post')) {
            
            foreach ($this->request->data['prodid'] as $key => $val) {
                if ($val != '') {
                    $return_data = array();
                    $basket_id = 'CR-' . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9);
                    $return_data['CreditReturn']['user_id'] = $user_id;
                    $return_data['CreditReturn']['credit_number'] = $basket_id;
                    $return_data['CreditReturn']['product_ndc'] = $val;
                    $return_data['CreditReturn']['product_lot'] = $this->request->data['product_lot'][$key];
                    $return_data['CreditReturn']['lot_expiry'] = $this->request->data['lot_expiry'][$key];
                    $return_data['CreditReturn']['product_price'] = $this->request->data['product_price'][$key];
                    $return_data['CreditReturn']['quantity'] = $this->request->data['quantity'][$key];
                    $return_data['CreditReturn']['restocking_fees'] = $this->request->data['restocking_fees'][$key];
                    $this->CreditReturn->create();
                    if ($this->CreditReturn->save($return_data)) {
                        
                    }
                }
            }
            $this->Session->setFlash(__('Return Added '), 'admin_success');
        }
        
        
        $this->paginate = array(
            'order' => array(
                'CreditReturn.created' => 'desc', 'limit' => 20),
            'conditions' => array('CreditReturn.user_id' => $user_id)
        );
        $this->set('CreditReturnlist', $this->paginate("CreditReturn"));
    }

    function admin_return_credit_pay($returnid, $user_id) {
        Configure::write('debug', 3);
        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));
        $this->CreditReturn->updateAll(
                array('CreditReturn.status' => "'Y'",
                ), array('CreditReturn.id' => $returnid)
        );
        $this->Session->setFlash(__('Paid Successfully '), 'admin_success');
        $this->redirect(array('action' => 'admin_return_credit/' . $user_id));
    }
    /**
     * @category
     * @param 
     * @author Atul Mishra 
     */
    public function admin_checked(){
        $this->layout= "ajax";
        $id     = $this->params["pass"][0];
        $value  = $this->params["pass"][1];
       
        $this->loadModel("OrderDist");
        $this->OrderDist->id=$id;
        if($value==1){
            $val = 0;
        }else if($value==0){
            $val = 1;
        }
        if($this->OrderDist->updateAll(array("OrderDist.checked"=>$val),array("OrderDist.id" => $id))){
            echo json_encode(array("status"=>1,"value"=>$val));
        }else{
            echo json_encode(array("status"=>0,"value"=>$val));
        }
        exit;
    }
	
	public function admin_advertise($id = NULL) {
       // Configure::write('debug',3);
        $this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));
		$this->loadModel('Advertise');
		
        $this->set('Users', $this->User->find("list", array('conditions' => array("User.level"=>'client'), 'fields' => array('id', 'fname'))));
       
		$this->Advertise->id;
        if ($this->request->is('post') or $this->request->is('put')) {
			
			$this->Advertise->create();
			$Adertise=array();
			$Adertise['advertise']=$this->request->data['Advertise']['advertise'];
			$Adertise['user_ids']=implode(",",$this->request->data['Advertise']['user_ids']);
			$Adertise['status']=$this->request->data['Advertise']['status'];
			$Adertise['id']=$this->request->data['Advertise']['id'];
			$Adertise['created']=$this->request->data['Advertise']['created'];
			
				if(!empty($this->request->data['Advertise']['advertise_img']))
                    {
						
                        $file = $this->request->data['Advertise']['advertise_img']; //put the data into a var for easy use

                        $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extensiondie;
					   
                        $arr_ext = array('jpg', 'jpeg', 'gif','png'); //set allowed extensions
						
                        //only process if the extension is valid
                        if(in_array($ext, $arr_ext))
                        {
                            //do the actual uploading of the file. First arg is the tmp name, second arg is
                            //where we are putting it
							//echo WWW_ROOT . '/img/advertise/' . $file['name'];die;
                            move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/advertise/' . $file['name']);

                            //prepare the filename for database entry
                            $Adertise['image'] = $file['name'];
                        }
                    }

			
			 if ($this->Advertise->save($Adertise)) {
            $this->Session->setFlash(__('Advertise Added '), 'admin_success');
			 }else{
				 $this->Session->setFlash(__('Error Add.'), 'admin_error');
			 }
			 $this->redirect('advertise');
        }
		
        $this->paginate = array(
            'order' => array(
                'Advertise.created' => 'desc', 'limit' => 20),
            
        );
		$this->request->data = $this->Advertise->read(null, $id);
		$this->request->data['Advertise']['user_ids']=explode(",",$this->request->data['Advertise']['user_ids']);	
			
        $this->set('Advertise', $this->paginate("Advertise"));
    }
	
	public function admin_advertise_delete($id = null) {
		$this->loadModel('Advertise');
        if (!$this->request->is('post')) {

            throw new MethodNotAllowedException();
        }

        $this->Advertise->id = $id;

        if (!$this->Advertise->exists()) {

            throw new NotFoundException(__('Invalid id'));
        }

        if ($this->Advertise->delete()) {

            $this->Session->setFlash(__('Advertise deleted'), 'admin_success');

            $this->redirect(array('action' => 'advertise'));
        }

        $this->Session->setFlash(__('Advertise was not deleted'), 'admin_error');

        $this->redirect(array('action' => 'advertise'));
    }
	
	    public function admin_order_copy($oid,$uid) {
        $this->layout = "admin_dashboard";
		//Configure::write('debug',3);
                $gmessage="";
		$copty_order=false;
		$remianing_quentity=array();
		$user_info=$this->User->find("first",array('conditions'=>array('id'=>$uid)));
		
		$Orders=$this->Order->find("first",array('conditions'=>array('Order.id'=>$oid)));
		
		//get order id from basket
		$Orders_basket=$this->Order->find("first",array('conditions'=>array('Order.status'=>'Fill','Order.userid'=>$user_info['User']['id'])));
		///print_r($Orders_basket);die;
		if(empty($Orders_basket)){
		$order_id='';
		$basket_id = rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9);
                
		}else{
			$order_id = $Orders_basket['Order']['id'];
			$basket_id = $Orders_basket['Order']['tmporderid'];
		}
		  debug($basket_id);
		foreach($Orders['OrderdetailMany'] as $keyord=>$Orderdetail){
			
                $Orderdetail_basket = $this->Orderdetail->find("first",array('conditions'=>array('Orderdetail.orderid'=>$order_id,'Orderdetail.productid'=>$Orderdetail['productid'])));
		

                //print_r($Orderdetail_basket);die;
                    if(empty($Orderdetail_basket)){
		$orderdetailsid='';
		$previous_qty=0;
		}else{
                    $orderdetailsid=$Orderdetail_basket['Orderdetail']['id'];
                    $previous_qty=$Orderdetail_basket['Orderdetail']['quantity'];
		}
			$this->paginate = array(
                            'conditions' => array('Products.id' => $Orderdetail['productid'])
                        );
            
                        
                        $products = $this->paginate("Products");  
            
            
            foreach ($products as $key => $product) {
                $product_instokc = 0;
				$instock=0;
                foreach ($product['ProductInfo'] as $ProductInfo) {
                    $instock = $instock + $ProductInfo['availability'];
                }
                $product_instokc = $instock;
            }
                if($order_id==''){
                        $data = array(
                                'tmporderid' => $basket_id,
                                'createddt' => date("Y-m-d H:i:s"),
                                'status' => 'Fill',
                                'Paid_Status' => 'unpaid',
                                'userid' => $user_info['User']['id']
                        );

                        $this->Order->create();
                        $this->Order->save($data);
                        $order_id = $this->Order->id;
                }
				   
                    if($orderdetailsid==''){
                                        $data = array(
                                'orderid' => $order_id,
                                'tmporderid' => $basket_id,
                                'productid' => $Orderdetail['productid'],
                                'quantity' => $Orderdetail['quantity'],
                                'original_p_price' => $Orderdetail['original_p_price'],
                                'price' => $Orderdetail['price'],
                                'createddt' => date("Y-m-d H:i:s"),
                                'orderstatus' => 'Fill',
                                'status' => 'Y'
                            );
                        debug('he');

                        $this->Orderdetail->create();
                        $this->Orderdetail->save($data);
                        $orderdetailsid = $this->Orderdetail->id;
                    }else{
                        debug('else1');
                             $this->Orderdetail->updateAll(
                                array('Orderdetail.createddt' => "'".date("Y-m-d H:i:s")."'", 'Orderdetail.quantity' => ($previous_qty+$Orderdetail['quantity'])), array('Orderdetail.id' => $orderdetailsid)
                            );
                    }
			//exit;   
                        $product_needed_remaning = $Orderdetail['quantity']; //15
                        
                        
                foreach ($products as $key => $product) {
                    //firs jdp strat lot
                    if ($user_info['User']['JDLotPriority'])
                        $order_array = array('ProductInfo.Pedigree' => 'ASC');
                    else
                    $order_array = array('ProductInfo.expdate' => 'ASC');
                    $prodcutinfo_all = $this->ProductInfo->find('all', array('order' => $order_array, 'conditions' => array('ProductInfo.prodid' => $product['Product']['id'])));
                    //print_r($prodcutinfo_all);die;
                    foreach ($product['ProductInfo'] as $ProductInfo) {
                        $totalprdoucavalibalilty = ($ProductInfo['balance'] > 0) ? $ProductInfo['balance'] : $ProductInfo['availability'];
                        if ($ProductInfo['availability'] > 0) {
                            $available_product_quaanty = $ProductInfo['availability']; //10//30
                            $remaintin_product = $available_product_quaanty - $product_needed_remaning; //10-15=-5//30-5=25
                            $podcut_alloted = ($remaintin_product >= 0) ? $product_needed_remaning : $available_product_quaanty; //15-5=10//15-0
                            $availabproduct_update = ($remaintin_product < 0) ? 0 : $remaintin_product; //0//25
                            $product_needed_remaning = ($remaintin_product >= 0) ? 0 : $remaintin_product; //-5//0
                            $product_needed_remaning = -1 * $product_needed_remaning; //5//0
                            //$podcut_alloted=($product_needed-$product_needed_remaning);//15-5=10//15-0
                            //Alot lot
                            //update productinfo table
                            $this->ProductInfo->id = $ProductInfo['id'];

                            $this->ProductInfo->saveField('balance', $totalprdoucavalibalilty);
                            $this->ProductInfo->saveField('availability', $availabproduct_update);


                            //Apply price discount accourding client base;
                            $productprice = $product['Product']['productprice'];
                            $purchasePrice = $product['Product']['PurchasePrice'];
                            $product_prafit = $productprice - $purchasePrice;
                            $discount = ($user_info['User'][$product['Product']['producttype'] . "_discount"] / 100);
                            $profit_percent = $product_prafit * $discount;

                            $priceafter_discount = ($profit_percent > 0) ? round(($productprice - $profit_percent), 2) : $productprice;
                            if ($user_info['User']['threpercenteoff'])
                                $priceafter_discount = $priceafter_discount - ($priceafter_discount * $$threpercenteoff / 100);
                            //update lot nubmer
                            $data = array(
                                'product_id' =>$Orderdetail['productid'],
                                'user_id' => $user_info['User']['id'],
                                'quantity' => $podcut_alloted,
                                'price' =>$Orderdetail['price'],
                                'orderid' => $order_id,
                                'tmporderid' =>$basket_id,
                                'lot_no' => $ProductInfo['batchno'],
                                'create_date' => date("Y-m-d H:i:s"),
                                'status' => 'N'
                            );
                        //    print_r($data);die;
                            $this->OrderLot->create();
                            $this->OrderLot->save($data);
                            $lotid = $this->OrderLot->id;

                            
                            $copty_order=true;
                            $this->OrderLot->updateAll(
                                array('OrderLot.tmporderid' => $basket_id, 'OrderLot.productinfoid' => $ProductInfo['id'], 'OrderLot.orderid' => $order_id, 'Orderdetailid' => $orderdetailsid), array('OrderLot.id' => $lotid)
                            );
                            
                            if ($product_needed_remaning == 0)
                                break;
                        }
                    }
                    $product_instokc = $instock;
                }
						
		}
        
                
			if($gmessage!=''){
				 $this->Session->setFlash(__($gmessage), 'error', array('class' => 'alert-error'));       
			}else{
				 $this->Session->setFlash(__('The Order Successfuly Copyed.'), 'admin_success');       
			}
			 $this->redirect(array('action' => 'client_order_history/'.$uid));
    }

}

?>