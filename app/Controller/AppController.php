<?php

/**

 * Application level Controller

 *

 * This file is application-wide controller file. You can put all

 * application-wide controller-related methods here.

 *

 * PHP 5

 *

 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)

 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)

 *

 * Licensed under The MIT License

 * Redistributions of files must retain the above copyright notice.

 *

 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)

 * @link          http://cakephp.org CakePHP(tm) Project

 * @package       app.Controller

 * @since         CakePHP(tm) v 0.2.9

 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)

 */



App::uses('Controller', 'Controller');



/**

 * Application Controller

 *

 * Add your application-wide methods in the class below, your controllers

 * will inherit them.

 *

 * @package       app.Controller

 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller

 */

class AppController extends Controller {

	

	/*public $components = array(

            'Auth' => array(

                'authenticate' => array(

                    'Form' => array(

                        'fields' => array('username' =>'username'),

                        'scope'  => array('User.status' => 1)

                    )

                ),
/*
                'authorize' => array(

                    'Actions' => array('actionPath' => 'controllers')

               ) 

			   

            ),

            'Session'

        );*/


public $components = array(
  'Auth' => array(
    'authorize' => array('Controller',)
   
), 'Session');
    public $helpers = array(

        'Session',

        'Form',

        'Html',

        'Cache',

        'Js',

        'Time'

    );

	

	

   function beforeFilter() {


		
        if ($this->request->prefix == 'admin') {
            $this->layout = 'admin';
            // Specify which controller/action handles logging in:
            AuthComponent::$sessionKey = 'Auth.Admin'; // solution from http://stackoverflow.com/questions/10538159/cakephp-auth-component-with-two-models-session
            $this->Auth->loginAction = array('controller'=>'admin','action'=>'login');
            $this->Auth->loginRedirect = array('controller'=>'admin','action'=>'index');
            $this->Auth->logoutRedirect = array('controller'=>'admin','action'=>'login');
            $this->Auth->authenticate = array(
                'Form' => array(
                    'userModel' => 'User',
                )
            );
            $this->Auth->allow('login');

        } else if ($this->params['controller'] == 'Sales') {
			
            // Specify which controller/action handles logging in:
            AuthComponent::$sessionKey = 'Auth.Sales'; // solution from http://stackoverflow.com/questions/10538159/cakephp-auth-component-with-two-models-session
            $this->Auth->loginAction = array('controller'=>'Sales','action'=>'login');
			$this->Auth->loginRedirect = array('controller'=>'Sales','action'=>'index');
            $this->Auth->logoutRedirect = array('controller'=>'Sales','action'=>'login');

         } else if ($this->params['controller'] == 'Dataentry') {
			
            // Specify which controller/action handles logging in:
            AuthComponent::$sessionKey = 'Auth.dataentry'; // solution from http://stackoverflow.com/questions/10538159/cakephp-auth-component-with-two-models-session
            $this->Auth->loginAction = array('controller'=>'Dataentry','action'=>'login');
			$this->Auth->loginRedirect = array('controller'=>'Dataentry','action'=>'index');
            $this->Auth->logoutRedirect = array('controller'=>'Dataentry','action'=>'login');

            
            
        } else {
            // If we get here, it is neither a 'phys' prefixed method, not an 'admin' prefixed method.
            // So, just allow access to everyone - or, alternatively, you could deny access - $this->Auth->deny();
           // $this->Auth->allow();           
        }
    }

	
 public function isAuthorized($user){
        // You can have various extra checks in here, if needed.
        // We'll just return true though. I'm pretty certain this method has to exist, even if it just returns true.
        return true;
    }
	

}

