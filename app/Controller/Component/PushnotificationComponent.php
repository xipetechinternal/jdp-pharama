<?php
App::uses('HttpSocket', 'Network/Http');

class PushnotificationComponent extends Component {
    
    /**
     * Geocodes an address.
     *
     * @param string $address
     * @param array $parameters
     * @return object
     * @todo Determine what to do if response status is an error
     */
	 
	function notification($Message,$Badge,$Token){		
			
			// Provide the Host Information.
			$tHost = 'gateway.sandbox.push.apple.com';
			
			$tPort = 2195;
			
			// Provide the Certificate and Key Data.
			$tCert = WWW_ROOT.'i-care_dev.pem';
			
			//$tPassphrase = 'DinnerOfTheDay';
			$tPassphrase = '123123';
			
			// Provide the Device Identifier (Ensure that the Identifier does not have spaces in it).
			// Replace this token with the token of the iOS device that is to receive the notification.
			//$tToken = '4d67fa1b3f287d49627f8b47c45bd85bfd5d8a04e6b4e5b33fe0593195e707fe';
			$tToken = $Token;
			file_put_contents("udid.txt",$tToken);
			// The message that is to appear on the dialog.
			//$tAlert = 'ha logout per token delete hoga';
			$tAlert = $Message;
			// The Badge Number for the Application Icon (integer >=0).
			$tBadge = $Badge;
			//$tBadge = $batch;
			// Audible Notification Option.
			$tSound = 'default';
			
			// The content that is returned by the LiveCode "pushNotificationReceived" message.
			//$tPayload = 'APNS Message Handled by Infranix';
			
			$tBody['aps'] = array (
				'alert' => $tAlert,
				'badge' => intval(1),
				'sound' => $tSound
				);
				
	
			$tBody ['payload'] = $tPayload;
				
			
			// Encode the body to JSON.
			$ttbody = json_encode ($tBody);//,JSON_UNESCAPED_UNICODE
			//$ttbody = json_encode ($tBody);
			
			// Create the Socket Stream.
			$tContext = stream_context_create ();
			stream_context_set_option ($tContext, 'ssl', 'local_cert', $tCert);
			
			// Remove this line if you would like to enter the Private Key Passphrase manually.
			stream_context_set_option ($tContext, 'ssl', 'passphrase', $tPassphrase);
			
			// Open the Connection to the APNS Server.
			$tSocket = stream_socket_client ('ssl://'.$tHost.':'.$tPort, $error, $errstr, 30, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $tContext);
			
			// Check if we were able to open a socket.
			if (!$tSocket)
				//exit ("APNS Connection Failed: $error $errstr" . PHP_EOL);
			return false;
			// Build the Binary Notification.
			$tMsg = chr (0) . chr (0) . chr (32) . pack ('H*', $tToken) . pack ('n', strlen ($ttbody)) . $ttbody;
			
			// Send the Notification to the Server.
			$tResult = fwrite ($tSocket, $tMsg, strlen ($tMsg));
			fclose ($tSocket);	
				
			if ($tResult){
				return true;		
			}else{
				return false;
			}
		
		}
	 
}