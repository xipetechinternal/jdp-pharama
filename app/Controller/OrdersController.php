<?php

/**

 * Static content controller.

 *

 * This file will render views from views/pages/

 *

 * PHP 5

 *

 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)

 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)

 *

 * Licensed under The MIT License

 * Redistributions of files must retain the above copyright notice.

 *

 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)

 * @link          http://cakephp.org CakePHP(tm) Project

 * @package       app.Controller

 * @since         CakePHP(tm) v 0.2.9

 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)

 */



App::uses('CakeTime', 'Utility');
App::uses('AppController', 'Controller');

App::uses('CakeEmail', 'Network/Email');



/**

 * Static content controller

 *

 * Override this controller by placing a copy in controllers directory of an application

 *

 * @package       app.Controller

 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html

 */

class OrdersController extends AppController {
	
	  public $components = array('RequestHandler');

   public $uses = array('User','EmailTemplateDescription','Order','Orderdetail','Products','ProductInfo','OrderLot','OrderDist','CreditReturn');

   public $paginate = array('limit' => 10);

    function beforeFilter() {

        parent::beforeFilter();

        $this->layout = "default";

        $this->Auth->allow('admin_login');

		$this->Auth->userScope = array('User.group_id' => 1);

		$this->Auth->loginAction = '/admin/users/login';

        $this->Auth->logoutRedirect = '/admin/users/login';

        $this->Auth->loginRedirect = array('plugin'=>false, 'controller' => 'users', 'action' => 'index','admin'=>true);    

        $this->User->bindModel(array('belongsTo'=>array(

            'Group' => array(

                'className' => 'Group',

                'foreignKey' => 'group_id',

                'dependent'=>true

            )

        )), false);

		

		

    }


		 public function admin_sales_report() {

		$this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));
		
		  $slaesmalist=$this->User->find('all',array('conditions'=>array('level'=>'sales')));
		  $salesman_array=array();
		  foreach($slaesmalist as $salesmant){
			  $salesman_array[$salesmant['User']['id']]=$salesmant['User'];
		  }
		  
		   $this->paginate = array('order'=>array(
        'Order.createddt' => 'desc'),
        'conditions' => array('Order.status'=>'con'),'fields' => array('DISTINCT Order.id','Order.tmporderid','Order.userid','Order.createddt','Order.status','User.username','User.id','User.sid','User.fname','User.email','User.phone','User.address','User.city','User.state','User.zip_code')		
    	);	
			//$products=$this->paginate("Order");
		//print_r($products);
		$this->set('salesman',$salesman_array);
        $this->set('users', $this->paginate("Order")); 
    }

	public function admin_order_ifno($id=NULL) {

		$this->layout = "admin_lightbox";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));
		
		  $slaesmalist=$this->User->find('all',array('conditions'=>array('level'=>'sales')));
		  $salesman_array=array();
		  foreach($slaesmalist as $salesmant){
			  $salesman_array[$salesmant['User']['id']]=$salesmant['User'];
		  }
		
		$this->set('salesman',$salesman_array);
		
        $this->set('users', $this->Orderdetail->find("all",array('recursive' =>  3,'conditions'=>array('Orderdetail.orderid'=>$id))));
		
		$Orderdetail = $this->OrderDist->find('all',array('recursive'=>2,'conditions'=>array('OrderDist.id'=>$id)));
		
		foreach($Orderdetail[0]['OrderdetailMany'] as $mkey=>$ordersd){		
			$Orderdetail_lot = $this->OrderLot->find('all',array('recursive'=>-1,'fields'=>array('id','product_id','quantity','lot_no','price','productinfoid','Orderdetailid','tmporderid','exchange_lot','exchange_lot_id'),'conditions'=>array('OrderLot.Orderdetailid'=>$ordersd['id'])));	
				
		foreach($Orderdetail_lot as $key=>$detail){
			
			$productifo_id=($detail['OrderLot']['exchange_lot_id']!='')?$detail['OrderLot']['exchange_lot_id']:$detail['OrderLot']['productinfoid'];
			$prodetajl = $this->ProductInfo->find('first',array('recursive'=>-1,'conditions'=>array('ProductInfo.id'=>$productifo_id)));
			$Orderdetail_lot[$key]['product_details']=$prodetajl;
		}
			$Orderdetail[0]['OrderdetailMany'][$mkey]['lot_details']=$Orderdetail_lot;
		}
			//print_r($Orderdetail);die;	
		 $this->set('Orderdetail', $Orderdetail);
		  $this->set('Orderdetailid',$oid); 
    }
	
	
	public function admin_report_monthly() {

		$this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));
			
		  if (isset($this->params['url']['selfrmdate'])) {
			if($this->params['url']['seltype']=='All'){
				$conditions = array(
				 'fields' => array('DISTINCT Order.id','Order.tmporderid','Order.userid','Order.createddt','Order.status','User.username','User.id','User.sid','User.fname','User.email','User.phone','User.address','User.city','User.state','User.zip_code'),	
        'conditions' => array(
        'and' => array(
                        array('Order.createddt <= ' => $this->params['url']['seltodate'].' 23:59',
                              'Order.createddt >= ' => $this->params['url']['selfrmdate'].' 00:00'
                             )
                      )));
			}else{
				$conditions = array(
				 'fields' => array('DISTINCT Order.id','Order.tmporderid','Order.userid','Order.createddt','Order.status','User.username','User.id','User.sid','User.fname','User.email','User.phone','User.address','User.city','User.state','User.zip_code'),
        'conditions' => array(
        'and' => array(
                         array('Order.createddt <= ' => $this->params['url']['seltodate'].' 23:59',
                              'Order.createddt >= ' => $this->params['url']['selfrmdate'].' 00:00'
                             ),            
            'Order.status =' => $this->request->data['ProductInfo']['seltype']
            )));
			}
			$this->paginate=$conditions;
			
			//print_r($this->paginate("Order"));
			$this->set('users', $this->paginate("Order"));
			
			$this->request->data['ProductInfo']['seltodate']=$this->params['url']['seltodate'];
			$this->request->data['ProductInfo']['selfrmdate']=$this->params['url']['selfrmdate'];
			$this->request->data['ProductInfo']['seltype']=$this->params['url']['seltype'];
			
		  }
		
        
    }
	
		public function admin_filler_sheet() {

		$this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));
			
		  if (isset($this->params['url']['selfrmdate'])) {
			
				$conditions = array(
        'conditions' => array(
        'and' => array(
                        array('Order.createddt <= ' => $this->params['url']['seltodate'].' 23:59',
                              'Order.createddt >= ' => $this->params['url']['selfrmdate'].' 00:00'
                             )
                      )));
			
			$this->paginate=$conditions;
			//print_r($this->paginate("Order"));
			$this->set('users', $this->paginate("Order"));
			
			$this->request->data['ProductInfo']['seltodate']=$this->params['url']['seltodate'];
			$this->request->data['ProductInfo']['selfrmdate']=$this->params['url']['selfrmdate'];
			
			$this -> render('admin_filler_sheetpdf');
			
		  }
		
        
    }
	
	public function admin_filler_sheetpdf_byclient($order_id) {
		set_time_limit(0);
		ini_set('memory_limit', '256M');
		$this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));
			
		  if ($order_id!='') {
			//Configure::write('debug',3);
        $conditions = array('fields' => array('DISTINCT Order.id','Order.tmporderid','Order.userid','Order.createddt','Order.status','User.id','User.username,User.fname','User.email','User.phone','User.address','User.contact_name','User.state_no','User.den_no','User.city','User.state','User.zip_code'),	
        'conditions' => array(
        'and' => array(
                        array('Order.id' => $order_id,                              
							  'Order.status' => array('Con','Accep','Test')
                             )
                      )));
			
			$this->paginate=$conditions;
			$order_list=$this->paginate("Order");
                        debug($order_list);
			
			if(empty($order_list)){
				$this->Session->setFlash(__('NO Order found between these dates.'), 'admin_error');
        		$this->redirect(array('action' => 'admin_filler_sheet'));

			}
                        
			foreach($order_list as $k=>$ordermany){
				$quantity=0;
				foreach($ordermany['OrderdetailMany'] as $key=>$order){
                                    
					$order_lot = $this->OrderLot->find('all',array('recursive' =>1,'fields'=>array('productinfoid','orderid','Orderdetailid','quantity','id','product_id','exchange_lot_id','productinfoid'),'conditions'=>array('OrderLot.Orderdetailid'=>$order['id'])));
					foreach($order_lot as $key3=>$lot){
                                            $productifo_id = ($lot['OrderLot']['exchange_lot_id'] != '') ? $lot['OrderLot']['exchange_lot_id'] : $lot['OrderLot']['productinfoid'];
                                            $prodetajl = $this->ProductInfo->find('first', array('recursive' => 1, 'conditions' => array('ProductInfo.id' => $productifo_id)));
                                            $order_lot[$key3]['ProductInfo']=$prodetajl;				
					}
					
					$order_list[$k]['OrderdetailMany'][$key]['productinfo']=$order_lot;
					$quantity=$quantity+$order['quantity'];
				}
				$order_list[$k]['Order']['itemcount']=count($ordermany['OrderdetailMany']);
				$order_list[$k]['Order']['quantity']=$quantity;
			}
			
			
			$data_byuser=array();
			foreach($order_list as $list){
				$data_byuser[$list['User']['id']][]=$list;
			}
			//print_r($data_byuser);
			
			$this->set('users',$data_byuser);
			
			//$this->request->data['ProductInfo']['seltodate']=$this->params['url']['seltodate'];
			//$this->request->data['ProductInfo']['selfrmdate']=$this->params['url']['selfrmdate'];
			
			$this->set('id', $id);
        $this->layout = 'pdf'; //this will use the pdf.ctp layout 
        $this->render(); 
			
		  }
		
        
    }
	
	public function admin_filler_sheetpdf() {
		set_time_limit(0);
		ini_set('memory_limit', '256M');
		$this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));
			
		  if (isset($this->params['url']['selfrmdate'])) {
			//Configure::write('debug',3);
				$conditions = array(
				
		'fields' => array('DISTINCT Order.id','Order.tmporderid','Order.userid','Order.createddt','Order.status','User.id','User.username,User.fname','User.email','User.phone','User.address','User.contact_name','User.state_no','User.den_no','User.city','User.state','User.zip_code'),	
        'conditions' => array(
        'and' => array(
                        array('Order.createddt <= ' => $this->params['url']['seltodate'].' 23:59',
                              'Order.createddt >= ' => $this->params['url']['selfrmdate'].' 00:00',
							  'Order.status' => array('Con','Accep','Test')
                             )
                      )));
			
			$this->paginate=$conditions;
			$order_list=$this->paginate("Order");
			
			if(empty($order_list)){
				$this->Session->setFlash(__('NO Order found between these dates.'), 'admin_error');
        		$this->redirect(array('action' => 'admin_filler_sheet'));

			}
			foreach($order_list as $k=>$ordermany){
				$quantity=0;
				foreach($ordermany['OrderdetailMany'] as $key=>$order){
					$order_lot = $this->OrderLot->find('all',array('recursive' =>2,'fields'=>array('productinfoid','orderid','Orderdetailid','quantity','id','product_id','exchange_lot_id','productinfoid'),'conditions'=>array('OrderLot.Orderdetailid'=>$order['id'])));
					
					foreach($order_lot as $key3=>$lot){
					$productifo_id = ($lot['OrderLot']['exchange_lot_id'] != '') ? $lot['OrderLot']['exchange_lot_id'] : $lot['OrderLot']['productinfoid'];
                $prodetajl = $this->ProductInfo->find('first', array('recursive' => 1, 'conditions' => array('ProductInfo.id' => $productifo_id)));
				$order_lot[$key3]['ProductInfo']=$prodetajl;				
					}
					
					$order_list[$k]['OrderdetailMany'][$key]['productinfo']=$order_lot;
					$quantity=$quantity+$order['quantity'];
				}
				$order_list[$k]['Order']['itemcount']=count($ordermany['OrderdetailMany']);
				$order_list[$k]['Order']['quantity']=$quantity;
			}
			
			
			$data_byuser=array();
			foreach($order_list as $list){
				$data_byuser[$list['User']['id']][]=$list;
			}
			//print_r($data_byuser);
			
			$this->set('users',$data_byuser);
			
			$this->request->data['ProductInfo']['seltodate']=$this->params['url']['seltodate'];
			$this->request->data['ProductInfo']['selfrmdate']=$this->params['url']['selfrmdate'];
			
			$this->set('id', $id);
        $this->layout = 'pdf'; //this will use the pdf.ctp layout 
        $this->render(); 
			
		  }
		
        
    }
	public function admin_sales_report_person() {

		$this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));
			$sales=$this->User->find('list',array('fields'=>array('username'),'conditions'=>array('User.level'=>'sales')));
		$this->set('sales',$sales);
		$clinets=$this->User->find('list',array('fields'=>array('username'),'conditions'=>array('User.level'=>'client')));
		$this->set('clinets',$clinets);
		
		//find by counditions
		
		$conditons=array();
		if ($this->params['url']['selfrmdate']!='' and $this->params['url']['seltodate']!='') {
			
			 
                        	$conditons['OrderDist.createddt <= '] = $this->params['url']['seltodate'];
                            $conditons['OrderDist.createddt >= ']=$this->params['url']['selfrmdate'];	
			
			//print_r($this->paginate("OrderDist"));
			$this->request->data['ProductInfo']['seltodate']=$this->params['url']['seltodate'];
			$this->request->data['ProductInfo']['selfrmdate']=$this->params['url']['selfrmdate'];
					}
		
		if ($this->params['url']['seltype']!='' and $this->params['url']['seltype']!='All') {			 
                        	$conditons['OrderDist.status'] = $this->params['url']['seltype'];             	
			
			$this->request->data['ProductInfo']['seltype']=$this->params['url']['seltype'];
			
		}

		if ($this->params['url']['sid']!='') {			 
            $conditons['User.sid'] = $this->params['url']['sid'];		
			$this->request->data['ProductInfo']['sid']=$this->params['url']['sid'];
		}
		
		if ($this->params['url']['cid']!='') {			 
            $conditons['User.id'] = $this->params['url']['cid'];		
			$this->request->data['ProductInfo']['cid']=$this->params['url']['cid'];
		}
		if ($this->params['url']['orderid']!='') {			 
            $conditons['OrderDist.tmporderid'] = $this->params['url']['orderid'];		
			$this->request->data['ProductInfo']['orderid']=$this->params['url']['orderid'];
		}
		$this->paginate=array("conditions"=>array($conditons));
			
			$this->set('users', $this->paginate("OrderDist"));
		  if (isset($this->params['url']['selfrmdate'])) {/*
			if($this->params['url']['seltype']=='All'){
				$conditions = array(
				 'fields' => array('DISTINCT Order.id','Order.tmporderid','Order.userid','Order.createddt','Order.status','User.username','User.id','User.sid','User.fname','User.email','User.phone','User.address','User.city','User.state','User.zip_code'),
        'conditions' => array(
        'and' => array(
                        array('Order.createddt <= ' => $this->params['url']['seltodate'],
                              'Order.createddt >= ' => $this->params['url']['selfrmdate']
                             ),
							 'User.sid =' =>$this->params['url']['sid'] 
                      )));
			}else{
				$conditions = array(
				 'fields' => array('DISTINCT Order.id','Order.tmporderid','Order.userid','Order.createddt','Order.status','User.username','User.id','User.sid','User.fname','User.email','User.phone','User.address','User.city','User.state','User.zip_code'),
        'conditions' => array(
        'and' => array(
                         array('Order.createddt <= ' => $this->params['url']['seltodate'],
                              'Order.createddt >= ' => $this->params['url']['selfrmdate']
                             ),            
            'Order.status =' => $this->request->data['ProductInfo']['seltype'],
			'User.sid =' => $this->params['url']['sid'] 
            )));
			}
			$this->paginate=$conditions;
			//print_r($this->paginate("Order"));
			$this->set('users', $this->paginate("Order"));
			
			$this->request->data['ProductInfo']['seltodate']=$this->params['url']['seltodate'];
			$this->request->data['ProductInfo']['selfrmdate']=$this->params['url']['selfrmdate'];
			$this->request->data['ProductInfo']['seltype']=$this->params['url']['seltype'];
			$this->request->data['ProductInfo']['sid']=$this->params['url']['sid'];
		  */}
		
        
    }
	
		public function admin_label_sheet() {

		$this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));
		
		$clients=$this->User->find("list",array("fields"=>array("fname"),"conditions"=>array("level"=>"client")));
		$this->set("clients",$clients);
		  if (isset($this->params['url']['selfrmdate'])) {
			
				$conditions = array(
        'conditions' => array(
        'and' => array(
                        array('Order.createddt <= ' => $this->params['url']['seltodate'].' 23:59',
                              'Order.createddt >= ' => $this->params['url']['selfrmdate'].' 00:00'
                             )
                      )));
			
			$this->paginate=$conditions;
			//print_r($this->paginate("Order"));
			$this->set('users', $this->paginate("Order"));
			
			$this->request->data['ProductInfo']['seltodate']=$this->params['url']['seltodate'];
			$this->request->data['ProductInfo']['selfrmdate']=$this->params['url']['selfrmdate'];
			
			$this -> render('admin_filler_sheetpdf');
			
		  }
		
        
    }
	
	public function admin_label_sheetpdf() {
		set_time_limit(0);
		ini_set('memory_limit', '256M');
		$this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));

        $this->set('description', __('Manage Users'));
			
		  if (isset($this->params['url']['selfrmdate'])) {
			//Configure::write('debug',3);
				$conditions = array('recursive'=>3,	
			
        'conditions' => array(
        'and' => array(
                        array('OrderDist.createddt <= ' => $this->params['url']['seltodate'].' 23:59',
                              'OrderDist.createddt >= ' => $this->params['url']['selfrmdate'].' 00:00',
							  'OrderDist.userid' => $this->params['url']['cid'],
							  'OrderDist.status' => array('Accep')
                             )
                      )));
			$order_list=$this->OrderDist->find("all",$conditions);
			//print_r($order_list);die;
			//$this->paginate=$conditions;
			//$order_list=$this->paginate("Order");
			
			if(empty($order_list)){
				$this->Session->setFlash(__('NO Order found between these dates.'), 'admin_error');
        		$this->redirect(array('action' => 'admin_label_sheet'));

			}
			
			
			
			
			//print_r($order_list);
			$this->set('users',$order_list);
			
			$this->request->data['ProductInfo']['seltodate']=$this->params['url']['seltodate'];
			$this->request->data['ProductInfo']['selfrmdate']=$this->params['url']['selfrmdate'];
			
			$this->set('id', $id);
        $this->layout = 'pdf'; //this will use the pdf.ctp layout 
        $this->render(); 
			
		  }
		
        
    }
	
	public function admin_return_credit() {

		$this->layout = "admin_dashboard";

        $this->set('title', __('Welcome Admin Panel'));
		//Configure::write('debug',3);
        $this->set('description', __('Manage Users'));
			$sales=$this->User->find('list',array('fields'=>array('username'),'conditions'=>array('User.level'=>'sales')));
		$this->set('sales',$sales);
		$clinets=$this->User->find('list',array('fields'=>array('username'),'conditions'=>array('User.level'=>'client')));
		$this->set('clinets',$clinets);
		
		//find by counditions
		
		$conditons=array();
		
		
		$this->set('cidd', $this->params['url']['cid']);
		$usesrs=array();
		$this->request->data['ProductInfo']['cid']=$this->params['url']['cid'];
		if ($this->params['url']['cid']!='' and $this->params['url']['ndc']!='') {
			
			$creditlist=$this->CreditReturn->find('list',array('fields'=>array('CreditReturn.order_lot'),'conditions'=>array('CreditReturn.user_id'=>$this->params['url']['cid'])));
			
			$product_id=$this->ProductInfo->find('first',array('recursive' =>  2,'fields'=>array('Product.id,ProductInfo.batchno'),'conditions'=>array("OR"=>array('Product.product_ndc'=>trim($this->params['url']['ndc']),'Product.productname like "%'.trim($this->params['url']['ndc']).'%"','ProductInfo.batchno'=>$this->params['url']['ndc']))));	
		     $conditons['NOT'] = array( 
                'OrderLot.id' => $creditlist 
       		 ) ;
            $conditons['OrderLot.user_id'] = $this->params['url']['cid'];
			$conditons['OrderLot.product_id']= $product_id['Product']['id'];		
			$this->request->data['ProductInfo']['cid']=$this->params['url']['cid'];
			$this->request->data['ProductInfo']['ndc']=$this->params['url']['ndc'];
			$this->request->data['ProductInfo']['batchno']=$this->params['url']['ndc'];
			$this->paginate=array("conditions"=>array($conditons),'limit'=>1000,'recursive' =>  2);
			$usesrs=$this->paginate("OrderLot");
			$this->set('cidd', $this->params['url']['cid']);
			//print_r($usesrs);
		}
		
		if ($this->request->is('post') || $this->request->is('put')) {
				
				$this->request->data['ProductInfo']['cid']=$this->request->data['ProductInfo']['customerid'];
				if(!empty($this->request->data['ProductInfo']['returnCheck'])){
					foreach($this->request->data['ProductInfo']['returnCheck'] as $key=>$val){
						$data=array();
						$quantity=$this->request->data['ProductInfo']['returncredit_'.$val];
						if($quantity>0){
							$data['CreditReturn']['user_id']=$this->request->data['ProductInfo']['customerid'];
							$data['CreditReturn']['order_lot']=$val;
							$data['CreditReturn']['quantity']=$quantity;
							$data['CreditReturn']['status']='N';
							$data['CreditReturn']['created']=date("Y-m-d H:i:s");
							 $this->CreditReturn->create();
							 $this->CreditReturn->save($data);
							 
						}
					}
				}else{
					$this->Session->setFlash(__('No Lot Selected.'), 'admin_error');
				}

			}
		
		
			
			$this->set('users', $usesrs);
			
			$creit_data=$this->CreditReturn->find('all',array('recursive' =>  4,'conditions'=>array('CreditReturn.user_id'=>$this->request->data['ProductInfo']['cid'],'CreditReturn.status'=>'N')));
			//print_r($creit_data);
		  $this->set('creit_data', $creit_data);
		$this->set('cidd', $this->params['url']['cid']);
        
    }
	
	public function admin_return_credit_delete($id = null,$cid=null) {

        if (!$this->request->is('post')) {

            throw new MethodNotAllowedException();

        }

        $this->CreditReturn->id = $id;

        if (!$this->CreditReturn->exists()) {

            throw new NotFoundException(__('Invalid credit '));

        }

        if ($this->CreditReturn->delete()) {

            $this->Session->setFlash(__('Credit return deleted'), 'admin_success');

            $this->redirect(array('action' => 'return_credit?cid='.$cid));

        }

        $this->Session->setFlash(__('credit was not deleted'), 'admin_error');

        $this->redirect(array('action' => 'return_credit?cid='.$cid));

    }
	
	}

