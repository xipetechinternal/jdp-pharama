<?php

App::uses('AppController', 'Controller');





/**

 * Users Controller

 *

 * @property User $User

 */

class AccountsController extends AppController {

    

    public $uses = array('User');



    function beforeFilter() {

        parent::beforeFilter();



        $this->layout = "admin_dashboard";

        $this->User->bindModel(array('belongsTo'=>array(

            'Group' => array(

                'className' => 'Group',

                'foreignKey' => 'group_id',

                'dependent'=>true

            )

        )), false);

		

	

    }

	

	

	 /**

     * Change Profile

     *

     * @param string $id

     * @return void

     */

   

    	public function admin_profile($id = null) {

			$this->User->id =  $this->Auth->user('id');

			if (!$this->User->exists()) {

				throw new NotFoundException(__('Invalid user'));

			}

			if ($this->request->is('post') || $this->request->is('put')) {

				if ($this->User->save($this->request->data)) {

					$this->Session->setFlash(__('The user account has been updated'), 'admin_success');

					$this->redirect(array('controller'=>'users','action' => 'admin_index'));

				} else {

					$this->Session->setFlash(__('The user account could not be updated. Please, try again.'), 'admin_error');

				}

			} else {

				$this->request->data = $this->User->read(null, $this->Auth->user('id'));

			}

			$groups = $this->User->Group->find('list');

			$this->set(compact('groups'));

    	}

	

	 /**

     * Change Password  

     **/

   

    	public function admin_password() {

			 $this->User->id =  $this->Auth->user('id');

			

			if (!$this->User->exists()) {

				throw new NotFoundException(__('Invalid user'));

			}

			

			if ($this->request->is('post')) {			

				$this->loadModel('User');

				$this->User->set($this->request->data);

				if($this->User->save($this->request->data) ) {

					$this->Session->setFlash(__('The user account password has been updated'), 'admin_success');

					$this->redirect(array('controller'=>'users','action' => 'admin_index'));

				} else {

					$this->Session->setFlash(__('The user account password could not be updated. Please, try again.'), 'admin_error');

				}

				

			}else {

				$this->request->data = $this->User->read(null, $this->Auth->user('id'));

				 unset($this->request->data['User']['password']);

				 unset($this->request->data['User']['password2']);

			}

			

			

    	}

	

	

	

	

	

     /**

     * logout method

     *

     * @return void

     */

     function admin_logout() {

      $this->Session->setFlash('Good-Bye', 'admin_logout');

        $this->redirect($this->Auth->logout());

     }    

	

}

?>