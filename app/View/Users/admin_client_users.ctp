<style type="text/css">
.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td {
    padding: 3px;
}
.table thead > tr > th{text-align: center; vertical-align: middle;font-size: 13px!important;}
.table tbody > tr > td{
	font-size: 13px!important;
}
.table tbody > tr > td .btn{
	margin-top: 2px;
	margin-bottom: 2px;
}

</style>

<div class="col-lg-12">
<h4><a href="<?php echo $this->Html->url('/admin/client_users');?>">Home</a> :: Manage Client User</h4>
<a href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'advertise')); ?>" class="btn btn-primary btn-mini">&nbsp;Adds </a>
<a href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'client_users_deactivate')); ?>" class="btn btn-primary btn-mini">&nbsp;Deactive Clients </a>
<div>&nbsp;</div>
            <div class="panel panel-primary">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          <h3 class="panel-title">Create/Edit Client User(s)</h3>
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse <?php if($id!=NULL){?> in <?php } ?>" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body" >

    
            <?php echo $this->Form->create('User',array('action'=>'admin_client_users','type'=>'file', 'class'=>'login-from')); ?>
           <div class="row">
                  <div class="col-md-6 ">
                   <?php echo $this->Form->input('username',array('placeholder' => 'Username',
        'label' => 'Username',
        'class'=>'form-control'));?> 
                  </div>
                  <div class="col-md-6 ">
                    <?php echo $this->Form->input('state_no',array('placeholder' => 'State No.',
        'label' => 'State No.',
        'class'=>'form-control'));?>
                  </div>
          </div>
          <div class="row">
                  <div class="col-md-6 form-group changepassword" <?php if($id!=NULL){?> style="display:none" <?php } ?>>
                   <?php echo $this->Form->input('password',array('placeholder' => 'Password',
       'label' => 'Password',
        'class'=>'form-control'));?> 
                  </div>
                  <div class="col-md-6 form-group">
                   <?php echo $this->Form->input('state_no_exp',array('placeholder' => 'State Exp',
       'label' => 'State Exp','type' => 'text',
        'class'=>'form-control'));?> 
                  </div>
          </div>
          <div class="row">
                  <div class="col-md-6 form-group changepassword" <?php if($id!=NULL){?> style="display:none" <?php } ?>>
                      <?php echo $this->Form->input('password2',array('placeholder' => 'Confirm Password',
       'label' => 'Confirm Password',
		'type'=>'password',
		'required'=>'required',
        'class'=>'form-control'));?> 
                  </div>
                  <div class="col-md-6 form-group">
                    <?php echo $this->Form->input('den_no',array('placeholder' => ' D.E.A No.',
       'label' => ' D.E.A No.',
        'class'=>'form-control'));?> 
                   
                  </div>
          </div>
          <div class="row">
                  <div class="col-md-6 form-group">
                     <?php echo $this->Form->input('fname',array('placeholder' => 'First Name',
       'label' => 'First Name',
	   'required'=>'required',
        'class'=>'form-control'));?> 
                  </div>
                  <div class="col-md-6 form-group">
                   <?php echo $this->Form->input('den_no_exp',array('placeholder' => ' D.E.A Exp',
       'label' => ' D.E.A Exp',
		'type'=>'text',
        'class'=>'form-control'));?> 
                  
                  </div>
         </div>
         <div class="row">
                  <div class="col-md-6 form-group">
                   <?php echo $this->Form->input('lname',array('placeholder' => 'Last Name',
       'label' => 'Last Name',
	   'required'=>'required',
        'class'=>'form-control'));?> 
                   
                  </div>
                  <div class="col-md-6 form-group">
                     <?php echo $this->Form->input('email',array('placeholder' => 'Email',
       'label' => 'Email',
        'class'=>'form-control'));?> 
                  </div>
        </div>
        <div class="row">
                  <div class="col-md-6 form-group">
                   <?php echo $this->Form->input('phone',array('placeholder' => 'Phone',
       'label' => 'Phone',
        'class'=>'form-control'));?> 
                  </div>
                  <div class="col-md-6 form-group">
                   
                    <?php echo $this->Form->input('fax',array('placeholder' => 'Fax',
       'label' => 'Fax',
        'class'=>'form-control'));?> 
                  </div>
        </div>
        <div class="row">
                  <div class="col-md-6 form-group">
                   <?php echo $this->Form->input('address',array('placeholder' => 'Address',
       'label' => 'Address',
        'class'=>'form-control'));?> 
                  </div>
                  <div class="col-md-2 form-group">                  
                    <?php echo $this->Form->input('city',array('placeholder' => 'City',
       'label' => 'City',
        'class'=>'form-control'));?> 
                  </div>
        
                  <div class="col-md-2 form-group">
                   <?php echo $this->Form->input('state',array('placeholder' => 'State',
       'label' => 'State',
        'class'=>'form-control'));?> 
                    
                  </div>
                  <div class="col-md-2 form-group">
                  <?php echo $this->Form->input('zip_code',array('placeholder' => 'Zip Code',
       'label' => 'Zip Code',
        'class'=>'form-control'));?> 
                    
                  </div>
         </div>
         <div class="row">
                  <div class="col-md-6 form-group">
                  
    				<?php echo $this->Form->input('sid',array('empty'=>array(''=>'--Select Name--'),'options' => $sales,
       'label' => 'Sales Name',
	   'required'=>'required',
        'class'=>'form-control'));?>
    
                    
                  </div>
                   <!--<div class="col-md-2 form-group">
                   <?php echo $this->Form->input('Brand_discount',array('title' => 'Brand Discount In %','placeholder' => 'Brand Discount In %',
       'label' => 'Brand Discount In %',
        'class'=>'form-control'));?>
                    
                  </div>
				 <div class="col-md-2 form-group">
                   <?php echo $this->Form->input('Generic_discount',array('title' => 'Generic Discount %','placeholder' => 'Generic Discount In %',
       'label' => 'Generic Discount %',
        'class'=>'form-control'));?> 
                    
                  </div>
				 <div class="col-md-2 form-group">
                   <?php echo $this->Form->input('OTC_discount',array('title' => 'OTC Discount In %','placeholder' => 'OTC Discount In %',
       'label' => 'OTC Discount In %',
        'class'=>'form-control'));?> 
                    
                  </div>-->
			<div class="col-md-6 form-group">
                   <?php echo $this->Form->input('Refrigirated_discount',array('title' => 'Discount In %','placeholder' => 'Discount In %',
       'label' => 'Discount In %',
        'class'=>'form-control'));?> 
                    
                  </div>
          <div class="col-md-6 form-group">
                   <?php echo $this->Form->input('CUSTOMER_NUMBER',array('title' => 'CUSTOMER NUMBER','placeholder' => 'CUSTOMER NUMBER',
	   'required'=>'required',
       'label' => 'CUSTOMER #',
        'class'=>'form-control'));?> 
                    
                  </div>
          <div class="col-md-6 form-group">
                   <?php echo $this->Form->input('Customer_Credit_Limit',array('title' => 'Credit Limit','placeholder' => 'Credit Limit',
	   'required'=>'required',
       'label' => 'Credit Limit $',
        'class'=>'form-control'));?> 
                    
                  </div>
           <div class="col-md-6 form-group">
           
                   <?php echo $this->Form->input('client_stock_type',array('title' => 'Show JDP stock ?','placeholder' => 'Show JDP stock ?',
	   'type'=>'checkbox',
	   
       'label' => 'Show JDP stock',
        'class'=>''));?> 
                  <!--<?php echo $this->Form->input('threpercenteoff',array('title' => '3% OFF','placeholder' => '3% OFF',
	   'type'=>'checkbox',	   
       'label' => '3% OFF',
	   'onclick'=>'$("#UserThrepercenteoff10").removeAttr("checked");$("#UserThrepercenteoff6").removeAttr("checked")',
	   'value'=>3,
        'class'=>''));?>
        <?php echo $this->Form->input('threpercenteoff6',array('title' => '6% OFF','placeholder' => '6% OFF',
	   'type'=>'checkbox',	   
       'label' => '6% OFF',
	   'onclick'=>'$("#UserThrepercenteoff10").removeAttr("checked");$("#UserThrepercenteoff").removeAttr("checked")',
	   'value'=>6,
        'class'=>''));?>
        <?php echo $this->Form->input('threpercenteoff10',array('title' => '10% OFF','placeholder' => '10% OFF',
	   'type'=>'checkbox',	   
       'label' => '10% OFF',
	   'onclick'=>'$("#UserThrepercenteoff").removeAttr("checked");$("#UserThrepercenteoff6").removeAttr("checked")',
	   'value'=>10,
        'class'=>''));?>--> 
        <?php echo $this->Form->input('JDLotPriority',array('title' => 'START / JD Lot Priority','placeholder' => 'START / JD Lot Priority',
	   'type'=>'checkbox',	   
       'label' => 'START/JD Lot Priority',
        'class'=>''));?>
        <?php echo $this->Form->input('product_inventory',array('title' => 'Product Inventory','placeholder' => 'Product Inventory',
	   'type'=>'checkbox',	   
       'label' => 'Product Inventory',
        'class'=>''));?>    
                  </div>
            
                 <div class="col-md-6 form-group">
           			<div>
                   <?php $options = array('15 Days'=>'15 Days from invoice date','30 days'=>'30 days from invoice date','Due date is same'=>'Due date is same as invoice date','per week'=>'per week ');
				   $attributes=array('legend'=>false, 'label'=>false, 'separator'=>'</div><div>');  
				    echo $this->Form->radio('client_due_datea',$options,$attributes);?> 
                  </div>
                  </div>
                </div>
                  <div class="gap20"></div>
                  <div class="row">
                  <div class="col-md-6 form-group">
                   <?php echo $this->Form->input('active',array('type' => 'hidden','value'=>'Y'));?> 
                  <?php echo $this->Form->input('level',array('type' => 'hidden','value'=>'client'));?>
                  <?php echo $this->Form->input('status',array('type' => 'hidden','value'=>'1'));?> 
                   <?php echo $this->Form->input('createddt',array('type' => 'hidden','value'=>date("Y-m-d H:i:s")));?>
                    <?php echo $this->Form->input('id');?>
<?php echo $this->Form->submit(__('Submit'), array('class'=>'btn btn-primary', 'div'=>false));?>&nbsp;
<?php echo $this->Form->reset(__('Cancel'), array('class'=>'btn btn-primary', 'div'=>false));?>
<?php if($id!=NULL){?>
&nbsp;<?php echo $this->Form->reset(__('Change Password'), array('value'=>'Change Password','class'=>'btn btn-primary','onclick'=>"$('.changepassword').show();",'type'=>'button','div'=>false));?>
<?php } ?>
                  </div>
                  </div>
               <?php echo $this->Form->end();?>
      </div>    </div>
  </div>
      
    


</div>

<div class="col-lg-12 text-center">

      <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Manage Client Accounts</h3>
      </div>
      <div class="panel-body">
       <div class="table-responsive">
             
			<?php $membertype=array(1=>'Gold',2=>'Silver',3=>'Bronze'); ?>
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                        <th><?php echo __('Email');?></th>
                        <th><?php echo __('Username');?></th>
                        <th><?php echo __('CUSTOMER #');?></th>
                         <th><?php echo __('Sales Manager');?></th>
                        <th><?php echo __('% Discount');?></th>
                        <th><?php echo __('$ Unpaid ');?></th>
                        <th><?php echo __('Phone');?></th> 
                        <th><?php echo __('Address');?></th> 
                        <th><?php echo __('Credit Limit');?></th>
                        <th><?php echo __('Created');?></th>
                        <th><?php echo __('Actions');?></th>
                    </tr>
                </thead>
                
                <tbody>
                
				<?php 
				$i=0;
				foreach ($users as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				?>
                <tr <?php echo $class;?> >               
                <td><?php echo h($user['User']['email']); ?></td>
                <td><?php echo h($user['User']['username']); ?></td>
                <td><?php echo h($user['User']['CUSTOMER_NUMBER']); ?></td>
                <td><?php echo h($sales[$user['User']['sid']]); ?></td>
                <td><!--Brand:<?php echo h($user['User']['Brand_discount']); ?><br />
                OTC:<?php echo h($user['User']['OTC_discount']); ?><br />
                Generic:<?php echo h($user['User']['Generic_discount']); ?><br />-->
                <?php echo h($user['User']['Refrigirated_discount']); ?>%<br />
                </td>
                <td>$<?php echo h(number_format(($user['User']['totalUnpaid']-$user['User']['batch_amount']),2)); ?><br />
                 <?php if($user['User']['batch_amount']>0){?><a  class="btn btn-info btn-mini btn-left-margin btn-xs" href="<?php echo $this->Html->url(array_merge(array('controller'=>'users','action'=>'admin_client_order_paid_batch'))).'/'.$user['User']['id'].'/'.$user['User']['batchamount']; ?>"><i class="icon-file-text"></i>B-<?php echo $user['User']['batch_amount'];}?></a>
                 </td>
                <td><?php echo h($user['User']['phone']); ?></td>
                <td><?php echo h($user['User']['address']); ?><br /><?php echo h($user['User']['city']); ?><br /><?php echo h($user['User']['state']); ?><br /><?php echo h($user['User']['zip_code']); ?></td>
                <td><?php echo h($customer_credit); ?></td>
                <td class="center"><?php echo h(date("m-d-Y",strtotime($user['User']['createddt']))); ?> </td>
                <td class="left" style="text-align:left">              
                <?php echo $this->Html->link($this->Html->tag('i','Edit',array('class'=>'icon-pencil icon-white')),array('action' => 'admin_client_users', $user['User']['id']),array("class"=>"btn btn-primary btn-mini btn-xs tooltip-top","data-original-title"=>"Edit",'escape'=>false)); ?>
                
                  <?php echo $this->Html->link($this->Html->tag('i','Order History',array('class'=>'icon-pencil icon-white')),array('action' => 'admin_client_order_history', $user['User']['id']),array("class"=>"btn btn-primary btn-mini btn-xs tooltip-top","data-original-title"=>"Edit",'escape'=>false)); ?>
                <?php echo $this->Html->link($this->Html->tag('i','Login',array('class'=>'icon-pencil icon-white')),array('admin' => false,"controller"=>"customers",'action' => 'client_login_byadmin', $user['User']['id']),array("target"=>"_new","class"=>"btn btn-primary btn-mini btn-xs tooltip-top","data-original-title"=>"Edit",'escape'=>false)); ?>
			  <?php echo $this->Html->link($this->Html->tag('i','Return Credit',array('class'=>'icon-pencil icon-white')),array('action' => 'admin_return_credit', $user['User']['id']),array("class"=>"btn btn-primary btn-mini btn-xs tooltip-top","data-original-title"=>"Edit",'escape'=>false)); ?>
              <?php echo $this->Html->link($this->Html->tag('i','Statement',array('class'=>'icon-pencil icon-white')),array('action' => 'admin_client_order_statement', $user['User']['id']),array("class"=>"btn btn-primary btn-mini btn-xs tooltip-top","data-original-title"=>"Statement",'escape'=>false)); ?>
              
              <?php 
			  if($user['User']['status']){
			  echo $this->Html->link($this->Html->tag('i','Deactivate',array('class'=>'icon-pencil icon-white')),array('action' => 'admin_client_status', $user['User']['id'],0),array("class"=>"btn btn-primary btn-mini btn-xs tooltip-top","data-original-title"=>"Deactivate",'escape'=>false));
			  }else{
				  echo $this->Html->link($this->Html->tag('i','Activate',array('class'=>'icon-pencil icon-white')),array('action' => 'admin_client_status', $user['User']['id'],1),array("class"=>"btn btn-primary btn-mini btn-xs tooltip-top","data-original-title"=>"Activate",'escape'=>false));
			  }?>
              
                <?php echo $this->Form->postLink($this->Html->tag('i','Delete',array('class'=>'icon-remove icon-white')), array('action' => 'admin_client_users_delete', $user['User']['id']), array("class"=>"btn btn-danger btn-mini btn-xs tooltip-top","data-original-title"=>"Delete",'escape'=>false), __('Are you sure you want to delete # %s?', $user['User']['id'])); ?>
                
                
 				</td>
                </tr>
                <?php endforeach; ?>                
                </tbody>
            </table>
            <div class="pagination pagination-right pagination-mini">
	  <ul>
		<?php echo $this->Paginator->prev(''); ?>
		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
		<?php echo $this->Paginator->next(''); ?>
	  </ul>
	</div>
       </div>
      </div>
    </div>


</div>
<script>
$(document).ready(function(){
	$("#UserAdminClientUsersForm").validate({

		rules: {
			"data[User][password]":{
				required: true,
				minlength:5,

				},
			"data[User][password2]":{
				required: true,
				minlength:5,
				equalTo: "#UserPassword"

				}
		},
		messages: {
			"data[User][password]":"Please check this field",
			"data[User][password2]":{equalTo:"make sure both match"}
		},
		errorElement:"span"

		});
		
		 $( "#Datepicker1" ).datepicker({dateFormat: 'yy-mm-dd' }); 
   $( "#UserStateNoExp" ).datepicker({dateFormat: 'yy-mm-dd' }); 
    $( "#UserDenNoExp" ).datepicker({dateFormat: 'yy-mm-dd' }); 
});
</script>