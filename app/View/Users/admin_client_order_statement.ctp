<div class="col-lg-12  row">
<div class="col-lg-6">
<h4><a href="<?php echo $this->Html->url('/admin/users');?>">Home</a> :: Order Information For Customer <?php echo $user_info['User']['fname']?></h4>   
</div>
<div class="col-lg-6"><a href="javascript:;" onclick="history.go(-1);" class="btn btn-primary btn-mini btn-left-margin">Back</a>
</div>
</div>

<div class="col-lg-12 text-center">
        <div class="panel panel-primary">
        <div class="panel-heading">
            <h2 class="panel-title">Order Statement</h2>
          </div>
            <div class="clearfix"></div>
            <div class="clearfix"></div>
            <h3>Statement List</h3>  
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                      <th><?php echo __('Statement Number');?></th>                        
                        <th><?php echo __('Date');?></th>
                        <th><?php echo __('$');?></th>
                        <th><?php echo __('Action');?></th>                  
                       
                    </tr>
                </thead>
                
                <tbody>
                <?php if(empty($StatementList)): ?>
		<tr>
			<td colspan="5" class="striped-info">No record found.</td>
		</tr>
		<?php endif;  ?>
        
				<?php 
				$i=0;
				
						
				foreach ($StatementList as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				
				?>
                <tr <?php echo $class;?> >               
               
                <td><?php echo h($user['OrderStatement']['statement']); ?></td>
                 <td><?php echo date("m-d-Y",strtotime($user['OrderStatement']['created'])); ?></td>
                
                <td><?php echo $starray[$user['OrderStatement']['statement']]; ?></td>
                <td><?php echo $this->Html->link('Statement',array('action' => 'admin_client_order_statement_invoice',$user['OrderStatement']['user_id'], $user['OrderStatement']['statement']),array("class"=>"btn btn-primary btn-mini","data-original-title"=>"Edit",'escape'=>false)); ?>  &nbsp;<?php
                 
			   ?> 
               
               <?php if($user['OrderStatement']['status']=="CLOSE"){echo $this->Html->link('OPEN',array('action' => 'admin_client_order_statement_status',$user['OrderStatement']['user_id'], $user['OrderStatement']['id'],"OPEN"),array("class"=>"btn btn-primary btn-mini tooltip-top","data-original-title"=>"Edit",'escape'=>false));
			   }else{
				   echo $this->Html->link('CLOSE',array('action' => 'admin_client_order_statement_status',$user['OrderStatement']['user_id'], $user['OrderStatement']['id'],"CLOSE"),array("class"=>"btn btn-primary btn-mini tooltip-top","data-original-title"=>"Edit",'escape'=>false));
			   }
			   echo '&nbsp;&nbsp;'.$this->Form->postLink($this->Html->tag('i','Delete',array('class'=>'icon-remove icon-white')), array('action' => 'admin_statement_delete', $user['OrderStatement']['statement'],$user['OrderStatement']['user_id']), array("class"=>"btn btn-danger btn-mini tooltip-top","data-original-title"=>"Delete",'escape'=>false), __('Are you sure you want to Delete  statement ?', $user['OrderStatement']['statement']));
			   ?>
               </td>
               
             
                </tr>
                <?php endforeach; ?>                
                </tbody>
                <tfoot>
          
        </tfoot>
            </table>
            <?php echo $this->Form->create('OrderHistory',array('type'=>'POST','url'=>array('action'=>'admin_client_order_statement/'.$uid,'controller'=>'users'),'class'=>'paraform')); ?>

          <div class="col-lg-12 text-right" style="padding:20px">
        	<?php echo $this->Form->button('Create Statement ', array(
    'type' => 'submit',
    'class' => 'btn btn-primary',
    'escape' => false
));?>
        </div>
          <div class="panel-body">
            <div class="table-responsive">
              <h3> Unpaid Order </h3>
              
              <table id="example" class="datatable display table table-striped table-bordered" width="100%" >
                <thead>
                  <tr>
                   
                    <th><strong><?php echo __('Order');?></strong></th>
                     <th><strong><?php echo __('PO Number');?></strong></th>
                    <th><strong><?php echo __('Quantity');?></strong></th>
                    <th><strong><?php echo __('Price');?></strong></th>
                    <th><strong><?php echo __('Status');?></strong></th>
                    
                    <th><strong><?php echo __('ORDER DATE');?></strong></th>
                    <th><strong><?php echo __('Due DATE');?></strong></th>
                    <th><strong><?php echo __('Invoice');?></strong></th>
                  </tr>
                </thead>
                 <?php if(empty($users)): ?>
		<tr>
			<td colspan="8" class="striped-info">No record found.</td>
		</tr>
		<?php endif;  ?>

                <tbody>
                
				<?php 
					
				$i=0;
				$product_price=0;
				$total_paid=0;
				$total_unpaid=0;
				foreach ($users as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				
				$product_qauantity=count($user['OrderdetailMany']);
				
				$priice=0;
				foreach($user['OrderdetailMany'] as $orderdeatils){
					$priice=$priice+($orderdeatils['price']*$orderdeatils['quantity']);
				}
				$product_price=$product_price+$priice;
				?>
                <tr <?php echo $class;?> > 
                        
              
                <td <?php echo h(($user['OrderDist']['Paid_Status']=='paid')?'style=color:blue':""); ?>><?php echo h($user['OrderDist']['tmporderid']); ?> &nbsp;</td>
                <td><?php echo h($user['OrderDist']['po_number']); ?> &nbsp;</td>
               
                             
               <td>  <?php echo $this->Html->link($product_qauantity.' Products', array('controller'=>'Users', 'action'=>'admin_client_orderinfo',$user['OrderDist']['id']),array('class' => ''));?>
                    &nbsp;</td>
                    <td>$<?php echo $priice ?> </td>
                    <td><?php echo h(($user['OrderDist']['status']=='Accep')?'Accept':$user['OrderDist']['status']); ?> &nbsp;</td>  
                    <td><?php echo date("m-d-Y",strtotime($user['OrderDist']['createddt'])); ?> &nbsp;</td>
                    <td><?php  if($user_info['User']['client_due_datea']=='15 Days'){
								echo $due_datea=date('Y-m-d', strtotime($user['OrderDist']['createddt']. ' + 15 day'));
							}elseif($user_info['User']['client_due_datea']=='30 days'){
								echo $due_datea=date('Y-m-d', strtotime($user['OrderDist']['createddt']. ' + 30 day'));
							}elseif($user_info['User']['client_due_datea']=='per week'){
								
									for($i=0;$i<=6;$i++){
										
									  $due_datea_t=date('Y-m-d', strtotime($user['OrderDist']['createddt']." + $i day"));
									 
									  if(date('l',strtotime($due_datea_t))=='Tuesday'){
										  if(date('W',strtotime($due_datea_t))!=date('W',strtotime($user['OrderDist']['createddt'])))
										  echo $due_datea=date('Y-m-d', strtotime($due_datea_t. ' + 0 day'));
										  else
										  echo $due_datea=date('Y-m-d', strtotime($due_datea_t. ' + 7 day'));	
									  }	
									}
								
							}else{
								echo $due_datea=date('Y-m-d', strtotime($user['OrderDist']['createddt']. ' + 0 day'));
							}?> &nbsp;
                    </td>  
                    <td class="td-font">
                    <?php if($user['OrderDist']['status']=='Accep' or $user['OrderDist']['status']=='Test'){?>
                     <?php echo $this->Html->link('Invoice',array('action' => 'admin_invoice', $user['OrderDist']['id']),array("class"=>"btn btn-primary  btn-mini btn-left-margin","data-original-title"=>"Edit",'escape'=>false)); ?><?php echo $this->Html->link('PDF Invoice',array('action' => 'admin_invoicepdf', $user['OrderDist']['id']),array("class"=>"btn btn-primary  btn-mini btn-left-margin","data-original-title"=>"Edit",'escape'=>false)); ?>
                   
                    <?php }?>
                    <?php  echo $this->Html->link($this->Html->tag('i',"Pedigree",array('class'=>'icon-pencil icon-white')),array('controller'=>'Users','action' => 'admin_client_orderinfo', $user['OrderDist']['id']),array("class"=>"btn btn-primary  btn-mini btn-left-margin","data-original-title"=>"Manually",'escape'=>false));?>
                 &nbsp;&nbsp;  
                 <input type="checkbox" name="data[OrderHistory][batch][]" placeholder="From Date" value="<?php echo $user['OrderDist']['id']?>" class="input" style=" height:20px; width:20px" id="OrderHistoryBatch<?php echo $user['OrderDist']['id']?>"> 
               
                    </td>
 
                </tr>
                <?php endforeach; ?>                
                </tbody>
                
              </table>
              <div class="pagination pagination-right pagination-mini">
	  <ul>
		<?php echo $this->Paginator->prev(''); ?>
		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
		<?php echo $this->Paginator->next(''); ?>
	  </ul>
	</div>
            </div>
            <div class="clearfix"></div>
             <div class="clearfix"></div>
             <h3>Return Credit--</h3>  
             <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                     
                     
                        <th><?php echo __('Credit Number');?></th>
                        <th><?php echo __('Quantity');?></th>
                        <th><?php echo __('Amount');?></th>
                        <th><?php echo __('Date');?></th>
                        <th><?php echo __('Action');?></th>                  
                       
                    </tr>
                </thead>
                
                <tbody>
                <?php if(empty($CreditReturnlist)): ?>
		<tr>
			<td colspan="5" class="striped-info">No record found.</td>
		</tr>
		<?php endif;  ?>
        
				<?php 
				$i=0;
				
						
				foreach ($CreditReturnlist as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				
				?>
                <tr <?php echo $class;?> >               
               
                <td><?php echo h($user['CreditReturn']['credit_number']); ?></td>
                 <td><?php echo h($user['CreditReturn']['quantity']); ?></td>
                <td><?php echo number_format(($user['CreditReturn']['quantity']*$user['CreditReturn']['product_price'])+$user['CreditReturn']['restocking_fees'],2); ?></td>
                <td><?php echo date("m-d-Y",strtotime($user['CreditReturn']['created'])); ?></td>
                <td><input type="checkbox" name="data[OrderHistory][returncredit][]" placeholder="From Date" value="<?php echo $user['CreditReturn']['id']?>" class="input" style=" height:20px; width:20px" id="OrderHistoryBatch<?php echo $user['CreditReturn']['id']?>"> 
                 </td>
               
             
                </tr>
                <?php endforeach; ?>                
                </tbody>
                <tfoot>
          
        </tfoot>
            </table>
            <!-- Added By Rashid Khan -->
            <!--<div class="table-responsive">
                <strong> Net Paid: <?php echo h('$'.number_format($totalPaid,2)); ?></strong><br>
                <strong> Net Unpaid: <?php echo h('$'.number_format($totalUnpaid,2)); ?></strong><br>
                
              </div>-->
              <!--  END  -->
            <div class="gap20"></div
            <div class="gap20"></div>
            

            
          </div>
        </div>
         <?php echo $this->Form->end();?>
      </div>

 
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Order Detail</h4>
      </div>
      <div class="modal-body">
        <div class="something"> </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){
	$("#ProductInfoAdminSalesReportPersonForm").validate();
	//$( "#ProductInfoSelfrmdate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
   // $( "#ProductInfoSeltodate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
	
	
    $( "#OrderHistorySelfrmdate, #OrderHistorySeltodate" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
       dateFormat: 'yy-mm-dd' ,
        onSelect: function( selectedDate ) {
            if(this.id == 'OrderHistorySelfrmdate'){
              var dateMin = $('#OrderHistorySelfrmdate').datepicker("getDate");
              var rMin = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 1); // Min Date = Selected + 1d
              var rMax = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 365); // Max Date = Selected + 31d
              $('#OrderHistorySeltodate').datepicker("option","minDate",rMin);
              $('#OrderHistorySeltodate').datepicker("option","maxDate",rMax);                    
            }

        }
    });

});
</script>

<script>



$(function(){

   $('.push').click(function(){
      var essay_id = $(this).attr('id');
	   
		
$.ajax({
          type : 'post',
           url : "<?php echo $this->webroot .'admin/'. $this->params["controller"]; ?>/getlot_details",
		   data:'order_id='+ essay_id,    
                     // in php you should use $_POST['post_id'] to get this value 
       success : function(r)
           {
              // now you can show output in your modal 
              $('#mymodal').show();  // put your modal id 
             $('.something').show().html(r);
           }
    });

      
	  

});

   });
</script>