<div class="col-lg-12  row">
    <div class="col-lg-6">
    
        <h4><a href="<?php echo $this->Html->url('/admin/users'); ?>">Home</a> :: Order Information For Customer <?php echo $Orderdetail['User']['fname'] ?></h4>   
    </div>
    <div class="col-lg-6"><a href="javascript:;" onclick="history.go(-1);" class="btn btn-primary">Back</a>
    </div>
</div>
<div class="col-lg-12 text-center">

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Split Invoices List</h3>
        </div>
        <div class="panel-body">
            <div class="table-responsive">

                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                    <thead>
                        <tr>
                            <th><?php echo __('Sl no.'); ?></th>
                            <th><?php echo __('Invoice No.'); ?></th>

                            <th><?php echo __('Quantity'); ?></th>                             
                            <th><?php echo __('Price'); ?></th>
                            <th><?php echo __('Action'); ?></th>

                        </tr>
                    </thead>
                    <?php if (empty($Orderdetail)): ?>
                        <tr>
                            <td colspan="7" class="striped-info">No record found.</td>
                        </tr>
                    <?php endif; ?>
                    <tbody>

                        <?php
                        $i = 0;
						$counter=0;
                        $product_price = 0;
						$quantity=0;
                        $lot_number = array();
						 $priice = 0;
						 $sn=1;
                        //print_r($Orderdetail);
                        foreach ($Orderdetail['OrderdetailMany'] as $key=>$user):
                            $class = ' class="odd"';
                            if ($i++ % 2 == 0) {
                                $class = ' class="even"';
                            }

                           if($counter<4){
                            $priice1 = ($user['price'] * $user['quantity']);
							$priice =$priice+$priice1;
							$quantity=$quantity+$user['quantity'];
							$counter++;
							?>
								<?php if($key==count($Orderdetail['OrderdetailMany'])-1){?>
                                <tr <?php echo $class; ?> > 
    
                                    <td><?php echo h($sn); ?> &nbsp;</td>
                                    <td>Invoice-<?php echo h($sn); ?> &nbsp;</td>
                                    <td><?php echo h($quantity); ?> &nbsp;</td>                               
                                    <td>$<?php echo $priice ?> </td> 
                                     <td>
                                     <?php echo $this->Html->link('PDF Invoice', array('action' => 'admin_invoicepdf_split', $user['orderid'],$sn), array("class" => "btn btn-primary  btn-mini btn-left-margin", "data-original-title" => "Edit", 'escape' => false)); ?>
									 <?php echo $this->Html->link('View', array('action' => 'admin_split_invoice', $user['orderid'],$sn), array("class" => "btn btn-primary  btn-mini btn-left-margin", "data-original-title" => "Copy", 'escape' => false));$sn=$sn+1?> </td>
                                </tr><?php
                                }
						   }else{
                            $priice1 = ($user['price'] * $user['quantity']);
                                $priice =$priice+$priice1;
                                $quantity=$quantity+$user['quantity'];

                            ?>
                            <tr <?php echo $class; ?> > 

                                <td><?php echo h($sn); ?> &nbsp;</td>
                                <td>Invoice-<?php echo h($sn); ?> &nbsp;</td>
								<td><?php echo h($quantity); ?> &nbsp;</td>                               
                                <td>$<?php echo $priice ?> </td> 
                                  <td>
                                  <?php echo $this->Html->link('PDF Invoice', array('action' => 'admin_invoicepdf_split', $user['orderid'],$sn), array("class" => "btn btn-primary  btn-mini btn-left-margin", "data-original-title" => "Edit", 'escape' => false)); ?>
								  <?php echo $this->Html->link('View', array('action' => 'admin_split_invoice', $user['orderid'],$sn), array("class" => "btn btn-primary  btn-mini btn-left-margin", "data-original-title" => "Copy", 'escape' => false));$sn=$sn+1?> </td>
                            </tr>
                            <?php
							$priice =0;
							$quantity=0;
							$counter=0;
							 } ?>
                        <?php endforeach; ?>                
                    </tbody>
                   
                </table>
                <div class="pagination pagination-right pagination-mini">
                    <ul>
                        <?php echo $this->Paginator->prev(''); ?>
                        <?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2)); ?>
                        <?php echo $this->Paginator->next(''); ?>
                    </ul>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="gap20"></div>

        </div>
    </div>


</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"> Lot  Exchange</h4>
            </div>
            <div class="modal-body">
                <div class="something"> </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



<script>
    $(function () {

        $('.push').click(function () {
            var essay_id = $(this).attr('id');


            $.ajax({
                type: 'post',
                url: "<?php echo $this->webroot . 'admin/' . $this->params["controller"]; ?>/lot_exchange",
                data: 'order_id=' + essay_id + '&userid=' +<?php echo $Orderdetailid ?>,
                // in php you should use $_POST['post_id'] to get this value 
                success: function (r)
                {
                    // now you can show output in your modal 
                    $('#mymodal').show();  // put your modal id 
                    $('.something').show().html(r);
                }
            });


            $('#myModal').on('hidden', function () {
                $(this).removeData('modal');
            });

        });

    });
</script>