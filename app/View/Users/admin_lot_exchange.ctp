<div class="col-lg-12 text-center">

      <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title"><a class="btn"><?php echo $Orderdetail[0]['product_details'][0]['Product']['productname']?><br><?php echo $Orderdetail[0]['product_details'][0]['Product']['[product_ndc']?></a></h3>
      </div>
      <div class="panel-body">
       <div class="table-responsive">
             
			<?php echo $this->Form->create('User',array('action'=>'admin_client_orderinfo/'.$oid,'type'=>'file', 'class'=>'login-from')); ?>
            
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                        
                        <th><?php echo __('Lot NO #');?></th>
                        <th><?php echo __('INV#');?></th>
                        <th><?php echo __('Exchange');?></th>
                        <th><?php echo __('Quantity');?></th> 
                        <th><?php echo __('Expiray Date');?></th> 
                        <th><?php echo __('Price');?></th>
                        
                    </tr>
                </thead>
                
                <tbody>
                <?php if(empty($Orderdetail)): ?>
		<tr>
			<td colspan="7" class="striped-info">No record found.</td>
		</tr>
		<?php endif;  ?>
				<?php 
				$i=0;
				
				foreach ($Orderdetail as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				$defaulstlot=($user['OrderLot']['exchange_lot_id']!='')?$user['OrderLot']['exchange_lot_id']:$user['OrderLot']['productinfoid'];
				?>
                <tr <?php echo $class;?> > 
                <td><?php echo h($user['OrderLot']['lot_no']); ?>&nbsp;</td>
                <td><?php echo h($user['product_details'][0]['ProductInfo']['InvoiceNo']); ?>&nbsp;</td>              
                <td><?php echo $this->Form->input('lot'.$user['OrderLot']['id'],array('default'=>$defaulstlot,'empty'=>array(''=>'--Select Lot--'),'options' => $productlist,'value'=>$defaulstlot,
       'label' => false,
	   'required'=>'required',
        'class'=>'form-control'));?>&nbsp;</td>
                <td><?php echo h($user['OrderLot']['quantity']); ?>&nbsp;</td>
                <td><?php echo h(date("Y-m-d",strtotime($user['product_details'][0]['ProductInfo']['expdate']))); ?>
                    &nbsp;</td>
                <td><?php echo h($user['OrderLot']['quantity']*$user['OrderLot']['price']); ?></td>
                
                </tr>
                <?php endforeach; ?>                
                </tbody>
            </table>
            <?php echo $this->Form->input('exchange',array('type' => 'hidden','value'=>'submit'));?>
            <?php echo $this->Form->submit(__('Submit'), array('class'=>'btn btn-primary', 'div'=>false));?>&nbsp;

            <?php echo $this->Form->end();?>
            <div class="pagination pagination-right pagination-mini">
	
	</div>
       </div>
      </div>
    </div>


</div>
