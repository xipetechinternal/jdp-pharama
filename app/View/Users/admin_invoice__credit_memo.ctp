<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Invoice</title>
    <?php echo $this->Html->css(array('bootstrap.min'));?>
    <?php echo $this->Html->script(array('jquery-1.8.3.min'));?>
   
    <style>
      @import url(http://fonts.googleapis.com/css?family=Bree+Serif);
      body, h1, h2, h3, h4, h5, h6{
      font-family: 'Bree Serif', serif;
      }
   .inv-txt{
    font-family:sans-serif; padding: 5px 2px 2px 12px;font-size: 9px !important; font-weight: normal;font-family: sans-serif !important;
}
.panel-body{
    padding-top: 6px;
    
}
.table thead>tr>th, .table tbody>tr>th, .table tfoot>tr>th, .table thead>tr>td, .table tbody>tr>td, .table tfoot>tr>td{
    padding: 5px !important;
}
h2{
    margin-bottom: 5px;
}
h4{
	font-size:14px;
}
.container{
	font-size:11px;
}

    table { page-break-inside:auto }
   tr    { page-break-inside:avoid; page-break-after:auto }
    table.print-friendly tr td, table.print-friendly tr th {
        page-break-inside: avoid !important;
    }
    
    .panel{
        margin-bottom: 5px;
    }
    .sign-box{
        width: 100%;
        max-width: 215px;
        display: block;
        border: 0.1em solid #ddd;
        border-radius: 3px;
        height: 80px;
       
    }
    </style>
    <script type="text/javascript" src="/js/wkhtmltopdf_tableSplitHack.js"></script>
  </head>
  
  <body>
    <div class="container">
      <div class="row">
        <!--<div class="col-xs-5">
          <h1 class="col-xs-8 text-center">
            <img src="<?php echo $this->Html->url('/img/logo.jpg');?>" alt="" class="img-responsive">
          </h1>
        </div>-->
        <div class="col-xs-12 text-center">
         <div style="max-width:300px; margin:0 auto;">
         <br />
        	 <img style="max-width:175px;height: 100px;display: inline-block;" src="<?php echo $this->Html->url('/img/logo.jpg');?>" alt="" class="img-responsive">
          </div>
            <h3 class="text-center" style="margin-top: 0px;">Credit Memo</h3>
          <div class="text-right"><small>Date :  <?php echo date('m/d/Y',strtotime($Orderdetail['CreditReturn']['created']));?> </small> <br /><small>INVOICE/ORDER NO :  <?php echo $Orderdetail['CreditReturn']['credit_number'];?></small></div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-5">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h5>From: <a href="#">JDPharmaceutical Inc.</a></h5>
            </div>
            <div class="panel-body">
             
                6034 San Fernando Road
                Glendale, CA 91202<br/>
                State No: WLS 6124<br/>
                Tel.818.956.0578<br/>
                Fax.818.956.0721<br/>
                Email : info@jdpwholesaler.com<br/>
                Website : www.jdpwholesaler.com
              
            </div>
          </div>
        </div>
        <div class="col-xs-5 col-xs-offset-2 text-right pull-right">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h5>To : <a href="#"><?php echo $User['User']['fname'];?></a></h5>
            </div>
            <div class="panel-body">
           
               <?php echo $User['User']['address'];?>,<?php echo $User['User']['city'];?>, 
               <?php echo $User['User']['state'];?>,<?php echo $User['User']['zip_code'];?><br />
                 
                State No : <?php echo $User['User']['state_no'];?>
                 <br />
                State Exp : <?php echo $User['User']['state_no_exp'];?>
                <br />
                D.E.A No : <?php echo $User['User']['den_no'];?>
                 <br />
                D.E.A. EXPIRATION DATE : <?php echo $User['User']['den_no_exp'];?>
                 <br />
              <!-- Business name : <?php echo $User['User']['contact_name'];?>
               <br />-->
                Tel : <?php echo $User['User']['phone'];?>, Fax : <?php echo $User['User']['fax'];?><br />
            
             
            </div>
          </div>
        </div>
      </div>
     
      <!-- / end client details section -->
      <table class="table table-bordered print-friendly" >
        <thead>
          <tr>
            <th>
              <h4>Item(s)</h4>
            </th>
            <th>
              <h4>Description</h4>
            </th>
            <th>
              <h4>Qty</h4>
            </th>
            <th>
              <h4>Original Price</h4>
            </th>
             <th>
              <h4>Restocking Fees</h4>
            </th>
            
            <th>
              <h4>Total</h4>
            </th>
          </tr>
        </thead>
       <tbody>

<?php if(empty($Orderdetail)): ?>
		<tr>
			<td colspan="7" class="striped-info">No record found.</td>
		</tr>
		<?php endif;  ?>
 				
                
  <tr>
    <td><?php echo $Orderdetail['CreditReturn']['product_lot']?></td>
   
    <td class="td-font"><?php echo $products['Products']['productname']?></br><?php echo $products['Products']['product_ndc']?></td>
    <td class="td-font" align="left"><?php echo $Orderdetail['CreditReturn']['quantity']?></td>
     <td class="td-font"><?php echo $Orderdetail['CreditReturn']['product_price']?></td>
    <td class="td-font"><?php echo $Orderdetail['CreditReturn']['restocking_fees']?></td>
    
    <td class="td-font"><?php echo ($Orderdetail['CreditReturn']['quantity']*$Orderdetail['CreditReturn']['product_price'])+$Orderdetail['CreditReturn']['restocking_fees']?></td>

    
  </tr>
  
    
          
        </tbody>
      </table>
      
      <div class="clearfix"></div>
      
       <div class="clearfix"></div>
       
       
        <div class="col-xs-10 col-xs-offset-4 text-right">
          <br><br>
        <button type="button" class="btn btn-success print" data-dismiss="modal">Print</button> <br /><br />
        </div>
       <div class="clearfix"></div>
       <div style=" " class="footer">
       <div class="col-xs-12 col-xs-offset-12 text-left"> Sign and print name below indicates pharmacist in charge has received the items.</div>
       <br />
       <div class="col-xs-8 col-xs-offset-8 text-left">       
       <span class="sign-box">&nbsp;</span>
       </div>
      </div>
      <div class="clearfix"></div>
      <br />
             <footer class="text-center">For returns please contact our customer service at Tel. 818.956.0578 or  email: drodriguez_jdp@yahoo.com</footer>
       <br><br>
 
    </div>
    
    <script>
	$('.print').click(function(){
		$('.print').hide();
     window.print();
	 $('.print').show();
});</script>
  </body>
</html>