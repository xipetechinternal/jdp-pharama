<div class="col-lg-12  row">
    <div class="col-lg-6">
        <h4><a href="<?php echo $this->Html->url('/admin/users'); ?>">Home</a> :: Order Information For Customer <?php echo $user_info['User']['fname'] ?></h4>   
    </div>
    <div class="col-lg-6">
    </div>
</div>

<div class="col-lg-12 text-right" style="padding:20px">
    
</div>
<!--<div class="col-lg-12 text-right" style="padding:20px"> &nbsp;&nbsp;<a class="btn btn-info btn-mini btn-left-margin" href="<?php echo $this->Html->url(array_merge(array('controller' => 'users', 'action' => 'client_order_history_csv'), $this->params['named'], array(''))) . '/Order-' . date('m-d-Y-His-A') . '.csv'; ?>"><i class="icon-file-text"></i> Download Excel/CSV</a></div>-->
<div class="col-lg-12 text-center">
    <div class="panel panel-primary">


        <div class="col-lg-12"> 
            <?php echo $this->Session->flash(); ?>

            <?php echo $this->Form->create('OrderHistory', array('type' => 'GET', 'url' => array('action' => 'admin_client_order_history/' . $uid, 'controller' => 'users'), 'class' => 'paraform')); ?>
            <?php
            echo $this->Form->input('orderid', array('placeholder' => 'From Date',
                'label' => false,
                'type' => 'hidden',
                'required' => 'required',
                'class' => 'form-control'));
            ?> 

            <div class="form-group">
                <div class="col-lg-2">
                    <label for="exampleInputEmail1">Select Date:</label>
                </div>
                <div class="col-lg-8">
                    <div class="form-group">
                        <div class="col-lg-4">
                            <?php
                            echo $this->Form->input('selfrmdate', array('placeholder' => 'From Date',
                                'label' => false,
                                'value' => $selfrmdate,
                                'class' => 'form-control'));
                            ?> 

                        </div>
                        <div class="col-lg-4">
                            <?php
                            echo $this->Form->input('seltodate', array('placeholder' => 'To Date',
                                'label' => false,
                                'value' => $seltodate,
                                'class' => 'form-control'));
                            ?>
                            <?php
                            echo $this->Form->input('uid', array('placeholder' => 'To Date',
                                'label' => false,
                                'value' => $uid,
                                'type' => 'hidden',
                                'class' => 'form-control'));
                            ?>

                        </div>
                        <div class="col-lg-4">
                            <?php
                            echo $this->Form->button('<i class="fa fa-search"></i> ', array(
                                'type' => 'submit',
                                'class' => 'btn btn-default',
                                'escape' => false
                            ));
                            ?>

                        </div>


                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-lg-2">
                <label for="exampleInputEmail1">Product NDC:</label>
            </div>
            <div class="col-lg-4">   <?php
                echo $this->Form->input('ndcproduct', array('placeholder' => 'Product NDC',
                    'label' => false,
                    'value' => $search,
                    'class' => 'form-control'));
                ?> 

            </div>
            <div class="clearfix"></div>
            <?php echo $this->Form->end(); ?>
            <div class="clearfix"></div>
            <div class="gap20"></div>





        </div>
        <div class="col-lg-12 text-left" style="padding:20px">
            <?PHP
            $searcset = "?";
            $searcset.=(isset($selfrmdate) and isset($seltodate)) ? "selfrmdate=$selfrmdate&seltodate=$seltodate" : '';
            $searcset.=(isset($search)) ? "ndcproduct=$search" : "";
            ?>
			<a  class="btn btn-primary btn-mini btn-left-margin" href="<?php echo $this->Html->url(array_merge(array('controller' => 'users', 'action' => 'admin_client_order_download_pdf'), $this->params['named'], array(''))) . '/' . $user_info['User']['id'] ?>"><i class="icon-file-text"></i>Download All PDF</a>

    <?php if (empty($customer_fields)) { ?>
        <a  class="btn btn-primary btn-mini btn-left-margin" href="<?php echo $this->Html->url(array_merge(array('controller' => 'users', 'action' => 'admin_client_order_unpaid_batch'), $this->params['named'], array(''))) . '/' . $user_info['User']['id'] . $searcset; ?>"><i class="icon-file-text"></i>UnPaid Invoices</a>
    <?php } else { ?>
        <button class="btn btn-primary btn-mini btn-left-margin" disabled="disabled"><i class="icon-file-text"></i>UnPaid Invoices</button>
    <?PHP } ?>

            <a class="btn btn-primary btn-mini btn-left-margin" href="<?php echo $this->Html->url(array_merge(array('controller' => 'users', 'action' => 'admin_client_order_history_purchase_size_excel'), $this->params['named'], array(''))) . '/' . $user_info['User']['id'] . $searcset; ?>"><i class="icon-file-text"></i> Download Purchase Size</a>
          
            <a class="btn btn-primary btn-mini btn-left-margin" href="<?php echo $this->Html->url(array_merge(array('controller' => 'users', 'action' => 'admin_client_order_history_excel'), $this->params['named'], array(''))) . '/' . $user_info['User']['id'] . $searcset; ?>"><i class="icon-file-text"></i> Download Excel/CSV</a>
            &nbsp;
            <a class="btn btn-primary btn-mini btn-left-margin" href="<?php echo $this->Html->url(array_merge(array('controller' => 'users', 'action' => 'admin_client_order_history_excel_undc'), $this->params['named'], array(''))) . '/' . $user_info['User']['id'] . $searcset; ?>"><i class="icon-file-text"></i> Download Excel/CSV Total</a>
            <a href="javascript:;" onclick="history.go(-1);" class="btn btn-primary btn-mini btn-left-margin">Back</a>
            
        </div>
        <div class="panel-heading">
            <h3 class="panel-title">Order History</h3>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <h2> Placed Orders</h2>

                <table id="example" class="datatable display table table-striped table-bordered" width="100%" >
                    <thead>
                        <tr>

                            <th>Select</th>
                            <th><strong><?php echo __('Order'); ?></strong></th>
                            <th><strong><?php echo __('PO Number'); ?></strong></th>
                            <th><strong><?php echo __('Quantity'); ?></strong></th>
                            <th><strong><?php echo __('Price'); ?></strong></th>
                            <th><strong><?php echo __('Status'); ?></strong></th>

                            <th><strong><?php echo __('ORDER DATE'); ?></strong></th>
                            <th><strong><?php echo __('Due Date'); ?></strong></th>
                            <th><strong><?php echo __('Amount'); ?></strong></th>
                            <th><strong><?php echo __('Invoice'); ?></strong></th>
                        </tr>
                    </thead>
                    <?php if (empty($users)): ?>
                        <tr>
                            <td colspan="7" class="striped-info">No record found.</td>
                        </tr>
                    <?php endif; ?>

                    <tbody>

                        <?php
                        $i = 0;
                        $product_price = 0;
                        $total_paid = 0;
                        $total_unpaid = 0;
                        foreach ($users as $user):
                            $class = ' class="odd"';
                            if ($i++ % 2 == 0) {
                                $class = ' class="even"';
                            }

                            $product_qauantity = count($user['OrderdetailMany']);

                            $priice = 0;
                            foreach ($user['OrderdetailMany'] as $orderdeatils) {
                                $priice = $priice + ($orderdeatils['price'] * $orderdeatils['quantity']);
                            }
                            $product_price = $product_price + $priice;
                            ?>
                            <tr <?php echo $class; ?> > 
                                
                                <td><input class="checked" type="checkbox" value="<?php echo $user['OrderDist']['checked']; ?>" data-bind="<?php echo h($user['OrderDist']['id']); ?>" <?php echo $user['OrderDist']['checked'] == 1 ? "checked" : ""; ?> name = "selected"><span></span></td>
                                <td <?php echo h(($user['OrderDist']['Paid_Status'] == 'paid') ? 'style=color:blue' : ""); ?>>
                                <?php if($user['OrderDist']['status']=="Test"){?>
                                <a data-tmporderid="<?php echo $user['OrderDist']['tmporderid']?>"  href="javascript:;" id="<?php echo $user['OrderDist']['id'] ?>" class="tmporderidchange"><?php echo h($user['OrderDist']['tmporderid']);?>-Change</a> &nbsp;
                                    <?php }else{
										echo h($user['OrderDist']['tmporderid']); } ?>
								 &nbsp;</td>
                                <td><?php echo h($user['OrderDist']['po_number']); ?>&nbsp;<a data-ponumber="<?php echo h($user['OrderDist']['po_number']); ?>"  href="javascript:;" id="<?php echo $user['OrderDist']['id'] ?>" class="push">-Change</a> &nbsp;</td>


                                <td>  <?php echo $this->Html->link($product_qauantity . ' Products', array('controller' => 'Users', 'action' => 'admin_client_orderinfo', $user['OrderDist']['id']), array('class' => '')); ?>
                                    &nbsp;</td>
                                <td>$<?php echo number_format($priice, 2) ?> </td>
                                
                                <td><?php echo h(($user['OrderDist']['status'] == 'Accep') ? 'Accept' : $user['OrderDist']['status']); ?> &nbsp;</td>  
                                
                                <td><?php echo date("m-d-Y", strtotime($user['OrderDist']['createddt'])); ?> &nbsp;
                                	<?php if($user['OrderDist']['status']=="Test"){?><a data-ponumber="<?php echo date("m-d-Y", strtotime($user['OrderDist']['createddt'])); ?>"  href="javascript:;" id="<?php echo $user['OrderDist']['id'] ?>" class="pushdatechange">-Change</a> &nbsp;
                                    <?php }?>
                                </td> 
                                
                                <td><?php
                                    if ($user_info['User']['client_due_datea'] == '15 Days') {
                                        echo $due_datea = date('m-d-Y', strtotime($user['OrderDist']['createddt'] . ' + 15 day'));
                                    } elseif ($user_info['User']['client_due_datea'] == '30 days') {
                                        echo $due_datea = date('m-d-Y', strtotime($user['OrderDist']['createddt'] . ' + 30 day'));
                                    } elseif ($user_info['User']['client_due_datea'] == 'per week') {

                                        for ($i = 0; $i <= 6; $i++) {

                                            $due_datea_t = date('Y-m-d', strtotime($user['OrderDist']['createddt'] . " + $i day"));

                                            if (date('l', strtotime($due_datea_t)) == 'Tuesday') {
                                                if (date('W', strtotime($due_datea_t)) != date('W', strtotime($user['OrderDist']['createddt'])))
                                                    echo $due_datea = date('m-d-Y', strtotime($due_datea_t . ' + 0 day'));
                                                else
                                                    echo $due_datea = date('m-d-Y', strtotime($due_datea_t . ' + 7 day'));
                                            }
                                        }
                                    }else {
                                        echo $due_datea = date('m-d-Y', strtotime($user['OrderDist']['createddt'] . ' + 0 day'));
                                    }
                                    ?> &nbsp;
                                </td>  
                                <td <?php if (strtotime($due_datea) < strtotime(date("Y-m-d")) and $user['OrderDist']['Paid_Status'] == 'unpaid') { ?>style=" background-color:#d2322d"<?php } ?>><?php if (strtotime($due_datea) < strtotime(date("Y-m-d")) and $user['OrderDist']['Paid_Status'] == 'unpaid') { ?>$<?php echo number_format($priice, 2);
                                }
                                    ?> &nbsp;</td>  
                                <td class="td-font" style="text-align:left">
                                
                                	<?php if($user['OrderDist']['status']=="Test"){?><a data-ponumber="<?php echo $user['OrderDist']['tmporderid']; ?>"  href="javascript:;" id="<?php echo $user['OrderDist']['id'] ?>" class="btn btn-primary  btn-mini btn-left-margin additem">Add Item</a> &nbsp;
                                    <?php }?>
                                    <?php if ($user['OrderDist']['status'] == 'Accep' or $user['OrderDist']['status'] == 'Test') { ?>
                                        <?php echo $this->Html->link('Invoice', array('action' => 'admin_invoice', $user['OrderDist']['id']), array("class" => "btn btn-primary  btn-mini btn-left-margin", "data-original-title" => "Edit", 'escape' => false)); ?>
                                        <?php echo $this->Html->link('Filler Sheet', array('controller' => 'Orders','action' => 'admin_filler_sheetpdf_byclient', $user['OrderDist']['id']), array("class" => "btn btn-primary  btn-mini btn-left-margin", "data-original-title" => "Edit", 'escape' => false)); ?>
                                        
										<?php echo $this->Html->link('PDF Invoice', array('action' => 'admin_invoicepdf', $user['OrderDist']['id']), array("class" => "btn btn-primary  btn-mini btn-left-margin", "data-original-title" => "Edit", 'escape' => false)); ?>
                                        <?php echo $this->Html->link('Split Invoice', array('action' => 'admin_client_invoice_split', $user['OrderDist']['id'],$user['OrderDist']['userid']), array("class" => "btn btn-primary  btn-mini btn-left-margin", "data-original-title" => "Copy", 'escape' => false));?>

                                    <?php } ?>
                                    <?php echo $this->Html->link($this->Html->tag('i', "Pedigree", array('class' => 'icon-pencil icon-white')), array('controller' => 'Users', 'action' => 'admin_client_orderinfo', $user['OrderDist']['id']), array("class" => "btn btn-primary  btn-mini btn-left-margin", "data-original-title" => "Manually", 'escape' => false)); ?>
                                   
                                    <?php
                                    if ($user['OrderDist']['Paid_Status'] == 'unpaid') {
                                        if ($user['OrderDist']['batch_no'] == $customer_fields['CustomerField']['batch_invoice'] and $customer_fields['CustomerField']['batch_invoice_status'] == 'unpaid') {
                                            ?><button class="btn btn-primary btn-xs  btn-mini btn-left-margin" disabled="disabled">Pay B-<?php echo substr($customer_fields['CustomerField']['batch_invoice'], 4, -4); ?></button>
                                            <?PHP
                                        } else {
                                            echo $this->Html->link($this->Html->tag('i', "Pay", array('class' => 'icon-pencil icon-white')), array('controller' => 'Users', 'action' => 'admin_client_paid', $user['OrderDist']['id'], $user['OrderDist']['userid']), array("class" => "btn btn-primary  btn-mini btn-left-margin", "data-original-title" => "Manually", 'escape' => false));
                                        }
                                        $total_unpaid = $total_unpaid + $priice;
                                    } else {
                                        echo $this->Html->link($this->Html->tag('i', "Paid", array('class' => 'icon-pencil icon-white')), array('controller' => 'Users', 'action' => 'admin_client_unpaid', $user['OrderDist']['id'], $user['OrderDist']['userid']), array("class" => "btn btn-primary  btn-mini btn-left-margin", "data-original-title" => "Manually", 'escape' => false));
                                        $total_paid = $total_paid + $priice;
                                    }
                                    ?>
                                    <?php if ($user['OrderDist']['status'] == 'Test') {   
											echo $this->Html->link('Order Copy', array('action' => 'admin_order_copy', $user['OrderDist']['id'],$user['OrderDist']['userid']), array("class" => "btn btn-primary  btn-mini btn-left-margin", "data-original-title" => "Copy", 'escape' => false));
									}?>
                                     <?php
                                    echo $this->Form->postLink($this->Html->tag('i', 'Delete', array('class' => 'icon-remove icon-white')), array('action' => 'admin_order_delete', $user['OrderDist']['id']), array("class" => "btn btn-danger btn-mini tooltip-top", "data-original-title" => "Delete", 'escape' => false), __('Are you sure you want to Delete  order ?', $user['Products']['productname']));
                                    ?>
                                </td>

                            </tr>
                    <?php endforeach; ?>                
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="3" style="text-align:right">Total Paid:</th>
                            <th><?php /* echo $total_paid */ ?><?php echo h('$' . number_format($totalPaid, 2)); ?></th>
                        </tr>

                        <tr>
                            <th colspan="3" style="text-align:right">Total UnPaid:</th>
                            <th><?php /* echo $total_unpaid */ ?><?php echo h('$' . number_format($totalUnpaid, 2)); ?></th>
                        </tr>
                        <tr>

                        <tr>
                            <th colspan="3" style="text-align:right">Total Return Credit:</th>
                            <th><?php /* echo $total_unpaid */ ?><?php echo h('$' . number_format($totalReturn, 2)); ?></th>
                        </tr>
                        <tr>
                            <th colspan="3" style="text-align:right">Total Remaining  UnPaid:</th>
                            <th><?php /* echo $total_unpaid */ ?><?php echo h('$' . number_format($totalUnpaid - $totalReturn, 2)); ?></th>
                        </tr>
                    <th colspan="3" style="text-align:right">Total:</th>
                    <th><?php /* echo $product_price */ ?><?php echo h('$' . number_format($totalUnpaid + $totalPaid, 2)); ?></th>
                    </tr>
                    </tfoot>
                </table>
                <div class="pagination pagination-right pagination-mini">
                    <ul>
                        <?php echo $this->Paginator->prev(''); ?>
                        <?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2)); ?>
                            <?php echo $this->Paginator->next(''); ?>
                    </ul>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="clearfix"></div>
            <!-- Added By Rashid Khan -->
            <!--<div class="table-responsive">
                <strong> Net Paid: <?php echo h('$' . number_format($totalPaid, 2)); ?></strong><br>
                <strong> Net Unpaid: <?php echo h('$' . number_format($totalUnpaid, 2)); ?></strong><br>
                
              </div>-->
            <!--  END  -->
            <div class="gap20"></div
            <div class="gap20"></div>
            <h2> Pending Orders</h2>

            <div class="table-responsive">
                <table id="example" class="datatable display table table-striped table-bordered" width="100%" >
                    <thead>
                        <tr>
                            <th><?php echo __('Sl no.'); ?></th>
                            <th><strong><?php echo __('Order'); ?></strong></th>
                            <th><strong><?php echo __('PO Number'); ?></strong></th>
                            <th><strong><?php echo __('Quantity'); ?></strong></th>
                            <th><strong><?php echo __('Price'); ?></strong></th>

                        </tr>
                    </thead>
<?php if (empty($pendingorder)): ?>
                        <tr>
                            <td colspan="7" class="striped-info">No record found.</td>
                        </tr>
<?php endif; ?>

                    <tbody>

                        <?php
                        $i = 0;
                        $product_price = 0;

                        foreach ($pendingorder as $user):
                            $class = ' class="odd"';
                            if ($i++ % 2 == 0) {
                                $class = ' class="even"';
                            }

                            $product_qauantity = count($user['OrderdetailMany']);

                            $priice = 0;
                            foreach ($user['OrderdetailMany'] as $orderdeatils) {
                                $priice = $priice + ($orderdeatils['price'] * $orderdeatils['quantity']);
                            }
                            $product_price = $product_price + $priice;
                            ?>
                            <tr <?php echo $class; ?> > 

                                <td><?php echo $i; ?> &nbsp;</td>
                                <td><?php echo h($user['Order']['tmporderid']); ?> &nbsp;</td>

                                <td><?php echo h($user['Order']['po_number']); ?> &nbsp;</td>

                                <td>  <?php echo $this->Html->link($product_qauantity . ' Products', array('controller' => 'Users', 'action' => 'admin_client_orderinfo', $user['Order']['id']), array('class' => '')); ?>
                                    &nbsp;</td>
                                <td>$<?php echo $priice ?> </td>  

                            </tr>
<?php endforeach; ?>                
                    </tbody>
                    <tfoot>
                        <tr>

                            <th colspan="3" style="text-align:right">Total:</th>
                            <th>$<?php echo $product_price ?></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>


<div class="modal fade change-date-modal" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
<?php echo $this->Form->create('OrderHistory', array('type' => 'GET', 'url' => array('action' => 'admin_client_order_date_edit/' . $uid, 'controller' => 'users'), 'class' => 'paraform')); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">Edit ORDER DATE</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">         

                    <?php
                    echo $this->Form->input('created_change', array('placeholder' => 'ORDER DATE',
                        'label' => false,
                        'type' => 'text',
                        'required' => 'required',
                        'class' => 'form-control'));
                    ?> 
                    <?php
                    echo $this->Form->input('order_id_crt', array('placeholder' => 'PO Number',
                        'label' => false,
                        'type' => 'hidden',
                        'required' => 'required',
                        'class' => 'form-control'));
                    ?> 
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
<?php echo $this->Form->end(); ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade bs-example-modal-sm" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
<?php echo $this->Form->create('OrderHistory', array('type' => 'GET', 'url' => array('action' => 'admin_client_order_po_edit/' . $uid, 'controller' => 'users'), 'class' => 'paraform')); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">Edit PO Number</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">         

                    <?php
                    echo $this->Form->input('po_number', array('placeholder' => 'PO Number',
                        'label' => false,
                        'type' => 'text',
                        'required' => 'required',
                        'class' => 'form-control'));
                    ?> 
                    <?php
                    echo $this->Form->input('order_id', array('placeholder' => 'PO Number',
                        'label' => false,
                        'type' => 'hidden',
                        'required' => 'required',
                        'class' => 'form-control'));
                    ?> 
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
<?php echo $this->Form->end(); ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<div class="modal fade orderid" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
<?php echo $this->Form->create('OrderHistory', array('type' => 'GET', 'url' => array('action' => 'admin_client_order_temporder_edit/' . $uid, 'controller' => 'users'), 'class' => 'paraform')); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">Edit Order Id</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">         

                    <?php
                    echo $this->Form->input('temp_number', array('placeholder' => 'Order Id',
                        'label' => false,
                        'type' => 'text',
                        'required' => 'required',
                        'class' => 'form-control'));
                    ?> 
                    <?php
                    echo $this->Form->input('tmp_order_id', array('placeholder' => 'PO Number',
                        'label' => false,
                        'type' => 'hidden',
                        'required' => 'required',
                        'class' => 'form-control'));
                    ?> 
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
<?php echo $this->Form->end(); ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<div class="modal fade bs-additem-modal-sm" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
<?php echo $this->Form->create('OrderHistoryitem', array('type' => 'GET', 'url' => array('action' => 'admin_client_order_add_itme/' . $uid, 'controller' => 'users'), 'class' => 'paraform')); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">Add Item</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">         

                   <?php				   
                        echo $this->Form->input('prodid', array('options' => $product_list,
                            'empty' => array('' => '--Select Product Name--'),
                            'label' => false,
                            'required' => 'required',
                            'class' => 'form-control'));
                        ?> 
                    <?php
                    echo $this->Form->input('quantity', array('placeholder' => 'Quantity',
                        'label' => false,
                        'type' => 'text',
                        'required' => 'required',
                        'class' => 'form-control'));
                    ?>
                    <?php
                    echo $this->Form->input('tmporderid', array('placeholder' => 'PO Number',
                        'label' => false,
                        'type' => 'hidden',
                        'required' => 'required',
                        'class' => 'form-control'));
                    ?>
                    <?php
                    echo $this->Form->input('order_id', array('placeholder' => 'PO Number',
                        'label' => false,
                        'type' => 'hidden',
                        'required' => 'required',
                        'class' => 'form-control'));
                    ?> 
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
<?php echo $this->Form->end(); ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<script>
    $(document).ready(function () {
        $("#ProductInfoAdminSalesReportPersonForm").validate();
        //$( "#ProductInfoSelfrmdate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
        // $( "#ProductInfoSeltodate" ).datepicker({dateFormat: 'yy-mm-dd' }); 


        $("#OrderHistorySelfrmdate, #OrderHistorySeltodate").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            dateFormat: 'yy-mm-dd',
            onSelect: function (selectedDate) {
                if (this.id == 'OrderHistorySelfrmdate') {
                    var dateMin = $('#OrderHistorySelfrmdate').datepicker("getDate");
                    var rMin = new Date(dateMin.getFullYear(), dateMin.getMonth(), dateMin.getDate() + 1); // Min Date = Selected + 1d
                    var rMax = new Date(dateMin.getFullYear(), dateMin.getMonth(), dateMin.getDate() + 365); // Max Date = Selected + 31d
                    $('#OrderHistorySeltodate').datepicker("option", "minDate", rMin);
                    $('#OrderHistorySeltodate').datepicker("option", "maxDate", rMax);
                }

            }
        });

    });
</script>

<script>
    $(function () {

        $('.push').click(function () {
            var essay_id = $(this).attr('id');
            $("#OrderHistoryPoNumber").val($(this).data('ponumber'));
            $("#OrderHistoryOrderId").val(essay_id);
            $('.bs-example-modal-sm').modal('show');

        });
		
		 $('.pushdatechange').click(function () {
            var essay_id = $(this).attr('id');
            $("#OrderHistoryCreatedChange").val($(this).data('ponumber'));
            $("#OrderHistoryOrderIdCrt").val(essay_id);
            $('.change-date-modal').modal('show');
			
					$("#OrderHistoryCreatedChange").datepicker({            
					changeMonth: true,
					dateFormat: 'mm-dd-yy',
					});

        });
		
		$('.tmporderidchange').click(function () {
            var essay_id = $(this).attr('id');
			console.log(essay_id);
            $("#OrderHistoryTempNumber").val($(this).data('tmporderid'));
            $("#OrderHistoryTmpOrderId").val(essay_id);
            $('.orderid').modal('show');
			
					

        });
		
		$('.additem').click(function () {
            var essay_id = $(this).attr('id');
            $("#OrderHistoryitemTmporderid").val($(this).data('ponumber'));
            $("#OrderHistoryitemOrderId").val(essay_id);
            $('.bs-additem-modal-sm').modal('show');

        });

    });



</script>
<script>
    $(".checked").click(function () {
        value = $(this).val();
        th = $(this);
        id    = $(this).attr("data-bind");
        string = $.trim(id)+"/"+$.trim(value);
        url   = "<?php echo $this->Html->url(array("controller"=>"users","action"=>"checked"))?>/"+string;
        $.get(url,function(data){
            data = JSON.parse(data);
            th.val(data.value);
        })
        
    })
</script>
