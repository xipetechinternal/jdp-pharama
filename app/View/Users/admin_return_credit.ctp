<style>
.navbar{
	z-index:0;
}
</style>
<div class="col-lg-10  main">
         <h4><a href="dashboard.php">Home</a> ::   Return Credit For Customers <?php echo $user_info['User']['fname']?></h4>
         </div>
<div class="clearfix"></div>
 <div class="gap20"></div>
          




<div class="table-responsive">
    <h3>Existing Code--</h3>          
 
 	
    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                     
                     
                        <th><?php echo __('Credit Number');?></th>
                        <th><?php echo __('Quantity');?></th>
                        <th><?php echo __('Amount');?></th>
                        <th><?php echo __('Date');?></th>
                        <th><?php echo __('Action');?></th>                  
                       
                    </tr>
                </thead>
                
                <tbody>
                <?php if(empty($CreditReturnlist)): ?>
		<tr>
			<td colspan="5" class="striped-info">No record found.</td>
		</tr>
		<?php endif;  ?>
        
				<?php 
				$i=0;
				
						
				foreach ($CreditReturnlist as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				
				?>
                <tr <?php echo $class;?> >               
               
                <td><?php echo h($user['CreditReturn']['credit_number']); ?></td>
                 <td><?php echo h($user['CreditReturn']['quantity']); ?></td>
                <td><?php echo number_format(($user['CreditReturn']['quantity']*$user['CreditReturn']['product_price'])+$user['CreditReturn']['restocking_fees'],2); ?></td>
                <td>	<?php echo date("m-d-Y",strtotime($user['CreditReturn']['created'])); ?></td>
                <td><?php
				if($user['CreditReturn']['status']=='Y'){
					 echo $this->Form->postLink($this->Html->tag('i','Payed',array('class'=>'icon-remove icon-white')), array('action' => 'admin_return_credit_pay', $user['CreditReturn']['id'], $user_info['User']['id']), array('disabled'=>'disabled',"class"=>"btn btn-primary btn-mini btn-xs tooltip-top","data-original-title"=>"Delete",'escape'=>false), __('Are you sure you want to Pay # %s?', $user['CreditReturn']['credit_number'])); 
				}else{
				 echo $this->Form->postLink($this->Html->tag('i','Pay',array('class'=>'icon-remove icon-white')), array('action' => 'admin_return_credit_pay', $user['CreditReturn']['id'], $user_info['User']['id']), array("class"=>"btn btn-primary btn-mini btn-xs tooltip-top","data-original-title"=>"Delete",'escape'=>false), __('Are you sure you want to Pay # %s?', $user['CreditReturn']['credit_number'])); }
				 echo "&nbsp;&nbsp;".$this->Form->postLink("Credit Memo", array('action' => 'admin_invoice_CreditMemo', $user['CreditReturn']['id'], $user_info['User']['id']), array("class"=>"btn btn-primary btn-mini btn-xs tooltip-top"));
				 ?>
                 </td>
               
             
                </tr>
                <?php endforeach; ?>                
                </tbody>
                <tfoot>
          
        </tfoot>
            </table>
           
            <div class="pagination pagination-right pagination-mini">
	  <ul>
		<?php echo $this->Paginator->prev(''); ?>
		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
		<?php echo $this->Paginator->next(''); ?>
	  </ul>
	</div>
    <div class="gap20"></div>
     <?php echo $this->Form->create('order',array('inputDefaults' => array('label' => false),'url'=>array('action'=>'return_credit/'.$user_info['User']['id'],'controller'=>'users'),'type'=>'post', 'class'=>'form-horizontal login-from')); ?>  

    <h3>New Code--</h3>
                
<table id="productTable" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th><?php echo __('Product NDC');?></th>
                        <th><?php echo __('Product Details');?></th>
                         <th><?php echo __('Lot Number');?></th>
                        <th><?php echo __('Expiry');?></th>
                        <th><?php echo __('Price Per Item');?></th>
                        <th><?php echo __('Quantity');?></th>
                        <th><?php echo __('Total Price');?></th>
                        <th><?php echo __('Restocking Fees');?></th>
                        <th><?php echo __('Extended Price');?></th> 
                        <th><?php echo __('Action');?></th> 
                            </tr>
                        </thead>
                        <tbody>                                    
                        <tr>
                               <td><?php echo $this->Form->input('product_ndc',array('name'=>'product_ndc[]','placeholder' => 'NDC','label' => false,'class'=>'form-control product_ndc'));?> &nbsp;</td>
                <td><?php echo $this->Form->input('prodid',array('name'=>'prodid[]','options' =>$products,//$products
			 'empty'=>array(''=>'--Select Product Name--'),
        'label' => false,
		'required'=>'required',
        'class'=>'form-control prodid'));?></td>
                <td><?php echo $this->Form->input('product_lot',array('name'=>'product_lot[]','required'=>'required','placeholder' => 'Lot','label' => false,'class'=>'form-control'));?></td>
                <td><?php echo $this->Form->input('lot_expiry',array('name'=>'lot_expiry[]','required'=>'required','id'=>'date','placeholder' => 'Expiry','label' => false,'class'=>'datepick form-control'));?></td>
                <td><?php echo $this->Form->input('product_price',array('name'=>'product_price[]','required'=>'required','placeholder' => 'Price','label' => false,'class'=>'product_price form-control'));?>&nbsp;</td>
                <td><?php echo $this->Form->input('quantity',array('name'=>'quantity[]','required'=>'required','placeholder' => 'Quantity','label' => false,'class'=>'form-control quantity'));?></td>
                 <td><?php echo $this->Form->input('total_price',array('name'=>'total_price[]','placeholder' => 'T Price','label' => false,'readonly'=>'readonly','class'=>'form-control tprice'));?></td>
                 <td><?php echo $this->Form->input('restocking_fees',array('name'=>'restocking_fees[]','placeholder' => 'Restocking Fees','label' => false,'class'=>'form-control restocking_fees'));?></td>
                 <td>  <?php echo $this->Form->input('extended_price',array('name'=>'extended_price[]','readonly'=>'readonly','placeholder' => 'Extended Price','label' => false,'class'=>'form-control extendedprice'));?> </td>
               <td><a id="addnew" class="btn btn-primary" href="javascript:;">Add</a></td>

</tr>
</tbody>
<tfoot>
<tr>
          <td colspan="9" align="right">
          	<?php echo $this->Form->button('Return Credit', array(
    'type' => 'submit',
    'class' => 'btn btn-primary',
    'escape' => false
));?>
          </td>
            
          </tr>
          </tfoot>
</table>

             <?php echo $this->Form->end();?>
       </div>
<div class="gap-20"></div>
		

   <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
      </div>
      </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->   
      
<script>


// start a counter for new row IDs
// by setting it to the number
// of existing rows
$(".datepick").datepicker({dateFormat: 'yy-mm-dd' ,minDate: 0 });
var newRowNum = 0;
//var products_jshon=<?php echo $products_jshon;?>;
// bind a click event to the "Add" link
$('#addnew').click(function() {
	
	

    $(".datepick").datepicker("destroy");

    // increment the counter
    newRowNum = $(productTable).children('tbody').children('tr').length + 1;

    // get the entire "Add" row --
    // "this" refers to the clicked element
    // and "parent" moves the selection up
    // to the parent node in the DOM
    var addRow = $(this).parent().parent();

    // copy the entire row from the DOM
    // with "clone"
    var newRow = addRow.clone();

    

    // insert a remove link in the last cell
    $('td:last-child', newRow).html('<a href="" class="remove">Remove<\/a>');


    // insert the new row into the table
    // "before" the Add row
    addRow.before(newRow);

	// set the values of the inputs
    // in the "Add" row to empty strings
    $('input', addRow).val('');
    

    // add the remove function to the new row
    $('a.remove', newRow).click(function() {
        $(this).closest('tr').remove();
        return false;
    });

    $('#date', newRow).each(function(i) {
        var newID = 'date_' + newRowNum;
        $(this).attr('id', newID);

    });
	
	$('#orderProdid', newRow).each(function(i) {
        var newID = 'orderProdid_' + newRowNum;
        $(this).attr('id', newID);

    });
	
	$(".datepick").datepicker({dateFormat: 'yy-mm-dd' ,minDate: 0 });
	
	$( ".product_price",newRow).on( "blur", function() {
		
        if($(this).val()!='' && $(this).parents('tr').find('.quantity').val()!=''){
			var price=($(this).parents('tr').find('.quantity').val())*$(this).val();
			
			$(this).parents('tr').find('.tprice').val(price);
		}
    });
	
	$( ".quantity",newRow).on( "blur", function() {
	    if($(this).val()!='' && $(this).parents('tr').find('.product_price').val()!=''){
			var price=($(this).parents('tr').find('.product_price').val())*$(this).val();
			
			$(this).parents('tr').find('.tprice').val(price);
		}
    });
	
	
	$( ".restocking_fees",newRow).on( "blur", function() {
	    if($(this).val()!='' && $(this).parents('tr').find('.tprice').val()!=''){
			var price=parseInt(($(this).parents('tr').find('.tprice').val()))+parseInt($(this).val());
			
			$(this).parents('tr').find('.extendedprice').val(price);
		}
    });
	
	$( ".product_ndc",newRow).on( "blur", function() {
  		obj=$(this);		
		obj.parents('tr').find('.prodid').val(obj.val());
			
	});

    // prevent the default click
    return false;
});

// remove's default rows
$('.removeDefault').click(function() {
    $(this).closest('tr').remove();
    return false;
});


function checklimit(obj,qauntity){
	console.log(parseInt(obj.val())+">="+parseInt(qauntity));
	if(parseInt(obj.val())>parseInt(qauntity)){
		alert("Return Quantity must be less than or equal to Purchase Quantity");
		obj.val('');
	}
}
$(document).ready(function(){
	$( ".product_ndc").on( "blur", function() {
  		obj=$(this);		
		obj.parents('tr').find('.prodid').val(obj.val());
			
	});
	/*$("#orderProductNdc").blur(function(e) {
		var products_jshon=<?php echo $products_jshon;?>;
        $.each(products_jshon, function(i, v) {
			if (v.label == $("#orderProductNdc").val()) {
				//alert(v.value);
				$( "#orderProdid" ).val(v.value);
				return;
			}
		});
    });*/
	
	$( ".product_price" ).on( "blur", function() {
		//console.log($(this).parents('tr').html());
        if($(this).val()!='' && $(this).parents('tr').find('.quantity').val()!=''){
			var price=($(this).parents('tr').find('.quantity').val())*$(this).val();
			
			$(this).parents('tr').find('.tprice').val(price);
		}
    });
	/*$('.product_price').blur(function(e) {
		//console.log($(this).parents('tr').html());
        if($(this).val()!='' && $(this).parents('tr').find('.quantity').val()!=''){
			var price=($(this).parents('tr').find('.quantity').val())*$(this).val();
			
			$(this).parents('tr').find('.tprice').val(price);
		}
    });*/
	$( ".quantity" ).on( "blur", function() {
	//$('.quantity').blur(function(e) {
		//console.log($(this).parents('tr').html());
        if($(this).val()!='' && $(this).parents('tr').find('.product_price').val()!=''){
			var price=($(this).parents('tr').find('.product_price').val())*$(this).val();
			
			$(this).parents('tr').find('.tprice').val(price);
		}
    });
	
	
	$( ".restocking_fees" ).on( "blur", function() {
	//$('.restocking_fees').blur(function(e) {
		//console.log($(this).parents('tr').html());
        if($(this).val()!='' && $(this).parents('tr').find('.tprice').val()!=''){
			var price=parseInt(($(this).parents('tr').find('.tprice').val()))+parseInt($(this).val());
			
			$(this).parents('tr').find('.extendedprice').val(price);
		}
    });
	
	$("#ProductInfoAdminSalesReportPersonForm").validate();
	//$( "#ProductInfoSelfrmdate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
   // $( "#ProductInfoSeltodate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
	
	$( ".expdate1" ).datepicker({dateFormat: 'yy-mm-dd' ,minDate: 0 });
	
   

});
</script>