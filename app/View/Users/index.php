<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title></title>
<link rel="stylesheet" href="<?php echo base_url('u_assets/css/bootstrap.min.css');?>">
<link rel="stylesheet" href="<?php echo base_url('u_assets/css/font-awesome.min.css');?>">
<link rel="stylesheet" href="<?php echo base_url('u_assets/css/flexslider.css');?>">
<link rel="stylesheet" href="<?php echo base_url('u_assets/css/style.css');?>">
<link rel="stylesheet" href="<?php echo base_url('u_assets/css/responsive.css');?>">
<script src="<?php echo base_url('assets/js/jquery.js');?>"></script>
<script src="<?php echo base_url('u_assets/js/media.match.min.js');?>"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.js"></script>
<style type="text/css">
#dis {
	text-align: center;
	height: 25px;
	width: 250px;
	color: white;
}
#city li {
	list-style-type: none;
}

.carousel-caption{     
	top: 39% !important;
    font-size: 31px;
}
</style>
</head>

<body>
<div id="main-wrapper">
<header id="header" class="header-style-1"> 
  
  <!-- end .header-top-bar -->
  <?php include'header.php';?>
  <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          <form  action="<?php echo site_url('admin/mailreg/');?>" method="post" enctype="multipart/form-data">
            <div class="form-group">
              <label for="recipient-name" class="control-label">Email</label>
              <input type="text"  name="email" class="form-control" id="recipient-name" required>
            </div>
            <input    type="submit" class="btn btn-primary" value="submit">
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  
  <!-- end .header-nav-bar -->
  
  <div id='loadingmessage1' style='display:none;'> <img src='<?php echo  base_url('u_assets/img/ajaxloader.gif');?>' align="center" width="30" height="30" /> </div>
  <div class="header-search-bar">
    <div class="container">
      <form  action="<?php echo site_url('post_jobs/filter_job');?>"  method="post">
        <div class="basic-form col-sm-9" style="margin-top:12px;"> 
          
          <!-- <div class="hsb-input-1">
            <input type="text" class="form-control" name="loking_for" placeholder="I'm looking for a ...">
          </div>
          <div class="hsb-text-1">in</div> -->
          
          <div class="hsb-container">
            <?php $city=$this->region_model->city();
           ?>
            <div class="hsb-select" style=" width: 43%;">
              <select class="form-control" name="destination" required>
                <option value="0">Select City</option>
                <?php foreach($city as $val){?>
                <option value="<?php echo $val['id'];?>"><?php echo $val['city'];?> </option>
                <?php }?>
              </select>
            </div>
            <?php $industry=$this->post_job->filter_job();?>
            <div class="hsb-select" style=" width: 43%;">
              <select class="form-control" name="category" required>
                <option value="0">Select Category</option>
                <?php foreach($industry as $category){?>
                <option value="<?php echo $category['category_name'];?>"><?php echo $category['category_name'];?> </option>
                <?php }?>
              </select>
            </div>
            <div class="hsb-submit" style=" float: right;">
              <input type="submit" class="btn btn-default btn-block" id="" value="Search">
            </div>
          </div>
        </div>
        <div class="col-sm-3">
            <div class="header-banner-box register" style="background-image:none; height:60px !important; margin:0; border:0px !important;">
            <div class="counter-container" style=" top:0px;">
              <ul class="counter clearfix">
                <li class="zero">0</li>
                <li>3</li>
                <li>5</li>
                <li>1</li>
                <li>0</li>
                <li>9</li>
              </ul>
              <div><span>Jobs</span></div>
            </div>
          </div>
        
        
        
        </div>
        <div class="clearfix"></div>
      </form>
    </div>
  </div>
  <!-- end .header-search-bar -->
  
  <div id='loeader' style='display:none;'> <img src='<?php echo  base_url('u_assets/img/ajaxloader.gif');?>' align="center" width="30" height="30" /> </div>
  
  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img data-u="image" src="http://versatileventure.net/job/u_assets/img/banner-img1.jpg" width="100%" height="300px" />
      <div class="carousel-caption">
       Did you drag by either horizontal or vertical?
      </div>
    </div>
    <div class="item">
      <img data-u="image" src="http://versatileventure.net/job/u_assets/img/banner-img2.jpg" width="100%" height="300px" />
      <div class="carousel-caption">
       Did you drag by either horizontal or vertical?
      </div>
    </div>
    
    <div class="item">
      <img data-u="image" src="http://versatileventure.net/job/u_assets/img/banner-img3.jpg" width="100%" height="300px"  />
      <div class="carousel-caption">
        Do you notice navigator responses when drag?
      </div>
    </div>
    
    <div class="item">
      <img data-u="image" src="http://versatileventure.net/job/u_assets/img/banner-img4.jpg" width="100%" height="300px" />
      <div class="carousel-caption">
        Do you notice arrow responses when click?
      </div>
    </div>
  
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
  
  
  
  
  
  
  
  
  
  
  <!-- end .header-banner --> 
</header>
<!-- end #header --> 
<!-- end .header-search-bar -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <center>
          <b>Terms and Conditions</b>
        </center>
      </div>
      <div class="modal-body">
        <form  action="" method="post" enctype="multipart/form-data">
          <div class="form-group"> <b> In using this website you are deemed to have read and agreed to the following terms and conditions:</b><br>
            <font size="2"> The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and any or all Agreements: "Client", “You” and “Your” refers to you, the person accessing this website and accepting the Company’s terms and conditions. "The Company", “Ourselves”, “We” and "Us", refers to our Company. “Party”, “Parties”, or “Us”, refers to both the Client and ourselves, or either the Client or ourselves.</font> </div>
          <div class="form-group">
            <input type="checkbox" value="1" id="checkbox" name="accepted">
            &nbsp;&nbsp; I agree with your terms and conditions. </div>
          <button type="button" id="register_button" class="btn btn-primary">Submit</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php $all_job=$this->post_job->all_job_list();

//print_r($all_job);?>
<div id="page-content">
<div class="container">
<div class="row">
<div class="col-sm-8 page-content">
  <div class="title-lines">
    <h3 class="mt0">Recent Jobs</h3>
  </div>
  <?php foreach($all_job as $job)
				  {?>
  <div class="jobs-item with-thumb" style="background-color:#ccc;">
    <div class="date"><span>
      <?php 
						                            $old_date=$job['date'];
													$middle=strtotime($old_date);
													echo $new_date=date('d/m/y',$middle);
													?>
      </span></div>
    <h6 class="title"><a href="#"><?php echo $job['job_title'];?></a></h6>
    <span class="meta">Address :&nbsp;&nbsp;&nbsp;<?php echo $job['address'];?></span>
    <p class="description" style="margin-left:90px;">Desired Skill :<?php echo $job['job_desc'];?> <a href="#" class="read-more">Read More</a></p>
    <div class="content">
      <h5> Requirements</h5>
      <ul class="additional-requirements clearfix">
        <li>Position :&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $job['position'];?></li>
        <li>Experience :&nbsp;&nbsp;&nbsp;<?php echo $job['year_exprience'];?></li>
        <li>Qualification : &nbsp;&nbsp;<?php echo $job['education'];?></li>
        <li>Address: &nbsp;&nbsp;&nbsp;<?php echo $job['address'];?></li>
      </ul>
      <hr>
      <div class="clearfix"> <a href="<?php echo site_url('jobs/job_apply/'.$job['uid']);?>" class="btn btn-default pull-left">Apply for this Job</a> </div>
    </div>
  </div>
  <?php }?>
  <div class="clearfix"> <a href="<?php echo base_url('index.php/jobs/job_list');?>" class="fa fa-angle-right">See all jobs</a> </div>
  
  <!--	
					
					<div class="title-lines">
						<h3>Our Prices</h3>
					</div>

				<div class="pricing-tables tables-3">
						<div class="pricing-tables-column">
							<div class="white-container">
								<h5 class="title">Bronze</h5>

								

								<ul class="features">
									<li>1 Job Ad</li>
									<li>30 Days</li>
									<li>Standard Positioning</li>
									<li>1 Lorem Ipsum</li>
								</ul>

								<a href="#" class="btn btn-default">Choose</a>
							</div>
						</div>

						<div class="pricing-tables-column">
							<div class="white-container">
								<h5 class="title">Silver</h5>

								

								<ul class="features">
									<li>5 Job Ad</li>
									<li>45 Days</li>
									<li>Extended Positioning</li>
									<li>5 Lorem Ipsum</li>
								</ul>

								<a href="#" class="btn btn-default">Choose</a>
							</div>
						</div>

						<div class="pricing-tables-column">
							<div class="white-container">
								<h5 class="title">Gold</h5>

								

								<ul class="features">
									<li>10 Job Ad</li>
									<li>60 Days</li>
									<li>Extra Positioning</li>
									<li>5 Lorem Ipsum</li>
								</ul>

								<a href="#" class="btn btn-default">Choose</a>
							</div>
						</div>
					</div> end .pricing-tables --> 
  
</div>
<!-- end .page-content -->

<?php $img=$this->region_model->all_banner();
			      $notify=$this->notification->get_notify();
			 ?>
<div class="col-sm-4 page-sidebar">
  <aside>
    <div class="white-container"  style="margin-bottom:-20px;height:25px;">
      <marquee direction="left">
      <?php print_r($notify[0]['home_notify']);?>
      </marquee>
    </div>
    <div class="white-container">
      <div class="widget sidebar-widget">
        <?php include'banner.php';?>
      </div>
    </div>
  </aside>
</div>
<!-- end .page-sidebar -->
<div class="col-sm-4 page-sidebar"  style="margin-top:-75px;">
<aside>
<div class="white-container">
<?php include'testimonial.php';?>
</div>
</aside>
</div>
<!-- end .page-sidebar -->

</div>
</div>
<!-- end .container -->

<?php $benificry=$this->notification->benificry_list();?>
<div class="success-stories-section">
  <div class="container">
    <h5 class="mt10">Beneficiaries</h5>
    <div>
      <div class="flexslider">
        <ul class="slides">
          <?php foreach($benificry as $val){?>
          <li><a href='<?php echo $val['url'];?>'><img class="thumb" src="<?php echo base_url('upload/'.$val['image']);?>" width="300" height="100" alt=""></a>
            <p  style="margin-left:25px;"><span>
              <?php  echo $val['title'];?>
              </span></p>
          </li>
          <?php }?>
        </ul>
      </div>
    </div>
  </div>
</div>
</div>
<!-- end #page-content -->

<script>
$(document).ready(function(){
    $("#name").blur(function(){
       var name=$("#name").val();
       $.ajax({
           type:"post",
           url:"<?php echo site_url('user/user_exist');?>",
            data:{name:name},
            success:function(data)
            { 
              if(data ==1)
              {  
                  
                  $("#check").html("This name already Exist");
              }    
            }       
       });
    });
    
    $("#submit").click(function(){
      $.ajax({
          type:"post",
          url:"<?php echo   site_url('user/register')?>",
          data:$("#register").serialize(),
             success:function(data)
             {
                  if(data)
                  {
					  alert(data);
				  }
                              
             }
      });
    });
	
	});
</script>
<script>
   
   $(document).ready(function(){
   $("#login_user").click(function()
   {    
	
      var name=$("#user").val();
	  var pwd=$("#pwd").val();
 
  if(name == "")
      {
		 $('#dis').slideDown().html("<span>Please type Username</span>");
	  }		  
	if(pwd =="")
	  {
		  $('#dis').slideDown().html("<span>Please type password</span>");
	  }
	if(name != "" && pwd !="")  
	{     $('#loadingmessage1').show();
		 $.ajax({
		  type:"post",
		  url:"<?php echo site_url('user/login');?>",
	      data:$('#log').serialize(),
		                 
          beforeSend: function(){
           $('#image').show();
          },
		  success:function(data)
		  {   
             
			  if(data == 2)
		      {  
		         window.location.href='<?php echo site_url();?>/user/emp_profile';
		      }
			  
		  if(data == 1)
		   {
			   window.location.href='<?php echo site_url();?>/user/seeker_profile';
		   }
		  if(data == 0)
		   {
			  alert('Invalid login Creditials! ');
		   }
		   $('#loadingmessage1').hide();
		  
	     }
	  });
	}
   });
});
</script>
<script>
$(document).ready(function(){
		//auto search city
		var div_val_arr = [];
			$("#id_div_values li").each(function() {
				div_val_arr.push($(this).text());
			});
			
	$( "#destination" ).autocomplete({
			source: div_val_arr,
			select: function( event, ui ) {
					//$('#abcd1234').css('display','block');
					
				  }
				});
			$('#destination').focusout(function(){myFunction();
			});
});			
			
</script>
<script>
$(document).ready(function(){
	$("#filter").click(function()
	{ 
	
	  $('#loeader').show();
	  $.ajax({
		        type:"post",
				url:"<?php echo site_url('post_jobs/filter_job');?>",
				data:$("#filter_job").serialize(),
				beforeSend:function()
				{
					 $('#image').show();
				},
				success:function(data)
				{
					$('#image').hide();
				}
		    
	        });  	
	});
	
   });

</script>
<?php include 'bottom.php';?>

                            