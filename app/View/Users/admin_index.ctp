<?php $this->set('title_for_layout', __('Admin Users',true)); ?>
<?php /*$this->Html->addCrumb($this->Html->tag('li',__('Admin Users',true),array('class'=>'active')),false,false);*/ ?>

         <!-- <div class="col-xs-12 text-center"><strong>Instock -</strong> $<?php echo round($instock,2)?></div>
          <div class="col-xs-12 text-center"><strong>Quantity Instock -</strong> <?php echo ($instockquantity)?></div>
          <div class="col-xs-12 text-center"><strong>Sell till now -</strong> $<?php echo ($totalsell)?></div>
          <div class="col-xs-12 text-center"><strong>Sell till now Quantity -</strong> <?php echo ($sellquentity)?></div>-->
          
          

<h3>Product Instock</h3>
<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                    
                        <th>Product Type</th>
                        <th>Quantity</th>
                        <th>Purchase Price</th> 
                       
                       
                    </tr>
                </thead>
                
                <tbody>
                		
            <?php if(!empty($instock)){				
			
				$i=0;
				$product_qt=0;
				$product_price=0;
				foreach ($instock as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}	
					$product_qt=$product_qt+$user[0]['quantity'];
					$product_price=$product_price+$user[0]['instock'];
				?>
                <tr <?php echo $class;?> > 
                        
               
                <td><?php echo h($user['Product']['producttype'])?></td>
                <td><?php echo h($user[0]['quantity'])?></td>               
                <td><?php echo h(round($user[0]['instock'],2))?></td>                
                </tr>
                <?php endforeach; ?>            
				
          <?php }else{
			  ?>
          <tr>      
			<td colspan="6" class="striped-info">No record found.</td>
		</tr>
			<?php } ?>			                
                </tbody>
                <tfoot>
          <tr>
          
            <th style="text-align:right">Total:</th>
            <th><?php echo h($product_qt);?></th>
            <th><?php echo h(round($product_price,2));?></th>
          </tr>
        </tfoot>
            </table>
             <div class="clear"><br /><br /></div>
             <h1>Users</h1>
             <hr />
          <div class="row placeholders">
            <div class="col-xs-6 col-sm-3 placeholder">
              <i class="fa fa-users fa-4x"></i>
              <h4>Admin User </h4>
              <span class="text-muted"><?php echo $admincount ?> Accounts</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <i class="fa fa-user fa-4x"></i>
              <h4>Sales User</h4>
              <span class="text-muted"><?php echo $salescount ?> Accounts</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <i class="fa fa-users fa-4x"></i>
              <h4>Data Entry Users</h4>
              <span class="text-muted"><?php echo $dataentrycount ?> Accounts</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <i class="fa fa-user fa-4x"></i>
              <h4>Client User</h4>
              <span class="text-muted"><?php echo $clientcount ?> Accounts</span>
            </div>
          </div>
          
         
<div class="row-fluid">
<!-- block -->
<h2 class="sub-header">Latest Orders</h2>
<div class="block">
    
    <div class="block-content collapse in">
        <div class="span12">
        
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                        <th><?php echo __('Client Name & Details');?></th>
                        <th><?php echo __('Order Id & Dates');?></th>
                        <th><?php echo __('Order Details');?></th> 
                        <th><?php echo __('Sales Info');?></th> 
                       
                    </tr>
                </thead>
                
                <tbody>
                
				<?php 
				$i=0;
				foreach ($users as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				$product_qauantity=count($user['OrderdetailMany']);
				
				$priice=0;
				foreach($user['OrderdetailMany'] as $orderdeatils){
					$priice=$priice+($orderdeatils['price']*$orderdeatils['quantity']);
				}
				$product_price=$product_price+$priice
				?>
                <tr <?php echo $class;?> > 
                        
               
                <td>Name:<?php echo h($user['User']['fname']); ?><br />
                	Address:<?php echo h($user['User']['address']); ?><br />
                    Phone:<?php echo h($user['User']['phone']); ?><br />
                    Email:<?php echo h($user['User']['email']); ?><br />
                    &nbsp;</td>
                <td><?php echo h($user['Order']['tmporderid']); ?><br />
                <?php echo h(date('Y-m-d',strtotime($user['Order']['createddt']))); ?>&nbsp;</td>
               
                <td><a class=" external" data-toggle="modal" href="/admin/orders/order_ifno/<?php echo $user['Order']['id']; ?>" data-target="#myModal"><?php echo h($product_qauantity); ?> Products</a><br />Total Amount:<?php echo h($priice); ?>&nbsp;</td>                
               <td>Name:<?php echo h($salesman[$user['User']['sid']]['fname']); ?><br />
                	Address:<?php echo h($salesman[$user['User']['sid']]['address']); ?><br />
                    Phone:<?php echo h($salesman[$user['User']['sid']]['phone']); ?><br />
                    Email:<?php echo h($salesman[$user['User']['sid']]['email']); ?><br />
                    &nbsp;</td>
                </tr>
                <?php endforeach; ?>                
                </tbody>
            </table>
	<div class="pagination pagination-right pagination-mini">
	  <ul>
		<?php echo $this->Paginator->prev(''); ?>
		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
		<?php echo $this->Paginator->next(''); ?>
	  </ul>
	</div>
        </div>
    </div>
</div>
<!-- /block -->
</div>





<!--POPUP-->
 <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
      </div>
      </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->   
<!--POPUP-->

<script type="text/javascript">
    var published = {
        toggle : function(id, url){
            obj = $('#'+id).parent();
            $.ajax({
                url: url,
                type: "POST",
				beforeSend: function(){
				$('.modal-body').html('<p align="center"> <img src="<?php echo $this->Html->url('/img/generated-image.gif');?>"></p>');
				},
                success: function(response){
                    obj.html(response);
					$('.tooltip-top').tooltip({ placement: 'top' });	
					$('.popover-top').popover({placement: 'top', trigger: 'hover'});
                }
            });
        }
    };
	
	var view = {
        toggle : function(url){	
            $.ajax({
                url: url,
                type: "POST",
				beforeSend: function(){
				$('.modal-body').html('<p align="center"> <img src="<?php echo $this->Html->url('/img/generated-image.gif');?>"></p>');
				},
                success: function(response){					
                $('.modal-body').html(response);
				$('.modal-header h3').html("<?php echo __("View"); ?>");
                }
            });
        }
    };
	
	var uprofiles = {
        toggle : function(url){	
            $.ajax({
                url: url,
                type: "POST",
				beforeSend: function(){
				$('.modal-body').html('<p align="center"> <img src="<?php echo $this->Html->url('/img/generated-image.gif');?>"></p>');
				},
                success: function(response){		
                    $('.modal-body').html(response);
					$('.modal-header h3').html("<?php echo __("Total Profiles"); ?>");
                }
            });
        }
    };
	
	var share = {
        toggle : function(url){	
            $.ajax({
                url: url,
                type: "POST",
				beforeSend: function(){
				$('.modal-body').html('<p align="center"> <img src="<?php echo $this->Html->url('/img/generated-image.gif');?>"></p>');
				},
                success: function(response){		
                    $('.modal-body').html(response);
					$('.modal-header h3').html("<?php echo __("Share Profiles"); ?>");
                }
            });
        }
    };
	
</script>