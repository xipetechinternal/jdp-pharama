<?php $this->set('title_for_layout', __('Terms & Conditions',true)); ?>
<?php $this->Html->addCrumb($this->Html->tag('li',__('Terms & Conditions',true),array('class'=>'active')),false,false); ?>
<div class="row-fluid">
<div class="block">
<div class="navbar navbar-inner block-header">
<div class="muted pull-left"><?php echo __('Terms & Conditions'); ?></div>
</div>
<div class="block-content collapse in">
<div class="span12">
<?php echo $this->Form->create('Page',array('class' => 'form-horizontal'));?>
	<fieldset>
		<legend><?php echo __('Edit Terms & Conditions'); ?></legend>
        
    <?php 
	echo $this->Form->input('id');
	echo $this->Form->input('title', array('div'=>'control-group',
				'before'=>'<label class="control-label">'.__('Title').' : </label><div class="controls">',
				'after'=>$this->Form->error('title', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
				'error' => array('attributes' => array('style' => 'display:none')),
				'label'=>false, 'class'=>'input-xlarge focused','type'=>'text'));
	echo $this->Form->input('content', array('div'=>'control-group',
				'before'=>'<label class="control-label">'.__('Content').' : </label><div class="controls">',
				'after'=>$this->Form->error('content', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
				'error' => array('attributes' => array('style' => 'display:none')),
				'label'=>false, 'class'=>'input-xxlarge focused','id'=>'tinymce_full'));
	?>
       <div class="form-actions">
            <?php echo $this->Form->submit(__('Submit'), array('class'=>'btn btn-primary', 'div'=>false));?>  
            <?php echo $this->Form->reset(__('Cancel'), array('class'=>'btn', 'div'=>false));?> 
        </div>
	</fieldset>
<?php echo $this->Form->end();?>
</div>
</div>
</div>
</div>