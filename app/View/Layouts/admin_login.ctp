<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php echo $title_for_layout; ?></title>	
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
		<?php echo $this->Html->charset(); ?>
        
        <!-- Bootstrap -->
    <?php
	echo $this->Html->css('admin/bootstrap.min');
	echo $this->Html->css('admin/bootstrap-theme.min');	
	echo $this->Html->css('admin/bootstrap_calendar');
	echo $this->Html->css('admin/main');
	?>
     <?php echo $this->Html->script(array('bootstrap_calendar.min'));?>
     <?php echo $this->Html->script(array('vendor/modernizr-2.6.2-respond-1.1.0.min'));?>
      <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-66023494-1', 'auto');
  ga('send', 'pageview');

</script>

      
    </head>
    <body>
       <div class="jumbotron">
       <div class="container">
       <div class="row">
         
<div class="col-lg-3 col-sm-6" >
        <a href="index.php"><img src="<?php echo $this->Html->url('/img/logo.png');?>" alt="" class="img-responsive"></a>

        </div>
         <div class="col-lg-6 col-sm-6 toppad30">
        <h2>Admin Login Panel</h2>

        </div>
        </div>

       </div>
       </div>     
    </div>   
    <div class="gap50"></div>
    <div class="container">
       <div class="row">
       
       <div class="col-lg-4" ></div>

 <div class="col-lg-4 text-center">

     

      <?php echo $this->fetch('content'); ?>

    


</div>


      
      <div class="col-lg-4" ></div>
      
       
       </div>
       </div>  
    <div class="gap50"></div>
    
    <div class="footer">
       <div class="container">
       <div class="row whitetxt">Copyright @ 2014. All Rights Reserved.</div>
       </div>     
    </div>   
    
    
        
    </body>
</html>
