<!DOCTYPE html>
<html class="no-js">
<!--<![endif]-->
<head>
<meta name="description" content="">
<meta name="viewport" content="width=device-width">
<?php echo $this->Html->charset(); ?>
<title><?php echo $title_for_layout; ?></title>
<?php echo $this->Html->css(array('bootstrap.min','main'));?> 
<?php echo $this->Html->script(array('/vendors/modernizr-2.6.2-respond-1.1.0.min'));?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script> 
<script>window.jQuery || document.write('<script src="<?php echo $this->Html->url('js/jquery-1.8.3.min.js');?>"><\/script>')</script> 
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
</head>
<body>
<div class="jumbotron">
  <div class="container">
    <div id="wrapper">
      <div class="row">
        <div class="col-lg-4"> <a href="/"><img src="<?php echo $this->Html->url('/img/logo2.png');?>" alt="" class="img-responsive"></a> </div>
        
         <div class="col-lg-6 col-sm-6 toppad30">
        <h2>Dataentry  Panel</h2>

        </div>
        
      </div>
    </div>
  </div>
</div>
<div class="container">
  <div id="content-area">
  
    <div class="clearfix"></div>
    
     <?php echo $this->fetch('content'); ?>		
  </div>
  <?php echo $this->element('footer');?>
  
  <footer class="text-center">
    <p>Terms to Use |  Privicay Policy</p>
    <small>Copyright &copy;<?php echo date("Y")?> JD Pharmaceutical Inc. All rights reserverd</small> </footer>
</div>
<!-- /container --> 
 
  <?php
	
	echo $this->Html->css('admin/datatables');	
	echo $this->Html->css('jquery.ui.datepicker.min');	
	echo $this->Html->css('jquery.ui.core.min');
	echo $this->Html->css('jquery.ui.theme.min');
	?>
<?php echo $this->Html->script(array('main','/css/bootstrap/js/bootstrap.min','plugins'));?>
<?php echo $this->Html->script(array('jquery.validation.functions'));?>
<?php echo $this->Html->script(array('jquery.validate.min'));?>
<?php echo $this->Html->script(array('jquery-ui-1.9.2.datepicker.custom.min'));?>

</body>
</html>
