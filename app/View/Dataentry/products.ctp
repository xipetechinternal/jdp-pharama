
      <div class="row">   

        <div class="col-md-offset-8 col-lg-4">
<div class="panel panel-default" style="margin-top:-100px">

  <div class="panel-body form-bg text-center ">
<p>Welcome <?php echo ucfirst($this->Session->read('Auth.dataentry.fname')); ?></p>

   <?php echo $this->Html->link('Sign out', array('controller'=>'Dataentry', 'action'=>'logout'),array('class' => 'btn btn-warning btn-block'));?>
  </div>
  
</div>
        </div>

        </div>
 
      <div class="clearfix"></div>
      <div class="row">   

      <div class="col-lg-12">
        
<h4><?php echo $this->Html->link('Home', array('controller'=>'Dataentry', 'action'=>'index'),array('class' => ''));?> :: Add Manage </h4>
<div class="col-lg-12  ">
 <?php echo $this->Form->create('ProductInfo',array('url'=>array('action'=>'products','controller'=>'Dataentry'),'type'=>'get', 'class'=>'form-horizontal login-from')); ?>
 <div class="clearfix"></div>
  <div class="gap20">&nbsp;<br /><br /><br /></div>
   <div class="gap20"></div>
  <div class="form-group">
   <div class="gap20"></div>
  <div class="col-lg-2"><label for="exampleInputEmail1">Search By:</label></div>
  <div class="col-lg-8"> <div class="form-group">
  <div class="col-lg-4">   <?php echo $this->Form->input('ndcproduct',array('placeholder' => 'Item no/Product NDC/Product Info',
        'label' => false,
		'value'=>$search,
		'required'=>'required',
        'class'=>'form-control'));?> 
   
  </div>
  <div class="col-lg-4"> <?php echo $this->Form->button('<i class="fa fa-search"></i> Search', array(
    'type' => 'submit',
    'class' => 'btn btn-default',
    'escape' => false
));?></div>

  <div class="clearfix"></div>
  
  <small>Search By <cite title="Source Title">Item no/Product NDC/Product Details</cite></small>
            </div></div>

  </div>
  <?php echo $this->Form->end();?>
  <div class="clearfix"></div>
  <div class="gap20"></div>
</div>
 <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Products</h3>
      </div>
 <?php 
	   echo $this->Session->flash(); 
	  
	   //echo $this->Session->flash('auth'); ?>
<div class="panel-body">
 <?php echo $this->Form->create('Products', array(
    'url' => array('controller' => 'Dataentry', 'action' => 'products')
));
?>
    
            <div class="gap20"></div>

            <div class="form-group">
              <?php echo $this->Form->input('itemnumber',array('placeholder' => 'Item Number',
        'label' => 'Item Number',
		'type'=>'hidden',
		'default'=>0,
		'required'=>'required',
        'class'=>'form-control'));?> 
            </div>
            <div class="gap20"></div>
            <div class="form-group">
            <?php echo $this->Form->input('product_ndc',array('placeholder' => 'Product NDC',
        'label' => 'Product NDC',
		'required'=>'required',
        'class'=>'form-control'));?> 
            </div>

                   
            <div class="gap20"></div>

            <div class="form-group">
             <?php echo $this->Form->input('productname',array('placeholder' => 'Product Name',
        'label' => 'Product Name',
		'required'=>'required',	
        'class'=>'form-control'));?> 
            </div>

             <div class="form-group">
                <?php echo $this->Form->input('producttype',array('onchange'=>'calculate_productprice()','options' => array(''=>'Product Type','Brand'=>'Brand','Generic'=>'Generic','OTC'=>'OTC','Refrigirated'=>'Refrigirated'),
        'label' => 'Product Type',
		'required'=>'required',
        'class'=>'form-control'));?>
            </div>
                  
            <div class="gap20"></div>

            
           <div class="form-group">
              <?php echo $this->Form->input('PurchasePrice',array('placeholder' => 'Purchase Price',
        'label' => 'Purchase Price',
		'onblur'=>'calculate_productprice()',		
        'class'=>'form-control'));?> 
            </div>

                        
            <div class="gap20"></div>

            <div class="form-group">
              <?php echo $this->Form->input('qtyinstock',array('placeholder' => 'Threshold',
        'label' => 'Threshold',
        'class'=>'form-control'));?> 

		<div class="gap20"></div>

            <div class="form-group">
              <?php echo $this->Form->input('manufacture',array('placeholder' => 'Manufacture',
        'label' => 'Manufacture',
		'required'=>'required',
        'class'=>'form-control'));?> 

		<div class="gap20"></div>
		 <div class="form-group">
              <?php echo $this->Form->input('awp',array('placeholder' => 'AWP',
        'label' => 'AWP',
        'class'=>'form-control'));?> 

            
            <div class="gap20"></div>

            <div class="form-group">
              <?php echo $this->Form->input('WAC',array('placeholder' => 'WAC',
        'label' => 'WAC',
        'class'=>'form-control'));?> 

            

		<div class="form-group">
              <?php echo $this->Form->input('productprice',array('placeholder' => 'Product Price',
        'label' => 'Product Price',
		'required'=>'required',
		'readonly' => 'readonly',
		
        'class'=>'form-control'));?> 
            </div>
			
            <div class="gap20"></div>


  <?php echo $this->Form->input('id',array('type'=>'hidden'));?>
  <?php echo $this->Form->input('user_id',array('type'=>'hidden','value'=>$user_id));?>
<?php echo $this->Form->submit(__('Submit'), array('class'=>'btn btn-success', 'div'=>false));?>&nbsp;

      </div>
  </div>
  </div>
<?php echo $this->Form->end();?>

      </div>

     <div class="col-lg-12 text-center">

      <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Product List</h3>
      </div>
      <div class="panel-body">
       <div class="table-responsive">
             
<input type="text" name="foucs" style="width:0px" id="foucs" />
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                        
                        <th><?php echo __('Item #');?></th>
                        <th><?php echo __('Product NDC');?></th> 
                        <th><?php echo __('Product Info');?></th> 
                        <th><?php echo __('Threshold');?></th>
                        <th><?php echo __('Manufacture');?></th>
                        <th><?php echo __('WAC');?></th>
						 <th><?php echo __('IN STOCK');?></th>
                        <th><?php echo __('Actions');?></th>
                    </tr>
                </thead>
                
                <tbody>
                
				<?php 
				$i=0;
				foreach ($users as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				$product_instock=0;
				foreach($user['ProductInfo'] as $productsinfo){
					$product_instock=$product_instock+$productsinfo['availability'];
				}
				?>
                <tr <?php echo $class;?> > 
                         
                <td><?php echo h($user['Products']['itemnumber']); ?>&nbsp;</td>
                <td><?php echo h($user['Products']['product_ndc']); ?>&nbsp;</td>
                <td><?php echo h('Product Name: '.$user['Products']['productname'])?><br>
                	Category :<?php echo h($user['Products']['producttype']); ?><br>
                    Price :<?php echo h($user['Products']['productprice']); ?>&nbsp;</td>
                <td><?php echo h($user['Products']['qtyinstock']); ?>&nbsp;</td>
                <td><?php echo h($user['Products']['manufacture']); ?>&nbsp;</td> 
                <td><?php echo h($user['Products']['WAC']); ?>&nbsp;</td>     
                    <td><?php echo $this->Html->link($product_instock,array('action' => 'manage_products?ndcproduct='.$user['Products']['product_ndc']),array("class"=>"btn btn-info btn-mini tooltip-top","data-original-title"=>"Edit",'escape'=>false)); ?></td>                     
                <td class="center"> <?php echo $this->Html->link($this->Html->tag('i','Edit',array('class'=>'icon-pencil icon-white')),array('action' => 'products', $user['Products']['id']),array("class"=>"btn btn-primary btn-mini tooltip-top","data-original-title"=>"Edit",'escape'=>false)); ?>
                
              <?php
                 echo $this->Form->postLink($this->Html->tag('i','Deactivate',array('class'=>'icon-remove icon-white')), array('action' => 'products_delete', $user['Products']['id']), array("class"=>"btn btn-danger btn-mini tooltip-top","data-original-title"=>"Delete",'escape'=>false), __('Are you sure you want to Deactivate # %s?', $user['Products']['productname']));
			  ?>
               
 				</td>
                </tr>
                <?php endforeach; ?>                
                </tbody>
            </table>
            <div class="pagination pagination-right pagination-mini">
	  <ul>
		<?php echo $this->Paginator->prev(''); ?>
		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
		<?php echo $this->Paginator->next(''); ?>
	  </ul>
	</div>
       </div>
      </div>
    </div>


</div>

    </div> 
<style>
#content-area{
	background-image:none;
}
</style>
<script>

$(document).ready(function(){
	<?php if($search!=''){?>
	$("#foucs").focus();
	$("#foucs").hide();
	<?php }?>
	$("#foucs").hide();
	$("#ProductsProductsForm").validate({

		rules: {
			"data[Products][productprice]":{
				required: true,
				number:true,

				}
		},
		messages: {
			"data[Products][productprice]":"Please enter a valid number",
			
		},
		errorElement:"span"

		});
});
function calculate_productprice(){
	 //console.log($('#ProductsProducttype').val());	 
	 
	 var product_price=0;
	 //Foe Brand
	 if($('#ProductsProducttype').val()=='Brand'){
		 if(($('#ProductsPurchasePrice').val())>0 && ($('#ProductsPurchasePrice').val())<1)
		 product_price=.5;

		 if(($('#ProductsPurchasePrice').val())>=1 && ($('#ProductsPurchasePrice').val())<=5)
		 product_price=1;
		 if(($('#ProductsPurchasePrice').val())>=6 && ($('#ProductsPurchasePrice').val())<=20)
		 product_price=1.45;
		if(($('#ProductsPurchasePrice').val())>=21 && ($('#ProductsPurchasePrice').val())<=50)
		 product_price=2;
		if(($('#ProductsPurchasePrice').val())>=51 && ($('#ProductsPurchasePrice').val())<=100)
		 product_price=3;
		if(($('#ProductsPurchasePrice').val())>=101 && ($('#ProductsPurchasePrice').val())<=200)
		 product_price=4;
		if(($('#ProductsPurchasePrice').val())>=201 && ($('#ProductsPurchasePrice').val())<=400)
		 product_price=5;
		if(($('#ProductsPurchasePrice').val())>=401 && ($('#ProductsPurchasePrice').val())<=600)
		 product_price=6;
		if(($('#ProductsPurchasePrice').val())>=601)
		 product_price=7;
	 }
	 //Foe Generic
	 if($('#ProductsProducttype').val()=='Generic'){
		  if(($('#ProductsPurchasePrice').val())>0 && ($('#ProductsPurchasePrice').val())<1)
		 product_price=.5;

		 if(($('#ProductsPurchasePrice').val())>=1 && ($('#ProductsPurchasePrice').val())<=5)
		 product_price=1;
		 if(($('#ProductsPurchasePrice').val())>=6 && ($('#ProductsPurchasePrice').val())<=20)
		 product_price=1.45;
		if(($('#ProductsPurchasePrice').val())>=21 && ($('#ProductsPurchasePrice').val())<=50)
		 product_price=2;
		if(($('#ProductsPurchasePrice').val())>=51 && ($('#ProductsPurchasePrice').val())<=100)
		 product_price=3;
		if(($('#ProductsPurchasePrice').val())>=101 && ($('#ProductsPurchasePrice').val())<=200)
		 product_price=5;
		if(($('#ProductsPurchasePrice').val())>=201 && ($('#ProductsPurchasePrice').val())<=400)
		 product_price=7;
		if(($('#ProductsPurchasePrice').val())>=401 && ($('#ProductsPurchasePrice').val())<=600)
		 product_price=9;
		if(($('#ProductsPurchasePrice').val())>=601)
		 product_price=11;
	 }

	 //Foe Refrigirated
	 if($('#ProductsProducttype').val()=='Refrigirated'){
		  if(($('#ProductsPurchasePrice').val())>0 && ($('#ProductsPurchasePrice').val())<1)
		 product_price=.5;

		 if(($('#ProductsPurchasePrice').val())>=1 && ($('#ProductsPurchasePrice').val())<=5)
		 product_price=1;
		 if(($('#ProductsPurchasePrice').val())>=6 && ($('#ProductsPurchasePrice').val())<=20)
		 product_price=1.45;
		if(($('#ProductsPurchasePrice').val())>=21 && ($('#ProductsPurchasePrice').val())<=50)
		 product_price=2;
		if(($('#ProductsPurchasePrice').val())>=51 && ($('#ProductsPurchasePrice').val())<=100)
		 product_price=3;
		if(($('#ProductsPurchasePrice').val())>=101 && ($('#ProductsPurchasePrice').val())<=200)
		 product_price=4;
		if(($('#ProductsPurchasePrice').val())>=201 && ($('#ProductsPurchasePrice').val())<=400)
		 product_price=5;
		if(($('#ProductsPurchasePrice').val())>=401 && ($('#ProductsPurchasePrice').val())<=600)
		 product_price=6;
		if(($('#ProductsPurchasePrice').val())>=601)
		 product_price=7;
	 }
	 
	 //Foe OTC
	 if($('#ProductsProducttype').val()=='OTC'){
		  if(($('#ProductsPurchasePrice').val())>0 && ($('#ProductsPurchasePrice').val())<1)
		 product_price=.5;

		 if(($('#ProductsPurchasePrice').val())>=1 && ($('#ProductsPurchasePrice').val())<=5)
		 product_price=1;
		 if(($('#ProductsPurchasePrice').val())>=6 && ($('#ProductsPurchasePrice').val())<=20)
		 product_price=1.45;
		if(($('#ProductsPurchasePrice').val())>=21 && ($('#ProductsPurchasePrice').val())<=50)
		 product_price=2;
		if(($('#ProductsPurchasePrice').val())>=51 && ($('#ProductsPurchasePrice').val())<=100)
		 product_price=3;
		if(($('#ProductsPurchasePrice').val())>=101 && ($('#ProductsPurchasePrice').val())<=200)
		 product_price=5;
		if(($('#ProductsPurchasePrice').val())>=201 && ($('#ProductsPurchasePrice').val())<=400)
		 product_price=7;
		if(($('#ProductsPurchasePrice').val())>=401 && ($('#ProductsPurchasePrice').val())<=600)
		 product_price=9;
		if(($('#ProductsPurchasePrice').val())>=601)
		 product_price=11;
	 }
	 $('#ProductsProductprice').val((parseFloat($('#ProductsPurchasePrice').val()))+parseFloat(product_price));
 }

</script>