
      <div class="row">   

        <div class="col-md-offset-8 col-lg-4">
<div class="panel panel-default" style="margin-top:-100px">

  <div class="panel-body form-bg text-center ">
<p>Welcome <?php echo ucfirst($this->Session->read('Auth.dataentry.fname')); ?></p>

   <?php echo $this->Html->link('Sign out', array('controller'=>'Dataentry', 'action'=>'logout'),array('class' => 'btn btn-warning btn-block'));?>
  </div>
  
</div>
        </div>

        </div>
 
      <div class="clearfix"></div>
      <div class="row">   

      <div class="col-lg-12">
        
<h4><?php echo $this->Html->link('Home', array('controller'=>'Dataentry', 'action'=>'index'),array('class' => ''));?> :: Deactive Products </h4>


      
 <?php 
	   echo $this->Session->flash(); 
	  
	   //echo $this->Session->flash('auth'); ?>


     <div class="col-lg-12 text-center">

      <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Product List</h3>
      </div>
      <div class="panel-body">
       <div class="table-responsive">
             
<input type="text" name="foucs" style="width:0px" id="foucs" />
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                        
                        <th><?php echo __('Item #');?></th>
                        <th><?php echo __('Product NDC');?></th> 
                        <th><?php echo __('Product Info');?></th> 
                        <th><?php echo __('Threshold');?></th>
                        <th><?php echo __('Manufacture');?></th>
                        <th><?php echo __('WAC');?></th>

                        <th><?php echo __('Actions');?></th>
                    </tr>
                </thead>
                
                <tbody>
                
				<?php 
				$i=0;
				foreach ($users as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				?>
                <tr <?php echo $class;?> > 
                      
                <td><?php echo h($user['Products']['itemnumber']); ?>&nbsp;</td>
                <td><?php echo h($user['Products']['product_ndc']); ?>&nbsp;</td>
                <td><?php echo h('Product Name: '.$user['Products']['productname'])?><br>
                	Category :<?php echo h($user['Products']['producttype']); ?><br>
                    Price :<?php echo h($user['Products']['productprice']); ?>&nbsp;</td>
                <td><?php echo h($user['Products']['qtyinstock']); ?>&nbsp;</td>
                <td><?php echo h($user['Products']['manufacture']); ?>&nbsp;</td> 
                <td><?php echo h($user['Products']['WAC']); ?>&nbsp;</td>     
                            
                <td class="center">                 
               <?php 
				    echo $this->Form->postLink($this->Html->tag('i','Activate',array('class'=>'icon-remove icon-white')), array('action' => 'products_activate', $user['Products']['id']), array("class"=>"btn btn-success btn-mini tooltip-top","data-original-title"=>"Delete",'escape'=>false), __('Are you sure you want to Activate # %s?', $user['Products']['productname']));
			  ?>
               
 				</td>
                </tr>
                <?php endforeach; ?>                
                </tbody>
            </table>
            <div class="pagination pagination-right pagination-mini">
	  <ul>
		<?php echo $this->Paginator->prev(''); ?>
		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
		<?php echo $this->Paginator->next(''); ?>
	  </ul>
	</div>
       </div>
      </div>
    </div>


</div>

   
<style>
#content-area{
	background-image:none;
}
</style>
<script>

$(document).ready(function(){
	<?php if($search!=''){?>
	$("#foucs").focus();
	$("#foucs").hide();
	<?php }?>
	$("#foucs").hide();
	$("#ProductsProductsForm").validate({

		rules: {
			"data[Products][productprice]":{
				required: true,
				number:true,

				}
		},
		messages: {
			"data[Products][productprice]":"Please enter a valid number",
			
		},
		errorElement:"span"

		});
});
</script>