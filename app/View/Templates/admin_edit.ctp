<?php $this->set('title_for_layout', __('Email Template',true)); ?>
<?php $this->Html->addCrumb(__('Email Templates',true), '/admin/templates/index',array('tag'=>'li'));  ?>
<?php $this->Html->addCrumb($this->Html->tag('li',__('Add',true),array('class'=>'active')),false,false); ?>
<div class="row-fluid">
<div class="block">
<div class="navbar navbar-inner block-header">
<div class="muted pull-left"><?php echo __('Email Templates'); ?></div>
</div>
<div class="block-content collapse in">
<div class="span12">
<?php echo $this->Form->create('Template',array('action' => 'admin_edit/'.$this->data['EmailTemplateDescription']['id'],'class' => 'form-horizontal'));?>
<fieldset>
<legend><?php echo __('Edit Email Template'); ?></legend>

<?php
echo $this->Form->input('EmailTemplateDescription.id',array('type'=>'hidden'));	
echo $this->Form->input('EmailTemplateDescription.subject', array('div'=>'control-group',
'before'=>' <label class="control-label">'.__('Subject').' : </label><div class="controls">', 
'after'=>$this->Form->error('subject', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));

echo $this->Form->input('EmailTemplateDescription.email_template_id', array('div'=>'control-group',  
'before'=>' <label class="control-label">'.__('Template').' : </label><div class="controls">', 
'after'=>'</div>','label'=>false, 'class'=>'input-xlarge focused','options'=>$templates,'empty'=>true));

echo $this->Form->input('EmailTemplateDescription.content', array('div'=>'control-group',
'before'=>' <label class="control-label">'.__('Email Body').' : </label><div class="controls">', 
'after'=>$this->Form->error('content', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused','id'=>'tinymce_full','type'=>'textarea'));

echo $this->Form->input('EmailTemplateDescription.status', array('div'=>'control-group', 
'before'=>' <label class="control-label">'.__('Status').' : </label><div class="controls">',
'after'=>'</div>','label'=>false, 'class'=>'uniform_on','type'=>'checkbox'));

?>                                        

<div class="form-actions">
<?php echo $this->Form->submit(__('Submit'), array('class'=>'btn btn-primary', 'div'=>false));?>&nbsp;
<?php echo $this->Form->reset(__('Cancel'), array('class'=>'btn', 'div'=>false));?>
</div>

</fieldset>
<?php echo $this->Form->end();?>
</div>
</div>
</div>
</div>

































