<div class="block-content collapse in">
<div class="span12">                            
<table class="table table-bordered">
	<tr>
		<th><?php echo __('Title'); ?></th>
		<td>
			<?php echo h($categories['Category']['name']); ?>
			&nbsp;
		</td>
	</tr>
	<tr>
		<th><?php echo __('Description'); ?></th>
		<td>
			<?php echo h($categories['Category']['description']); ?>
			&nbsp;
		</td>
	</tr>
	<tr>
		<th><?php echo __('Status'); ?></th>
		<td>
			<?php echo h($categories['Category']['status']? "Active" : "Inactive"); ?>
			&nbsp;
		</td>
	</tr>
	<tr>
		<th><?php echo __('Action'); ?></th>
		<td>
			<?php echo $this->Html->link('Edit', array('action'=>'edit', $categories['Category']['id']));?>
			&nbsp;
		</td>
	</tr>
	</table>
</div>
</div>
