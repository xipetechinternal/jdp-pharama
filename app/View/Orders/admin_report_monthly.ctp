<div class="col-lg-10  main">
         <h4><a href="dashboard.php">Home</a> :: Reports Monthly</h4>
         </div>
<div class="clearfix"></div>
 <div class="gap20"></div>
           <?php echo $this->Form->create('ProductInfo',array('url'=>array('action'=>'admin_report_monthly','controller'=>'Orders'),'type'=>'get', 'class'=>'form-horizontal login-from')); ?>

<div class="col-lg-10  ">
  <div class="form-group">
  <div class="col-lg-2"><label for="exampleInputEmail1">Select Date:</label></div>
  <div class="col-lg-8"> <div class="form-group">
  <div class="col-lg-4">   <?php echo $this->Form->input('selfrmdate',array('placeholder' => 'From Date',
        'label' => false,
		'required'=>'required',
        'class'=>'form-control'));?> 
   
  </div><div class="col-lg-4"><?php echo $this->Form->input('seltodate',array('placeholder' => 'To Date',
        'label' => false,
		'required'=>'required',
        'class'=>'form-control'));?>  </div>
  <div class="col-lg-4"> <?php echo $this->Form->button('<i class="fa fa-search"></i> Send', array(
    'type' => 'submit',
    'class' => 'btn btn-primary',
    'escape' => false
));?></div>
  <div class="clearfix"></div>
  <div class="col-lg-6">
  <?php
  $options = array(
    'All' => 'All',
    'Fill' => 'Filled',
	'Con' =>'Confirmed'
);

$attributes = array(
    'legend' => false,
	'required'=>'required',
    'value' => $foo
);

echo $this->Form->radio('seltype', $options, $attributes);
?>
  </div>
  
            </div></div>

  </div>
  <?php echo $this->Form->end();?>
  <div class="clearfix"></div>
  <div class="gap20"></div>
</div>


<div class="table-responsive">
             

            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                    
                        <th><?php echo __('Order');?></th>
                        <th><?php echo __('Quantity');?></th>
                        <th><?php echo __('Price');?></th> 
                       
                       
                    </tr>
                </thead>
                
                <tbody>
                <?php if(empty($users)): ?>
		<tr>
			<td colspan="6" class="striped-info">No record found.</td>
		</tr>
		<?php endif;  ?>
				<?php 
				$i=0;
				$total_price=0;
				
				foreach ($users as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				
				$product_qauantity=count($user['OrderdetailMany']);
				
				$priice=0;
				foreach($user['OrderdetailMany'] as $orderdeatils){
					$priice=$priice+($orderdeatils['price']*$orderdeatils['quantity']);
				}
				$product_price=$product_price+$priice;
				?>
                <tr <?php echo $class;?> > 
                  
                    &nbsp;</td>      
               
                <td><?php echo h($user['Order']['tmporderid']); ?><br />
                	<?php echo h(date("Y-m-d",strtotime($user['Order']['createddt']))); ?>
                    &nbsp;</td>
                <td><a class=" external" data-toggle="modal" href="/admin/orders/order_ifno/<?php echo $user['Order']['id']; ?>" data-target="#myModal"><?php echo h($product_qauantity); ?> Products</a><br />&nbsp;</td>
               
             <td><?php echo h($priice); ?>&nbsp;</td>
              <?php $total_price=$total_price+$user['Orderdetail']['price'];?>
                </tr>
                <?php endforeach; ?>                
                </tbody>
                <tfoot>
          <tr>
          
            <th colspan="2" style="text-align:right">Total:</th>
            <th><?php echo $product_price?></th>
          </tr>
        </tfoot>
            </table>
            <div class="pagination pagination-right pagination-mini">
	  <ul>
		<?php echo $this->Paginator->prev(''); ?>
		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
		<?php echo $this->Paginator->next(''); ?>
	  </ul>
	</div>
       </div>


   <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
      </div>
      </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->   
      
      <script>
$(document).ready(function(){
	$("#ProductInfoAdminReportMonthlyForm").validate();
	//$( "#ProductInfoSelfrmdate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
   // $( "#ProductInfoSeltodate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
	
	
    $( "#ProductInfoSelfrmdate, #ProductInfoSeltodate" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
       dateFormat: 'yy-mm-dd' ,
        onSelect: function( selectedDate ) {
            if(this.id == 'ProductInfoSelfrmdate'){
              var dateMin = $('#ProductInfoSelfrmdate').datepicker("getDate");
              var rMin = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 1); // Min Date = Selected + 1d
              var rMax = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 365); // Max Date = Selected + 31d
              $('#ProductInfoSeltodate').datepicker("option","minDate",rMin);
              $('#ProductInfoSeltodate').datepicker("option","maxDate",rMax);                    
            }

        }
    });

});
</script>