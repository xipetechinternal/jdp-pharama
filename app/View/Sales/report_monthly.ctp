

      <div class="row">   

        <div class="col-md-offset-8 col-lg-4">
<div class="panel panel-default" style="margin-top:-100px">

  <div class="panel-body form-bg text-center ">
<p>Welcome <?php echo ucfirst($this->Session->read('Auth.Sales.fname')); ?></p>

   <?php echo $this->Html->link('Sign out', array('controller'=>'Sales', 'action'=>'logout'),array('class' => 'btn btn-warning btn-block'));?>
   
   
</p>
  </div>
</div>
        </div>

        </div>
 
      <div class="clearfix"></div>
<div class="row">
  <div class="col-lg-12"> 
  <?php echo $this->Session->flash();  ?>
    <ol class="breadcrumb">
      <li><a href="<?php echo $this->Html->url('/Sales/index');?>">Home</a></li>
      <li class="active">Order History</li>
    </ol>
   
     
      <div class="form-group">
      <?php echo $this->Form->create('Sales',array('id'=>'product_serach','url'=>array('controller'=>'Sales','action'=>'report_monthly'),'type'=>'POST', 'class'=>'paraform')); ?>
      <div class="col-lg-10  ">
  <div class="form-group">
  <div class="col-lg-2"><label for="exampleInputEmail1">Select Date:</label></div>
  <div class="col-lg-8"> <div class="form-group">
  <div class="col-lg-4">   <?php echo $this->Form->input('selfrmdate',array('placeholder' => 'From Date',
        'label' => false,
		'required'=>'required',
        'class'=>'form-control'));?> 
   
  </div><div class="col-lg-4"><?php echo $this->Form->input('seltodate',array('placeholder' => 'To Date',
        'label' => false,
		'required'=>'required',
        'class'=>'form-control'));?>  </div>
  <div class="col-lg-4"> <?php echo $this->Form->button('<i class="fa fa-search"></i> Send', array(
    'type' => 'submit',
    'class' => 'btn btn-default',
    'escape' => false
));?></div>
  <div class="clearfix"></div>
  
  <div class="col-lg-6">
  <?php echo $this->Form->input('sid',array('empty'=>array(''=>'--Select customer --'),'options' => $sales,
        'label' => false,
		'required'=>'required',
        'class'=>'form-control'));?>
  </div>
  
            </div></div>

  </div>
  <?php echo $this->Form->end();?>
  <div class="clearfix"></div>
  <div class="gap20"></div>
</div>
            
            
          </div>
      
      <div class="clearfix"></div>
      <div class="gap20"></div>
    
    
    
    
    
  </div>
  
  
      <div class="col-lg-12 text-center">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">Order History</h3>
          </div>
          <div class="panel-body">
       <div class="table-responsive">
              
            <table id="example" class="datatable display table table-striped table-bordered" width="100%" >
                <thead>
                  <tr>
                    <th><?php echo __('Sl no.');?></th>
                    <th><strong><?php echo __('Order');?></strong></th>
                    <th><strong><?php echo __('Quantity');?></strong></th>
                    <th><strong><?php echo __('Price');?></strong></th>
                    <th><strong><?php echo __('Status');?></strong></th>
                  
                  </tr>
                </thead>
                 <?php if(empty($users)): ?>
		<tr>
			<td colspan="7" class="striped-info">No record found.</td>
		</tr>
		<?php endif;  ?>

                <tbody>
                
				<?php 
				
				$i=0;
				$product_price=0;
				foreach ($users as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				
				$product_qauantity=count($user['OrderdetailMany']);
				
				$priice=0;
				foreach($user['OrderdetailMany'] as $orderdeatils){
					$priice=$priice+($orderdeatils['price']*$orderdeatils['quantity']);
				}
				$product_price=$product_price+$priice;
				?>
                <tr <?php echo $class;?> > 
                        
               <td><?php echo $i; ?> &nbsp;</td>
                <td><?php echo h($user['Order']['tmporderid']); ?> &nbsp;</td>
               
               
                             
               <td>  <a href="#myModal"  rel="chatusr"  id="<?php echo $user['Order']['id']?>" class="btn push btn-warning" data-toggle="modal"><?php echo $product_qauantity.' Products'?> &nbsp;</a> 
                    &nbsp;</td>
                    <td>$<?php echo $priice ?> </td>
                    <td><?php echo h(($user['Order']['status']=='Accep')?'Accept':$user['Order']['status']); ?> &nbsp;</td>  
                   
 
                </tr>
                <?php endforeach; ?>                
                </tbody>
                <tfoot>
          <tr>
          
            <th colspan="3" style="text-align:right">Total:</th>
            <th>$<?php echo $product_price?></th>
          </tr>
        </tfoot>
              </table>
            <div class="pagination pagination-right pagination-mini">
	  <ul>
		<?php echo $this->Paginator->prev(''); ?>
		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
		<?php echo $this->Paginator->next(''); ?>
	  </ul>
	</div>
       </div>
       <div class="clearfix"></div>
<div class="gap20"></div>
	   
      </div>
        </div>
      </div>
     
      </div>
      </div>
    
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Order Detail</h4>
      </div>
      <div class="modal-body">
        <div class="something"> </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<style>
#content-area{
	background-image:none;
}
</style>

<script>
$(function(){

   $('.push').click(function(){
      var essay_id = $(this).attr('id');
	   
		
$.ajax({
          type : 'post',
           url : "<?php echo $this->webroot .$this->params["controller"]; ?>/orderinfo/"+essay_id,
		   data:'order_id='+ essay_id,    
                     // in php you should use $_POST['post_id'] to get this value 
       success : function(r)
           {
              // now you can show output in your modal 
              $('#mymodal').show();  // put your modal id 
             $('.something').show().html(r);
           }
    });

      
	  

});

   });
   
   
   $(document).ready(function(){
	$("#product_serach").validate();
	//$( "#ProductInfoSelfrmdate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
   // $( "#ProductInfoSeltodate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
	
	
    $( "#SalesSelfrmdate, #SalesSeltodate" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
       dateFormat: 'yy-mm-dd' ,
        onSelect: function( selectedDate ) {
            if(this.id == 'SalesSelfrmdate'){
              var dateMin = $('#SalesSelfrmdate').datepicker("getDate");
              var rMin = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 1); // Min Date = Selected + 1d
              var rMax = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 365); // Max Date = Selected + 31d
              $('#SalesSeltodate').datepicker("option","minDate",rMin);
              $('#SalesSeltodate').datepicker("option","maxDate",rMax);                    
            }

        }
    });

});
</script>