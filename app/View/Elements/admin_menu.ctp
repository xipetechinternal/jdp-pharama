<div class="span3" id="sidebar">
<ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
<li>
<a href="<?php echo $this->Html->url('/admin/pages/index');?>"><i class="icon-chevron-right"></i><?php echo __('Dashboard'); ?> </a>
</li>
<li <?php if($this->params['controller']=='categories'){ ?> class="active" <?php } ?> >
<a href="<?php echo $this->Html->url('/admin/categories/index');?>"><i class="icon-chevron-right"></i><?php echo __('Categories'); ?> </a>
</li>
<li <?php if($this->params['controller']=='users'){ ?> class="active" <?php } ?> >
<a href="<?php echo $this->Html->url('/admin/users/index');?>"><i class="icon-chevron-right"></i><?php echo __('Admin Users'); ?></a>
</li>

<li <?php if($this->params['controller']=='sales'){ ?> class="active" <?php } ?> >
<a href="<?php echo $this->Html->url('/admin/sales/index');?>"><i class="icon-chevron-right"></i><?php echo __('Sales Users'); ?></a>
</li>

<li <?php if($this->params['controller']=='operaters'){ ?> class="active" <?php } ?> >
<a href="<?php echo $this->Html->url('/admin/operaters/index');?>"><i class="icon-chevron-right"></i><?php echo __('Operaters Users'); ?></a>
</li>

<li <?php if($this->params['controller']=='customers'){ ?> class="active" <?php } ?> >
<a href="<?php echo $this->Html->url('/admin/customers/index');?>"><i class="icon-chevron-right"></i><?php echo __('Customers'); ?></a>
</li>

<li <?php if($this->params['controller']=='products'){ ?> class="active" <?php } ?> >
<a href="<?php echo $this->Html->url('/admin/products/index');?>"><i class="icon-chevron-right"></i><?php echo __('Products'); ?></a>
</li>

<li <?php if($this->params['controller']=='productStocks'){ ?> class="active" <?php } ?> >
<a href="<?php echo $this->Html->url('/admin/productStocks/index');?>"><i class="icon-chevron-right"></i><?php echo __('Products Stocks'); ?></a>
</li>

<li <?php if($this->params['controller']=='productStocks'){ ?> class="active" <?php } ?> >
<a href="<?php echo $this->Html->url('/admin/productStocks/index');?>"><i class="icon-chevron-right"></i><?php echo __('Sales Reports'); ?></a>
</li>

<li <?php if($this->params['controller']=='productStocks'){ ?> class="active" <?php } ?> >
<a href="<?php echo $this->Html->url('/admin/productStocks/index');?>"><i class="icon-chevron-right"></i><?php echo __('Threshold Stock Reports'); ?></a>
</li>

<li <?php if($this->params['controller']=='templates'){ ?> class="active" <?php } ?> >
<a href="<?php echo $this->Html->url('/admin/templates/index');?>"><i class="icon-chevron-right"></i> <?php echo __('Templates'); ?></a>
</li>

<li <?php if($this->params['controller']=='pages' && $this->params['action']=='admin_terms_conditions' ){ ?> class="active" <?php } ?> >
<a href="<?php echo $this->Html->url('/admin/pages/terms_conditions/1');?>"><i class="icon-chevron-right"></i> <?php echo __('Terms & Conditions'); ?></a>
</li>

<li <?php if($this->params['controller']=='pages' && $this->params['action']=='admin_privacy_policy'){ ?> class="active" <?php } ?> >
<a href="<?php echo $this->Html->url('/admin/pages/privacy_policy/2');?>"><i class="icon-chevron-right"></i> <?php echo __('Privacy Policy'); ?></a>
</li>

<li <?php if($this->params['controller']=='pages' && $this->params['action']=='admin_about_us'){ ?> class="active" <?php } ?> >
<a href="<?php echo $this->Html->url('/admin/pages/about_us/3');?>"><i class="icon-chevron-right"></i> <?php echo __('About Us'); ?></a>
</li>

<li <?php if($this->params['controller']=='pages' && $this->params['action']=='admin_online_store'){ ?> class="active" <?php } ?> >
<a href="<?php echo $this->Html->url('/admin/pages/online_store/4');?>"><i class="icon-chevron-right"></i> <?php echo __('Online Store'); ?></a>
</li>

<li <?php if($this->params['controller']=='pages' && $this->params['action']=='admin_documents'){ ?> class="active" <?php } ?> >
<a href="<?php echo $this->Html->url('/admin/pages/documents/5');?>"><i class="icon-chevron-right"></i> <?php echo __('Documents'); ?></a>
</li>



<!--<li>
<a href="#"><i class="icon-chevron-right"></i> Buttons & Icons</a>
</li>
<li>
<a href="#"><i class="icon-chevron-right"></i> WYSIWYG Editors</a>
</li>
<li>
<a href="#"><i class="icon-chevron-right"></i> UI & Interface</a>
</li>
<li>
<a href="#"><span class="badge badge-success pull-right">731</span> Orders</a>
</li>
<li>
<a href="#"><span class="badge badge-success pull-right">812</span> Invoices</a>
</li>
<li>
<a href="#"><span class="badge badge-info pull-right">27</span> Clients</a>
</li>
<li>
<a href="#"><span class="badge badge-info pull-right">1,234</span> Users</a>
</li>
<li>
<a href="#"><span class="badge badge-info pull-right">2,221</span> Messages</a>
</li>
<li>
<a href="#"><span class="badge badge-info pull-right">11</span> Reports</a>
</li>
<li>
<a href="#"><span class="badge badge-important pull-right">83</span> Errors</a>
</li>
<li>
<a href="#"><span class="badge badge-warning pull-right">4,231</span> Logs</a>
</li>-->
</ul>
</div>