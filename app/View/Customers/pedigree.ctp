

      
 
      <div class="clearfix"><br /></div>

<ol class="breadcrumb">
      <li><a href="<?php echo $this->Html->url('/customers/index');?>">Home</a></li>
      <li class="active">Order with Pedigree <?php echo $user_info['User']['fname']?></li>
    </ol>




<div class="col-lg-12 text-center">
        <div class="panel panel-primary">
        
        
        <div class="col-lg-12"> 
  <?php echo $this->Session->flash();  ?>
    
    <?php echo $this->Form->create('OrderHistory',array('type'=>'GET','url'=>array('action'=>'pedigree','controller'=>'customers'),'class'=>'paraform')); ?>
    <div class="form-group">
        <div class="col-lg-2" style="padding-top:10px"> 
          <label for="exampleInputEmail1">Invoice:</label>
        </div>
        <br />
    <div class="col-lg-10"> 
    <div class="form-group">
   <?php echo $this->Form->input('invoice',array('placeholder' => 'Invoice No',
        'label' => false,
		'type'=>'text',
		'style'=>'width:340px',
		'value'=>$invoice,
        'class'=>'form-control'));?> 
        </div>
        </div>
     </div>
      <div class="form-group">
        <div class="col-lg-2">
          <label for="exampleInputEmail1">Select Date:</label>
        </div>
        <div class="col-lg-8">
          <div class="form-group">
            <div class="col-lg-4">
            <?php echo $this->Form->input('selfrmdate',array('placeholder' => 'From Date',
        'label' => false,
		'value'=>$selfrmdate,
        'class'=>'form-control'));?> 
             
            </div>
            <div class="col-lg-4">
            <?php echo $this->Form->input('seltodate',array('placeholder' => 'To Date',
        'label' => false,
		'value'=>$seltodate,
        'class'=>'form-control'));?>
		<?php echo $this->Form->input('uid',array('placeholder' => 'To Date',
        'label' => false,
		'value'=>$uid,
		'type'=>'hidden',
        'class'=>'form-control'));?>
              
            </div>
            <div class="col-lg-4">
            <?php echo $this->Form->button('<i class="fa fa-search"></i> ', array(
    'type' => 'submit',
    'class' => 'btn btn-default',
    'escape' => false
));?>
              
            </div>
           
            
          </div>
        </div>
      </div>
        <div class="clearfix"></div>
           <?php echo $this->Form->end();?>
      <div class="clearfix"></div>
      <div class="gap20"></div>
    
    
    
    
    
  </div>
        
          <div class="panel-heading">
            <h3 class="panel-title">Order History</h3>
          </div>
          <div class="panel-body">
            <div class="table-responsive">
              <h2> Placed Orders</h2>
              
              <table id="example" class="datatable display table table-striped table-bordered" width="100%" >
                <thead>
                  <tr>
                   
                    <th><strong><?php echo __('Order');?></strong></th>
                     <th><strong><?php echo __('PO Number');?></strong></th>
                    <th><strong><?php echo __('Quantity');?></strong></th>
                    <th><strong><?php echo __('Price');?></strong></th>
                    <th><strong><?php echo __('Status');?></strong></th>
                    
                    <th><strong><?php echo __('ORDER DATE');?></strong></th>
                    <th><strong><?php echo __('Invoice');?></strong></th>
                  </tr>
                </thead>
                 <?php if(empty($users)): ?>
		<tr>
			<td colspan="7" class="striped-info">No record found.</td>
		</tr>
		<?php endif;  ?>

                <tbody>
                
				<?php 
					
				$i=0;
				$product_price=0;
				$total_paid=0;
				$total_unpaid=0;
				foreach ($users as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				
				$product_qauantity=count($user['OrderdetailMany']);
				
				$priice=0;
				foreach($user['OrderdetailMany'] as $orderdeatils){
					$priice=$priice+($orderdeatils['price']*$orderdeatils['quantity']);
				}
				$product_price=$product_price+$priice;
				?>
                <tr <?php echo $class;?> > 
                        
              
                <td <?php echo h(($user['OrderDist']['Paid_Status']=='paid')?'style=color:blue':""); ?>><?php echo h($user['OrderDist']['tmporderid']); ?> &nbsp;</td>
                <td><?php echo h($user['OrderDist']['po_number']); ?> &nbsp;</td>
               
                             
               <td>  <?php echo $this->Html->link($product_qauantity.' Products', array('controller'=>'customers', 'action'=>'orderinfo',$user['OrderDist']['id']),array('class' => ''));?>
                    &nbsp;</td>
                    <td>$<?php echo $priice ?> </td>
                    <td><?php echo h(($user['OrderDist']['status']=='Accep')?'Accept':$user['OrderDist']['status']); ?> &nbsp;</td>  
                    <td><?php echo date("m-d-Y",strtotime($user['OrderDist']['createddt'])); ?> &nbsp;</td>  
                    <td class="td-font">
                    <?php if($user['OrderDist']['status']=='Accep' or $user['OrderDist']['status']=='Test'){?>
                     <?php echo $this->Html->link('Invoice',array('action' => 'invoice', $user['OrderDist']['id']),array("class"=>"","data-original-title"=>"Edit",'escape'=>false)); ?>|<?php echo $this->Html->link('PDF Invoice',array('action' => 'invoicepdf', $user['OrderDist']['id']),array("class"=>"","data-original-title"=>"Edit",'escape'=>false)); ?>
                   
                    <?php }?>
                    <?php  echo $this->Html->link($this->Html->tag('i',"Pedigree",array('class'=>'icon-pencil icon-white')),array('controller'=>'customers','action' => 'orderinfo', $user['OrderDist']['id']),array("class"=>"btn btn-info  btn-mini btn-left-margin","data-original-title"=>"Manually",'escape'=>false));?>
                  
              
                    </td>
 
                </tr>
                <?php endforeach; ?>                
                </tbody>
                <tfoot>
          <tr>
          	<th colspan="3" style="text-align:right">Total Paid:</th>
            <th><?php /*echo $total_paid*/?><?php echo h('$'.number_format($totalPaid,2)); ?></th>
           </tr>
           <tr>
            <th colspan="3" style="text-align:right">Total UnPaid:</th>
            <th><?php /*echo $total_unpaid*/?><?php echo h('$'.number_format($totalUnpaid,2)); ?></th>
             </tr>
           <tr>
            <th colspan="3" style="text-align:right">Total:</th>
            <th><?php /*echo $product_price*/?><?php echo h('$'.number_format($totalUnpaid+$totalPaid,2)); ?></th>
          </tr>
        </tfoot>
              </table>
              <div class="pagination pagination-right pagination-mini">
	  <ul>
		<?php echo $this->Paginator->prev(''); ?>
		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
		<?php echo $this->Paginator->next(''); ?>
	  </ul>
	</div>
            </div>
            <div class="clearfix"></div>
             <div class="clearfix"></div>
            <!-- Added By Rashid Khan -->
            <!--<div class="table-responsive">
                <strong> Net Paid: <?php echo h('$'.number_format($totalPaid,2)); ?></strong><br>
                <strong> Net Unpaid: <?php echo h('$'.number_format($totalUnpaid,2)); ?></strong><br>
                
              </div>-->
              <!--  END  -->
            <div class="gap20"></div
            <div class="gap20"></div>
           

            
          </div>
        </div>
      

 
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Order Detail</h4>
      </div>
      <div class="modal-body">
        <div class="something"> </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){
	$("#ProductInfoAdminSalesReportPersonForm").validate();
	//$( "#ProductInfoSelfrmdate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
   // $( "#ProductInfoSeltodate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
	
	
    $( "#OrderHistorySelfrmdate, #OrderHistorySeltodate" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
       dateFormat: 'yy-mm-dd' ,
        onSelect: function( selectedDate ) {
            if(this.id == 'OrderHistorySelfrmdate'){
              var dateMin = $('#OrderHistorySelfrmdate').datepicker("getDate");
              var rMin = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 1); // Min Date = Selected + 1d
              var rMax = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 365); // Max Date = Selected + 31d
              $('#OrderHistorySeltodate').datepicker("option","minDate",rMin);
              $('#OrderHistorySeltodate').datepicker("option","maxDate",rMax);                    
            }

        }
    });

});
</script>

<script>



$(function(){

   $('.push').click(function(){
      var essay_id = $(this).attr('id');
	   
		
$.ajax({
          type : 'post',
           url : "<?php echo $this->webroot .'admin/'. $this->params["controller"]; ?>/getlot_details",
		   data:'order_id='+ essay_id,    
                     // in php you should use $_POST['post_id'] to get this value 
       success : function(r)
           {
              // now you can show output in your modal 
              $('#mymodal').show();  // put your modal id 
             $('.something').show().html(r);
           }
    });

      
	  

});

   });
</script>