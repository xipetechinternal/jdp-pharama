

 
<div class="clearfix"></div>

<div class="row">
  <div> 
  <?php echo $this->Session->flash();  ?>
    <ol class="breadcrumb">
      <li><a href="<?php echo $this->Html->url('/customers/index');?>">Home</a></li>
      <li class="active">Order History</li>
    </ol>
    
    <div class="form-group">
      <div class="col-md-2">
        <label for="exampleInputEmail1" style="margin-top:5px;">Enter Search :</label>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <?php echo $this->Form->create('customers',array('id'=>'product_serach','controller'=>'customers','action'=>'product_search','type'=>'GET', 'class'=>'paraform')); ?>
          <div class="input-group">

            <?php echo $this->Form->input('searchtxt',array('placeholder' => 'Enter Keyword',
  'label' => false,
'required'=>'required',

  'class'=>'form-control'));?>
            <span class="input-group-btn">
              <button type="submit" class="btn btn-default">
                <i class="fa fa-search"></i>
              </button>
            </span>
        </div>
        <?php echo $this->Form->end();?>
        </div>
      </div>
    </div>
  </div>
</div>

<?php if($product_type!=NULL):;?>

<div class="clearfix"></div>

<div class="text-center">
  <div class="panel panel-primary">
    <div class="panel-heading">
      <h3 class="panel-title">Product List</h3>
    </div>
    
    <div class="panel-body1">
      <div class="table-responsive1">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
          <thead>
            <tr>       
              <th><?php echo __('Item&nbsp;#');?></th>
              <th><?php echo __('Product NDC');?></th> 
              <th><?php echo __('Product Info');?></th> 
              <th><?php echo __('Price');?></th>
              <th><?php echo __('Inventory');?></th>
               <th><?php echo __('Manufacture');?></th>
              <th><?php echo __('Actions');?></th>
            </tr>
          </thead>
                
          <tbody>
            <?php if(empty($products)): ?>
        		<tr>
        			<td colspan="7" class="striped-info">No record found.</td>
        		</tr>
        		<?php endif;  ?>
        				<?php 
        				$i=0;
        				foreach ($products as $user): 
        				$class =' class="odd"';
        				if ($i++ % 2 == 0) {
        					$class = ' class="even"';
        				}
				//Apply price discount accourding client base;
				/*$priceafter_discount=$user['Product']['productprice'];
				$discount=($this->Session->read("Auth.User.".$user['Product']['producttype']."_discount")/100);
				$priceafter_discount=($discount>0)?round($priceafter_discount-($discount*$priceafter_discount),2):$priceafter_discount;*/
				
				$productPrice=$user['Product']['productprice'];
				$purchasePrice=$user['Product']['PurchasePrice'];
				//$product_prafit=$productPrice-$purchasePrice;
				$discount=($this->Session->read("Auth.User.Refrigirated_discount")/100);				
				$profit_percent=$purchasePrice*$discount;
				
				$priceafter_discount=($profit_percent>0)?round(($purchasePrice-$profit_percent),2):$purchasePrice;
				/*if($threpercenteoff)
				$priceafter_discount=$priceafter_discount-($priceafter_discount*3/100);*/
				?>
                <tr <?php echo $class;?> > 
                           
                <td><?php echo h($user['Product']['itemnumber']); ?>&nbsp;</td>
                <td><?php echo h($user['Product']['product_ndc']); ?>&nbsp;</td>
                <td><?php echo h('Product Name: '.$user['Product']['productname'])?><br>
                	Category :<?php echo h($user['Product']['producttype']); ?><br>
                    &nbsp;</td>
                <td><?php echo ($user['Product']['instock']>0)?h(round($priceafter_discount,2)):'Call for Price'; ?>&nbsp;</td>
                <td><?php echo h($user['Product']['instock']); ?>&nbsp;</td>        
                  <td><?php echo h($user['Product']['manufacture']); ?>&nbsp;</td>         
                <td class="center">  <?php if($user['Product']['add_restriction']){?>
                <a href="<?Php echo $this->Html->url('/customers/add_restriction_serach/'.$product_type)?>"  class="btn btn-primary push-request" data-toggle="modal">Request</a>
                <?php }else{
					 echo ($user['Product']['instock']>0 and $user['Product']['active']!="False")? '<a href="#myModal" data-qtyinstock="'.$user['Product']['instock'].'" data-producttype="'.$user['Product']['producttype'].'" rel="chatusr"  id="'.$user['Product']['id'].'" class="push btn push btn-success" data-toggle="modal">Order</a>':'<a class="btn btn-primary push-request" data-id="'.$user['Product']['id'].'" data-producttype="'.$user['Product']['producttype'].'" href="#requestModal"  data-toggle="modal"> Request </a>';
				}?>
                
               
               
 				</td>
                </tr>
                <?php endforeach; ?>                
                </tbody>
            </table>
            <div class="pagination pagination-right pagination-mini">
	  <ul>
		<?php echo $this->Paginator->prev(''); ?>
		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
		<?php echo $this->Paginator->next(''); ?>
	  </ul>
	</div>
       </div>
      </div>
    </div>


</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Place Order</h4>
      </div>
      <div class="modal-body">
        <div class="something"><?php echo $this->Form->create('customers',array('action'=>'basketUpdate','type'=>'file', 'class'=>'login-from')); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <?php echo $this->Form->input('prodtype',array('placeholder' => 'Item Number',
        'label' => '',
		'required'=>'required',
		'type'=>'hidden',
        'class'=>'form-control'));?>
        <?php echo $this->Form->input('product_search',array('placeholder' => 'Item Number',
        'label' => '',
		'value'=>$product_type,
		'required'=>'required',
		'type'=>'hidden',
        'class'=>'form-control'));?>

       <?php echo $this->Form->input('pro_id',array('placeholder' => 'Item Number',
        'label' => '',
		'required'=>'required',
		'type'=>'hidden',
        'class'=>'form-control'));?>
 
  <tr>
    <td align="left" valign="top">Quantity :   <?php echo $this->Form->input('quantity',array('placeholder' => 'Quantity',
        'label' => '',
		'required'=>'required',		
        'class'=>'form-control'));?><div class="gap20"></div></td>
  </tr>

 <tr>
    <td align="left" valign="top"> <?php echo $this->Form->submit(__('Order'), array('class'=>'btn btn-primary', 'div'=>false));?>&nbsp;</td>
  </tr>
  
  
</table>
<?php echo $this->Form->end();?> </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="requestModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Place Request</h4>
      </div>
      <div class="modal-body">
        <div class="something"><?php echo $this->Form->create('customers',array('action'=>'product_request','type'=>'file', 'class'=>'login-from')); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <?php echo $this->Form->input('reprodtype',array('placeholder' => 'Item Number',
        'label' => '',
		'required'=>'required',
		'type'=>'hidden',
        'class'=>'form-control'));?>
      <?php echo $this->Form->input('reproduct_search',array('placeholder' => 'Item Number',
        'label' => '',		
		'required'=>'required',
		'type'=>'hidden',
        'class'=>'form-control'));?>

       <?php echo $this->Form->input('repro_id',array('placeholder' => 'Item Number',
        'label' => '',
		'required'=>'required',
		'type'=>'hidden',
        'class'=>'form-control'));?>
 
  <tr>
    <td align="left" valign="top">Quantity :   <?php echo $this->Form->input('requantity',array('placeholder' => 'Quantity',
        'label' => '',
		'required'=>'required',		
        'class'=>'form-control'));?><div class="gap20"></div></td>
  </tr>

 <tr>
    <td align="left" valign="top"> <?php echo $this->Form->submit(__('Request'), array('class'=>'btn btn-primary', 'div'=>false));?>&nbsp;</td>
  </tr>
  
  
</table>
<?php echo $this->Form->end();?> </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php endif;?>

<div id="search_alaert_modal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- dialog body -->
      <div class="modal-body" style="padding-bottom:0px">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       <strong style="color:#eb7228;font-size:18px">ATTENTION</strong>
      </div>
      <!-- dialog buttons -->
      <div class="modal-footer text-center"><p class="text-left"> PLEASE ORDER <a href="http://jdpwholesaler.com/customers/product_search?searchtxt=<?php echo $user['Product']['sub_ndc']?>"><?php echo $user['Product']['sub_ndc']?></a> NDC BECAUSE THE NDC YOU PUT IS NOT AVAILABLE DUE TO SHORTAGE OR DISCONTINUATION OF THAT NDC</p></div>
    </div>
  </div>
</div>
<script>

	<?php if($user['Product']['substitute']){?>
    $(document).ready(function(e) {
        $("#search_alaert_modal").modal({                    // wire up the actual modal functionality and show the dialog
      "backdrop"  : "static",
      "keyboard"  : true,
      "show"      : true                     // ensure the modal is shown immediately
    });
    });
    <?php }?>
    



$(function(){

   $("#product_serach").validate();

   });
   $(function(){

   $('.push').click(function(){
      var essay_id = $(this).attr('id');
	    var quantity = $(this).data('qtyinstock');
		$("#customersProId").val(essay_id);
		$("#customersProdtype").val($(this).data('producttype'));
		
$('#mymodal').show();
      
	  $("#customersBasketUpdateForm").validate({

		rules: {
			"data[customers][quantity]":{
				required: true,
				number:true,
				min:1,
				max:quantity

				}
		},
		messages: {
			"data[customers][quantity]":"Please enter a valid number & Order should be less than ".quantity,
			
		},
		errorElement:"span"

		});


});

 $('.push-request').click(function(){
      var essay_id = $(this).data('id');
	   
		$("#customersReproId").val(essay_id);
		$("#customersReprodtype").val($(this).data('producttype'));
		$("#customersReproductSearch").val($("#customersSearchtxt").val());
$('#requestModal').show();
      
	  $("#customersProductRequestForm").validate({

		rules: {
			"data[customers][requantity]":{
				required: true,
				number:true,
				min:1,
				

				}
		},
		messages: {
			"data[customers][requantity]":"Please enter a valid number",
			
		},
		errorElement:"span"

		});


});

   });

</script>