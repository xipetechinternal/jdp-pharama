<?php $this->set('title_for_layout', __('Customers',true)); ?>
<?php $this->Html->addCrumb(__('Customers',true), '/admin/customers/index',array('tag'=>'li'));  ?>
<?php $this->Html->addCrumb($this->Html->tag('li',__('Add',true),array('class'=>'active')),false,false); ?>

<div class="row-fluid">
  <div class="block">
    <div class="navbar navbar-inner block-header">
      <div class="muted pull-left"><?php echo __('Add Customers User'); ?></div>
    </div>
    <div class="block-content collapse in">
      <div class="span12"> <?php echo $this->Form->create('Sale',array('controller'=>'sales','action' => 'admin_add','class' => 'form-horizontal'));?>
        <fieldset>
          <legend><?php echo __('Personal Details'); ?></legend>
          <?php

echo $this->Form->input('User.username', array('div'=>'control-group',
'before'=>' <label class="control-label">'.__('Username').' : </label><div class="controls">', 
'after'=>$this->Form->error('User.username', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));


echo $this->Form->input('User.name', array('div'=>'control-group',
'before'=>' <label class="control-label">'.__('Name').' : </label><div class="controls">', 
'after'=>$this->Form->error('User.name', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));


echo $this->Form->input('User.email', array('div'=>'control-group', 
'before'=>' <label class="control-label">'.__('Email').' : </label><div class="controls">',
'after'=>$this->Form->error('User.email', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));

echo $this->Form->input('User.password', array('div'=>'control-group', 
'before'=>' <label class="control-label">'.__('Password').' : </label><div class="controls">',
'after'=>$this->Form->error('User.password', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));


echo $this->Form->input('User.password2', array('div'=>'control-group', 'type'=>'password', 
'before'=>' <label class="control-label">'.__('Confirm Password').' : </label><div class="controls">',
'after'=>$this->Form->error('User.password2', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused')); ?>
        </fieldset>
        <fieldset>
          <legend><?php echo __('Contact Details'); ?></legend>
          <?php 

echo $this->Form->input('User.contact_name', array('div'=>'control-group',
'before'=>' <label class="control-label">'.__('Contact Name').' : </label><div class="controls">', 
'after'=>$this->Form->error('User.contact_name', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));

echo $this->Form->input('User.state_no', array('div'=>'control-group',
'before'=>' <label class="control-label">'.__('State No.').' : </label><div class="controls">', 
'after'=>$this->Form->error('User.state_no', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));

echo $this->Form->input('User.state_exp_date', array('div'=>'control-group',
'before'=>' <label class="control-label">'.__('State Expariy').' : </label><div class="controls">', 
'after'=>$this->Form->error('User.state_exp_date', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused datepicker','type'=>'text'));

echo $this->Form->input('User.den_no', array('div'=>'control-group',
'before'=>' <label class="control-label">'.__('DEN NO.').' : </label><div class="controls">', 
'after'=>$this->Form->error('User.den_no', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));

echo $this->Form->input('User.den_exp_date', array('div'=>'control-group',
'before'=>' <label class="control-label">'.__('DEN Expairy').' : </label><div class="controls">', 
'after'=>$this->Form->error('User.den_exp_date', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused datepicker','type'=>'text'));

echo $this->Form->input('User.phone', array('div'=>'control-group',
'before'=>' <label class="control-label">'.__('Phone').' : </label><div class="controls">', 
'after'=>$this->Form->error('User.phone', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));

echo $this->Form->input('User.fax', array('div'=>'control-group',
'before'=>' <label class="control-label">'.__('Fax').' : </label><div class="controls">', 
'after'=>$this->Form->error('User.fax', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));

echo $this->Form->input('User.city', array('div'=>'control-group',
'before'=>' <label class="control-label">'.__('City').' : </label><div class="controls">', 
'after'=>$this->Form->error('User.city', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));


echo $this->Form->input('User.state', array('div'=>'control-group',
'before'=>' <label class="control-label">'.__('State').' : </label><div class="controls">', 
'after'=>$this->Form->error('User.state', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));

echo $this->Form->input('User.zip_code', array('div'=>'control-group',
'before'=>' <label class="control-label">'.__('Zip Code').' : </label><div class="controls">', 
'after'=>$this->Form->error('User.zip_code', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));

echo $this->Form->input('User.address', array('div'=>'control-group',
'before'=>' <label class="control-label">'.__('Address').' : </label><div class="controls">', 
'after'=>$this->Form->error('User.address', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));

echo $this->Form->input('User.member_type', array('div'=>'control-group',  
'before'=>' <label class="control-label">'.__('Member Type').' : </label><div class="controls">', 
'after'=>'</div>','label'=>false, 'class'=>'input-xlarge focused','empty' => '-- Select--','options'=>array('1'=>'Gold','2'=>'Silver','3'=>'Bronze')));


echo $this->Form->input('User.parent_id', array('div'=>'control-group',  
'before'=>' <label class="control-label">'.__('Sales').' : </label><div class="controls">', 
'after'=>'</div>','label'=>false, 'class'=>'input-xlarge focused','empty' => '-- Select--','options'=>$sales));

echo $this->Form->input('User.group_id', array('div'=>'control-group',  
'before'=>' <label class="control-label">'.__('Group').' : </label><div class="controls">', 
'after'=>'</div>','label'=>false, 'class'=>'input-xlarge focused','disabled'=>true,'selected'=>array('0'=>2)));

echo $this->Form->input('User.status', array('div'=>'control-group', 
'before'=>' <label class="control-label">'.__('Status').' : </label><div class="controls">',
'after'=>'</div>','label'=>false, 'class'=>'uniform_on','type'=>'checkbox'));
?>
          <div class="form-actions"> <?php echo $this->Form->submit(__('Submit'), array('class'=>'btn btn-primary', 'div'=>false));?>&nbsp; <?php echo $this->Form->reset(__('Cancel'), array('class'=>'btn', 'div'=>false));?> </div>
        </fieldset>
        <?php echo $this->Form->end();?> </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    jQuery(function(){
    
    
    jQuery("#UserUsername").validate({
    expression: "if (VAL) return true; else return false;",
    message: "Please enter Username"
    });  
    
    jQuery("#UserName").validate({
    expression: "if (VAL) return true; else return false;",
    message: "Please enter First Name"
    });
    
    jQuery("#UserCity").validate({
    expression: "if (VAL) return true; else return false;",
    message: "Please enter City"
    });	
    
    jQuery("#UserState").validate({
    expression: "if (VAL) return true; else return false;",
    message: "Please enter State"
    });	
    
    jQuery("#UserZipCode").validate({
    expression: "if (VAL) return true; else return false;",
    message: "Please enter Zip Code"
    });	
    
    
    jQuery("#UserContactName").validate({
    expression: "if (VAL) return true; else return false;",
    message: "Please enter contact name"
    });	
    
    
    jQuery("#UserStateNo").validate({
    expression: "if (VAL) return true; else return false;",
    message: "Please enter State No"
    });	
    
    jQuery("#UserStateExpDate").validate({
    expression: "if (VAL) return true; else return false;",
    message: "Please enter State No Expiry Date"
    });	
    
    jQuery("#UserDenNo").validate({
    expression: "if (VAL) return true; else return false;",
    message: "Please enter DEN No "
    });	
    
    
    jQuery("#UserDenExpDate").validate({
    expression: "if (VAL) return true; else return false;",
    message: "Please enter DEN No Expiry Date"
    });	
    
    
    
    jQuery("#UserEmail").validate({
    expression: "if 	(VAL.match(/^[^\\W][a-zA-Z0-9\\_\\-\\.]+([a-zA-Z0-9\\_\\-\\.]+)*\\@[a-zA-Z0-9_]+(\\.[a-zA-Z0-9_]+)*\\.[a-zA-Z]{2,4}$/)) return true; else return false;",
    message: "Please enter a valid Email ID"
    });
	
   
    
    jQuery('.form-horizontal').validated(function(){
    this.submit();
    });
    });
</script>