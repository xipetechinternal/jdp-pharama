

      
 
      <div class="clearfix"></div>
      
<div class="row">
  <div class="col-lg-12"> 
  <?php echo $this->Session->flash();  ?>
    <ol class="breadcrumb">
      <li><a href="<?php echo $this->Html->url('/customers/index');?>">Home</a></li>
      <li class="active">Product Type Search</li>
    </ol>
    <?php echo $this->Form->create('OrderHistory',array('url'=>array('action'=>'producta_inventory','controller'=>'customers'),'class'=>'paraform')); ?>
   <?php echo $this->Form->input('orderid',array('placeholder' => 'From Date',
        'label' => false,
		'type'=>'hidden',
		'required'=>'required',
        'class'=>'form-control'));?> 
     
      <div class="form-group">
        <div class="col-lg-2">
          <label for="exampleInputEmail1">Select Date:</label>
        </div>
        <div class="col-lg-8">
          <div class="form-group">
            <div class="col-lg-4">
            <?php echo $this->Form->input('selfrmdate',array('placeholder' => 'From Date',
        'label' => false,
		
        'class'=>'form-control'));?> 
             
            </div>
            <div class="col-lg-4">
            <?php echo $this->Form->input('seltodate',array('placeholder' => 'To Date',
        'label' => false,
		
        'class'=>'form-control'));?>
              
            </div>
            <div class="col-lg-4">
            <?php echo $this->Form->button('<i class="fa fa-search"></i> ', array(
    'type' => 'submit',
    'class' => 'btn btn-default',
    'escape' => false
));?>
              
            </div>
           
            
          </div>
        </div>
      </div>
        <div class="clearfix"></div>
      <div class="form-group">
      <div class="col-md-2">
        <label for="exampleInputEmail1" style="margin-top:5px;">Enter Search :</label>
      </div>
      <div class="col-md-6">
        <div class="form-group">
        
          <div class="input-group">

            <?php echo $this->Form->input('searchtxt',array('placeholder' => 'Prodcut NDC',
  'label' => false,
  'class'=>'form-control'));?>
            
        </div>
      
        </div>
      </div>
    </div>
      <?php echo $this->Form->end();?>
      <div class="clearfix"></div>
      <div class="gap20"></div>
    
    
    
    
    
  </div>
  
  
      <div class="col-lg-12 text-center">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">Product Inventory</h3>
          </div>
          <div class="panel-body">
            <div class="table-responsive">
             
              <?PHP $searcset=(isset($this->request->data['OrderHistory']['selfrmdate']) and isset($this->request->data['OrderHistory']['seltodate']))?$this->request->data['OrderHistory']['selfrmdate'].'/'.$this->request->data['OrderHistory']['seltodate']:'';?>
                 <div class="col-lg-12 text-right" style="padding:20px"> &nbsp;&nbsp;<a class="btn btn-warning btn-mini btn-left-margin" href="<?php echo $this->Html->url(array_merge(array('controller'=>'customers','action'=>'producta_inventory_excel/'.$searcset),$this->params['named'],array(''))); ?>"><i class="icon-file-text"></i> Download Excel/CSV</a></div>
                 
                
              <table id="example" class="datatable display table table-striped table-bordered" width="100%" >
                <thead>
                  <tr>
                    
                    <th><strong><?php echo __('Product NDC');?></strong></th>
                    <th><strong><?php echo __('Item Number');?></strong></th>
                    <th><strong><?php echo __('Product Name');?></strong></th>
                    <th><strong><?php echo __('Quantity');?></strong></th>
                    <th><strong><?php echo __('Size');?></strong></th>
                    
                    <th><strong><?php echo __('Total Size Purchase');?></strong></th>
                    <th><strong><?php echo __('Total Dispense');?></strong></th>
                    <th><strong><?php echo __('Total Left over');?></strong></th>
                  </tr>
                </thead>
                 <?php if(empty($users)): ?>
		<tr>
			<td colspan="8" class="striped-info">History not available.</td>
		</tr>
		<?php endif;  ?>

                <tbody>
                
				<?php 
				
				$i=0;
				$product_price=0;
				foreach ($users as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				
				
				?>
                <tr <?php echo $class;?> > 
                        
               
                <td><?php echo h($user['Product NDC']); ?> &nbsp;</td>
               <td><?php echo h($user['Item Number']); ?> &nbsp;</td>
               
                             
               <td>  <?php echo h($user['Product Name']); ?>&nbsp;</td>
                    <td><?php echo h($user['Quantity']); ?> </td>
                    <td><?php echo h($user['Size']); ?> &nbsp;</td>  
                    <td><?php echo h($user['Total Size Purchase']); ?> &nbsp;</td>  
                    <td><?php echo h($user['Total Dispense']); ?> &nbsp;</td>
                    <td><?php echo h($user['Total Left over']); ?> &nbsp;</td>
 
                </tr>
                <?php endforeach; ?>                
                </tbody>
                
              </table>
              
            </div>
            <div class="clearfix"></div>
            <div class="gap20"></div>
           

            
          </div>
        </div>
      </div>
     
      </div>
      
<script>
$(document).ready(function(){
	$("#ProductInfoAdminSalesReportPersonForm").validate();
	//$( "#ProductInfoSelfrmdate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
   // $( "#ProductInfoSeltodate" ).datepicker({dateFormat: 'yy-mm-dd' }); 
	
	
    $( "#OrderHistorySelfrmdate, #OrderHistorySeltodate" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
       dateFormat: 'yy-mm-dd' ,
        onSelect: function( selectedDate ) {
            if(this.id == 'OrderHistorySelfrmdate'){
              var dateMin = $('#OrderHistorySelfrmdate').datepicker("getDate");
              var rMin = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 1); // Min Date = Selected + 1d
              var rMax = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 365); // Max Date = Selected + 31d
              $('#OrderHistorySeltodate').datepicker("option","minDate",rMin);
              $('#OrderHistorySeltodate').datepicker("option","maxDate",rMax);                    
            }

        }
    });

});
</script>