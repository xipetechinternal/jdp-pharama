
<div class="block-content collapse in">
<div class="span12">                            
<table class="table table-bordered">
    <tbody>
        <tr>
            <th><?php echo __('Id'); ?></th>
            <td><?php echo h($product['Product']['id']); ?></td>
        </tr>
        <tr>
            <th><?php echo __('Category'); ?></th>
            <td><?php echo h($product['Category']['name']); ?></td>
        </tr>
        <tr>
            <th><?php echo __('Name'); ?></th>
            <td><?php echo h($product['Product']['product_name']); ?></td>
        </tr>
        <tr>
            <th><?php echo __('NDC'); ?></th>
            <td><?php echo h($product['Product']['product_ndc']); ?></td>
        </tr>
        <tr>
            <th><?php echo __('Price'); ?></th>
            <td><?php echo h($product['Product']['product_price']); ?></td>
        </tr>
        <tr>
            <th><?php echo __('Item No.'); ?></th>
            <td><?php echo h($product['Product']['item_number']); ?></td>
        </tr> 
        <tr>
            <th><?php echo __('Threshold'); ?></th>
            <td><?php echo h($product['Product']['threshold']); ?></td>
        </tr>
      
        <tr>
            <th><?php echo __('Created'); ?></th>
            <td><?php echo h(date("j M Y , g:i A",strtotime($product['Product']['created']))); ?></td>
        </tr>
        <tr>
            <th><?php echo __('Modified'); ?></th>
            <td><?php echo h(date("j M Y , g:i A",strtotime($product['Product']['modified']))); ?></td>
        </tr>
    </tbody>
</table> 
</div>
</div>
