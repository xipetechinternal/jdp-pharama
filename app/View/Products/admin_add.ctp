<?php $this->set('title_for_layout', __('Prodcuts',true)); ?>
<?php $this->Html->addCrumb(__('Prodcuts',true), '/admin/sales/index',array('tag'=>'li'));  ?>
<?php $this->Html->addCrumb($this->Html->tag('li',__('Add',true),array('class'=>'active')),false,false); ?>

<div class="row-fluid">
<div class="block">
<div class="navbar navbar-inner block-header">
<div class="muted pull-left"><?php echo __('Prodcuts'); ?></div>
</div>
<div class="block-content collapse in">
<div class="span12">
<?php echo $this->Form->create('Product',array('controller'=>'products','action' => 'admin_add','class' => 'form-horizontal'));?>
<fieldset>
<legend><?php echo __('Add Prodcut'); ?></legend>

<?php

echo $this->Form->input('Product.category_id', array('div'=>'control-group',  
'before'=>' <label class="control-label">'.__('Categories').' : </label><div class="controls">', 
'after'=>'</div>','label'=>false, 'class'=>'input-xlarge focused','options'=>$categories,'empty'=>'----Select----'));

echo $this->Form->input('Product.product_name', array('div'=>'control-group',
'before'=>' <label class="control-label">'.__('Product Name').' : </label><div class="controls">', 
'after'=>$this->Form->error('Product.product_name', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));


echo $this->Form->input('Product.product_price', array('div'=>'control-group',
'before'=>' <label class="control-label">'.__('Price').' : </label><div class="controls">', 
'after'=>$this->Form->error('Product.product_price', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));


echo $this->Form->input('Product.product_ndc', array('div'=>'control-group', 
'before'=>' <label class="control-label">'.__('Product NDC ').' : </label><div class="controls">',
'after'=>$this->Form->error('Product.product_ndc', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));

echo $this->Form->input('Product.item_number', array('div'=>'control-group', 
'before'=>' <label class="control-label">'.__('Item No.').' : </label><div class="controls">',
'after'=>$this->Form->error('Product.item_number', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));


echo $this->Form->input('Product.threshold', array('div'=>'control-group', 'type'=>'text', 
'before'=>' <label class="control-label">'.__('Threshold').' : </label><div class="controls">',
'after'=>$this->Form->error('Product.threshold', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));








echo $this->Form->input('Product.status', array('div'=>'control-group', 
'before'=>' <label class="control-label">'.__('Status').' : </label><div class="controls">',
'after'=>'</div>','label'=>false, 'class'=>'uniform_on','type'=>'checkbox'));
?>                                        

<div class="form-actions">
<?php echo $this->Form->submit(__('Submit'), array('class'=>'btn btn-primary', 'div'=>false));?>&nbsp;
<?php echo $this->Form->reset(__('Cancel'), array('class'=>'btn', 'div'=>false));?>
</div>

</fieldset>
<?php echo $this->Form->end();?>

</div>
</div>
</div>
</div>