<div class="col-lg-12">
<h4><a href="<?php echo $this->Html->url('/admin/users');?>">Home</a> :: Products Activity</h4>
<div class="col-lg-12  ">
 <?php echo $this->Form->create('ProductInfo',array('url'=>array('action'=>'admin_product_change_history','controller'=>'products'),'type'=>'get', 'class'=>'form-horizontal login-from')); ?>
 <div class="clearfix"></div>
  <div class="gap20">&nbsp;<br /><br /><br /></div>
   <div class="gap20"></div>
  <div class="form-group">
   <div class="gap20"></div>
  <div class="col-lg-2"><label for="exampleInputEmail1">Search By:</label></div>
  <div class="col-lg-8"> <div class="form-group">
  <div class="col-lg-4">   <?php echo $this->Form->input('ndcproduct',array('placeholder' => 'Item no/Product NDC/Product Info',
        'label' => false,
		'value'=>$search,
		'required'=>'required',
        'class'=>'form-control'));?> 
   
  </div>
  <div class="col-lg-4"> <?php echo $this->Form->button('<i class="fa fa-search"></i> Search', array(
    'type' => 'submit',
    'class' => 'btn btn-primary',
    'escape' => false
));?></div>

  <div class="clearfix"></div>
  
  <small>Search By <cite title="Source Title">Item no/Product NDC/Product Details/Lot#/Modified By</cite></small>
            </div></div>

  </div>
  <?php echo $this->Form->end();?>
  <div class="clearfix"></div>
  <div class="gap20"></div>
</div>
      


</div>
<div class="col-lg-12 text-left" style="padding:20px">
                        <a href="javascript:;" onclick="history.go(-1);" class="btn btn-primary btn-mini btn-left-margin">Back</a>
            
        </div>
<div class="col-lg-12 text-center">

      <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Inventory changes</h3>
      </div>
      <div class="panel-body">
       <div class="table-responsive">
             
			<input type="text" name="foucs" id="foucs" />
            <table cellpadding="0" cellspacing="0" border="0" class="table-condensed table  table-bordered" id="example">
                <thead>
                    <tr>
                        <th><?php echo __('Product NDC');?></th>
                        <th><?php echo __('Product Description');?></th>
                        <th><?php echo __('WAC');?></th> 
                        <th><?php echo __('Old Purchase Price');?></th>
                        <th><?php echo __('New Purchase Price');?></th>
                        <th><?php echo __('Invoice');?></th>
                        <th><?php echo __('Vendor');?></th> 
                        <th><?php echo __('Modified Date');?></th>
                        <th><?php echo __('Modified By');?></th>
                        <th><?php echo __('Actions');?></th>
                    </tr>
                </thead>
                  <?php if(empty($users)): ?>
		<tr>
			<td colspan="12" class="striped-info">No Change History found.</td>
		</tr>
		<?php endif;  ?>

                <tbody>
                
				<?php 
				$i=0;
				foreach ($users as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				?>
                <tr <?php echo $class;?> > 
                <td><?php echo h($user['Product']['product_ndc']); ?></td>              
                <td><?php echo h($user['Product']['productname']); ?>&nbsp;</td>
                <td><?php echo h($user['ProductPriceChange']['WAC']); ?>&nbsp;</td>
                <td><?php echo h($user['ProductPriceChange']['Old_Extended_Price']); ?>&nbsp;</td>
                <td><?php echo h($user['ProductPriceChange']['Purchase_Price']); ?>&nbsp;</td>
                <td><?php echo h($user['ProductPriceChange']['Invoice']); ?>&nbsp;</td>
                <td><?php echo h($user['ProductPriceChange']['Vendor']); ?>&nbsp;</td>                  
                <td><?php echo h(date("m-d-Y",strtotime($user['ProductPriceChange']['MODIFIED_DATE']))); ?>&nbsp;</td>
                <td><?php echo h($user['User']['fname']); ?>&nbsp;</td> 
                <td><?php echo $this->Html->link($this->Html->tag('i','Invoices',array('class'=>'icon-pencil icon-white')),array('action' => 'admin_product_change_history_invoices', $user['ProductPriceChange']['id']),array("class"=>"btn btn-primary btn-mini tooltip-top","data-original-title"=>"Invoices",'escape'=>false)); ?></td>   
                </tr>
                <?php endforeach; ?>                
                </tbody>
            </table>
            <div class="pagination pagination-right pagination-mini">
	  <ul>
		<?php echo $this->Paginator->prev(''); ?>
		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
		<?php echo $this->Paginator->next(''); ?>
	  </ul>
	</div>
       </div>
      </div>
    </div>


</div>
<script>
$(document).ready(function(){
	<?php if($search!=''){?>
	$("#foucs").focus();
	$("#foucs").hide();
	<?php }?>
	$("#foucs").hide();
	$("#ProductsAdminAddProductsForm").validate({

		rules: {
			"data[Products][productprice]":{
				required: true,
				number:true,

				}
		},
		messages: {
			"data[Products][productprice]":"Please enter a valid number",
			
		},
		errorElement:"span"

		});
});

 $('.push').click(function(){
	   
	   var essay_id = $(this).attr('id');
	   $('#myModal').modal('show');
		$("#ProductInfoProdid").val(essay_id);
		$("#product_name").html($(this).data('prodcutname'));
		$('#mymodal').show();
		$( "#ProductInfoExpdate" ).datepicker({dateFormat: 'yy-mm-dd' ,minDate: 0 });
		$("#ProductInfoAdminAddProductsForm").validate({
		rules: {
			"data[ProductInfo][availability]":{
				required: true,
				number:true,
				min: 1

				}
		},
		messages: {
			"data[ProductInfo][availability]":"Please enter a valid number",
			
		},
		errorElement:"span"});
		
	   });

   
    



</script>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Update Product</h4>
      </div>
      <div class="modal-body">
        <div class="col-lg-12">

      <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Add Quantity</h3>
      </div>
      <div class="panel-body">

 <?php echo $this->Form->create('ProductInfo',array('url'=>array('action'=>'admin_productsdetails_updated','controller'=>'Products'),'type'=>'file', 'class'=>'login-from')); ?>
      <div class="form-group">
             
              
           
           <span id="product_name">test</span>
          <?php echo $this->Form->input('prodid',array('type'=>'hidden'));?>
  
              </div>
            
            <div class="gap20"></div>
            <div class="form-group">
               <?php echo $this->Form->input('batchno',array('placeholder' => 'Lot No',
        'label' => '',
		'required'=>'required',
        'class'=>'form-control'));?> 
            
            </div>

                   
            <div class="gap20"></div>

            <div class="form-group">
              <?php echo $this->Form->input('expdate',array('placeholder' => 'Expiry Date',
        'label' => '',
		'type'=>'text',
		'required'=>'required',	
        'class'=>'form-control'));?> 

            </div>

                              
            <div class="gap20"></div>

            <div class="form-group">
              <?php echo $this->Form->input('availability',array('placeholder' => 'Product Availability',
        'label' => '',
		'required'=>'required',
        'class'=>'form-control'));?> 

            </div>
            
           

            
            <div class="gap20"></div>


 <?php echo $this->Form->submit(__('Update'), array('class'=>'btn btn-success', 'div'=>false));?>&nbsp;


          <?php echo $this->Form->end();?>
      </div>
    </div>


</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       
      </div>
    </div>
  </div>
</div>