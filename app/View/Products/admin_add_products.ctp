<div class="col-lg-12">
<h4><a href="<?php echo $this->Html->url('/admin/users');?>">Home</a> :: Manage Products</h4>
<div class="col-lg-12  ">
 <?php echo $this->Form->create('ProductInfo',array('url'=>array('action'=>'admin_add_products','controller'=>'products'),'type'=>'get', 'class'=>'form-horizontal login-from')); ?>
 <div class="clearfix"></div>
  <div class="gap20">&nbsp;<br /><br /><br /></div>
   <div class="gap20"></div>
  <div class="form-group">
   <div class="gap20"></div>
  <div class="col-lg-2"><label for="exampleInputEmail1">Search By:</label></div>
  <div class="col-lg-8"> <div class="form-group">
  <div class="col-lg-4">   <?php echo $this->Form->input('ndcproduct',array('placeholder' => 'Item no/Product NDC/Product Info',
        'label' => false,
		'value'=>$search,
		'required'=>'required',
        'class'=>'form-control'));?> 
   
  </div>
  <div class="col-lg-4"> <?php echo $this->Form->button('<i class="fa fa-search"></i> Search', array(
    'type' => 'submit',
    'class' => 'btn btn-primary',
    'escape' => false
));?></div>

  <div class="clearfix"></div>
  
  <small>Search By <cite title="Source Title">Item no/Product NDC/Product Details</cite></small>
            </div></div>

  </div>
  <?php echo $this->Form->end();?>
  <div class="clearfix"></div>
  <div class="gap20"></div>
</div>
      <div class="panel panel-primary">
      
      <div class="panel-heading">
        <h3 class="panel-title">Products</h3>
      </div>
      <div class="panel-body" style="margin:15px">

    
           <?php echo $this->Form->create('Products',array('action'=>'admin_add_products','type'=>'file', 'class'=>'form-horizontal login-from')); ?>
		<?php  if ($id == NULL) {?>
           <div class="form-group">
             <?php echo $this->Form->input('itemnumber',array('placeholder' => 'Item Number',
        'label' => 'Item Number',
		'type' =>'hidden',
		'default'=>0,
		'required'=>'required',
        'class'=>'form-control'));?> 
            </div>
			<?php }else{?>
            <div class="form-group">
             <?php echo $this->Form->input('itemnumber',array('placeholder' => 'Item Number',
        'label' => 'Item Number',
		'type' =>'text',
		'default'=>0,
		'required'=>'required',
        'class'=>'form-control'));?> 
            </div>
            <?php }?>
            <div class="form-group">
              <?php echo $this->Form->input('product_ndc',array('placeholder' => 'Product NDC',
        'label' => 'Product NDC',
		'required'=>'required',
        'class'=>'form-control'));?> 
            </div>
            
            
            
            <div class="form-group">
              <?php echo $this->Form->input('product_size',array('placeholder' => 'Product Size',
        'label' => 'Product Size',
		'required'=>'required',
        'class'=>'form-control'));?> 
            </div>
            
            <div class="form-group">
              <?php echo $this->Form->input('product_unit',array('placeholder' => 'Product Unit',
        'label' => 'Product Unit',
		'required'=>'required',
        'class'=>'form-control'));?> 
            </div>
    
               <div class="form-group">
               <?php echo $this->Form->input('productname',array('placeholder' => 'Product Name',
        'label' => 'Product Name',
		'required'=>'required',	
        'class'=>'form-control'));?> 
            </div>
         
            <div class="form-group">
              <?php echo $this->Form->input('producttype',array('onchange'=>'calculate_productprice()','options' => array(''=>'Product Type','Brand'=>'Brand','Generic'=>'Generic','OTC'=>'OTC','Refrigirated'=>'Refrigirated','Controlled'=>'Controlled'),
        'label' => 'Product Type',
		'required'=>'required',
        'class'=>'form-control'));?>
            </div>
            <?php if($id==''){?>
            <div class="form-group">
            
        		<?php echo $this->Form->input('original_price',array('placeholder' => 'Purchase Price',
        'label' => 'Purchase Price',
		'required'=>'required',			
        'class'=>'form-control'));?> 
        
            </div>
            <?php }else{?>
            <div class="form-group">
            <div class="col-xs-2">
               <?php echo $this->Form->input('Previous_Price1',array('placeholder' => 'Purchase Price',
        'label' => 'Previous Price1',	
		'value'=>$ProductPriceChange[1]['ProductPriceChange']['Old_Extended_Price']	,			
        'class'=>'form-control'));?>
        	</div>
            <div class="col-xs-2">
               <?php echo $this->Form->input('Previous_Price2',array('placeholder' => 'Previous Price2',
        'label' => 'Previous Price2',
		'value'=>$ProductPriceChange[0]['ProductPriceChange']['Old_Extended_Price']	,					
        'class'=>'form-control'));?>
        	</div>
            <div class="col-xs-2">
        		<?php echo $this->Form->input('original_price',array('placeholder' => 'Purchase Price',
        'label' => 'Purchase Price',
		'required'=>'required',			
        'class'=>'form-control'));?> 
        </div>
        		<div class="col-xs-2">
        		<?php echo $this->Form->input('update_price',array('placeholder' => 'Update Price',
        'label' => 'Update Price',				
        'class'=>'form-control'));?> 
        </div>
        
        <div class="col-xs-2">
        		<?php echo $this->Form->input('vendor_update',array('placeholder' => 'Vendor',
        'label' => 'Vendor',					
        'class'=>'form-control'));?> 
        </div>
        
        <div class="col-xs-2">
        		<?php echo $this->Form->input('invoice_update',array('placeholder' => 'Invoice',
        'label' => 'Invoice',					
        'class'=>'form-control'));?> 
        <?php echo $this->Html->link($this->Html->tag('i','Changed History ',array('class'=>'icon-pencil icon-white')),array('action' => 'admin_product_change_history', $id),array("class"=>"btn btn-primary btn-mini tooltip-top","data-original-title"=>"Edit",'target'=>"_blank",'escape'=>false)); ?>

        </div>
        
            </div>
             <?php }?>
            <div class="form-group">
               <?php echo $this->Form->input('PurchasePrice',array('placeholder' => 'Min Selling Price',
        'label' => 'Min Selling Price',
		'onblur'=>'calculate_productprice()',
			
        'class'=>'form-control'));?> 
            </div>
            

            
            <div class="form-group">
              <?php echo $this->Form->input('qtyinstock',array('placeholder' => 'Threshold',
        'label' => 'Threshold',
		'required'=>'required',
        'class'=>'form-control'));?> 
            </div>
            
           <div class="form-group">
              <?php echo $this->Form->input('manufacture',array('placeholder' => 'Manufacture',
        'label' => 'Manufacture',
		'required'=>'required',
        'class'=>'form-control'));?> 
            </div>
        <div class="form-group">
              <?php echo $this->Form->input('awp',array('placeholder' => 'AWP',
        'label' => 'AWP',		
        'class'=>'form-control'));?> 
            </div>
            
		<div class="form-group">
              <?php echo $this->Form->input('WAC',array('placeholder' => 'WAC',
        'label' => 'WAC',
		'required'=>'required',
        'class'=>'form-control'));?> 
            </div>
            
            <div class="form-group">
               <?php echo $this->Form->input('productprice',array('placeholder' => 'Extended Price',
        'label' => 'Extended Price',
		'readonly'=>'readonly',	
		'type'=>'hidden',
		'required'=>'required',
        'class'=>'form-control'));?> 
            </div>
            
          <div class="form-group">
              <?php echo $this->Form->input('source',array('placeholder' => 'Source',
        'label' => 'Source',
		
        'class'=>'form-control'));?> 
            </div>   
            <div class="gap20"></div>
            <div class="form-group">
              <?php echo $this->Form->input('sub_ndc',array('placeholder' => 'Sub Product NDC',
        'label' => 'Sub Product NDC',
		'disabled'=>'disabled',	
        'class'=>'form-control'));?> 
            </div>
            <div class="col-md-12 form-group">
              <?php echo $this->Form->input('substitute',array('placeholder' => 'Substitute',
        'label' => 'Substitute',
		'type'=>'checkbox',
		'style'=>'width:20px',
        'class'=>''));?> 
            </div>
            <div class="col-md-12 form-group">
              <?php echo $this->Form->input('add_restriction',array('placeholder' => 'Add Restriction',
        'label' => 'Add Restriction',
		'type'=>'checkbox',
		'style'=>'width:20px',
        'class'=>''));?> 
            </div>
            
            <div class="gap20"></div>

<div class="col-md-12 form-group">&nbsp;</div>
 <div class="form-actions">
 <?php echo $this->Form->input('id',array('type'=>'hidden'));?>
 <?php echo $this->Form->input('createddt',array('type'=>'hidden','value'=>date("Y-m-d H:i:s")));?>
<?php echo $this->Form->submit(__('Submit'), array('class'=>'btn btn-primary', 'div'=>false));?>&nbsp;
<?php echo $this->Form->reset(__('Cancel'), array('class'=>'btn btn-primary', 'div'=>false));?>
</div>

</fieldset>
<?php echo $this->Form->end();?>
      </div>
    </div>


</div>
<div class="col-lg-12 text-right" style="padding:20px"> &nbsp;&nbsp;<a class="btn btn-primary btn-mini btn-left-margin" href="<?php echo $this->Html->url(array_merge(array('controller'=>'products','action'=>'add_productscsv'),$this->params['named'],array(''))).'/Reporte-'.date('m-d-Y-His-A').'.csv'; ?>"><i class="icon-file-text"></i> Download Excel/CSV</a></div>
<div class="col-lg-12 text-center">

      <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Product List</h3>
      </div>
      <div class="panel-body">
       <div class="table-responsive">
             
			<input type="text" name="foucs" id="foucs" />
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                      
                        <th><?php echo __('Item #');?></th>
                        <th><?php echo __('Product NDC');?></th> 
                        <th><?php echo __('Product Info');?></th> 
                        <th><?php echo __('Manufacture');?></th>
                        <th><?php echo __('WAC');?></th>
                        <th><?php echo __('AWP');?></th>
                        <th><?php echo __('Threshold');?></th>
                        <th><?php echo __('IN STOCK');?></th>
                        <th><?php echo __('Actions');?></th>
                    </tr>
                </thead>
                
                <tbody>
                
				<?php 
				$i=0;
				foreach ($users as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				$product_instock=0;
				foreach($user['ProductInfo'] as $productsinfo){
					$product_instock=$product_instock+$productsinfo['availability'];
				}
				?>
                <tr <?php echo $class;?> > 
                           
                <td><?php echo h($user['Products']['itemnumber']); ?>&nbsp;</td>
                <td><?php echo h($user['Products']['product_ndc']); ?>&nbsp;</td>
                <td><?php echo h('Product Name: '.$user['Products']['productname'])?><br>
                	Category :<?php echo h($user['Products']['producttype']); ?><br>
                    Price :<?php echo h($user['Products']['productprice']); ?><br />
                    Min Selling Price :<?php echo h($user['Products']['PurchasePrice']); ?><br />
                    Purchase Price :<?php echo h(round($user['Products']['original_price'],2)); ?><br />
                    Source :<?php echo h($user['Products']['source']); ?><br />
                    Last modified  :<?php echo h(date("m-d-Y",strtotime($user['Products']['createddt']))); ?><br />
                    &nbsp;</td>
                    <td><?php echo h($user['Products']['manufacture']); ?>&nbsp;</td>    
                    <td><?php echo h($user['Products']['WAC']); ?>&nbsp;</td>
                    <td><?php echo h($user['Products']['awp']); ?>&nbsp;</td>
                <td><?php echo h($user['Products']['qtyinstock']); ?>&nbsp;</td>    
                 <td><?php echo $this->Html->link($product_instock,array('action' => 'admin_manage_products?ndcproduct='.$user['Products']['product_ndc']),array("class"=>"btn btn-primary btn-mini tooltip-top","data-original-title"=>"Edit",'escape'=>false)); ?></td>              
                <td class="center" style="text-align:left"> <?php echo $this->Html->link($this->Html->tag('i','Edit',array('class'=>'icon-pencil icon-white')),array('action' => 'admin_add_products', $user['Products']['id']),array("class"=>"btn btn-primary btn-mini tooltip-top","data-original-title"=>"Edit",'escape'=>false)); ?>
                            <?php if($user['Products']['active']=="False"){
				    echo $this->Form->postLink($this->Html->tag('i','Activate',array('class'=>'icon-remove icon-white')), array('action' => 'admin_products_activate', $user['Products']['id']), array("class"=>"btn btn-success btn-mini tooltip-top","data-original-title"=>"Delete",'escape'=>false), __('Are you sure you want to Activate # %s?', $user['Products']['productname']));
			   }else{
                 echo $this->Form->postLink($this->Html->tag('i','Deactivate',array('class'=>'icon-remove icon-white')), array('action' => 'admin_products_delete', $user['Products']['id']), array("class"=>"btn btn-danger btn-mini tooltip-top","data-original-title"=>"Delete",'escape'=>false), __('Are you sure you want to Deactivate # %s?', $user['Products']['productname']));
			   }			   
			   ?>
               
                <!--<a href="#" rel="chatusr" data-prodcutname="<?php echo $user['Products']['productname']?>"  id="<?php echo $user['Products']['id']?>" class="push btn btn-success btn-mini" data-toggle="modal">Update</a>
--> 				</td>
                </tr>
                <?php endforeach; ?>                
                </tbody>
            </table>
            <div class="pagination pagination-right pagination-mini">
	  <ul>
		<?php echo $this->Paginator->prev(''); ?>
		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
		<?php echo $this->Paginator->next(''); ?>
	  </ul>
	</div>
       </div>
      </div>
    </div>


</div>
<script>
$(document).ready(function(){
	<?php if($search!=''){?>
	$("#foucs").focus();
	$("#foucs").hide();
	<?php }?>
	$("#foucs").hide();
	$("#ProductsAdminAddProductsForm").validate({

		rules: {
			"data[Products][productprice]":{
				required: true,
				number:true,

				}
		},
		messages: {
			"data[Products][productprice]":"Please enter a valid number",
			
		},
		errorElement:"span"

		});
});

 $('.push').click(function(){
	   
	   var essay_id = $(this).attr('id');
	   $('#myModal').modal('show');
		$("#ProductInfoProdid").val(essay_id);
		$("#product_name").html($(this).data('prodcutname'));
		$('#mymodal').show();
		$( "#ProductInfoExpdate" ).datepicker({dateFormat: 'yy-mm-dd' ,minDate: 0 });
		$("#ProductInfoAdminAddProductsForm").validate({
		rules: {
			"data[ProductInfo][availability]":{
				required: true,
				number:true,
				min: 1

				}
		},
		messages: {
			"data[ProductInfo][availability]":"Please enter a valid number",
			
		},
		errorElement:"span"});
		
	   });

   
 function calculate_productprice(){
	 //console.log($('#ProductsProducttype').val());	 
	 
	 var product_price=0;
	 //Foe Brand
	 if($('#ProductsProducttype').val()=='Brand'){
		  if(($('#ProductsPurchasePrice').val())>0 && ($('#ProductsPurchasePrice').val())<1)
		 product_price=.5;

		 if(($('#ProductsPurchasePrice').val())>=1 && ($('#ProductsPurchasePrice').val())<=5)
		 product_price=1;
		 if(($('#ProductsPurchasePrice').val())>=6 && ($('#ProductsPurchasePrice').val())<=20)
		 product_price=1.45;
		if(($('#ProductsPurchasePrice').val())>=21 && ($('#ProductsPurchasePrice').val())<=50)
		 product_price=2;
		if(($('#ProductsPurchasePrice').val())>=51 && ($('#ProductsPurchasePrice').val())<=100)
		 product_price=3;
		if(($('#ProductsPurchasePrice').val())>=101 && ($('#ProductsPurchasePrice').val())<=200)
		 product_price=4;
		if(($('#ProductsPurchasePrice').val())>=201 && ($('#ProductsPurchasePrice').val())<=400)
		 product_price=5;
		if(($('#ProductsPurchasePrice').val())>=401 && ($('#ProductsPurchasePrice').val())<=600)
		 product_price=6;
		if(($('#ProductsPurchasePrice').val())>=601)
		 product_price=7;
	 }
	 //Foe Generic
	 if($('#ProductsProducttype').val()=='Generic'){
		  if(($('#ProductsPurchasePrice').val())>0 && ($('#ProductsPurchasePrice').val())<1)
		 product_price=.5;

		 if(($('#ProductsPurchasePrice').val())>=1 && ($('#ProductsPurchasePrice').val())<=5)
		 product_price=1;
		 if(($('#ProductsPurchasePrice').val())>=6 && ($('#ProductsPurchasePrice').val())<=20)
		 product_price=1.45;
		if(($('#ProductsPurchasePrice').val())>=21 && ($('#ProductsPurchasePrice').val())<=50)
		 product_price=2;
		if(($('#ProductsPurchasePrice').val())>=51 && ($('#ProductsPurchasePrice').val())<=100)
		 product_price=3;
		if(($('#ProductsPurchasePrice').val())>=101 && ($('#ProductsPurchasePrice').val())<=200)
		 product_price=5;
		if(($('#ProductsPurchasePrice').val())>=201 && ($('#ProductsPurchasePrice').val())<=400)
		 product_price=7;
		if(($('#ProductsPurchasePrice').val())>=401 && ($('#ProductsPurchasePrice').val())<=600)
		 product_price=9;
		if(($('#ProductsPurchasePrice').val())>=601)
		 product_price=11;
	 }

	 //Foe Refrigirated
	 if($('#ProductsProducttype').val()=='Refrigirated'){
		  if(($('#ProductsPurchasePrice').val())>0 && ($('#ProductsPurchasePrice').val())<1)
		 product_price=.5;

		 if(($('#ProductsPurchasePrice').val())>=1 && ($('#ProductsPurchasePrice').val())<=5)
		 product_price=1;
		 if(($('#ProductsPurchasePrice').val())>=6 && ($('#ProductsPurchasePrice').val())<=20)
		 product_price=1.45;
		if(($('#ProductsPurchasePrice').val())>=21 && ($('#ProductsPurchasePrice').val())<=50)
		 product_price=2;
		if(($('#ProductsPurchasePrice').val())>=51 && ($('#ProductsPurchasePrice').val())<=100)
		 product_price=3;
		if(($('#ProductsPurchasePrice').val())>=101 && ($('#ProductsPurchasePrice').val())<=200)
		 product_price=4;
		if(($('#ProductsPurchasePrice').val())>=201 && ($('#ProductsPurchasePrice').val())<=400)
		 product_price=5;
		if(($('#ProductsPurchasePrice').val())>=401 && ($('#ProductsPurchasePrice').val())<=600)
		 product_price=6;
		if(($('#ProductsPurchasePrice').val())>=601)
		 product_price=7;
	 }
	 
	 //Foe OTC
	 if($('#ProductsProducttype').val()=='OTC'){
		  if(($('#ProductsPurchasePrice').val())>0 && ($('#ProductsPurchasePrice').val())<1)
		 product_price=.5;

		 if(($('#ProductsPurchasePrice').val())>=1 && ($('#ProductsPurchasePrice').val())<=5)
		 product_price=1;
		 if(($('#ProductsPurchasePrice').val())>=6 && ($('#ProductsPurchasePrice').val())<=20)
		 product_price=1.45;
		if(($('#ProductsPurchasePrice').val())>=21 && ($('#ProductsPurchasePrice').val())<=50)
		 product_price=2;
		if(($('#ProductsPurchasePrice').val())>=51 && ($('#ProductsPurchasePrice').val())<=100)
		 product_price=3;
		if(($('#ProductsPurchasePrice').val())>=101 && ($('#ProductsPurchasePrice').val())<=200)
		 product_price=5;
		if(($('#ProductsPurchasePrice').val())>=201 && ($('#ProductsPurchasePrice').val())<=400)
		 product_price=7;
		if(($('#ProductsPurchasePrice').val())>=401 && ($('#ProductsPurchasePrice').val())<=600)
		 product_price=9;
		if(($('#ProductsPurchasePrice').val())>=601)
		 product_price=11;
	 }
	 
	  //Foe controlled 
	 if($('#ProductsProducttype').val()=='Controlled'){
		 product_price=((3*$('#ProductsPurchasePrice').val())/100)
		

	 }
	 
	 
	 $('#ProductsProductprice').val(((parseFloat($('#ProductsPurchasePrice').val()))+parseFloat(product_price)));
 }
 
 $("#ProductsSubstitute").change(function() {
    if(this.checked) {
        $("#ProductsSubNdc").removeAttr('disabled');
		}else{
			 $("#ProductsSubNdc").attr('disabled','disabled');
		}
	});

$(document).ready(function(e) {
	var isChecked = $('#ProductsSubNdc').attr('checked')?true:false;
	if(isChecked){
		$("#ProductsSubNdc").removeAttr('disabled');
	}
     $('#ProductsProductprice').val((parseFloat($('#ProductsProductprice').val())).toFixed(2));
	 
	 
});

</script>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Update Product</h4>
      </div>
      <div class="modal-body">
        <div class="col-lg-12">

      <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Add Quantity</h3>
      </div>
      <div class="panel-body">

 <?php echo $this->Form->create('ProductInfo',array('url'=>array('action'=>'admin_productsdetails_updated','controller'=>'Products'),'type'=>'file', 'class'=>'login-from')); ?>
      <div class="form-group">
             
              
           
           <span id="product_name">test</span>
          <?php echo $this->Form->input('prodid',array('type'=>'hidden'));?>
  
              </div>
            
            <div class="gap20"></div>
            <div class="form-group">
               <?php echo $this->Form->input('batchno',array('placeholder' => 'Lot No',
        'label' => '',
		'required'=>'required',
        'class'=>'form-control'));?> 
            
            </div>

                   
            <div class="gap20"></div>

            <div class="form-group">
              <?php echo $this->Form->input('expdate',array('placeholder' => 'Expiry Date',
        'label' => '',
		'type'=>'text',
		'required'=>'required',	
        'class'=>'form-control'));?> 

            </div>

                              
            <div class="gap20"></div>

            <div class="form-group">
              <?php echo $this->Form->input('availability',array('placeholder' => 'Product Availability',
        'label' => '',
		'required'=>'required',
        'class'=>'form-control'));?> 

            </div>
            
           

            
            <div class="gap20"></div>


 <?php echo $this->Form->submit(__('Update'), array('class'=>'btn btn-success', 'div'=>false));?>&nbsp;


          <?php echo $this->Form->end();?>
      </div>
    </div>


</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       
      </div>
    </div>
  </div>
</div>