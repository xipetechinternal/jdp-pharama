<div class="col-lg-12">
<h4><a href="<?php echo $this->Html->url('/admin/users');?>">Home</a> :: Manage Products</h4>

      <div class="panel panel-primary">
      
      
      
    </div>


</div>
<div class="col-lg-12  ">
 <?php echo $this->Form->create('ProductInfo',array('url'=>array('action'=>'admin_deactive_products','controller'=>'products'),'type'=>'get', 'class'=>'form-horizontal login-from')); ?>
 <div class="clearfix"></div>
  <div class="gap20">&nbsp;<br /><br /><br /></div>
   <div class="gap20"></div>
  <div class="form-group">
   <div class="gap20"></div>
  <div class="col-lg-2"><label for="exampleInputEmail1">Search By:</label></div>
  <div class="col-lg-8"> <div class="form-group">
  <div class="col-lg-4">   <?php echo $this->Form->input('ndcproduct',array('placeholder' => 'Item no/Product NDC/Product Info',
        'label' => false,
		'value'=>$search,
		'required'=>'required',
        'class'=>'form-control'));?> 
   
  </div>
  <div class="col-lg-4"> <?php echo $this->Form->button('<i class="fa fa-search"></i> Search', array(
    'type' => 'submit',
    'class' => 'btn btn-primary',
    'escape' => false
));?></div>

  <div class="clearfix"></div>
  
  <small>Search By <cite title="Source Title">Item no/Product NDC/Product Details</cite></small>
            </div></div>

  </div>
  <?php echo $this->Form->end();?>
  <div class="clearfix"></div>
  <div class="gap20"></div>
</div>
<div class="col-lg-12 text-center">

      <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Deactive Product List</h3>
      </div>
      <div class="panel-body">
       <div class="table-responsive">
             
			<input type="text" name="foucs" id="foucs" />
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                        
                        <th><?php echo __('Item #');?></th>
                        <th><?php echo __('Product NDC');?></th> 
                        <th><?php echo __('Product Info');?></th> 
                        <th><?php echo __('Manufacture');?></th>
                        <th><?php echo __('WAC');?></th>
                        <th><?php echo __('AWP');?></th>
                        <th><?php echo __('Threshold');?></th>
                        <th><?php echo __('Actions');?></th>
                    </tr>
                </thead>
                
                <tbody>
                
				<?php 
				$i=0;
				foreach ($users as $user): 
				$class =' class="odd"';
				if ($i++ % 2 == 0) {
					$class = ' class="even"';
				}
				?>
                <tr <?php echo $class;?> > 
                          
                <td><?php echo h($user['Products']['itemnumber']); ?>&nbsp;</td>
                <td><?php echo h($user['Products']['product_ndc']); ?>&nbsp;</td>
                <td><?php echo h('Product Name: '.$user['Products']['productname'])?><br>
                	Category :<?php echo h($user['Products']['producttype']); ?><br>
                    Price :<?php echo h($user['Products']['productprice']); ?><br />
                    Purchase Price :<?php echo h($user['Products']['PurchasePrice']); ?><br />
                    &nbsp;</td>
                    <td><?php echo h($user['Products']['manufacture']); ?>&nbsp;</td>    
                    <td><?php echo h($user['Products']['WAC']); ?>&nbsp;</td>
                    <td><?php echo h($user['Products']['awp']); ?>&nbsp;</td>
                <td><?php echo h($user['Products']['qtyinstock']); ?>&nbsp;</td>                
                <td class="center">
               <?php if($user['Products']['active']=="False"){
				    echo $this->Form->postLink($this->Html->tag('i','Activate',array('class'=>'icon-remove icon-white')), array('action' => 'admin_products_activate', $user['Products']['id']), array("class"=>"btn btn-primary btn-mini tooltip-top","data-original-title"=>"Delete",'escape'=>false), __('Are you sure you want to Activate # %s?', $user['Products']['productname']));
			   }else{
                 echo $this->Form->postLink($this->Html->tag('i','Deactivate',array('class'=>'icon-remove icon-white')), array('action' => 'admin_products_delete', $user['Products']['id']), array("class"=>"btn btn-danger btn-mini tooltip-top","data-original-title"=>"Delete",'escape'=>false), __('Are you sure you want to Deactivate # %s?', $user['Products']['productname']));
			   }?>
                <!--<a href="#" rel="chatusr" data-prodcutname="<?php echo $user['Products']['productname']?>"  id="<?php echo $user['Products']['id']?>" class="push btn btn-success btn-mini" data-toggle="modal">Update</a>
--> 				</td>
                </tr>
                <?php endforeach; ?>                
                </tbody>
            </table>
            <div class="pagination pagination-right pagination-mini">
	  <ul>
		<?php echo $this->Paginator->prev(''); ?>
		<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2));?>
		<?php echo $this->Paginator->next(''); ?>
	  </ul>
	</div>
       </div>
      </div>
    </div>


</div>
<script>
$(document).ready(function(){
	<?php if($search!=''){?>
	$("#foucs").focus();
	$("#foucs").hide();
	<?php }?>
	$("#foucs").hide();
	$("#ProductsAdminAddProductsForm").validate({

		rules: {
			"data[Products][productprice]":{
				required: true,
				number:true,

				}
		},
		messages: {
			"data[Products][productprice]":"Please enter a valid number",
			
		},
		errorElement:"span"

		});
});

 $('.push').click(function(){
	   
	   var essay_id = $(this).attr('id');
	   $('#myModal').modal('show');
		$("#ProductInfoProdid").val(essay_id);
		$("#product_name").html($(this).data('prodcutname'));
		$('#mymodal').show();
		$( "#ProductInfoExpdate" ).datepicker({dateFormat: 'yy-mm-dd' ,minDate: 0 });
		$("#ProductInfoAdminAddProductsForm").validate({
		rules: {
			"data[ProductInfo][availability]":{
				required: true,
				number:true,
				min: 1

				}
		},
		messages: {
			"data[ProductInfo][availability]":"Please enter a valid number",
			
		},
		errorElement:"span"});
		
	   });

   
    



</script>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Update Product</h4>
      </div>
      <div class="modal-body">
        <div class="col-lg-12">

      <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Add Quantity</h3>
      </div>
      <div class="panel-body">

 <?php echo $this->Form->create('ProductInfo',array('url'=>array('action'=>'admin_productsdetails_updated','controller'=>'Products'),'type'=>'file', 'class'=>'login-from')); ?>
      <div class="form-group">
             
              
           
           <span id="product_name">test</span>
          <?php echo $this->Form->input('prodid',array('type'=>'hidden'));?>
  
              </div>
            
            <div class="gap20"></div>
            <div class="form-group">
               <?php echo $this->Form->input('batchno',array('placeholder' => 'Lot No',
        'label' => '',
		'required'=>'required',
        'class'=>'form-control'));?> 
            
            </div>

                   
            <div class="gap20"></div>

            <div class="form-group">
              <?php echo $this->Form->input('expdate',array('placeholder' => 'Expiry Date',
        'label' => '',
		'type'=>'text',
		'required'=>'required',	
        'class'=>'form-control'));?> 

            </div>

                              
            <div class="gap20"></div>

            <div class="form-group">
              <?php echo $this->Form->input('availability',array('placeholder' => 'Product Availability',
        'label' => '',
		'required'=>'required',
        'class'=>'form-control'));?> 

            </div>
            
           

            
            <div class="gap20"></div>


 <?php echo $this->Form->submit(__('Update'), array('class'=>'btn btn-success', 'div'=>false));?>&nbsp;


          <?php echo $this->Form->end();?>
      </div>
    </div>


</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       
      </div>
    </div>
  </div>
</div>