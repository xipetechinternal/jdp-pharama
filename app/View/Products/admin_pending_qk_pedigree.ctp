<?php echo $this->Html->script(array('jQuery.print'));?>
<div class="col-lg-12">
<h4><a href="<?php echo $this->Html->url('/admin/users');?>">Home</a> :: TI,TH,TS</h4>
 <div class="row">
  <div class="col-xs-9">
  
  </div>
  <div class="col-xs-3">
  
          <button type="button" class="btn btn-primary print" data-dismiss="modal">Print</button>
          <button class="btn btn-primary btn-mini tooltip-top" onclick="history.go(-1);">Back </button> <br /><br />
       
   </div>
  </div>     
<div class="col-lg-12" id="print_div">
<div class="panel panel-primary" id="hederdiv">
      <div class="panel-heading">
        <h3 class="panel-title">TI,TH,TS</h3>
      </div>
      
          
      
      
      
    </div>
    <h3 class="text-left">TI</h3>
    <?php
	$pedigree=$dhtml['TempPedigree']['INVOICE_T1'];
					  $pedigree=json_decode($pedigree);
					  ?>
     <?php echo $this->Form->create('pedigree',array('url'=>array('action'=>'admin_pending_qk_pedigree/'.$dhtml['TempPedigree']['id'],'controller'=>'products'), 'class'=>'form-horizontal login-from')); ?>
     <div class="col-lg-12 text-right"> <?php echo $this->Form->button('Update', array(
    'type' => 'submit',
    'class' => 'btn btn-primary',
    'escape' => false
));?></div>
<div class="gap20"><br /></div>
<table  style="width:100%" border="1" cellpadding="0" cellspacing="0" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                     <th align="center"><?php echo __('Ndc');?></th>
                       <th align="center"><?php echo __('Item #');?></th>
                        <th align="center"><?php echo __('dis');?></th>
                        <th align="center"><?php echo __('lot');?></th>
                        <th align="center"><?php echo __('Exp');?></th>
                        <th align="center"><?php echo __('Rec date');?></th> 
                       <th align="center"><?php echo __('Quantity');?></th> 
                       <th align="center"><?php echo __('Manufacture');?></th> 
                    </tr>
                </thead>                
                <tbody>
               
						<tr> 
						 <td align="center"><?php echo $dhtml['TempPedigree']['NDC'];?></td>
                         <td align="center"><?php echo @$dhtml['Product']['itemnumber'];?></td>               
						 <td align="center"><?php echo $dhtml['TempPedigree']['Description'];?></td>
						 <td align="center"><?php echo $dhtml['TempPedigree']['LOT'];?></td>              
						 <td align="center"><?php echo $pedigree[0]->Expiration;?></td>
						 <td align="center"><?php echo date('m-d-Y');?></td>
                         <td align="center"><?php $qty=$dhtml['TempPedigree']['Qty'];?>
						 <?php echo $this->Form->input('Quantity',array('placeholder' => 'Quantity','value'=>$qty,
        'label' => false,
		'required'=>'required',
        'class'=>'form-control'));?></td>  
						<td align="center"><?php echo $pedigree[0]->Manufacturer;?></td>
						</tr>
                                  
                </tbody>               
            </table>
  
  <div class="gap20"></div>
  <h3 class="text-left">TH</h3>
	<?php	$tabledata=json_decode($dhtml['TempPedigree']['Invoice_T2']);
	$tabledata=str_replace('You','JD Pharmaceutical',$tabledata);
	$tabledata=str_replace("nbsp",'',$tabledata);
	$tabledata=str_replace("&",'',$tabledata);
	$tabledata=str_replace("amp;",'',$tabledata);
			echo $tabledata=str_replace("<table>",'<table class="table table-hover table-striped table-bordered" id="pedgreetable">',$tabledata);
		$Invoice_P1=json_decode($dhtml['TempPedigree']['Invoice_P1']);
	//echo '<br><p align="center">'.$Invoice_P1.'</p>';
	
	//$Invoice_P2=json_decode($dhtml['TempPedigree']['Invoice_P2']);
	//echo '<p align="center"><img src="'.$Invoice_P2.'" width="20%"></p>';
	
	//$Invoice_P3=json_decode($dhtml['TempPedigree']['Invoice_P3']);
	//echo '<br><p align="center">'.$Invoice_P3.'</p>';
	?>
    <br>
    <h3 class="text-left">TS</h3>
    
    <strong>Jd pharmaceutical wholesaler inc. certifies that it</strong>
    <br>
    <br>
    <ol>
    	<li>is authorized under the Drug Quality and Security Act (the "Act") to sell the products identified in this Transaction History to you</li>
    <li>received these products from a person or entity authorized under the Act</li>
    <li>received a Transaction Statement from the prior owner of these products</li>    
    <li> did not knowingly ship a suspect or illegitimate produc</li>    
    <li> had systems in place to comply with the verification requirements of the Act</li>    
    <li>did not knowingly provide false Transaction Information </li>
    <li>did not knowingly alter the Transaction History</li>
   </ol>
     <p></p>
   <!-- <p align="center">
jd pharmaceutical wholesaler inc. certifies that it:(a) is authorized under the Drug Quality and Security Act (the "Act") to sell the products identified in this Transaction History to you(b) received these products from a person or entity authorized under the Act(c) received a Transaction Statement from the prior owner of these products(d) did not knowingly ship a suspect or illegitimate product(e) had systems in place to comply with the verification requirements of the Act(f) did not knowingly provide false Transaction Information (g) did not knowingly alter the Transaction History.</p>-->
   <!-- <br><p align="center">DEROU BIGLARI</p>
    <p align="center">OPERATIONS MANAGER
JD PHARMACEUTICAL WHOLESALER INC

         			</p>-->
    
</div>

</div>
<script>
	$('.print').click(function(){
		$('.print').hide();
     //$("#print_div").print();
	 printDiv("print_div");
	 $('.print').show();
});
$("#pedgreetable tr:nth-child(2) td:last").html("Invoice No <?php echo $dhtml["TempPedigree"]["Invoice"]?>");

function printDiv(id) {

  var html = "";

  $('link').each(function() { // find all <link tags that have
    if ($(this).attr('rel').indexOf('stylesheet') !=-1) { // rel="stylesheet"
      html += '<link rel="stylesheet" href="'+$(this).attr("href")+'" />';
    }
  });
  html += '<link rel="stylesheet" type="text/css" media="print" href="http://jdpwholesaler.com/css/admin/print.css"><style>table{font-size:9px;}.panel-title{font-size:10px} h3{font-size:10px; margin:0px !important} img{height:40px;}#hederdiv{display:none}</style><body onload="window.focus(); window.print()" style="font-size:9px !important">'+$("#"+id).html()+'</body>';
  var w = window.open("","print");
  if (w) { w.document.write(html); w.document.close() }

}
</script>  
<style>
table {
max-width: 100%;
background-color: transparent;
border-collapse: collapse;
border-spacing: 0;
border:1px;
}
th {
text-align: center;
font-weight:bold;
}
h5, .h5 {
font-weight:bold;
}
</style>