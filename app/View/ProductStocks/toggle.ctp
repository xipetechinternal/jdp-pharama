<div class="btn-group">
<button class="btn btn-mini btn-info">Info</button>
<button data-toggle="dropdown" class="btn btn-info btn-mini dropdown-toggle"><span class="caret"></span></button>
<ul class="dropdown-menu">
<li><a href="#myModal" data-toggle="modal" onclick="uprofiles.toggle('');"><i class="icon-user"></i> Clients <span class="badge"></span></a></li>

<li><a href="#"><i class="icon-tags"></i> Orders <span class="badge"></span></a></li>
</ul>
</div>

<?php echo $this->Html->link($this->Html->tag('i',false,array('class'=>'icon-eye-open')),'#myModal',array("class"=>"btn btn-mini tooltip-top","data-original-title"=>"View","data-toggle"=>"modal",'escape'=>false,'onclick' => "view.toggle('".$this->Html->url('/admin/productStocks/view/').$user_id."');")); ?>

<?php 
if($status)
echo $this->Html->link($this->Html->tag('i',false,array('class'=>'icon-ok icon-white')),'#',array('class'=>'btn btn-inverse btn-mini tooltip-top','data-original-title'=>'Publish','escape'=>false,'onclick' => "published.toggle('status-".$user_id."','".$this->Html->url('/productStocks/toggle/').$user_id."/".intval($status)."');",'id' =>'status-'.$user_id));
else
echo $this->Html->link($this->Html->tag('i',false,array('class'=>'icon-off icon-white')),'#',array('class'=>'btn btn-inverse btn-mini tooltip-top','data-original-title'=>'Publish','escape'=>false,'onclick' => "published.toggle('status-".$user_id."','".$this->Html->url('/productStocks/toggle/').$user_id."/".intval($status)."');",'id' =>'status-'.$user_id));
?>

<?php echo $this->Html->link($this->Html->tag('i',false,array('class'=>'icon-pencil icon-white')),array('action' => 'admin_edit', $user_id,'admin'=>true),array("class"=>"btn btn-primary btn-mini tooltip-top","data-original-title"=>"Edit",'escape'=>false));?>

<?php echo $this->Form->postLink($this->Html->tag('i',false,array('class'=>'icon-remove icon-white')), array('action' => 'delete', $user_id,'admin'=>true), array("class"=>"btn btn-danger btn-mini tooltip-top","data-original-title"=>"Delete",'escape'=>false), __('Are you sure you want to delete # %s?', $user_id));?>

