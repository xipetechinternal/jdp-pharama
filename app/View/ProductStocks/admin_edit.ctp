<?php $this->set('title_for_layout', __('Product Stocks',true)); ?>
<?php $this->Html->addCrumb(__('Product Stocks',true), '/admin/productStocks/index',array('tag'=>'li'));  ?>
<?php $this->Html->addCrumb($this->Html->tag('li',__('Edit',true),array('class'=>'active')),false,false); ?>

<div class="row-fluid">
<div class="block">
<div class="navbar navbar-inner block-header">
<div class="muted pull-left"><?php echo __('ProductStocks'); ?></div>
</div>
<div class="block-content collapse in">
<div class="span12">
<?php echo $this->Form->create('ProductStock',array('controller'=>'productStocks','action' => 'admin_edit','class' => 'form-horizontal'));?>
<fieldset>
<legend><?php echo __('Add Prodcut'); ?></legend>

<?php
echo $this->Form->input('ProductStock.id');

echo $this->Form->input('ProductStock.product_id', array('div'=>'control-group',  
'before'=>' <label class="control-label">'.__('Product').' : </label><div class="controls">', 
'after'=>$this->Form->error('ProductStock._id', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused','options'=>$products,'empty'=>'----Select----'));

echo $this->Form->input('ProductStock.lot_no', array('div'=>'control-group',
'before'=>' <label class="control-label">'.__('Lot No.').' : </label><div class="controls">', 
'after'=>$this->Form->error('ProductStock.lot_no', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));


echo $this->Form->input('ProductStock.mfg_date', array('div'=>'control-group',
'before'=>' <label class="control-label">'.__('Mfg Date').' : </label><div class="controls">', 
'after'=>$this->Form->error('ProductStock.mfg_date', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused datepicker','type'=>'text'));

echo $this->Form->input('ProductStock.exp_date', array('div'=>'control-group', 
'before'=>' <label class="control-label">'.__('Exp Date').' : </label><div class="controls">',
'after'=>$this->Form->error('ProductStock.exp_date', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused datepicker','type'=>'text'));

echo $this->Form->input('ProductStock.no_of_quantity', array('div'=>'control-group', 'type'=>'text', 
'before'=>' <label class="control-label">'.__('No Of Quantity').' : </label><div class="controls">',
'after'=>$this->Form->error('ProductStock.no_of_quantity', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'input-xlarge focused'));

echo $this->Form->input('ProductStock.status', array('div'=>'control-group', 
'before'=>' <label class="control-label">'.__('Status').' : </label><div class="controls">',
'after'=>'</div>','label'=>false, 'class'=>'uniform_on','type'=>'checkbox'));
?>                                        

<div class="form-actions">
<?php echo $this->Form->submit(__('Submit'), array('class'=>'btn btn-primary', 'div'=>false));?>&nbsp;
<?php echo $this->Form->reset(__('Cancel'), array('class'=>'btn', 'div'=>false));?>
</div>

</fieldset>
<?php echo $this->Form->end();?>

</div>
</div>
</div>
</div>